# Azure Emulator #
## CodeName: Azure ##
Repository Now Public For Everyone

### Team ###
+Boris (XDR)  
+Antonio (iHton)  
+Antoine (TyrexFr)  
+Claudio (sant0ro/bi0s)  
+Jonas (Jonas)  
+Vabbo (VabboH)

+Jamal (SirJamal)

+Maarten (Maartenvn)

+Lucca Faria(Droppy)

### How to Install Azure Emulator ###


**1** Install .Net Framework 4.6 (4.5.3) [Here](http://adf.ly/1713566/dotnet453slim)


**2** Download the Repository


**3** If doesn't exist this folder: Azure/bin/debug/ Create It


**4** Copy the content of the folder Azure/Azure.Emulator/building/ To Azure/bin/debug


**5** Open The Solution on Visual Studio (Recommended VS 2015, VS 2014, VS 2013)


**6** Click on Build Solution.


**7** If Get Errors Build Again.

**8** Download the swfs and use an r63b supported cms.

**9** Enjoy your retro ;)



**Common Errors**


* The Azure.Emulator isn't the Startup Project, You Must Set It
* Gives a Message saying 'symbols doesn't loaded' This Means Nothing

### Problems or Issues ###
[Report an issue](https://bitbucket.org/hmercury/emulator/issues)

### New Bugs List ###
[Click Here](https://bitbucket.org/hmercury/emulator/issue/30/new-bug-list-09-12-14)

### Bugs List ###
[Click Here](https://bitbucket.org/hmercury/emulator/issue/26/new-complete-bug-list)

### Old Bug List ###
[Click Here](https://bitbucket.org/hmercury/emulator/issue/18/bugs-of-the-emulator-complete-list)