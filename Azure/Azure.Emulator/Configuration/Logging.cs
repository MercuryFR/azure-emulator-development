using System;
using System.Text;

namespace Azure.Configuration
{
    public static class Logging
    {
        internal static bool DisabledState
        {
            get { return Writer.Writer.DisabledState; }
            set { Writer.Writer.DisabledState = value; }
        }

        public static void LogQueryError(Exception exception, string query)
        {
            Writer.Writer.LogQueryError(exception, query);
        }

        internal static void WriteLine(string line, ConsoleColor colour = ConsoleColor.Green)
        {
            Writer.Writer.WriteLine(line, colour);
        }

        internal static void LogException(string logText)
        {
            Writer.Writer.LogException(string.Format("{0}{1}{2}", Environment.NewLine, logText, Environment.NewLine));
        }

        internal static void LogCriticalException(string logText) { Writer.Writer.LogCriticalException(logText); }

        internal static void LogCacheError(string logText) { Writer.Writer.LogCacheError(logText); }

        internal static void LogMessage(string logText) { Writer.Writer.LogMessage(logText); }

        internal static void LogThreadException(string exception, string threadname)
        {
            Writer.Writer.LogThreadException(exception, threadname);
        }

        internal static void LogPacketException(string packet, string exception)
        {
            Writer.Writer.LogPacketException(packet, exception);
        }

        internal static void HandleException(Exception pException, string pLocation)
        {
            Writer.Writer.HandleException(pException, pLocation);
        }

        internal static void DisablePrimaryWriting(bool clearConsole)
        {
            Writer.Writer.DisablePrimaryWriting(clearConsole);
        }

        internal static void LogShutdown(StringBuilder builder) { Writer.Writer.LogShutdown(builder); }
    }
}