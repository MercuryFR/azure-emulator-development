using System;
using Azure.Connection.Connection;
using Azure.Connection.Net;

namespace Azure.Configuration
{
    public class ConnectionHandling
    {
        public NewSocketManager Manager;

        public ConnectionHandling(int port, int maxConnections, int connectionsPerIp, bool enabeNagles)
        {
            Manager = new NewSocketManager();
            Manager.Init(port, maxConnections, connectionsPerIp, new InitialPacketParser(), !enabeNagles);
        }
        internal void Init() { Manager.connectionEvent += ManagerConnectionEvent; }

        internal void Start() { Manager.InitializeConnectionRequests(); }

        internal void Destroy() { Manager.Destroy(); }

        private static void ManagerConnectionEvent(ConnectionInformation connection)
        {
            connection.ConnectionChanged += ConnectionChanged;
            Azure.GetGame().GetClientManager().CreateAndStartClient(checked((uint) connection.GetConnectionId()), connection);
        }

        private static void ConnectionChanged(ConnectionInformation information, ConnectionState state)
        {
            if (state == ConnectionState.Closed)
                CloseConnection(information);
        }

        private static void CloseConnection(ConnectionInformation connection)
        {
            try
            {
                connection.Dispose();
                Azure.GetGame().GetClientManager().DisposeConnection(checked((uint) connection.GetConnectionId()));
            }
            catch (Exception ex)
            {
                Logging.LogException(ex.ToString());
            }
        }
    }
}