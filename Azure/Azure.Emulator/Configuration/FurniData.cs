﻿//************************************************************************************
// FurniData Class
//
// Copyright (c) 2015 Xdr
// All rights reserved.
//
//************************************************************************************

using System;
using System.Collections.Generic;
using System.Net;
using System.Xml;

namespace Azure.Configuration
{
    public struct FurniData
    {
        public short Id;
        public string Name;
        public ushort X, Y;
        public bool CanSit, CanWalk;

        public FurniData(short id, string name, ushort x = 0, ushort y = 0, bool canSit = false, bool canWalk = false)
        {
            Id = id;
            Name = name;
            X = x;
            Y = y;
            CanSit = canSit;
            CanWalk = canWalk;
        }
    }

    static class FurniDataParser
    {
        public static Dictionary<string, FurniData> FloorItems;
        public static Dictionary<string, FurniData> WallItems;
        public static void SetCache()
        {
            var xmlParser = new XmlDocument();
            var wC = new WebClient();
            try
            {
                xmlParser.LoadXml(wC.DownloadString(ExtraSettings.FurniDataUrl));
                FloorItems = new Dictionary<string, FurniData>();
                foreach (XmlNode node in xmlParser.DocumentElement.SelectNodes("/furnidata/roomitemtypes/furnitype"))
                {
                    FloorItems.Add(node.Attributes["classname"].Value,
                        new FurniData(short.Parse(node.Attributes["id"].Value), node.SelectSingleNode("name").InnerText,
                            ushort.Parse(node.SelectSingleNode("xdim").InnerText),
                            ushort.Parse(node.SelectSingleNode("ydim").InnerText),
                            node.SelectSingleNode("cansiton").InnerText == "1",
                            node.SelectSingleNode("canstandon").InnerText == "1"));
                }
                WallItems = new Dictionary<string, FurniData>();
                foreach (XmlNode node in xmlParser.DocumentElement.SelectNodes("/furnidata/wallitemtypes/furnitype"))
                    WallItems.Add(node.Attributes["classname"].Value, new FurniData(short.Parse(node.Attributes["id"].Value), node.SelectSingleNode("name").InnerText));
            }
            catch (WebException e)
            {
                Logging.WriteLine(
                    string.Format("Error downloading furnidata.xml: {0}", Environment.NewLine + e),
                    ConsoleColor.Red);
                Logging.WriteLine("Type a key to close", ConsoleColor.Violet);
                Console.ReadKey();
                Environment.Exit(e.HResult);
            }
            catch (XmlException e)
            {
                Logging.WriteLine(string.Format("Error parsing furnidata.xml: {0}", Environment.NewLine + e),
                    ConsoleColor.Red);
                Logging.WriteLine("Type a key to close", ConsoleColor.Violet);
                Console.ReadKey();
                Environment.Exit(e.HResult);
            }
            catch (NullReferenceException e)
            {
                Logging.WriteLine(string.Format("Error parsing value null of furnidata.xml: {0}", Environment.NewLine + e), ConsoleColor.Red);
                Logging.WriteLine("Type a key to close", ConsoleColor.Violet);
                Console.ReadKey();
                Environment.Exit(e.HResult);
            }
            wC.Dispose();
            xmlParser = null;
        }

        public static void Clear()
        {
            FloorItems.Clear();
            WallItems.Clear();
            FloorItems = null;
            WallItems = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}