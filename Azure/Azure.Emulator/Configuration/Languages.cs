﻿using System;
using System.Collections.Specialized;
using System.Data;

namespace Azure.Configuration
{
    class Languages
    {
        internal HybridDictionary Texts;

        internal Languages(string language)
        {
            Texts = new HybridDictionary();
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT * FROM server_langs WHERE lang = '{0}' ORDER BY id DESC",
                    language));
                var table = queryReactor.GetTable();
                if (table == null)
                    return;
                foreach (DataRow dataRow in table.Rows)
                    try
                    {
                        var name = dataRow["name"].ToString();
                        var text = dataRow["text"].ToString();
                        Texts.Add(name, text);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("[Language] Exception: " + ex);
                    }
            }
        }

        internal string GetVar(string var)
        {
            if (!Texts.Contains(var))
                Console.WriteLine("[Language] Not Found: " + var);
            return Texts[var].ToString();
        }

        internal int Count() { return Texts.Count; }
    }
}