using System;

namespace Azure.Configuration
{
    class MissingLocaleException : Exception
    {
        public MissingLocaleException(string message) : base(message) { }
    }
}