﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Azure.Configuration
{
    class ConfigurationData
    {
        internal Dictionary<string, string> Data;
        internal bool FileHasBeenRead;
        internal ConfigurationData(string filePath, bool mayNotExist = false)
        {
            Data = new Dictionary<string, string>();
            if (File.Exists(filePath))
            {
                FileHasBeenRead = true;
                try
                {
                    using (var streamReader = new StreamReader(filePath))
                    {
                        string text;
                        while ((text = streamReader.ReadLine()) != null)
                        {
                            if (text.Length < 1 || text.StartsWith("#"))
                                continue;
                            var num = text.IndexOf('=');
                            if (num == -1)
                                continue;
                            var key = text.Substring(0, num);
                            var value = text.Substring(checked(num + 1));
                            Data.Add(key, value);
                        }
                        streamReader.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw new ArgumentException(string.Format("Could not process configuration file: {0}", ex.Message));
                }
                return;
            }
            if (!mayNotExist)
                throw new ArgumentException(string.Format("No se encontró el archivo de config. en '{0}'.", filePath));
        }
    }
}