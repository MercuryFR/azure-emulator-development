﻿using System;
using Azure.HabboHotel;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.Configuration
{
    class ConsoleCommandHandling
    {
        internal static bool IsWaiting = false;
        internal static Game GetGame() { return Azure.GetGame(); }
        internal static void InvokeCommand(string inputData)
        {
            if (string.IsNullOrEmpty(inputData) && Logging.DisabledState)
                return;
            Console.WriteLine();
            try
            {
                var strArray = inputData.Split(new[] { ' ' });
                switch (strArray[0])
                {
                    case "pakets":
                    case "pacotes":
                    case "reloadpackets":
                    case "packets":
                    case "renewpackets":
                        LibraryParser.ReloadDictionarys();
                        LibraryParser.RegisterLibrary();
                        LibraryParser.RegisterConfig();
                        LibraryParser.RegisterIncoming();
                        LibraryParser.RegisterOutgoing();
                        Console.WriteLine("> Packets Reloaded Suceffuly...");
                        Console.WriteLine();
                        break;
                    case "shutdown":
                    case "apagar":
                    case "desligar":
                        Logging.LogMessage(string.Format("≫ Server shutting down at {0}", DateTime.Now));
                        Logging.DisablePrimaryWriting(true);
                        Logging.WriteLine("≫ Shutdown Initalized", ConsoleColor.Yellow);
                        Azure.PerformShutDown(false);
                        Console.WriteLine();
                        break;
                    case "restart":
                        Logging.LogMessage(string.Format("Server Restarting at {0}", DateTime.Now));
                        Logging.DisablePrimaryWriting(true);
                        Logging.WriteLine("≫ Restart Initialized", ConsoleColor.Yellow);
                        Azure.PerformShutDown(true);
                        Console.WriteLine();
                        break;
                    case "flush":
                    case "atualizar":
                    case "refrescar":
                        if (strArray.Length >= 2)
                            break;
                        Console.WriteLine("Please specify parameter. Type 'help' to know more about Console Commands");
                        Console.WriteLine();
                        break;
                    case "alert":
                        {
                            var str = inputData.Substring(6);
                            var message = new ServerMessage(LibraryParser.OutgoingRequest("BroadcastNotifMessageComposer"));
                            message.AppendString(str);
                            message.AppendString("");
                            GetGame().GetClientManager().QueueBroadcaseMessage(message);
                            Console.WriteLine("[{0}] was sent!", str);
                            break;
                        }

                    case "help":
                        Console.WriteLine("shutdown )/ apagar - for shutting down AzureEmulator");
                        Console.WriteLine("packets - reload packets ids");
                        Console.WriteLine("alert");
                        Console.WriteLine("flush / refrescar");
                        Console.WriteLine("      settings");
                        Console.WriteLine("           catalog - re-load Catalogue");
                        Console.WriteLine("alert (msg) - send alert to Every1!");
                        Console.WriteLine();
                        break;
                    default:
                        UnknownCommand(inputData);
                        break;
                }
                switch (strArray[1])
                {
                    case "database":
                        Azure.GetDatabaseManager().Destroy();
                        Console.WriteLine("Database destroyed");
                        Console.WriteLine();
                        break;
                    case "console":
                    case "consola":
                        Console.Clear();
                        Console.WriteLine();
                        break;
                    default:
                        UnknownCommand(inputData);
                        Console.WriteLine();
                        break;
                }
                switch (strArray[2])
                {
                    case "catalog":
                    case "shop":
                    case "catalogo":
                        FurniDataParser.SetCache();
                        using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
                            GetGame().GetCatalog().Initialize(adapter);
                        FurniDataParser.Clear();

                        GetGame()
                            .GetClientManager()
                            .QueueBroadcaseMessage(
                                new ServerMessage(LibraryParser.OutgoingRequest("PublishShopMessageComposer")));
                        Console.WriteLine("Catalogue was re-loaded.");
                        Console.WriteLine();
                        break;
                    case "modeldata":
                        using (var adapter2 = Azure.GetDatabaseManager().GetQueryReactor())
                            GetGame().GetRoomManager().LoadModels(adapter2);
                        Console.WriteLine("Room models were re-loaded.");
                        Console.WriteLine();
                        break;
                    case "bans":
                        using (var adapter3 = Azure.GetDatabaseManager().GetQueryReactor())
                            GetGame().GetBanManager().LoadBans(adapter3);
                        Console.WriteLine("Bans were re-loaded");
                        Console.WriteLine();
                        break;
                    default:
                        Console.WriteLine();
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private static void UnknownCommand(string command) { }
    }
}