using System.Collections.Generic;
using System.Data;
using Azure.Database.Manager.Session_Details.Interfaces;

namespace Azure.Configuration
{
    class ConfigData
    {
        internal Dictionary<string, string> DbData;
        internal ConfigData(IRegularQueryAdapter dbClient)
        {
            try
            {
                DbData = new Dictionary<string, string>();
                DbData.Clear();
                dbClient.SetQuery("SELECT * FROM server_settings");
                var table = dbClient.GetTable();
                foreach (DataRow dataRow in table.Rows)
                    DbData.Add(dataRow[0].ToString(), dataRow[1].ToString());
            }
            catch
            {
            }

        }


    }
}