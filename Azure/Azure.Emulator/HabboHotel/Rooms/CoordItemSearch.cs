using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms
{
    internal class CoordItemSearch
    {
        private readonly HybridDictionary _items;

        public CoordItemSearch(HybridDictionary itemArray)
        {
            this._items = itemArray;
        }

        internal List<RoomItem> GetRoomItemForSquare(int pX, int pY, double minZ)
        {
            var list = new List<RoomItem>();
            var point = new Point(pX, pY);
            if (!this._items.Contains(point))
            {
                return list;
            }
            var list2 = (List<RoomItem>)this._items[point];
            list.AddRange(list2.Where(current => current.Z > minZ && current.X == pX && current.Y == pY));
            return list;
        }

        internal List<RoomItem> GetRoomItemForSquare(int pX, int pY)
        {
            var point = new Point(pX, pY);
            var list = new List<RoomItem>();
            if (!this._items.Contains(point))
            {
                return list;
            }
            var list2 = (List<RoomItem>)this._items[point];
            list.AddRange(list2.Where(current => current.Coordinate.X == point.X && current.Coordinate.Y == point.Y));
            return list;
        }

        internal List<RoomItem> GetAllRoomItemForSquare(int pX, int pY)
        {
            var point = new Point(pX, pY);
            var list = new List<RoomItem>();
            if (!this._items.Contains(point))
            {
                return list;
            }
            var list2 = (List<RoomItem>)this._items[point];
            foreach (RoomItem current in list2.Where(current => !list.Contains(current)))
            {
                list.Add(current);
            }
            return list;
        }
    }
}