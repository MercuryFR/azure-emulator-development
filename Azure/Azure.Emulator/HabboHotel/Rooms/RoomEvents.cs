using System;
using System.Collections.Generic;
using System.Data;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.GameClients;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Rooms
{
    internal class RoomEvents
    {
        private readonly Dictionary<uint, RoomEvent> _events;

        internal RoomEvents()
        {
            this._events = new Dictionary<uint, RoomEvent>();
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT * FROM rooms_events WHERE `expire` > UNIX_TIMESTAMP()");
                DataTable table = queryReactor.GetTable();
                foreach (DataRow dataRow in table.Rows)
                {
                    this._events.Add((uint)dataRow[0],
                        new RoomEvent((uint)dataRow[0], dataRow[1].ToString(), dataRow[2].ToString(), (int)dataRow[3], (int)dataRow[4]));
                }
            }
        }

        internal void AddNewEvent(uint roomId, string eventName, string eventDesc, GameClient session, int time = 7200, int category = 1)
        {
            checked
            {
                if (this._events.ContainsKey(roomId))
                {
                    RoomEvent roomEvent = this._events[roomId];
                    roomEvent.Name = eventName;
                    roomEvent.Description = eventDesc;
                    if (roomEvent.HasExpired)
                    {
                        roomEvent.Time = Azure.GetUnixTimestamp() + time;
                    }
                    else
                    {
                        roomEvent.Time += time;
                    }
                    using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                    {
                        queryReactor.SetQuery(string.Concat(new object[]
                        {
                            "REPLACE INTO rooms_events VALUES (",
                            roomId,
                            ", @name, @desc, ",
                            roomEvent.Time,
                            ", @category)"
                        }));
                        queryReactor.AddParameter("name", eventName);
                        queryReactor.AddParameter("desc", eventDesc);
                        queryReactor.AddParameter("category", category);
                        queryReactor.RunQuery();
                        goto IL_17C;
                    }
                }
                using (IQueryAdapter queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryreactor2.SetQuery(string.Concat(new object[]
                    {
                        "REPLACE INTO rooms_events VALUES (",
                        roomId,
                        ", @name, @desc, ",
                        Azure.GetUnixTimestamp() + 7200,
                        ", @category)"
                    }));
                    queryreactor2.AddParameter("name", eventName);
                    queryreactor2.AddParameter("desc", eventDesc);
                    queryreactor2.AddParameter("category", category);
                    queryreactor2.RunQuery();
                }
                this._events.Add(roomId, new RoomEvent(roomId, eventName, eventDesc, 0));
                IL_17C:
                Azure.GetGame().GetRoomManager().GenerateRoomData(roomId).Event = this._events[roomId];
                Room room = Azure.GetGame().GetRoomManager().GetRoom(roomId);
                if (room != null)
                {
                    room.Event = this._events[roomId];
                }
                if (session.GetHabbo().CurrentRoomId == roomId)
                {
                    this.SerializeEventInfo(roomId);
                }
            }
        }

        internal void RemoveEvent(uint roomId)
        {
            this._events.Remove(roomId);
            this.SerializeEventInfo(roomId);
        }

        internal Dictionary<uint, RoomEvent> GetEvents()
        {
            return this._events;
        }

        internal RoomEvent GetEvent(uint roomId)
        {
            return this._events.ContainsKey(roomId) ? this._events[roomId] : null;
        }

        internal bool RoomHasEvents(uint roomId)
        {
            return this._events.ContainsKey(roomId);
        }

        internal void SerializeEventInfo(uint roomId)
        {
            Room room = Azure.GetGame().GetRoomManager().GetRoom(roomId);
            if (room == null)
            {
                return;
            }
            RoomEvent @event = this.GetEvent(roomId);
            if (@event == null || @event.HasExpired)
            {
                return;
            }
            if (!this.RoomHasEvents(roomId))
            {
                return;
            }
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("RoomEventMessageComposer"));
            serverMessage.AppendInteger(roomId);
            serverMessage.AppendInteger(room.OwnerId);
            serverMessage.AppendString(room.Owner);
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(1);
            serverMessage.AppendString(@event.Name);
            serverMessage.AppendString(@event.Description);
            serverMessage.AppendInteger(0);
            serverMessage.AppendInteger(
                checked((int)Math.Floor((@event.Time - Azure.GetUnixTimestamp()) / 60.0)));
            serverMessage.AppendInteger(@event.Category);
            room.SendMessage(serverMessage);
        }

        internal void UpdateEvent(RoomEvent Event)
        {
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Concat(new object[]
                {
                    "REPLACE INTO rooms_events VALUES (",
                    Event.RoomId,
                    ", @name, @desc, ",
                    Event.Time,
                    ")"
                }));
                queryReactor.AddParameter("name", Event.Name);
                queryReactor.AddParameter("desc", Event.Description);
                queryReactor.RunQuery();
            }
            this.SerializeEventInfo(Event.RoomId);
        }
    }
}