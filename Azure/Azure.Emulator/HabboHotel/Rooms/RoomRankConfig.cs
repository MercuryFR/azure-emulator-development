﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Azure.HabboHotel.Rooms
{
    internal class RoomRankConfig
    {
        internal List<int> ROOMS_TO_MODIFY;
        internal int BOTS_DEFAULT_COLOR;
        internal String BOTS_DEFAULT_BADGE;

        internal void Initialize()
        {
            this.ROOMS_TO_MODIFY = new List<int>();

            string roomWithsColors = Azure.GetConfig().Data["game.roomswithbotscolor"];
            if (!roomWithsColors.Equals("") && roomWithsColors.Contains(","))
            {
                string[] v = roomWithsColors.Split(',');
                for (int i = 0; i < v.Length; i++)
                {
                    this.ROOMS_TO_MODIFY.Add(Int32.Parse(v[i]));
                }
            }
            else
            {
                this.ROOMS_TO_MODIFY.Add(Int32.Parse(roomWithsColors));
            }

            this.BOTS_DEFAULT_COLOR = Int32.Parse(Azure.GetConfig().Data["game.botdefaultcolor"]);
            this.BOTS_DEFAULT_BADGE = Azure.GetConfig().Data["game.botbadge"];
        }
    }
}