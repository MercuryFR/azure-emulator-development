﻿using System;
using System.Collections.Generic;
using Azure.HabboHotel.Items;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Effects
{
    public class ResetPosition : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.EffectResetPosition;
        private readonly Room _mRoom;
        private readonly List<WiredItemType> _mBanned;

        public ResetPosition(RoomItem item, Room room)
        {
            this.Item = item;
            this._mRoom = room;
            this.OtherString = "";
            this.OtherExtraString = "";
            this.OtherExtraString2 = "";
            this.Delay = 0;
            this.Items = new List<RoomItem>();
            this._mBanned = new List<WiredItemType>();
        }

        public WiredItemType Type
        {
            get
            {
                return MType;
            }
        }

        public RoomItem Item { get; set; }

        public Room Room
        {
            get
            {
                return this._mRoom;
            }
        }

        public List<RoomItem> Items { get; set; }

        public int Delay { get; set; }

        public string OtherString { get; set; }

        public string OtherExtraString { get; set; }

        public string OtherExtraString2 { get; set; }

        public bool OtherBool { get; set; }

        public bool Execute(params object[] stuff)
        {
            var roomUser = (RoomUser)stuff[0];
            var item = (WiredItemType)stuff[1];

            if (this._mBanned.Contains(item))
            {
                return false;
            }
            if (roomUser == null || roomUser.GetClient() == null || roomUser.GetClient().GetHabbo() == null)
            {
                return false;
            }

            Room uRoom = roomUser.GetClient().GetHabbo().CurrentRoom;

            if (uRoom == null)
            {
                return false;
            }

            if (String.IsNullOrWhiteSpace(this.OtherString) || String.IsNullOrWhiteSpace(this.OtherExtraString))
            {
                return false;
            }

            string[] booleans = this.OtherString.Split(',');

            if (booleans.Length < 3)
            {
                return false;
            }

            bool extraData = booleans[0] == "true";
            bool rot = booleans[1] == "true";
            bool position = booleans[2] == "true";

            foreach (string itemData in this.OtherExtraString.Split('/'))
            {
                if (String.IsNullOrWhiteSpace(itemData))
                {
                    continue;
                }

                string[] innerData = itemData.Split('|');
                uint itemId = uint.Parse(innerData[0]);

                RoomItem fItem = uRoom.GetRoomItemHandler().GetItem(itemId);

                if (fItem == null)
                {
                    continue;
                }

                string extraDataToSet = (extraData) ? innerData[1] : fItem.ExtraData;
                int rotationToSet = (rot) ? int.Parse(innerData[2]) : fItem.Rot;

                string[] positions = innerData[3].Split(',');
                int xToSet = (position) ? int.Parse(positions[0]) : fItem.X;
                int yToSet = (position) ? int.Parse(positions[1]) : fItem.Y;
                double zToSet = (position) ? double.Parse(positions[2]) : fItem.Z;

                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ItemAnimationMessageComposer"));
                serverMessage.AppendInteger(fItem.X);
                serverMessage.AppendInteger(fItem.Y);
                serverMessage.AppendInteger(xToSet);
                serverMessage.AppendInteger(yToSet);
                serverMessage.AppendInteger(1);
                serverMessage.AppendInteger(fItem.Id);
                serverMessage.AppendString(fItem.Z.ToString(Azure.CultureInfo));
                serverMessage.AppendString(zToSet.ToString(Azure.CultureInfo));
                serverMessage.AppendInteger(0);
                uRoom.SendMessage(serverMessage);

                uRoom.GetRoomItemHandler()
                     .SetFloorItem(roomUser.GetClient(), fItem, xToSet, yToSet, rotationToSet, false, false, false, false);
                fItem.ExtraData = extraDataToSet;
                fItem.UpdateState();
            }
            return true;
        }
    }
}