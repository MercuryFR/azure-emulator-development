using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Effects
{
    internal class TeleportToFurni : IWiredItem, IWiredCycler
    {
        private const WiredItemType MType = WiredItemType.EffectTeleportToFurni;
        private readonly Room _mRoom;
        private readonly List<WiredItemType> _mBanned;

        private long _mNext;

        public TeleportToFurni(RoomItem item, Room room)
        {
            this.Item = item;
            this._mRoom = room;
            this.ToWork = new Queue();
            this.Items = new List<RoomItem>();
            this.Delay = 0;
            this._mNext = 0L;
            this._mBanned = new List<WiredItemType>
            {
                WiredItemType.TriggerRepeatEffect
            };
        }

        public WiredItemType Type
        {
            get
            {
                return MType;
            }
        }

        public RoomItem Item { get; set; }

        public Room Room
        {
            get
            {
                return this._mRoom;
            }
        }

        public List<RoomItem> Items { get; set; }

        public string OtherString
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public string OtherExtraString
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public string OtherExtraString2
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public bool OtherBool
        {
            get
            {
                return true;
            }
            set
            {
            }
        }

        public int Delay { get; set; }

        public Queue ToWork { get; set; }

        public bool Execute(params object[] stuff)
        {
            var roomUser = (RoomUser)stuff[0];
            var item = (WiredItemType)stuff[1];
            if (this._mBanned.Contains(item))
            {
                return false;
            }
            if (roomUser == null || !this.Items.Any() || this._mRoom.GetWiredHandler().IsCycleQueued(this))
            {
                return false;
            }
            if (this.Delay < 500)
            {
                this.Delay = 500;
            }
            if (this._mNext == 0L || this._mNext < Azure.Now())
            {
                this._mNext = checked(Azure.Now() + unchecked(this.Delay));
            }
            lock (this.ToWork.SyncRoot)
            {
                if (!this.ToWork.Contains(roomUser))
                {
                    this.ToWork.Enqueue(roomUser);
                }
            }
            this._mRoom.GetWiredHandler().EnqueueCycle(this);
            return true;
        }

        public bool OnCycle()
        {
            if (this.ToWork.Count == 0)
            {
                return true;
            }
            if (this.Room == null || this.Room.GetRoomItemHandler() == null || this.Room.GetRoomItemHandler().MFloorItems == null)
            {
                return false;
            }

            var queue = new Queue();
            long num = Azure.Now();
            lock (this.ToWork.SyncRoot)
            {
                while (this.ToWork.Count > 0)
                {
                    var roomUser = (RoomUser)this.ToWork.Dequeue();
                    if (roomUser == null || roomUser.GetClient() == null)
                    {
                        continue;
                    }
                    if (this._mNext < num)
                    {
                        bool flag2 = this.Teleport(roomUser);
                        if (!flag2)
                        {
                            continue;
                        }
                        return false;
                    }
                    if (checked(this._mNext - num) < 500L)
                    {
                        roomUser.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(4);
                        queue.Enqueue(roomUser);
                    }
                    else
                    {
                        queue.Enqueue(roomUser);
                    }
                }
            }
            if (this._mNext < num)
            {
                this._mNext = 0L;
                return true;
            }
            this.ToWork = queue;
            return false;
        }

        private bool Teleport(RoomUser user)
        {
            if (!this.Items.Any())
            {
                return true;
            }
            if (user == null || user.GetClient() == null || user.GetClient().GetHabbo() == null)
            {
                return true;
            }
            var rnd = new Random();
            this.Items = (
                          from x in this.Items
                          orderby rnd.Next()
                          select x).ToList<RoomItem>();
            RoomItem roomItem = null;
            foreach (
                RoomItem current in
                this.Items.Where(
                    current => current != null && this.Room.GetRoomItemHandler().MFloorItems.ContainsKey(current.Id)))
            {
                roomItem = current;
            }
            if (roomItem == null)
            {
                user.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(0);
                return false;
            }
            this._mRoom.GetGameMap().TeleportToItem(user, roomItem);
            this._mRoom.GetRoomUserManager().UpdateUserStatusses();
            user.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(0);
            return true;
        }
    }
}