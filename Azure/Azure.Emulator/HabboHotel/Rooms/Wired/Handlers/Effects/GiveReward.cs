using System;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Items;
using Azure.Messages;
using Azure.Messages.Parsers;
using Azure.Util;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Effects
{
    public class GiveReward : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.EffectGiveReward;
        private readonly List<WiredItemType> _mBanned;

        public GiveReward(RoomItem item, Room room)
        {
            Item = item;
            Room = room;
            OtherString = string.Empty;
            OtherExtraString = string.Empty;
            OtherExtraString2 = string.Empty;
            _mBanned = new List<WiredItemType>();
        }

        public WiredItemType Type
        {
            get { return MType; }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; private set; }

        public List<RoomItem> Items
        {
            get { return new List<RoomItem>(); }
            set { }
        }

        public int Delay
        {
            get { return 0; }
            set { }
        }

        public string OtherString { get; set; }

        public string OtherExtraString { get; set; }

        public string OtherExtraString2 { get; set; }

        public bool OtherBool { get; set; }

        public bool Execute(params object[] stuff)
        {
            var roomUser = (RoomUser) stuff[0];
            var item = (WiredItemType) stuff[1];
            if (_mBanned.Contains(item))
                return false;

            if (roomUser == null)
                return false;
            var array = OtherString.Split(';');
            var flag = true;

            if (!OtherBool)
            {
                var array2 = array;
                var i = 0;
                while (i < array2.Length)
                {
                    var text = array2[i];
                    var array3 = text.Split(',');
                    var flag2 = false;
                    var product = "";
                    var num = 0;
                    try
                    {
                        flag2 = int.Parse(array3[0]) == 0;
                        product = array3[1];
                        num = int.Parse(array3[2]);
                    }
                    catch
                    {
                    }
                    if (RandomNumber.Get(0, 101) <= num)
                    {
                        if (flag2)
                        {
                            roomUser.GetClient()
                                .GetHabbo()
                                .GetBadgeComponent()
                                .GiveBadge(product, true, roomUser.GetClient(), true);
                            break;
                        }
                        if (product.StartsWith("avatar_effect"))
                        {
                            var s = product.Trim().Replace("avatar_effect", "");
                            var effectId = int.Parse(s);
                            roomUser.GetClient()
                                .GetHabbo()
                                .GetAvatarEffectsInventoryComponent()
                                .AddNewEffect(effectId, 68400);
                            var serverMessage =
                                new ServerMessage(LibraryParser.OutgoingRequest("WiredRewardAlertMessageComposer"));
                            serverMessage.AppendInteger(6);
                            roomUser.GetClient().SendMessage(serverMessage);
                            break;
                        }
                        Item item2 = null;
                        try
                        {
                            item2 = (
                                from x in Azure.GetGame().GetItemManager().Items.Values
                                where x.Name == product
                                select x).FirstOrDefault<Item>();
                        }
                        catch (Exception)
                        {
                            flag = false;
                        }
                        if (item2 == null)
                        {
                            flag = false;
                            break;
                        }
                        roomUser.GetClient()
                            .GetHabbo()
                            .GetInventoryComponent()
                            .AddNewItem(0u, item2.ItemId, "0", 0u, true, false, 0, 0, "");
                        var serverMessage2 =
                            new ServerMessage(LibraryParser.OutgoingRequest("WiredRewardAlertMessageComposer"));
                        serverMessage2.AppendInteger(6);
                        roomUser.GetClient().SendMessage(serverMessage2);
                        roomUser.GetClient()
                            .SendMessage(
                                new ServerMessage(LibraryParser.OutgoingRequest("UpdateInventoryMessageComposer")));
                        break;
                    }
                    flag = false;
                    i++;
                }
            }
            else
            {
                var array4 = array;
                foreach (string text2 in array4)
                {
                    var array5 = text2.Split(',');
                    var flag3 = int.Parse(array5[0]) == 0;
                    var product = array5[1];
                    int.Parse(array5[2]);
                    if (flag3)
                        roomUser.GetClient()
                            .GetHabbo()
                            .GetBadgeComponent()
                            .GiveBadge(product, true, roomUser.GetClient(), true);
                    else if (product.StartsWith("avatar_effect"))
                    {
                        var s2 = product.Trim().Replace("avatar_effect", "");
                        var effectId2 = int.Parse(s2);
                        roomUser.GetClient()
                            .GetHabbo()
                            .GetAvatarEffectsInventoryComponent()
                            .AddNewEffect(effectId2, 68400);
                        var serverMessage3 =
                            new ServerMessage(LibraryParser.OutgoingRequest("WiredRewardAlertMessageComposer"));
                        serverMessage3.AppendInteger(6);
                        roomUser.GetClient().SendMessage(serverMessage3);
                    }
                    else
                    {
                        Item item3 = null;
                        try
                        {
                            item3 = (
                                from x in Azure.GetGame().GetItemManager().Items.Values
                                where x.Name == product
                                select x).FirstOrDefault<Item>();
                        }
                        catch (Exception)
                        {
                            flag = false;
                        }
                        if (item3 == null)
                            flag = false;
                        else
                        {
                            roomUser.GetClient()
                                .GetHabbo()
                                .GetInventoryComponent()
                                .AddNewItem(0u, item3.ItemId, "0", 0u, true, false, 0, 0, "");
                            var serverMessage4 =
                                new ServerMessage(LibraryParser.OutgoingRequest("WiredRewardAlertMessageComposer"));
                            serverMessage4.AppendInteger(6);
                            roomUser.GetClient().SendMessage(serverMessage4);
                            roomUser.GetClient()
                                .SendMessage(
                                    new ServerMessage(LibraryParser.OutgoingRequest("UpdateInventoryMessageComposer")));
                        }
                    }
                }
            }
            if (flag)
                return true;
            var serverMessage5 = new ServerMessage(LibraryParser.OutgoingRequest("WiredRewardAlertMessageComposer"));
            serverMessage5.AppendInteger(4);
            roomUser.GetClient().SendMessage(serverMessage5);
            return true;
        }
    }
}