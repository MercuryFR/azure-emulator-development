using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Effects
{
    public class ToggleFurniState : IWiredItem, IWiredCycler
    {
        private const WiredItemType MType = WiredItemType.EffectToggleFurniState;
        private readonly Room _mRoom;
        private long _mNext;

        public ToggleFurniState(RoomItem item, Room Room)
        {
            this.Item = item;
            this._mRoom = Room;
            this.Items = new List<RoomItem>();
            this.Delay = 0;
            this._mNext = 0L;
        }

        public WiredItemType Type
        {
            get
            {
                return MType;
            }
        }

        public RoomItem Item { get; set; }

        public Room Room
        {
            get
            {
                return this._mRoom;
            }
        }

        public List<RoomItem> Items { get; set; }

        public string OtherString
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public string OtherExtraString
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public string OtherExtraString2
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public bool OtherBool
        {
            get
            {
                return true;
            }
            set
            {
            }
        }

        public int Delay { get; set; }

        public Queue ToWork
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public bool Execute(params object[] stuff)
        {
            var roomUser = (RoomUser)stuff[0];
            if (roomUser == null || !this.Items.Any())
            {
                return false;
            }
            if (this._mNext == 0L || this._mNext < Azure.Now())
            {
                this._mNext = checked(Azure.Now() + unchecked(this.Delay));
            }
            this._mRoom.GetWiredHandler().EnqueueCycle(this);
            return true;
        }

        public bool OnCycle()
        {
            if (!this.Items.Any())
            {
                return true;
            }

            long num = Azure.Now();
            if (this._mNext < num)
            {
                foreach (
                    RoomItem current in
                    this.Items.Where(
                        current => current != null && this.Room.GetRoomItemHandler().MFloorItems.ContainsKey(current.Id))
                    )
                {
                    current.Interactor.OnWiredTrigger(current);
                }
            }
            if (this._mNext >= num)
            {
                return false;
            }
            this._mNext = 0L;
            return true;
        }
    }
}