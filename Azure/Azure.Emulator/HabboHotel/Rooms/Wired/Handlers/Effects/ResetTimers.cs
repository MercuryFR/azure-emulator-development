﻿using System.Collections.Generic;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Effects
{
    public class ResetTimers : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.EffectResetTimers;
        private readonly Room _mRoom;

        //private List<WiredItemType> mBanned;
        public ResetTimers(RoomItem item, Room room)
        {
            this.Item = item;
            this._mRoom = room;
            this.OtherString = "";
            this.OtherExtraString = "";
            this.OtherExtraString2 = "";
            //this.mBanned = new List<WiredItemType>();
        }

        public WiredItemType Type
        {
            get
            {
                return MType;
            }
        }

        public RoomItem Item { get; set; }

        public Room Room
        {
            get
            {
                return this._mRoom;
            }
        }

        public List<RoomItem> Items
        {
            get
            {
                return new List<RoomItem>();
            }
            set
            {
            }
        }

        public int Delay
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }

        public string OtherString { get; set; }

        public string OtherExtraString { get; set; }

        public string OtherExtraString2 { get; set; }

        public bool OtherBool { get; set; }

        public bool Execute(params object[] stuff)
        {
            //var roomUser = (RoomUser)stuff[0];
            //var item = (WiredItemType)stuff[1];
            foreach (RoomItem aitem in this.Items)
            {
                switch (aitem.GetBaseItem().InteractionType)
                {
                    case InteractionType.triggerrepeater:
                    case InteractionType.triggertimer:
                        IWiredItem trigger = this.Room.GetWiredHandler().GetWired(aitem);

                        trigger.Delay = 5000;
                        this.Room.GetWiredHandler().ReloadWired(trigger);
                        break;
                }
            }

            return true;
        }
    }
}