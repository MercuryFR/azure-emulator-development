using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Effects
{
    class MoveRotateFurni : IWiredItem, IWiredCycler
    {
        private const WiredItemType MType = WiredItemType.EffectMoveRotateFurni;
        private long _mNext;
        private int _mRot;
        private int _mDir;

        public MoveRotateFurni(RoomItem item, Room room)
        {
            Item = item;
            Room = room;
            Items = new List<RoomItem>();
            Delay = 0;
            _mNext = 0L;
            _mRot = 0;
            _mDir = 0;
        }

        public WiredItemType Type
        {
            get { return MType; }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; private set; }

        public List<RoomItem> Items { get; set; }

        readonly ConcurrentQueue<RoomItem> _toRemove = new ConcurrentQueue<RoomItem>();

        public string OtherString
        {
            get { return string.Format("{0};{1}", _mRot, _mDir); }
            set
            {
                _mRot = int.Parse(value.Split(';')[0]);
                _mDir = int.Parse(value.Split(';')[1]);
            }
        }

        public string OtherExtraString
        {
            get { return ""; }
            set { }
        }

        public string OtherExtraString2
        {
            get { return ""; }
            set { }
        }

        public bool OtherBool
        {
            get { return true; }
            set { }
        }

        public int Delay { get; set; }

        public Queue ToWork
        {
            get { return null; }
            set { }
        }

        public bool Execute(params object[] stuff)
        {
            if (!Items.Any())
                return true;
            checked
            {
                if (Delay > 0)
                {
                    if (_mNext == 0L || _mNext < Azure.Now())
                        _mNext = Azure.Now() + unchecked(Delay);
                    Room.GetWiredHandler().EnqueueCycle(this);
                }
                else
                {
                    _mNext = 0L;
                    if (OnCycle())
                        return true;
                    if (_mNext == 0L || _mNext < Azure.Now())
                        _mNext = Azure.Now() + unchecked(Delay);
                    Room.GetWiredHandler().EnqueueCycle(this);
                }
                return true;
            }
        }

        public bool OnCycle()
        {
            var num = Azure.Now();
            if (Room == null || Room.GetRoomItemHandler() == null || Room.GetRoomItemHandler().MFloorItems == null)
                return false;
            if (_mNext >= num)
                return false;

            foreach (var item in Items)
            {
                if (Room != null && Room.GetRoomItemHandler() != null &&
                    Room.GetRoomItemHandler().GetItem(item.Id) == null)
                {
                    _toRemove.Enqueue(item);
                    continue;
                }
                HandleMovement(item);
            }

            RoomItem rI;
            while (_toRemove.TryDequeue(out rI))
            {
                if (Items.Contains(rI))
                    Items.Remove(rI);
            }

            _mNext = 0L;
            return true;
        }

        private bool HandleMovement(RoomItem item)
        {
            var newPoint = Movement.HandleMovement(item.Coordinate, (MovementState)_mDir, item.Rot);
            var newRotation = Movement.HandleRotation(item.Rot, (RotationState)item.Rot);

            if (newPoint != item.Coordinate && newRotation == item.Rot)
            {
                if (Room.GetGameMap().SquareIsOpen(newPoint.X, newPoint.Y, false))
                    return Room.GetRoomItemHandler()
                        .SetFloorItem(null, item, newPoint.X, newPoint.Y, newRotation, false, false, true, true);
            }
            else if (newPoint != item.Coordinate || newRotation != item.Rot)
                if (Room.GetGameMap().SquareIsOpen(newPoint.X, newPoint.Y, false))
                    return Room.GetRoomItemHandler()
                        .SetFloorItem(null, item, newPoint.X, newPoint.Y, newRotation, false, false, true, false);

            return false;
        }
    }
}