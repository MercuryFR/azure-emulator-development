using System.Collections.Generic;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Effects
{
    public class ShowMessage : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.EffectShowMessage;
        private readonly Room _mRoom;
        private readonly List<WiredItemType> _mBanned;

        public ShowMessage(RoomItem item, Room room)
        {
            this.Item = item;
            this._mRoom = room;
            this.OtherString = "";
            this._mBanned = new List<WiredItemType>
            {
                WiredItemType.TriggerRepeatEffect
            };
        }

        public WiredItemType Type
        {
            get
            {
                return MType;
            }
        }

        public RoomItem Item { get; set; }

        public Room Room
        {
            get
            {
                return this._mRoom;
            }
        }

        public List<RoomItem> Items
        {
            get
            {
                return new List<RoomItem>();
            }
            set
            {
            }
        }

        public int Delay
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }

        public string OtherString { get; set; }

        public string OtherExtraString
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public string OtherExtraString2
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public bool OtherBool
        {
            get
            {
                return true;
            }
            set
            {
            }
        }

        public bool Execute(params object[] stuff)
        {
            var roomUser = (RoomUser)stuff[0];
            var item = (WiredItemType)stuff[1];
            if (this._mBanned.Contains(item))
            {
                return false;
            }
            if (roomUser != null && !string.IsNullOrEmpty(this.OtherString))
            {
                roomUser.GetClient().SendWhisper(this.OtherString);
            }
            return true;
        }
    }
}