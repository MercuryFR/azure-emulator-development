﻿using System;
using System.Collections.Generic;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Conditions
{
    class HowManyUsers : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.ConditionHowManyUsers;

        public HowManyUsers(RoomItem item, Room room)
        {
            Item = item;
            Room = room;
            Items = new List<RoomItem>();
            OtherString = "";
        }

        public WiredItemType Type
        {
            get { return MType; }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; private set; }

        public List<RoomItem> Items { get; set; }

        public string OtherString { get; set; }

        public string OtherExtraString
        {
            get { return ""; }
            set { }
        }

        public string OtherExtraString2
        {
            get { return ""; }
            set { }
        }

        public bool OtherBool
        {
            get { return true; }
            set { }
        }

        public int Delay
        {
            get { return 0; }
            set { }
        }

        public bool Execute(params object[] stuff)
        {
            var approved = false;

            var minimum = 1;
            var maximum = 50;

            if (!String.IsNullOrWhiteSpace(OtherString))
            {
                var integers = OtherString.Split(',');
                minimum = int.Parse(integers[0]);
                maximum = int.Parse(integers[1]);
            }

            if (Room.UsersNow >= minimum && Room.UsersNow <= maximum)
                approved = true;

            return approved;
        }
    }
}