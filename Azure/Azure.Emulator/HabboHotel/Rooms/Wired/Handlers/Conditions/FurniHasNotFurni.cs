﻿using System;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Conditions
{
    class FurniHasNotFurni : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.ConditionFurniHasNotFurni;

        public FurniHasNotFurni(RoomItem item, Room room)
        {
            Item = item;
            Room = room;
            Items = new List<RoomItem>();
        }

        public WiredItemType Type
        {
            get { return MType; }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; private set; }

        public List<RoomItem> Items { get; set; }

        public string OtherString
        {
            get { return ""; }
            set { }
        }

        public string OtherExtraString
        {
            get { return ""; }
            set { }
        }

        public string OtherExtraString2
        {
            get { return ""; }
            set { }
        }

        public bool OtherBool
        {
            get { return true; }
            set { }
        }

        public int Delay
        {
            get { return 0; }
            set { }
        }

        public bool Execute(params object[] stuff)
        {
            if (!Items.Any())
                return true;

            foreach (var current in Items.Where(current => current != null && Room.GetRoomItemHandler().MFloorItems.ContainsKey(current.Id)))
            {
                if (
                    current.AffectedTiles.Values.Where(
                        current2 => Room.GetGameMap().GetRoomItemForSquare(current2.X, current2.Y).Any())
                        .Any(
                            current2 =>
                                Room.GetGameMap()
                                    .GetRoomItemForSquare(current2.X, current2.Y)
                                    .Any(current3 => current3.Id != current.Id && current3.Z >= current.Z)))
                    return false;
                if (!Room.GetGameMap().GetRoomItemForSquare(current.X, current.Y).Any())
                    continue;
                if (
                    Room.GetGameMap()
                        .GetRoomItemForSquare(current.X, current.Y)
                        .Any(current4 => current4.Id != current.Id && current4.Z >= current.Z))
                    return false;
            }

            return true;
        }
    }
}