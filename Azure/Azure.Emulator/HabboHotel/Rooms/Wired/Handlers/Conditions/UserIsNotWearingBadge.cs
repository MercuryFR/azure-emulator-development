﻿using System;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Users.Badges;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Conditions
{
    class UserIsNotWearingBadge : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.ConditionIsNotWearingBadge;

        public UserIsNotWearingBadge(RoomItem item, Room room)
        {
            Item = item;
            Room = room;
            Items = new List<RoomItem>();
            OtherString = "";
        }

        public WiredItemType Type
        {
            get { return MType; }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; private set; }

        public List<RoomItem> Items { get; set; }

        public string OtherString { get; set; }

        public string OtherExtraString
        {
            get { return ""; }
            set { }
        }

        public string OtherExtraString2
        {
            get { return ""; }
            set { }
        }

        public bool OtherBool
        {
            get { return true; }
            set { }
        }

        public int Delay
        {
            get { return 0; }
            set { }
        }

        public bool Execute(params object[] stuff)
        {
            if (stuff == null || !(stuff[0] is RoomUser))
                return false;
            var roomUser = (RoomUser) stuff[0];

            if (roomUser.IsBot || roomUser.GetClient() == null || string.IsNullOrWhiteSpace(OtherString))
                return false;

            return
                roomUser.GetClient()
                    .GetHabbo()
                    .GetBadgeComponent()
                    .BadgeList
                    .Values
                    .Cast<Badge>()
                    .All(badge => badge.Slot <= 0 || !String.Equals(badge.Code, OtherString, StringComparison.CurrentCultureIgnoreCase));
        }
    }
}