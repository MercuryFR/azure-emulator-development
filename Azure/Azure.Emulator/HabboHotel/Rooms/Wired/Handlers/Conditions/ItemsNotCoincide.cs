﻿using System;
using System.Collections.Generic;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Conditions
{
    class ItemsNotCoincide : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.ConditionItemsDontMatch;

        public ItemsNotCoincide(RoomItem item, Room room)
        {
            Item = item;
            Room = room;
            Items = new List<RoomItem>();
            OtherString = "";
            OtherExtraString = "";
            OtherExtraString2 = "";
        }

        public WiredItemType Type
        {
            get { return MType; }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; private set; }

        public List<RoomItem> Items { get; set; }

        public string OtherString { get; set; }

        public string OtherExtraString { get; set; }

        public string OtherExtraString2 { get; set; }

        public bool OtherBool
        {
            get { return true; }
            set { }
        }

        public int Delay
        {
            get { return 0; }
            set { }
        }

        public bool Execute(params object[] stuff)
        {
            if (String.IsNullOrWhiteSpace(OtherString))
                return false;

            var booleans = OtherString.ToLower().Split(',');
            var useExtradata = booleans[0] == "true";
            var useRot = booleans[1] == "true";
            var usePos = booleans[2] == "true";

            var edApproved = true;
            var rotApproved = true;
            var posApproved = true;

            RoomItem lastitem = null;

            foreach (var current in Items)
            {
                if (lastitem == null)
                {
                    lastitem = current;
                    continue;
                }

                if (useRot)
                    if (current.Rot != lastitem.Rot)
                        rotApproved = false;

                if (useExtradata)
                {
                    if (current.ExtraData == "")
                        current.ExtraData = "0";

                    if (lastitem.ExtraData == "")
                        lastitem.ExtraData = "0";

                    if (current.ExtraData != lastitem.ExtraData)
                        edApproved = false;
                }

                if (!usePos)
                    continue;
                if ((current.X != lastitem.Y) && (current.Y != lastitem.Y))
                    posApproved = false;
            }

            return !edApproved || !posApproved || !rotApproved;
        }
    }
}