﻿using System.Collections.Generic;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Conditions
{
    class DateRangeActive : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.ConditionDateRangeActive;
        private readonly Room _mRoom;

        public DateRangeActive(RoomItem item, Room room)
        {
            Item = item;
            _mRoom = room;
            Items = new List<RoomItem>();
            OtherString = "";
        }

        public WiredItemType Type
        {
            get { return MType; }
        }

        public RoomItem Item { get; set; }

        public Room Room
        {
            get { return _mRoom; }
        }

        public List<RoomItem> Items { get; set; }

        public string OtherString { get; set; }

        public string OtherExtraString
        {
            get { return ""; }
            set { }
        }

        public string OtherExtraString2
        {
            get { return ""; }
            set { }
        }

        public bool OtherBool
        {
            get { return true; }
            set { }
        }

        public int Delay
        {
            get { return 0; }
            set { }
        }

        public bool Execute(params object[] stuff)
        {
            int date1;
            var date2 = 0;

            var strArray = OtherString.Split(',');

            if (string.IsNullOrWhiteSpace(strArray[0]))
                return false;

            int.TryParse(strArray[0], out date1);

            if (strArray.Length > 1)
                int.TryParse(strArray[1], out date2);

            if (date1 == 0)
                return false;

            var currentTimestamp = Azure.GetUnixTimestamp();

            return date2 < 1 ? currentTimestamp >= date1 : currentTimestamp >= date1 && currentTimestamp <= date2;
        }
    }
}