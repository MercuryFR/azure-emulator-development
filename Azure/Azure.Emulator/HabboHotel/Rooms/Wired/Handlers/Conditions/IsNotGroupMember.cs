﻿using System.Collections.Generic;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Conditions
{
    class IsNotGroupMember : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.ConditionIsNotGroupMember;

        public IsNotGroupMember(RoomItem item, Room room)
        {
            Item = item;
            Room = room;
            Items = new List<RoomItem>();
        }

        public WiredItemType Type
        {
            get { return MType; }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; private set; }

        public List<RoomItem> Items { get; set; }

        public string OtherString
        {
            get { return ""; }
            set { }
        }

        public string OtherExtraString
        {
            get { return ""; }
            set { }
        }

        public string OtherExtraString2
        {
            get { return ""; }
            set { }
        }

        public bool OtherBool
        {
            get { return true; }
            set { }
        }

        public int Delay
        {
            get { return 0; }
            set { }
        }

        public bool Execute(params object[] stuff)
        {
            if (stuff == null || !(stuff[0] is RoomUser))
                return false;

            var roomUser = (RoomUser) stuff[0];
            if (roomUser == null)
                return false;

            if (Room.Group == null)
                return false;

            return !Room.Group.Members.ContainsKey(roomUser.GetClient().GetHabbo().Id);
        }
    }
}