using System;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Conditions
{
    class FurniHasUsers : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.ConditionFurniHasUsers;

        public FurniHasUsers(RoomItem item, Room room)
        {
            Item = item;
            Room = room;
            Items = new List<RoomItem>();
        }

        public WiredItemType Type
        {
            get { return MType; }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; private set; }

        public List<RoomItem> Items { get; set; }

        public string OtherString
        {
            get { return ""; }
            set { }
        }

        public string OtherExtraString
        {
            get { return ""; }
            set { }
        }

        public string OtherExtraString2
        {
            get { return ""; }
            set { }
        }

        public bool OtherBool
        {
            get { return true; }
            set { }
        }

        public int Delay
        {
            get { return 0; }
            set { }
        }

        public bool Execute(params object[] stuff)
        {
            return !Items.Any() ||
                   Items.Where(
                       current => current != null && Room.GetRoomItemHandler().MFloorItems.ContainsKey(current.Id))
                       .Where(
                           current =>
                               !current.AffectedTiles.Values.Any(
                                   current2 => Room.GetGameMap().SquareHasUsers(current2.X, current2.Y)))
                       .All(current => Room.GetGameMap().SquareHasUsers(current.X, current.Y));
        }
    }
}