using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Triggers
{
    internal class Repeater : IWiredItem, IWiredCycler
    {
        private const WiredItemType MType = WiredItemType.TriggerRepeatEffect;
        private long _mNext;

        public Repeater(RoomItem item, Room room)
        {
            this.Item = item;
            this.Room = room;
            this.Delay = 5000;
            this.Room.GetWiredHandler().EnqueueCycle(this);
            if (this._mNext == 0L || this._mNext < Azure.Now())
            {
                this._mNext = checked(Azure.Now() + unchecked(this.Delay));
            }
        }

        public WiredItemType Type
        {
            get
            {
                return MType;
            }
        }

        public RoomItem Item { get; set; }

        public Room Room { get; private set; }

        public List<RoomItem> Items
        {
            get
            {
                return new List<RoomItem>();
            }
            set
            {
            }
        }

        public int Delay { get; set; }

        public string OtherString
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public string OtherExtraString
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public string OtherExtraString2
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public bool OtherBool
        {
            get
            {
                return true;
            }
            set
            {
            }
        }

        public Queue ToWork
        {
            get
            {
                return new Queue();
            }
            set
            {
            }
        }

        public bool Execute(params object[] stuff)
        {
            if (this._mNext == 0L || this._mNext < Azure.Now())
            {
                this._mNext = checked(Azure.Now() + unchecked(this.Delay));
            }
            if (!this.Room.GetWiredHandler().IsCycleQueued(this))
            {
                this.Room.GetWiredHandler().EnqueueCycle(this);
            }
            return true;
        }

        public bool OnCycle()
        {
            long num = Azure.Now();
            if (this._mNext >= num)
            {
                return false;
            }
            List<IWiredItem> conditions = this.Room.GetWiredHandler().GetConditions(this);
            List<IWiredItem> effects = this.Room.GetWiredHandler().GetEffects(this);
            if (conditions.Any())
            {
                foreach (IWiredItem current in conditions)
                {
                    if (!current.Execute(null))
                    {
                        return false;
                    }
                    WiredHandler.OnEvent(current);
                }
            }
            if (effects.Any())
            {
                foreach (IWiredItem current2 in effects)
                {
                    foreach (RoomUser current3 in this.Room.GetRoomUserManager().UserList.Values)
                    {
                        current2.Execute(new object[]
                        {
                            current3,
                            MType
                        });
                    }
                    WiredHandler.OnEvent(current2);
                }
            }
            this._mNext = checked(Azure.Now() + unchecked(this.Delay));
            return false;
        }
    }
}