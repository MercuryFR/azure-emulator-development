using System;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms.Wired.Handlers.Triggers
{
    public class SaysKeyword : IWiredItem
    {
        private const WiredItemType MType = WiredItemType.TriggerUserSaysKeyword;
        private readonly Room _mRoom;

        public SaysKeyword(RoomItem item, Room room)
        {
            this.Item = item;
            this._mRoom = room;
            this.OtherString = "";
            this.OtherBool = false;
        }

        public WiredItemType Type
        {
            get
            {
                return MType;
            }
        }

        public RoomItem Item { get; set; }

        public Room Room
        {
            get
            {
                return this._mRoom;
            }
        }

        public List<RoomItem> Items
        {
            get
            {
                return new List<RoomItem>();
            }
            set
            {
            }
        }

        public int Delay
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }

        public string OtherString { get; set; }

        public string OtherExtraString
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public string OtherExtraString2
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public bool OtherBool { get; set; }

        public bool Execute(params object[] stuff)
        {
            var roomUser = (RoomUser)stuff[0];
            var text = (string)stuff[1];
            if (string.IsNullOrEmpty(this.OtherString))
            {
                return false;
            }
            if (!String.Equals(text, this.OtherString, StringComparison.CurrentCultureIgnoreCase))
            {
                return false;
            }
            List<IWiredItem> conditions = this._mRoom.GetWiredHandler().GetConditions(this);
            List<IWiredItem> effects = this._mRoom.GetWiredHandler().GetEffects(this);
            bool flag = true;
            if (conditions.Any())
            {
                foreach (IWiredItem current in conditions)
                {
                    WiredHandler.OnEvent(current);
                    if (!current.Execute(new object[]
                    {
                        roomUser
                    }))
                    {
                        flag = false;
                    }
                }
            }
            if (flag)
            {
                roomUser.GetClient().SendWhisper(text);
            }
            if (effects.Any() && flag)
            {
                foreach (IWiredItem current2 in effects.Where(current2 => current2.Execute(new object[]
                {
                    roomUser,
                    this.Type
                })))
                {
                    WiredHandler.OnEvent(current2);
                }
            }

            WiredHandler.OnEvent(this);
            return true;
        }
    }
}