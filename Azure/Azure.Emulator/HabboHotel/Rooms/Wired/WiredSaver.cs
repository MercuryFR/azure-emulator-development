using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Rooms.Wired
{
    public static class WiredSaver
    {
        public static void SaveWired(GameClient session, RoomItem item, ClientMessage request)
        {
            if (!item.IsWired)
            {
                return;
            }

            Room room = item.GetRoom();
            WiredHandler wiredHandler = room.GetWiredHandler();
            checked
            {
                switch (item.GetBaseItem().InteractionType)
                {
                    case InteractionType.triggerroomenter:
                        {
                            request.GetInteger();
                            string otherString = request.GetString();
                            IWiredItem wired = wiredHandler.GetWired(item);
                            wired.OtherString = otherString;
                            wiredHandler.ReloadWired(wired);
                            break;
                        }

                    case InteractionType.triggerlongrepeater:
                        {
                            request.GetInteger();
                            int delay = request.GetInteger() * 5000;
                            IWiredItem wired2 = wiredHandler.GetWired(item);
                            wired2.Delay = delay;
                            wiredHandler.ReloadWired(wired2);
                            break;
                        }

                    case InteractionType.triggerrepeater:
                        {
                            request.GetInteger();
                            int delay = request.GetInteger() * 500;
                            IWiredItem wired2 = wiredHandler.GetWired(item);
                            wired2.Delay = delay;
                            wiredHandler.ReloadWired(wired2);
                            break;
                        }
                    case InteractionType.triggeronusersay:
                        {
                            request.GetInteger();
                            int num = request.GetInteger();
                            string otherString2 = request.GetString();
                            IWiredItem wired3 = wiredHandler.GetWired(item);
                            wired3.OtherString = otherString2;
                            wired3.OtherBool = (num == 1);
                            wiredHandler.ReloadWired(wired3);
                            break;
                        }
                    case InteractionType.triggerstatechanged:
                        {
                            request.GetInteger();
                            request.GetString();
                            List<RoomItem> furniItems = GetFurniItems(request, room);
                            int num2 = request.GetInteger();
                            IWiredItem wired4 = wiredHandler.GetWired(item);
                            wired4.Delay = num2 * 500;
                            wired4.Items = furniItems;
                            wiredHandler.ReloadWired(wired4);
                            break;
                        }
                    case InteractionType.triggerwalkonfurni:
                        {
                            request.GetInteger();
                            request.GetString();
                            List<RoomItem> furniItems2 = GetFurniItems(request, room);
                            int num3 = request.GetInteger();
                            IWiredItem wired5 = wiredHandler.GetWired(item);
                            wired5.Delay = num3 * 500;
                            wired5.Items = furniItems2;
                            wiredHandler.ReloadWired(wired5);
                            break;
                        }

                    case InteractionType.triggerwalkofffurni:
                        {
                            request.GetInteger();
                            request.GetString();
                            List<RoomItem> furniItems3 = GetFurniItems(request, room);
                            int num4 = request.GetInteger();
                            IWiredItem wired6 = wiredHandler.GetWired(item);
                            wired6.Delay = num4 * 500;
                            wired6.Items = furniItems3;
                            wiredHandler.ReloadWired(wired6);
                            break;
                        }
                    case InteractionType.actionmoverotate:
                        {
                            request.GetInteger();
                            int num5 = request.GetInteger();
                            int num6 = request.GetInteger();
                            request.GetString();
                            List<RoomItem> furniItems4 = GetFurniItems(request, room);
                            int num7 = request.GetInteger();
                            IWiredItem wired7 = wiredHandler.GetWired(item);
                            wired7.Items = furniItems4;
                            wired7.Delay = num7 * 500;
                            wired7.OtherString = string.Format("{0};{1}", num6, num5);
                            wiredHandler.ReloadWired(wired7);
                            break;
                        }
                    case InteractionType.actionshowmessage:
                    case InteractionType.actionkickuser:
                        {
                            request.GetInteger();
                            string otherString3 = request.GetString();
                            IWiredItem wired8 = wiredHandler.GetWired(item);
                            wired8.OtherString = otherString3;
                            wiredHandler.ReloadWired(wired8);
                            break;
                        }
                    case InteractionType.actionteleportto:
                        {
                            request.GetInteger();
                            request.GetString();
                            List<RoomItem> furniItems5 = GetFurniItems(request, room);
                            int num8 = request.GetInteger();
                            IWiredItem wired9 = wiredHandler.GetWired(item);
                            wired9.Items = furniItems5;
                            wired9.Delay = num8 * 500;
                            wiredHandler.ReloadWired(wired9);
                            break;
                        }
                    case InteractionType.actiontogglestate:
                        {
                            request.GetInteger();
                            request.GetString();
                            List<RoomItem> furniItems6 = GetFurniItems(request, room);
                            int num9 = request.GetInteger();
                            IWiredItem wired10 = wiredHandler.GetWired(item);
                            wired10.Items = furniItems6;
                            wired10.Delay = num9 * 500;
                            wiredHandler.ReloadWired(wired10);
                            break;
                        }
                    case InteractionType.actiongivereward:
                        {
                            request.GetInteger();
                            int num10 = request.GetInteger();
                            bool otherBool = request.GetInteger() == 1;
                            int num11 = request.GetInteger();
                            request.GetInteger();
                            string otherString4 = request.GetString();
                            List<RoomItem> furniItems7 = GetFurniItems(request, room);
                            IWiredItem wired11 = wiredHandler.GetWired(item);
                            wired11.Items = furniItems7;
                            wired11.Delay = 0;
                            wired11.OtherBool = otherBool;
                            wired11.OtherString = otherString4;
                            wired11.OtherExtraString = num10.ToString();
                            wired11.OtherExtraString2 = num11.ToString();
                            wiredHandler.ReloadWired(wired11);
                            break;
                        }
                    case InteractionType.actionmuteuser:
                        {
                            request.GetInteger();
                            int minutes = request.GetInteger() * 500;
                            string message = request.GetString();
                            List<RoomItem> furniItems7 = GetFurniItems(request, room);
                            IWiredItem wired11 = wiredHandler.GetWired(item);
                            wired11.Items = furniItems7;
                            wired11.Delay = minutes;
                            wired11.OtherBool = false;
                            wired11.OtherString = message;
                            wiredHandler.ReloadWired(wired11);
                            break;
                        }
                    case InteractionType.triggerscoreachieved:
                        {
                            request.GetInteger();
                            int pointsRequired = request.GetInteger();

                            IWiredItem wired11 = wiredHandler.GetWired(item);
                            wired11.Delay = 0;
                            wired11.OtherString = pointsRequired.ToString();
                            wiredHandler.ReloadWired(wired11);
                            break;
                        }

                    case InteractionType.conditionitemsmatches:
                    case InteractionType.conditionitemsdontmatch:
                    case InteractionType.actionposreset:
                        {
                            request.GetInteger();
                            bool actualExtraData = request.GetInteger() == 1;
                            bool actualRot = request.GetInteger() == 1;
                            bool actualPosition = request.GetInteger() == 1;

                            string booleans = string.Format("{0},{1},{2}", actualExtraData, actualRot, actualPosition).ToLower();

                            request.GetString();
                            List<RoomItem> items = GetFurniItems(request, room);

                            int delay = request.GetInteger() * 500;
                            IWiredItem wiry = wiredHandler.GetWired(item);

                            string dataToSave = "";
                            string extraStringForWi = "";

                            foreach (RoomItem aItem in items)
                            {
                                dataToSave += string.Format("{0}|{1}|{2}|{3},{4},{5}", aItem.Id, aItem.ExtraData, aItem.Rot, aItem.X, aItem.Y, aItem.Z);
                                extraStringForWi += string.Format("{0},{1},{2},{3},{4}", aItem.Id, (actualExtraData) ? aItem.ExtraData : "N", (actualRot) ? aItem.Rot.ToString() : "N", (actualPosition) ? aItem.X.ToString() : "N", (actualPosition) ? aItem.Y.ToString() : "N");

                                if (aItem == items.Last())
                                {
                                    continue;
                                }
                                dataToSave += "/";
                                extraStringForWi += ";";
                            }

                            wiry.Items = items;
                            wiry.Delay = delay;
                            wiry.OtherBool = true;
                            wiry.OtherString = booleans;
                            wiry.OtherExtraString = dataToSave;
                            wiry.OtherExtraString2 = extraStringForWi;
                            wiredHandler.ReloadWired(wiry);
                            break;
                        }

                    case InteractionType.conditiongroupmember:
                    case InteractionType.conditionnotgroupmember:
                        {
                            // Nothing is needed.
                            break;
                        }

                    case InteractionType.conditionhowmanyusersinroom:
                    case InteractionType.conditionnegativehowmanyusers:
                        {
                            request.GetInteger();
                            int minimum = request.GetInteger();
                            int maximum = request.GetInteger();

                            string ei = string.Format("{0},{1}", minimum, maximum);
                            IWiredItem wired12 = wiredHandler.GetWired(item);
                            wired12.Items = new List<RoomItem>();
                            wired12.OtherString = ei;
                            wiredHandler.ReloadWired(wired12);
                            break;
                        }

                    case InteractionType.conditionusernotwearingeffect:
                    case InteractionType.conditionuserwearingeffect:
                        {
                            request.GetInteger();
                            int effect = request.GetInteger();
                            IWiredItem wired12 = wiredHandler.GetWired(item);
                            wired12.Items = new List<RoomItem>();
                            wired12.OtherString = effect.ToString();
                            wiredHandler.ReloadWired(wired12);
                            break;
                        }

                    case InteractionType.conditionuserwearingbadge:
                    case InteractionType.conditionusernotwearingbadge:
                        {
                            request.GetInteger();
                            string badge = request.GetString();
                            IWiredItem wired12 = wiredHandler.GetWired(item);
                            wired12.Items = new List<RoomItem>();
                            wired12.OtherString = badge;
                            wiredHandler.ReloadWired(wired12);
                            break;
                        }

                    case InteractionType.conditiondaterangeactive:
                        {
                            request.GetInteger();
                            int startDate = request.GetInteger();
                            int endDate = request.GetInteger(); //timestamps

                            IWiredItem wired12 = wiredHandler.GetWired(item);
                            wired12.Items = new List<RoomItem>();
                            wired12.OtherString = string.Format("{0},{1}", startDate, endDate);

                            if (startDate == 0)
                            {
                                wired12.OtherString = "";
                                session.SendNotif(
                                    @"Para poder guardar la fecha debes introducirlo as�: <b>YYYY/MM/dd hh:mm</b> o bien <b>dd/MM/YYYY hh:mm</b><br />No especifiques fecha de t�rmino si no hay. La hora y los minutos son opcionales. An�mate a arreglarlo.<br /><br /><br />Con cari�o,<br /><b>Finn</b>");
                            }

                            wiredHandler.ReloadWired(wired12);
                            break;
                        }

                    case InteractionType.conditionfurnishaveusers:
                    case InteractionType.conditiontriggeronfurni:
                    case InteractionType.conditionfurnihasfurni:
                    case InteractionType.conditionfurnitypematches:
                    case InteractionType.conditionfurnishavenotusers:
                    case InteractionType.conditiontriggerernotonfurni:
                    case InteractionType.conditionfurnihasnotfurni:
                    case InteractionType.conditionfurnitypedontmatch:
                        {
                            request.GetInteger();
                            request.GetString();
                            List<RoomItem> furniItems8 = GetFurniItems(request, room);
                            IWiredItem wired12 = wiredHandler.GetWired(item);
                            wired12.Items = furniItems8;
                            wiredHandler.ReloadWired(wired12);
                            break;
                        }

                    case InteractionType.actiongivescore:
                        {
                            request.GetInteger();
                            int scoreToGive = request.GetInteger();
                            int maxTimesPerGame = request.GetInteger();

                            string newExtraInfo = string.Format("{0},{1}", scoreToGive, maxTimesPerGame);

                            List<RoomItem> furniItems8 = GetFurniItems(request, room);
                            IWiredItem wired12 = wiredHandler.GetWired(item);
                            wired12.Items = furniItems8;
                            wired12.OtherString = newExtraInfo;
                            wiredHandler.ReloadWired(wired12);
                            break;
                        }
                }
                session.SendMessage(new ServerMessage(LibraryParser.OutgoingRequest("SaveWiredMessageComposer")));
            }
        }

        private static List<RoomItem> GetFurniItems(ClientMessage request, Room room)
        {
            var list = new List<RoomItem>();
            int num = request.GetInteger();
            checked
            {
                for (int i = 0; i < num; i++)
                {
                    RoomItem item = room.GetRoomItemHandler().GetItem(request.GetUInteger());
                    if (item != null)
                    {
                        list.Add(item);
                    }
                }
                return list;
            }
        }
    }
}