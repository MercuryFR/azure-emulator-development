using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Rooms.Wired.Handlers.Conditions;
using Azure.HabboHotel.Rooms.Wired.Handlers.Effects;
using Azure.HabboHotel.Rooms.Wired.Handlers.Triggers;

namespace Azure.HabboHotel.Rooms.Wired
{
    public class WiredHandler
    {
        private readonly List<IWiredItem> _wiredItems;
        private readonly Room _room;
        private Queue _cycleItems;

        public WiredHandler(Room room)
        {
            _wiredItems = new List<IWiredItem>();
            _room = room;
            _cycleItems = new Queue();
        }

        public static void OnEvent(IWiredItem item)
        {
            item.Item.ExtraData = item.Item.ExtraData == "0" ? "1" : "0";
            item.Item.UpdateState(false, true);
            item.Item.ReqUpdate(1, true);
        }

        public IWiredItem LoadWired(IWiredItem fItem)
        {
            if (fItem == null || fItem.Item == null)
            {
                if (_wiredItems.Contains(fItem))
                    _wiredItems.Remove(fItem);
                return null;
            }
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT * FROM items_wireds WHERE id=@id LIMIT 1");
                queryReactor.AddParameter("id", fItem.Item.Id);
                var row = queryReactor.GetRow();
                if (row == null)
                {
                    var wiredItem = GenerateNewItem(fItem.Item);
                    AddWired(wiredItem);
                    SaveWired(wiredItem);
                    return wiredItem;
                }
                fItem.OtherString = row["string"].ToString();
                fItem.OtherBool = (row["bool"].ToString() == "1");
                fItem.Delay = (int) row["delay"];
                fItem.OtherExtraString = row["extra_string"].ToString();
                fItem.OtherExtraString2 = row["extra_string_2"].ToString();
                var array = row["items"].ToString().Split(';');
                foreach (string s in array)
                {
                    int value;
                    if (!int.TryParse(s, out value))
                        continue;
                    var item = _room.GetRoomItemHandler().GetItem(Convert.ToUInt32(value));
                    fItem.Items.Add(item);
                }
                AddWired(fItem);
            }
            return fItem;
        }

        public void SaveWired(IWiredItem fItem)
        {
            if (fItem == null)
                return;
            checked
            {
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    var text = "";
                    var num = 0;
                    foreach (var current in fItem.Items)
                    {
                        if (num != 0)
                            text += ";";
                        text += current.Id;
                        num++;
                    }
                    if (fItem.OtherString == null)
                        fItem.OtherString = "";
                    if (fItem.OtherExtraString == null)
                        fItem.OtherExtraString = "";
                    if (fItem.OtherExtraString2 == null)
                        fItem.OtherExtraString2 = "";
                    queryReactor.SetQuery(
                        "REPLACE INTO items_wireds VALUES (@id, @items, @delay, @string, @bool, @extrastring, @extrastring2)");
                    queryReactor.AddParameter("id", fItem.Item.Id);
                    queryReactor.AddParameter("items", text);
                    queryReactor.AddParameter("delay", fItem.Delay);
                    queryReactor.AddParameter("string", fItem.OtherString);
                    queryReactor.AddParameter("bool", Azure.BoolToEnum(fItem.OtherBool));
                    queryReactor.AddParameter("extrastring", fItem.OtherExtraString);
                    queryReactor.AddParameter("extrastring2", fItem.OtherExtraString2);
                    queryReactor.RunQuery();
                }
            }
        }

        public void ReloadWired(IWiredItem item)
        {
            SaveWired(item);
            RemoveWired(item);
            AddWired(item);
        }

        public void ResetExtraString(WiredItemType type)
        {
            lock (_wiredItems)
            {
                foreach (var current in _wiredItems.Where(current => current.Type == type))
                    current.OtherExtraString = "0";
            }
        }

        public bool ExecuteWired(WiredItemType type, params object[] stuff)
        {
            try
            {
                if (!IsTrigger(type) || stuff == null)
                    return false;
                if (_wiredItems.Any(current => current != null && current.Type == type && current.Execute(stuff)))
                    return true;
            }
            catch (Exception e)
            {
                Writer.Writer.HandleException(e, "WiredHandler.cs:ExecuteWired Type: " + type);
            }
            return false;
        }

        public void OnCycle()
        {
            try
            {
                foreach (var current in _wiredItems)
                    if (current is IWiredCycler && current.Type == WiredItemType.TriggerRepeatEffect &&
                        !IsCycleQueued(current))
                        _cycleItems.Enqueue(current);
                    else if (current is IWiredCycler && current.Type == WiredItemType.TriggerLongRepeater &&
                             !IsCycleQueued(current))
                        _cycleItems.Enqueue(current);
                var queue = new Queue();
                lock (_cycleItems.SyncRoot)
                {
                    while (_cycleItems.Count > 0)
                    {
                        var wiredItem = (IWiredItem) _cycleItems.Dequeue();
                        var item = wiredItem as IWiredCycler;
                        if (item == null)
                            continue;
                        var wiredCycler = item;
                        if (!wiredCycler.OnCycle())
                            queue.Enqueue(item);
                    }
                }

                _cycleItems = queue;
            }
            catch (Exception e)
            {
                Writer.Writer.HandleException(e, "WiredHandler.cs:OnCycle");
            }
        }

        public void EnqueueCycle(IWiredItem item)
        {
            if (!_cycleItems.Contains(item))
                _cycleItems.Enqueue(item);
        }

        public bool IsCycleQueued(IWiredItem item) { return _cycleItems.Contains(item); }

        public void AddWired(IWiredItem item)
        {
            if (_wiredItems.Contains(item))
                _wiredItems.Remove(item);
            _wiredItems.Add(item);
        }

        public void RemoveWired(IWiredItem item)
        {
            if (!_wiredItems.Contains(item))
                _wiredItems.Remove(item);
            _wiredItems.Remove(item);
        }

        public void RemoveWired(RoomItem item)
        {
            foreach (var current in _wiredItems)
            {
                if (current == null || current.Item == null || current.Item.Id != item.Id)
                    continue;
                var queue = new Queue();
                lock (_cycleItems.SyncRoot)
                {
                    while (_cycleItems.Count > 0)
                    {
                        var wiredItem = (IWiredItem) _cycleItems.Dequeue();
                        if (wiredItem.Item.Id != item.Id)
                            queue.Enqueue(wiredItem);
                    }
                }
                _cycleItems = queue;
                _wiredItems.Remove(current);
                break;
            }
        }

        public IWiredItem GenerateNewItem(RoomItem item)
        {
            switch (item.GetBaseItem().InteractionType)
            {
                case InteractionType.triggerroomenter:
                    return new UserEntersRoom(item, _room);
                case InteractionType.triggergameend:
                    return new GameEnds(item, _room);
                case InteractionType.triggergamestart:
                    return new GameStarts(item, _room);
                case InteractionType.triggerrepeater:
                    return new Repeater(item, _room);
                case InteractionType.triggerlongrepeater:
                    return new LongRepeater(item, _room);
                case InteractionType.triggeronusersay:
                    return new SaysKeyword(item, _room);
                case InteractionType.triggerscoreachieved:
                    return new ScoreAchieved(item, _room);
                case InteractionType.triggerstatechanged:
                    return new FurniStateToggled(item, _room);
                case InteractionType.triggerwalkonfurni:
                    return new WalksOnFurni(item, _room);
                case InteractionType.triggerwalkofffurni:
                    return new WalksOffFurni(item, _room);
                case InteractionType.actionmoverotate:
                    return new MoveRotateFurni(item, _room);
                case InteractionType.actionshowmessage:
                    return new ShowMessage(item, _room);
                case InteractionType.actionteleportto:
                    return new TeleportToFurni(item, _room);
                case InteractionType.actiontogglestate:
                    return new ToggleFurniState(item, _room);
                case InteractionType.actionkickuser:
                    return new KickUser(item, _room);

                case InteractionType.conditionfurnishaveusers:
                    return new FurniHasUsers(item, _room);

                    // CONDICIONES NUEVAS:

                case InteractionType.conditionitemsmatches:
                    return new ItemsCoincide(item, _room);

                case InteractionType.conditionfurnitypematches:
                    return new ItemsTypeMatches(item, _room);
                case InteractionType.conditionhowmanyusersinroom:
                    return new HowManyUsers(item, _room);

                case InteractionType.conditiongroupmember:
                    return new IsGroupMember(item, _room);
                case InteractionType.conditiontriggeronfurni:
                    return new TriggererOnFurni(item, _room);
                case InteractionType.conditionfurnihasfurni:
                    return new FurniHasFurni(item, _room);
                case InteractionType.conditionuserwearingeffect:
                    return new UserIsWearingEffect(item, _room);
                case InteractionType.conditionuserwearingbadge:
                    return new UserIsWearingBadge(item, _room);
                case InteractionType.conditiondaterangeactive:
                    return new DateRangeActive(item, _room);

                    // CONDICIONES NEGATIVAS:
                case InteractionType.conditiontriggerernotonfurni:
                    return new TriggererNotOnFurni(item, _room);

                case InteractionType.conditionfurnihasnotfurni:
                    return new FurniHasNotFurni(item, _room);

                case InteractionType.conditionfurnishavenotusers:
                    return new FurniHasNotUsers(item, _room);

                case InteractionType.conditionitemsdontmatch:
                    return new ItemsNotCoincide(item, _room);

                case InteractionType.conditionfurnitypedontmatch:
                    return new ItemsTypeDontMatch(item, _room);

                case InteractionType.conditionnotgroupmember:
                    return new IsNotGroupMember(item, _room);

                case InteractionType.conditionnegativehowmanyusers:
                    return new NotHowManyUsersInRoom(item, _room);
                case InteractionType.conditionusernotwearingeffect:
                    return new UserIsNotWearingEffect(item, _room);
                case InteractionType.conditionusernotwearingbadge:
                    return new UserIsNotWearingBadge(item, _room);

                    // Efectos NUEVOS:
                case InteractionType.actiongivereward:
                    return new GiveReward(item, _room);
                case InteractionType.actionposreset:
                    return new ResetPosition(item, _room);
                case InteractionType.actiongivescore:
                    return new GiveScore(item, _room);
                case InteractionType.actionmuteuser:
                    return new MuteUser(item, _room);
            }
            return null;
        }

        public List<IWiredItem> GetConditions(IWiredItem item)
        {
            return
                _wiredItems.Where(
                    current =>
                        IsCondition(current.Type) && current.Item.X == item.Item.X && current.Item.Y == item.Item.Y)
                    .ToList();
        }

        public List<IWiredItem> GetEffects(IWiredItem item)
        {
            return
                _wiredItems.Where(
                    current => IsEffect(current.Type) && current.Item.X == item.Item.X && current.Item.Y == item.Item.Y)
                    .ToList();
        }

        public IWiredItem GetWired(RoomItem item)
        {
            return _wiredItems.FirstOrDefault(current => item.Id == current.Item.Id);
        }

        public void MoveWired(RoomItem item)
        {
            var wired = GetWired(item);
            if (wired == null)
                return;
            wired.Item = item;
            RemoveWired(item);
            AddWired(wired);
        }

        public void Destroy()
        {
            _wiredItems.Clear();
            _cycleItems.Clear();
        }

        private static bool IsTrigger(WiredItemType type)
        {
            switch (type)
            {
                case WiredItemType.TriggerUserEntersRoom:
                case WiredItemType.TriggerUserSaysKeyword:
                case WiredItemType.TriggerRepeatEffect:
                case WiredItemType.TriggerGameStarts:
                case WiredItemType.TriggerGameEnds:
                case WiredItemType.TriggerToggleFurni:
                case WiredItemType.TriggerWalksOnFurni:
                case WiredItemType.TriggerWalksOffFurni:
                case WiredItemType.TriggerScoreAchieved:
                case WiredItemType.TriggerLongRepeater:
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsEffect(WiredItemType type)
        {
            switch (type)
            {
                case WiredItemType.EffectShowMessage:
                case WiredItemType.EffectTeleportToFurni:
                case WiredItemType.EffectToggleFurniState:
                case WiredItemType.EffectMoveRotateFurni:
                case WiredItemType.EffectKickUser:
                case WiredItemType.EffectGiveReward:
                case WiredItemType.EffectResetPosition:
                case WiredItemType.EffectGiveScore:
                case WiredItemType.EffectResetTimers:
                case WiredItemType.EffectMuteUser:
                    return true;
            }
            return false;
        }

        private static bool IsCondition(WiredItemType type)
        {
            switch (type)
            {
                case WiredItemType.ConditionFurniHasUsers:
                case WiredItemType.ConditionFurniHasFurni:
                case WiredItemType.ConditionTriggererOnFurni:
                case WiredItemType.ConditionFurniCoincides:
                case WiredItemType.ConditionIsGroupMember:
                case WiredItemType.ConditionFurniTypeMatches:
                case WiredItemType.ConditionHowManyUsers:
                case WiredItemType.ConditionTriggererNotOnFurni:
                case WiredItemType.ConditionFurniHasNotFurni:
                case WiredItemType.ConditionFurniHaveNotUsers:
                case WiredItemType.ConditionItemsDontMatch:
                case WiredItemType.ConditionFurniTypeDontMatch:
                case WiredItemType.ConditionIsNotGroupMember:
                case WiredItemType.ConditionNotHowManyUsers:
                case WiredItemType.ConditionIsWearingEffect:
                case WiredItemType.ConditionIsNotWearingEffect:
                case WiredItemType.ConditionIsWearingBadge:
                case WiredItemType.ConditionIsNotWearingBadge:
                case WiredItemType.ConditionDateRangeActive:
                    return true;
                default:
                    return false;
            }
        }
    }
}