using System;
using System.Collections;

namespace Azure.HabboHotel.Rooms.Wired
{
    public interface WiredCycler
    {
        Queue ToWork { get; set; }

        bool OnCycle();
    }
}