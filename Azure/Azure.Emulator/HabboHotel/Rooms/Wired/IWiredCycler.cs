using System.Collections;

namespace Azure.HabboHotel.Rooms.Wired
{
    public interface IWiredCycler
    {
        Queue ToWork { get; set; }

        bool OnCycle();
    }
}