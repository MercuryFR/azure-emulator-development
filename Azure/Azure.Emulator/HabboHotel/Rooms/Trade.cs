using System;
using System.Collections.Generic;
using System.Linq;
using Azure.Configuration;
using Azure.HabboHotel.Items;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Rooms
{
    internal class Trade
    {
        private readonly TradeUser[] _users;
        private readonly uint _roomId;
        private readonly uint _oneId;
        private readonly uint _twoId;

        private int _tradeStage;

        internal Trade(uint userOneId, uint userTwoId, uint roomId)
        {
            this._oneId = userOneId;
            this._twoId = userTwoId;
            this._users = new TradeUser[2];
            this._users[0] = new TradeUser(userOneId, roomId);
            this._users[1] = new TradeUser(userTwoId, roomId);
            this._tradeStage = 1;
            this._roomId = roomId;
            TradeUser[] users = this._users;
            foreach (TradeUser tradeUser in users.Where(tradeUser => !tradeUser.GetRoomUser().Statusses.ContainsKey("trd")))
            {
                tradeUser.GetRoomUser().AddStatus("trd", "");
                tradeUser.GetRoomUser().UpdateNeeded = true;
            }
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeStartMessageComposer"));
            serverMessage.AppendInteger(userOneId);
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(userTwoId);
            serverMessage.AppendInteger(1);
            this.SendMessageToUsers(serverMessage);
        }

        internal bool AllUsersAccepted
        {
            get
            {
                checked
                {
                    return this._users.All(t => t == null || t.HasAccepted);
                }
            }
        }

        internal bool ContainsUser(uint id)
        {
            checked
            {
                return this._users.Any(t => t != null && t.UserId == id);
            }
        }

        internal TradeUser GetTradeUser(uint id)
        {
            checked
            {
                return this._users.FirstOrDefault(t => t != null && t.UserId == id);
            }
        }

        internal void OfferItem(uint userId, UserItem item)
        {
            TradeUser tradeUser = this.GetTradeUser(userId);
            if (tradeUser == null || item == null || !item.BaseItem.AllowTrade || tradeUser.HasAccepted ||
                this._tradeStage != 1)
            {
                return;
            }
            this.ClearAccepted();
            if (!tradeUser.OfferedItems.Contains(item))
            {
                tradeUser.OfferedItems.Add(item);
            }
            this.UpdateTradeWindow();
        }

        internal void TakeBackItem(uint userId, UserItem item)
        {
            TradeUser tradeUser = this.GetTradeUser(userId);
            if (tradeUser == null || item == null || tradeUser.HasAccepted || this._tradeStage != 1)
            {
                return;
            }
            this.ClearAccepted();
            tradeUser.OfferedItems.Remove(item);
            this.UpdateTradeWindow();
        }

        internal void Accept(uint userId)
        {
            TradeUser tradeUser = this.GetTradeUser(userId);
            if (tradeUser == null || this._tradeStage != 1)
            {
                return;
            }
            tradeUser.HasAccepted = true;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeAcceptMessageComposer"));
            serverMessage.AppendInteger(userId);
            serverMessage.AppendInteger(1);
            this.SendMessageToUsers(serverMessage);
            checked
            {
                if (!this.AllUsersAccepted)
                {
                    return;
                }
                this.SendMessageToUsers(new ServerMessage(LibraryParser.OutgoingRequest("TradeConfirmationMessageComposer")));
                this._tradeStage++;
                this.ClearAccepted();
            }
        }

        internal void Unaccept(uint userId)
        {
            TradeUser tradeUser = this.GetTradeUser(userId);
            if (tradeUser == null || this._tradeStage != 1 || this.AllUsersAccepted)
            {
                return;
            }
            tradeUser.HasAccepted = false;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeAcceptMessageComposer"));
            serverMessage.AppendInteger(userId);
            serverMessage.AppendInteger(0);
            this.SendMessageToUsers(serverMessage);
        }

        internal void CompleteTrade(uint userId)
        {
            TradeUser tradeUser = this.GetTradeUser(userId);
            if (tradeUser == null || this._tradeStage != 2)
            {
                return;
            }
            tradeUser.HasAccepted = true;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeAcceptMessageComposer"));
            serverMessage.AppendInteger(userId);
            serverMessage.AppendInteger(1);
            this.SendMessageToUsers(serverMessage);
            if (!this.AllUsersAccepted)
            {
                return;
            }
            this._tradeStage = 999;
            this.Finnito();
        }

        internal void ClearAccepted()
        {
            TradeUser[] users = this._users;
            foreach (TradeUser tradeUser in users)
            {
                tradeUser.HasAccepted = false;
            }
        }

        internal void UpdateTradeWindow()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeUpdateMessageComposer"));
            checked
            {
                foreach (TradeUser tradeUser in this._users.Where(tradeUser => tradeUser != null))
                {
                    serverMessage.AppendInteger(tradeUser.UserId);
                    serverMessage.AppendInteger(tradeUser.OfferedItems.Count);
                    foreach (UserItem current in tradeUser.OfferedItems)
                    {
                        serverMessage.AppendInteger(current.Id);
                        serverMessage.AppendString(current.BaseItem.Type.ToString().ToLower());
                        serverMessage.AppendInteger(current.Id);
                        serverMessage.AppendInteger(current.BaseItem.SpriteId);
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendBool(true);
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendString("");
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendInteger(0);
                        if (current.BaseItem.Type == 's')
                        {
                            serverMessage.AppendInteger(0);
                        }
                    }
                }
                this.SendMessageToUsers(serverMessage);
            }
        }

        internal void DeliverItems()
        {
            List<UserItem> offeredItems = this.GetTradeUser(this._oneId).OfferedItems;
            List<UserItem> offeredItems2 = this.GetTradeUser(this._twoId).OfferedItems;
            if (
            offeredItems.Any(
                current =>
                          this.GetTradeUser(this._oneId).GetClient().GetHabbo().GetInventoryComponent().GetItem(current.Id) == null))
            {
                this.GetTradeUser(this._oneId).GetClient().SendNotif("El tradeo ha fallado.");
                this.GetTradeUser(this._twoId).GetClient().SendNotif("El tradeo ha fallado.");
                return;
            }
            if (
            offeredItems2.Any(
                current2 =>
                           this.GetTradeUser(this._twoId).GetClient().GetHabbo().GetInventoryComponent().GetItem(current2.Id) == null))
            {
                this.GetTradeUser(this._oneId).GetClient().SendNotif("El tradeo ha fallado.");
                this.GetTradeUser(this._twoId).GetClient().SendNotif("El tradeo ha fallado.");
                return;
            }
            this.GetTradeUser(this._twoId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
            this.GetTradeUser(this._oneId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
            foreach (UserItem current3 in offeredItems)
            {
                this.GetTradeUser(this._oneId).GetClient().GetHabbo().GetInventoryComponent().RemoveItem(current3.Id, false);
                this.GetTradeUser(this._twoId)
                    .GetClient()
                    .GetHabbo()
                    .GetInventoryComponent()
                    .AddNewItem(current3.Id, current3.BaseItemId, current3.ExtraData, current3.GroupId, false, false, 0, 0,
                         current3.SongCode);
                this.GetTradeUser(this._oneId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
                this.GetTradeUser(this._twoId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
            }
            foreach (UserItem current4 in offeredItems2)
            {
                this.GetTradeUser(this._twoId).GetClient().GetHabbo().GetInventoryComponent().RemoveItem(current4.Id, false);
                this.GetTradeUser(this._oneId)
                    .GetClient()
                    .GetHabbo()
                    .GetInventoryComponent()
                    .AddNewItem(current4.Id, current4.BaseItemId, current4.ExtraData, current4.GroupId, false, false, 0, 0,
                         current4.SongCode);
                this.GetTradeUser(this._twoId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
                this.GetTradeUser(this._oneId).GetClient().GetHabbo().GetInventoryComponent().RunDbUpdate();
            }
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("NewInventoryObjectMessageComposer"));
            serverMessage.AppendInteger(1);
            int i = 1;
            if (offeredItems.Any(current5 => current5.BaseItem.Type.ToString().ToLower() != "s"))
            {
                i = 2;
            }
            serverMessage.AppendInteger(i);
            serverMessage.AppendInteger(offeredItems.Count);
            foreach (UserItem current6 in offeredItems)
            {
                serverMessage.AppendInteger(current6.Id);
            }
            this.GetTradeUser(this._twoId).GetClient().SendMessage(serverMessage);
            var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("NewInventoryObjectMessageComposer"));
            serverMessage2.AppendInteger(1);
            i = 1;
            if (offeredItems2.Any(current7 => current7.BaseItem.Type.ToString().ToLower() != "s"))
            {
                i = 2;
            }
            serverMessage2.AppendInteger(i);
            serverMessage2.AppendInteger(offeredItems2.Count);
            foreach (UserItem current8 in offeredItems2)
            {
                serverMessage2.AppendInteger(current8.Id);
            }
            this.GetTradeUser(this._oneId).GetClient().SendMessage(serverMessage2);
            this.GetTradeUser(this._oneId).GetClient().GetHabbo().GetInventoryComponent().UpdateItems(false);
            this.GetTradeUser(this._twoId).GetClient().GetHabbo().GetInventoryComponent().UpdateItems(false);
        }

        internal void CloseTradeClean()
        {
            checked
            {
                foreach (
                    TradeUser tradeUser in this._users.Where(tradeUser => tradeUser != null && tradeUser.GetRoomUser() != null))
                {
                    tradeUser.GetRoomUser().RemoveStatus("trd");
                    tradeUser.GetRoomUser().UpdateNeeded = true;
                }
                this.SendMessageToUsers(new ServerMessage(LibraryParser.OutgoingRequest("TradeCompletedMessageComposer")));
                this.GetRoom().ActiveTrades.Remove(this);
            }
        }

        internal void CloseTrade(uint userId)
        {
            checked
            {
                foreach (
                    TradeUser tradeUser in this._users.Where(tradeUser => tradeUser != null && tradeUser.GetRoomUser() != null))
                {
                    tradeUser.GetRoomUser().RemoveStatus("trd");
                    tradeUser.GetRoomUser().UpdateNeeded = true;
                }
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TradeCloseMessageComposer"));
                serverMessage.AppendInteger(userId);
                serverMessage.AppendInteger(0);
                this.SendMessageToUsers(serverMessage);
            }
        }

        internal void SendMessageToUsers(ServerMessage message)
        {
            if (this._users == null)
            {
                return;
            }
            checked
            {
                foreach (TradeUser tradeUser in this._users.Where(tradeUser => tradeUser != null && tradeUser.GetClient() != null))
                {
                    tradeUser.GetClient().SendMessage(message);
                }
            }
        }

        private void Finnito()
        {
            try
            {
                this.DeliverItems();
                this.CloseTradeClean();
            }
            catch (Exception ex)
            {
                Logging.LogThreadException(ex.ToString(), "Trade task");
            }
        }

        private Room GetRoom()
        {
            return Azure.GetGame().GetRoomManager().GetRoom(this._roomId);
        }
    }
}