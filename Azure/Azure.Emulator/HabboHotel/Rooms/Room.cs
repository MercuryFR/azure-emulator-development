using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using Azure.Configuration;
using Azure.HabboHotel.Catalogs;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Groups;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.RoomBots;
using Azure.HabboHotel.Rooms.Games;
using Azure.HabboHotel.Rooms.RoomInvokedItems;
using Azure.HabboHotel.Rooms.Wired;
using Azure.HabboHotel.SoundMachine;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Rooms
{
    public class Room
    {
        internal string Name;
        internal string Description;
        internal string Type;

        internal string Owner;
        internal int OwnerId;

        internal string Password;
        internal int Category;
        internal int State;
        internal int TradeState;
        internal int UsersNow;
        internal int UsersMax;
        internal string ModelName;
        internal int Score;

        internal bool RoomMuted;

        internal ArrayList Tags;
        internal int AllowPets;
        internal int AllowPetsEating;
        internal int AllowWalkthrough;

        internal int WallThickness;
        internal int FloorThickness;
        internal int Hidewall;
        internal int WallHeight;

        internal int ChatType;
        internal int ChatBalloon;
        internal int ChatSpeed;
        internal int ChatMaxDistance;
        internal int ChatFloodProtection;

        internal uint GroupId;
        internal Guild Group;
        internal RoomEvent Event;

        internal TeamManager TeamBanzai;
        internal TeamManager TeamFreeze;
        internal List<uint> UsersWithRights;
        internal bool EveryoneGotRights;
        internal int WhoCanKick;
        internal int WhoCanMute;
        internal int WhoCanBan;
        internal Dictionary<long, double> Bans;
        internal Dictionary<uint, string> LoadedGroups;
        internal Dictionary<uint, uint> MutedUsers;
        internal string Wallpaper;
        internal string Floor;
        internal string Landscape;
        internal bool MutedBots;
        internal MoodlightData MoodlightData;
        internal TonerData TonerData;
        internal ArrayList ActiveTrades;
        internal ConcurrentBag<Chatlog> RoomChat;
        internal List<string> WordFilter;
        private int _tagCount;
        private bool _mCycleEnded;
        private int _idleTime;
        private GameManager _game;
        private Gamemap _gameMap;
        private RoomItemHandling _roomItemHandling;
        private RoomUserManager _roomUserManager;
        private Soccer _soccer;
        private BattleBanzai _banzai;
        private Freeze _freeze;
        private GameItemHandler _gameItemHandler;
        private RoomMusicController _musicController;
        private WiredHandler _wiredHandler;
        private bool _isCrashed;
        private bool _mDisposed;
        private Queue _roomKick;

        private Thread _roomThread;
        private Timer _processTimer;

        internal Room(RoomData data)
        {
            InitializeFromRoomData(data);
            GetRoomItemHandler().LoadFurniture();
            GetGameMap().GenerateMaps(true);
        }

        internal int UserCount
        {
            get { return _roomUserManager.GetRoomUserCount(); }
        }

        internal int TagCount
        {
            get { return Tags.Count; }
        }

        internal uint RoomId { get; private set; }

        internal bool CanTradeInRoom
        {
            get { return true; }
        }

        internal RoomData RoomData { get; private set; }

        public WiredHandler GetWiredHandler() { return _wiredHandler ?? (_wiredHandler = new WiredHandler(this)); }

        internal Gamemap GetGameMap() { return _gameMap; }

        internal RoomItemHandling GetRoomItemHandler() { return _roomItemHandling; }

        internal RoomUserManager GetRoomUserManager() { return _roomUserManager; }

        internal Soccer GetSoccer() { return _soccer ?? (_soccer = new Soccer(this)); }

        internal TeamManager GetTeamManagerForBanzai()
        {
            return TeamBanzai ?? (TeamBanzai = TeamManager.CreateTeamforGame("banzai"));
        }

        internal TeamManager GetTeamManagerForFreeze()
        {
            return TeamFreeze ?? (TeamFreeze = TeamManager.CreateTeamforGame("freeze"));
        }

        internal BattleBanzai GetBanzai() { return _banzai ?? (_banzai = new BattleBanzai(this)); }

        internal Freeze GetFreeze() { return _freeze ?? (_freeze = new Freeze(this)); }

        internal GameManager GetGameManager() { return _game ?? (_game = new GameManager(this)); }

        internal GameItemHandler GetGameItemHandler()
        {
            return _gameItemHandler ?? (_gameItemHandler = new GameItemHandler(this));
        }

        internal RoomMusicController GetRoomMusicController()
        {
            return _musicController ?? (_musicController = new RoomMusicController());
        }

        internal bool GotMusicController() { return _musicController != null; }

        internal bool GotSoccer() { return _soccer != null; }

        internal bool GotBanzai() { return _banzai != null; }

        internal bool GotFreeze() { return _freeze != null; }

        internal void StartRoomProcessing() { _processTimer = new Timer(ProcessRoom, null, 0, 490); }

        internal void InitUserBots()
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT * FROM bots WHERE room_id = {0} AND ai_type = 'generic'",
                    RoomId));
                var table = queryReactor.GetTable();
                if (table == null)
                    return;
                foreach (var roomBot in from DataRow dataRow in table.Rows select BotManager.GenerateBotFromRow(dataRow)
                    )
                    _roomUserManager.DeployBot(roomBot, null);
            }
        }

        internal void ClearTags()
        {
            Tags.Clear();
            _tagCount = 0;
        }

        internal void AddTagRange(List<string> tags)
        {
            checked
            {
                _tagCount += tags.Count;
                Tags.AddRange(tags);
            }
        }

        internal void InitBots()
        {
            var botsForRoom = Azure.GetGame().GetBotManager().GetBotsForRoom(RoomId);
            foreach (var current in botsForRoom.Where(current => !current.IsPet))
                DeployBot(current);
        }

        internal void InitPets()
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT * FROM bots WHERE room_id = {0} AND ai_type='pet'", RoomId));
                var table = queryReactor.GetTable();
                if (table == null)
                    return;
                foreach (DataRow dataRow in table.Rows)
                {
                    queryReactor.SetQuery(string.Format("SELECT * FROM pets_data WHERE id={0} LIMIT 1", dataRow[0]));
                    var row = queryReactor.GetRow();
                    if (row == null)
                        continue;
                    var pet = Catalog.GeneratePetFromRow(dataRow, row);
                    var bot = new RoomBot(pet.PetId, Convert.ToUInt32(OwnerId), AIType.Pet, false);
                    bot.Update(RoomId, "freeroam", pet.Name, "",
                        pet.Look, pet.X, pet.Y, checked((int) pet.Z), 4, 0, 0, 0, 0, null, null, "", 0, 0, false, false);

                    _roomUserManager.DeployBot(bot, pet);
                }
            }
        }

        internal RoomUser DeployBot(RoomBot bot) { return _roomUserManager.DeployBot(bot, null); }

        internal void QueueRoomKick(RoomKick kick)
        {
            lock (_roomKick.SyncRoot)
            {
                _roomKick.Enqueue(kick);
            }
        }

        internal void OnRoomKick()
        {
            var list = _roomUserManager.UserList.Values.Where(
                current => !current.IsBot && current.GetClient().GetHabbo().Rank < 4u).ToList();
            checked
            {
                foreach (var t in list)
                {
                    GetRoomUserManager().RemoveUserFromRoom(t.GetClient(), true, false);
                    t.GetClient().CurrentRoomUserId = -1;
                }
            }
        }

        internal void OnUserEnter(RoomUser user)
        {
            GetWiredHandler().ExecuteWired(WiredItemType.TriggerUserEntersRoom, new object[]
            {
                user
            });

            var count = 0;

            foreach (var current in _roomUserManager.UserList.Values)
            {
                if (current.IsBot || current.IsPet)
                {
                    current.BotAI.OnUserEnterRoom(user);
                    count++;
                }

                if (count >= 3)
                    break;
            }
        }

        internal void OnUserSay(RoomUser user, string message, bool shout)
        {
            foreach (var current in _roomUserManager.UserList.Values)
                try
                {
                    if (!current.IsBot && !current.IsPet)
                        continue;
                    if (!current.IsPet && message.ToLower().StartsWith(current.BotData.Name.ToLower()))
                    {
                        message = message.Substring(current.BotData.Name.Length);
                        if (shout)
                            current.BotAI.OnUserShout(user, message);
                        else
                            current.BotAI.OnUserSay(user, message);
                    }
                    else if (current.IsPet && message.StartsWith(current.PetData.Name) && current.PetData.Type != 16)
                    {
                        message = message.Substring(current.PetData.Name.Length);
                        current.BotAI.OnUserSay(user, message);
                    }
                }
                catch (Exception)
                {
                    return;
                }
        }

        internal void LoadMusic()
        {
            DataTable table;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    string.Format(
                        "SELECT items_songs.songid,items_rooms.id,items_rooms.base_item FROM items_songs LEFT JOIN items_rooms ON items_rooms.id = items_songs.itemid WHERE items_songs.roomid = {0}",
                        RoomId));
                table = queryReactor.GetTable();
            }

            if (table == null)
                return;
            foreach (DataRow dataRow in table.Rows)
            {
                var songId = (uint) dataRow[0];
                var num = Convert.ToUInt32(dataRow[1]);
                var baseItem = Convert.ToInt32(dataRow[2]);
                var songCode = "";
                var extraData = "";
                using (var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryreactor2.SetQuery(string.Format("SELECT extra_data,songcode FROM items_rooms WHERE id = {0}",
                        num));
                    var row = queryreactor2.GetRow();
                    if (row != null)
                    {
                        extraData = (string) row["extra_data"];
                        songCode = (string) row["songcode"];
                    }
                }
                var diskItem = new SongItem(num, songId, baseItem, extraData, songCode);
                GetRoomMusicController().AddDisk(diskItem);
            }
        }

        internal void LoadRights()
        {
            UsersWithRights = new List<uint>();
            DataTable dataTable;
            if (Group != null)
                return;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format(
                    "SELECT rooms_rights.user_id FROM rooms_rights WHERE room_id = {0}", RoomId));
                dataTable = queryReactor.GetTable();
            }
            if (dataTable == null)
                return;
            foreach (DataRow dataRow in dataTable.Rows)
                UsersWithRights.Add(Convert.ToUInt32(dataRow["user_id"]));
        }

        internal void LoadBans()
        {
            Bans = new Dictionary<long, double>();
            DataTable table;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT user_id, expire FROM rooms_bans WHERE room_id = {0}", RoomId));
                table = queryReactor.GetTable();
            }
            if (table == null)
                return;
            foreach (DataRow dataRow in table.Rows)
                Bans.Add((uint) dataRow[0], Convert.ToDouble(dataRow[1]));
        }

        internal int GetRightsLevel(GameClient session)
        {
            try
            {
                if (session == null || session.GetHabbo() == null)
                    return 0;
                if (session.GetHabbo().UserName == Owner || session.GetHabbo().HasFuse("fuse_admin") ||
                    session.GetHabbo().HasFuse("fuse_any_room_controller"))
                    return 4;
                if (session.GetHabbo().HasFuse("fuse_any_rooms_rights"))
                    return 3;
                if (EveryoneGotRights || UsersWithRights.Contains(session.GetHabbo().Id))
                    return 1;
            }
            catch (Exception pException)
            {
                Logging.HandleException(pException, "GetRightsLevel");
            }
            return 0;
        }

        internal bool CheckRights(GameClient session) { return CheckRights(session, false, false); }

        internal bool CheckGroupRights(GameClient session)
        {
            try
            {
                if (Group == null)
                    return false;
                if (Group.AdminOnlyDeco == 0u && Group.Members.ContainsKey(session.GetHabbo().Id))
                    return true;
                if (Group.Admins.ContainsKey(session.GetHabbo().Id))
                {
                    session.SendNotif("Group Admin");
                    return true;
                }
                if (session.GetHabbo().UserName == Owner)
                    return true;
                if (session.GetHabbo().HasFuse("fuse_admin") || session.GetHabbo().HasFuse("fuse_any_room_controller"))
                    return true;
            }
            catch (Exception pException)
            {
                Logging.HandleException(pException, "Room.CheckGroupRights");
            }
            return false;
        }

        internal bool CheckRights(GameClient session, bool requireOwnership, bool checkForGroups = false)
        {
            try
            {
                if (session == null || session.GetHabbo() == null)
                    return false;
                if (session.GetHabbo().UserName == Owner && Type == "private")
                    return true;
                if (session.GetHabbo().HasFuse("fuse_admin") || session.GetHabbo().HasFuse("fuse_any_room_controller"))
                    return true;
                if (!requireOwnership && Type == "private")
                {
                    if (session.GetHabbo().HasFuse("fuse_any_rooms_rights"))
                        return true;
                    if (EveryoneGotRights || UsersWithRights.Contains(session.GetHabbo().Id))
                        return true;
                }
                if (checkForGroups && Type == "private")
                {
                    if (Group == null)
                        return false;
                    if (Group.Admins.ContainsKey(session.GetHabbo().Id))
                        return true;
                    if (Group.AdminOnlyDeco == 0u && Group.Members.ContainsKey(session.GetHabbo().Id))
                        return true;
                }
            }
            catch (Exception pException)
            {
                Logging.HandleException(pException, "Room.CheckRights");
            }
            return false;
        }

        internal void ProcessRoom(object callItem) { ProcessRoom(); }

        internal void ProcessRoom()
        {
            try
            {
                if (_isCrashed || _mDisposed)
                    return;
                try
                {
                    var idle = 0;
                    GetRoomItemHandler().OnCycle();
                    GetRoomUserManager().OnCycle(ref idle);

                    if (idle > 0)
                        _idleTime++;
                    else
                        _idleTime = 0;

                    if (!_mCycleEnded)
                    {
                        if (_idleTime >= 10 /* && usersQueueToEnter.Count == 0*/)
                        {
                            Azure.GetGame().GetRoomManager().UnloadRoom(this, "No users");
                            return;
                        }
                        var serverMessage = GetRoomUserManager().SerializeStatusUpdates(false);

                        if (serverMessage != null)
                            SendMessage(serverMessage);
                    }

                    if (_gameItemHandler != null)
                        _gameItemHandler.OnCycle();
                    if (_game != null)
                        _game.OnCycle();
                    if (GotBanzai())
                        _banzai.OnCycle();
                    if (GotSoccer())
                        _soccer.OnCycle();
                    if (GetRoomMusicController() != null)
                        GetRoomMusicController().Update(this);
                    _roomUserManager.UserList.OnCycle();

                    GetWiredHandler().OnCycle();
                    WorkRoomKickQueue();
                }
                catch (Exception e)
                {
                    Writer.Writer.LogException(e.ToString());
                    OnRoomCrash(e);
                }
            }
            catch (Exception e)
            {
                Logging.LogCriticalException(string.Format("Sub crash in room cycle: {0}", e));
            }
        }

        internal void SendMessage(byte[] message)
        {
            try
            {
                var roomUsers = GetRoomUserManager().GetRoomUsers();

                foreach (var user in roomUsers)
                    user.SendMessage(message);
            }
            catch
            {
            }
        }

        internal void BroadcastChatMessage(ServerMessage chatMsg, RoomUser roomUser, uint p)
        {
            try
            {
                var roomUsers = GetRoomUserManager().GetRoomUsers();
                var msg = chatMsg.GetReversedBytes();
                foreach (
                    var user in
                        roomUsers.Where(user => user.OnCampingTent || !roomUser.OnCampingTent)
                            .Where(user => !user.GetClient().GetHabbo().MutedUsers.Contains(p)))
                    user.SendMessage(msg);
            }
            catch
            {
            }
        }

        internal void SendMessage(ServerMessage message)
        {
            if (message != null)
                SendMessage(message.GetReversedBytes());
        }

        internal void SendMessage(List<ServerMessage> messages)
        {
            if (!messages.Any())
                return;

            try
            {
                var array = new byte[0];
                var num = 0;
                foreach (var current in messages)
                {
                    var bytes = current.GetReversedBytes();
                    var newSize = array.Length + bytes.Length;
                    Array.Resize(ref array, newSize);
                    foreach (byte t in bytes)
                    {
                        array[num] = t;
                        num++;
                    }
                }

                SendMessage(array);
            }
            catch (Exception pException)
            {
                Logging.HandleException(pException, "Room.SendMessage List<ServerMessage>");
            }
        }

        internal void SendMessageToUsersWithRights(ServerMessage message)
        {
            try
            {
                var bytes = message.GetReversedBytes();
                foreach (
                    var client in
                        _roomUserManager.UserList.Values.Where(current => !current.IsBot)
                            .Select(current => current.GetClient())
                            .Where(client => client != null && CheckRights(client)))
                    try
                    {
                        client.GetConnection().SendData(bytes);
                    }
                    catch (Exception pException)
                    {
                        Logging.HandleException(pException, "Room.SendMessageToUsersWithRights");
                    }
            }
            catch (Exception pException2)
            {
                Logging.HandleException(pException2, "Room.SendMessageToUsersWithRights");
            }
        }

        internal void Destroy()
        {
            SendMessage(new ServerMessage(LibraryParser.OutgoingRequest("OutOfRoomMessageComposer")));
            Dispose();
        }

        internal bool UserIsBanned(uint pId) { return Bans.ContainsKey(pId); }

        internal void RemoveBan(uint pId) { Bans.Remove(pId); }

        internal void AddBan(int pId, long time)
        {
            if (!Bans.ContainsKey(Convert.ToInt32(pId)))
                Bans.Add(pId, checked(unchecked(Azure.GetUnixTimestamp()) + time));
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "REPLACE INTO rooms_bans VALUES (",
                    pId,
                    ", ",
                    RoomId,
                    ", '",
                    (Azure.GetUnixTimestamp() + time),
                    "')"
                }));
        }

        internal List<uint> BannedUsers()
        {
            var list = new List<uint>();
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    string.Format("SELECT user_id FROM rooms_bans WHERE expire > UNIX_TIMESTAMP() AND room_id={0}",
                        RoomId));
                var table = queryReactor.GetTable();
                list.AddRange(from DataRow dataRow in table.Rows select (uint) dataRow[0]);
            }
            return list;
        }

        internal bool HasBanExpired(uint pId) { return !UserIsBanned(pId) || Bans[pId] < Azure.GetUnixTimestamp(); }

        internal void Unban(uint userId)
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "DELETE FROM rooms_bans WHERE user_id=",
                    userId,
                    " AND room_id=",
                    RoomId,
                    " LIMIT 1"
                }));
            Bans.Remove(userId);
        }

        internal bool HasActiveTrade(RoomUser user)
        {
            return !user.IsBot && HasActiveTrade(user.GetClient().GetHabbo().Id);
        }

        internal bool HasActiveTrade(uint userId)
        {
            var array = ActiveTrades.ToArray();
            return array.Cast<Trade>().Any(trade => trade.ContainsUser(userId));
        }

        internal Trade GetUserTrade(uint userId)
        {
            var array = ActiveTrades.ToArray();
            return array.Cast<Trade>().FirstOrDefault(trade => trade.ContainsUser(userId));
        }

        internal void TryStartTrade(RoomUser userOne, RoomUser userTwo)
        {
            if (userOne == null || userTwo == null || userOne.IsBot || userTwo.IsBot || userOne.IsTrading ||
                userTwo.IsTrading || HasActiveTrade(userOne) || HasActiveTrade(userTwo))
                return;
            ActiveTrades.Add(new Trade(userOne.GetClient().GetHabbo().Id, userTwo.GetClient().GetHabbo().Id, RoomId));
        }

        internal void TryStopTrade(uint userId)
        {
            var userTrade = GetUserTrade(userId);
            if (userTrade == null)
                return;
            userTrade.CloseTrade(userId);
            ActiveTrades.Remove(userTrade);
        }

        internal void SetMaxUsers(int maxUsers)
        {
            UsersMax = maxUsers;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE rooms_data SET users_max = ",
                    maxUsers,
                    " WHERE id = ",
                    RoomId
                }));
        }

        internal void FlushSettings()
        {
            _mCycleEnded = true;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                GetRoomItemHandler().SaveFurniture(queryReactor, null);
            Tags.Clear();
            UsersWithRights.Clear();
            Bans.Clear();
            ActiveTrades.Clear();
            LoadedGroups.Clear();
            if (GotFreeze())
                _freeze = new Freeze(this);
            if (GotBanzai())
                _banzai = new BattleBanzai(this);
            if (GotSoccer())
                _soccer = new Soccer(this);
            if (_gameItemHandler != null)
                _gameItemHandler = new GameItemHandler(this);
        }

        internal void ReloadSettings()
        {
            var data = Azure.GetGame().GetRoomManager().GenerateRoomData(RoomId);
            InitializeFromRoomData(data);
        }

        internal void OnReload()
        {
            var roomUsers = GetRoomUserManager().GetRoomUsers();
            var userId = new Hashtable(GetRoomUserManager().UsersByUserId);
            var Username = new Hashtable(GetRoomUserManager().UsersByUserName);
            var primaryId = 0;
            var secondaryId = 0;
            GetRoomUserManager().BackupCounters(ref primaryId, ref secondaryId);
            FlushSettings();
            ReloadSettings();
            GetRoomUserManager().UpdateUserStats(roomUsers, userId, Username, primaryId, secondaryId);
            UpdateFurniture();
            GetGameMap().GenerateMaps(true);
        }

        internal void UpdateFurniture()
        {
            var list = new List<ServerMessage>();
            var array = GetRoomItemHandler().MFloorItems.Values.ToArray();
            var array2 = array;
            foreach (var roomItem in array2)
            {
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("UpdateRoomItemMessageComposer"));
                roomItem.Serialize(serverMessage);
                list.Add(serverMessage);
            }
            Array.Clear(array, 0, array.Length);
            var array3 = GetRoomItemHandler().MWallItems.Values.ToArray();
            var array4 = array3;
            foreach (var roomItem2 in array4)
            {
                var serverMessage2 =
                    new ServerMessage(LibraryParser.OutgoingRequest("UpdateRoomWallItemMessageComposer"));
                roomItem2.Serialize(serverMessage2);
                list.Add(serverMessage2);
            }
            Array.Clear(array3, 0, array3.Length);
            SendMessage(list);
        }

        internal bool CheckMute(GameClient session)
        {
            if (!MutedUsers.ContainsKey(session.GetHabbo().Id))
                return session.GetHabbo().Muted || RoomMuted;
            if (MutedUsers[session.GetHabbo().Id] >= Azure.GetUnixTimestamp())
                return true;
            MutedUsers.Remove(session.GetHabbo().Id);
            return session.GetHabbo().Muted || RoomMuted;
        }

        internal void AddChatlog(uint id, string message, bool banned)
        {
            var item = new Chatlog(id, message, Azure.GetUnixTimestamp(), false);
            RoomChat.Add(item);
        }

        internal void ResetGamemap(string newModelName, int wallHeight, int wallThick, int floorThick)
        {
            ModelName = newModelName;
            RoomData.ModelName = newModelName;
            RoomData.ResetModel();
            RoomData.WallHeight = wallHeight;
            RoomData.WallThickness = WallThickness;
            RoomData.FloorThickness = floorThick;
            _gameMap = new Gamemap(this);
        }

        private void InitializeFromRoomData(RoomData data)
        {
            Initialize(data.Id, data.Name, data.Description, data.Type, data.Owner, data.OwnerId, data.Category,
                data.State, data.TradeState, data.UsersMax, data.ModelName, data.Score, data.Tags, data.AllowPets,
                data.AllowPetsEating, data.AllowWalkthrough, data.Hidewall, data.Password, data.Wallpaper, data.Floor,
                data.Landscape, data, data.AllowRightsOverride, data.WallThickness, data.FloorThickness, data.Group,
                data.GameId, data.ChatType, data.ChatBalloon, data.ChatSpeed, data.ChatMaxDistance,
                data.ChatFloodProtection, data.WhoCanMute, data.WhoCanKick, data.WhoCanBan, data.GroupId, data.RoomChat,
                data.WordFilter, data.WallHeight);
        }

        private void Initialize(uint id, string rName, string description, string type, string owner, int ownerId,
            int category, int state, int tradeState, int usersMax, string modelName, int score,
            IEnumerable<string> pTags, int allowPets, int allowPetsEating, int allowWalkthrough, int hidewall,
            string password, string wallpaper, string floor, string landscape, RoomData roomData, bool rightOverride,
            int walltickness, int floorthickness, Guild group, int gameId, int chattype, int chatballoon, int chatspeed,
            int chatmaxdis, int chatprotection, int whomute, int whokick, int whoban, uint groupid,
            ConcurrentBag<Chatlog> chat, List<string> wordFilter, int wallHeight)
        {
            _mDisposed = false;
            RoomId = id;
            Name = rName;
            Description = description;
            Owner = owner;
            OwnerId = ownerId;
            Category = category;
            Type = type;
            State = state;
            TradeState = tradeState;
            UsersNow = 0;
            UsersMax = usersMax;
            ModelName = modelName;
            Score = score;
            _tagCount = 0;
            Tags = new ArrayList();
            foreach (string current in pTags)
            {
                _tagCount++;
                Tags.Add(current);
            }
            ChatType = chattype;
            ChatBalloon = chatballoon;
            ChatSpeed = chatspeed;
            ChatMaxDistance = chatmaxdis;
            ChatFloodProtection = chatprotection;
            AllowPets = allowPets;
            AllowPetsEating = allowPetsEating;
            AllowWalkthrough = allowWalkthrough;
            Hidewall = hidewall;
            Group = group;
            Password = password;
            Bans = new Dictionary<long, double>();
            MutedUsers = new Dictionary<uint, uint>();
            Wallpaper = wallpaper;
            Floor = floor;
            Landscape = landscape;
            ActiveTrades = new ArrayList();
            MutedBots = false;
            _mCycleEnded = false;
            RoomData = roomData;
            EveryoneGotRights = rightOverride;
            LoadedGroups = new Dictionary<uint, string>();
            _roomKick = new Queue();
            _idleTime = 0;
            RoomMuted = false;
            WallThickness = walltickness;
            FloorThickness = floorthickness;
            WallHeight = wallHeight;
            _gameMap = new Gamemap(this);
            _roomItemHandling = new RoomItemHandling(this);
            _roomUserManager = new RoomUserManager(this);
            RoomChat = chat;
            WordFilter = wordFilter;
            Event = Azure.GetGame().GetRoomEvents().GetEvent(id);
            WhoCanBan = whoban;
            WhoCanKick = whokick;
            WhoCanBan = whoban;
            GroupId = groupid;
            LoadRights();
            LoadMusic();
            LoadBans();
            InitUserBots();

            _roomThread = new Thread(StartRoomProcessing);
            _roomThread.Start();
            Azure.GetGame().GetRoomManager().QueueActiveRoomAdd(RoomData);
        }

        private void WorkRoomKickQueue()
        {
            if (_roomKick.Count <= 0)
                return;
            lock (_roomKick.SyncRoot)
            {
                while (_roomKick.Count > 0)
                {
                    var roomKick = (RoomKick) _roomKick.Dequeue();
                    var list = new List<RoomUser>();
                    foreach (
                        var current in
                            _roomUserManager.UserList.Values.Where(
                                current =>
                                    !current.IsBot && current.GetClient().GetHabbo().Rank < (ulong) roomKick.minrank))
                    {
                        if (roomKick.allert.Length > 0)
                            current.GetClient()
                                .SendNotif(string.Format("You have been kicked by an moderator: {0}", roomKick.allert));
                        list.Add(current);
                    }
                    foreach (var current2 in list)
                    {
                        GetRoomUserManager().RemoveUserFromRoom(current2.GetClient(), true, false);
                        current2.GetClient().CurrentRoomUserId = -1;
                    }
                }
            }
        }

        private void OnRoomCrash(Exception e)
        {
            Logging.LogThreadException(e.ToString(), string.Format("Room cycle task for room {0}", RoomId));
            Azure.GetGame().GetRoomManager().UnloadRoom(this, "Room crashed");
            _isCrashed = true;
        }

        private void Dispose()
        {
            if (_mDisposed)
                return;
            _mDisposed = true;
            _mCycleEnded = true;
            Azure.GetGame().GetRoomManager().QueueActiveRoomRemove(RoomData);
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                GetRoomItemHandler().SaveFurniture(queryReactor, null);
                queryReactor.RunFastQuery(string.Format("UPDATE rooms_data SET users_now=0 WHERE id = {0} LIMIT 1",
                    RoomId));
            }
            _processTimer.Dispose();
            _processTimer = null;
            _tagCount = 0;
            Tags.Clear();
            _roomUserManager.UserList.Clear();
            UsersWithRights.Clear();
            Bans.Clear();
            LoadedGroups.Clear();
            Chatlog result;
            while (RoomChat.TryTake(out result))
            {
            }
            GetWiredHandler().Destroy();
            foreach (var current in GetRoomItemHandler().MFloorItems.Values)
                current.Destroy();
            foreach (var current2 in GetRoomItemHandler().MWallItems.Values)
                current2.Destroy();
            ActiveTrades.Clear();
        }
    }
}