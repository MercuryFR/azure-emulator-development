using System;
using System.Text;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Rooms
{
    internal class DynamicRoomModel
    {
        internal int DoorX;
        internal int DoorY;
        internal double DoorZ;
        internal int DoorOrientation;
        internal string Heightmap;
        internal SquareState[][] SqState;
        internal int[][] SqFloorHeight;
        internal byte[][] SqSeatRot;
        internal char[][] SqChar;
        internal int MapSizeX;
        internal int MapSizeY;
        internal bool ClubOnly;
        internal bool HeightmapSerialized;
        private readonly Room _mRoom;

        private RoomModel _staticModel;
        private ServerMessage _serializedHeightmap;

        internal DynamicRoomModel(RoomModel pModel, Room room)
        {
            this._staticModel = pModel;
            this.DoorX = this._staticModel.DoorX;
            this.DoorY = this._staticModel.DoorY;
            this.DoorZ = this._staticModel.DoorZ;
            this.DoorOrientation = this._staticModel.DoorOrientation;
            this.Heightmap = this._staticModel.Heightmap;
            this.MapSizeX = this._staticModel.MapSizeX;
            this.MapSizeY = this._staticModel.MapSizeY;
            this.ClubOnly = this._staticModel.ClubOnly;
            this._mRoom = room;
            this.Generate();
        }

        internal void Generate()
        {
            this.SqState = new SquareState[this.MapSizeX][];
            for (int i = 0; i < this.MapSizeX; i++)
            {
                this.SqState[i] = new SquareState[this.MapSizeY];
            }
            this.SqFloorHeight = new int[this.MapSizeX][];
            for (int i = 0; i < this.MapSizeX; i++)
            {
                this.SqFloorHeight[i] = new int[this.MapSizeY];
            }
            this.SqSeatRot = new byte[this.MapSizeX][];
            for (int i = 0; i < this.MapSizeX; i++)
            {
                this.SqSeatRot[i] = new byte[this.MapSizeY];
            }
            this.SqChar = new char[this.MapSizeX][];
            for (int i = 0; i < this.MapSizeX; i++)
            {
                this.SqChar[i] = new char[this.MapSizeY];
            }
            checked
            {
                for (int i = 0; i < this.MapSizeY; i++)
                {
                    for (int j = 0; j < this.MapSizeX; j++)
                    {
                        if (j > this._staticModel.MapSizeX - 1 || i > this._staticModel.MapSizeY - 1)
                        {
                            this.SqState[j][i] = SquareState.Blocked;
                        }
                        else
                        {
                            this.SqState[j][i] = this._staticModel.SqState[j, i];
                            this.SqFloorHeight[j][i] = this._staticModel.SqFloorHeight[j, i];
                            this.SqSeatRot[j][i] = this._staticModel.SqSeatRot[j, i];
                            this.SqChar[j][i] = this._staticModel.SqChar[j, i];
                        }
                    }
                }
                this.HeightmapSerialized = false;
            }
        }

        internal void RefreshArrays()
        {
            this.Generate();
        }

        internal void SetUpdateState()
        {
            this.HeightmapSerialized = false;
        }

        internal ServerMessage GetHeightmap()
        {
            if (this.HeightmapSerialized)
            {
                return this._serializedHeightmap;
            }
            this._serializedHeightmap = this.SerializeHeightmap();
            this.HeightmapSerialized = true;
            return this._serializedHeightmap;
        }

        internal void AddX()
        {
            checked
            {
                this.MapSizeX++;
                this.RefreshArrays();
            }
        }

        internal void OpenSquare(int x, int y, double z)
        {
            if (z > 9.0)
            {
                z = 9.0;
            }
            if (z < 0.0)
            {
                z = 0.0;
            }
            this.SqFloorHeight[x][y] = checked((short)z);
            this.SqState[x][y] = SquareState.Open;
        }

        internal void AddY()
        {
            checked
            {
                this.MapSizeY++;
                this.RefreshArrays();
            }
        }

        internal void SetMapsize(int x, int y)
        {
            this.MapSizeX = x;
            this.MapSizeY = y;
            this.RefreshArrays();
        }

        internal void Destroy()
        {
            Array.Clear(this.SqState, 0, this.SqState.Length);
            Array.Clear(this.SqFloorHeight, 0, this.SqFloorHeight.Length);
            Array.Clear(this.SqSeatRot, 0, this.SqSeatRot.Length);
            this._staticModel = null;
            this.Heightmap = null;
            this.SqState = null;
            this.SqFloorHeight = null;
            this.SqSeatRot = null;
        }

        private ServerMessage SerializeHeightmap()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("FloorMapMessageComposer"));
            serverMessage.AppendBool(true);
            serverMessage.AppendInteger(this._mRoom.WallHeight);
            var stringBuilder = new StringBuilder();
            checked
            {
                for (int i = 0; i < this.MapSizeY; i++)
                {
                    for (int j = 0; j < this.MapSizeX; j++)
                    {
                        try
                        {
                            stringBuilder.Append(this.SqChar[j][i].ToString());
                        }
                        catch (Exception)
                        {
                            stringBuilder.Append("0");
                        }
                    }
                    stringBuilder.Append(Convert.ToChar(13));
                }
                string s = stringBuilder.ToString();
                serverMessage.AppendString(s);
                return serverMessage;
            }
        }
    }
}