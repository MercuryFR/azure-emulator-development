using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.HabboHotel.Rooms
{
    internal class Chatlog
    {
        internal uint UserId;
        internal string Message;
        internal double Timestamp;
        internal bool IsSaved;

        internal Chatlog(uint user, string msg, double time, bool fromDatabase = false)
        {
            this.UserId = user;
            this.Message = msg;
            this.Timestamp = time;
            this.IsSaved = fromDatabase;
        }

        internal void Save(uint roomId)
        {
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    "REPLACE INTO users_chatlogs (user_id, room_id, timestamp, message) VALUES (@user, @room, @time, @message)");
                queryReactor.AddParameter("user", this.UserId);
                queryReactor.AddParameter("room", roomId);
                queryReactor.AddParameter("time", this.Timestamp);
                queryReactor.AddParameter("message", this.Message);
                queryReactor.RunQuery();
            }
        }
    }
}