namespace Azure.HabboHotel.Rooms
{
    internal class RoomEvent
    {
        internal string Name;
        internal string Description;
        internal int Time;
        internal uint RoomId;
        internal int Category;

        internal RoomEvent(uint roomId, string name, string description, int time = 0, int category = 1)
        {
            this.RoomId = roomId;
            this.Name = name;
            this.Description = description;
            this.Time = ((time == 0) ? checked(Azure.GetUnixTimestamp() + 7200) : time);
            this.Category = category;
        }

        internal bool HasExpired
        {
            get
            {
                return Azure.GetUnixTimestamp() > this.Time;
            }
        }
    }
}