using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Text;
using Azure.Configuration;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.PathFinding;
using Azure.HabboHotel.Pathfinding;
using Azure.HabboHotel.Rooms.Games;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Rooms
{
    internal class Gamemap
    {
        internal bool DiagonalEnabled;
        internal bool GotPublicPool;
        internal ServerMessage SerializedFloormap;
        internal HashSet<Point> WalkableList;
        private Room _room;
        private HybridDictionary _userMap;

        public Gamemap(Room room)
        {
            _room = room;
            DiagonalEnabled = true;
            StaticModel = Azure.GetGame().GetRoomManager().GetModel(room.ModelName, room.RoomId);
            if (StaticModel == null)
                throw new Exception(string.Format("No modeldata found for roomID {0}", room.RoomId));
            Model = new DynamicRoomModel(StaticModel, room);
            CoordinatedItems = new HybridDictionary();
            GotPublicPool = room.RoomData.Model.GotPublicPool;
            GameMap = new byte[Model.MapSizeX, Model.MapSizeY];
            ItemHeightMap = new double[Model.MapSizeX, Model.MapSizeY];
            _userMap = new HybridDictionary();
            WalkableList = GetWalkablePoints();
            GuildGates = new Dictionary<Point, RoomItem>();
        }

        internal DynamicRoomModel Model { get; private set; }
        internal RoomModel StaticModel { get; private set; }
        internal byte[,] EffectMap { get; private set; }
        internal HybridDictionary CoordinatedItems { get; private set; }
        internal Dictionary<Point, RoomItem> GuildGates;
        internal byte[,] GameMap { get; private set; }
        internal double[,] ItemHeightMap { get; private set; }

        internal static bool CanWalk(byte pState, bool pOverride)
        {
            return pOverride || pState == 3 || pState == 1;
        }

        internal static Dictionary<int, ThreeDCoord> GetAffectedTiles(int length, int width,
            int posX, int posY, int rotation)
        {
            var x = 0;
            var pointList = new Dictionary<int, ThreeDCoord>();
            if (length == 1 && width == 1)
                pointList.Add(x++, new ThreeDCoord(posX, posY, 0));
            if (length > 1)
                switch (rotation)
                {
                    case 4:
                    case 0:
                        for (var i = 0; i < length; i++)
                        {
                            pointList.Add(x++, new ThreeDCoord(posX, posY + i, i));
                            for (var j = 1; j < width; j++)
                                pointList.Add(x++, new ThreeDCoord(posX + j, posY + i, (i < j) ? j : i));
                        }
                        break;
                    case 6:
                    case 2:
                        for (var i = 0; i < length; i++)
                        {
                            pointList.Add(x++, new ThreeDCoord(posX + i, posY, i));
                            for (var j = 1; j < width; j++)
                                pointList.Add(x++, new ThreeDCoord(posX + i, posY + j, (i < j) ? j : i));
                        }
                        break;
                }
            if (width <= 1)
                return pointList;
            switch (rotation)
            {
                case 4:
                case 0:
                    for (var i = 0; i < width; i++)
                    {
                        pointList.Add(x++, new ThreeDCoord(posX + i, posY, i));
                        for (var j = 1; j < length; j++)
                            pointList.Add(x++, new ThreeDCoord(posX + i, posY + j, (i < j) ? j : i));
                    }
                    break;
                case 6:
                case 2:
                    for (var i = 0; i < width; i++)
                    {
                        pointList.Add(x++, new ThreeDCoord(posX, posY + i, i));
                        for (var j = 1; j < length; j++)
                            pointList.Add(x++, new ThreeDCoord(posX + j, posY + i, (i < j) ? j : i));
                    }
                    break;
            }

            return pointList;
        }

        internal static bool TilesTouching(int x1, int y1, int x2, int y2)
        {
            return checked(Math.Abs(x1 - x2) <= 1 && Math.Abs(y1 - y2) <= 1) || (x1 == x2 && y1 == y2);
        }

        internal static int TileDistance(int x1, int y1, int x2, int y2)
        {
            return checked(Math.Abs(x1 - x2) + Math.Abs(y1 - y2));
        }

        internal void AddUserToMap(RoomUser user, Point coord)
        {
            if (_userMap.Contains(coord))
            {
                ((List<RoomUser>)_userMap[coord]).Add(user);
                return;
            }
            var list = new List<RoomUser> { user };
            _userMap.Add(coord, list);
        }

        internal void TeleportToItem(RoomUser user, RoomItem item)
        {
            GameMap[user.X, user.Y] = user.SqState;
            UpdateUserMovement(new Point(user.Coordinate.X, user.Coordinate.Y),
                new Point(item.Coordinate.X, item.Coordinate.Y), user);
            user.X = item.X;
            user.Y = item.Y;
            user.Z = item.Z;
            user.SqState = GameMap[item.X, item.Y];
            GameMap[user.X, user.Y] = 1;
            user.RotBody = item.Rot;
            user.RotHead = item.Rot;
            user.GoalX = user.X;
            user.GoalY = user.Y;
            user.SetStep = false;
            user.IsWalking = false;
            user.UpdateNeeded = true;
        }

        internal void UpdateUserMovement(Point oldCoord, Point newCoord, RoomUser user)
        {
            RemoveUserFromMap(user, oldCoord);
            AddUserToMap(user, newCoord);
        }

        internal void RemoveUserFromMap(RoomUser user, Point coord)
        {
            if (_userMap.Contains(coord))
                ((List<RoomUser>)_userMap[coord]).Remove(user);
        }

        internal bool MapGotUser(Point coord)
        {
            return GetRoomUsers(coord).Any();
        }

        internal List<RoomUser> GetRoomUsers(Point coord)
        {
            if (_userMap.Contains(coord))
                return (List<RoomUser>)_userMap[coord];
            return new List<RoomUser>();
        }

        internal Point GetRandomWalkableSquare()
        {
            HashSet<Point> list = WalkableList;
            if (!list.Any())
                return new Point(0, 0);
            checked
            {
                int randomNumber = new Random().Next(0, list.Count);
                int num = 0;
                foreach (Point current in list)
                {
                    if (num == randomNumber)
                        return current;
                    num++;
                }
                return new Point(0, 0);
            }
        }

        internal string GenerateMapDump()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Game map:");
            checked
            {
                for (int i = 0; i < Model.MapSizeY; i++)
                {
                    var stringBuilder2 = new StringBuilder();
                    for (int j = 0; j < Model.MapSizeX; j++)
                        stringBuilder2.Append(GameMap[j, i].ToString());
                    stringBuilder.AppendLine(stringBuilder2.ToString());
                }
                stringBuilder.AppendLine();
                stringBuilder.AppendLine("Item height map:");
                for (int k = 0; k < Model.MapSizeY; k++)
                {
                    var stringBuilder3 = new StringBuilder();
                    for (int l = 0; l < Model.MapSizeX; l++)
                        stringBuilder3.AppendFormat("[{0}]", ItemHeightMap[l, k]);
                    stringBuilder.AppendLine(stringBuilder3.ToString());
                }
                stringBuilder.AppendLine();
                stringBuilder.AppendLine("Static data:");
                for (int m = 0; m < Model.MapSizeY; m++)
                {
                    var stringBuilder4 = new StringBuilder();
                    for (int n = 0; n < Model.MapSizeX; n++)
                        stringBuilder4.AppendFormat("[{0}]", Model.SqState[n][m]);
                    stringBuilder.AppendLine(stringBuilder4.ToString());
                }
                stringBuilder.AppendLine();
                stringBuilder.AppendLine("Static data height:");
                for (int num = 0; num < Model.MapSizeY; num++)
                {
                    var stringBuilder5 = new StringBuilder();
                    for (int num2 = 0; num2 < Model.MapSizeX; num2++)
                        stringBuilder5.AppendFormat("[{0}]", Model.SqFloorHeight[num2][num]);
                    stringBuilder.AppendLine(stringBuilder5.ToString());
                }
                stringBuilder.AppendLine();
                stringBuilder.AppendLine("Pool map:");
                for (int num3 = 0; num3 < Model.MapSizeY; num3++)
                {
                    var stringBuilder6 = new StringBuilder();
                    for (int num4 = 0; num4 < Model.MapSizeX; num4++)
                        stringBuilder6.AppendFormat("[{0}]", EffectMap[num4, num3]);
                    stringBuilder.AppendLine(stringBuilder6.ToString());
                }
                stringBuilder.AppendLine();
                return stringBuilder.ToString();
            }
        }

        internal void AddToMap(RoomItem item)
        {
            AddItemToMap(item, true);
        }

        internal void UpdateMapForItem(RoomItem item)
        {
            RemoveFromMap(item);
            AddToMap(item);
        }

        internal void GenerateMaps(bool checkLines = true)
        {
            int num = 0;
            int num2 = 0;
            CoordinatedItems = new HybridDictionary();
            if (checkLines)
            {
                RoomItem[] array = _room.GetRoomItemHandler().MFloorItems.Values.ToArray();
                RoomItem[] array2 = array;
                foreach (RoomItem roomItem in array2)
                {
                    if (roomItem.X > Model.MapSizeX && roomItem.X > num)
                        num = roomItem.X;
                    if (roomItem.Y > Model.MapSizeY && roomItem.Y > num2)
                        num2 = roomItem.Y;
                }
                Array.Clear(array, 0, array.Length);
            }
            RoomItem[] array3;
            RoomItem[] array4;
            checked
            {
                if (num2 > Model.MapSizeY - 1 || num > Model.MapSizeX - 1)
                {
                    if (num < Model.MapSizeX)
                        num = Model.MapSizeX;
                    if (num2 < Model.MapSizeY)
                        num2 = Model.MapSizeY;
                    Model.SetMapsize(num + 7, num2 + 7);
                    GenerateMaps(false);
                    return;
                }
                if (num != StaticModel.MapSizeX || num2 != StaticModel.MapSizeY)
                {
                    EffectMap = new byte[Model.MapSizeX, Model.MapSizeY];
                    GameMap = new byte[Model.MapSizeX, Model.MapSizeY];
                    ItemHeightMap = new double[Model.MapSizeX, Model.MapSizeY];
                    for (int j = 0; j < Model.MapSizeY; j++)
                        for (int k = 0; k < Model.MapSizeX; k++)
                        {
                            GameMap[k, j] = 0;
                            EffectMap[k, j] = 0;
                            if (k == Model.DoorX && j == Model.DoorY)
                                GameMap[k, j] = 3;
                            else
                                switch (Model.SqState[k][j])
                                {
                                    case SquareState.Open:
                                        GameMap[k, j] = 1;
                                        break;
                                    case SquareState.Seat:
                                        GameMap[k, j] = 2;
                                        break;
                                    case SquareState.Pool:
                                        EffectMap[k, j] = 6;
                                        break;
                                }
                    }
                    if (GotPublicPool)
                        for (int l = 0; l < StaticModel.MapSizeY; l++)
                            for (int m = 0; m < StaticModel.MapSizeX; m++)
                                if (StaticModel.MRoomModelfx[m, l] != 0)
                                    EffectMap[m, l] = StaticModel.MRoomModelfx[m, l];
                }
                else
                {
                    EffectMap = new byte[Model.MapSizeX, Model.MapSizeY];
                    GameMap = new byte[Model.MapSizeX, Model.MapSizeY];
                    ItemHeightMap = new double[Model.MapSizeX, Model.MapSizeY];
                    for (int n = 0; n < Model.MapSizeY; n++)
                    {
                        for (int num3 = 0; num3 < Model.MapSizeX; num3++)
                        {
                            GameMap[num3, n] = 0;
                            EffectMap[num3, n] = 0;
                            if (num3 == Model.DoorX && n == Model.DoorY)
                                GameMap[num3, n] = 3;
                            else
                                switch (Model.SqState[num3][n])
                                {
                                    case SquareState.Open:
                                        GameMap[num3, n] = 1;
                                        break;
                                    case SquareState.Seat:
                                        GameMap[num3, n] = 2;
                                        break;
                                    case SquareState.Pool:
                                        EffectMap[num3, n] = 6;
                                        break;
                                }
                        }
                    }
                    if (GotPublicPool)
                        for (int num4 = 0; num4 < StaticModel.MapSizeY; num4++)
                            for (int num5 = 0; num5 < StaticModel.MapSizeX; num5++)
                                if (StaticModel.MRoomModelfx[num5, num4] != 0)
                                    EffectMap[num5, num4] = StaticModel.MRoomModelfx[num5, num4];
                }
                array3 = _room.GetRoomItemHandler().MFloorItems.Values.ToArray();
                array4 = array3;
            }
            foreach (RoomItem t in array4.Where(t => !AddItemToMap(t)))
                break;
            Array.Clear(array3, 0, array3.Length);
            if (_room.AllowWalkthrough == 0)
            {
                foreach (RoomUser current in _room.GetRoomUserManager().UserList.Values)
                {
                    current.SqState = GameMap[current.X, current.Y];
                    GameMap[current.X, current.Y] = 0;
                }
            }
            GameMap[Model.DoorX, Model.DoorY] = 3;
        }

        internal void AddCoordinatedItem(RoomItem item, Point coord)
        {
            List<RoomItem> list;
            if (!CoordinatedItems.Contains(coord))
            {
                list = new List<RoomItem> { item };
                CoordinatedItems.Add(coord, list);
                return;
            }
            list = (List<RoomItem>)CoordinatedItems[coord];
            if (list.Contains(item))
                return;
            list.Add(item);
            CoordinatedItems[coord] = list;
        }

        internal List<RoomItem> GetCoordinatedItems(Point coord)
        {
            var point = new Point(coord.X, coord.Y);
            if (CoordinatedItems.Contains(point))
                return (List<RoomItem>)CoordinatedItems[point];
            return new List<RoomItem>();
        }

        internal bool RemoveCoordinatedItem(RoomItem item, Point coord)
        {
            var point = new Point(coord.X, coord.Y);
            if (!CoordinatedItems.Contains(point))
                return false;
            ((List<RoomItem>)CoordinatedItems[point]).Remove(item);
            return true;
        }

        internal bool RemoveFromMap(RoomItem item, bool handleGameItem)
        {
            RemoveSpecialItem(item);
            if (_room.GotSoccer())
                _room.GetSoccer().OnGateRemove(item);
            bool result = false;
            foreach (var current in item.GetCoords.Where(current => RemoveCoordinatedItem(item, current)))
                result = true;
            var hybridDictionary = new HybridDictionary();
            foreach (var current2 in item.GetCoords)
            {
                var point = new Point(current2.X, current2.Y);
                if (CoordinatedItems.Contains(point))
                {
                    var value = (List<RoomItem>)CoordinatedItems[point];
                    if (!hybridDictionary.Contains(current2))
                        hybridDictionary.Add(current2, value);
                }
                SetDefaultValue(current2.X, current2.Y);
            }
            foreach (Point point2 in hybridDictionary.Keys)
            {
                var list = (List<RoomItem>)hybridDictionary[point2];
                foreach (RoomItem current3 in list)
                    ConstructMapForItem(current3, point2);
            }
            if (GuildGates.ContainsKey(item.Coordinate))
                GuildGates.Remove(item.Coordinate);
            _room.GetRoomItemHandler().OnHeightmapUpdate(hybridDictionary.Keys);
            hybridDictionary.Clear();
            hybridDictionary = null;

            return result;
        }

        internal bool RemoveFromMap(RoomItem item)
        {
            return RemoveFromMap(item, true);
        }

        internal bool AddItemToMap(RoomItem item, bool handleGameItem, bool newItem = true)
        {
            if (handleGameItem)
            {
                AddSpecialItems(item);
                InteractionType interactionType = item.GetBaseItem().InteractionType;
                if (interactionType != InteractionType.roller)
                {
                    switch (interactionType)
                    {
                        case InteractionType.footballgoalgreen:
                        case InteractionType.footballcountergreen:
                        case InteractionType.banzaigategreen:
                        case InteractionType.banzaiscoregreen:
                        case InteractionType.freezegreencounter:
                        case InteractionType.freezegreengate:
                            _room.GetGameManager().AddFurnitureToTeam(item, Team.green);
                            break;
                        case InteractionType.footballgoalyellow:
                        case InteractionType.footballcounteryellow:
                        case InteractionType.banzaigateyellow:
                        case InteractionType.banzaiscoreyellow:
                        case InteractionType.freezeyellowcounter:
                        case InteractionType.freezeyellowgate:
                            _room.GetGameManager().AddFurnitureToTeam(item, Team.yellow);
                            break;
                        case InteractionType.footballgoalblue:
                        case InteractionType.footballcounterblue:
                        case InteractionType.banzaigateblue:
                        case InteractionType.banzaiscoreblue:
                        case InteractionType.freezebluecounter:
                        case InteractionType.freezebluegate:
                            _room.GetGameManager().AddFurnitureToTeam(item, Team.blue);
                            break;
                        case InteractionType.footballgoalred:
                        case InteractionType.footballcounterred:
                        case InteractionType.banzaigatered:
                        case InteractionType.banzaiscorered:
                        case InteractionType.freezeredcounter:
                        case InteractionType.freezeredgate:
                            _room.GetGameManager().AddFurnitureToTeam(item, Team.red);
                            break;
                        case InteractionType.freezeexit:
                            _room.GetFreeze().ExitTeleport = item;
                            break;
                        case InteractionType.gld_gate:
                            {
                                if (!GuildGates.ContainsKey(item.Coordinate))
                                    GuildGates.Add(item.Coordinate, item);
                                break;
                            }
                    }
                }
                else
                    if (!_room.GetRoomItemHandler().MRollers.ContainsKey(item.Id))
                        _room.GetRoomItemHandler().MRollers.Add(item.Id, item);
            }
            if (item.GetBaseItem().Type != 's')
                return true;
            foreach (var coord in item.GetCoords.Select(current => new Point(current.X, current.Y)))
                AddCoordinatedItem(item, coord);
            checked
            {
                if (item.X > Model.MapSizeX - 1)
                {
                    Model.AddX();
                    GenerateMaps(true);
                    return false;
                }
                if (item.Y <= Model.MapSizeY - 1)
                    return item.GetCoords.All(current2 => ConstructMapForItem(item, current2));
                Model.AddY();
                GenerateMaps(true);
                return false;
            }
        }

        internal bool CanWalk(int x, int y, bool Override, uint HorseId = 0u)
        {
            return _room.AllowWalkthrough == 1 || Override || _room.GetRoomUserManager().GetUserForSquare(x, y) == null ||
                   _room.AllowWalkthrough != 0;
        }

        internal bool AddItemToMap(RoomItem item, bool newItem = true)
        {
            return AddItemToMap(item, true, newItem);
        }

        internal byte GetFloorStatus(Point coord)
        {
            if (coord.X > GameMap.GetUpperBound(0) || coord.Y > GameMap.GetUpperBound(1))
                return 1;
            return GameMap[coord.X, coord.Y];
        }

        internal double GetHeightForSquareFromData(Point coord)
        {
            try
            {
                var high = Model.SqFloorHeight.GetUpperBound(0);
                if (coord.X > high || coord.Y > Model.SqFloorHeight[high].GetUpperBound(0))
                    return 1.0;
                return Model.SqFloorHeight[coord.X][coord.Y];
            }
            catch (Exception)
            {
                return 1.0;
            }
        }

        internal bool CanRollItemHere(int x, int y)
        {
            return ValidTile(x, y) && Model.SqState[x][y] != SquareState.Blocked;
        }

        internal bool SquareIsOpen(int x, int y, bool pOverride)
        {
            return checked(Model.MapSizeX - 1 >= x && Model.MapSizeY - 1 >= y) && CanWalk(GameMap[x, y], pOverride);
        }

        internal bool IsValidStep3(RoomUser User, Vector2D From, Vector2D To, bool EndOfPath, bool Override, GameClients.GameClient Client)
        {
            Point square = new Point(To.X, To.Y);
            if (GuildGates.ContainsKey(square))
            {
                var roomItem = GuildGates[square];
                var guildId = roomItem.GroupId;
                if (guildId > 0)
                    if (User != null && User.GetClient().GetHabbo() != null && User.GetClient().GetHabbo().MyGroups != null &&
                        User.GetClient().GetHabbo().MyGroups.Contains(guildId))
                    {
                        roomItem.ExtraData = "1";
                        roomItem.UpdateState();
                        return true;
                    }
            }

            if (!ValidTile(To.X, To.Y))
            {
                return false;
            }

            if (Override)
            {
                return true;
            }

            if (((this.GameMap[To.X, To.Y] == 3 && !EndOfPath) || this.GameMap[To.X, To.Y] == 0 || (this.GameMap[To.X, To.Y] == 2 && !EndOfPath)))
            {
                User.Path.Clear();
                User.PathRecalcNeeded = false;

                return false;
            }

            var Userx = _room.GetRoomUserManager().GetUserForSquare(To.X, To.Y);
            if (Userx != null)
            {
                if (!Userx.IsWalking && EndOfPath)
                    return false;
            }

            double HeightDiff = this.SqAbsoluteHeight(To.X, To.Y) - this.SqAbsoluteHeight(From.X, From.Y);

            if (HeightDiff > 1.5)
            {
                return false;
            }

            return true;
        }

        internal bool IsValidStep2(RoomUser user, Point @from, Point to, bool endOfPath, bool Override)
        {
            if (GuildGates.ContainsKey(to))
            {
                var roomItem = GuildGates[to];
                var guildId = roomItem.GroupId;
                if (guildId > 0)
                    if (user != null && user.GetClient().GetHabbo() != null && user.GetClient().GetHabbo().MyGroups != null &&
                        user.GetClient().GetHabbo().MyGroups.Contains(guildId))
                    {
                        roomItem.ExtraData = "1";
                        roomItem.UpdateState();
                        return true;
                    }
            }
            if (!ValidTile2(to.X, to.Y))
                return false;
            if (Override)
                return true;
            if (GameMap[to.X, to.Y] == 3 && !endOfPath || GameMap[to.X, to.Y] == 0 ||
                GameMap[to.X, to.Y] == 2 && !endOfPath ||
                SqAbsoluteHeight(to.X, to.Y) - SqAbsoluteHeight(@from.X, @from.Y) > 1.5)
                return false;
            RoomUser userForSquare = _room.GetRoomUserManager().GetUserForSquare(to.X, to.Y);
            if (userForSquare != null && endOfPath && _room.AllowWalkthrough == 0)
            {
                user.HasPathBlocked = true;
                user.Path.Clear();
                user.IsWalking = false;
                user.RemoveStatus("mv");
                _room.GetRoomUserManager().UpdateUserStatus(user, false);
                if (user.RidingHorse && !user.IsPet && !user.IsBot)
                {
                    RoomUser roomUserByVirtualId = _room.GetRoomUserManager().GetRoomUserByVirtualId(Convert.ToInt32(user.HorseId));
                    roomUserByVirtualId.IsWalking = false;
                    roomUserByVirtualId.RemoveStatus("mv");
                    var message = new ServerMessage(LibraryParser.OutgoingRequest("UpdateUserStatusMessageComposer"));
                    message.AppendInteger(1);
                    roomUserByVirtualId.SerializeStatus(message, "");
                    user.GetClient().GetHabbo().CurrentRoom.SendMessage(message);
                }
            }
            else if (userForSquare != null && _room.AllowWalkthrough == 0 && !userForSquare.IsWalking)
                return false;
            user.HasPathBlocked = false;
            return true;
        }

        internal bool AntiChoques(int x, int y, RoomUser user)
        {
            RoomUser roomUser;
            _room.GetRoomUserManager().ToSet.TryGetValue(new Point(x, y), out roomUser);
            return (roomUser == null || roomUser == user);
        }

        internal Point getRandomWalkableSquare()
        {
            var walkableSquares = new List<Point>();
            for (int y = 0; y < GameMap.GetUpperBound(1) - 1; y++)
                for (int x = 0; x < GameMap.GetUpperBound(0) - 1; x++)
                    if (StaticModel.DoorX != x && StaticModel.DoorY != y && GameMap[x, y] == 1)
                        walkableSquares.Add(new Point(x, y));
            int RandomNumber = Azure.GetRandomNumber(0, walkableSquares.Count);
            int i = 0;
            foreach (Point coord in walkableSquares)
            {
                if (i == RandomNumber)
                    return coord;
                i++;
            }
            return new Point(0, 0);
        }

        internal bool IsValidStep(RoomUser user, Vector2D @from, Vector2D to, bool endOfPath, bool Override)
        {
            if (user == null)
                return false;
            var square = new Point(to.X, to.Y);
            if (user.IsBot == false && user.GetClient() != null)
                if (GuildGates.ContainsKey(square))
                {
                    uint guildId = GuildGates[square].GroupId;
                    if (guildId > 0)
                        if (user.GetClient().GetHabbo() != null && user.GetClient().GetHabbo().MyGroups.Any() &&
                            user.GetClient().GetHabbo().MyGroups.Contains(guildId))
                            return true;
                }
            if (!ValidTile(to.X, to.Y))
                return false;
            if (Override)
                return true;
            if (GameMap[to.X, to.Y] == 3 && !endOfPath || GameMap[to.X, to.Y] == 0 ||
                GameMap[to.X, to.Y] == 2 && !endOfPath ||
                SqAbsoluteHeight(to.X, to.Y) - SqAbsoluteHeight(@from.X, @from.Y) > 1.5)
                return false;
            RoomUser userForSquare = _room.GetRoomUserManager().GetUserForSquare(to.X, to.Y);
            if (userForSquare != null && endOfPath && _room.AllowWalkthrough == 0)
            {
                user.HasPathBlocked = true;
                user.Path.Clear();
                user.IsWalking = false;
                user.RemoveStatus("mv");
                _room.GetRoomUserManager().UpdateUserStatus(user, false);
                if (!user.RidingHorse || user.IsPet || user.IsBot)
                    return true;
                RoomUser roomUserByVirtualId = _room.GetRoomUserManager().GetRoomUserByVirtualId(Convert.ToInt32(user.HorseId));
                roomUserByVirtualId.IsWalking = false;
                roomUserByVirtualId.RemoveStatus("mv");
                var message = new ServerMessage(LibraryParser.OutgoingRequest("UpdateUserStatusMessageComposer"));
                message.AppendInteger(1);
                roomUserByVirtualId.SerializeStatus(message, "");
                user.GetClient().GetHabbo().CurrentRoom.SendMessage(message);
            }
            else if (userForSquare != null && _room.AllowWalkthrough == 0 && !userForSquare.IsWalking)
                return false;
            return true;
        }

        internal bool ValidTile2(int x, int y)
        {
            if (!ValidTile(x, y))
            {
                return false;
            }
            bool result;
            try
            {
                result = (Model.SqState[x][y] == SquareState.Open);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        internal bool ItemCanBePlacedHere(int x, int y)
        {
            return checked(Model.MapSizeX - 1 >= x && Model.MapSizeY - 1 >= y) && (x != Model.DoorX || y != Model.DoorY) &&
                   GameMap[x, y] == 1;
        }

        internal double SqAbsoluteHeight(int x, int y)
        {
            var point = new Point(x, y);
            if (!CoordinatedItems.Contains(point))
            {
                return Model.SqFloorHeight[x][y];
            }
            var itemsOnSquare = (List<RoomItem>)CoordinatedItems[point];
            return SqAbsoluteHeight(x, y, itemsOnSquare);
        }

        internal double SqAbsoluteHeight(int x, int y, List<RoomItem> itemsOnSquare)
        {
            double result;
            try
            {
                var highestStack = 0.0;
                var deduct = false;
                var deductable = 0.0;
                foreach (
                    var item in
                        itemsOnSquare.Where(item => item != null && item.GetBaseItem() != null)
                            .Where(item => item.TotalHeight > highestStack))
                {
                    if (item.GetBaseItem().IsSeat || item.GetBaseItem().InteractionType == InteractionType.bed)
                    {
                        deduct = true;
                        deductable = item.GetBaseItem().Height;
                    }
                    else
                        deduct = false;

                    if (item.GetBaseItem().StackMultipler)
                    {
                        if (string.IsNullOrEmpty(item.ExtraData))
                            item.ExtraData = "0";

                        var itemHeight = item.GetBaseItem().ToggleHeight[int.Parse(item.ExtraData)];
                        highestStack = itemHeight;
                    }
                    else
                        highestStack = item.TotalHeight;
                }

                double floorHeight = Model.SqFloorHeight[x][y];
                var stackHeight = highestStack - Model.SqFloorHeight[x][y];

                if (deduct)
                    stackHeight -= deductable;

                if (stackHeight < 0)
                    stackHeight = 0;

                return (floorHeight + stackHeight);
            }
            catch (Exception pException)
            {
                Logging.HandleException(pException, "Room.SqAbsoluteHeight");
                result = 0.0;
            }
            return result;
        }

        internal bool ValidTile(int x, int y)
        {
            return x > 0 && y > 0 && x < Model.MapSizeX && y < Model.MapSizeY && !SquareHasUsers(x, y);
        }

        internal List<RoomItem> GetRoomItemForSquare(int pX, int pY)
        {
            var point = new Point(pX, pY);
            var list = new List<RoomItem>();
            if (!CoordinatedItems.Contains(point))
            {
                return list;
            }
            var list2 = (List<RoomItem>)CoordinatedItems[point];
            list.AddRange(list2.Where(current => current.Coordinate.X == point.X && current.Coordinate.Y == point.Y));
            return list;
        }

        internal List<RoomItem> GetAllRoomItemForSquare(int pX, int pY)
        {
            var point = new Point(pX, pY);
            var list = new List<RoomItem>();
            if (!CoordinatedItems.Contains(point))
            {
                return list;
            }
            var list2 = (List<RoomItem>)CoordinatedItems[point];
            foreach (RoomItem current in list2.Where(current => !list.Contains(current)))
            {
                list.Add(current);
            }
            return list;
        }

        internal bool SquareHasUsers(int x, int y)
        {
            return MapGotUser(new Point(x, y));
        }

        internal void Destroy()
        {
            _userMap.Clear();
            Model.Destroy();
            CoordinatedItems.Clear();
            Array.Clear(GameMap, 0, GameMap.Length);
            Array.Clear(EffectMap, 0, EffectMap.Length);
            Array.Clear(ItemHeightMap, 0, ItemHeightMap.Length);
            _userMap = null;
            GameMap = null;
            EffectMap = null;
            ItemHeightMap = null;
            CoordinatedItems = null;
            Model = null;
            _room = null;
            StaticModel = null;
        }

        internal RoomItem GetHighestItemForSquare(int x, int y, out double z, RoomItem exception = null)
        {
            RoomItem roomItem = null;
            double num = -1.0;
            double num2 = 0.0;
            foreach (RoomItem current in GetRoomItemForSquare(x, y))
            {
                if (current.Z > num)
                {
                    num = current.Z;
                    num2 = current.GetBaseItem().Height;
                    roomItem = current;
                }
                if (exception == null || exception != roomItem)
                {
                    continue;
                }
                num = -1.0;
                num2 = 0.0;
                roomItem = null;
            }
            z = num + num2;
            return roomItem;
        }

        internal ServerMessage GetNewHeightmap()
        {
            if (SerializedFloormap != null)
            {
                return SerializedFloormap;
            }
            SerializedFloormap = NewHeightMap();
            return SerializedFloormap;
        }

        private static bool IsSoccerGoal(InteractionType type)
        {
            return type == InteractionType.footballgoalblue || type == InteractionType.footballgoalgreen ||
                   type == InteractionType.footballgoalred || type == InteractionType.footballgoalyellow;
        }

        private void SetDefaultValue(int x, int y)
        {
            GameMap[x, y] = 0;
            EffectMap[x, y] = 0;
            ItemHeightMap[x, y] = 0.0;
            if (x == Model.DoorX && y == Model.DoorY)
            {
                GameMap[x, y] = 3;
                return;
            }
            if (Model.SqState[x][y] == SquareState.Open)
            {
                GameMap[x, y] = 1;
                return;
            }
            if (Model.SqState[x][y] == SquareState.Seat)
            {
                GameMap[x, y] = 2;
            }
        }

        private HashSet<Point> GetWalkablePoints()
        {
            var list = new HashSet<Point>();
            checked
            {
                for (int i = 0; i < GameMap.GetUpperBound(1) - 1; i++)
                {
                    for (int j = 0; j < GameMap.GetUpperBound(0) - 1; j++)
                    {
                        if (StaticModel.DoorX != j && StaticModel.DoorY != i && GameMap[j, i] == 0)
                        {
                            list.Add(new Point(j, i));
                        }
                    }
                }
                return list;
            }
        }

        private bool ConstructMapForItem(RoomItem item, Point coord)
        {
            try
            {
                checked
                {
                    if (coord.X > Model.MapSizeX - 1)
                    {
                        Model.AddX();
                        GenerateMaps(true);
                        return false;
                    }
                    if (coord.Y > Model.MapSizeY - 1)
                    {
                        Model.AddY();
                        GenerateMaps(true);
                        return false;
                    }
                    if (Model.SqState[coord.X][coord.Y] == SquareState.Blocked)
                    {
                        Model.OpenSquare(coord.X, coord.Y, item.Z);
                        Model.SetUpdateState();
                    }
                }
                if (ItemHeightMap[coord.X, coord.Y] <= item.TotalHeight)
                {
                    ItemHeightMap[coord.X, coord.Y] = item.TotalHeight - Model.SqFloorHeight[item.X][item.Y];
                    EffectMap[coord.X, coord.Y] = 0;
                    InteractionType interactionType = item.GetBaseItem().InteractionType;
                    if (interactionType != InteractionType.pool)
                    {
                        switch (interactionType)
                        {
                            case InteractionType.iceskates:
                                EffectMap[coord.X, coord.Y] = 3;
                                break;
                            case InteractionType.normslaskates:
                                EffectMap[coord.X, coord.Y] = 2;
                                break;
                            case InteractionType.lowpool:
                                EffectMap[coord.X, coord.Y] = 4;
                                break;
                            case InteractionType.haloweenpool:
                                EffectMap[coord.X, coord.Y] = 5;
                                break;
                            case InteractionType.snowboardslope:
                                EffectMap[coord.X, coord.Y] = 7;
                                break;
                        }
                    }
                    else
                    {
                        EffectMap[coord.X, coord.Y] = 1;
                    }
                    if (item.GetBaseItem().Walkable)
                    {
                        if (GameMap[coord.X, coord.Y] != 3)
                        {
                            GameMap[coord.X, coord.Y] = 1;
                        }
                    }
                    else
                    {
                        if (item.Z <= Model.SqFloorHeight[item.X][item.Y] + 0.1 &&
                            item.GetBaseItem().InteractionType == InteractionType.gate && item.ExtraData == "1")
                        {
                            if (GameMap[coord.X, coord.Y] != 3)
                            {
                                GameMap[coord.X, coord.Y] = 1;
                            }
                        }
                        else
                        {
                            if (item.GetBaseItem().IsSeat)
                            {
                                GameMap[coord.X, coord.Y] = 3;
                            }
                            else if (item.GetBaseItem().InteractionType == InteractionType.bed ||
                                     item.GetBaseItem().InteractionType == InteractionType.bedtent)
                            {
                                if (coord.X == item.X && coord.Y == item.Y)
                                {
                                    GameMap[coord.X, coord.Y] = 3;
                                }
                            }
                            else
                            {
                                if (GameMap[coord.X, coord.Y] != 3)
                                {
                                    GameMap[coord.X, coord.Y] = 0;
                                }
                            }
                        }
                    }
                }
                if (item.GetBaseItem().InteractionType == InteractionType.bed ||
                    item.GetBaseItem().InteractionType == InteractionType.bedtent)
                {
                    GameMap[coord.X, coord.Y] = 3;
                }
            }
            catch (Exception ex)
            {
                Logging.LogException(string.Concat(new object[]
                {
                    "Error during map generation for room ",
                    _room.RoomId,
                    ". Exception: ",
                    ex.ToString()
                }));
            }
            return true;
        }

        private void AddSpecialItems(RoomItem item)
        {
            switch (item.GetBaseItem().InteractionType)
            {
                case InteractionType.banzaifloor:
                    _room.GetBanzai().AddTile(item, item.Id);
                    break;
                case InteractionType.banzaitele:
                    _room.GetGameItemHandler().AddTeleport(item, item.Id);
                    item.ExtraData = "";
                    break;
                case InteractionType.banzaipuck:
                    _room.GetBanzai().AddPuck(item);
                    break;
                case InteractionType.banzaipyramid:
                    _room.GetGameItemHandler().AddPyramid(item, item.Id);
                    break;
                case InteractionType.freezeexit:
                    RoomItem exitTeleport = _room.GetFreeze().ExitTeleport;
                    if (exitTeleport == null || (int)item.Id != (int)exitTeleport.Id)
                    {
                        break;
                    }
                    _room.GetFreeze().ExitTeleport = null;
                    break;
                case InteractionType.freezetileblock:
                    _room.GetFreeze().AddFreezeBlock(item);
                    break;
                case InteractionType.freezetile:
                    _room.GetFreeze().AddFreezeTile(item);
                    break;
                case InteractionType.football:
                    _room.GetSoccer().AddBall(item);
                    break;
            }
        }

        private void RemoveSpecialItem(RoomItem item)
        {
            switch (item.GetBaseItem().InteractionType)
            {
                case InteractionType.banzaifloor:
                    _room.GetBanzai().RemoveTile(item.Id);
                    break;
                case InteractionType.banzaitele:
                    _room.GetGameItemHandler().RemoveTeleport(item.Id);
                    break;
                case InteractionType.banzaipuck:
                    _room.GetBanzai().RemovePuck(item.Id);
                    break;
                case InteractionType.banzaipyramid:
                    _room.GetGameItemHandler().RemovePyramid(item.Id);
                    break;
                case InteractionType.freezetileblock:
                    _room.GetFreeze().RemoveFreezeBlock(item.Id);
                    break;
                case InteractionType.freezetile:
                    _room.GetFreeze().RemoveFreezeTile(item.Id);
                    break;
                case InteractionType.fbgate:
                    //   this.room.GetSoccer().UnRegisterGate(item);
                    break;
                case InteractionType.football:
                    _room.GetSoccer().RemoveBall(item.Id);
                    break;
            }
        }

        private ServerMessage NewHeightMap()
        {
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("HeightMapMessageComposer"));
            serverMessage.AppendInteger(Model.MapSizeX);
            checked
            {
                serverMessage.AppendInteger(Model.MapSizeX * Model.MapSizeY);
                for (int i = 0; i < Model.MapSizeY; i++)
                {
                    for (int j = 0; j < Model.MapSizeX; j++)
                    {
                        serverMessage.AppendShort((short)(SqAbsoluteHeight(j, i) * 256));
                        //  serverMessage.AppendShort(this.Model.SqFloorHeight[j, i] * 256);
                    }
                }
                return serverMessage;
            }
        }
    }
}