using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Azure.Collections;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Rooms.Wired;

namespace Azure.HabboHotel.Rooms.Games
{
    internal class GameManager
    {
        internal int[] TeamPoints;
        private QueuedDictionary<uint, RoomItem> _redTeamItems;
        private QueuedDictionary<uint, RoomItem> _blueTeamItems;
        private QueuedDictionary<uint, RoomItem> _greenTeamItems;
        private QueuedDictionary<uint, RoomItem> _yellowTeamItems;
        private Room _room;

        public GameManager(Room room)
        {
            this.TeamPoints = new int[5];
            this._redTeamItems = new QueuedDictionary<uint, RoomItem>();
            this._blueTeamItems = new QueuedDictionary<uint, RoomItem>();
            this._greenTeamItems = new QueuedDictionary<uint, RoomItem>();
            this._yellowTeamItems = new QueuedDictionary<uint, RoomItem>();
            this._room = room;
        }

        internal event TeamScoreChangedDelegate OnScoreChanged;
        internal event RoomEventDelegate OnGameStart;
        internal event RoomEventDelegate OnGameEnd;

        internal int[] Points
        {
            get
            {
                return this.TeamPoints;
            }
            set
            {
                this.TeamPoints = value;
            }
        }

        internal void OnCycle()
        {
            this._redTeamItems.OnCycle();
            this._blueTeamItems.OnCycle();
            this._greenTeamItems.OnCycle();
            this._yellowTeamItems.OnCycle();
        }

        internal QueuedDictionary<uint, RoomItem> GetItems(Team team)
        {
            switch (team)
            {
                case Team.red:
                    return this._redTeamItems;
                case Team.green:
                    return this._greenTeamItems;
                case Team.blue:
                    return this._blueTeamItems;
                case Team.yellow:
                    return this._yellowTeamItems;
                default:
                    return new QueuedDictionary<uint, RoomItem>();
            }
        }

        internal Team GetWinningTeam()
        {
            int result = 1;
            int num = 0;
            checked
            {
                for (int i = 1; i < 5; i++)
                {
                    if (this.TeamPoints[i] <= num)
                    {
                        continue;
                    }
                    num = this.TeamPoints[i];
                    result = i;
                }
                return (Team)result;
            }
        }

        internal void AddPointToTeam(Team team, RoomUser user)
        {
            this.AddPointToTeam(team, 1, user);
        }

        internal void AddPointToTeam(Team team, int points, RoomUser user)
        {
            int num = checked(this.TeamPoints[(int)team] += points);
            if (num < 0)
            {
                num = 0;
            }

            this.TeamPoints[(int)team] = num;
            if (this.OnScoreChanged != null)
            {
                this.OnScoreChanged(null, new TeamScoreChangedArgs(num, team, user));
            }
            foreach (
                RoomItem current in
                this.GetFurniItems(team).Values.Where(current => !IsSoccerGoal(current.GetBaseItem().InteractionType)))
            {
                current.ExtraData = this.TeamPoints[(int)team].ToString();
                current.UpdateState();
            }

            this._room.GetWiredHandler().ExecuteWired(WiredItemType.TriggerScoreAchieved, new object[]
            {
                user
            });
        }

        internal void Reset()
        {
            checked
            {
                this.AddPointToTeam(Team.blue, this.GetScoreForTeam(Team.blue) * -1, null);
                this.AddPointToTeam(Team.green, this.GetScoreForTeam(Team.green) * -1, null);
                this.AddPointToTeam(Team.red, this.GetScoreForTeam(Team.red) * -1, null);
                this.AddPointToTeam(Team.yellow, this.GetScoreForTeam(Team.yellow) * -1, null);
            }
        }

        internal void AddFurnitureToTeam(RoomItem item, Team team)
        {
            switch (team)
            {
                case Team.red:
                    this._redTeamItems.Add(item.Id, item);
                    return;
                case Team.green:
                    this._greenTeamItems.Add(item.Id, item);
                    return;
                case Team.blue:
                    this._blueTeamItems.Add(item.Id, item);
                    return;
                case Team.yellow:
                    this._yellowTeamItems.Add(item.Id, item);
                    return;
                default:
                    return;
            }
        }

        internal void RemoveFurnitureFromTeam(RoomItem item, Team team)
        {
            switch (team)
            {
                case Team.red:
                    this._redTeamItems.Remove(item.Id);
                    return;
                case Team.green:
                    this._greenTeamItems.Remove(item.Id);
                    return;
                case Team.blue:
                    this._blueTeamItems.Remove(item.Id);
                    return;
                case Team.yellow:
                    this._yellowTeamItems.Remove(item.Id);
                    return;
                default:
                    return;
            }
        }

        internal RoomItem GetFirstScoreBoard(Team team)
        {
            switch (team)
            {
                case Team.red:
                    goto IL_BF;
                case Team.green:
                    break;
                case Team.blue:
                    using (var enumerator = this._blueTeamItems.Values.GetEnumerator())
                    {
                        while (enumerator.MoveNext())
                        {
                            RoomItem current = enumerator.Current;
                            if (current.GetBaseItem().InteractionType != InteractionType.freezebluecounter)
                            {
                                continue;
                            }
                            RoomItem result = current;
                            return result;
                        }
                        goto IL_151;
                    }
                case Team.yellow:
                    goto IL_108;
                default:
                    goto IL_151;
            }
            using (var enumerator2 = this._greenTeamItems.Values.GetEnumerator())
            {
                while (enumerator2.MoveNext())
                {
                    RoomItem current2 = enumerator2.Current;
                    if (current2.GetBaseItem().InteractionType != InteractionType.freezegreencounter)
                    {
                        continue;
                    }
                    RoomItem result = current2;
                    return result;
                }
                goto IL_151;
            }
            IL_BF:
            using (var enumerator3 = this._redTeamItems.Values.GetEnumerator())
            {
                while (enumerator3.MoveNext())
                {
                    RoomItem current3 = enumerator3.Current;
                    if (current3.GetBaseItem().InteractionType != InteractionType.freezeredcounter)
                    {
                        continue;
                    }
                    RoomItem result = current3;
                    return result;
                }
                goto IL_151;
            }
            IL_108:
            foreach (
                RoomItem result in
                this._yellowTeamItems.Values.Where(
                    current4 => current4.GetBaseItem().InteractionType == InteractionType.freezeyellowcounter))
            {
                return result;
            }
            IL_151:
            return null;
        }

        internal void UnlockGates()
        {
            foreach (RoomItem current in this._redTeamItems.Values)
            {
                this.UnlockGate(current);
            }
            foreach (RoomItem current2 in this._greenTeamItems.Values)
            {
                this.UnlockGate(current2);
            }
            foreach (RoomItem current3 in this._blueTeamItems.Values)
            {
                this.UnlockGate(current3);
            }
            foreach (RoomItem current4 in this._yellowTeamItems.Values)
            {
                this.UnlockGate(current4);
            }
        }

        internal void LockGates()
        {
            foreach (RoomItem current in this._redTeamItems.Values)
            {
                this.LockGate(current);
            }
            foreach (RoomItem current2 in this._greenTeamItems.Values)
            {
                this.LockGate(current2);
            }
            foreach (RoomItem current3 in this._blueTeamItems.Values)
            {
                this.LockGate(current3);
            }
            foreach (RoomItem current4 in this._yellowTeamItems.Values)
            {
                this.LockGate(current4);
            }
        }

        internal void StopGame()
        {
            if (this.OnGameEnd != null)
            {
                this.OnGameEnd(null, null);
            }
        }

        internal void StartGame()
        {
            if (this.OnGameStart != null)
            {
                this.OnGameStart(null, null);
            }
            this.GetRoom().GetWiredHandler().ResetExtraString(WiredItemType.EffectGiveScore);
        }

        internal Room GetRoom()
        {
            return this._room;
        }

        internal void Destroy()
        {
            Array.Clear(this.TeamPoints, 0, this.TeamPoints.Length);
            this._redTeamItems.Destroy();
            this._blueTeamItems.Destroy();
            this._greenTeamItems.Destroy();
            this._yellowTeamItems.Destroy();
            this.TeamPoints = null;
            this.OnScoreChanged = null;
            this.OnGameStart = null;
            this.OnGameEnd = null;
            this._redTeamItems = null;
            this._blueTeamItems = null;
            this._greenTeamItems = null;
            this._yellowTeamItems = null;
            this._room = null;
        }

        private static bool IsSoccerGoal(InteractionType type)
        {
            return type == InteractionType.footballgoalblue || type == InteractionType.footballgoalgreen ||
                   type == InteractionType.footballgoalred || type == InteractionType.footballgoalyellow;
        }

        private int GetScoreForTeam(Team team)
        {
            return this.TeamPoints[(int)team];
        }

        private QueuedDictionary<uint, RoomItem> GetFurniItems(Team team)
        {
            switch (team)
            {
                case Team.red:
                    return this._redTeamItems;
                case Team.green:
                    return this._greenTeamItems;
                case Team.blue:
                    return this._blueTeamItems;
                case Team.yellow:
                    return this._yellowTeamItems;
                default:
                    return new QueuedDictionary<uint, RoomItem>();
            }
        }

        private void LockGate(RoomItem item)
        {
            InteractionType interactionType = item.GetBaseItem().InteractionType;
            if (interactionType != InteractionType.freezebluegate && interactionType != InteractionType.freezegreengate &&
                interactionType != InteractionType.freezeredgate && interactionType != InteractionType.freezeyellowgate &&
                interactionType != InteractionType.banzaigateblue && interactionType != InteractionType.banzaigatered &&
                interactionType != InteractionType.banzaigategreen &&
                interactionType != InteractionType.banzaigateyellow)
            {
                return;
            }
            foreach (RoomUser current in this._room.GetGameMap().GetRoomUsers(new Point(item.X, item.Y)))
            {
                current.SqState = 0;
            }
            this._room.GetGameMap().GameMap[item.X, item.Y] = 0;
        }

        private void UnlockGate(RoomItem item)
        {
            InteractionType interactionType = item.GetBaseItem().InteractionType;
            if (interactionType != InteractionType.freezebluegate && interactionType != InteractionType.freezegreengate &&
                interactionType != InteractionType.freezeredgate && interactionType != InteractionType.freezeyellowgate &&
                interactionType != InteractionType.banzaigateblue && interactionType != InteractionType.banzaigatered &&
                interactionType != InteractionType.banzaigategreen &&
                interactionType != InteractionType.banzaigateyellow)
            {
                return;
            }
            foreach (RoomUser current in this._room.GetGameMap().GetRoomUsers(new Point(item.X, item.Y)))
            {
                current.SqState = 1;
            }
            this._room.GetGameMap().GameMap[item.X, item.Y] = 1;
        }
    }
}