using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using Azure.HabboHotel.Items;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Rooms.Games
{
    internal class Freeze
    {
        private Room _room;
        private HybridDictionary _freezeTiles;
        private HybridDictionary _freezeBlocks;
        private Random _rnd;

        public Freeze(Room room)
        {
            this._room = room;
            this._freezeTiles = new HybridDictionary();
            this._freezeBlocks = new HybridDictionary();
            this.ExitTeleport = null;
            this._rnd = new Random();
            this.GameStarted = false;
        }

        public bool IsFreezeActive
        {
            get
            {
                return this.GameStarted;
            }
        }

        internal bool GameStarted { get; private set; }

        internal RoomItem ExitTeleport { get; set; }

        public static void CycleUser(RoomUser user)
        {
            if (user.Freezed)
            {
                checked
                {
                    ++user.FreezeCounter;
                }
                if (user.FreezeCounter > 10)
                {
                    user.Freezed = false;
                    user.FreezeCounter = 0;
                    ActivateShield(user);
                }
            }
            if (!user.ShieldActive)
            {
                return;
            }
            checked
            {
                ++user.ShieldCounter;
            }
            if (user.ShieldCounter <= 10)
            {
                return;
            }
            user.ShieldActive = false;
            user.ShieldCounter = 10;
            user.ApplyEffect((int)(user.Team + 39));
        }

        internal static void OnCycle()
        {
        }

        internal void StartGame()
        {
            this.GameStarted = true;
            this.CountTeamPoints();
            this.ResetGame();
            this._room.GetGameManager().LockGates();
            this._room.GetGameManager().StartGame();
        }

        internal void StopGame()
        {
            this.GameStarted = false;
            this._room.GetGameManager().UnlockGates();
            this._room.GetGameManager().StopGame();
            Team winningTeam = this._room.GetGameManager().GetWinningTeam();
            foreach (RoomUser avatar in this._room.GetRoomUserManager().UserList.Values)
            {
                avatar.FreezeLives = 0;
                if (avatar.Team != winningTeam)
                {
                    continue;
                }
                avatar.UnIdle();
                avatar.DanceId = 0;
                var waveAtWin = new ServerMessage(LibraryParser.OutgoingRequest("RoomUserActionMessageComposer"));
                waveAtWin.AppendInteger(avatar.VirtualId);
                waveAtWin.AppendInteger(1);
                this._room.SendMessage(waveAtWin);
            }
        }

        internal void ResetGame()
        {
            foreach (RoomItem roomItem in this._freezeBlocks.Values.Cast<RoomItem>().Where(roomItem => !string.IsNullOrEmpty(roomItem.ExtraData) && !roomItem.GetBaseItem().InteractionType.Equals(InteractionType.freezebluegate) && (!roomItem.GetBaseItem().InteractionType.Equals(InteractionType.freezeredgate) && !roomItem.GetBaseItem().InteractionType.Equals(InteractionType.freezegreengate)) && !roomItem.GetBaseItem().InteractionType.Equals(InteractionType.freezeyellowgate)))
            {
                roomItem.ExtraData = "";
                roomItem.UpdateState(false, true);
                this._room.GetGameMap().AddItemToMap(roomItem, false);
            }
        }

        internal void OnUserWalk(RoomUser user)
        {
            if (!this.GameStarted || user.Team == Team.none)
            {
                return;
            }
            if (user.X == user.GoalX && user.GoalY == user.Y && user.ThrowBallAtGoal)
            {
                foreach (RoomItem roomItem in this._freezeTiles.Values.Cast<RoomItem>().Where(roomItem => roomItem.InteractionCountHelper == 0 && (roomItem.X == user.X && roomItem.Y == user.Y)))
                {
                    roomItem.InteractionCountHelper = 1;
                    roomItem.ExtraData = "1000";
                    roomItem.UpdateState();
                    roomItem.InteractingUser = user.UserId;
                    roomItem.FreezePowerUp = user.BanzaiPowerUp;
                    roomItem.ReqUpdate(4, true);
                    switch (user.BanzaiPowerUp)
                    {
                        case FreezePowerUp.GreenArrow:
                        case FreezePowerUp.OrangeSnowball:
                            user.BanzaiPowerUp = FreezePowerUp.None;
                            goto label_12;
                        default:
                            goto label_12;
                    }
                }
                label_12:
                ;
            }
            foreach (RoomItem roomItem in this._freezeBlocks.Values)
            {
                if (user.X == roomItem.X && user.Y == roomItem.Y && roomItem.FreezePowerUp != FreezePowerUp.None)
                {
                    this.PickUpPowerUp(roomItem, user);
                }
            }
        }

        internal void OnFreezeTiles(RoomItem item, FreezePowerUp powerUp, uint userID)
        {
            if (this._room.GetRoomUserManager().GetRoomUserByHabbo(userID) == null)
            {
                return;
            }
            List<RoomItem> items;
            switch (powerUp)
            {
                case FreezePowerUp.BlueArrow:
                    items = this.GetVerticalItems(item.X, item.Y, 5);
                    break;
                case FreezePowerUp.GreenArrow:
                    items = this.GetDiagonalItems(item.X, item.Y, 5);
                    break;
                case FreezePowerUp.OrangeSnowball:
                    items = this.GetVerticalItems(item.X, item.Y, 5);
                    items.AddRange(this.GetDiagonalItems(item.X, item.Y, 5));
                    break;
                default:
                    items = this.GetVerticalItems(item.X, item.Y, 3);
                    break;
            }
            this.HandleBanzaiFreezeItems(items);
        }

        internal void AddFreezeTile(RoomItem item)
        {
            if (!this._freezeTiles.Contains(item.Id))
            {
                this._freezeTiles.Remove(item.Id);
            }
            this._freezeTiles.Add(item.Id, item);
        }

        internal void RemoveFreezeTile(uint itemId)
        {
            if (!this._freezeTiles.Contains(itemId))
            {
                return;
            }
            this._freezeTiles.Remove(itemId);
        }

        internal void AddFreezeBlock(RoomItem item)
        {
            if (this._freezeBlocks.Contains(item.Id))
            {
                this._freezeBlocks.Remove(item.Id);
            }
            this._freezeBlocks.Add(item.Id, item);
        }

        internal void RemoveFreezeBlock(uint itemId)
        {
            this._freezeBlocks.Remove(itemId);
        }

        internal void Destroy()
        {
            this._freezeBlocks.Clear();
            this._freezeTiles.Clear();
            this._room = null;
            this._freezeTiles = null;
            this._freezeBlocks = null;
            this.ExitTeleport = null;
            this._rnd = null;
        }

        internal void FreezeStart()
        {
            throw new NotImplementedException();
        }

        internal void FreezeEnd()
        {
            throw new NotImplementedException();
        }

        private static void ActivateShield(RoomUser user)
        {
            user.ApplyEffect((int)(user.Team + 48));
            user.ShieldActive = true;
            user.ShieldCounter = 0;
        }

        private static bool SquareGotFreezeTile(IEnumerable<RoomItem> items)
        {
            return items.Any(roomItem => roomItem.GetBaseItem().InteractionType == InteractionType.freezetile);
        }

        private static bool SquareGotFreezeBlock(IEnumerable<RoomItem> items)
        {
            return items.Any(roomItem => roomItem.GetBaseItem().InteractionType == InteractionType.freezetileblock);
        }

        private void CountTeamPoints()
        {
            this._room.GetGameManager().Reset();
            foreach (RoomUser roomUser in this._room.GetRoomUserManager().UserList.Values.Where(roomUser => !roomUser.IsBot && roomUser.Team != Team.none && roomUser.GetClient() != null))
            {
                roomUser.BanzaiPowerUp = FreezePowerUp.None;
                roomUser.FreezeLives = 3;
                roomUser.ShieldActive = false;
                roomUser.ShieldCounter = 11;
                this._room.GetGameManager().AddPointToTeam(roomUser.Team, 30, null);
                var serverMessage = new ServerMessage();
                serverMessage.Init(LibraryParser.OutgoingRequest("UpdateFreezeLivesMessageComposer"));
                serverMessage.AppendInteger(roomUser.InternalRoomId);
                serverMessage.AppendInteger(roomUser.FreezeLives);
                roomUser.GetClient().SendMessage(serverMessage);
            }
        }

        private void HandleBanzaiFreezeItems(IEnumerable<RoomItem> items)
        {
            foreach (RoomItem roomItem in items)
            {
                switch (roomItem.GetBaseItem().InteractionType)
                {
                    case InteractionType.freezetileblock:
                        this.SetRandomPowerUp(roomItem);
                        roomItem.UpdateState(false, true);
                        continue;
                    case InteractionType.freezetile:
                        roomItem.ExtraData = "11000";
                        roomItem.UpdateState(false, true);
                        continue;
                    default:
                        continue;
                }
            }
        }

        private void SetRandomPowerUp(RoomItem item)
        {
            if (!string.IsNullOrEmpty(item.ExtraData))
            {
                return;
            }
            switch (this._rnd.Next(1, 14))
            {
                case 2:
                    item.ExtraData = "2000";
                    item.FreezePowerUp = FreezePowerUp.BlueArrow;
                    break;
                case 3:
                    item.ExtraData = "3000";
                    item.FreezePowerUp = FreezePowerUp.Snowballs;
                    break;
                case 4:
                    item.ExtraData = "4000";
                    item.FreezePowerUp = FreezePowerUp.GreenArrow;
                    break;
                case 5:
                    item.ExtraData = "5000";
                    item.FreezePowerUp = FreezePowerUp.OrangeSnowball;
                    break;
                case 6:
                    item.ExtraData = "6000";
                    item.FreezePowerUp = FreezePowerUp.Heart;
                    break;
                case 7:
                    item.ExtraData = "7000";
                    item.FreezePowerUp = FreezePowerUp.Shield;
                    break;
                default:
                    item.ExtraData = "1000";
                    item.FreezePowerUp = FreezePowerUp.None;
                    break;
            }
            this._room.GetGameMap().RemoveFromMap(item, false);
            item.UpdateState(false, true);
        }

        private void PickUpPowerUp(RoomItem item, RoomUser user)
        {
            switch (item.FreezePowerUp)
            {
                case FreezePowerUp.BlueArrow:
                case FreezePowerUp.GreenArrow:
                case FreezePowerUp.OrangeSnowball:
                    user.BanzaiPowerUp = item.FreezePowerUp;
                    break;
                case FreezePowerUp.Shield:
                    ActivateShield(user);
                    break;
                case FreezePowerUp.Heart:
                    if (user.FreezeLives < 5)
                    {
                        checked
                        {
                            ++user.FreezeLives;
                        }
                        this._room.GetGameManager().AddPointToTeam(user.Team, 10, user);
                    }
                    var serverMessage = new ServerMessage();
                    serverMessage.Init(LibraryParser.OutgoingRequest("UpdateFreezeLivesMessageComposer"));
                    serverMessage.AppendInteger(user.InternalRoomId);
                    serverMessage.AppendInteger(user.FreezeLives);
                    user.GetClient().SendMessage(serverMessage);
                    break;
            }
            item.FreezePowerUp = FreezePowerUp.None;
            item.ExtraData = string.Format("1{0}", item.ExtraData);
            item.UpdateState(false, true);
        }

        private void HandleUserFreeze(Point point)
        {
            RoomUser user = this._room.GetGameMap().GetRoomUsers(point).FirstOrDefault();
            if (user == null || user.IsWalking && user.SetX != point.X && user.SetY != point.Y)
            {
                return;
            }
            this.FreezeUser(user);
        }

        private void FreezeUser(RoomUser user)
        {
            if (user.IsBot || user.ShieldActive || user.Team == Team.none || user.Freezed)
            {
                return;
            }
            user.Freezed = true;
            user.FreezeCounter = 0;
            checked
            {
                --user.FreezeLives;
            }
            if (user.FreezeLives <= 0)
            {
                var serverMessage = new ServerMessage();
                serverMessage.Init(LibraryParser.OutgoingRequest("UpdateFreezeLivesMessageComposer"));
                serverMessage.AppendInteger(user.InternalRoomId);
                serverMessage.AppendInteger(user.FreezeLives);
                user.GetClient().SendMessage(serverMessage);
                user.ApplyEffect(-1);
                this._room.GetGameManager().AddPointToTeam(user.Team, -10, user);
                TeamManager managerForFreeze = this._room.GetTeamManagerForFreeze();
                managerForFreeze.OnUserLeave(user);
                user.Team = Team.none;
                if (this.ExitTeleport != null)
                {
                    this._room.GetGameMap().TeleportToItem(user, this.ExitTeleport);
                }
                user.Freezed = false;
                user.SetStep = false;
                user.IsWalking = false;
                user.UpdateNeeded = true;
                if (!managerForFreeze.BlueTeam.Any() && !managerForFreeze.RedTeam.Any() &&
                    !managerForFreeze.GreenTeam.Any() && managerForFreeze.YellowTeam.Any())
                {
                    this.StopGame();
                }
                else if (managerForFreeze.BlueTeam.Any() && !managerForFreeze.RedTeam.Any() &&
                         !managerForFreeze.GreenTeam.Any() && !managerForFreeze.YellowTeam.Any())
                {
                    this.StopGame();
                }
                else if (!managerForFreeze.BlueTeam.Any() && managerForFreeze.RedTeam.Any() &&
                         !managerForFreeze.GreenTeam.Any() && !managerForFreeze.YellowTeam.Any())
                {
                    this.StopGame();
                }
                else
                {
                    if (managerForFreeze.BlueTeam.Any() || managerForFreeze.RedTeam.Any() ||
                        !managerForFreeze.GreenTeam.Any() || managerForFreeze.YellowTeam.Any())
                    {
                        return;
                    }
                    this.StopGame();
                }
            }
            else
            {
                this._room.GetGameManager().AddPointToTeam(user.Team, -10, user);
                user.ApplyEffect(12);
                var serverMessage = new ServerMessage();
                serverMessage.Init(LibraryParser.OutgoingRequest("UpdateFreezeLivesMessageComposer"));
                serverMessage.AppendInteger(user.InternalRoomId);
                serverMessage.AppendInteger(user.FreezeLives);
                user.GetClient().SendMessage(serverMessage);
            }
        }

        private List<RoomItem> GetVerticalItems(int x, int y, int length)
        {
            var list = new List<RoomItem>();
            int num1 = 0;
            Point point;
            while (num1 < length)
            {
                point = new Point(checked(x + num1), y);
                List<RoomItem> itemsForSquare = this.GetItemsForSquare(point);
                if (SquareGotFreezeTile(itemsForSquare))
                {
                    this.HandleUserFreeze(point);
                    list.AddRange(itemsForSquare);
                    if (!SquareGotFreezeBlock(itemsForSquare))
                        checked
                        {
                            ++num1;
                        }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            int num2 = 1;
            while (num2 < length)
            {
                point = new Point(x, checked(y + num2));
                List<RoomItem> itemsForSquare = this.GetItemsForSquare(point);
                if (SquareGotFreezeTile(itemsForSquare))
                {
                    this.HandleUserFreeze(point);
                    list.AddRange(itemsForSquare);
                    if (!SquareGotFreezeBlock(itemsForSquare))
                        checked
                        {
                            ++num2;
                        }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            int num3 = 1;
            while (num3 < length)
            {
                point = new Point(checked(x - num3), y);
                List<RoomItem> itemsForSquare = this.GetItemsForSquare(point);
                if (SquareGotFreezeTile(itemsForSquare))
                {
                    this.HandleUserFreeze(point);
                    list.AddRange(itemsForSquare);
                    if (!SquareGotFreezeBlock(itemsForSquare))
                        checked
                        {
                            ++num3;
                        }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            int num4 = 1;
            while (num4 < length)
            {
                point = new Point(x, checked(y - num4));
                List<RoomItem> itemsForSquare = this.GetItemsForSquare(point);
                if (SquareGotFreezeTile(itemsForSquare))
                {
                    this.HandleUserFreeze(point);
                    list.AddRange(itemsForSquare);
                    if (!SquareGotFreezeBlock(itemsForSquare))
                        checked
                        {
                            ++num4;
                        }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            return list;
        }

        private List<RoomItem> GetDiagonalItems(int x, int y, int length)
        {
            var list = new List<RoomItem>();
            int num1 = 0;
            Point point;
            while (num1 < length)
            {
                point = new Point(checked(x + num1), checked(y + num1));
                List<RoomItem> itemsForSquare = this.GetItemsForSquare(point);
                if (SquareGotFreezeTile(itemsForSquare))
                {
                    this.HandleUserFreeze(point);
                    list.AddRange(itemsForSquare);
                    if (!SquareGotFreezeBlock(itemsForSquare))
                        checked
                        {
                            ++num1;
                        }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            int num2 = 0;
            while (num2 < length)
            {
                point = new Point(checked(x - num2), checked(y - num2));
                List<RoomItem> itemsForSquare = this.GetItemsForSquare(point);
                if (SquareGotFreezeTile(itemsForSquare))
                {
                    this.HandleUserFreeze(point);
                    list.AddRange(itemsForSquare);
                    if (!SquareGotFreezeBlock(itemsForSquare))
                        checked
                        {
                            ++num2;
                        }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            int num3 = 0;
            while (num3 < length)
            {
                point = new Point(checked(x - num3), checked(y + num3));
                List<RoomItem> itemsForSquare = this.GetItemsForSquare(point);
                if (SquareGotFreezeTile(itemsForSquare))
                {
                    this.HandleUserFreeze(point);
                    list.AddRange(itemsForSquare);
                    if (!SquareGotFreezeBlock(itemsForSquare))
                        checked
                        {
                            ++num3;
                        }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            int num4 = 0;
            while (num4 < length)
            {
                point = new Point(checked(x + num4), checked(y - num4));
                List<RoomItem> itemsForSquare = this.GetItemsForSquare(point);
                if (SquareGotFreezeTile(itemsForSquare))
                {
                    this.HandleUserFreeze(point);
                    list.AddRange(itemsForSquare);
                    if (!SquareGotFreezeBlock(itemsForSquare))
                        checked
                        {
                            ++num4;
                        }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            return list;
        }

        private List<RoomItem> GetItemsForSquare(Point point)
        {
            return this._room.GetGameMap().GetCoordinatedItems(point);
        }
    }
}