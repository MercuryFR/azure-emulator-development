using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using Azure.Collections;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Rooms.Wired;
using Azure.Messages;
using Azure.Enclosure;
using Azure.Messages.Parsers;
using Azure.Util;

namespace Azure.HabboHotel.Rooms.Games
{
    internal class BattleBanzai
    {
        internal HybridDictionary BanzaiTiles;
        private Room _room;
        private QueuedDictionary<uint, RoomItem> _pucks;
        private byte[,] _floorMap;
        private GameField _field;
        private double _timestarted;

        public BattleBanzai(Room room)
        {
            this._room = room;
            this.BanzaiTiles = new HybridDictionary();
            this.IsBanzaiActive = false;
            this._pucks = new QueuedDictionary<uint, RoomItem>();
            this._timestarted = 0.0;
        }

        internal bool IsBanzaiActive { get; private set; }

        internal void AddTile(RoomItem item, uint itemId)
        {
            if (this.BanzaiTiles.Contains(itemId))
            {
                return;
            }
            this.BanzaiTiles.Add(itemId, item);
        }

        internal void RemoveTile(uint itemId)
        {
            this.BanzaiTiles.Remove(itemId);
        }

        internal void OnCycle()
        {
            this._pucks.OnCycle();
        }

        internal void AddPuck(RoomItem item)
        {
            if (this._pucks.ContainsKey(item.Id))
            {
                return;
            }
            this._pucks.Add(item.Id, item);
        }

        internal void RemovePuck(uint itemId)
        {
            this._pucks.Remove(itemId);
        }

        internal void OnUserWalk(RoomUser user)
        {
            if (user == null)
            {
                return;
            }
            foreach (RoomItem roomItem in this._pucks.Values)
            {
                int num1 = checked(user.X - roomItem.X);
                int num2 = checked(user.Y - roomItem.Y);
                if (num1 > 1 || num1 < -1 || num2 > 1 || num2 < -1)
                {
                    continue;
                }
                int num3 = checked(num1 * -1);
                int num4 = checked(num2 * -1);
                int num5 = checked(num3 + roomItem.X);
                int num6 = checked(num4 + roomItem.Y);
                if ((int)roomItem.InteractingBallUser == (int)user.UserId && this._room.GetGameMap().ValidTile(num5, num6))
                {
                    roomItem.InteractingBallUser = 0U;
                    this.MovePuck(roomItem, user.GetClient(), user.Coordinate, roomItem.Coordinate, 6, user.Team);
                }
                else if (this._room.GetGameMap().ValidTile(num5, num6))
                {
                    this.MovePuck(roomItem, user.GetClient(), num5, num6, user.Team);
                }
            }
            if (!this.IsBanzaiActive)
            {
                return;
            }
            this.HandleBanzaiTiles(user.Coordinate, user.Team, user);
        }

        internal void BanzaiStart()
        {
            if (this.IsBanzaiActive)
            {
                return;
            }
            this._room.GetGameManager().StartGame();
            this._floorMap = new byte[this._room.GetGameMap().Model.MapSizeY, this._room.GetGameMap().Model.MapSizeX];
            this._field = new GameField(this._floorMap, true);
            this._timestarted = Azure.GetUnixTimestamp();
            this._room.GetGameManager().LockGates();
            int index = 1;
            while (index < 5)
            {
                this._room.GetGameManager().Points[index] = 0;
                checked
                {
                    ++index;
                }
            }
            foreach (RoomItem roomItem in this.BanzaiTiles.Values)
            {
                roomItem.ExtraData = "1";
                roomItem.Value = 0;
                roomItem.Team = Team.none;
                roomItem.UpdateState();
            }
            this._room.GetRoomItemHandler().MFloorItems.QueueDelegate(this.ResetTiles);
            this.IsBanzaiActive = true;
            this._room.GetWiredHandler().ExecuteWired(WiredItemType.TriggerGameStarts, new object[0]);
            foreach (RoomUser roomUser in this._room.GetRoomUserManager().GetRoomUsers())
            {
                roomUser.LockedTilesCount = 0;
            }
        }

        internal void ResetTiles()
        {
            foreach (RoomItem roomItem in this._room.GetRoomItemHandler().MFloorItems.Values)
            {
                switch (roomItem.GetBaseItem().InteractionType)
                {
                    case InteractionType.banzaiscoreblue:
                    case InteractionType.banzaiscorered:
                    case InteractionType.banzaiscoreyellow:
                    case InteractionType.banzaiscoregreen:
                        roomItem.ExtraData = "0";
                        roomItem.UpdateState();
                        break;
                }
            }
        }

        internal void BanzaiEnd()
        {
            this.IsBanzaiActive = false;
            this._room.GetGameManager().StopGame();
            this._floorMap = null;
            this._room.GetWiredHandler().ExecuteWired(WiredItemType.TriggerGameEnds, new object[0]);
            Team winningTeam = this._room.GetGameManager().GetWinningTeam();
            this._room.GetGameManager().UnlockGates();
            foreach (RoomItem roomItem in this.BanzaiTiles.Values)
            {
                if (roomItem.Team == winningTeam)
                {
                    roomItem.InteractionCount = 0;
                    roomItem.InteractionCountHelper = 0;
                    roomItem.UpdateNeeded = true;
                }
                else if (roomItem.Team == Team.none)
                {
                    roomItem.ExtraData = "0";
                    roomItem.UpdateState();
                }
            }
            if (winningTeam == Team.none)
            {
                return;
            }
            foreach (RoomUser avatar in this._room.GetRoomUserManager().GetRoomUsers())
            {
                if (avatar.Team != Team.none && Azure.GetUnixTimestamp() - this._timestarted > 5.0)
                {
                    Azure.GetGame()
                                      .GetAchievementManager()
                                      .ProgressUserAchievement(avatar.GetClient(), "ACH_BattleBallTilesLocked",
                                           avatar.LockedTilesCount, false);
                    Azure.GetGame()
                                      .GetAchievementManager()
                                      .ProgressUserAchievement(avatar.GetClient(), "ACH_BattleBallPlayer", 1, false);
                }

                if ((winningTeam == Team.red && avatar.CurrentEffect != 33) || (winningTeam == Team.green && avatar.CurrentEffect != 34) ||
                    (winningTeam == Team.blue && avatar.CurrentEffect != 35) || (winningTeam == Team.yellow && avatar.CurrentEffect != 36))
                {
                    continue;
                }
                if (Azure.GetUnixTimestamp() - this._timestarted > 5.0)
                {
                    Azure.GetGame()
                                      .GetAchievementManager()
                                      .ProgressUserAchievement(avatar.GetClient(), "ACH_BattleBallWinner", 1, false);
                }
                var waveAtWin = new ServerMessage(LibraryParser.OutgoingRequest("RoomUserActionMessageComposer"));
                waveAtWin.AppendInteger(avatar.VirtualId);
                waveAtWin.AppendInteger(1);
                this._room.SendMessage(waveAtWin);
            }
            this._field.Destroy();
        }

        internal void MovePuck(RoomItem item, GameClient mover, int newX, int newY, Team team)
        {
            if (!this._room.GetGameMap().ItemCanBePlacedHere(newX, newY))
            {
                return;
            }
            Point coordinate1 = item.Coordinate;
            double k = this._room.GetGameMap().Model.SqFloorHeight[newX][newY];
            if (coordinate1.X == newX && coordinate1.Y == newY)
            {
                return;
            }
            item.ExtraData = ((int)team).ToString();
            item.UpdateNeeded = true;
            item.UpdateState();
            var serverMessage1 = new ServerMessage();
            serverMessage1.Init(LibraryParser.OutgoingRequest("ItemAnimationMessageComposer"));
            ServerMessage serverMessage2 = serverMessage1;
            Point coordinate2 = item.Coordinate;
            int x = coordinate2.X;
            serverMessage2.AppendInteger(x);
            ServerMessage serverMessage3 = serverMessage1;
            coordinate2 = item.Coordinate;
            int y = coordinate2.Y;
            serverMessage3.AppendInteger(y);
            serverMessage1.AppendInteger(newX);
            serverMessage1.AppendInteger(newY);
            serverMessage1.AppendInteger(1);
            serverMessage1.AppendInteger(item.Id);
            serverMessage1.AppendString(TextHandling.GetString(item.Z));
            serverMessage1.AppendString(TextHandling.GetString(k));
            serverMessage1.AppendInteger(1);
            this._room.SendMessage(serverMessage1);
            this._room.GetRoomItemHandler().SetFloorItem(mover, item, newX, newY, item.Rot, false, false, false, false);
            if (mover == null || mover.GetHabbo() == null)
            {
                return;
            }
            RoomUser roomUserByHabbo = mover.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(mover.GetHabbo().Id);
            if (!this.IsBanzaiActive)
            {
                return;
            }
            this.HandleBanzaiTiles(new Point(newX, newY), team, roomUserByHabbo);
        }

        internal void MovePuck(RoomItem item, GameClient client, Point user, Point ball, int length, Team team)
        {
            int num1 = checked(user.X - ball.X);
            int num2 = checked(user.Y - ball.Y);
            if (num1 > 1 || num1 < -1 || num2 > 1 || num2 < -1)
            {
                return;
            }
            var list = new List<Point>();
            int num3 = ball.X;
            int num4 = ball.Y;
            int num5 = 1;
            while (num5 < length)
            {
                int num6 = checked(num1 * -num5);
                int num7 = checked(num2 * -num5);
                num3 = checked(num6 + item.X);
                num4 = checked(num7 + item.Y);
                if (!this._room.GetGameMap().ItemCanBePlacedHere(num3, num4))
                {
                    if (num5 != 1)
                    {
                        if (num5 != length)
                        {
                            list.Add(new Point(num3, num4));
                        }
                        int num8 = checked(num5 - 1);
                        int num9 = checked(num1 * -num8);
                        int num10 = checked(num2 * -num8);
                        num3 = checked(num9 + item.X);
                        num4 = checked(num10 + item.Y);
                    }
                    break;
                }
                if (num5 != length)
                {
                    list.Add(new Point(num3, num4));
                }
                checked
                {
                    ++num5;
                }
            }
            if (client == null || client.GetHabbo() == null)
            {
                return;
            }
            RoomUser roomUserByHabbo = client.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(client.GetHabbo().Id);
            foreach (Point coord in list)
            {
                this.HandleBanzaiTiles(coord, team, roomUserByHabbo);
            }
            if (num3 != ball.X || num4 != ball.Y)
            {
                this.MovePuck(item, client, num3, num4, team);
            }
        }

        internal void Destroy()
        {
            this.BanzaiTiles.Clear();
            this._pucks.Clear();
            Array.Clear(this._floorMap, 0, this._floorMap.Length);
            this._field.Destroy();
            this._room = null;
            this.BanzaiTiles = null;
            this._pucks = null;
            this._floorMap = null;
            this._field = null;
        }

        private static void SetMaxForTile(RoomItem item, Team team, RoomUser user)
        {
            if (item.Value < 3)
            {
                item.Value = 3;
                item.Team = team;
            }
            int num = checked(item.Value + unchecked((int)item.Team) * 3 - 1);
            item.ExtraData = num.ToString();
        }

        private void SetTile(RoomItem item, Team team, RoomUser user)
        {
            if (item.Team == team)
            {
                if (item.Value < 3)
                {
                    checked
                    {
                        ++item.Value;
                    }
                    if (item.Value == 3)
                    {
                        checked
                        {
                            ++user.LockedTilesCount;
                        }
                        this._room.GetGameManager().AddPointToTeam(item.Team, user);
                        this._field.UpdateLocation(item.X, item.Y, checked((byte)(uint)team));
                        foreach (PointField pointField in this._field.DoUpdate(false))
                        {
                            var team1 = (Team)pointField.ForValue;
                            foreach (Point point in pointField.GetPoints())
                            {
                                this.HandleMaxBanzaiTiles(new Point(point.X, point.Y), team1, user);
                                this._floorMap[point.Y, point.X] = pointField.ForValue;
                            }
                        }
                    }
                }
            }
            else if (item.Value < 3)
            {
                item.Team = team;
                item.Value = 1;
            }
            int num = checked(item.Value + unchecked((int)item.Team) * 3 - 1);
            item.ExtraData = num.ToString();
        }

        private void HandleBanzaiTiles(Point coord, Team team, RoomUser user)
        {
            if (team == Team.none)
            {
                return;
            }
            this._room.GetGameMap().GetCoordinatedItems(coord);
            int num = 0;
            foreach (RoomItem roomItem in this.BanzaiTiles.Values)
            {
                if (roomItem.GetBaseItem().InteractionType != InteractionType.banzaifloor)
                {
                    user.Team = Team.none;
                    user.ApplyEffect(0);
                }
                else if (roomItem.ExtraData.Equals("5") || roomItem.ExtraData.Equals("8") ||
                         roomItem.ExtraData.Equals("11") || roomItem.ExtraData.Equals("14"))
                    checked
                    {
                        ++num;
                    }
                else if (roomItem.X == coord.X && roomItem.Y == coord.Y)
                {
                    this.SetTile(roomItem, team, user);
                    if (roomItem.ExtraData.Equals("5") || roomItem.ExtraData.Equals("8") ||
                        roomItem.ExtraData.Equals("11") || roomItem.ExtraData.Equals("14"))
                        checked
                        {
                            ++num;
                        }
                    roomItem.UpdateState(false, true);
                }
            }
            if (num != this.BanzaiTiles.Count)
            {
                return;
            }
            this.BanzaiEnd();
        }

        private void HandleMaxBanzaiTiles(Point coord, Team team, RoomUser user)
        {
            if (team == Team.none)
            {
                return;
            }
            this._room.GetGameMap().GetCoordinatedItems(coord);
            foreach (RoomItem roomItem in this.BanzaiTiles.Values.Cast<RoomItem>().Where(roomItem => roomItem.GetBaseItem().InteractionType == InteractionType.banzaifloor && (roomItem.X == coord.X && roomItem.Y == coord.Y)))
            {
                SetMaxForTile(roomItem, team, user);
                this._room.GetGameManager().AddPointToTeam(team, user);
                roomItem.UpdateState(false, true);
            }
        }
    }
}