using System;
using System.Collections.Generic;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms
{
    internal class TradeUser
    {
        internal uint UserId;
        internal List<UserItem> OfferedItems;
        private readonly uint _roomId;

        internal TradeUser(uint userId, uint roomId)
        {
            this.UserId = userId;
            this._roomId = roomId;
            this.HasAccepted = false;
            this.OfferedItems = new List<UserItem>();
        }

        internal bool HasAccepted { get; set; }

        internal RoomUser GetRoomUser()
        {
            Room room = Azure.GetGame().GetRoomManager().GetRoom(this._roomId);
            if (room == null)
            {
                return null;
            }
            return room.GetRoomUserManager().GetRoomUserByHabbo(this.UserId);
        }

        internal GameClient GetClient()
        {
            return Azure.GetGame().GetClientManager().GetClientByUserId(this.UserId);
        }
    }
}