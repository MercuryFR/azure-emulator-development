using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Azure.Configuration;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Groups;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Rooms
{
    internal class RoomData
    {
        internal uint Id;
        internal string Name;
        internal string Description;
        internal string Type;
        internal string Owner;
        internal int OwnerId;
        internal string Password;
        internal int State;
        internal int TradeState;
        internal int Category;
        internal int UsersNow;
        internal int UsersMax;
        internal string ModelName;
        internal string CCTs;
        internal int Score;
        internal List<string> Tags;
        internal int AllowPets;
        internal int AllowPetsEating;
        internal int AllowWalkthrough;
        internal int ChatType;
        internal int ChatBalloon;
        internal int ChatSpeed;
        internal int ChatMaxDistance;
        internal int ChatFloodProtection;
        internal bool AllowRightsOverride;
        internal int Hidewall;
        internal string Wallpaper;
        internal string Floor;
        internal string Landscape;
        internal int WallThickness;
        internal int FloorThickness;
        internal Guild Group;
        internal RoomEvent Event;
        internal int GameId;
        internal int WhoCanKick;
        internal int WhoCanBan;
        internal int WhoCanMute;
        internal uint GroupId;
        internal ConcurrentBag<Chatlog> RoomChat;
        internal List<string> WordFilter;
        internal int WallHeight;

        private RoomModel mModel;

        internal int TagCount
        {
            get
            {
                return this.Tags.Count;
            }
        }

        internal bool HasEvent
        {
            get
            {
                return false;
            }
        }

        internal RoomModel Model
        {
            get
            {
                return this.mModel ?? (this.mModel = Azure.GetGame().GetRoomManager().GetModel(this.ModelName, this.Id));
            }
        }

        internal void ResetModel()
        {
            this.mModel = Azure.GetGame().GetRoomManager().GetModel(this.ModelName, this.Id);
        }

        internal void FillNull(uint pId)
        {
            this.Id = pId;
            this.Name = "Unknown Room";
            this.Description = "-";
            this.Type = "private";
            this.Owner = "-";
            this.Category = 0;
            this.UsersNow = 0;
            this.UsersMax = 0;
            this.ModelName = "NO_MODEL";
            this.CCTs = "";
            this.Score = 0;
            this.Tags = new List<string>();
            this.AllowPets = 1;
            this.AllowPetsEating = 0;
            this.AllowWalkthrough = 1;
            this.Hidewall = 0;
            this.Password = "";
            this.Wallpaper = "0.0";
            this.Floor = "0.0";
            this.Landscape = "0.0";
            this.WallThickness = 0;
            this.FloorThickness = 0;
            this.Group = null;
            this.AllowRightsOverride = false;
            this.Event = null;
            this.GameId = 0;
            this.WhoCanBan = 0;
            this.WhoCanKick = 0;
            this.WhoCanMute = 0;
            this.TradeState = 2;
            this.State = 0;
            this.RoomChat = new ConcurrentBag<Chatlog>();
            this.WordFilter = new List<string>();
            this.WallHeight = -1;
            this.mModel = Azure.GetGame().GetRoomManager().GetModel(this.ModelName, this.Id);
        }

        internal void Fill(DataRow row)
        {
            this.Id = Convert.ToUInt32(row["id"]);
            this.Name = (string)row["caption"];
            this.Description = (string)row["description"];
            this.Type = (string)row["roomtype"];
            this.Owner = (string)row["owner"];
            this.OwnerId = 0;
            this.RoomChat = new ConcurrentBag<Chatlog>();
            this.WordFilter = new List<string>();
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT id FROM users WHERE Username = @owner");
                queryReactor.AddParameter("owner", this.Owner);
                int integer = queryReactor.GetInteger();
                if (integer > 0)
                {
                    this.OwnerId = integer;
                }
                queryReactor.SetQuery(
                    "SELECT user_id, message, timestamp FROM users_chatlogs WHERE room_id=@id ORDER BY timestamp DESC LIMIT 150");
                queryReactor.AddParameter("id", this.Id);
                DataTable table = queryReactor.GetTable();
                foreach (DataRow dataRow in table.Rows)
                {
                    this.RoomChat.Add(new Chatlog((uint)dataRow[0], (string)dataRow[1], Convert.ToDouble(dataRow[2]), false));
                }
                queryReactor.SetQuery("SELECT word FROM rooms_wordfilter WHERE room_id = @id");
                queryReactor.AddParameter("id", this.Id);
                DataTable table2 = queryReactor.GetTable();
                foreach (DataRow dataRow2 in table2.Rows)
                {
                    this.WordFilter.Add(dataRow2["word"].ToString());
                }
            }
            string a = row["state"].ToString().ToLower();

            if (a == "open")
            {
                this.State = 0;
                goto IL_24B;
            }
            if (a == "password")
            {
                this.State = 2;
                goto IL_24B;
            }
            this.State = 1;
            IL_24B:
            this.TradeState = int.Parse(row["trade_state"].ToString());
            this.Category = (int)row["category"];
            if (!string.IsNullOrEmpty(row["users_now"].ToString()))
            {
                this.UsersNow = (int)row["users_now"];
            }
            else
            {
                this.UsersNow = 0;
            }
            this.UsersMax = (int)row["users_max"];
            this.ModelName = (string)row["model_name"];
            this.WallHeight = int.Parse(row["walls_height"].ToString());
            this.CCTs = (string)row["public_ccts"];
            this.Score = (int)row["score"];
            this.Tags = new List<string>();
            this.AllowPets = Convert.ToInt32(row["allow_pets"].ToString());
            this.AllowPetsEating = Convert.ToInt32(row["allow_pets_eat"].ToString());
            this.AllowWalkthrough = Convert.ToInt32(row["allow_walkthrough"].ToString());
            this.AllowRightsOverride = false;
            this.Hidewall = Convert.ToInt32(row["allow_hidewall"].ToString());
            this.Password = (string)row["password"];
            this.Wallpaper = (string)row["wallpaper"];
            this.Floor = (string)row["floor"];
            this.Landscape = (string)row["landscape"];
            this.FloorThickness = (int)row["floorthick"];
            this.WallThickness = (int)row["wallthick"];
            this.ChatType = (int)row["chat_type"];
            this.ChatBalloon = (int)row["chat_balloon"];
            this.ChatSpeed = (int)row["chat_speed"];
            this.ChatMaxDistance = (int)row["chat_max_distance"];
            this.ChatFloodProtection = (int)row["chat_flood_protection"];
            this.GameId = (int)row["game_id"];
            this.WhoCanMute = Convert.ToInt32(row["mute_settings"]);
            this.WhoCanKick = Convert.ToInt32(row["kick_settings"]);
            this.WhoCanBan = Convert.ToInt32(row["ban_settings"]);
            this.GroupId = (uint)row["group_id"];
            this.Group = Azure.GetGame().GetGroupManager().GetGroup(this.GroupId);
            this.Event = Azure.GetGame().GetRoomEvents().GetEvent(this.Id);
            var dictionary = new Dictionary<int, int>();
            if (!string.IsNullOrEmpty(row["icon_items"].ToString()))
            {
                string[] array = row["icon_items"].ToString().Split(new[]
                {
                    '|'
                });
                foreach (string text in array)
                {
                    if (string.IsNullOrEmpty(text))
                    {
                        continue;
                    }
                    string[] array2 = text.Replace('.', ',').Split(',');
                    int key = 0;
                    int value = 0;
                    int.TryParse(array2[0], out key);
                    if (array2.Length > 1)
                    {
                        int.TryParse(array2[1], out value);
                    }
                    try
                    {
                        if (!dictionary.ContainsKey(key))
                        {
                            dictionary.Add(key, value);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.LogException(string.Concat(new[]
                        {
                            "Exception: ",
                            ex.ToString(),
                            "[",
                            text,
                            "]"
                        }));
                    }
                }
            }
            if (!string.IsNullOrEmpty(row["tags"].ToString()))
            {
                string[] array3 = row["tags"].ToString().Split(',');
                foreach (string item in array3)
                {
                    this.Tags.Add(item);
                }
            }
            this.mModel = Azure.GetGame().GetRoomManager().GetModel(this.ModelName, this.Id);
        }

        internal void Fill(Room room)
        {
            this.Id = room.RoomId;
            this.Name = room.Name;
            this.Description = room.Description;
            this.Type = room.Type;
            this.Owner = room.Owner;
            this.Category = room.Category;
            this.State = room.State;
            this.UsersNow = room.UsersNow;
            this.UsersMax = room.UsersMax;
            this.ModelName = room.ModelName;
            this.WallHeight = room.WallHeight;
            this.Score = room.Score;
            this.Tags = new List<string>();
            object[] array = room.Tags.ToArray();
            foreach (string item in array.Cast<string>())
            {
                this.Tags.Add(item);
            }
            this.AllowPets = room.AllowPets;
            this.AllowPetsEating = room.AllowPetsEating;
            this.AllowWalkthrough = room.AllowWalkthrough;
            this.Hidewall = room.Hidewall;
            this.Password = room.Password;
            this.Wallpaper = room.Wallpaper;
            this.Floor = room.Floor;
            this.Landscape = room.Landscape;
            this.FloorThickness = room.FloorThickness;
            this.WallThickness = room.WallThickness;
            this.Group = room.Group;
            this.Event = room.Event;
            this.ChatType = room.ChatType;
            this.ChatBalloon = room.ChatBalloon;
            this.ChatSpeed = room.ChatSpeed;
            this.ChatMaxDistance = room.ChatMaxDistance;
            this.ChatFloodProtection = room.ChatFloodProtection;
            this.WhoCanMute = room.WhoCanMute;
            this.WhoCanKick = room.WhoCanKick;
            this.WhoCanBan = room.WhoCanBan;
            this.RoomChat = room.RoomChat;
            this.WordFilter = room.WordFilter;
            this.mModel = Azure.GetGame().GetRoomManager().GetModel(this.ModelName, this.Id);
        }

        internal void Serialize(ServerMessage message, bool showEvents)
        {
            message.AppendInteger(this.Id);
            message.AppendString(this.Name);
            message.AppendBool(this.Type == "private");
            message.AppendInteger(this.OwnerId);
            message.AppendString(this.Owner);
            message.AppendInteger(this.State);
            message.AppendInteger(this.UsersNow);
            message.AppendInteger(this.UsersMax);
            message.AppendString(this.Description);
            message.AppendInteger(this.TradeState);
            message.AppendInteger(this.Score);
            message.AppendInteger(0);
            message.AppendInteger(0);
            message.AppendInteger(this.Category);
            if (this.Group != null)
            {
                message.AppendInteger(this.Group.Id);
                message.AppendString(this.Group.Name);
                message.AppendString(this.Group.Badge);
                message.AppendString("");
            }
            else
            {
                message.AppendInteger(0);
                message.AppendString("");
                message.AppendString("");
                message.AppendString("");
            }
            message.AppendInteger(this.TagCount);
            foreach (string current in this.Tags)
            {
                message.AppendString(current);
            }
            message.AppendInteger(0);
            message.AppendInteger(0);
            /*message.AppendBool(false);
            message.AppendBool(false);*/
                message.AppendInteger(0);
            if (showEvents && this.Event != null)
            {
                if (this.Event.HasExpired)
                {
                    Azure.GetGame().GetRoomEvents().RemoveEvent(this.Id);
                }
                message.AppendInteger(1);
                message.AppendString(this.Event.Name);
                message.AppendString(this.Event.Description);
                message.AppendInteger(checked((int)Math.Floor((this.Event.Time - Azure.GetUnixTimestamp()) / 60.0)));
                return;
            }
            //message.AppendInteger(0);
            message.AppendBool(false);
            message.AppendBool(false);
            message.AppendString("");
            message.AppendString("");
            message.AppendInteger(0);
        }

        internal void SerializeRoomData(ServerMessage response, bool fromView, GameClient session, bool sendRoom = false)
        {
            Room room = Azure.GetGame().GetRoomManager().GetRoom(session.GetHabbo().CurrentRoomId);
            if (room == null || !room.CheckRights(session, true, false))
            {
                return;
            }
            response.Init(LibraryParser.OutgoingRequest("RoomDataMessageComposer"));
            response.AppendBool(true);
            response.AppendInteger(this.Id);
            response.AppendString(this.Name);
            response.AppendBool(this.Type == "private");
            response.AppendInteger(this.OwnerId);
            response.AppendString(this.Owner);
            response.AppendInteger(this.State);
            response.AppendInteger(this.UsersNow);
            response.AppendInteger(this.UsersMax);
            response.AppendString(this.Description);
            response.AppendInteger(this.TradeState);
            response.AppendInteger(this.Score);
            response.AppendInteger(0);
            response.AppendInteger(0);
            response.AppendInteger(this.Category);
            if (this.GroupId > 0u)
            {
                response.AppendInteger(this.Group.Id);
                response.AppendString(this.Group.Name);
                response.AppendString(this.Group.Badge);
                response.AppendString("");
            }
            else
            {
                response.AppendInteger(0);
                response.AppendString("");
                response.AppendString("");
                response.AppendString("");
            }
            response.AppendInteger(this.TagCount);
            string[] array = this.Tags.ToArray();
            foreach (string s in array)
            {
                response.AppendString(s);
            }
            response.AppendInteger(0);
            response.AppendInteger(0);
            response.AppendInteger(0);
            response.AppendBool(this.AllowPets == 1);
            response.AppendBool(this.AllowPetsEating == 1);
            response.AppendString("");
            response.AppendString("");
            response.AppendInteger(0);
            response.AppendBool(fromView);
            response.AppendBool(Azure.GetGame().GetNavigator().RoomIsPublicItem(this.Id));
            response.AppendBool(false);
            response.AppendBool(false);
            response.AppendInteger(this.WhoCanMute);
            response.AppendInteger(this.WhoCanKick);
            response.AppendInteger(this.WhoCanBan);
            response.AppendBool(room.CheckRights(session, true, false));
            response.AppendInteger(this.ChatType);
            response.AppendInteger(this.ChatBalloon);
            response.AppendInteger(this.ChatSpeed);
            response.AppendInteger(this.ChatMaxDistance);
            response.AppendInteger(this.ChatFloodProtection);
            if (sendRoom)
            {
                if (Azure.GetGame().GetRoomManager().GetRoom(this.Id) != null)
                {
                    Azure.GetGame().GetRoomManager().GetRoom(this.Id).SendMessage(response);
                }
            }
            else
            {
                session.SendMessage(response);
            }
        }
    }
}