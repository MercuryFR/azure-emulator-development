using Azure.Messages.Handlers;

namespace Azure.HabboHotel.Rooms
{
    internal class TeleUserData
    {
        private readonly uint _roomId;
        private readonly uint _teleId;
        private readonly GameClientMessageHandler _mHandler;
        private readonly HabboHotel.Users.Habbo _mUserRefference;

        internal TeleUserData(GameClientMessageHandler pHandler, HabboHotel.Users.Habbo pUserRefference, uint roomId, uint teleId)
        {
            this._mHandler = pHandler;
            this._mUserRefference = pUserRefference;
            this._roomId = roomId;
            this._teleId = teleId;
        }

        internal void Execute()
        {
            if (this._mHandler == null || this._mUserRefference == null)
            {
                return;
            }
            this._mUserRefference.IsTeleporting = true;
            this._mUserRefference.TeleporterId = this._teleId;
            this._mHandler.PrepareRoomForUser(this._roomId, "");
        }
    }
}