namespace Azure.HabboHotel.Rooms
{
    internal struct InvokedChatMessage
    {
        internal RoomUser User;
        internal string Message;
        internal bool Shout;
        internal int ColourType;
        internal int Count;

        public InvokedChatMessage(RoomUser user, string message, bool shout, int colour, int count)
        {
            this.User = user;
            this.Message = message;
            this.Shout = shout;
            this.ColourType = colour;
            this.Count = count;
        }

        internal void Dispose()
        {
            this.User = null;
            this.Message = null;
        }
    }
}