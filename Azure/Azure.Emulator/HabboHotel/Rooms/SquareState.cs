using System;

namespace Azure.HabboHotel.Rooms
{
    internal enum SquareState
    {
        Open,
        Blocked,
        Seat,
        Pool,
        Vip
    }
}