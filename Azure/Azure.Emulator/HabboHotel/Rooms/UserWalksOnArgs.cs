using System;

namespace Azure.HabboHotel.Rooms
{
    public class UserWalksOnArgs : EventArgs
    {
        internal readonly RoomUser User;

        public UserWalksOnArgs(RoomUser user)
        {
            this.User = user;
        }
    }
}