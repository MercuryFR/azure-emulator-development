﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using Azure.Configuration;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.Events;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Navigators;

namespace Azure.HabboHotel.Rooms
{
    class RoomManager
    {
        public Queue ActiveRoomsRemoveQueue;
        internal Dictionary<uint, Room> LoadedRooms;
        private readonly Queue _roomsToAddQueue;
        private readonly Queue _roomsToRemoveQueue;
        private readonly Queue _roomDataToAddQueue;
        private readonly Queue _votedRoomsAddQueue;
        private readonly Queue _votedRoomsRemoveQueue;
        private readonly Queue _activeRoomsUpdateQueue;
        private readonly Queue _activeRoomsAddQueue;
        private readonly HybridDictionary _roomModels;
        private readonly HybridDictionary _loadedRoomData;
        private readonly Dictionary<RoomData, int> _votedRooms;
        private readonly Dictionary<RoomData, int> _activeRooms;
        private readonly EventManager _eventManager;
        private readonly Queue ballRoomsAddQueue;
        private readonly Queue ballRoomsRemoveQueue;

        private IEnumerable<KeyValuePair<RoomData, int>> _orderedVotedRooms;

        private IEnumerable<KeyValuePair<RoomData, int>> _orderedActiveRooms;

        internal RoomManager()
        {
            LoadedRooms = new Dictionary<uint, Room>();
            _roomModels = new HybridDictionary();
            _loadedRoomData = new HybridDictionary();
            _votedRooms = new Dictionary<RoomData, int>();
            _activeRooms = new Dictionary<RoomData, int>();
            _roomsToAddQueue = new Queue();
            _roomsToRemoveQueue = new Queue();
            _roomDataToAddQueue = new Queue();
            _votedRoomsRemoveQueue = new Queue();
            _votedRoomsAddQueue = new Queue();
            ActiveRoomsRemoveQueue = new Queue();
            _activeRoomsUpdateQueue = new Queue();
            _activeRoomsAddQueue = new Queue();
            _eventManager = new EventManager();
        }

        internal int LoadedRoomsCount
        {
            get { return LoadedRooms.Count; }
        }

        internal void QueueBallAdd(Room data)
        {
            lock (ballRoomsAddQueue.SyncRoot)
            {
                ballRoomsAddQueue.Enqueue(data);
            }
        }

        internal void QueueBallRemove(Room data)
        {
            lock (ballRoomsRemoveQueue.SyncRoot)
            {
                ballRoomsRemoveQueue.Enqueue(data);
            }
        }

        internal KeyValuePair<RoomData, int>[] GetActiveRooms() { return _orderedActiveRooms == null ? null : _orderedActiveRooms.ToArray(); }

        internal KeyValuePair<RoomData, int>[] GetVotedRooms()
        {
            return _orderedVotedRooms == null ? null : _orderedVotedRooms.ToArray();
        }

        internal RoomModel GetModel(string model, uint roomId)
        {
            if (model == "custom" && _roomModels.Contains(string.Format("custom_{0}", roomId)))
                return (RoomModel)_roomModels[string.Format("custom_{0}", roomId)];
            if (_roomModels.Contains(model))
                return (RoomModel)_roomModels[model];
            return null;
        }

        internal RoomData GenerateNullableRoomData(uint roomId)
        {
            if (GenerateRoomData(roomId) != null)
                return GenerateRoomData(roomId);
            var roomData = new RoomData();
            roomData.FillNull(roomId);
            return roomData;
        }

        internal RoomData GenerateRoomData(uint roomId)
        {
            if (_loadedRoomData.Contains(roomId))
                return (RoomData)_loadedRoomData[roomId];
            var roomData = new RoomData();
            if (IsRoomLoaded(roomId))
                return GetRoom(roomId).RoomData;
            DataRow dataRow;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT * FROM rooms_data WHERE id = {0} LIMIT 1", roomId));
                dataRow = queryReactor.GetRow();
            }
            if (dataRow == null)
                return null;
            roomData.Fill(dataRow);
            _loadedRoomData.Add(roomId, roomData);
            return roomData;
        }

        internal KeyValuePair<RoomData, int>[] GetEventRooms() { return _eventManager.GetRooms(); }

        internal bool IsRoomLoaded(uint roomId) { return LoadedRooms.ContainsKey(roomId); }

        internal Room LoadRoom(uint id)
        {
            if (IsRoomLoaded(id))
                return GetRoom(id);
            var roomData = GenerateRoomData(id);
            if (roomData == null)
                return null;
            var room = new Room(roomData);
            Logging.WriteLine(string.Format("[RoomMgr] Room #[{0}] was loaded.", id), ConsoleColor.Blue);
            lock (_roomsToAddQueue.SyncRoot)
            {
                _roomsToAddQueue.Enqueue(room);
            }
            room.InitBots();
            room.InitPets();
            return room;
        }

        internal RoomData FetchRoomData(uint roomId, DataRow dRow)
        {
            if (_loadedRoomData.Contains(roomId))
                return (RoomData)_loadedRoomData[roomId];
            var roomData = new RoomData();
            if (IsRoomLoaded(roomId))
                roomData.Fill(GetRoom(roomId));
            else
                roomData.Fill(dRow);
            _loadedRoomData.Add(roomId, roomData);
            return roomData;
        }

        internal Room GetRoom(uint roomId)
        {
            Room result;
            return LoadedRooms.TryGetValue(roomId, out result) ? result : null;
        }

        internal RoomData CreateRoom(GameClient session, string name, string desc, string model, int category,
            int maxVisitors, int tradeState)
        {
            if (!_roomModels.Contains(model))
            {
                session.SendNotif("I can't create your room with that model!");
                return null;
            }

            uint roomId;
            using (var dbClient = Azure.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery(
                    "INSERT INTO rooms_data (roomtype,caption,description,owner,model_name,category,users_max,trade_state) VALUES ('private',@caption,@desc,@Username,@model,@cat,@usmax,@tstate)");
                dbClient.AddParameter("caption", name);
                dbClient.AddParameter("desc", desc);
                dbClient.AddParameter("Username", session.GetHabbo().UserName);
                dbClient.AddParameter("model", model);
                dbClient.AddParameter("cat", category);
                dbClient.AddParameter("usmax", maxVisitors);
                dbClient.AddParameter("tstate", tradeState.ToString());
                roomId = (uint)dbClient.InsertQuery();
            }
            var data = GenerateRoomData(roomId);
            if (data == null)
                return null;

            
            session.GetHabbo().UsersRooms.Add(data);
            return data;
        }

        internal void InitVotedRooms(IQueryAdapter dbClient)
        {
            dbClient.SetQuery(
                "SELECT * FROM rooms_data WHERE score > 0 AND roomtype = 'private' ORDER BY score DESC LIMIT 40");
            var table = dbClient.GetTable();
            foreach (
                var data in
                    from DataRow dataRow in table.Rows select FetchRoomData(Convert.ToUInt32(dataRow["id"]), dataRow))
                QueueVoteAdd(data);
        }

        internal void LoadNewModel(string model)
        {
            if (_roomModels.Contains(model))
                _roomModels.Remove(model);

            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    "SELECT id,door_x,door_y,door_z,door_dir,heightmap,public_items,club_only,poolmap FROM rooms_models WHERE id = @model");
                queryReactor.AddParameter("model", model);
                var table = queryReactor.GetTable();
                if (table == null)
                    return;
                foreach (DataRow row in table.Rows)
                {
                    var staticFurniMap = (string)row["public_items"];
                    _roomModels.Add(model,
                        new RoomModel((int)row["door_x"], (int)row["door_y"], (double)row["door_z"],
                            (int)row["door_dir"], (string)row["heightmap"], staticFurniMap,
                            Azure.EnumToBool(row["club_only"].ToString()), (string)row["poolmap"]));
                }
            }
        }

        internal void LoadModels(IQueryAdapter dbClient, out uint loadedModel)
        {
            LoadModels(dbClient);
            loadedModel = (uint)_roomModels.Count;
        }

        internal void LoadModels(IQueryAdapter dbClient)
        {
            _roomModels.Clear();
            dbClient.SetQuery(
                "SELECT id,door_x,door_y,door_z,door_dir,heightmap,public_items,club_only,poolmap FROM rooms_models");
            var table = dbClient.GetTable();
            if (table == null)
                return;
            foreach (DataRow dataRow in table.Rows)
            {
                var key = (string)dataRow["id"];
                if (key.StartsWith("model_floorplan_"))
                    continue;
                var staticFurniMap = (string)dataRow["public_items"];
                _roomModels.Add(key,
                    new RoomModel((int)dataRow["door_x"], (int)dataRow["door_y"], (double)dataRow["door_z"],
                        (int)dataRow["door_dir"], (string)dataRow["heightmap"], staticFurniMap,
                        Azure.EnumToBool(dataRow["club_only"].ToString()), (string)dataRow["poolmap"]));
            }
            dbClient.SetQuery("SELECT roomid,door_x,door_y,door_z,door_dir,heightmap FROM rooms_models_customs");
            var DataCustom = dbClient.GetTable();

            if (DataCustom == null)
                return;

            foreach (DataRow Row in DataCustom.Rows)
            {
                var Modelname = string.Format("custom_{0}", Row["roomid"]);
                _roomModels.Add(Modelname,
                    new RoomModel((int)Row["door_x"], (int)Row["door_y"], (Double)Row["door_z"],
                        (int)Row["door_dir"],
                        (string)Row["heightmap"], "", false, ""));
            }
        }

        internal void OnCycle()
        {
            try
            {
                WorkRoomDataQueue();
                WorkRoomsToAddQueue();
                WorkRoomsToRemoveQueue();

                var flag = WorkActiveRoomsAddQueue();
                var flag2 = WorkActiveRoomsRemoveQueue();
                var flag3 = WorkActiveRoomsUpdateQueue();
                if (flag || flag2 || flag3)
                    SortActiveRooms();
                var flag4 = WorkVotedRoomsAddQueue();
                var flag5 = WorkVotedRoomsRemoveQueue();
                if (flag4 || flag5)
                    SortVotedRooms();
                Azure.GetGame().RoomManagerCycleEnded = true;
            }
            catch (Exception ex)
            {
                Logging.LogThreadException(ex.ToString(), "RoomManager.OnCycle Exception --> Not inclusive");
            }
        }

        internal void QueueVoteAdd(RoomData data)
        {
            lock (_votedRoomsAddQueue.SyncRoot)
            {
                _votedRoomsAddQueue.Enqueue(data);
            }
        }

        internal void QueueVoteRemove(RoomData data)
        {
            lock (_votedRoomsRemoveQueue.SyncRoot)
            {
                _votedRoomsRemoveQueue.Enqueue(data);
            }
        }

        internal void QueueActiveRoomUpdate(RoomData data)
        {
            lock (_activeRoomsUpdateQueue.SyncRoot)
            {
                _activeRoomsUpdateQueue.Enqueue(data);
            }
        }

        internal void QueueActiveRoomAdd(RoomData data)
        {
            lock (_activeRoomsAddQueue.SyncRoot)
            {
                _activeRoomsAddQueue.Enqueue(data);
            }
        }

        internal void QueueActiveRoomRemove(RoomData data)
        {
            lock (ActiveRoomsRemoveQueue.SyncRoot)
            {
                ActiveRoomsRemoveQueue.Enqueue(data);
            }
        }

        internal void RemoveAllRooms()
        {
            var count = LoadedRooms.Count;
            var num = 0;
            foreach (var current in LoadedRooms.Values)
            {
                Azure.GetGame().GetRoomManager().UnloadRoom(current, "RemoveAllRooms void called");
                Console.WriteLine("≫ Saving Rooms Content: {0}%",
                    string.Format("{0:0.##}", num / (double)count * 100.0));
                checked
                {
                    num++;
                }
            }
            Console.WriteLine("≫ RoomManager Destroyed!");
        }

        internal void UnloadRoom(Room room, string reason)
        {
            if (room == null)
                return;

            if (Azure.GetGame().GetNavigator().PrivateCategories.Contains(room.Category))
                ((FlatCat)Azure.GetGame().GetNavigator().PrivateCategories[room.Category]).UsersNow -= room.UserCount;
            room.UsersNow = 0;
            room.RoomData.UsersNow = 0;

            var stringBuilder = new StringBuilder();
            checked
            {
                for (var i = 0; i < room.TagCount; i++)
                {
                    if (i > 0)
                        stringBuilder.Append(",");
                    stringBuilder.Append(room.Tags[i]);
                }
                var text = "open";
                if (room.State == 1)
                    text = "locked";
                else if (room.State > 1)
                    text = "password";
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery(string.Concat(new object[]
                    {
                        "UPDATE rooms_data SET caption = @caption, description = @description, password = @password, category = ",
                        room.Category,
                        ", state = '",
                        text,
                        "', tags = @tags, users_now = '0', users_max = ",
                        room.UsersMax,
                        ", allow_pets = '",
                        room.AllowPets,
                        "', allow_pets_eat = '",
                        room.AllowPetsEating,
                        "', allow_walkthrough = '",
                        room.AllowWalkthrough,
                        "', allow_hidewall = '",
                        room.Hidewall,
                        "', floorthick = ",
                        room.FloorThickness,
                        ", wallthick = ",
                        room.WallThickness,
                        ", mute_settings='",
                        room.WhoCanMute,
                        "', kick_settings='",
                        room.WhoCanKick,
                        "',ban_settings='",
                        room.WhoCanBan,
                        "', walls_height = '", room.WallHeight,
                        "', chat_type = @chat_t,chat_balloon = @chat_b,chat_speed = @chat_s,chat_max_distance = @chat_m,chat_flood_protection = @chat_f WHERE id = ",
                        room.RoomId
                    }));
                    queryReactor.AddParameter("caption", room.Name);
                    queryReactor.AddParameter("description", room.Description);
                    queryReactor.AddParameter("password", room.Password);
                    queryReactor.AddParameter("tags", stringBuilder.ToString());
                    queryReactor.AddParameter("chat_t", room.ChatType);
                    queryReactor.AddParameter("chat_b", room.ChatBalloon);
                    queryReactor.AddParameter("chat_s", room.ChatSpeed);
                    queryReactor.AddParameter("chat_m", room.ChatMaxDistance);
                    queryReactor.AddParameter("chat_f", room.ChatFloodProtection);
                    queryReactor.RunQuery();
                }
                lock (_roomsToRemoveQueue.SyncRoot)
                {
                    _roomsToRemoveQueue.Enqueue(room.RoomId);
                }
                Logging.WriteLine(string.Format("[RoomMgr] Room #[{0}] was unloaded, reason: " + reason, room.RoomId), ConsoleColor.Yellow);
                foreach (var current in room.GetRoomUserManager().UserList.Values)
                {
                    if (current == null)
                        continue;
                    if (current.IsPet)
                        using (var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            queryreactor2.SetQuery("UPDATE bots SET x=@x, y=@y, z=@z WHERE id=@id LIMIT 1;");
                            queryreactor2.AddParameter("x", current.X);
                            queryreactor2.AddParameter("y", current.Y);
                            queryreactor2.AddParameter("z", current.Z);
                            queryreactor2.AddParameter("id", current.PetData.PetId);
                            queryreactor2.RunQuery();
                            goto IL_4AA;
                        }
                    goto IL_38A;
                    IL_4AA:
                    if (current.GetClient() == null)
                        continue;
                    room.GetRoomUserManager().RemoveUserFromRoom(current.GetClient(), true, false);
                    current.GetClient().CurrentRoomUserId = -1;
                    continue;
                    IL_38A:
                    if (current.IsBot)
                        using (var queryreactor3 = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            queryreactor3.SetQuery(
                                "UPDATE bots SET x=@x, y=@y, z=@z, name=@name, motto=@motto, look=@look, rotation=@rotation, dance=@dance WHERE id=@id LIMIT 1;");
                            queryreactor3.AddParameter("name", current.BotData.Name);
                            queryreactor3.AddParameter("motto", current.BotData.Motto);
                            queryreactor3.AddParameter("look", current.BotData.Look);
                            queryreactor3.AddParameter("rotation", current.BotData.Rot);
                            queryreactor3.AddParameter("dance", current.BotData.DanceId);
                            queryreactor3.AddParameter("x", current.X);
                            queryreactor3.AddParameter("y", current.Y);
                            queryreactor3.AddParameter("z", current.Z);
                            queryreactor3.AddParameter("id", current.BotData.BotId);
                            queryreactor3.RunQuery();
                        }
                    goto IL_4AA;
                }
                lock (room.RoomChat)
                {
                    foreach (var current2 in room.RoomChat)
                        current2.Save(room.RoomId);
                }
                room.Destroy();
            }
        }

        private void SortActiveRooms() { _orderedActiveRooms = _activeRooms.OrderByDescending(t => t.Value).Take(40); }

        private void SortVotedRooms() { _orderedVotedRooms = _votedRooms.OrderByDescending(t => t.Value).Take(40); }

        private bool WorkActiveRoomsUpdateQueue()
        {
            if (_activeRoomsUpdateQueue.Count <= 0)
                return false;
            lock (_activeRoomsUpdateQueue.SyncRoot)
            {
                while (_activeRoomsUpdateQueue.Count > 0)
                {
                    var roomData = (RoomData)_activeRoomsUpdateQueue.Dequeue();
                    if (roomData.ModelName.Contains("snowwar"))
                        continue;
                    if (!_activeRooms.ContainsKey(roomData))
                        _activeRooms.Add(roomData, roomData.UsersNow);
                    else
                        _activeRooms[roomData] = roomData.UsersNow;
                }
            }
            return true;
        }

        private bool WorkActiveRoomsAddQueue()
        {
            if (_activeRoomsAddQueue.Count <= 0)
                return false;
            lock (_activeRoomsAddQueue.SyncRoot)
            {
                while (_activeRoomsAddQueue.Count > 0)
                {
                    var roomData = (RoomData)_activeRoomsAddQueue.Dequeue();
                    if (!_activeRooms.ContainsKey(roomData) && !roomData.ModelName.Contains("snowwar"))
                        _activeRooms.Add(roomData, roomData.UsersNow);
                }
            }
            return true;
        }

        private bool WorkActiveRoomsRemoveQueue()
        {
            if (ActiveRoomsRemoveQueue.Count <= 0)
                return false;
            lock (ActiveRoomsRemoveQueue.SyncRoot)
            {
                while (ActiveRoomsRemoveQueue.Count > 0)
                {
                    var key = (RoomData)ActiveRoomsRemoveQueue.Dequeue();
                    _activeRooms.Remove(key);
                }
            }
            return true;
        }

        private bool WorkVotedRoomsAddQueue()
        {
            if (_votedRoomsAddQueue.Count <= 0)
                return false;
            lock (_votedRoomsAddQueue.SyncRoot)
            {
                while (_votedRoomsAddQueue.Count > 0)
                {
                    var roomData = (RoomData)_votedRoomsAddQueue.Dequeue();
                    if (!_votedRooms.ContainsKey(roomData))
                        _votedRooms.Add(roomData, roomData.Score);
                    else
                        _votedRooms[roomData] = roomData.Score;
                }
            }
            return true;
        }

        private bool WorkVotedRoomsRemoveQueue()
        {
            if (_votedRoomsRemoveQueue.Count <= 0)
                return false;
            lock (_votedRoomsRemoveQueue.SyncRoot)
            {
                while (_votedRoomsRemoveQueue.Count > 0)
                {
                    var key = (RoomData)_votedRoomsRemoveQueue.Dequeue();
                    _votedRooms.Remove(key);
                }
            }
            return true;
        }

        private void WorkRoomsToAddQueue()
        {
            if (_roomsToAddQueue.Count <= 0)
                return;
            lock (_roomsToAddQueue.SyncRoot)
            {
                while (_roomsToAddQueue.Count > 0)
                {
                    var room = (Room)_roomsToAddQueue.Dequeue();
                    if (!LoadedRooms.ContainsKey(room.RoomId))
                        LoadedRooms.Add(room.RoomId, room);
                }
            }
        }

        private void WorkRoomsToRemoveQueue()
        {
            if (_roomsToRemoveQueue.Count <= 0)
                return;
            lock (_roomsToRemoveQueue.SyncRoot)
            {
                while (_roomsToRemoveQueue.Count > 0)
                {
                    var key = (uint)_roomsToRemoveQueue.Dequeue();
                    LoadedRooms.Remove(key);
                }
            }
        }

        private void WorkRoomDataQueue()
        {
            if (_roomDataToAddQueue.Count <= 0)
                return;
            lock (_roomDataToAddQueue.SyncRoot)
            {
                while (_roomDataToAddQueue.Count > 0)
                {
                    var roomData = (RoomData)_roomDataToAddQueue.Dequeue();
                    if (!LoadedRooms.ContainsKey(roomData.Id))
                        _loadedRoomData.Add(roomData.Id, roomData);
                }
            }
        }
    }
}