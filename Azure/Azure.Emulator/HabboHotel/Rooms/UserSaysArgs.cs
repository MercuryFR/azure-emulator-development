using System;

namespace Azure.HabboHotel.Rooms
{
    public class UserSaysArgs : EventArgs
    {
        internal readonly RoomUser User;
        internal readonly string Message;

        public UserSaysArgs(RoomUser user, string message)
        {
            this.User = user;
            this.Message = message;
        }
    }
}