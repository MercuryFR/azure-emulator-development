using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Globalization;
using System.Linq;
using Azure.Configuration;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.Collections;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Navigators;
using Azure.HabboHotel.PathFinding;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.Quests;
using Azure.HabboHotel.RoomBots;
using Azure.HabboHotel.Rooms.Games;
using Azure.Messages;
using Azure.Messages.Parsers;
using Azure.Util;
using Azure.HabboHotel.Pathfinding;

namespace Azure.HabboHotel.Rooms
{
    class RoomUserManager
    {
        internal HybridDictionary UsersByUserName;
        internal HybridDictionary UsersByUserId;
        internal Dictionary<Point, RoomUser> ToSet;
        private readonly List<RoomUser> _toRemove;
        private Room _room;
        private HybridDictionary _pets;
        private HybridDictionary _bots;
        private int _userCount;
        private int _primaryPrivateUserId;
        private int _secondaryPrivateUserId;

        public RoomUserManager(Room room)
        {
            _room = room;
            UserList = new QueuedDictionary<int, RoomUser>(OnUserAdd, null, OnRemove, null);
            _pets = new HybridDictionary();
            _bots = new HybridDictionary();
            UsersByUserName = new HybridDictionary();
            UsersByUserId = new HybridDictionary();
            _primaryPrivateUserId = 0;
            _secondaryPrivateUserId = 0;
            _toRemove = new List<RoomUser>(room.UsersMax);
            ToSet = new Dictionary<Point, RoomUser>();
            PetCount = 0;
            _userCount = 0;
        }

        internal event RoomEventDelegate OnUserEnter;

        internal int PetCount { get; private set; }

        internal QueuedDictionary<int, RoomUser> UserList { get; private set; }

        public RoomUser GetRoomUserByHabbo(uint pId)
        {
            return UsersByUserId.Contains(pId) ? (RoomUser) UsersByUserId[pId] : null;
        }

        internal int GetRoomUserCount() { return (UserList.Inner.Count - _bots.Count - _pets.Count); }

        internal RoomUser DeployBot(RoomBot bot, Pet petData)
        {
            var virtualId = _primaryPrivateUserId++;
            var roomUser = new RoomUser(0u, _room.RoomId, virtualId, _room, false);
            var num = _secondaryPrivateUserId++;
            roomUser.InternalRoomId = num;
            UserList.Add(num, roomUser);
            var model = _room.GetGameMap().Model;
            var coord = new Point(bot.X, bot.Y);
            if (bot.X > 0 && bot.Y > 0 && bot.X < model.MapSizeX && bot.Y < model.MapSizeY)
            {
                _room.GetGameMap().AddUserToMap(roomUser, coord);
                roomUser.SetPos(bot.X, bot.Y, bot.Z);
                roomUser.SetRot(bot.Rot, false);
            }
            else
            {
                bot.X = model.DoorX;
                bot.Y = model.DoorY;
                roomUser.SetPos(model.DoorX, model.DoorY, model.DoorZ);
                roomUser.SetRot(model.DoorOrientation, false);
            }

            bot.RoomUser = roomUser;
            roomUser.BotData = bot;

            checked
            {
                roomUser.BotAI = bot.GenerateBotAI(roomUser.VirtualId, (int) bot.BotId);
                if (roomUser.IsPet)
                {
                    roomUser.BotAI.Init(bot.BotId, roomUser.VirtualId, _room.RoomId, roomUser, _room);
                    roomUser.PetData = petData;
                    roomUser.PetData.VirtualId = roomUser.VirtualId;
                }
                else
                    roomUser.BotAI.Init(bot.BotId, roomUser.VirtualId, _room.RoomId, roomUser, _room);

                UpdateUserStatus(roomUser, false);
                roomUser.UpdateNeeded = true;
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SetRoomUserMessageComposer"));
                serverMessage.AppendInteger(1);
                roomUser.Serialize(serverMessage, _room.GetGameMap().GotPublicPool);
                _room.SendMessage(serverMessage);
                roomUser.BotAI.OnSelfEnterRoom();
                if (roomUser.IsPet)
                {
                    if (_pets.Contains(roomUser.PetData.PetId))
                        _pets[roomUser.PetData.PetId] = roomUser;
                    else
                        _pets.Add(roomUser.PetData.PetId, roomUser);
                    PetCount++;
                }

                roomUser.BotAI.Modified();
                if (roomUser.BotData.AiType != AIType.Generic)
                    return roomUser;
                if (_bots.Contains(roomUser.BotData.BotId))
                    _bots[roomUser.BotData.BotId] = roomUser;
                else
                    _bots.Add(roomUser.BotData.BotId, roomUser);
                serverMessage.Init(LibraryParser.OutgoingRequest("DanceStatusMessageComposer"));
                serverMessage.AppendInteger(roomUser.VirtualId);
                serverMessage.AppendInteger(roomUser.BotData.DanceId);
                _room.SendMessage(serverMessage);
                PetCount++;

                return roomUser;
            }
        }

        internal void UpdateBot(int virtualId, RoomUser roomUser, string name, string motto, string look, string gender,
            List<string> speech, List<string> responses, bool speak, int speechDelay, bool mix)
        {
            var bot = GetRoomUserByVirtualId(virtualId);
            checked
            {
                if (bot == null || !bot.IsBot)
                    return;

                var rBot = bot.BotData;

                rBot.Name = name;
                rBot.Motto = motto;
                rBot.Look = look;
                rBot.Gender = gender;
                rBot.RandomSpeech = speech;
                rBot.Responses = responses;
                rBot.AutomaticChat = speak;
                rBot.SpeechInterval = speechDelay;
                rBot.RoomUser = roomUser;
                rBot.MixPhrases = mix;

                if (rBot.RoomUser == null || rBot.RoomUser.BotAI == null)
                    return;

                rBot.RoomUser.BotAI.Modified();
            }
        }

        internal void RemoveBot(int virtualId, bool kicked)
        {
            var roomUserByVirtualId = GetRoomUserByVirtualId(virtualId);
            checked
            {
                if (roomUserByVirtualId == null || !roomUserByVirtualId.IsBot)
                    return;
                if (roomUserByVirtualId.IsPet)
                {
                    _pets.Remove(roomUserByVirtualId.PetData.PetId);
                    PetCount--;
                }
                roomUserByVirtualId.BotAI.OnSelfLeaveRoom(kicked);
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("UserLeftRoomMessageComposer"));
                serverMessage.AppendString(roomUserByVirtualId.VirtualId.ToString());
                _room.SendMessage(serverMessage);
                UserList.Remove(roomUserByVirtualId.InternalRoomId);
            }
        }

        internal RoomUser GetUserForSquare(int x, int y)
        {
            return _room.GetGameMap().GetRoomUsers(new Point(x, y)).FirstOrDefault();
        }

        internal void AddUserToRoom(GameClient session, bool spectator, bool snow = false)
        {
            if (session == null || session.GetHabbo() == null)
                return;
            var roomUser = new RoomUser(session.GetHabbo().Id, _room.RoomId, _primaryPrivateUserId++, _room, spectator);
            if (roomUser.GetClient() == null || roomUser.GetClient().GetHabbo() == null)
                return;

            roomUser.UserId = session.GetHabbo().Id;
            var userName = session.GetHabbo().UserName;
            var userId = roomUser.UserId;
            if (UsersByUserName.Contains(userName.ToLower()))
                UsersByUserName.Remove(userName.ToLower());
            if (UsersByUserId.Contains(userId))
                UsersByUserId.Remove(userId);
            UsersByUserName.Add(session.GetHabbo().UserName.ToLower(), roomUser);
            UsersByUserId.Add(session.GetHabbo().Id, roomUser);
            var num = _secondaryPrivateUserId++;
            roomUser.InternalRoomId = num;
            session.CurrentRoomUserId = num;
            session.GetHabbo().CurrentRoomId = _room.RoomId;
            UserList.Add(num, roomUser);

            if (Azure.GetGame().GetNavigator().PrivateCategories.Contains(_room.Category))
                ((FlatCat) Azure.GetGame().GetNavigator().PrivateCategories[_room.Category]).UsersNow++;
        }

        internal void UpdateUser(string oldName, string newName)
        {
            if (oldName == newName)
                return;

            if (!UsersByUserName.Contains(oldName))
                return;
            UsersByUserName.Add(newName, UsersByUserName[oldName]);
            UsersByUserName.Remove(oldName);
            // 
            Azure.GetGame().GetClientManager().UpdateClient(oldName, newName);
        }

        internal void RequestRoomReload() { UserList.QueueDelegate(_room.OnReload); }

        internal void UpdateUserStats(HashSet<RoomUser> users, Hashtable userId, Hashtable Username, int primaryId,
            int secondaryId)
        {
            foreach (var current in users)
                UserList.Inner.TryAdd(current.InternalRoomId, current);
            foreach (RoomUser roomUser in Username.Values)
                UsersByUserName.Add(roomUser.GetClient().GetHabbo().UserName.ToLower(), roomUser);
            foreach (RoomUser roomUser2 in userId.Values)
                UsersByUserId.Add(roomUser2.UserId, roomUser2);
            _primaryPrivateUserId = primaryId;
            _secondaryPrivateUserId = secondaryId;
        }

        internal void RemoveUserFromRoom(GameClient session, bool notifyClient, bool notifyKick)
        {
            try
            {
                if (session == null || session.GetHabbo() == null || _room == null)
                    return;
                session.GetHabbo().GetAvatarEffectsInventoryComponent().OnRoomExit();
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                    queryReactor.RunFastQuery(string.Concat(new object[]
                    {
                        "UPDATE users_rooms_visits SET exit_timestamp = '",
                        Azure.GetUnixTimestamp(),
                        "' WHERE room_id = '",
                        _room.RoomId,
                        "' AND user_id = '",
                        session.GetHabbo().Id,
                        "' ORDER BY entry_timestamp DESC LIMIT 1"
                    }));
                var roomUserByHabbo = GetRoomUserByHabbo(session.GetHabbo().Id);
                if (roomUserByHabbo == null)
                    return;
                if (notifyKick)
                {
                    var room = Azure.GetGame().GetRoomManager().GetRoom(roomUserByHabbo.RoomId);
                    var model = room.GetGameMap().Model;
                    roomUserByHabbo.MoveTo(model.DoorX, model.DoorY);
                    roomUserByHabbo.CanWalk = false;
                    session.GetMessageHandler()
                        .GetResponse()
                        .Init(LibraryParser.OutgoingRequest("RoomErrorMessageComposer"));
                    session.GetMessageHandler().GetResponse().AppendInteger(4008);
                    session.GetMessageHandler().SendResponse();

                    session.GetMessageHandler()
                        .GetResponse()
                        .Init(LibraryParser.OutgoingRequest("OutOfRoomMessageComposer"));
                    session.GetMessageHandler().GetResponse().AppendShort(2);
                    session.GetMessageHandler().SendResponse();
                }
                else if (notifyClient)
                {
                    var serverMessage =
                        new ServerMessage(LibraryParser.OutgoingRequest("UserIsPlayingFreezeMessageComposer"));
                    serverMessage.AppendBool(roomUserByHabbo.Team != Team.none);
                    roomUserByHabbo.GetClient().SendMessage(serverMessage);
                    session.GetMessageHandler()
                        .GetResponse()
                        .Init(LibraryParser.OutgoingRequest("OutOfRoomMessageComposer"));
                    session.GetMessageHandler().GetResponse().AppendShort(2);
                    session.GetMessageHandler().SendResponse();
                }
                if (roomUserByHabbo.Team != Team.none)
                {
                    _room.GetTeamManagerForBanzai().OnUserLeave(roomUserByHabbo);
                    _room.GetTeamManagerForFreeze().OnUserLeave(roomUserByHabbo);
                }
                if (roomUserByHabbo.RidingHorse)
                {
                    roomUserByHabbo.RidingHorse = false;
                    var horse = GetRoomUserByVirtualId((int) roomUserByHabbo.HorseId);
                    if (horse != null)
                    {
                        horse.RidingHorse = false;
                        horse.HorseId = 0u;
                    }
                }
                if (roomUserByHabbo.IsLyingDown || roomUserByHabbo.IsSitting)
                {
                    roomUserByHabbo.IsSitting = false;
                    roomUserByHabbo.IsLyingDown = false;
                }
                RemoveRoomUser(roomUserByHabbo);
                if (session.GetHabbo() != null && !roomUserByHabbo.IsSpectator)
                {
                    if (roomUserByHabbo.CurrentItemEffect != ItemEffectType.None)
                        roomUserByHabbo.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect = -1;
                    if (session.GetHabbo() != null)
                    {
                        if (_room.HasActiveTrade(session.GetHabbo().Id))
                            _room.TryStopTrade(session.GetHabbo().Id);
                        session.GetHabbo().CurrentRoomId = 0;
                        if (session.GetHabbo().GetMessenger() != null)
                            session.GetHabbo().GetMessenger().OnStatusChanged(true);
                    }

                    using (var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
                        if (session.GetHabbo() != null)
                            queryreactor2.RunFastQuery(string.Concat(new object[]
                            {
                                "UPDATE users_rooms_visits SET exit_timestamp = '",
                                Azure.GetUnixTimestamp(),
                                "' WHERE room_id = '",
                                _room.RoomId,
                                "' AND user_id = '",
                                session.GetHabbo().Id,
                                "' ORDER BY exit_timestamp DESC LIMIT 1"
                            }));
                }
                UsersByUserId.Remove(roomUserByHabbo.UserId);
                if (session.GetHabbo() != null)
                    UsersByUserName.Remove(session.GetHabbo().UserName.ToLower());
                roomUserByHabbo.Dispose();
            }
            catch (Exception ex)
            {
                Logging.LogCriticalException(string.Format("Error during removing user from room:{0}", ex));
            }
        }

        internal void RemoveRoomUser(RoomUser user)
        {
            UserList.Remove(user.InternalRoomId);
            user.InternalRoomId = -1;
            _room.GetGameMap().GameMap[user.X, user.Y] = user.SqState;
            _room.GetGameMap().RemoveUserFromMap(user, new Point(user.X, user.Y));

            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("UserLeftRoomMessageComposer"));
            serverMessage.AppendString(user.VirtualId.ToString());
            _room.SendMessage(serverMessage);
        }

        internal RoomUser GetPet(uint petId)
        {
            if (_pets.Contains(petId))
                return (RoomUser) _pets[petId];
            return null;
        }

        internal RoomUser GetBot(uint botId)
        {
            if (_bots.Contains(botId))
                return (RoomUser) _bots[botId];
            Logging.WriteLine(string.Format("Couldn't get BOT: {0}", botId), ConsoleColor.Blue);
            return null;
        }

        internal void UpdateUserCount(int count)
        {
            _userCount = count;
            _room.RoomData.UsersNow = count;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE rooms_data SET users_now = ",
                    count,
                    " WHERE id = ",
                    _room.RoomId,
                    " LIMIT 1"
                }));
            Azure.GetGame().GetRoomManager().QueueActiveRoomUpdate(_room.RoomData);
        }

        internal RoomUser GetRoomUserByVirtualId(int virtualId) { return UserList.GetValue(virtualId); }

        internal List<RoomUser> GetUsersInCampingTent() { return GetRoomUsers().Where(x => x.OnCampingTent).ToList(); }

        internal HashSet<RoomUser> GetRoomUsers()
        {
            return new HashSet<RoomUser>(UserList.Values.Where(x => x.IsBot == false));
        }

        internal List<RoomUser> GetRoomUserByRank(int minRank)
        {
            return
                UserList.Values.Where(
                    current =>
                        !current.IsBot && current.GetClient() != null && current.GetClient().GetHabbo() != null &&
                        current.GetClient().GetHabbo().Rank > (ulong) minRank).ToList();
        }

        internal RoomUser GetRoomUserByHabbo(string pName)
        {
            if (UsersByUserName.Contains(pName.ToLower()))
                return (RoomUser) UsersByUserName[pName.ToLower()];
            return null;
        }

        internal void SavePets(IQueryAdapter dbClient)
        {
            try
            {
                if (GetPets().Any())
                    AppendPetsUpdateString(dbClient);
            }
            catch (Exception ex)
            {
                Logging.LogCriticalException(string.Concat(new object[]
                {
                    "Error during saving furniture for room ",
                    _room.RoomId,
                    ". Stack: ",
                    ex.ToString()
                }));
            }
        }

        internal void AppendPetsUpdateString(IQueryAdapter dbClient)
        {
            var queryChunk = new QueryChunk("INSERT INTO bots (id,user_id,room_id,name,x,y,z) VALUES ");
            var queryChunk2 =
                new QueryChunk(
                    "INSERT INTO pets_data (type,race,color,experience,energy,createstamp,nutrition,respect) VALUES ");
            var queryChunk3 = new QueryChunk();
            var list = new List<uint>();
            foreach (var current in GetPets().Where(current => !list.Contains(current.PetId)))
            {
                list.Add(current.PetId);
                switch (current.DbState)
                {
                    case DatabaseUpdateState.NeedsInsert:
                        queryChunk.AddParameter(string.Format("{0}name", current.PetId), current.Name);
                        queryChunk2.AddParameter(string.Format("{0}race", current.PetId), current.Race);
                        queryChunk2.AddParameter(string.Format("{0}color", current.PetId), current.Color);
                        queryChunk.AddQuery(string.Concat(new object[]
                        {
                            "(",
                            current.PetId,
                            ",",
                            current.OwnerId,
                            ",",
                            current.RoomId,
                            ",@",
                            current.PetId,
                            "name,", current.X, ",", current.Y, ",", current.Z, ")"
                        }));
                        queryChunk2.AddQuery(string.Concat(new object[]
                        {
                            "(",
                            current.Type,
                            ",@",
                            current.PetId,
                            "race,@",
                            current.PetId,
                            "color,0,100,'",
                            current.CreationStamp,
                            "',0,0)"
                        }));
                        break;
                    case DatabaseUpdateState.NeedsUpdate:
                        queryChunk3.AddParameter(string.Format("{0}name", current.PetId), current.Name);
                        queryChunk3.AddParameter(string.Format("{0}race", current.PetId), current.Race);
                        queryChunk3.AddParameter(string.Format("{0}color", current.PetId), current.Color);
                        queryChunk3.AddQuery(string.Concat(new object[]
                        {
                            "UPDATE bots SET room_id = ",
                            current.RoomId,
                            ", name = @",
                            current.PetId,
                            "name, x = ",
                            current.X,
                            ", Y = ",
                            current.Y,
                            ", Z = ",
                            current.Z,
                            " WHERE id = ",
                            current.PetId
                        }));
                        queryChunk3.AddQuery(string.Concat(new object[]
                        {
                            "UPDATE pets_data SET race = @",
                            current.PetId,
                            "race, color = @",
                            current.PetId,
                            "color, type = ",
                            current.Type,
                            ", experience = ",
                            current.Experience,
                            ", energy = ",
                            current.Energy,
                            ", nutrition = ",
                            current.Nutrition,
                            ", respect = ",
                            current.Respect,
                            ", createstamp = '",
                            current.CreationStamp,
                            "' WHERE id = ",
                            current.PetId
                        }));
                        break;
                }
                current.DbState = DatabaseUpdateState.Updated;
            }
            queryChunk.Execute(dbClient);
            queryChunk3.Execute(dbClient);
            queryChunk.Dispose();
            queryChunk3.Dispose();
            queryChunk = null;
            queryChunk3 = null;
        }

        internal List<Pet> GetPets()
        {
            var list = UserList.ToList();
            return
                (from current in list select current.Value into value where value.IsPet select value.PetData).ToList();
        }

        internal ServerMessage SerializeStatusUpdates(bool all)
        {
            var list = new List<RoomUser>();
            foreach (var current in UserList.Values)
            {
                if (!all)
                {
                    if (!current.UpdateNeeded)
                        continue;
                    current.UpdateNeeded = false;
                }
                list.Add(current);
            }
            if (!list.Any())
                return null;

            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("UpdateUserStatusMessageComposer"));
            serverMessage.AppendInteger(list.Count);
            foreach (var current2 in list)
                current2.SerializeStatus(serverMessage);
            return serverMessage;
        }

        internal void UpdateUserStatusses()
        {
            OnCycleDoneDelegate function = OnUserUpdateStatus;
            UserList.QueueDelegate(function);
        }

        internal void BackupCounters(ref int primaryCounter, ref int secondaryCounter)
        {
            primaryCounter = _primaryPrivateUserId;
            secondaryCounter = _secondaryPrivateUserId;
        }

        internal void UpdateUserStatus(RoomUser user, bool cyclegameitems)
        {
            try
            {
                if (user == null)
                    return;
                var isBot = user.IsBot;
                if (isBot)
                    cyclegameitems = false;
                var coordItemSearch = new CoordItemSearch(_room.GetGameMap().CoordinatedItems);
                var allRoomItemForSquare = coordItemSearch.GetAllRoomItemForSquare(user.X, user.Y);
                var ItemsOnSquare = _room.GetGameMap().GetCoordinatedItems(new Point(user.X, user.Y));
                var newZ = _room.GetGameMap().SqAbsoluteHeight(user.X, user.Y, ItemsOnSquare) +
                           ((user.RidingHorse && user.IsPet == false) ? 1 : 0);
                if (newZ != user.Z)
                {
                    user.Z = newZ;
                    user.UpdateNeeded = true;
                }
                var model = _room.GetGameMap().Model;
                if (model.SqState[user.X][user.Y] == SquareState.Seat || user.IsSitting || user.IsLyingDown)
                    if (user.IsSitting)
                    {
                        var height = Convert.ToString(model.SqFloorHeight[user.X][user.Y] + 0.55);
                        if (!user.Statusses.ContainsKey("sit"))
                        {
                            user.Statusses.Add("sit", height);
                            user.Z = model.SqFloorHeight[user.X][user.Y];
                            user.UpdateNeeded = true;
                        }
                        else if (height != user.Statusses["sit"])
                        {
                            user.Statusses["sit"] = height;
                            user.Z = model.SqFloorHeight[user.X][user.Y];
                            user.UpdateNeeded = true;
                        }
                    }
                    else if (user.IsLyingDown)
                    {
                        if (!user.Statusses.ContainsKey("lay"))
                            user.Statusses.Add("lay", Convert.ToString(model.SqFloorHeight[user.X][user.Y] + 0.55));
                        user.Z = model.SqFloorHeight[user.X][user.Y];
                        user.UpdateNeeded = true;
                    }
                    else
                    {
                        if (!user.Statusses.ContainsKey("sit"))
                            user.Statusses.Add("sit", "1.0");
                        user.Z = model.SqFloorHeight[user.X][user.Y];
                        user.RotHead = model.SqSeatRot[user.X][user.Y];
                        user.RotBody = model.SqSeatRot[user.X][user.Y];
                        user.UpdateNeeded = true;
                    }
                if (!allRoomItemForSquare.Any())
                    user.LastItem = 0;
                using (var enumerator = allRoomItemForSquare.GetEnumerator())
                    while (enumerator.MoveNext())
                    {
                        var item = enumerator.Current;
                        if (cyclegameitems)
                        {
                            item.UserWalksOnFurni(user);
                            Azure.GetGame()
                                .GetQuestManager()
                                .ProgressUserQuest(user.GetClient(), QuestType.StandOn, item.GetBaseItem().ItemId);
                        }
                        if (item.GetBaseItem().IsSeat)
                        {
                            if (!user.Statusses.ContainsKey("sit"))
                                if (item.GetBaseItem().StackMultipler && !string.IsNullOrWhiteSpace(item.ExtraData))
                                    if (item.ExtraData != "0")
                                    {
                                        var num2 = Convert.ToInt32(item.ExtraData);
                                        user.Statusses.Add("sit",
                                            item.GetBaseItem().ToggleHeight[num2].ToString(CultureInfo.InvariantCulture)
                                                .Replace(',', '.'));
                                    }
                                    else
                                        user.Statusses.Add("sit", TextHandling.GetString(item.GetBaseItem().Height));
                                else
                                    user.Statusses.Add("sit", TextHandling.GetString(item.GetBaseItem().Height));
                            user.Z = item.Z;
                            user.RotHead = item.Rot;
                            user.RotBody = item.Rot;
                            user.UpdateNeeded = true;
                        }
                        var interactionType = item.GetBaseItem().InteractionType;
                        checked
                        {
                            switch (interactionType)
                            {
                                case InteractionType.gld_gate:
                                {
                                    item.Interactor.OnUserWalk(user.GetClient(), item, user);
                                    break;
                                }
                                case InteractionType.none:
                                    break;
                                case InteractionType.bed:
                                {
                                    if (!user.Statusses.ContainsKey("lay"))
                                        user.Statusses.Add("lay",
                                            string.Format("{0} null", TextHandling.GetString(item.GetBaseItem().Height)));
                                    user.Z = item.Z;
                                    break;
                                }
                                case InteractionType.fbgate:
                                    break;
                                case InteractionType.banzaigateblue:
                                case InteractionType.banzaigatered:
                                case InteractionType.banzaigateyellow:
                                case InteractionType.banzaigategreen:
                                {
                                    if (!cyclegameitems)
                                        break;
                                    var num3 = (int) (item.Team + 32);
                                    var teamManagerForBanzai =
                                        user.GetClient().GetHabbo().CurrentRoom.GetTeamManagerForBanzai();
                                    var avatarEffectsInventoryComponent =
                                        user.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent();
                                    if (user.Team == Team.none)
                                    {
                                        if (!teamManagerForBanzai.CanEnterOnTeam(item.Team))
                                            break;
                                        if (user.Team != Team.none)
                                            teamManagerForBanzai.OnUserLeave(user);
                                        user.Team = item.Team;
                                        teamManagerForBanzai.AddUser(user);
                                        if (avatarEffectsInventoryComponent.CurrentEffect != num3)
                                            avatarEffectsInventoryComponent.ActivateCustomEffect(num3);
                                        break;
                                    }
                                    if (user.Team != Team.none && user.Team != item.Team)
                                    {
                                        teamManagerForBanzai.OnUserLeave(user);
                                        user.Team = Team.none;
                                        avatarEffectsInventoryComponent.ActivateCustomEffect(0);
                                        break;
                                    }
                                    teamManagerForBanzai.OnUserLeave(user);
                                    if (avatarEffectsInventoryComponent.CurrentEffect == num3)
                                        avatarEffectsInventoryComponent.ActivateCustomEffect(0);
                                    user.Team = Team.none;
                                    break;
                                }
                                case InteractionType.jump:
                                    break;
                                case InteractionType.pinata:
                                {
                                    if (!user.IsWalking || item.ExtraData.Length <= 0)
                                        break;
                                    var num5 = int.Parse(item.ExtraData);
                                    if (num5 >= 100 || user.CurrentEffect != 158)
                                        break;
                                    var num6 = num5 + 1;
                                    item.ExtraData = num6.ToString();
                                    item.UpdateState();
                                    Azure.GetGame()
                                        .GetAchievementManager()
                                        .ProgressUserAchievement(user.GetClient(), "ACH_PinataWhacker", 1, false);
                                    if (num6 == 100)
                                    {
                                        Azure.GetGame().GetPinataHandler().DeliverRandomPinataItem(user, _room, item);
                                        Azure.GetGame()
                                            .GetAchievementManager()
                                            .ProgressUserAchievement(user.GetClient(), "ACH_PinataBreaker", 1, false);
                                    }
                                    break;
                                }
                                case InteractionType.tilestackmagic:
                                case InteractionType.poster:
                                    break;
                                case InteractionType.tent:
                                case InteractionType.bedtent:
                                    if (user.LastItem == item.Id)
                                        break;
                                    if (!user.IsBot && !user.OnCampingTent)
                                    {
                                        var serverMessage22 = new ServerMessage();
                                        serverMessage22.Init(
                                            LibraryParser.OutgoingRequest("UpdateFloorItemExtraDataMessageComposer"));
                                        serverMessage22.AppendString(item.Id.ToString());
                                        serverMessage22.AppendInteger(0);
                                        serverMessage22.AppendString("1");
                                        user.GetClient().SendMessage(serverMessage22);
                                        user.OnCampingTent = true;
                                        user.LastItem = item.Id;
                                    }
                                    break;
                                case InteractionType.runwaysage:
                                {
                                    var num7 = new Random().Next(1, 4);
                                    item.ExtraData = num7.ToString();
                                    item.UpdateState();
                                    break;
                                }
                                case InteractionType.shower:
                                {
                                    item.ExtraData = "1";
                                    item.UpdateState();
                                    break;
                                }
                                case InteractionType.banzaitele:
                                {
                                    _room.GetGameItemHandler().OnTeleportRoomUserEnter(user, item);
                                    break;
                                }
                                case InteractionType.freezeyellowgate:
                                case InteractionType.freezeredgate:
                                case InteractionType.freezegreengate:
                                case InteractionType.freezebluegate:
                                {
                                    if (cyclegameitems)
                                    {
                                        var num4 = (int) (item.Team + 39);
                                        var teamManagerForFreeze =
                                            user.GetClient().GetHabbo().CurrentRoom.GetTeamManagerForFreeze();
                                        var avatarEffectsInventoryComponent2 =
                                            user.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent();
                                        if (user.Team != item.Team)
                                        {
                                            if (teamManagerForFreeze.CanEnterOnTeam(item.Team))
                                            {
                                                if (user.Team != Team.none)
                                                    teamManagerForFreeze.OnUserLeave(user);
                                                user.Team = item.Team;
                                                teamManagerForFreeze.AddUser(user);
                                                if (avatarEffectsInventoryComponent2.CurrentEffect != num4)
                                                    avatarEffectsInventoryComponent2.ActivateCustomEffect(num4);
                                            }
                                        }
                                        else
                                        {
                                            teamManagerForFreeze.OnUserLeave(user);
                                            if (avatarEffectsInventoryComponent2.CurrentEffect == num4)
                                                avatarEffectsInventoryComponent2.ActivateCustomEffect(0);
                                            user.Team = Team.none;
                                        }
                                        var serverMessage33 =
                                            new ServerMessage(
                                                LibraryParser.OutgoingRequest("UserIsPlayingFreezeMessageComposer"));
                                        serverMessage33.AppendBool(user.Team != Team.none);
                                        user.GetClient().SendMessage(serverMessage33);
                                    }
                                    break;
                                }
                                default:
                                    break;
                            }

                            if (item.GetBaseItem().InteractionType == InteractionType.bedtent)
                                user.OnCampingTent = true;
                        }
                        user.LastItem = item.Id;
                    }
                if (user.IsSitting && user.TeleportEnabled)
                {
                    user.Z -= 0.35;
                    user.UpdateNeeded = true;
                }
                if (!cyclegameitems)
                    return;
                if (_room.GotSoccer())
                    _room.GetSoccer().OnUserWalk(user);
                if (_room.GotBanzai())
                    _room.GetBanzai().OnUserWalk(user);
                _room.GetFreeze().OnUserWalk(user);
            }
            catch (Exception e)
            {
                Logging.HandleException(e, "RoomUserManager.cs:UpdateUserStatus");
            }
        }

        internal void TurnHeads(int x, int y, uint senderId)
        {
            foreach (
                var current in
                    UserList.Values.Where(
                        current => current.HabboId != senderId && !current.RidingHorse && !current.IsPet))
                current.SetRot(PathFinder.CalculateRotation(current.X, current.Y, x, y), true);
        }

        internal void OnCycle(ref int idleCount)
        {
            _toRemove.Clear();
            var count = 0;
            foreach (var roomUser in UserList.Values)
            {
                if (!IsValid(roomUser))
                    if (roomUser.GetClient() != null)
                        RemoveUserFromRoom(roomUser.GetClient(), false, false);
                    else
                        RemoveRoomUser(roomUser);

                roomUser.IdleTime++;

                if (roomUser.IsPet || roomUser.IsBot)
                    if (!roomUser.IsWalking && roomUser.Statusses.ContainsKey("mv"))
                        roomUser.ClearMovement(true);

                if (!roomUser.IsAsleep && roomUser.IdleTime >= 600 && !roomUser.IsBot && !roomUser.IsPet)
                {
                    roomUser.IsAsleep = true;

                    var sleepMsg = new ServerMessage(LibraryParser.OutgoingRequest("RoomUserIdleMessageComposer"));
                    sleepMsg.AppendInteger(roomUser.VirtualId);
                    sleepMsg.AppendBool(true);
                    _room.SendMessage(sleepMsg);
                }
                if (roomUser.NeedsAutokick && !_toRemove.Contains(roomUser))
                {
                    _toRemove.Add(roomUser);
                    continue;
                }

                if (roomUser.CarryItemId > 0)
                {
                    roomUser.CarryTimer--;
                    if (roomUser.CarryTimer <= 0)
                        roomUser.CarryItem(0);
                }
                if (_room.GotFreeze())
                    Freeze.CycleUser(roomUser);

                var invalidStep = false;

                if (roomUser.SetStep)
                {
                    if (_room.GetGameMap().CanWalk(roomUser.SetX, roomUser.SetY, roomUser.AllowOverride) ||
                        roomUser.RidingHorse)
                    {
                        _room.GetGameMap()
                            .UpdateUserMovement(new Point(roomUser.Coordinate.X, roomUser.Coordinate.Y),
                                new Point(roomUser.SetX, roomUser.SetY), roomUser);
                        var items = _room.GetGameMap().GetCoordinatedItems(new Point(roomUser.X, roomUser.Y));
                        roomUser.X = roomUser.SetX;
                        roomUser.Y = roomUser.SetY;
                        roomUser.Z = roomUser.SetZ;
                        try
                        {
                            foreach (var itemE in items)
                            {
                                itemE.UserWalksOffFurni(roomUser);

                                switch (itemE.GetBaseItem().InteractionType)
                                {
                                    case InteractionType.tent:
                                    case InteractionType.bedtent:
                                        if (!roomUser.IsBot && roomUser.OnCampingTent)
                                        {
                                            var serverMessage = new ServerMessage();
                                            serverMessage.Init(
                                                LibraryParser.OutgoingRequest(
                                                    "UpdateFloorItemExtraDataMessageComposer"));
                                            serverMessage.AppendString(itemE.Id.ToString());
                                            serverMessage.AppendInteger(0);
                                            serverMessage.AppendString("0");
                                            roomUser.GetClient().SendMessage(serverMessage);
                                            roomUser.OnCampingTent = false;
                                        }
                                        break;
                                    case InteractionType.runwaysage:
                                    case InteractionType.shower:
                                        itemE.ExtraData = "0";
                                        itemE.UpdateState();
                                        break;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Writer.Writer.LogException("RoomUserManager OnCycle: " + Environment.NewLine + e);
                        }
                        if (roomUser.X == _room.GetGameMap().Model.DoorX &&
                            roomUser.Y == _room.GetGameMap().Model.DoorY && !_toRemove.Contains(roomUser) &&
                            !roomUser.IsBot)
                        {
                            _toRemove.Add(roomUser);
                            continue;
                        }
                        UpdateUserStatus(roomUser, true);
                    }
                    else
                        invalidStep = true;
                    roomUser.SetStep = false;
                }

                if (roomUser.PathRecalcNeeded)
                {
                    roomUser.Path.Clear();
                    roomUser.Path = PathFinder.FindPath(roomUser, _room.GetGameMap().DiagonalEnabled,
                        _room.GetGameMap(), new Vector2D(roomUser.X, roomUser.Y),
                        new Vector2D(roomUser.GoalX, roomUser.GoalY));

                    if (roomUser.Path.Count > 1)
                    {
                        roomUser.PathStep = 1;
                        roomUser.IsWalking = true;
                        roomUser.PathRecalcNeeded = false;
                    }
                    else
                    {
                        roomUser.PathRecalcNeeded = false;
                        roomUser.Path.Clear();
                    }
                }

                if (!roomUser.IsWalking || roomUser.Freezed)
                {
                    if (roomUser.Statusses.ContainsKey("mv"))
                    {
                        roomUser.RemoveStatus("mv");
                        roomUser.UpdateNeeded = true;
                    }
                }
                else
                {
                    if (invalidStep || (roomUser.PathStep >= roomUser.Path.Count) ||
                        (roomUser.GoalX == roomUser.X && roomUser.GoalY == roomUser.Y))
                        //No path found, or reached goal (:
                    {
                        roomUser.IsWalking = false;
                        roomUser.RemoveStatus("mv");
                        if (roomUser.IsPet && (roomUser.PetData.Type == 3 || roomUser.PetData.Type == 4) &&
                            roomUser.PetData.WaitingForBreading > 0 &&
                            (roomUser.PetData.BreadingTile.X == roomUser.X &&
                             roomUser.PetData.BreadingTile.Y == roomUser.Y))
                        {
                            roomUser.Freezed = true;
                            _room.GetGameMap().RemoveUserFromMap(roomUser, roomUser.Coordinate);
                            switch (roomUser.PetData.Type)
                            {
                                case 3:
                                    if (
                                        _room.GetRoomItemHandler().breedingTerrier[roomUser.PetData.WaitingForBreading]
                                            .PetsList.Count == 2)
                                    {
                                        var owner =
                                            Azure.GetGame()
                                                .GetClientManager()
                                                .GetClientByUserId(roomUser.PetData.OwnerId);
                                        if (owner != null)
                                            owner.SendMessage(PetBreeding.GetMessage(
                                                roomUser.PetData.WaitingForBreading,
                                                _room.GetRoomItemHandler().breedingTerrier[
                                                    roomUser.PetData.WaitingForBreading].PetsList[0],
                                                _room.GetRoomItemHandler().breedingTerrier[
                                                    roomUser.PetData.WaitingForBreading].PetsList[1]));
                                    }
                                    break;
                                case 4:
                                    if (
                                        _room.GetRoomItemHandler().breedingBear[roomUser.PetData.WaitingForBreading]
                                            .PetsList.Count == 2)
                                    {
                                        var owner =
                                            Azure.GetGame()
                                                .GetClientManager()
                                                .GetClientByUserId(roomUser.PetData.OwnerId);
                                        if (owner != null)
                                            owner.SendMessage(PetBreeding.GetMessage(
                                                roomUser.PetData.WaitingForBreading,
                                                _room.GetRoomItemHandler().breedingBear[
                                                    roomUser.PetData.WaitingForBreading].PetsList[0],
                                                _room.GetRoomItemHandler().breedingBear[
                                                    roomUser.PetData.WaitingForBreading].PetsList[1]));
                                    }
                                    break;
                            }
                        }
                        else if (roomUser.IsPet && (roomUser.PetData.Type == 3 || roomUser.PetData.Type == 4) &&
                                 roomUser.PetData.WaitingForBreading > 0)
                        {
                            roomUser.Freezed = false;
                            roomUser.PetData.WaitingForBreading = 0;
                            roomUser.PetData.BreadingTile = new Point();
                        }
                        roomUser.HandelingBallStatus = 0;
                        if (roomUser.RidingHorse && roomUser.IsPet == false)
                        {
                            var mascotaVinculada = GetRoomUserByVirtualId(Convert.ToInt32(roomUser.HorseId));
                            if (mascotaVinculada != null)
                            {
                                mascotaVinculada.IsWalking = false;
                                mascotaVinculada.RemoveStatus("mv");
                                var mess =
                                    new ServerMessage(
                                        LibraryParser.OutgoingRequest("UpdateUserStatusMessageComposer"));
                                mess.AppendInteger(1);
                                mascotaVinculada.SerializeStatus(mess, "");
                                roomUser.GetClient().GetHabbo().CurrentRoom.SendMessage(mess);
                            }
                        }
                        UpdateUserStatus(roomUser, false);
                    }
                    else
                    {
                        var s = (roomUser.Path.Count - roomUser.PathStep) - 1;
                        var nextStep = roomUser.Path[s];
                        roomUser.PathStep++;
                        var nextX = nextStep.X;
                        var nextY = nextStep.Y;
                        roomUser.RemoveStatus("mv");
                        if (_room.GetGameMap()
                            .IsValidStep3(roomUser, new Vector2D(roomUser.X, roomUser.Y), new Vector2D(nextX, nextY),
                                (roomUser.GoalX == nextX && roomUser.GoalY == nextY), roomUser.AllowOverride,
                                roomUser.GetClient()))
                        {
                            var nextZ = _room.GetGameMap().SqAbsoluteHeight(nextX, nextY);
                            if (roomUser.Statusses.ContainsKey("lay"))
                            {
                                roomUser.Statusses.Remove("lay");
                                roomUser.UpdateNeeded = true;
                            }
                            else if (roomUser.Statusses.ContainsKey("sit"))
                            {
                                roomUser.Statusses.Remove("sit");
                                roomUser.UpdateNeeded = true;
                            }
                            var user = "";
                            var mascote = "";
                            if (roomUser.RidingHorse && roomUser.IsPet == false)
                            {
                                user = ("mv " + nextX + "," + nextY + "," + TextHandling.GetString(nextZ + 1));
                                roomUser.AddStatus("mv",
                                    +nextX + "," + nextY + "," + TextHandling.GetString(nextZ + 1));
                                mascote = ("mv " + nextX + "," + nextY + "," + TextHandling.GetString(nextZ));
                            }
                            else
                                roomUser.AddStatus("mv", nextX + "," + nextY + "," + TextHandling.GetString(nextZ));
                            var newRot = Rotation.Calculate(roomUser.X, roomUser.Y, nextX, nextY,
                                roomUser.IsMoonwalking);
                            roomUser.RotBody = newRot;
                            roomUser.RotHead = newRot;
                            roomUser.SetStep = true;
                            roomUser.SetX = nextX;
                            roomUser.SetY = nextY;
                            roomUser.SetZ = nextZ;
                            if (_room.GotSoccer())
                                _room.GetSoccer().OnUserWalk(roomUser);
                            UpdateUserEffect(roomUser, roomUser.SetX, roomUser.SetY);
                            _room.GetGameMap().GameMap[roomUser.X, roomUser.Y] = roomUser.SqState;
                            // REstore the old one
                            roomUser.SqState = _room.GetGameMap().GameMap[roomUser.SetX, roomUser.SetY];
                            //Backup the new one
                            if (roomUser.IsSitting)
                                roomUser.IsSitting = false;
                            if (roomUser.IsLyingDown)
                                roomUser.IsLyingDown = false;
                            if (roomUser.RidingHorse && roomUser.IsPet == false)
                            {
                                var mascotaVinculada = GetRoomUserByVirtualId(Convert.ToInt32(roomUser.HorseId));
                                if (mascotaVinculada != null)
                                {
                                    mascotaVinculada.RotBody = newRot;
                                    mascotaVinculada.RotHead = newRot;
                                    mascotaVinculada.SetStep = true;
                                    mascotaVinculada.SetX = nextX;
                                    mascotaVinculada.SetY = nextY;
                                    mascotaVinculada.SetZ = nextZ;
                                    UpdateUserEffect(mascotaVinculada, mascotaVinculada.SetX, mascotaVinculada.SetY);
                                    var mess =
                                        new ServerMessage(
                                            LibraryParser.OutgoingRequest("UpdateUserStatusMessageComposer"));
                                    mess.AppendInteger(2);
                                    roomUser.SerializeStatus(mess, user);
                                    mascotaVinculada.SerializeStatus(mess, mascote);
                                    _room.SendMessage(mess);
                                    UpdateUserEffect(roomUser, roomUser.SetX, roomUser.SetY);
                                }
                            }
                        }
                        else
                            roomUser.PathRecalcNeeded = true;
                    }
                    if (!roomUser.RidingHorse)
                        roomUser.UpdateNeeded = true;
                }

                if (roomUser.IsBot)
                    roomUser.BotAI.OnTimerTick();
                else if (!roomUser.IsPet)
                    count++;

                UpdateUserEffect(roomUser, roomUser.X, roomUser.Y);
            }

            if (count == 0)
                idleCount++;

            lock (_toRemove)
            {
                foreach (var toRemove in _toRemove)
                {
                    var client = Azure.GetGame().GetClientManager().GetClientByUserId(toRemove.HabboId);
                    if (client != null)
                        RemoveUserFromRoom(client, true, false);
                    else
                        RemoveRoomUser(toRemove);
                }
                if (_userCount != count)
                    UpdateUserCount(count);
            }
        }

        internal void Destroy()
        {
            _room = null;
            UsersByUserName.Clear();
            UsersByUserName = null;
            UsersByUserId.Clear();
            UsersByUserId = null;
            OnUserEnter = null;
            _pets.Clear();
            _bots.Clear();
            _pets = null;
            _bots = null;
            UserList.Destroy();
            UserList = null;
        }

        private void UpdateUserEffect(RoomUser user, int x, int y)
        {
            if (user.IsBot)
                return;
            try
            {
                var b = _room.GetGameMap().EffectMap[x, y];
                if (b > 0)
                {
                    if (user.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect == 0)
                        user.CurrentItemEffect = ItemEffectType.None;
                    var itemEffectType = ByteToItemEffectEnum.Parse(b);
                    if (itemEffectType == user.CurrentItemEffect)
                        return;
                    switch (itemEffectType)
                    {
                        case ItemEffectType.None:
                            user.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(-1);
                            user.CurrentItemEffect = itemEffectType;
                            break;
                        case ItemEffectType.Swim:
                            user.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(28);
                            user.CurrentItemEffect = itemEffectType;
                            break;
                        case ItemEffectType.SwimLow:
                            user.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(30);
                            user.CurrentItemEffect = itemEffectType;
                            break;
                        case ItemEffectType.SwimHalloween:
                            user.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(37);
                            user.CurrentItemEffect = itemEffectType;
                            break;
                        case ItemEffectType.Iceskates:
                            user.GetClient()
                                .GetHabbo()
                                .GetAvatarEffectsInventoryComponent()
                                .ActivateCustomEffect(user.GetClient().GetHabbo().Gender.ToUpper() == "M" ? 38 : 39);
                            user.CurrentItemEffect = ItemEffectType.Iceskates;
                            break;
                        case ItemEffectType.Normalskates:
                            user.GetClient()
                                .GetHabbo()
                                .GetAvatarEffectsInventoryComponent()
                                .ActivateCustomEffect(user.GetClient().GetHabbo().Gender.ToUpper() == "M" ? 55 : 56);
                            user.CurrentItemEffect = itemEffectType;
                            break;
                        case ItemEffectType.SnowBoard:
                        {
                            user.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(97);
                            user.CurrentItemEffect = itemEffectType;
                        }
                            break;
                    }
                }
                else
                {
                    if (user.CurrentItemEffect == ItemEffectType.None || b != 0)
                        return;
                    user.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(-1);
                    user.CurrentItemEffect = ItemEffectType.None;
                }
            }
            catch
            {
            }
        }

        private void OnUserAdd(object sender, EventArgs args)
        {
            try
            {
                if (sender == null)
                    return;
                var value = ((KeyValuePair<int, RoomUser>) sender).Value;
                if (value == null || value.GetClient() == null || value.GetClient().GetHabbo() == null)
                    return;
                var client = value.GetClient();
                if (client == null || client.GetHabbo() == null || _room == null)
                    return;
                if (!value.IsSpectator)
                {
                    var model = _room.GetGameMap().Model;
                    if (model == null)
                        return;
                    value.SetPos(model.DoorX, model.DoorY, model.DoorZ);
                    value.SetRot(model.DoorOrientation, false);
                    if (_room.CheckRights(client, true, false))
                        value.AddStatus("flatctrl 4", "");
                    else if (_room.CheckRights(client, false, true))
                        value.AddStatus("flatctrl 1", "");
                    else if (_room.CheckRights(client))
                        value.AddStatus("flatctrl 1", "");
                    value.CurrentItemEffect = ItemEffectType.None;
                    if (!value.IsBot && value.GetClient().GetHabbo().IsTeleporting)
                    {
                        var item = _room.GetRoomItemHandler().GetItem(value.GetClient().GetHabbo().TeleporterId);
                        if (item != null)
                        {
                            item.ExtraData = "2";
                            item.UpdateState(false, true);
                            value.SetPos(item.X, item.Y, item.Z);
                            value.SetRot(item.Rot, false);
                            item.InteractingUser2 = client.GetHabbo().Id;
                            item.ExtraData = "0";
                            item.UpdateState(false, true);
                        }
                    }
                    if (!value.IsBot && value.GetClient().GetHabbo().IsHopping)
                    {
                        var item2 = _room.GetRoomItemHandler().GetItem(value.GetClient().GetHabbo().HopperId);
                        if (item2 != null)
                        {
                            item2.ExtraData = "1";
                            item2.UpdateState(false, true);
                            value.SetPos(item2.X, item2.Y, item2.Z);
                            value.SetRot(item2.Rot, false);
                            value.AllowOverride = false;
                            item2.InteractingUser2 = client.GetHabbo().Id;
                            item2.ExtraData = "2";
                            item2.UpdateState(false, true);
                        }
                    }
                    if (!value.IsSpectator)
                    {
                        var serverMessage =
                            new ServerMessage(LibraryParser.OutgoingRequest("SetRoomUserMessageComposer"));
                        serverMessage.AppendInteger(1);
                        value.Serialize(serverMessage, _room.GetGameMap().GotPublicPool);
                        _room.SendMessage(serverMessage);
                    }
                    if (!value.IsBot)
                    {
                        var serverMessage2 = new ServerMessage();
                        serverMessage2.Init(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                        serverMessage2.AppendInteger(value.VirtualId);
                        serverMessage2.AppendString(value.GetClient().GetHabbo().Look);
                        serverMessage2.AppendString(value.GetClient().GetHabbo().Gender.ToLower());
                        serverMessage2.AppendString(value.GetClient().GetHabbo().Motto);
                        serverMessage2.AppendInteger(value.GetClient().GetHabbo().AchievementPoints);
                        _room.SendMessage(serverMessage2);
                    }
                    if (_room.Owner != client.GetHabbo().UserName)
                    {
                        Azure.GetGame()
                            .GetQuestManager()
                            .ProgressUserQuest(value.GetClient(), QuestType.SocialVisit, 0u);
                        Azure.GetGame()
                            .GetAchievementManager()
                            .ProgressUserAchievement(value.GetClient(), "ACH_RoomEntry", 1, false);
                    }
                }
                if (client.GetHabbo().GetMessenger() != null)
                    client.GetHabbo().GetMessenger().OnStatusChanged(true);
                value.GetClient().GetMessageHandler().OnRoomUserAdd();

                if (OnUserEnter != null)
                    OnUserEnter(value, null);
                if (_room.GotMusicController() && _room.GotMusicController())
                    _room.GetRoomMusicController().OnNewUserEnter(value);
                _room.OnUserEnter(value);
            }
            catch (Exception ex)
            {
                Logging.LogCriticalException(ex.ToString());
            }
        }

        private void OnRemove(object sender, EventArgs args)
        {
            try
            {
                var keyValuePair = (KeyValuePair<int, RoomUser>) sender;
                var value = keyValuePair.Value;
                var client = value.GetClient();
                var list = UserList.Values.Where(current => current.IsBot && !current.IsPet).ToList();
                var list2 = new List<RoomUser>();
                foreach (var current2 in list)
                {
                    current2.BotAI.OnUserLeaveRoom(client);
                    if (current2.IsPet && current2.PetData.OwnerId == value.UserId &&
                        !_room.CheckRights(client, true, false))
                        list2.Add(current2);
                }
                foreach (
                    var current3 in
                        list2.Where(
                            current3 =>
                                value.GetClient() != null && value.GetClient().GetHabbo() != null &&
                                value.GetClient().GetHabbo().GetInventoryComponent() != null))
                {
                    value.GetClient().GetHabbo().GetInventoryComponent().AddPet(current3.PetData);
                    RemoveBot(current3.VirtualId, false);
                }
                _room.GetGameMap().RemoveUserFromMap(value, new Point(value.X, value.Y));
            }
            catch (Exception ex)
            {
                Logging.LogCriticalException(ex.ToString());
            }
        }

        private void OnUserUpdateStatus()
        {
            foreach (var current in UserList.Values)
                UpdateUserStatus(current, false);
        }

        private bool IsValid(RoomUser user)
        {
            return user != null && (user.IsBot ||
                                    (user.GetClient() != null && user.GetClient().GetHabbo() != null &&
                                     user.GetClient().GetHabbo().CurrentRoomId == _room.RoomId));
        }
    }
}