using System;
using Azure.HabboHotel.Rooms.Games;

namespace Azure.HabboHotel.Rooms
{
    public class TeamScoreChangedArgs : EventArgs
    {
        internal readonly int Points;
        internal readonly Team Team;
        internal readonly RoomUser User;

        public TeamScoreChangedArgs(int points, Team team, RoomUser user)
        {
            this.Points = points;
            this.Team = team;
            this.User = user;
        }
    }
}