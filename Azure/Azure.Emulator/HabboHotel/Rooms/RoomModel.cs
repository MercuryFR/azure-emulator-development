using System;

namespace Azure.HabboHotel.Rooms
{
    internal class RoomModel
    {
        internal int DoorX;
        internal int DoorY;
        internal double DoorZ;
        internal int DoorOrientation;
        internal string Heightmap;
        internal SquareState[,] SqState;
        internal int[,] SqFloorHeight;
        internal byte[,] SqSeatRot;
        internal char[,] SqChar;
        internal byte[,] MRoomModelfx;
        internal int MapSizeX;
        internal int MapSizeY;
        internal string StaticFurniMap;
        internal bool ClubOnly;
        internal bool GotPublicPool;

        internal RoomModel(int doorX, int doorY, double doorZ, int doorOrientation, string heightmap,
            string staticFurniMap, bool clubOnly, string poolmap)
        {
            checked
            {
                try
                {
                    this.DoorX = doorX;
                    this.DoorY = doorY;
                    this.DoorZ = doorZ;
                    this.DoorOrientation = doorOrientation;
                    this.Heightmap = heightmap.ToLower();
                    this.StaticFurniMap = staticFurniMap;
                    this.GotPublicPool = !string.IsNullOrEmpty(poolmap);

                    heightmap = heightmap.Replace(string.Format("{0}", Convert.ToChar(10)), "");
                    string[] array = heightmap.Split(Convert.ToChar(13));
                    this.MapSizeX = array[0].Length;
                    this.MapSizeY = array.Length;
                    this.ClubOnly = clubOnly;

                    this.SqState = new SquareState[this.MapSizeX, this.MapSizeY];
                    this.SqFloorHeight = new int[this.MapSizeX, this.MapSizeY];
                    this.SqSeatRot = new byte[this.MapSizeX, this.MapSizeY];
                    this.SqChar = new char[this.MapSizeX, this.MapSizeY];
                    if (this.GotPublicPool)
                    {
                        this.MRoomModelfx = new byte[this.MapSizeX, this.MapSizeY];
                    }
                    for (int y = 0; y < this.MapSizeY; y++)
                    {
                        string text2 = array[y].Replace(string.Format("{0}", Convert.ToChar(13)), "").Replace(string.Format("{0}", Convert.ToChar(10)), "");

                        for (int x = 0; x < this.MapSizeX; x++)
                        {
                            char c = 'x';
                            try
                            {
                                c = text2[x];
                            }
                            catch (Exception)
                            {
                            }
                            if (x == doorX && y == doorY)
                            {
                                this.SqFloorHeight[x, y] = (int)this.DoorZ;
                                this.SqState[x, y] = SquareState.Open;
                                if (this.SqFloorHeight[x, y] > 9)
                                {
                                    this.SqChar[x, y] = "abcdefghijklmnopqrstuvw"[(this.SqFloorHeight[x, y] - 10)];
                                }
                                else
                                {
                                    this.SqChar[x, y] = char.Parse(this.DoorZ.ToString());
                                }
                            }
                            else
                            {
                                if (c.Equals('x'))
                                {
                                    this.SqFloorHeight[x, y] = -1;
                                    this.SqState[x, y] = SquareState.Blocked;
                                    this.SqChar[x, y] = c;
                                }
                                else
                                {
                                    if (char.IsDigit(c))
                                    {
                                        this.SqFloorHeight[x, y] = int.Parse(c.ToString());
                                        this.SqState[x, y] = SquareState.Open;
                                        this.SqChar[x, y] = c;
                                    }
                                    else
                                    {
                                        if (!char.IsLetter(c))
                                        {
                                            continue;
                                        }
                                        this.SqFloorHeight[x, y] = "abcdefghijklmnopqrstuvw".IndexOf(char.ToLower(c)) + 10;
                                        this.SqState[x, y] = SquareState.Open;
                                        this.SqChar[x, y] = c;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
        }
    }
}