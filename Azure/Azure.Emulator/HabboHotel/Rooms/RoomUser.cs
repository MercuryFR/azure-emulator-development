using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.PathFinding;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.RoomBots;
using Azure.HabboHotel.Rooms.Games;
using Azure.HabboHotel.Rooms.Wired;
using Azure.Messages;
using Azure.Messages.Parsers;
using Azure.Util;

namespace Azure.HabboHotel.Rooms
{
    public class RoomUser : IEquatable<RoomUser>
    {
        internal uint HabboId;
        internal int VirtualId;
        internal uint RoomId;
        internal uint UserId;
        internal RoomUser FollowingOwner;
        internal bool InteractingGate;
        internal uint GateId;
        internal int LastInteraction;
        internal int LockedTilesCount;
        internal int CarryItemId;
        internal int CarryTimer;
        internal int SignTime;
        internal int IdleTime;

        internal int X;
        internal int Y;
        internal double Z;
        internal byte SqState;
        internal int RotHead;
        internal int RotBody;
        internal bool CanWalk;
        internal bool AllowOverride;
        internal bool TeleportEnabled;
        internal int GoalX;
        internal int GoalY;
        internal uint LoveLockPartner;

        internal List<Vector2D> Path = new List<Vector2D>();
        internal bool PathRecalcNeeded;
        internal int PathStep = 1;
        internal bool SetStep;
        internal int SetX;
        internal int SetY;
        internal double SetZ;

        internal RoomBot BotData;
        internal BotAI BotAI;
        internal ItemEffectType CurrentItemEffect;
        internal bool Freezed; //En el freeze
        internal bool Frozen; //por comando
        internal int FreezeCounter;
        internal Team Team;
        internal FreezePowerUp BanzaiPowerUp;
        internal int FreezeLives;
        internal bool ShieldActive;
        internal int ShieldCounter;
        internal bool ThrowBallAtGoal;
        internal bool IsMoonwalking;
        internal bool IsSitting;
        internal bool IsLyingDown;
        internal bool HasPathBlocked;
        internal bool IsFlooded;
        internal int FloodExpiryTime;

        internal bool RidingHorse;
        internal uint HorseId;

        internal uint LastItem;
        internal bool OnCampingTent;
        internal bool FastWalking;
        internal int LastBubble = 0;
        internal Pet PetData;
        internal bool IsWalking;
        internal bool UpdateNeeded;
        internal bool IsAsleep;
        internal Dictionary<string, string> Statusses;
        internal int DanceId;
        internal int TeleDelay;
        internal bool IsSpectator;
        internal int InternalRoomId;
        private readonly Queue _events;
        private int _floodCount;
        private GameClient _mClient;
        private Room _mRoom;
        internal int HandelingBallStatus = 0;

        internal RoomUser(uint habboId, uint roomId, int virtualId, Room room, bool isSpectator)
        {
            Freezed = false;
            HabboId = habboId;
            RoomId = roomId;
            VirtualId = virtualId;
            IdleTime = 0;
            X = 0;
            Y = 0;
            Z = 0.0;
            RotHead = 0;
            RotBody = 0;
            UpdateNeeded = true;
            Statusses = new Dictionary<string, string>();
            TeleDelay = -1;
            _mRoom = room;
            AllowOverride = false;
            CanWalk = true;
            IsSpectator = isSpectator;
            SqState = 3;
            InternalRoomId = 0;
            CurrentItemEffect = ItemEffectType.None;
            _events = new Queue();
            FreezeLives = 0;
            InteractingGate = false;
            GateId = 0u;
            LastInteraction = 0;
            LockedTilesCount = 0;
        }

        internal RoomUser(uint habboId, uint roomId, int virtualId, GameClient pClient, Room room)
        {
            _mClient = pClient;
            Freezed = false;
            HabboId = habboId;
            RoomId = roomId;
            VirtualId = virtualId;
            IdleTime = 0;
            X = 0;
            Y = 0;
            Z = 0.0;
            RotHead = 0;
            RotBody = 0;
            UpdateNeeded = true;
            Statusses = new Dictionary<string, string>();
            TeleDelay = -1;
            LastInteraction = 0;
            AllowOverride = false;
            CanWalk = true;
            IsSpectator = GetClient().GetHabbo().SpectatorMode;
            SqState = 3;
            InternalRoomId = 0;
            CurrentItemEffect = ItemEffectType.None;
            _mRoom = room;
            _events = new Queue();
            InteractingGate = false;
            GateId = 0u;
            LockedTilesCount = 0;
        }

        internal Point Coordinate
        {
            get { return new Point(X, Y); }
        }

        internal Point SquareBehind
        {
            get
            {
                var x = X;
                var y = Y;

                switch (RotBody)
                {
                    case 0:
                        y++;
                        break;
                    case 1:
                        x--;
                        y++;
                        break;
                    case 2:
                        x--;
                        break;
                    case 3:
                        x--;
                        y--;
                        break;
                    case 4:
                        y--;
                        break;
                    case 5:
                        x++;
                        y--;
                        break;
                    case 6:
                        x++;
                        break;
                    case 7:
                        x++;
                        y++;
                        break;
                }

                return new Point(x, y);
            }
        }

        internal Point SquareInFront
        {
            get
            {
                checked
                {
                    var x = X + 1;
                    var y = 0;
                    switch (RotBody)
                    {
                        case 0:
                            x = X;
                            y = Y - 1;
                            break;
                        case 1:
                            x = X + 1;
                            y = Y - 1;
                            break;
                        case 2:
                            x = X + 1;
                            y = Y;
                            break;
                        case 3:
                            x = X + 1;
                            y = Y + 1;
                            break;
                        case 4:
                            x = X;
                            y = Y + 1;
                            break;
                        case 5:
                            x = X - 1;
                            y = Y + 1;
                            break;
                        case 6:
                            x = X - 1;
                            y = Y;
                            break;
                        case 7:
                            x = X - 1;
                            y = Y - 1;
                            break;
                    }
                    return new Point(x, y);
                }
            }
        }

        internal bool IsPet
        {
            get { return IsBot && BotData.IsPet; }
        }

        internal int CurrentEffect
        {
            get
            {
                if (GetClient() == null || GetClient().GetHabbo() == null)
                    return 0;

                return GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect;
            }
        }

        internal bool IsDancing
        {
            get { return DanceId >= 1; }
        }

        internal bool NeedsAutokick
        {
            get
            {
                return !IsBot &&
                       (GetClient() == null || GetClient().GetHabbo() == null ||
                        (GetClient().GetHabbo().Rank <= 6u && IdleTime >= 1800));
            }
        }

        internal bool IsTrading
        {
            get { return !IsBot && Statusses.ContainsKey("trd"); }
        }

        internal bool IsBot
        {
            get { return BotData != null; }
        }

        public bool Equals(RoomUser comparedUser) { return comparedUser.HabboId == HabboId; }

        internal static int GetSpeechEmotion(string message)
        {
            message = message.ToLower();
            if (message.Contains(":)") || message.Contains(":d") || message.Contains("=]") || message.Contains("=d") ||
                message.Contains(":>"))
                return 1;
            if (message.Contains(">:(") || message.Contains(":@"))
                return 2;
            if (message.Contains(":o"))
                return 3;
            if (message.Contains(":(") || message.Contains("=[") || message.Contains(":'(") || message.Contains("='["))
                return 4;
            return 0;
        }

        internal string GetUserName()
        {
            if (!IsBot)
                return GetClient() != null ? GetClient().GetHabbo().UserName : string.Empty;
            if (!IsPet)
                return BotData == null ? string.Empty : BotData.Name;
            return PetData.Name;
        }

        internal bool IsOwner() { return !IsBot && GetUserName() == GetRoom().Owner; }

        internal void UnIdle()
        {
            IdleTime = 0;
            if (!IsAsleep)
                return;
            IsAsleep = false;
            var sleep = new ServerMessage(LibraryParser.OutgoingRequest("RoomUserIdleMessageComposer"));
            sleep.AppendInteger(VirtualId);
            sleep.AppendBool(false);
            GetRoom().SendMessage(sleep);
        }

        internal void Dispose()
        {
            Statusses.Clear();
            _mRoom = null;
            _mClient = null;
        }

        internal void Chat(GameClient session, string msg, bool shout, int count, int textColor = 0)
        {
            if (IsPet || IsBot)
            {
                if (!IsPet)
                    textColor = 2;

                var botChatmsg = new ServerMessage();
                botChatmsg.Init(shout
                    ? LibraryParser.OutgoingRequest("ShoutMessageComposer")
                    : LibraryParser.OutgoingRequest("ChatMessageComposer"));
                botChatmsg.AppendInteger(VirtualId);
                botChatmsg.AppendString(msg);
                botChatmsg.AppendInteger(0);
                botChatmsg.AppendInteger(textColor);
                botChatmsg.AppendInteger(0);
                botChatmsg.AppendInteger(count);

                GetRoom().SendMessage(botChatmsg);
                return;
            }

            if (msg.Length > 100) // si el mensaje es mayor que la m�xima longitud (scripter)
                return;

            if (session == null || session.GetHabbo() == null)
                return;
            if (session.GetHabbo().Rank <= 9 && BlockAds.CheckPublicistas(msg))
            {
                session.PublicistaCount++;
                session.HandlePublicista(msg);
                return;
            }

            if (!IsBot && IsFlooded && FloodExpiryTime <= Azure.GetUnixTimestamp())
                IsFlooded = false;
            else if (!IsBot && IsFlooded)
                return; // ciao flooders!

            if (session.GetHabbo().Rank < 4 && GetRoom().CheckMute(session))
                return;

            UnIdle();
            if (!IsPet && !IsBot)
            {
                if (msg.StartsWith(":") && GetClient().GetHabbo().GetCommandHandler().Parse(msg))
                    return;

                var habbo = GetClient().GetHabbo();

                if (GetRoom().GetWiredHandler().ExecuteWired(WiredItemType.TriggerUserSaysKeyword, new object[]
                {
                    this,
                    msg
                }))
                    return;

                uint rank = 1;

                if (session.GetHabbo() != null)
                    rank = session.GetHabbo().Rank;

                GetRoom().AddChatlog(session.GetHabbo().Id, msg, false);

                msg = GetRoom()
                    .WordFilter
                    .Aggregate(msg,
                        (current1, current) => Regex.Replace(current1, current, "bobba", RegexOptions.IgnoreCase));

                if (rank < 4)
                {
                    var span = DateTime.Now - habbo.SpamFloodTime;
                    if ((span.TotalSeconds > habbo.SpamProtectionTime) && habbo.SpamProtectionBol)
                    {
                        _floodCount = 0;
                        habbo.SpamProtectionBol = false;
                        habbo.SpamProtectionAbuse = 0;
                    }
                    else if (span.TotalSeconds > 4.0)
                        _floodCount = 0;
                    ServerMessage message;
                    if ((span.TotalSeconds < habbo.SpamProtectionTime) && habbo.SpamProtectionBol)
                    {
                        message = new ServerMessage(LibraryParser.OutgoingRequest("FloodFilterMessageComposer"));
                        var i = habbo.SpamProtectionTime - span.Seconds;
                        message.AppendInteger(i);
                        IsFlooded = true;
                        FloodExpiryTime = Azure.GetUnixTimestamp() + i;
                        GetClient().SendMessage(message);
                        return;
                    }
                    if (((span.TotalSeconds < 4.0) && (_floodCount > 5)) && (rank < 5))
                    {
                        message = new ServerMessage(LibraryParser.OutgoingRequest("FloodFilterMessageComposer"));
                        habbo.SpamProtectionCount++;
                        if ((habbo.SpamProtectionCount % 2) == 0)
                            habbo.SpamProtectionTime = 10 * habbo.SpamProtectionCount;
                        else
                            habbo.SpamProtectionTime = 10 * (habbo.SpamProtectionCount - 1);
                        habbo.SpamProtectionBol = true;
                        var j = habbo.SpamProtectionTime - span.Seconds;
                        message.AppendInteger(j);
                        IsFlooded = true;
                        FloodExpiryTime = Azure.GetUnixTimestamp() + j;
                        GetClient().SendMessage(message);
                        return;
                    }
                    habbo.SpamFloodTime = DateTime.Now;
                    _floodCount++;
                }
            }
            else if (!IsPet)
                textColor = 2;

            var chatMsg = new ServerMessage();
            chatMsg.Init(shout
                ? LibraryParser.OutgoingRequest("ShoutMessageComposer")
                : LibraryParser.OutgoingRequest("ChatMessageComposer"));
            chatMsg.AppendInteger(VirtualId);
            chatMsg.AppendString(msg);
            chatMsg.AppendInteger(GetSpeechEmotion(msg));
            chatMsg.AppendInteger(textColor);
            chatMsg.AppendInteger(0);
            chatMsg.AppendInteger(count);
            GetRoom().BroadcastChatMessage(chatMsg, this, session.GetHabbo().Id);

            GetRoom().OnUserSay(this, msg, shout);

            GetRoom().GetRoomUserManager().TurnHeads(X, Y, HabboId);
        }

        internal bool IncrementAndCheckFlood(out int muteTime)
        {
            muteTime = 20;
            var timeSpan = DateTime.Now - GetClient().GetHabbo().SpamFloodTime;
            if (timeSpan.TotalSeconds > GetClient().GetHabbo().SpamProtectionTime &&
                GetClient().GetHabbo().SpamProtectionBol)
            {
                _floodCount = 0;
                GetClient().GetHabbo().SpamProtectionBol = false;
                GetClient().GetHabbo().SpamProtectionAbuse = 0;
            }
            else if (timeSpan.TotalSeconds > 2.0)
                _floodCount = 0;
            checked
            {
                if (timeSpan.TotalSeconds < 2.0 && _floodCount > 6 && GetClient().GetHabbo().Rank < 5u)
                {
                    muteTime = GetClient().GetHabbo().SpamProtectionTime - timeSpan.Seconds + 30;
                    return true;
                }
                GetClient().GetHabbo().SpamFloodTime = DateTime.Now;
                _floodCount++;
                return false;
            }
        }

        internal void ClearMovement(bool update)
        {
            IsWalking = false;
            Statusses.Remove("mv");
            GoalX = 0;
            GoalY = 0;
            SetStep = false;
            try
            {
                GetRoom().GetRoomUserManager().ToSet.Remove(new Point(SetX, SetY));
            }
            catch (Exception)
            {
            }
            SetX = 0;
            SetY = 0;
            SetZ = 0.0;
            if (update)
                UpdateNeeded = true;
        }

        internal void MoveTo(Point c) { MoveTo(c.X, c.Y); }

        internal void MoveTo(int pX, int pY, bool pOverride)
        {
            if (TeleportEnabled)
            {
                UnIdle();
                GetRoom()
                    .SendMessage(GetRoom()
                        .GetRoomItemHandler()
                        .UpdateUserOnRoller(this, new Point(pX, pY), 0u,
                            GetRoom().GetGameMap().SqAbsoluteHeight(GoalX, GoalY)));
                if (Statusses.ContainsKey("sit"))
                    Z -= 0.35;
                UpdateNeeded = true;
                GetRoom().GetRoomUserManager().UpdateUserStatus(this, false);
                return;
            }
            if (GetRoom().GetGameMap().SquareHasUsers(pX, pY) && !pOverride)
                return;
            if (Frozen)
                return;
            if (!IsBot)
                if (IsMoonwalking)
                    GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(136);
                else if (!IsMoonwalking &&
                         GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect == 136)
                    GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(0);
            var coordItemSearch = new CoordItemSearch(GetRoom().GetGameMap().CoordinatedItems);
            var allRoomItemForSquare = coordItemSearch.GetAllRoomItemForSquare(pX, pY);
            if ((RidingHorse && !IsBot && allRoomItemForSquare.Any()) || (IsPet && allRoomItemForSquare.Any()))
                if (
                    allRoomItemForSquare.Any(
                        current =>
                            (current.GetBaseItem().IsSeat ||
                             current.GetBaseItem().InteractionType == InteractionType.lowpool ||
                             current.GetBaseItem().InteractionType == InteractionType.pool ||
                             current.GetBaseItem().InteractionType == InteractionType.haloweenpool ||
                             current.GetBaseItem().InteractionType == InteractionType.bed)))
                    return;
            UnIdle();
            GoalX = pX;
            GoalY = pY;
            PathRecalcNeeded = true;
            ThrowBallAtGoal = false;
        }

        internal void MoveTo(int pX, int pY) { MoveTo(pX, pY, false); }

        internal void UnlockWalking()
        {
            AllowOverride = false;
            CanWalk = true;
        }

        internal void SetPos(int pX, int pY, double pZ)
        {
            X = pX;
            Y = pY;
            Z = pZ;
        }

        internal void CarryItem(int item)
        {
            CarryItemId = item;
            CarryTimer = item > 0 ? 240 : 0;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ApplyHanditemMessageComposer"));
            serverMessage.AppendInteger(VirtualId);
            serverMessage.AppendInteger(item);
            GetRoom().SendMessage(serverMessage);
        }

        internal void SetRot(int rotation) { SetRot(rotation, false); }

        internal void SetRot(int rotation, bool headOnly)
        {
            if (Statusses.ContainsKey("lay") || IsWalking)
                return;
            checked
            {
                var num = RotBody - rotation;
                RotHead = RotBody;
                if (Statusses.ContainsKey("sit") || headOnly)
                    switch (RotBody)
                    {
                        case 4:
                        case 2:
                            if (num > 0)
                                RotHead = RotBody - 1;
                            else if (num < 0)
                                RotHead = RotBody + 1;
                            break;
                        case 6:
                        case 0:
                            if (num > 0)
                                RotHead = RotBody - 1;
                            else if (num < 0)
                                RotHead = RotBody + 1;
                            break;
                    }
                else if (num <= -2 || num >= 2)
                {
                    RotHead = rotation;
                    RotBody = rotation;
                }
                else
                    RotHead = rotation;
                UpdateNeeded = true;
            }
        }

        internal void SetStatus(string key, string value)
        {
            if (Statusses.ContainsKey(key))
            {
                Statusses[key] = value;
                return;
            }
            AddStatus(key, value);
        }

        internal void AddStatus(string key, string value) { Statusses[key] = value; }

        internal void RemoveStatus(string key)
        {
            if (Statusses.ContainsKey(key))
                Statusses.Remove(key);
        }

        internal void ApplyEffect(int effectId)
        {
            if (IsBot || GetClient() == null || GetClient().GetHabbo() == null ||
                GetClient().GetHabbo().GetAvatarEffectsInventoryComponent() == null)
                return;
            GetClient().GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(effectId);
        }

        internal void Serialize(ServerMessage message, bool gotPublicRoom)
        {
            if (message == null)
                return;
            if (IsSpectator)
                return;
            if (!IsBot)
            {
                if (GetClient() == null || GetClient().GetHabbo() == null)
                    return;
                var group = Azure.GetGame().GetGroupManager().GetGroup(GetClient().GetHabbo().FavouriteGroup);
                var habbo = GetClient().GetHabbo();
                message.AppendInteger(habbo.Id);
                message.AppendString(habbo.UserName);
                message.AppendString(habbo.Motto);
                message.AppendString(habbo.Look);
                message.AppendInteger(VirtualId);
                message.AppendInteger(X);
                message.AppendInteger(Y);
                message.AppendString(TextHandling.GetString(Z));
                message.AppendInteger(0);
                message.AppendInteger(1);
                message.AppendString(habbo.Gender.ToLower());
                if (@group != null)
                {
                    message.AppendInteger(@group.Id);
                    message.AppendInteger(0);
                    message.AppendString(@group.Name);
                }
                else
                {
                    message.AppendInteger(0);
                    message.AppendInteger(0);
                    message.AppendString("");
                }
                message.AppendString("");
                message.AppendInteger(habbo.AchievementPoints);
                message.AppendBool(false);
                return;
            }
            message.AppendInteger(BotAI.BaseId);
            message.AppendString(BotData.Name);
            message.AppendString(BotData.Motto);
            if (BotData.AiType == AIType.Pet)
                if (PetData.Type == 16u)
                    message.AppendString(PetData.MoplaBreed.PlantData);
                else if (PetData.HaveSaddle == Convert.ToBoolean(2))
                    message.AppendString(string.Concat(new object[]
                    {
                        BotData.Look.ToLower(),
                        " 3 4 10 0 2 ",
                        PetData.PetHair,
                        " ",
                        PetData.HairDye,
                        " 3 ",
                        PetData.PetHair,
                        " ",
                        PetData.HairDye
                    }));
                else if (PetData.HaveSaddle == Convert.ToBoolean(1))
                    message.AppendString(string.Concat(new object[]
                    {
                        BotData.Look.ToLower(),
                        " 3 2 ",
                        PetData.PetHair,
                        " ",
                        PetData.HairDye,
                        " 3 ",
                        PetData.PetHair,
                        " ",
                        PetData.HairDye,
                        " 4 9 0"
                    }));
                else
                    message.AppendString(string.Concat(new object[]
                    {
                        BotData.Look.ToLower(),
                        " 2 2 ",
                        PetData.PetHair,
                        " ",
                        PetData.HairDye,
                        " 3 ",
                        PetData.PetHair,
                        " ",
                        PetData.HairDye
                    }));
            else
                message.AppendString(BotData.Look.ToLower());
            message.AppendInteger(VirtualId);
            message.AppendInteger(X);
            message.AppendInteger(Y);
            message.AppendString(TextHandling.GetString(Z));
            message.AppendInteger(0);
            message.AppendInteger((BotData.AiType == AIType.Generic) ? 4 : 2);
            if (BotData.AiType == AIType.Pet)
            {
                message.AppendInteger(PetData.Type);
                message.AppendInteger(PetData.OwnerId);
                message.AppendString(PetData.OwnerName);
                message.AppendInteger((PetData.Type == 16u) ? 0 : 1);
                message.AppendBool(PetData.HaveSaddle);
                message.AppendBool(RidingHorse);
                message.AppendInteger(0);
                message.AppendInteger((PetData.Type == 16u) ? 1 : 0);
                message.AppendString((PetData.Type == 16u) ? PetData.MoplaBreed.GrowStatus : "");
                return;
            }
            message.AppendString(BotData.Gender.ToLower());
            message.AppendInteger(BotData.OwnerId);
            message.AppendString(Azure.GetGame().GetClientManager().GetNameById(BotData.OwnerId));
            message.AppendInteger(5);
            message.AppendShort(1);
            message.AppendShort(2);
            message.AppendShort(3);
            message.AppendShort(4);
            message.AppendShort(5);
        }

        internal void SerializeStatus(ServerMessage message)
        {
            message.AppendInteger(VirtualId);
            message.AppendInteger(X);
            message.AppendInteger(Y);
            message.AppendString(TextHandling.GetString(Z));
            message.AppendInteger(RotHead);
            message.AppendInteger(RotBody);
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("/");
            if (IsPet && PetData.Type == 16u)
                stringBuilder.AppendFormat("/{0}{1}", PetData.MoplaBreed.GrowStatus, (Statusses.Count >= 1) ? "/" : "");
            lock (Statusses)
            {
                foreach (var current in Statusses)
                {
                    stringBuilder.Append(current.Key);
                    if (!string.IsNullOrEmpty(current.Value))
                    {
                        stringBuilder.Append(" ");
                        stringBuilder.Append(current.Value);
                    }
                    stringBuilder.Append("/");
                }
            }
            stringBuilder.Append("/");
            message.AppendString(stringBuilder.ToString());

            if (!Statusses.ContainsKey("sign"))
                return;
            RemoveStatus("sign");
            UpdateNeeded = true;
        }

        internal void SerializeStatus(ServerMessage message, string status)
        {
            if (IsSpectator)
                return;
            message.AppendInteger(VirtualId);
            message.AppendInteger(X);
            message.AppendInteger(Y);
            message.AppendString(TextHandling.GetString(Z));
            message.AppendInteger(RotHead);
            message.AppendInteger(RotBody);
            message.AppendString(status);
        }

        internal GameClient GetClient()
        {
            if (IsBot)
                return null;

            if (_mClient != null)
                return _mClient;

            return _mClient = Azure.GetGame().GetClientManager().GetClientByUserId(HabboId);
        }

        internal void SendMessage(byte[] message) { GetClient().GetConnection().SendData(message); }

        private Room GetRoom() { return _mRoom ?? (_mRoom = Azure.GetGame().GetRoomManager().GetRoom(RoomId)); }
    }
}