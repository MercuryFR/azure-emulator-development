using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Linq;
using Azure.Configuration;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.Collections;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Pathfinding;
using Azure.HabboHotel.Pets;
using Azure.Messages;
using Azure.Messages.Parsers;
using Azure.Util;
using System.Globalization;

namespace Azure.HabboHotel.Rooms
{
    internal class RoomItemHandling
    {
        public int HopperCount;
        internal QueuedDictionary<uint, RoomItem> MFloorItems;
        internal QueuedDictionary<uint, RoomItem> MWallItems;
        internal QueuedDictionary<uint, RoomItem> MRollers;
        internal Dictionary<UInt32, RoomItem> breedingTerrier;
        internal Dictionary<UInt32, RoomItem> breedingBear;
        private readonly List<uint> _rollerItemsMoved;
        private readonly List<uint> _rollerUsersMoved;
        private readonly List<ServerMessage> _rollerMessages;
        private Room _room;
        private HybridDictionary _mRemovedItems;
        private HybridDictionary _mMovedItems;
        private HybridDictionary _mAddedItems;
        private int _mRollerSpeed;
        private int _mRoolerCycle;
        private Queue _roomItemUpdateQueue;

        public RoomItemHandling(Room room)
        {
            this._room = room;
            this._mRemovedItems = new HybridDictionary();
            this._mMovedItems = new HybridDictionary();
            this._mAddedItems = new HybridDictionary();
            this.MRollers = new QueuedDictionary<uint, RoomItem>();
            this.MWallItems = new QueuedDictionary<uint, RoomItem>();
            this.MFloorItems = new QueuedDictionary<uint, RoomItem>();
            this._roomItemUpdateQueue = new Queue();
            this.breedingBear = new Dictionary<uint, RoomItem>();
            this.breedingTerrier = new Dictionary<uint, RoomItem>();
            this.GotRollers = false;
            this._mRoolerCycle = 0;
            this._mRollerSpeed = 4;
            this.HopperCount = 0;
            this._rollerItemsMoved = new List<uint>();
            this._rollerUsersMoved = new List<uint>();
            this._rollerMessages = new List<ServerMessage>();
        }

        internal bool GotRollers { get; set; }

        internal Point GetRandomBreedingBear(Pet pet)
        {
            if (!breedingBear.Any())
                return new Point();
            List<UInt32> keys = new List<UInt32>(breedingBear.Keys);
            int size = breedingBear.Count;
            Random rand = new Random();
            UInt32 randomKey = keys[rand.Next(size)];

            breedingBear[randomKey].PetsList.Add(pet);
            pet.WaitingForBreading = breedingBear[randomKey].Id;
            pet.BreadingTile = breedingBear[randomKey].Coordinate;

            return breedingBear[randomKey].Coordinate;
        }

        internal Point GetRandomBreedingTerrier(Pet pet)
        {
            if (!breedingTerrier.Any())
                return new Point();
            var keys = new List<UInt32>(breedingTerrier.Keys);
            var size = breedingTerrier.Count;
            var rand = new Random();
            var randomKey = keys[rand.Next(size)];

            breedingTerrier[randomKey].PetsList.Add(pet);
            pet.WaitingForBreading = breedingTerrier[randomKey].Id;
            pet.BreadingTile = breedingTerrier[randomKey].Coordinate;

            return breedingTerrier[randomKey].Coordinate;
        }

        public void SaveFurniture(IQueryAdapter dbClient, GameClient session = null)
        {
            try
            {
                if (this._mAddedItems.Count <= 0 && this._mRemovedItems.Count <= 0 && this._mMovedItems.Count <= 0 &&
                    this._room.GetRoomUserManager().PetCount <= 0)
                {
                    return;
                }
                var queryChunk = new QueryChunk();
                var queryChunk2 = new QueryChunk();
                var queryChunk3 = new QueryChunk();
                foreach (RoomItem roomItem in this._mRemovedItems.Values)
                {
                    queryChunk.AddQuery(string.Format("UPDATE items_rooms SET room_id='0', x='0', y='0', z='0', rot='0' WHERE id = {0} ", roomItem.Id));
                }

                foreach (
                    var item in
                        _mMovedItems.Values.Cast<RoomItem>()
                            .Where(
                                item =>
                                    item != null && item.GetBaseItem() != null &&
                                    item.GetBaseItem().Name != "guild_forum" &&
                                    item.GetBaseItem().InteractionType != InteractionType.gld_gate))
                {
                    if (!string.IsNullOrEmpty(item.ExtraData))
                    {
                        if (item.GetBaseItem().IsGroupItem)
                            try
                            {
                                item.ExtraData = item.ExtraData + ";" + item.GroupData.Split(';')[1] + ";" +
                                                 item.GroupData.Split(';')[2] + ";" + item.GroupData.Split(';')[3];
                            }
                            catch
                            {
                            }

                        queryChunk3.AddQuery("UPDATE items_rooms SET extra_data = @data" + item.Id +
                                                 " WHERE id = " + item.Id);
                        queryChunk3.AddParameter("data" + item.Id, item.ExtraData);
                    }
                }

                if (this._mAddedItems.Count > 0)
                {
                    foreach (RoomItem roomItem2 in this._mAddedItems.Values)
                    {
                        if (!string.IsNullOrEmpty(roomItem2.ExtraData))
                        {
                            queryChunk3.AddQuery(string.Concat(new object[]
                            {
                                "UPDATE items_rooms SET extra_data=@edata",
                                roomItem2.Id,
                                " WHERE id='",
                                roomItem2.Id,
                                "'"
                            }));
                            queryChunk3.AddParameter(string.Format("edata{0}", roomItem2.Id), roomItem2.ExtraData);
                        }
                        if (roomItem2.IsFloorItem)
                        {
                            queryChunk2.AddQuery(string.Concat(new object[]
                            {
                                "UPDATE items_rooms SET room_id=",
                                roomItem2.RoomId,
                                ", x='",
                                roomItem2.X,
                                "', y='",
                                roomItem2.Y,
                                "', z='",
                                roomItem2.Z.ToString().Replace(',', '.'),
                                "', rot='",
                                roomItem2.Rot,
                                "' WHERE id='",
                                roomItem2.Id,
                                "'"
                            }));
                        }
                        else
                        {
                            queryChunk2.AddQuery(string.Concat(new object[]
                            {
                                "UPDATE items_rooms SET room_id='",
                                roomItem2.RoomId,
                                "',wall_pos='",
                                roomItem2.WallCoord,
                                "' WHERE id='",
                                roomItem2.Id,
                                "'"
                            }));
                        }
                    }
                }
                foreach (RoomItem roomItem3 in this._mMovedItems.Values)
                {
                    if (!string.IsNullOrEmpty(roomItem3.ExtraData))
                    {
                        queryChunk.AddQuery(string.Concat(new object[]
                        {
                            "UPDATE items_rooms SET extra_data = @EData",
                            roomItem3.Id,
                            " WHERE id='",
                            roomItem3.Id,
                            "'"
                        }));
                        queryChunk.AddParameter(string.Format("EData{0}", roomItem3.Id), roomItem3.ExtraData);
                    }
                    if (roomItem3.IsWallItem &&
                        (!roomItem3.GetBaseItem().Name.Contains("wallpaper_single") ||
                         !roomItem3.GetBaseItem().Name.Contains("floor_single") ||
                         !roomItem3.GetBaseItem().Name.Contains("landscape_single")))
                    {
                        queryChunk.AddQuery(string.Concat(new object[]
                        {
                            "UPDATE items_rooms SET wall_pos='",
                            roomItem3.WallCoord,
                            "' WHERE id=",
                            roomItem3.Id
                        }));
                    }
                    else
                    {
                        if (roomItem3.GetBaseItem().Name.Contains("wallpaper_single") ||
                            roomItem3.GetBaseItem().Name.Contains("floor_single") ||
                            roomItem3.GetBaseItem().Name.Contains("landscape_single"))
                        {
                            queryChunk.AddQuery(string.Format("DELETE FROM items_rooms WHERE id={0} LIMIT 1", roomItem3.Id));
                        }
                        else
                        {
                            queryChunk.AddQuery(string.Concat(new object[]
                            {
                                "UPDATE items_rooms SET x=",
                                roomItem3.X,
                                ", y=",
                                roomItem3.Y,
                                ", z='",
                                roomItem3.Z.ToString().Replace(',', '.'),
                                "', rot=",
                                roomItem3.Rot,
                                " WHERE id=",
                                roomItem3.Id
                            }));
                        }
                    }
                }
                this._room.GetRoomUserManager().AppendPetsUpdateString(dbClient);
                if (session != null)
                {
                    session.GetHabbo().GetInventoryComponent().RunDbUpdate();
                }
                this._mAddedItems.Clear();
                this._mRemovedItems.Clear();
                this._mMovedItems.Clear();
                queryChunk.Execute(dbClient);
                queryChunk2.Execute(dbClient);
                queryChunk3.Execute(dbClient);
                queryChunk.Dispose();
                queryChunk2.Dispose();
                queryChunk3.Dispose();
                queryChunk = null;
                queryChunk2 = null;
                queryChunk3 = null;
            }
            catch (Exception ex)
            {
                Logging.LogCriticalException(string.Concat(new object[]
                {
                    "Error during saving furniture for room ",
                    this._room.RoomId,
                    ". Stack: ",
                    ex.ToString()
                }));
            }
        }

        public bool CheckPosItem(GameClient session, RoomItem rItem, int newX, int newY, int newRot, bool newItem,
            bool sendNotify = true)
        {
            try
            {
                Dictionary<int, ThreeDCoord> dictionary = Gamemap.GetAffectedTiles(rItem.GetBaseItem().Length, rItem.GetBaseItem().Width, newX, newY, newRot);
                if (!this._room.GetGameMap().ValidTile(newX, newY))
                {
                    return false;
                }
                if (
                dictionary.Values.Any(
                    coord =>
                            (this._room.GetGameMap().Model.DoorX == coord.X) && (this._room.GetGameMap().Model.DoorY == coord.Y)))
                {
                    return false;
                }
                if ((this._room.GetGameMap().Model.DoorX == newX) && (this._room.GetGameMap().Model.DoorY == newY))
                {
                    return false;
                }
                if (dictionary.Values.Any(coord => !this._room.GetGameMap().ValidTile(coord.X, coord.Y)))
                {
                    return false;
                }
                double num = this._room.GetGameMap().Model.SqFloorHeight[newX][newY];
                if ((((rItem.Rot == newRot) && (rItem.X == newX)) && (rItem.Y == newY)) && (rItem.Z != num))
                {
                    return false;
                }
                if (this._room.GetGameMap().Model.SqState[newX][newY] != SquareState.Open)
                {
                    return false;
                }
                if (
                dictionary.Values.Any(
                    coord => this._room.GetGameMap().Model.SqState[coord.X][coord.Y] != SquareState.Open))
                {
                    return false;
                }
                if (!rItem.GetBaseItem().IsSeat)
                {
                    if (this._room.GetGameMap().SquareHasUsers(newX, newY))
                    {
                        return false;
                    }
                    if (dictionary.Values.Any(coord => this._room.GetGameMap().SquareHasUsers(coord.X, coord.Y)))
                    {
                        return false;
                    }
                }
                List<RoomItem> furniObjects = this.GetFurniObjects(newX, newY);
                var collection = new List<RoomItem>();
                var list3 = new List<RoomItem>();
                foreach (
                    List<RoomItem> list4 in
                    dictionary.Values
                              .Select(coord => this.GetFurniObjects(coord.X, coord.Y))
                              .Where(list4 => list4 != null))
                {
                    collection.AddRange(list4);
                }
                if (furniObjects == null)
                {
                    furniObjects = new List<RoomItem>();
                }
                list3.AddRange(furniObjects);
                list3.AddRange(collection);
                return list3.All(item => (item.Id == rItem.Id) || item.GetBaseItem().Stackable);
            }
            catch
            {
                return false;
            }
        }

        internal void QueueRoomItemUpdate(RoomItem item)
        {
            lock (this._roomItemUpdateQueue.SyncRoot)
            {
                this._roomItemUpdateQueue.Enqueue(item);
            }
        }

        internal List<RoomItem> RemoveAllFurniture(GameClient session)
        {
            var list = new List<RoomItem>();
            RoomItem[] array = this.MFloorItems.Values.ToArray();
            foreach (RoomItem roomItem in array)
            {
                roomItem.Interactor.OnRemove(session, roomItem);
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("PickUpFloorItemMessageComposer"));
                serverMessage.AppendString(string.Format("{0}{1}", roomItem.Id, string.Empty));
                serverMessage.AppendBool(false);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(roomItem.UserId);
                this._room.SendMessage(serverMessage);
                list.Add(roomItem);
            }
            RoomItem[] array2 = this.MWallItems.Values.ToArray();
            foreach (RoomItem roomItem2 in array2)
            {
                roomItem2.Interactor.OnRemove(session, roomItem2);
                var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("PickUpWallItemMessageComposer"));
                serverMessage2.AppendString(string.Format("{0}{1}", roomItem2.Id, string.Empty));
                serverMessage2.AppendInteger(roomItem2.UserId);
                this._room.SendMessage(serverMessage2);
                list.Add(roomItem2);
            }
            this.MWallItems.Clear();
            this.MFloorItems.Clear();
            this._mRemovedItems.Clear();
            this._mMovedItems.Clear();
            this._mAddedItems.Clear();
            this.MRollers.QueueDelegate(this.ClearRollers);
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Format("UPDATE items_rooms SET room_id='0' WHERE room_id = {0}", this._room.RoomId));
            }
            this._room.GetGameMap().GenerateMaps(true);
            this._room.GetRoomUserManager().UpdateUserStatusses();
            return list;
        }

        internal void SetSpeed(int p)
        {
            this._mRollerSpeed = p;
        }

        internal void LoadFurniture()
        {
            if (MFloorItems == null)
                MFloorItems = new QueuedDictionary<uint, RoomItem>();
            else
                MFloorItems.Clear();

            if (MWallItems == null)
                MWallItems = new QueuedDictionary<uint, RoomItem>();
            else
                MWallItems.Clear();

            checked
            {
                using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery(
                        "SELECT `items_rooms`.* , COALESCE(`items_groups`.`group_id`, 0) AS group_id FROM `items_rooms` LEFT OUTER JOIN `items_groups` ON `items_rooms`.`id` = `items_groups`.`id` WHERE items_rooms.room_id=@roomid LIMIT 2000");
                    queryReactor.AddParameter("roomid", this._room.RoomId);
                    DataTable table = queryReactor.GetTable();
                    if (table.Rows.Count == 2000)
                    {
                        GameClient clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId((uint)this._room.OwnerId);
                        if (clientByUserId != null)
                        {
                            clientByUserId.SendNotif(
                                "Your room has more than 2000 items in it. The current limit of items per room is 2000.\nTo view the rest, pick some of these items up!");
                        }
                    }
                    foreach (DataRow dataRow in table.Rows)
                    {
                        try
                        {
                            uint num = Convert.ToUInt32(dataRow[0]);
                            int x = Convert.ToInt32(dataRow[5]);
                            int y = Convert.ToInt32(dataRow[6]);
                            double num2 = Convert.ToDouble(dataRow[7]);
                            sbyte rot = Convert.ToSByte(dataRow[8]);
                            uint num3 = Convert.ToUInt32(dataRow[1]);
                            if (num3 == 0u)
                            {
                                queryReactor.SetQuery("UPDATE items_rooms SET user_id=@userid WHERE id=@itemID LIMIT 1");
                                queryReactor.AddParameter("itemID", num);
                                queryReactor.AddParameter("userid", this._room.OwnerId);
                                queryReactor.RunQuery();
                            }
                            string locationData;
                            if (string.IsNullOrWhiteSpace(dataRow[9].ToString()))
                            {
                                queryReactor.SetQuery(string.Format("SELECT type FROM catalog_furnis WHERE id={0} LIMIT 1", dataRow[3]));
                                string @string = queryReactor.GetString();
                                if (@string == "i")
                                {
                                    locationData = ":w=0,2 l=11,53 l";
                                    queryReactor.RunFastQuery(string.Concat(new object[]
                                    {
                                        "UPDATE items_rooms SET wall_pos='",
                                        locationData,
                                        "' WHERE id=",
                                        num,
                                        " LIMIT 1"
                                    }));
                                }
                            }
                            locationData = Convert.ToString(dataRow[9]);
                            uint num4 = Convert.ToUInt32(dataRow[3]);
                            string extraData;
                            if (DBNull.Value.Equals(dataRow[4]))
                            {
                                extraData = string.Empty;
                            }
                            else
                            {
                                extraData = (string)dataRow[4];
                            }
                            string songCode;
                            if (DBNull.Value.Equals(dataRow["songcode"]))
                            {
                                songCode = string.Empty;
                            }
                            else
                            {
                                songCode = (string)dataRow["songcode"];
                            }
                            uint group = Convert.ToUInt32(dataRow["group_id"]);
                            if (!string.IsNullOrWhiteSpace(locationData))
                            {
                                var wallCoord = new WallCoordinate(":" + locationData.Split(':')[1]);
                                var value = new RoomItem(num, this._room.RoomId, num4, extraData, wallCoord, this._room, num3,
                                    group, Azure.GetGame().GetItemManager().GetItem(num4).FlatId, Azure.EnumToBool((string)dataRow["builders"]));
                                if (!this.MWallItems.ContainsKey(num))
                                {
                                    this.MWallItems.Inner.TryAdd(num, value);
                                }
                            }
                            else
                            {
                                var roomItem = new RoomItem(num, this._room.RoomId, num4, extraData, x, y, num2, rot, this._room,
                                    num3, group, Azure.GetGame().GetItemManager().GetItem(num4).FlatId,
                                    songCode, Azure.EnumToBool((string)dataRow["builders"]));
                                if (!this._room.GetGameMap().ValidTile(x, y))
                                {
                                    GameClient clientByUserId2 = Azure.GetGame().GetClientManager().GetClientByUserId(num3);
                                    if (clientByUserId2 != null)
                                    {
                                        clientByUserId2.GetHabbo()
                                                       .GetInventoryComponent()
                                                       .AddNewItem(roomItem.Id, roomItem.BaseItem, roomItem.ExtraData, group, true,
                                                            true, 0, 0, "");
                                        queryReactor.RunFastQuery(string.Format("UPDATE items_rooms SET room_id='0' WHERE id='{0}' LIMIT 1", roomItem.Id));
                                        clientByUserId2.GetHabbo().GetInventoryComponent().UpdateItems(true);
                                    }
                                    else
                                    {
                                        queryReactor.RunFastQuery(string.Format("UPDATE items_rooms SET room_id='0' WHERE id='{0}' LIMIT 1", roomItem.Id));
                                    }
                                }
                                else
                                {
                                    if (roomItem.GetBaseItem().InteractionType == InteractionType.hopper)
                                    {
                                        this.HopperCount++;
                                    }
                                    if (!this.MFloorItems.ContainsKey(num))
                                    {
                                        this.MFloorItems.Inner.TryAdd(num, roomItem);
                                    }
                                }
                            }
                        }
                        catch (Exception value2)
                        {
                            Console.WriteLine(value2);
                        }
                    }

                    foreach (RoomItem current in this.MFloorItems.Values)
                    {
                        if (current.IsWired)
                        {
                            this._room.GetWiredHandler().LoadWired(this._room.GetWiredHandler().GenerateNewItem(current));
                        }
                        if (current.IsRoller)
                        {
                            this.GotRollers = true;
                        }
                        else
                        {
                            if (current.GetBaseItem().InteractionType == InteractionType.dimmer)
                            {
                                if (this._room.MoodlightData == null)
                                {
                                    this._room.MoodlightData = new MoodlightData(current.Id);
                                }
                            }
                            else
                            {
                                if (current.GetBaseItem().InteractionType == InteractionType.roombg &&
                                    this._room.TonerData == null)
                                {
                                    this._room.TonerData = new TonerData(current.Id);
                                }
                            }
                        }
                    }
                }
            }
        }

        internal RoomItem GetItem(uint pId)
        {
            this.MFloorItems.OnCycle();
            this.MWallItems.OnCycle();

            if (this.MFloorItems.ContainsKey(pId))
            {
                return this.MFloorItems.GetValue(pId);
            }
            return this.MWallItems.ContainsKey(pId) ? this.MWallItems.GetValue(pId) : null;
        }

        internal void RemoveFurniture(GameClient session, uint pId, bool wasPicked = true)
        {
            RoomItem item = this.GetItem(pId);
            if (item == null)
            {
                return;
            }
            if (item.GetBaseItem().InteractionType == InteractionType.fbgate)
            {
                this._room.GetSoccer().UnRegisterGate(item);
            }
            if (item.GetBaseItem().InteractionType != InteractionType.gift)
            {
                item.Interactor.OnRemove(session, item);
            }
            this.RemoveRoomItem(item, wasPicked);
            this.MFloorItems.OnCycle();
        }

        internal void RemoveRoomItem(RoomItem item, bool wasPicked)
        {
            if (item.IsWallItem)
            {
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("PickUpWallItemMessageComposer"));
                serverMessage.AppendString(string.Format("{0}{1}", item.Id, string.Empty));
                if (wasPicked)
                {
                    serverMessage.AppendInteger(item.UserId);
                }
                else
                {
                    serverMessage.AppendInteger(0);
                }
                this._room.SendMessage(serverMessage);
            }
            else
            {
                if (item.IsFloorItem)
                {
                    var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("PickUpFloorItemMessageComposer"));
                    serverMessage2.AppendString(string.Format("{0}{1}", item.Id, string.Empty));
                    serverMessage2.AppendBool(false);
                    serverMessage2.AppendInteger((!wasPicked &&
                                                item.GetBaseItem().InteractionType == InteractionType.moplaseed)
                                               ? -1
                                               : 0);
                    if (wasPicked)
                    {
                        serverMessage2.AppendInteger(item.UserId);
                    }
                    else
                    {
                        serverMessage2.AppendInteger(0);
                    }
                    this._room.SendMessage(serverMessage2);
                }
            }
            if (item.IsWallItem)
            {
                this.MWallItems.Remove(item.Id);
            }
            else
            {
                this.MFloorItems.Remove(item.Id);
                this.MFloorItems.OnCycle();
                this._room.GetGameMap().RemoveFromMap(item);
            }
            this.RemoveItem(item);
            this._room.GetRoomUserManager().UpdateUserStatusses();
        }

        internal ServerMessage UpdateItemOnRoller(RoomItem pItem, Point nextCoord, uint pRolledId, double nextZ)
        {
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("ItemAnimationMessageComposer"));
            serverMessage.AppendInteger(pItem.X);
            serverMessage.AppendInteger(pItem.Y);
            serverMessage.AppendInteger(nextCoord.X);
            serverMessage.AppendInteger(nextCoord.Y);
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(pItem.Id);
            serverMessage.AppendString(TextHandling.GetString(pItem.Z));
            serverMessage.AppendString(TextHandling.GetString(nextZ));
            serverMessage.AppendInteger(pRolledId);
            this.SetFloorItem(pItem, nextCoord.X, nextCoord.Y, nextZ);
            return serverMessage;
        }

        internal ServerMessage UpdateUserOnRoller(RoomUser pUser, Point pNextCoord, uint pRollerId, double nextZ)
        {
            var serverMessage = new ServerMessage(0);
            serverMessage.Init(LibraryParser.OutgoingRequest("ItemAnimationMessageComposer"));
            serverMessage.AppendInteger(pUser.X);
            serverMessage.AppendInteger(pUser.Y);
            serverMessage.AppendInteger(pNextCoord.X);
            serverMessage.AppendInteger(pNextCoord.Y);
            serverMessage.AppendInteger(0);
            serverMessage.AppendInteger(pRollerId);
            serverMessage.AppendInteger(2);
            serverMessage.AppendInteger(pUser.VirtualId);
            serverMessage.AppendString(TextHandling.GetString(pUser.Z));
            serverMessage.AppendString(TextHandling.GetString(nextZ));
            this._room.GetGameMap()
                .UpdateUserMovement(new Point(pUser.X, pUser.Y), new Point(pNextCoord.X, pNextCoord.Y), pUser);
            this._room.GetGameMap().GameMap[pUser.X, pUser.Y] = 1;
            pUser.X = pNextCoord.X;
            pUser.Y = pNextCoord.Y;
            pUser.Z = nextZ;
            this._room.GetGameMap().GameMap[pUser.X, pUser.Y] = 0;
            return serverMessage;
        }

        internal bool SetFloorItem(GameClient session, RoomItem item, int newX, int newY, int newRot, bool newItem,
            bool onRoller, bool sendMessage)
        {
            return SetFloorItem(session, item, newX, newY, newRot, newItem, onRoller, sendMessage, true);
        }

        internal bool SetFloorItem(GameClient session, RoomItem item, int newX, int newY, int newRot, bool newItem,
            bool onRoller, bool sendMessage, bool updateRoomUserStatuses)
        {
            var flag = false;
            if (!newItem)
                flag = _room.GetGameMap().RemoveFromMap(item);

            var affectedTiles = Gamemap.GetAffectedTiles(item.GetBaseItem().Length,
                item.GetBaseItem().Width, newX, newY, newRot);

            if (!_room.GetGameMap().ValidTile(newX, newY) || (_room.GetGameMap().SquareHasUsers(newX, newY) && !item.GetBaseItem().IsSeat))
            {

                if (!flag)
                    return false;

                AddItem(item);
                _room.GetGameMap().AddToMap(item);
                return false;
            }

            if (
                affectedTiles.Values.Any(
                    current =>
                        !_room.GetGameMap().ValidTile(current.X, current.Y) ||
                        (_room.GetGameMap().SquareHasUsers(current.X, current.Y) && !item.GetBaseItem().IsSeat)))
            {
                if (!flag)
                    return false;
                AddItem(item);
                _room.GetGameMap().AddToMap(item);
                return false;
            }
            double num = _room.GetGameMap().Model.SqFloorHeight[newX][newY];
            if (!onRoller)
            {
                if (_room.GetGameMap().Model.SqState[newX][newY] != SquareState.Open && !item.GetBaseItem().IsSeat)
                {
                    if (!flag)
                        return false;
                    AddItem(item);
                    return false;
                }
                if (
                    affectedTiles.Values.Any(
                        current2 =>
                            !item.GetBaseItem().IsSeat && _room.GetGameMap().Model.SqState[current2.X][current2.Y] != SquareState.Open))
                {
                    if (!flag)
                        return false;

                    AddItem(item);
                    _room.GetGameMap().AddToMap(item);
                    return false;
                }
                if (!item.GetBaseItem().IsSeat && !item.IsRoller)
                    if (
                        affectedTiles.Values.Any(
                            current3 => _room.GetGameMap().GetRoomUsers(new Point(current3.X, current3.Y)).Any()))
                    {
                        if (!flag)
                            return false;
                        AddItem(item);
                        _room.GetGameMap().AddToMap(item);
                        return false;
                    }
            }
            var furniObjects = GetFurniObjects(newX, newY);
            var list = new List<RoomItem>();
            var list2 = new List<RoomItem>();
            foreach (
                var furniObjects2 in
                    affectedTiles.Values
                        .Select(current4 => GetFurniObjects(current4.X, current4.Y))
                        .Where(furniObjects2 => furniObjects2 != null))
                list.AddRange(furniObjects2);
            list2.AddRange(furniObjects);
            list2.AddRange(list);
            if (!onRoller)
                if (
                    list2.Any(
                        current5 =>
                            current5 != null && current5.Id != item.Id && current5.GetBaseItem() != null &&
                            !current5.GetBaseItem().Stackable))
                {
                    if (!flag)
                        return false;
                    AddItem(item);
                    _room.GetGameMap().AddToMap(item);
                    return false;
                }
            if (item.Rot != newRot && item.X == newX && item.Y == newY)
                num = item.Z;
            foreach (var current6 in list2)
                if (current6.Id != item.Id && current6.TotalHeight > num)
                    num = current6.TotalHeight;

            if (newRot != 0 && newRot != 2 && newRot != 4 && newRot != 6 && newRot != 8 &&
                item.GetBaseItem().InteractionType != InteractionType.mannequin)
                newRot = 0;
            item.Rot = newRot;

            item.SetState(newX, newY, num, affectedTiles);
            if (!onRoller && session != null)
                item.Interactor.OnPlace(session, item);
            if (newItem)
            {
                if (MFloorItems.ContainsKey(item.Id))
                    return true;
                if (item.IsFloorItem && !MFloorItems.ContainsKey(item.Id))
                    MFloorItems.Add(item.Id, item);
                else if (item.IsWallItem && !MWallItems.ContainsKey(item.Id))
                    MWallItems.Add(item.Id, item);
                AddItem(item);
                if (sendMessage)
                {
                    var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("AddFloorItemMessageComposer"));
                    item.Serialize(serverMessage);
                    serverMessage.AppendString(_room.Group != null ? session.GetHabbo().UserName : _room.Owner);
                    _room.SendMessage(serverMessage);
                }
            }
            else
            {
                UpdateItem(item);
                if (!onRoller && sendMessage)
                {
                    var message = new ServerMessage(LibraryParser.OutgoingRequest("UpdateRoomItemMessageComposer"));
                    item.Serialize(message);
                    _room.SendMessage(message);
                }
                if (item.IsWired)
                    _room.GetWiredHandler().MoveWired(item);
            }
            _room.GetGameMap().AddToMap(item);
            if (item.GetBaseItem().IsSeat)
                updateRoomUserStatuses = true;
            if (updateRoomUserStatuses)
                _room.GetRoomUserManager().UpdateUserStatusses();
            if (newItem)
                OnHeightmapUpdate(affectedTiles);

            return true;
        }

        internal void OnHeightmapUpdate(Dictionary<int, ThreeDCoord> affectedTiles)
        {
            var message = new ServerMessage(LibraryParser.OutgoingRequest("UpdateFurniStackMapMessageComposer"));
            message.AppendByted((byte)affectedTiles.Count);
            foreach (ThreeDCoord coord in affectedTiles.Values)
            {
                message.AppendByted((byte)coord.X);
                message.AppendByted((byte)coord.Y);
                message.AppendShort((short)(this._room.GetGameMap().SqAbsoluteHeight(coord.X, coord.Y) * 256));
            }
            this._room.SendMessage(message);
        }

        internal void OnHeightmapUpdate(ICollection affectedTiles)
        {
            var message = new ServerMessage(LibraryParser.OutgoingRequest("UpdateFurniStackMapMessageComposer"));
            message.AppendByted((byte)affectedTiles.Count);
            foreach (Point coord in affectedTiles)
            {
                message.AppendByted((byte)coord.X);
                message.AppendByted((byte)coord.Y);
                message.AppendShort((short)(this._room.GetGameMap().SqAbsoluteHeight(coord.X, coord.Y) * 256));
            }
            this._room.SendMessage(message);
        }

        internal void OnHeightmapUpdate(List<Point> oldCoords, List<Point> newCoords)
        {
            var message = new ServerMessage(LibraryParser.OutgoingRequest("UpdateFurniStackMapMessageComposer"));
            message.AppendByted((byte)(oldCoords.Count + newCoords.Count));
            foreach (Point coord in oldCoords)
            {
                message.AppendByted((byte)coord.X);
                message.AppendByted((byte)coord.Y);
                message.AppendShort((short)(this._room.GetGameMap().SqAbsoluteHeight(coord.X, coord.Y) * 256));
            }
            foreach (Point nCoord in newCoords)
            {
                message.AppendByted((byte)nCoord.X);
                message.AppendByted((byte)nCoord.Y);
                message.AppendShort((short)(this._room.GetGameMap().SqAbsoluteHeight(nCoord.X, nCoord.Y) * 256));
            }
            this._room.SendMessage(message);
        }

        internal List<RoomItem> GetFurniObjects(int x, int y)
        {
            return this._room.GetGameMap().GetCoordinatedItems(new Point(x, y));
        }

        internal bool SetFloorItem(RoomItem item, int newX, int newY, double newZ)
        {
            this._room.GetGameMap().RemoveFromMap(item);
            item.SetState(newX, newY, newZ,
                Gamemap.GetAffectedTiles(item.GetBaseItem().Length,
                    item.GetBaseItem().Width, newX, newY, item.Rot));
            if (item.GetBaseItem().InteractionType == InteractionType.roombg && this._room.TonerData == null)
            {
                this._room.TonerData = new TonerData(item.Id);
            }
            this.UpdateItem(item);
            this._room.GetGameMap().AddItemToMap(item, true);
            return true;
        }

        internal bool SetFloorItem(RoomItem item, int newX, int newY, double newZ, int rot, bool sendupdate)
        {
            this._room.GetGameMap().RemoveFromMap(item);
            item.SetState(newX, newY, newZ,
                Gamemap.GetAffectedTiles(item.GetBaseItem().Length,
                    item.GetBaseItem().Width, newX, newY, rot));
            if (item.GetBaseItem().InteractionType == InteractionType.roombg && this._room.TonerData == null)
            {
                this._room.TonerData = new TonerData(item.Id);
            }
            this.UpdateItem(item);
            this._room.GetGameMap().AddItemToMap(item, true);
            if (!sendupdate)
            {
                return true;
            }
            var message = new ServerMessage(LibraryParser.OutgoingRequest("UpdateRoomItemMessageComposer"));
            item.Serialize(message);
            this._room.SendMessage(message);
            return true;
        }

        internal bool SetWallItem(GameClient session, RoomItem item)
        {
            if (!item.IsWallItem || this.MWallItems.ContainsKey(item.Id))
            {
                return false;
            }
            if (this.MFloorItems.ContainsKey(item.Id))
            {
                return true;
            }
            item.Interactor.OnPlace(session, item);
            if (item.GetBaseItem().InteractionType == InteractionType.dimmer && this._room.MoodlightData == null)
            {
                this._room.MoodlightData = new MoodlightData(item.Id);
                item.ExtraData = this._room.MoodlightData.GenerateExtraData();
            }
            this.MWallItems.Add(item.Id, item);
            this.AddItem(item);
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("AddWallItemMessageComposer"));
            item.Serialize(serverMessage);
            serverMessage.AppendString(this._room.Owner);
            this._room.SendMessage(serverMessage);
            return true;
        }

        internal void UpdateItem(RoomItem item)
        {
            if (this._mAddedItems.Contains(item.Id))
            {
                return;
            }
            if (this._mRemovedItems.Contains(item.Id))
            {
                this._mRemovedItems.Remove(item.Id);
            }
            if (!this._mMovedItems.Contains(item.Id))
            {
                this._mMovedItems.Add(item.Id, item);
            }
        }

        internal void AddItem(RoomItem item)
        {
            if (this._mRemovedItems.Contains(item.Id))
            {
                this._mRemovedItems.Remove(item.Id);
            }
            if (!this._mMovedItems.Contains(item.Id) && !this._mAddedItems.Contains(item.Id))
            {
                this._mAddedItems.Add(item.Id, item);
            }
        }

        internal void RemoveItem(RoomItem item)
        {
            if (this._mAddedItems.Contains(item.Id))
            {
                this._mAddedItems.Remove(item.Id);
            }
            if (this._mMovedItems.Contains(item.Id))
            {
                this._mMovedItems.Remove(item.Id);
            }
            if (!this._mRemovedItems.Contains(item.Id))
            {
                this._mRemovedItems.Add(item.Id, item);
            }
            this.MRollers.Remove(item.Id);
        }

        internal void OnCycle()
        {
            if (this.GotRollers)
            {
                try
                {
                    this._room.SendMessage(this.CycleRollers());
                }
                catch (Exception ex)
                {
                    Logging.LogThreadException(ex.ToString(), string.Format("rollers for room with ID {0}", this._room.RoomId));
                    this.GotRollers = false;
                }
            }
            if (this._roomItemUpdateQueue.Count > 0)
            {
                var list = new List<RoomItem>();
                lock (this._roomItemUpdateQueue.SyncRoot)
                {
                    while (this._roomItemUpdateQueue.Count > 0)
                    {
                        var roomItem = (RoomItem)this._roomItemUpdateQueue.Dequeue();
                        roomItem.ProcessUpdates();
                        if (roomItem.IsTrans || roomItem.UpdateCounter > 0)
                        {
                            list.Add(roomItem);
                        }
                    }
                    foreach (RoomItem current in list)
                    {
                        this._roomItemUpdateQueue.Enqueue(current);
                    }
                }
            }
            this.MFloorItems.OnCycle();
            this.MWallItems.OnCycle();
        }

        internal void Destroy()
        {
            this.MFloorItems.Clear();
            this.MWallItems.Clear();
            this._mRemovedItems.Clear();
            this._mMovedItems.Clear();
            this._mAddedItems.Clear();
            this._roomItemUpdateQueue.Clear();
            this._room = null;
            this.MFloorItems = null;
            this.MWallItems = null;
            this._mRemovedItems = null;
            this._mMovedItems = null;
            this._mAddedItems = null;
            this.breedingBear.Clear();
            this.breedingTerrier.Clear();
            this.MWallItems = null;
            this._roomItemUpdateQueue = null;
            this.breedingBear.Clear();
            this.breedingTerrier.Clear();
        }

        private void ClearRollers()
        {
            this.MRollers.Clear();
        }

        private List<ServerMessage> CycleRollers()
        {
            this.MRollers.OnCycle();
            if (!this.GotRollers)
            {
                return new List<ServerMessage>();
            }
            if (this._mRoolerCycle >= this._mRollerSpeed || this._mRollerSpeed == 0)
            {
                this._rollerItemsMoved.Clear();
                this._rollerUsersMoved.Clear();
                this._rollerMessages.Clear();
                foreach (RoomItem current in this.MRollers.Values)
                {
                    Point squareInFront = current.SquareInFront;
                    List<RoomItem> roomItemForSquare = this._room.GetGameMap().GetRoomItemForSquare(current.X, current.Y);
                    RoomUser userForSquare = this._room.GetRoomUserManager().GetUserForSquare(current.X, current.Y);
                    if (!roomItemForSquare.Any() && userForSquare == null)
                    {
                        continue;
                    }
                    List<RoomItem> coordinatedItems = this._room.GetGameMap().GetCoordinatedItems(squareInFront);
                    double nextZ = 0.0;
                    int num = 0;
                    bool flag = false;
                    double num2 = 0.0;
                    bool flag2 = true;
                    foreach (RoomItem current2 in coordinatedItems.Where(current2 => current2.IsRoller))
                    {
                        flag = true;
                        if (current2.TotalHeight > num2)
                        {
                            num2 = current2.TotalHeight;
                        }
                    }
                    if (flag)
                    {
                        using (List<RoomItem>.Enumerator enumerator3 = coordinatedItems.GetEnumerator())
                        {
                            while (enumerator3.MoveNext())
                            {
                                RoomItem current3 = enumerator3.Current;
                                if (current3.TotalHeight > num2)
                                {
                                    flag2 = false;
                                }
                            }
                            goto IL_192;
                        }
                    }
                    goto IL_17C;
                    IL_192:
                    nextZ = num2;
                    bool flag3 = num > 0 ||
                                 this._room.GetRoomUserManager().GetUserForSquare(squareInFront.X, squareInFront.Y) != null;
                    foreach (RoomItem current4 in roomItemForSquare)
                    {
                        double num3 = current4.Z - current.TotalHeight;
                        if (this._rollerItemsMoved.Contains(current4.Id) ||
                            !this._room.GetGameMap().CanRollItemHere(squareInFront.X, squareInFront.Y) || !flag2 ||
                            !(current.Z < current4.Z) ||
                            this._room.GetRoomUserManager().GetUserForSquare(squareInFront.X, squareInFront.Y) != null)
                        {
                            continue;
                        }
                        this._rollerMessages.Add(this.UpdateItemOnRoller(current4, squareInFront, current.Id, num2 + num3));
                        this._rollerItemsMoved.Add(current4.Id);
                    }
                    if (userForSquare != null && !userForSquare.IsWalking && flag2 && !flag3 &&
                        this._room.GetGameMap().CanRollItemHere(squareInFront.X, squareInFront.Y) &&
                        this._room.GetGameMap().GetFloorStatus(squareInFront) != 0 &&
                        !this._rollerUsersMoved.Contains(userForSquare.HabboId))
                    {
                        this._rollerMessages.Add(this.UpdateUserOnRoller(userForSquare, squareInFront, current.Id, nextZ));
                        this._rollerUsersMoved.Add(userForSquare.HabboId);
                    }
                    continue;
                    IL_17C:
                    num2 += this._room.GetGameMap().GetHeightForSquareFromData(squareInFront);
                    goto IL_192;
                }
                this._mRoolerCycle = 0;
                return this._rollerMessages;
            }
            checked
            {
                this._mRoolerCycle++;
            }
            return new List<ServerMessage>();
        }
    }
}