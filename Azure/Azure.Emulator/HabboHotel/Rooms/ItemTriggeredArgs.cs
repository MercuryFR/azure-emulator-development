using System;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Rooms
{
    public class ItemTriggeredArgs : EventArgs
    {
        internal readonly RoomUser TriggeringUser;
        internal readonly RoomItem TriggeringItem;

        public ItemTriggeredArgs(RoomUser user, RoomItem item)
        {
            this.TriggeringUser = user;
            this.TriggeringItem = item;
        }
    }
}