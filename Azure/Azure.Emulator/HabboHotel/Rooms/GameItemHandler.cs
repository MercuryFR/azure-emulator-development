using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Azure.Collections;
using Azure.HabboHotel.Items;
using Azure.Util;

namespace Azure.HabboHotel.Rooms
{
    internal class GameItemHandler
    {
        private QueuedDictionary<uint, RoomItem> _banzaiTeleports;
        private ConcurrentDictionary<uint, RoomItem> _banzaiPyramids;
        private Room _room;

        public GameItemHandler(Room room)
        {
            this._room = room;
            this._banzaiPyramids = new ConcurrentDictionary<uint, RoomItem>();
            this._banzaiTeleports = new QueuedDictionary<uint, RoomItem>();
        }

        internal void OnCycle()
        {
            this.CyclePyramids();
            this.CycleRandomTeleports();
        }

        internal void AddPyramid(RoomItem item, uint itemId)
        {
            if (this._banzaiPyramids.ContainsKey(itemId))
            {
                this._banzaiPyramids[itemId] = item;
                return;
            }
            this._banzaiPyramids.TryAdd(itemId, item);
        }

        internal void RemovePyramid(uint itemId)
        {
            RoomItem e;
            this._banzaiPyramids.TryRemove(itemId, out e);
        }

        internal void AddTeleport(RoomItem item, uint itemId)
        {
            if (this._banzaiTeleports.ContainsKey(itemId))
            {
                this._banzaiTeleports.Inner[itemId] = item;
                return;
            }
            this._banzaiTeleports.Add(itemId, item);
        }

        internal void RemoveTeleport(uint itemId)
        {
            this._banzaiTeleports.Remove(itemId);
        }

        internal void OnTeleportRoomUserEnter(RoomUser user, RoomItem item)
        {
            IEnumerable<RoomItem> enumerable = from p in this._banzaiTeleports.Inner.Values
                                               where p.Id != item.Id
                                               select p;
            if (!enumerable.Any())
                return;

            int num = enumerable.Count();
            int num2 = RandomNumber.Get(0, num);
            int num3 = 0;
            checked
            {
                foreach (RoomItem current in enumerable.Where(current => current != null))
                {
                    if (num3 == num2)
                    {
                        current.ExtraData = "1";
                        current.UpdateNeeded = true;
                        this._room.GetGameMap().TeleportToItem(user, current);
                        item.ExtraData = "1";
                        item.UpdateNeeded = true;
                        current.UpdateState();
                        item.UpdateState();
                    }
                    num3++;
                }
            }
        }

        internal void Destroy()
        {
            if (this._banzaiTeleports != null)
            {
                this._banzaiTeleports.Destroy();
            }
            if (this._banzaiPyramids != null)
            {
                this._banzaiPyramids.Clear();
            }
            this._banzaiPyramids = null;
            this._banzaiTeleports = null;
            this._room = null;
        }

        private void CyclePyramids()
        {
            foreach (RoomItem item in this._banzaiPyramids.Select(pyramid => pyramid.Value).Where(current => current != null))
            {
                if (item.InteractionCountHelper == 0 && item.ExtraData == "1")
                {
                    this._room.GetGameMap().RemoveFromMap(item, false);
                    item.InteractionCountHelper = 1;
                }
                if (string.IsNullOrEmpty(item.ExtraData))
                    item.ExtraData = "0";

                var randomNumber = Azure.GetRandomNumber(0, 30);
                if (randomNumber <= 26)
                    continue;
                if (item.ExtraData == "0")
                {
                    item.ExtraData = "1";
                    item.UpdateState();
                    _room.GetGameMap().RemoveFromMap(item, false);
                }
                else
                {
                    if (!_room.GetGameMap().ItemCanBePlacedHere(item.X, item.Y))
                        continue;
                    item.ExtraData = "0";
                    item.UpdateState();
                    _room.GetGameMap().AddItemToMap(item, false);
                }
            }
        }

        private void CycleRandomTeleports()
        {
            this._banzaiTeleports.OnCycle();
        }
    }
}