using System;
using System.Collections.Generic;
using System.Data;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.Rooms;

namespace Azure.HabboHotel.Items
{
    class PinataHandler
    {
        internal Dictionary<uint, PinataItem> Pinatas;
        private DataTable _table;

        internal void Initialize(IQueryAdapter dbClient)
        {
            dbClient.SetQuery("SELECT * FROM items_pinatas");
            Pinatas = new Dictionary<uint, PinataItem>();
            _table = dbClient.GetTable();
            foreach (DataRow dataRow in _table.Rows)
            {
                var value = new PinataItem(dataRow);
                Pinatas.Add(uint.Parse(dataRow["item_baseid"].ToString()), value);
            }
        }

        internal void DeliverRandomPinataItem(RoomUser user, Room room, RoomItem item)
        {
            if (room == null || item == null || item.GetBaseItem().InteractionType != InteractionType.pinata ||
                !Pinatas.ContainsKey(item.GetBaseItem().ItemId))
                return;
            PinataItem pinataItem;
            Pinatas.TryGetValue(item.GetBaseItem().ItemId, out pinataItem);
            if (pinataItem == null || pinataItem.Rewards.Count < 1)
                return;
            //var getX = item.X;
            //var getY = item.Y;
            //var getZ = item.Z;
            item.RefreshItem();
            item.BaseItem = pinataItem.Rewards[new Random().Next(checked(pinataItem.Rewards.Count - 1))];
            item.ExtraData = "";
            room.GetRoomItemHandler().RemoveFurniture(user.GetClient(), item.Id, false);
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE items_rooms SET base_item = '",
                    item.BaseItem,
                    "', extra_data = '' WHERE id = ",
                    item.Id
                }));
                queryReactor.RunQuery();
            }
            if (!room.GetRoomItemHandler().SetFloorItem(user.GetClient(), item, item.X, item.Y, 0, true, false, true))
                user.GetClient().GetHabbo().GetInventoryComponent().AddItem(item);
        }
    }
}