using Azure.HabboHotel.Rooms;

namespace Azure.HabboHotel.Items
{
    public delegate void OnItemTrigger(object sender, ItemTriggeredArgs e);
}