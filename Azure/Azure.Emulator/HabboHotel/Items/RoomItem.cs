using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Azure.Configuration;
using Azure.HabboHotel.Items.Interactor;
using Azure.HabboHotel.Pathfinding;
using Azure.HabboHotel.PathFinding;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.Rooms;
using Azure.HabboHotel.Rooms.Games;
using Azure.HabboHotel.Rooms.Wired;
using Azure.HabboHotel.SoundMachine;
using Azure.Messages;
using Azure.Messages.Parsers;
using Azure.Util;

namespace Azure.HabboHotel.Items
{
    public class RoomItem : IEquatable<RoomItem>
    {
        public byte InteractionCount;
        internal uint Id;
        internal uint RoomId;
        internal uint BaseItem;
        internal uint UserId;
        internal string ExtraData;
        internal Team Team;
        internal byte InteractionCountHelper;
        internal int Value;
        internal FreezePowerUp FreezePowerUp;
        internal string GroupData;
        internal uint GroupId;
        internal uint InteractingBallUser;
        internal string SongCode;
        internal int Rot;
        internal WallCoordinate WallCoord;
        internal List<Pet> PetsList = new List<Pet>(2);
        internal int UpdateCounter;
        internal uint InteractingUser;
        internal uint InteractingUser2;
        internal bool IsTrans;
        internal bool PendingReset;
        internal bool MagicRemove;
        internal int LimitedNo;
        internal int LimitedTot;
        internal bool VikingCotieBurning;
        internal bool OnCannonActing = false;
        internal IComeDirection ComeDirection;
        internal int BallValue;
        internal bool BallIsMoving;
        private bool _updateNeeded;
        private Item _mBaseItem;
        private Room _mRoom;
        internal bool IsBuilder;

        internal RoomItem(uint id, uint roomId, uint baseItem, string extraData, int x, int y, double z, int rot,
            Room pRoom, uint userid, uint @group, int flatId, string songCode, bool isBuilder)
        {
            Id = id;
            RoomId = roomId;
            BaseItem = baseItem;
            ExtraData = extraData;
            GroupId = @group;
            X = x;
            Y = y;
            if (!double.IsInfinity(z))
                Z = z;
            Rot = rot;
            UpdateNeeded = false;
            UpdateCounter = 0;
            InteractingUser = 0u;
            InteractingUser2 = 0u;
            IsTrans = false;
            InteractingBallUser = 0u;
            InteractionCount = 0;
            Value = 0;
            UserId = userid;
            SongCode = songCode;
            IsBuilder = isBuilder;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT * FROM items_limited WHERE item_id={0} LIMIT 1", id));
                var row = queryReactor.GetRow();
                if (row != null)
                {
                    LimitedNo = int.Parse(row[1].ToString());
                    LimitedTot = int.Parse(row[2].ToString());
                }
                else
                {
                    LimitedNo = 0;
                    LimitedTot = 0;
                }
            }
            if (GetBaseItem().Name.StartsWith("gld_") || GetBaseItem().Name.StartsWith("guild_") ||
                GetBaseItem().Name.Contains("grp"))
            {
                GroupData = extraData;
                ExtraData = GroupData.Split(';')[0];
                if (GroupData.Contains(";;"))
                {
                    GroupData = GroupData.Replace(";;", ";");
                    _mRoom.GetRoomItemHandler().UpdateItem(this);
                }
            }
            _mBaseItem = Azure.GetGame().GetItemManager().GetItem(baseItem);
            _mRoom = pRoom;
            if (GetBaseItem() == null)
                Logging.LogException(string.Format("Unknown baseID: {0}", baseItem));
            var interactionType = GetBaseItem().InteractionType;
            if (interactionType <= InteractionType.roller)
                switch (interactionType)
                {
                    case InteractionType.hopper:
                        IsTrans = true;
                        ReqUpdate(0, true);
                        break;
                    case InteractionType.teleport:
                        IsTrans = true;
                        ReqUpdate(0, true);
                        break;
                    default:
                        if (interactionType == InteractionType.roller)
                        {
                            IsRoller = true;
                            pRoom.GetRoomItemHandler().GotRollers = true;
                        }
                        break;
                }
            else
                switch (interactionType)
                {
                    case InteractionType.footballcountergreen:
                    case InteractionType.banzaigategreen:
                    case InteractionType.banzaiscoregreen:
                    case InteractionType.freezegreencounter:
                    case InteractionType.freezegreengate:
                        Team = Team.green;
                        break;
                    case InteractionType.footballcounteryellow:
                    case InteractionType.banzaigateyellow:
                    case InteractionType.banzaiscoreyellow:
                    case InteractionType.freezeyellowcounter:
                    case InteractionType.freezeyellowgate:
                        Team = Team.yellow;
                        break;
                    case InteractionType.footballcounterblue:
                    case InteractionType.banzaigateblue:
                    case InteractionType.banzaiscoreblue:
                    case InteractionType.freezebluecounter:
                    case InteractionType.freezebluegate:
                        Team = Team.blue;
                        break;
                    case InteractionType.footballcounterred:
                    case InteractionType.banzaigatered:
                    case InteractionType.banzaiscorered:
                    case InteractionType.freezeredcounter:
                    case InteractionType.freezeredgate:
                        Team = Team.red;
                        break;
                    case InteractionType.banzaifloor:
                    case InteractionType.banzaicounter:
                    case InteractionType.banzaipuck:
                    case InteractionType.banzaipyramid:
                    case InteractionType.freezetimer:
                    case InteractionType.freezeexit:
                        break;
                    case InteractionType.banzaitele:
                        ExtraData = "";
                        break;
                    case InteractionType.breedingterrier:
                    {
                        if (!pRoom.GetRoomItemHandler().breedingTerrier.ContainsKey(Id))
                            pRoom.GetRoomItemHandler().breedingTerrier.Add(Id, this);
                        break;
                    }
                    case InteractionType.breedingbear:
                    {
                        if (!pRoom.GetRoomItemHandler().breedingBear.ContainsKey(Id))
                            pRoom.GetRoomItemHandler().breedingBear.Add(Id, this);
                        break;
                    }
                    default:
                        if (interactionType == InteractionType.vikingcotie)
                        {
                            int num;
                            if (int.TryParse(extraData, out num) && num >= 1 && num < 5)
                                VikingCotieBurning = true;
                        }
                        break;
                }
            IsWallItem = (GetBaseItem().Type.ToString().ToLower() == "i");
            IsFloorItem = (GetBaseItem().Type.ToString().ToLower() == "s");
            AffectedTiles = Gamemap.GetAffectedTiles(GetBaseItem().Length, GetBaseItem().Width, X, Y, rot);
        }

        internal RoomItem(uint id, uint roomId, uint baseItem, string extraData, WallCoordinate wallCoord, Room pRoom,
            uint userid, uint @group, int flatId, bool isBuilder)
        {
            Id = id;
            RoomId = roomId;
            BaseItem = baseItem;
            ExtraData = extraData;
            GroupId = @group;
            X = 0;
            Y = 0;
            Z = 0.0;
            UpdateNeeded = false;
            UpdateCounter = 0;
            InteractingUser = 0u;
            InteractingUser2 = 0u;
            IsTrans = false;
            InteractingBallUser = 0u;
            InteractionCount = 0;
            Value = 0;
            WallCoord = wallCoord;
            UserId = userid;
            _mBaseItem = Azure.GetGame().GetItemManager().GetItem(baseItem);
            _mRoom = pRoom;
            IsBuilder = isBuilder;
            if (GetBaseItem() == null)
                Logging.LogException(string.Format("Unknown baseID: {0}", baseItem));
            IsWallItem = true;
            IsFloorItem = false;
            AffectedTiles = new Dictionary<int, ThreeDCoord>();
            SongCode = "";
        }

        internal event OnItemTrigger ItemTriggerEventHandler;
        internal event UserWalksFurniDelegate OnUserWalksOffFurni;
        internal event UserWalksFurniDelegate OnUserWalksOnFurni;

        public bool IsWired
        {
            get
            {
                switch (GetBaseItem().InteractionType)
                {
                    case InteractionType.triggertimer:
                    case InteractionType.triggerroomenter:
                    case InteractionType.triggergameend:
                    case InteractionType.triggergamestart:
                    case InteractionType.triggerrepeater:
                    case InteractionType.triggerlongrepeater:
                    case InteractionType.triggeronusersay:
                    case InteractionType.triggerscoreachieved:
                    case InteractionType.triggerstatechanged:
                    case InteractionType.triggerwalkonfurni:
                    case InteractionType.triggerwalkofffurni:
                    case InteractionType.actiongivescore:
                    case InteractionType.actionposreset:
                    case InteractionType.actionmoverotate:
                    case InteractionType.actionresettimer:
                    case InteractionType.actionshowmessage:
                    case InteractionType.actionteleportto:
                    case InteractionType.actiontogglestate:
                    case InteractionType.actionkickuser:
                    case InteractionType.actiongivereward:
                    case InteractionType.actionmuteuser:
                    case InteractionType.conditionfurnishaveusers:
                    case InteractionType.conditionstatepos:
                    case InteractionType.conditiontimelessthan:
                    case InteractionType.conditiontimemorethan:
                    case InteractionType.conditiontriggeronfurni:
                    case InteractionType.conditionfurnihasfurni:
                    case InteractionType.conditionitemsmatches:
                    case InteractionType.conditiongroupmember:
                    case InteractionType.conditionfurnitypematches:
                    case InteractionType.conditionhowmanyusersinroom:
                    case InteractionType.conditiontriggerernotonfurni:
                    case InteractionType.conditionfurnihasnotfurni:
                    case InteractionType.conditionfurnishavenotusers:
                    case InteractionType.conditionitemsdontmatch:
                    case InteractionType.conditionfurnitypedontmatch:
                    case InteractionType.conditionnotgroupmember:
                    case InteractionType.conditionnegativehowmanyusers:
                    case InteractionType.conditionuserwearingeffect:
                    case InteractionType.conditionusernotwearingeffect:
                    case InteractionType.conditionuserwearingbadge:
                    case InteractionType.conditionusernotwearingbadge:
                    case InteractionType.conditiondaterangeactive:
                        return true;
                    default:
                        return false;
                }
            }
        }

        internal Dictionary<int, ThreeDCoord> AffectedTiles { get; private set; }

        internal int X { get; private set; }

        internal int Y { get; private set; }

        internal double Z { get; private set; }

        internal bool UpdateNeeded
        {
            get { return _updateNeeded; }
            set
            {
                if (value)
                    GetRoom().GetRoomItemHandler().QueueRoomItemUpdate(this);
                _updateNeeded = value;
            }
        }

        internal bool IsRoller { get; private set; }

        internal Point Coordinate
        {
            get { return new Point(X, Y); }
        }

        internal List<Point> GetCoords
        {
            get
            {
                var list = new List<Point> {Coordinate};
                list.AddRange(AffectedTiles.Values.Select(current => new Point(current.X, current.Y)));
                return list;
            }
        }

        internal double TotalHeight
        {
            get
            {
                try
                {
                    if (GetBaseItem() == null)
                        return Z;
                    if (!GetBaseItem().StackMultipler)
                        return Z + GetBaseItem().Height;
                    if (string.IsNullOrEmpty(ExtraData))
                        ExtraData = "0";

                    return Z + GetBaseItem().ToggleHeight[int.Parse(ExtraData)];
                }
                catch (Exception e)
                {
                    Writer.Writer.LogException("TotalHeight with furni BaseId: " + BaseItem + " in RoomId:" + RoomId +
                                               Environment.NewLine + e);
                    return 1;
                }
            }
        }

        internal bool IsWallItem { get; private set; }

        internal bool IsFloorItem { get; private set; }

        internal Point SquareInFront
        {
            get
            {
                var result = new Point(X, Y);
                checked
                {
                    switch (Rot)
                    {
                        case 0:
                            result.Y--;
                            break;
                        case 2:
                            result.X++;
                            break;
                        case 4:
                            result.Y++;
                            break;
                        case 6:
                            result.X--;
                            break;
                    }
                    return result;
                }
            }
        }

        internal Point SquareBehind
        {
            get
            {
                var result = new Point(X, Y);
                checked
                {
                    switch (Rot)
                    {
                        case 0:
                            result.Y++;
                            break;
                        case 2:
                            result.X--;
                            break;
                        case 4:
                            result.Y--;
                            break;
                        case 6:
                            result.X++;
                            break;
                    }
                    return result;
                }
            }
        }

        internal IFurniInteractor Interactor
        {
            get
            {
                if (IsWired)
                    return new InteractorWired();
                var interactionType = GetBaseItem().InteractionType;
                switch (interactionType)
                {
                    case InteractionType.gate:
                        return new InteractorGate();
                    case InteractionType.gld_gate:
                        return new InteractorGroupGate();
                    case InteractionType.scoreboard:
                        return new InteractorScoreboard();
                    case InteractionType.vendingmachine:
                        return new InteractorVendor();
                    case InteractionType.alert:
                        return new InteractorAlert();
                    case InteractionType.onewaygate:
                        return new InteractorOneWayGate();
                    case InteractionType.loveshuffler:
                        return new InteractorLoveShuffler();
                    case InteractionType.habbowheel:
                        return new InteractorHabboWheel();
                    case InteractionType.dice:
                        return new InteractorDice();
                    case InteractionType.bottle:
                        return new InteractorSpinningBottle();
                    case InteractionType.hopper:
                        return new InteractorHopper();
                    case InteractionType.teleport:
                        return new InteractorTeleport();
                    case InteractionType.football:
                        return new InteractorFootball();
                    case InteractionType.footballcountergreen:
                    case InteractionType.footballcounteryellow:
                    case InteractionType.footballcounterblue:
                    case InteractionType.footballcounterred:
                        return new InteractorScoreCounter();
                    case InteractionType.banzaiscoreblue:
                    case InteractionType.banzaiscorered:
                    case InteractionType.banzaiscoreyellow:
                    case InteractionType.banzaiscoregreen:
                        return new InteractorBanzaiScoreCounter();
                    case InteractionType.banzaicounter:
                        return new InteractorBanzaiTimer();
                    case InteractionType.freezetimer:
                        return new InteractorFreezeTimer();
                    case InteractionType.freezeyellowcounter:
                    case InteractionType.freezeredcounter:
                    case InteractionType.freezebluecounter:
                    case InteractionType.freezegreencounter:
                        return new InteractorFreezeScoreCounter();
                    case InteractionType.freezetileblock:
                    case InteractionType.freezetile:
                        return new InteractorFreezeTile();
                    case InteractionType.jukebox:
                        return new InteractorJukebox();
                    case InteractionType.puzzlebox:
                        return new InteractorPuzzleBox();
                    case InteractionType.mannequin:
                        return new InteractorMannequin();
                    case InteractionType.fireworks:
                        return new InteractorFireworks();
                    case InteractionType.groupforumterminal:
                        return new InteractorGroupForumTerminal();
                    case InteractionType.vikingcotie:
                        return new InteractorVikingCotie();
                    case InteractionType.cannon:
                        return new InteractorCannon();
                    default:
                        return new InteractorGenericSwitch();
                }
            }
        }

        public bool Equals(RoomItem comparedItem) { return comparedItem.Id == Id; }

        internal void SetState(int pX, int pY, double pZ, Dictionary<int, ThreeDCoord> tiles)
        {
            X = pX;
            Y = pY;
            if (!double.IsInfinity(pZ))
                Z = pZ;
            AffectedTiles = tiles;
        }

        internal void OnTrigger(RoomUser user)
        {
            if (ItemTriggerEventHandler != null)
                ItemTriggerEventHandler(null, new ItemTriggeredArgs(user, this));
        }

        internal void Destroy()
        {
            _mRoom = null;
            AffectedTiles.Clear();
            ItemTriggerEventHandler = null;
            OnUserWalksOffFurni = null;
            OnUserWalksOnFurni = null;
        }

        internal void ProcessUpdates()
        {
            checked
            {
                UpdateCounter--;
                if (UpdateCounter > 0 && !IsTrans)
                    return;
                UpdateNeeded = false;
                UpdateCounter = 0;
                var random = new Random();
                var interactionType = GetBaseItem().InteractionType;
                if (interactionType <= InteractionType.pressurepad)
                    switch (interactionType)
                    {
                        case InteractionType.scoreboard:
                        {
                            if (string.IsNullOrEmpty(ExtraData))
                                return;
                            var num = 0;
                            try
                            {
                                num = int.Parse(ExtraData);
                            }
                            catch
                            {
                            }
                            if (num > 0)
                            {
                                if (InteractionCountHelper == 1)
                                {
                                    num--;
                                    InteractionCountHelper = 0;
                                    ExtraData = num.ToString();
                                    UpdateState();
                                }
                                else
                                    InteractionCountHelper += 1;
                                UpdateCounter = 1;
                                return;
                            }
                            UpdateCounter = 0;
                            return;
                        }
                        case InteractionType.vendingmachine:
                            if (ExtraData == "1")
                            {
                                var roomUser = GetRoom().GetRoomUserManager().GetRoomUserByHabbo(InteractingUser);
                                if (roomUser != null)
                                {
                                    roomUser.UnlockWalking();
                                    if (GetBaseItem().VendingIds.Count > 0)
                                    {
                                        var item =
                                            GetBaseItem().VendingIds[
                                                RandomNumber.Get(0, GetBaseItem().VendingIds.Count - 1)];
                                        roomUser.CarryItem(item);
                                    }
                                }
                                InteractingUser = 0u;
                                ExtraData = "0";
                                UpdateState(false, true);
                            }
                            break;
                        case InteractionType.alert:
                            if (ExtraData == "1")
                            {
                                ExtraData = "0";
                                UpdateState(false, true);
                            }
                            break;
                        case InteractionType.onewaygate:
                        {
                            RoomUser roomUser = null;
                            if (InteractingUser > 0u)
                                roomUser = GetRoom().GetRoomUserManager().GetRoomUserByHabbo(InteractingUser);
                            if (roomUser != null && roomUser.X == X && roomUser.Y == Y)
                            {
                                ExtraData = "1";
                                roomUser.MoveTo(SquareBehind);
                                roomUser.InteractingGate = false;
                                roomUser.GateId = 0u;
                                ReqUpdate(1, false);
                                UpdateState(false, true);
                            }
                            else if (roomUser != null && roomUser.Coordinate == SquareBehind)
                            {
                                roomUser.UnlockWalking();
                                ExtraData = "0";
                                InteractingUser = 0u;
                                roomUser.InteractingGate = false;
                                roomUser.GateId = 0u;
                                UpdateState(false, true);
                            }
                            else if (ExtraData == "1")
                            {
                                ExtraData = "0";
                                UpdateState(false, true);
                            }
                            if (roomUser == null)
                                InteractingUser = 0u;
                            break;
                        }
                        case InteractionType.loveshuffler:
                            if (ExtraData == "0")
                            {
                                ExtraData = RandomNumber.Get(1, 4).ToString();
                                ReqUpdate(20, false);
                            }
                            else
                                ExtraData = "-1";
                            UpdateState(false, true);
                            return;
                        case InteractionType.habbowheel:
                            ExtraData = RandomNumber.Get(1, 10).ToString();
                            UpdateState();
                            return;
                        case InteractionType.dice:
                            ExtraData = random.Next(1, 7).ToString();
                            UpdateState();
                            return;
                        case InteractionType.bottle:
                            ExtraData = RandomNumber.Get(0, 7).ToString();
                            UpdateState();
                            return;
                        case InteractionType.hopper:
                        {
                            var flag = false;
                            var flag2 = false;
                            var num2 = 0;
                            if (InteractingUser > 0u)
                            {
                                var roomUser = GetRoom().GetRoomUserManager().GetRoomUserByHabbo(InteractingUser);
                                if (roomUser != null)
                                    if (roomUser.Coordinate == Coordinate)
                                    {
                                        roomUser.AllowOverride = false;
                                        if (roomUser.TeleDelay == 0)
                                        {
                                            var aHopper = HopperHandler.GetAHopper(roomUser.RoomId);
                                            var hopperId = HopperHandler.GetHopperId(aHopper);
                                            if (!roomUser.IsBot && roomUser.GetClient() != null &&
                                                roomUser.GetClient().GetHabbo() != null &&
                                                roomUser.GetClient().GetMessageHandler() != null)
                                            {
                                                roomUser.GetClient().GetHabbo().IsHopping = true;
                                                roomUser.GetClient().GetHabbo().HopperId = hopperId;
                                                var roomFwd =
                                                    new ServerMessage(
                                                        LibraryParser.OutgoingRequest("RoomForwardMessageComposer"));
                                                roomFwd.AppendInteger(aHopper);
                                                roomUser.GetClient().SendMessage(roomFwd);
                                                InteractingUser = 0u;
                                            }
                                        }
                                        else
                                        {
                                            roomUser.TeleDelay--;
                                            flag = true;
                                        }
                                    }
                                    else if (roomUser.Coordinate == SquareInFront)
                                    {
                                        roomUser.AllowOverride = true;
                                        flag2 = true;
                                        if (roomUser.IsWalking && (roomUser.GoalX != X || roomUser.GoalY != Y))
                                            roomUser.ClearMovement(true);
                                        roomUser.CanWalk = false;
                                        roomUser.AllowOverride = true;
                                        roomUser.MoveTo(Coordinate.X, Coordinate.Y, true);
                                    }
                                    else
                                        InteractingUser = 0u;
                                else
                                    InteractingUser = 0u;
                            }
                            if (InteractingUser2 > 0u)
                            {
                                var roomUserByHabbo = GetRoom()
                                    .GetRoomUserManager()
                                    .GetRoomUserByHabbo(InteractingUser2);
                                if (roomUserByHabbo != null)
                                {
                                    flag2 = true;
                                    roomUserByHabbo.UnlockWalking();
                                    roomUserByHabbo.MoveTo(SquareInFront);
                                }
                                InteractingUser2 = 0u;
                            }
                            if (flag2)
                            {
                                if (ExtraData != "1")
                                {
                                    ExtraData = "1";
                                    UpdateState(false, true);
                                }
                            }
                            else if (flag)
                            {
                                if (ExtraData != "2")
                                {
                                    ExtraData = "2";
                                    UpdateState(false, true);
                                }
                            }
                            else if (ExtraData != "0")
                                if (num2 == 0)
                                {
                                    ExtraData = "0";
                                    UpdateState(false, true);
                                }
                                else
                                    num2--;
                            ReqUpdate(1, false);
                            return;
                        }
                        case InteractionType.teleport:
                        {
                            var flag3 = false;
                            var flag4 = false;
                            if (InteractingUser > 0)
                            {
                                var roomUserByHabbo2 = GetRoom().GetRoomUserManager().GetRoomUserByHabbo(InteractingUser);
                                if (roomUserByHabbo2 == null)
                                {
                                    InteractingUser = 0u;
                                    return;
                                }
                                if (roomUserByHabbo2.Coordinate == Coordinate)
                                {
                                    roomUserByHabbo2.AllowOverride = false;
                                    if (TeleHandler.IsTeleLinked(Id, _mRoom))
                                    {
                                        flag4 = true;
                                        var linkedTele = TeleHandler.GetLinkedTele(Id, _mRoom);
                                        var teleRoomId = TeleHandler.GetTeleRoomId(linkedTele, _mRoom);
                                        if (teleRoomId == RoomId)
                                        {
                                            var item2 = GetRoom().GetRoomItemHandler().GetItem(linkedTele);
                                            if (item2 == null)
                                                roomUserByHabbo2.UnlockWalking();
                                            else
                                            {
                                                roomUserByHabbo2.SetPos(item2.X, item2.Y, item2.Z);
                                                roomUserByHabbo2.SetRot(item2.Rot, false);
                                                item2.ExtraData = "2";
                                                item2.UpdateState(false, true);
                                                item2.InteractingUser2 = InteractingUser;
                                            }
                                        }
                                        else if (!roomUserByHabbo2.IsBot && roomUserByHabbo2.GetClient() != null &&
                                                 roomUserByHabbo2.GetClient().GetHabbo() != null &&
                                                 roomUserByHabbo2.GetClient().GetMessageHandler() != null)
                                        {
                                            roomUserByHabbo2.GetClient().GetHabbo().IsTeleporting = true;
                                            roomUserByHabbo2.GetClient().GetHabbo().TeleportingRoomId = teleRoomId;
                                            roomUserByHabbo2.GetClient().GetHabbo().TeleporterId = linkedTele;
                                            roomUserByHabbo2.GetClient()
                                                .GetMessageHandler()
                                                .PrepareRoomForUser(teleRoomId, "");
                                        }
                                        InteractingUser = 0u;
                                    }
                                    else
                                    {
                                        roomUserByHabbo2.UnlockWalking();
                                        InteractingUser = 0u;
                                        roomUserByHabbo2.CanWalk = true;
                                        roomUserByHabbo2.TeleportEnabled = false;
                                        roomUserByHabbo2.MoveTo(SquareInFront);
                                    }
                                }
                                else if (roomUserByHabbo2.Coordinate == SquareInFront)
                                {
                                    
                                    flag3 = true;
                                    if (roomUserByHabbo2.IsWalking &&
                                        (roomUserByHabbo2.GoalX != X || roomUserByHabbo2.GoalY != Y))
                                        roomUserByHabbo2.ClearMovement(true);
                                    roomUserByHabbo2.SetRot(PathFinder.CalculateRotation(roomUserByHabbo2.X,
                                        roomUserByHabbo2.Y, X, Y));
                                    roomUserByHabbo2.UnlockWalking();
                                    roomUserByHabbo2.CanWalk = false;
                                    roomUserByHabbo2.TeleportEnabled = true;
                                    roomUserByHabbo2.MoveTo(X, Y, true);
                                }
                                else
                                    InteractingUser = 0u;
                            }
                            if (InteractingUser2 > 0u)
                            {
                                var roomUserByHabbo3 =
                                    GetRoom().GetRoomUserManager().GetRoomUserByHabbo(InteractingUser2);
                                if (roomUserByHabbo3 != null)
                                {
                                    flag3 = true;
                                    roomUserByHabbo3.UnlockWalking();
                                    roomUserByHabbo3.TeleportEnabled = false;
                                    roomUserByHabbo3.MoveTo(SquareInFront);
                                }
                                InteractingUser2 = 0u;
                            }
                            if (flag3)
                            {
                                if (ExtraData != "1")
                                {
                                    ExtraData = "1";
                                    UpdateState(false, true);
                                }
                            }
                            else if (flag4)
                            {
                                if (ExtraData != "2")
                                {
                                    ExtraData = "2";
                                    UpdateState(false, true);
                                }
                            }
                            else if (ExtraData != "0")
                            {
                                ExtraData = "0";
                                UpdateState(false, true);
                            }
                            ReqUpdate(1, false);
                            return;
                        }
                        default:
                            switch (interactionType)
                            {
                                case InteractionType.banzaifloor:
                                    if (Value == 3)
                                    {
                                        if (InteractionCountHelper == 1)
                                        {
                                            InteractionCountHelper = 0;
                                            switch (Team)
                                            {
                                                case Team.red:
                                                    ExtraData = "5";
                                                    break;
                                                case Team.green:
                                                    ExtraData = "8";
                                                    break;
                                                case Team.blue:
                                                    ExtraData = "11";
                                                    break;
                                                case Team.yellow:
                                                    ExtraData = "14";
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            ExtraData = "";
                                            InteractionCountHelper += 1;
                                        }
                                        UpdateState();
                                        InteractionCount += 1;
                                        if (InteractionCount < 16)
                                        {
                                            UpdateCounter = 1;
                                            return;
                                        }
                                        UpdateCounter = 0;
                                    }
                                    break;
                                case InteractionType.banzaiscoreblue:
                                case InteractionType.banzaiscorered:
                                case InteractionType.banzaiscoreyellow:
                                case InteractionType.banzaiscoregreen:
                                case InteractionType.banzaipyramid:
                                case InteractionType.freezeexit:
                                case InteractionType.freezeredcounter:
                                case InteractionType.freezebluecounter:
                                case InteractionType.freezeyellowcounter:
                                case InteractionType.freezegreencounter:
                                case InteractionType.freezeyellowgate:
                                case InteractionType.freezeredgate:
                                case InteractionType.freezegreengate:
                                case InteractionType.freezebluegate:
                                case InteractionType.freezetileblock:
                                case InteractionType.jukebox:
                                case InteractionType.musicdisc:
                                case InteractionType.puzzlebox:
                                case InteractionType.roombg:
                                case InteractionType.actionkickuser:
                                case InteractionType.actiongivereward:
                                case InteractionType.arrowplate:
                                    break;
                                case InteractionType.banzaicounter:
                                {
                                    if (string.IsNullOrEmpty(ExtraData))
                                        return;
                                    var num4 = 0;
                                    try
                                    {
                                        num4 = int.Parse(ExtraData);
                                    }
                                    catch
                                    {
                                    }
                                    if (num4 > 0)
                                    {
                                        if (InteractionCountHelper == 1)
                                        {
                                            num4--;
                                            InteractionCountHelper = 0;
                                            if (!GetRoom().GetBanzai().IsBanzaiActive)
                                                break;
                                            ExtraData = num4.ToString();
                                            UpdateState();
                                        }
                                        else
                                            InteractionCountHelper += 1;
                                        UpdateCounter = 1;
                                        return;
                                    }
                                    UpdateCounter = 0;
                                    GetRoom().GetBanzai().BanzaiEnd();
                                    return;
                                }
                                case InteractionType.banzaitele:
                                    ExtraData = string.Empty;
                                    UpdateState();
                                    return;

                                case InteractionType.banzaipuck:
                                    if (InteractionCount > 4)
                                    {
                                        InteractionCount += 1;
                                        UpdateCounter = 1;
                                        return;
                                    }
                                    InteractionCount = 0;
                                    UpdateCounter = 0;
                                    return;
                                case InteractionType.freezetimer:
                                {
                                    if (string.IsNullOrEmpty(ExtraData))
                                        return;
                                    var num5 = 0;
                                    try
                                    {
                                        num5 = int.Parse(ExtraData);
                                    }
                                    catch
                                    {
                                    }
                                    if (num5 > 0)
                                    {
                                        if (InteractionCountHelper == 1)
                                        {
                                            num5--;
                                            InteractionCountHelper = 0;
                                            if (!GetRoom().GetFreeze().GameStarted)
                                                break;
                                            ExtraData = num5.ToString();
                                            UpdateState();
                                        }
                                        else
                                            InteractionCountHelper += 1;
                                        UpdateCounter = 1;
                                        return;
                                    }
                                    UpdateNeeded = false;
                                    GetRoom().GetFreeze().StopGame();
                                    return;
                                }
                                case InteractionType.freezetile:
                                    if (InteractingUser > 0u)
                                    {
                                        ExtraData = "11000";
                                        UpdateState(false, true);
                                        GetRoom().GetFreeze().OnFreezeTiles(this, FreezePowerUp, InteractingUser);
                                        InteractingUser = 0u;
                                        InteractionCountHelper = 0;
                                    }
                                    break;
                                case InteractionType.wearitem:
                                {
                                    ExtraData = "1";
                                    UpdateState();
                                    var text = "";
                                    var clientByUserId =
                                        Azure.GetGame().GetClientManager().GetClientByUserId(InteractingUser);
                                    unchecked
                                    {
                                        if (!clientByUserId.GetHabbo().Look.Contains("ha"))
                                            text = string.Format("{0}.ha-1006-1326", clientByUserId.GetHabbo().Look);
                                        else
                                        {
                                            var array = clientByUserId.GetHabbo().Look.Split('.');
                                            var array2 = array;
                                            foreach (string text2 in array2)
                                            {
                                                var str = text2;
                                                if (text2.Contains("ha"))
                                                    str = "ha-1006-1326";
                                                text = string.Format("{0}{1}.", text, str);
                                            }
                                        }
                                        if (text.EndsWith("."))
                                            text = text.TrimEnd('.');
                                        clientByUserId.GetHabbo().Look = text;
                                        clientByUserId.GetMessageHandler()
                                            .GetResponse()
                                            .Init(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                                        clientByUserId.GetMessageHandler().GetResponse().AppendInteger(-1);
                                        clientByUserId.GetMessageHandler()
                                            .GetResponse()
                                            .AppendString(clientByUserId.GetHabbo().Look);
                                        clientByUserId.GetMessageHandler()
                                            .GetResponse()
                                            .AppendString(clientByUserId.GetHabbo().Gender.ToLower());
                                        clientByUserId.GetMessageHandler()
                                            .GetResponse()
                                            .AppendString(clientByUserId.GetHabbo().Motto);
                                        clientByUserId.GetMessageHandler()
                                            .GetResponse()
                                            .AppendInteger(clientByUserId.GetHabbo().AchievementPoints);
                                        clientByUserId.GetMessageHandler().SendResponse();
                                        var serverMessage = new ServerMessage();
                                        serverMessage.Init(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                                        serverMessage.AppendInteger(InteractingUser2);
                                        serverMessage.AppendString(clientByUserId.GetHabbo().Look);
                                        serverMessage.AppendString(clientByUserId.GetHabbo().Gender.ToLower());
                                        serverMessage.AppendString(clientByUserId.GetHabbo().Motto);
                                        serverMessage.AppendInteger(clientByUserId.GetHabbo().AchievementPoints);
                                        GetRoom().SendMessage(serverMessage);
                                        return;
                                    }
                                }
                                case InteractionType.triggertimer:
                                case InteractionType.triggerroomenter:
                                case InteractionType.triggergameend:
                                case InteractionType.triggergamestart:
                                case InteractionType.triggerrepeater:
                                case InteractionType.triggerlongrepeater:
                                case InteractionType.triggeronusersay:
                                case InteractionType.triggerscoreachieved:
                                case InteractionType.triggerstatechanged:
                                case InteractionType.triggerwalkonfurni:
                                case InteractionType.triggerwalkofffurni:
                                case InteractionType.actiongivescore:
                                case InteractionType.actionposreset:
                                case InteractionType.actionmoverotate:
                                case InteractionType.actionresettimer:
                                case InteractionType.actionshowmessage:
                                case InteractionType.actionteleportto:
                                case InteractionType.actiontogglestate:
                                case InteractionType.conditionfurnishaveusers:
                                case InteractionType.conditionstatepos:
                                case InteractionType.conditiontimelessthan:
                                case InteractionType.conditiontimemorethan:
                                case InteractionType.conditiontriggeronfurni:
                                case InteractionType.conditionfurnihasfurni:
                                case InteractionType.conditionitemsmatches:
                                case InteractionType.conditiongroupmember:
                                case InteractionType.conditionfurnitypematches:
                                case InteractionType.conditionhowmanyusersinroom:
                                case InteractionType.conditiontriggerernotonfurni:
                                case InteractionType.conditionfurnihasnotfurni:
                                case InteractionType.conditionfurnishavenotusers:
                                case InteractionType.conditionitemsdontmatch:
                                case InteractionType.conditionfurnitypedontmatch:
                                case InteractionType.conditionnotgroupmember:
                                case InteractionType.conditionuserwearingeffect:
                                case InteractionType.conditionuserwearingbadge:
                                case InteractionType.conditionusernotwearingeffect:
                                case InteractionType.conditionusernotwearingbadge:
                                case InteractionType.conditiondaterangeactive:
                                    ExtraData = "0";
                                    UpdateState(false, true);
                                    break;
                                case InteractionType.pressurepad:
                                    ExtraData = "1";
                                    UpdateState();
                                    return;
                                default:
                                    return;
                            }
                            break;
                    }
                else
                {
                    if (interactionType == InteractionType.gift)
                        return;
                    if (interactionType != InteractionType.vip_gate)
                        return;
                    RoomUser roomUser = null;
                    if (InteractingUser > 0u)
                        roomUser = GetRoom().GetRoomUserManager().GetRoomUserByHabbo(InteractingUser);
                    var num6 = 0;
                    var num7 = 0;
                    if (roomUser != null && roomUser.X == X && roomUser.Y == Y)
                    {
                        switch (roomUser.RotBody)
                        {
                            case 4:
                                num6 = 1;
                                break;
                            case 0:
                                num6 = -1;
                                break;
                            case 6:
                                num7 = -1;
                                break;
                            case 2:
                                num7 = 1;
                                break;
                        }
                        roomUser.MoveTo(roomUser.X + num7, roomUser.Y + num6);
                        ReqUpdate(1, false);
                    }
                    else if (roomUser != null &&
                             (roomUser.Coordinate == SquareBehind || roomUser.Coordinate == SquareInFront))
                    {
                        roomUser.UnlockWalking();
                        ExtraData = "0";
                        InteractingUser = 0u;
                        UpdateState(false, true);
                    }
                    else if (ExtraData == "1")
                    {
                        ExtraData = "0";
                        UpdateState(false, true);
                    }
                    if (roomUser == null)
                        InteractingUser = 0u;
                }
            }
        }

        internal void ReqUpdate(int cycles, bool setUpdate)
        {
            UpdateCounter = cycles;
            if (setUpdate)
                UpdateNeeded = true;
        }

        internal void UpdateState() { UpdateState(true, true); }

        internal void UpdateState(bool inDb, bool inRoom)
        {
            if (GetRoom() == null)
                return;
            var s = ExtraData;
            if (GetBaseItem().InteractionType == InteractionType.mystery_box)
            {
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery(string.Format("SELECT extra_data FROM items_rooms WHERE id={0} LIMIT 1", Id));
                    ExtraData = queryReactor.GetString();
                }
                if (ExtraData.Contains(Convert.ToChar(5).ToString()))
                {
                    var num = int.Parse(ExtraData.Split(Convert.ToChar(5))[0]);
                    var num2 = int.Parse(ExtraData.Split(Convert.ToChar(5))[1]);
                    s = checked(3 * num - num2).ToString();
                }
            }
            if (inDb)
                GetRoom().GetRoomItemHandler().UpdateItem(this);
            if (!inRoom)
                return;
            var serverMessage = new ServerMessage(0);
            if (IsFloorItem)
            {
                serverMessage.Init(LibraryParser.OutgoingRequest("UpdateFloorItemExtraDataMessageComposer"));
                serverMessage.AppendString(Id.ToString());
                switch (GetBaseItem().InteractionType)
                {
                    case InteractionType.mannequin:
                        serverMessage.AppendInteger(1);
                        serverMessage.AppendInteger(3);
                        if (ExtraData.Contains(Convert.ToChar(5).ToString()))
                        {
                            var array = ExtraData.Split(Convert.ToChar(5));
                            serverMessage.AppendString("GENDER");
                            serverMessage.AppendString(array[0]);
                            serverMessage.AppendString("FIGURE");
                            serverMessage.AppendString(array[1]);
                            serverMessage.AppendString("OUTFIT_NAME");
                            serverMessage.AppendString(array[2]);
                        }
                        else
                        {
                            serverMessage.AppendString("GENDER");
                            serverMessage.AppendString("");
                            serverMessage.AppendString("FIGURE");
                            serverMessage.AppendString("");
                            serverMessage.AppendString("OUTFIT_NAME");
                            serverMessage.AppendString("");
                        }
                        break;
                    case InteractionType.pinata:
                        serverMessage.AppendInteger(7);
                        if (ExtraData.Length <= 0)
                        {
                            serverMessage.AppendString("6");
                            serverMessage.AppendInteger(0);
                            serverMessage.AppendInteger(100);
                        }
                        else
                        {
                            serverMessage.AppendString((int.Parse(ExtraData) == 100) ? "8" : "6");
                            serverMessage.AppendInteger(int.Parse(ExtraData));
                            serverMessage.AppendInteger(100);
                        }
                        break;
                    default:
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendString(s);
                        break;
                }
            }
            else
            {
                serverMessage.Init(LibraryParser.OutgoingRequest("UpdateRoomWallItemMessageComposer"));
                Serialize(serverMessage);
            }
            GetRoom().SendMessage(serverMessage);
        }

        internal void Serialize(ServerMessage message)
        {
            checked
            {
                if (IsFloorItem)
                {
                    message.AppendInteger(Id);
                    message.AppendInteger(GetBaseItem().SpriteId);
                    message.AppendInteger(X);
                    message.AppendInteger(Y);
                    message.AppendInteger(Rot);
                    message.AppendString(TextHandling.GetString(Z));
                    message.AppendString(TextHandling.GetString(GetBaseItem().Height));

                    switch (GetBaseItem().InteractionType)
                    {
                        case InteractionType.gld_gate:
                        case InteractionType.gld_item:
                        case InteractionType.groupforumterminal:
                        {
                            var group2 = Azure.GetGame().GetGroupManager().GetGroup(GroupId);
                            if (group2 == null)
                            {
                                message.AppendInteger(1);
                                message.AppendInteger(0);
                                message.AppendString(ExtraData);
                            }
                            else
                            {
                                message.AppendInteger(0);
                                message.AppendInteger(2);
                                message.AppendInteger(5);
                                message.AppendString(ExtraData);
                                message.AppendString(GroupId.ToString());
                                message.AppendString(group2.Badge);
                                message.AppendString(Azure.GetGame()
                                    .GetGroupManager()
                                    .GetGroupColour(group2.Colour1, true));
                                message.AppendString(Azure.GetGame()
                                    .GetGroupManager()
                                    .GetGroupColour(group2.Colour2, false));
                            }
                        }
                            break;
                        case InteractionType.youtubetv:
                            message.AppendInteger(0);
                            if (!Azure.GetGame().GetVideoManager().TVExists(Id))
                            {
                                message.AppendInteger(0);
                                message.AppendString("");
                            }
                            else
                            {
                                message.AppendInteger(1);
                                message.AppendInteger(1);
                                message.AppendString("THUMBNAIL_URL");
                                message.AppendString(string.Format("{0}{1}", ExtraSettings.YOUTUBE_THUMBNAIL_SUBURL,
                                    ExtraData));
                            }
                            break;
                        case InteractionType.musicdisc:
                            message.AppendInteger(SongManager.GetSongId(SongCode));
                            message.AppendInteger(0);
                            message.AppendString(ExtraData);
                            break;
                        case InteractionType.background:
                            message.AppendInteger(0);
                            message.AppendInteger(1);
                            if (ExtraData != "")
                            {
                                message.AppendInteger(ExtraData.Split(Convert.ToChar(9)).Length / 2);
                                for (var i = 0; i <= ExtraData.Split(Convert.ToChar(9)).Length - 1; i++)
                                    message.AppendString(ExtraData.Split(Convert.ToChar(9))[i]);
                            }
                            else
                                message.AppendInteger(0);
                            break;
                        case InteractionType.gift:
                        {
                            var split = ExtraData.Split((char) 9);
                            var giftMessage = "";
                            var giftRibbon = 1;
                            var giftColor = 2;
                            var showGiver = false;
                            var giverName = "";
                            var giverLook = "";
                            var product = "A1 PIZ";
                            try
                            {
                                giftMessage = split[1];
                                giftRibbon = int.Parse(split[2]);
                                giftColor = int.Parse(split[3]);
                                showGiver = Azure.EnumToBool(split[4]);
                                giverName = split[5];
                                giverLook = split[6];
                                product = split[7];
                            }
                            catch
                            {
                            }
                            var ribbonAndColor = (giftRibbon * 1000) + giftColor;
                            message.AppendInteger(ribbonAndColor);
                            message.AppendInteger(1);
                            message.AppendInteger((showGiver) ? 6 : 4);
                            message.AppendString("EXTRA_PARAM");
                            message.AppendString("");
                            message.AppendString("MESSAGE");
                            message.AppendString(giftMessage);
                            if (showGiver)
                            {
                                message.AppendString("PURCHASER_NAME");
                                message.AppendString(giverName);
                                message.AppendString("PURCHASER_FIGURE");
                                message.AppendString(giverLook);
                            }
                            message.AppendString("PRODUCT_CODE");
                            message.AppendString(product);
                            message.AppendString("state");
                            message.AppendString(MagicRemove ? "1" : "0");
                        }
                            break;
                        case InteractionType.pinata:
                            message.AppendInteger(0);
                            message.AppendInteger(7);
                            message.AppendString((ExtraData == "100") ? "8" : "6");
                            if (ExtraData.Length <= 0)
                            {
                                message.AppendInteger(0);
                                message.AppendInteger(100);
                            }
                            else
                            {
                                message.AppendInteger(int.Parse(ExtraData));
                                message.AppendInteger(100);
                            }
                            break;
                        case InteractionType.mannequin:
                            message.AppendInteger(0);
                            message.AppendInteger(1);
                            message.AppendInteger(3);
                            if (ExtraData.Contains(Convert.ToChar(5).ToString()))
                            {
                                var array = ExtraData.Split((char) 5);
                                message.AppendString("GENDER");
                                message.AppendString(array[0]);
                                message.AppendString("FIGURE");
                                message.AppendString(array[1]);
                                message.AppendString("OUTFIT_NAME");
                                message.AppendString(array[2]);
                            }
                            else
                            {
                                message.AppendString("GENDER");
                                message.AppendString("");
                                message.AppendString("FIGURE");
                                message.AppendString("");
                                message.AppendString("OUTFIT_NAME");
                                message.AppendString("");
                            }
                            break;
                        case InteractionType.badge_display:
                            message.AppendInteger(0);
                            message.AppendInteger(2);
                            message.AppendInteger(4);
                            message.AppendString("0");
                            message.AppendString(ExtraData);
                            message.AppendString("");
                            message.AppendString("");
                            break;
                        case InteractionType.lovelock:
                        {
                            var data = ExtraData.Split((char) 5);
                            message.AppendInteger(0);
                            message.AppendInteger(2);
                            message.AppendInteger(data.Length);
                            foreach (string datak in data)
                                message.AppendString(datak);
                        }
                            break;
                        case InteractionType.moplaseed:
                            message.AppendInteger(0);
                            message.AppendInteger(1);
                            message.AppendInteger(1);
                            message.AppendString("rarity");
                            message.AppendString(ExtraData);
                            break;
                        case InteractionType.roombg:
                            if (_mRoom.TonerData == null)
                                _mRoom.TonerData = new TonerData(Id);
                            _mRoom.TonerData.GenerateExtraData(message);
                            break;
                        case InteractionType.mystery_box:
                            message.AppendInteger(0);
                            message.AppendInteger(0);
                            if (ExtraData.Contains(Convert.ToChar(5).ToString()))
                            {
                                var num3 = int.Parse(ExtraData.Split((char) 5)[0]);
                                var num4 = int.Parse(ExtraData.Split((char) 5)[1]);
                                message.AppendString((3 * num3 - num4).ToString());
                            }
                            else
                            {
                                ExtraData = string.Format("0{0}0", Convert.ToChar(5));
                                message.AppendString("0");
                            }
                            break;
                        default:
                            if (LimitedNo > 0)
                            {
                                message.AppendInteger(1);
                                message.AppendInteger(256);
                                message.AppendString(ExtraData);
                                message.AppendInteger(LimitedNo);
                                message.AppendInteger(LimitedTot);
                            }
                            else if (GetBaseItem().IsGroupItem)
                            {
                                message.AppendInteger(0);
                                try
                                {
                                    var group =
                                        Azure.GetGame().GetGroupManager().GetGroup(uint.Parse(GroupData.Split(';')[1]));
                                    if (group != null)
                                    {
                                        message.AppendInteger(2);
                                        message.AppendInteger(5);
                                        message.AppendString(ExtraData);
                                        message.AppendString(group.Id.ToString());
                                        message.AppendString(group.Badge);
                                        message.AppendString(GroupData.Split(';')[2]);
                                        message.AppendString(GroupData.Split(';')[3]);
                                    }
                                }
                                catch
                                {
                                    message.AppendInteger(0);
                                }
                            }
                            else if (GetBaseItem().Name.StartsWith("easter13_egg_"))
                            {
                                message.AppendInteger(0);
                                message.AppendInteger(7);
                                message.AppendString("0");//state 
                                message.AppendInteger(0);//actual
                                message.AppendInteger(0);//max
                            }
                            else
                            {
                                message.AppendInteger((GetBaseItem().InteractionType == InteractionType.tilestackmagic)
                                    ? 0
                                    : 1);
                                message.AppendInteger(0);
                                message.AppendString(ExtraData);
                            }
                            break;
                    }
                    message.AppendInteger(-1);
                    message.AppendInteger((GetBaseItem().InteractionType == InteractionType.mystery_box ||
                                           GetBaseItem().InteractionType == InteractionType.youtubetv ||
                                           GetBaseItem().InteractionType == InteractionType.background)
                        ? 2
                        : ((GetBaseItem().InteractionType == InteractionType.moplaseed || GetBaseItem().Modes > 1)
                            ? 1
                            : 0));
                    message.AppendInteger(IsBuilder ? -12345678 : Convert.ToInt32(UserId));//-12345678 for bc
                    return;
                }
                if (!IsWallItem)
                    return;
                message.AppendString(string.Format("{0}{1}", Id, string.Empty));
                message.AppendInteger(GetBaseItem().SpriteId);
                message.AppendString(WallCoord.ToString());
                var interactionType = GetBaseItem().InteractionType;
                if (interactionType == InteractionType.postit)
                    message.AppendString(ExtraData.Split(new[]
                    {
                        ' '
                    })[0]);
                else
                    message.AppendString(ExtraData);
                message.AppendInteger(-1);
                message.AppendInteger(GetBaseItem().Modes > 1 ? 1 : 0);
                message.AppendInteger(UserId);
            }
        }

        internal void RefreshItem() { _mBaseItem = null; }

        internal Item GetBaseItem()
        {
            return _mBaseItem ?? (_mBaseItem = Azure.GetGame().GetItemManager().GetItem(BaseItem));
        }

        internal Room GetRoom() { return _mRoom ?? (_mRoom = Azure.GetGame().GetRoomManager().GetRoom(RoomId)); }

        internal void UserWalksOnFurni(RoomUser user)
        {
            if (OnUserWalksOnFurni != null)
                OnUserWalksOnFurni(this, new UserWalksOnArgs(user));
            GetRoom().GetWiredHandler().ExecuteWired(WiredItemType.TriggerWalksOnFurni, new object[]
            {
                user,
                this
            });
            user.LastItem = Id;
        }

        internal void UserWalksOffFurni(RoomUser user)
        {
            if (OnUserWalksOffFurni != null)
                OnUserWalksOffFurni(this, new UserWalksOnArgs(user));
            GetRoom().GetWiredHandler().ExecuteWired(WiredItemType.TriggerWalksOffFurni, new object[]
            {
                user,
                this
            });
        }
    }
}