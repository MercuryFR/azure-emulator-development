using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Azure.HabboHotel.Items
{
    class MoodlightData
    {
        internal bool Enabled;
        internal int CurrentPreset;
        internal List<MoodlightPreset> Presets;
        internal uint ItemId;

        internal MoodlightData(uint itemId)
        {
            ItemId = itemId;
            DataRow row;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    string.Format(
                        "SELECT enabled,current_preset,preset_one,preset_two,preset_three FROM items_moodlight WHERE item_id = {0}",
                        itemId));
                row = queryReactor.GetRow();
            }
            if (row == null)
                throw new NullReferenceException("No moodlightdata found in the database");
            Enabled = Azure.EnumToBool(row["enabled"].ToString());
            CurrentPreset = (int) row["current_preset"];

            Presets = new List<MoodlightPreset>
            {
                GeneratePreset((string) row["preset_one"]),
                GeneratePreset((string) row["preset_two"]),
                GeneratePreset((string) row["preset_three"])
            };
        }

        internal static MoodlightPreset GeneratePreset(string data)
        {
            var array = data.Split(',');
            if (!IsValidColor(array[0]))
                array[0] = "#000000";

            return new MoodlightPreset(array[0], int.Parse(array[1]), Azure.EnumToBool(array[2]));
        }

        internal static bool IsValidColor(string colorCode)
        {
            switch (colorCode)
            {
                case "#000000":
                case "#0053F7":
                case "#EA4532":
                case "#82F349":
                case "#74F5F5":
                case "#E759DE":
                case "#F2F851":
                    return true;
            }
            return false;
        }

        internal static bool IsValidIntensity(int intensity) { return intensity >= 0 && intensity <= 255; }

        internal void Enable()
        {
            Enabled = true;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Format("UPDATE items_moodlight SET enabled = '1' WHERE item_id = {0}",
                    ItemId));
        }

        internal void Disable()
        {
            Enabled = false;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Format("UPDATE items_moodlight SET enabled = '0' WHERE item_id = {0}",
                    ItemId));
        }

        internal void UpdatePreset(int preset, string color, int intensity, bool bgOnly, bool hax = false)
        {
            if (!IsValidColor(color) || (!IsValidIntensity(intensity) && !hax))
                return;
            string text;
            switch (preset)
            {
                case 2:
                    text = "two";
                    goto IL_43;
                case 3:
                    text = "three";
                    goto IL_43;
            }
            text = "one";
            IL_43:
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Concat(new object[]
                {
                    "UPDATE items_moodlight SET preset_",
                    text,
                    " = '@color,",
                    intensity,
                    ",",
                    Azure.BoolToEnum(bgOnly),
                    "' WHERE item_id = ",
                    ItemId
                }));
                queryReactor.AddParameter("color", color);
                queryReactor.RunQuery();
            }
            GetPreset(preset).ColorCode = color;
            GetPreset(preset).ColorIntensity = intensity;
            GetPreset(preset).BackgroundOnly = bgOnly;
        }

        internal MoodlightPreset GetPreset(int i)
        {
            checked
            {
                i--;
                return Presets[i] ?? new MoodlightPreset("#000000", 255, false);
            }
        }

        internal string GenerateExtraData()
        {
            var preset = GetPreset(CurrentPreset);
            var stringBuilder = new StringBuilder();
            stringBuilder.Append(Enabled ? 2 : 1);
            stringBuilder.Append(",");
            stringBuilder.Append(CurrentPreset);
            stringBuilder.Append(",");
            stringBuilder.Append(preset.BackgroundOnly ? 2 : 1);
            stringBuilder.Append(",");
            stringBuilder.Append(preset.ColorCode);
            stringBuilder.Append(",");
            stringBuilder.Append(preset.ColorIntensity);
            return stringBuilder.ToString();
        }
    }
}