using System;
using System.Collections.Generic;
using System.Data;

namespace Azure.HabboHotel.Items
{
    class PinataItem
    {
        internal uint ItemBaseId;
        internal List<uint> Rewards;

        internal PinataItem(DataRow row)
        {
            Rewards = new List<uint>();
            ItemBaseId = Convert.ToUInt32(row["item_baseid"]);
            var text = (string) row["rewards"];
            var array = text.Split(';');
            foreach (var value in array)
                Rewards.Add(Convert.ToUInt32(value));
        }
    }
}