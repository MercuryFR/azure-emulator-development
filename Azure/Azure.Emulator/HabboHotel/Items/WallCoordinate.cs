﻿using Azure.Util;

namespace Azure.HabboHotel.Items
{
    internal class WallCoordinate
    {
        private readonly int _widthX;
        private readonly int _widthY;

        private readonly int _lengthX;
        private readonly int _lengthY;

        private readonly char _side;

        public WallCoordinate(string wallPosition)
        {
            var posD = wallPosition.Split(' ');
            _side = posD[2] == "l" ? 'l' : 'r';
            var widD = posD[0].Substring(3).Split(',');
            _widthX = TextHandling.Parse(widD[0]);
            _widthY = TextHandling.Parse(widD[1]);
            var lenD = posD[1].Substring(2).Split(',');
            _lengthX = TextHandling.Parse(lenD[0]);
            _lengthY = TextHandling.Parse(lenD[1]);
        }

        public WallCoordinate(double x, double y, sbyte n)
        {
            TextHandling.Split(x, out _widthX, out _widthY);
            TextHandling.Split(y, out _lengthX, out _lengthY);
            _side = n == 7 ? 'r' : 'l';
        }

        public override string ToString()
        {
            return ":w=" + _widthX + "," + _widthY + " " + "l=" + _lengthX + "," + _lengthY + " " + _side;
        }

        internal string GenerateDbShit()
        {
            return "x: " + TextHandling.Combine(_widthX, _widthY) + " y: " + TextHandling.Combine(_lengthX, _lengthY);
        }

        internal double GetXValue() { return TextHandling.Combine(_widthX, _widthY); }

        internal double GetYValue() { return TextHandling.Combine(_lengthX, _lengthY); }

        internal int N() { return _side == 'l' ? 8 : 7; }
    }
}