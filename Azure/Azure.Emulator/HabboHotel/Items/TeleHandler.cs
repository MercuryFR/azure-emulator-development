using System;
using Azure.HabboHotel.Rooms;

namespace Azure.HabboHotel.Items
{
    static class TeleHandler
    {
        internal static uint GetLinkedTele(uint teleId, Room pRoom)
        {
            uint result;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT tele_two_id FROM items_teleports WHERE tele_one_id = {0}",
                    teleId));
                var row = queryReactor.GetRow();
                result = row == null ? 0u : Convert.ToUInt32(row[0]);
            }
            return result;
        }

        internal static uint GetTeleRoomId(uint teleId, Room pRoom)
        {
            if (pRoom.GetRoomItemHandler().GetItem(teleId) != null)
                return pRoom.RoomId;
            uint result;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT room_id FROM items_rooms WHERE id = {0} LIMIT 1", teleId));
                var row = queryReactor.GetRow();
                result = row == null ? 0u : Convert.ToUInt32(row[0]);
            }
            return result;
        }

        internal static bool IsTeleLinked(uint teleId, Room pRoom)
        {
            var linkedTele = GetLinkedTele(teleId, pRoom);
            if (linkedTele == 0u)
                return false;
            var item = pRoom.GetRoomItemHandler().GetItem(linkedTele);
            return (item != null && item.GetBaseItem().InteractionType == InteractionType.teleport) ||
                   GetTeleRoomId(linkedTele, pRoom) != 0u;
        }
    }
}