namespace Azure.HabboHotel.Items
{
    class MoodlightPreset
    {
        internal string ColorCode;
        internal int ColorIntensity;
        internal bool BackgroundOnly;

        internal MoodlightPreset(string colorCode, int colorIntensity, bool backgroundOnly)
        {
            ColorCode = colorCode;
            ColorIntensity = colorIntensity;
            BackgroundOnly = backgroundOnly;
        }
    }
}