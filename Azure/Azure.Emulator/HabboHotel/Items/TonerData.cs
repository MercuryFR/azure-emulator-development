using System.Data;
using Azure.Messages;

namespace Azure.HabboHotel.Items
{
    class TonerData
    {
        internal int Enabled;
        internal uint ItemId;
        internal int Data1, Data2, Data3;

        internal TonerData(uint item)
        {
            ItemId = item;
            DataRow row;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    string.Format("SELECT enabled,data1,data2,data3 FROM items_toners WHERE id={0} LIMIT 1", ItemId));
                row = queryReactor.GetRow();
            }
            if (row == null)
            {
                Data1 = Data2 = Data3 = 1;
                return;
            }
            Enabled = int.Parse(row[0].ToString());
            Data1 = (int) row[1];
            Data2 = (int) row[2];
            Data3 = (int) row[3];
        }

        internal ServerMessage GenerateExtraData(ServerMessage message)
        {
            message.AppendInteger(0);
            message.AppendInteger(5);
            message.AppendInteger(4);
            message.AppendInteger(Enabled);
            message.AppendInteger(Data1);
            message.AppendInteger(Data2);
            message.AppendInteger(Data3);
            return message;
        }
    }
}