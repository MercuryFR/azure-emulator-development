using System;
using System.Collections.Generic;
using System.Data;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Rooms;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Items.Interactor
{
    internal class InteractorWired : IFurniInteractor
    {
        public void OnPlace(GameClient session, RoomItem item)
        {
        }

        public void OnRemove(GameClient session, RoomItem item)
        {
            Room room = item.GetRoom();
            room.GetWiredHandler().RemoveWired(item);
        }

        public void OnTrigger(GameClient session, RoomItem item, int request, bool hasRights)
        {
            if (session == null || item == null || !hasRights)
            {
                return;
            }

            string extraInfo = "";
            bool flag = false;
            int i = 0;
            var list = new List<RoomItem>();
            string extraString = "";
            string extraString2 = "";
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT * FROM items_wireds WHERE id=@id LIMIT 1");
                queryReactor.AddParameter("id", item.Id);
                DataRow row = queryReactor.GetRow();
                if (row != null)
                {
                    extraInfo = row["string"].ToString();
                    i = (int)row["delay"] / 500;
                    flag = (row["bool"].ToString() == "1");
                    extraString = row["extra_string"].ToString();
                    extraString2 = row["extra_string_2"].ToString();
                    string[] array = row["items"].ToString().Split(';');
                    foreach (string s in array)
                    {
                        uint pId;
                        if (!uint.TryParse(s, out pId))
                        {
                            continue;
                        }
                        RoomItem item2 = item.GetRoom().GetRoomItemHandler().GetItem(pId);
                        if (item2 != null && !list.Contains(item2))
                        {
                            list.Add(item2);
                        }
                    }
                }
            }
            switch (item.GetBaseItem().InteractionType)
            {
                case InteractionType.triggertimer:
                    {
                        var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("WiredTriggerMessageComposer"));
                        serverMessage.AppendBool(false);
                        serverMessage.AppendInteger(5);
                        serverMessage.AppendInteger(list.Count);
                        foreach (RoomItem current in list)
                        {
                            serverMessage.AppendInteger(current.Id);
                        }
                        serverMessage.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage.AppendInteger(item.Id);
                        serverMessage.AppendString(extraInfo);
                        serverMessage.AppendInteger(1);
                        serverMessage.AppendInteger(1);
                        serverMessage.AppendInteger(1);
                        serverMessage.AppendInteger(3);
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendInteger(0);
                        session.SendMessage(serverMessage);
                        return;
                    }
                case InteractionType.triggerroomenter:
                    {
                        var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("WiredTriggerMessageComposer"));
                        serverMessage2.AppendBool(false);
                        serverMessage2.AppendInteger(0);
                        serverMessage2.AppendInteger(list.Count);
                        foreach (RoomItem current2 in list)
                        {
                            serverMessage2.AppendInteger(current2.Id);
                        }
                        serverMessage2.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage2.AppendInteger(item.Id);
                        serverMessage2.AppendString(extraInfo);
                        serverMessage2.AppendInteger(0);
                        serverMessage2.AppendInteger(0);
                        serverMessage2.AppendInteger(7);
                        serverMessage2.AppendInteger(0);
                        serverMessage2.AppendInteger(0);
                        serverMessage2.AppendInteger(0);
                        session.SendMessage(serverMessage2);
                        return;
                    }
                case InteractionType.triggergameend:
                    {
                        var serverMessage3 = new ServerMessage(LibraryParser.OutgoingRequest("WiredTriggerMessageComposer"));
                        serverMessage3.AppendBool(false);
                        serverMessage3.AppendInteger(0);
                        serverMessage3.AppendInteger(list.Count);
                        foreach (RoomItem current3 in list)
                        {
                            serverMessage3.AppendInteger(current3.Id);
                        }
                        serverMessage3.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage3.AppendInteger(item.Id);
                        serverMessage3.AppendString(extraInfo);
                        serverMessage3.AppendInteger(0);
                        serverMessage3.AppendInteger(0);
                        serverMessage3.AppendInteger(8);
                        serverMessage3.AppendInteger(0);
                        serverMessage3.AppendInteger(0);
                        serverMessage3.AppendInteger(0);
                        session.SendMessage(serverMessage3);
                        return;
                    }
                case InteractionType.triggergamestart:
                    {
                        var serverMessage4 = new ServerMessage(LibraryParser.OutgoingRequest("WiredTriggerMessageComposer"));
                        serverMessage4.AppendBool(false);
                        serverMessage4.AppendInteger(0);
                        serverMessage4.AppendInteger(list.Count);
                        foreach (RoomItem current4 in list)
                        {
                            serverMessage4.AppendInteger(current4.Id);
                        }
                        serverMessage4.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage4.AppendInteger(item.Id);
                        serverMessage4.AppendString(extraInfo);
                        serverMessage4.AppendInteger(0);
                        serverMessage4.AppendInteger(0);
                        serverMessage4.AppendInteger(8);
                        serverMessage4.AppendInteger(0);
                        serverMessage4.AppendInteger(0);
                        serverMessage4.AppendInteger(0);
                        session.SendMessage(serverMessage4);
                        return;
                    }
                case InteractionType.triggerlongrepeater:
                    {
                        var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("WiredTriggerMessageComposer"));
                        serverMessage.AppendBool(false);
                        serverMessage.AppendInteger(5);
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage.AppendInteger(item.Id);
                        serverMessage.AppendString("");
                        serverMessage.AppendInteger(1);
                        serverMessage.AppendInteger(i / 10); //fix
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendInteger(12);
                        serverMessage.AppendInteger(0);
                        session.SendMessage(serverMessage);
                        return;
                    }

                case InteractionType.triggerrepeater:
                    {
                        var serverMessage5 = new ServerMessage(LibraryParser.OutgoingRequest("WiredTriggerMessageComposer"));
                        serverMessage5.AppendBool(false);
                        serverMessage5.AppendInteger(5);
                        serverMessage5.AppendInteger(list.Count);
                        foreach (RoomItem current5 in list)
                        {
                            serverMessage5.AppendInteger(current5.Id);
                        }
                        serverMessage5.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage5.AppendInteger(item.Id);
                        serverMessage5.AppendString(extraInfo);
                        serverMessage5.AppendInteger(1);
                        serverMessage5.AppendInteger(i);
                        serverMessage5.AppendInteger(0);
                        serverMessage5.AppendInteger(6);
                        serverMessage5.AppendInteger(0);
                        serverMessage5.AppendInteger(0);
                        session.SendMessage(serverMessage5);
                        return;
                    }
                case InteractionType.triggeronusersay:
                    {
                        var serverMessage6 = new ServerMessage(LibraryParser.OutgoingRequest("WiredTriggerMessageComposer"));
                        serverMessage6.AppendBool(false);
                        serverMessage6.AppendInteger(0);
                        serverMessage6.AppendInteger(list.Count);
                        foreach (RoomItem current6 in list)
                        {
                            serverMessage6.AppendInteger(current6.Id);
                        }
                        serverMessage6.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage6.AppendInteger(item.Id);
                        serverMessage6.AppendString(extraInfo);
                        serverMessage6.AppendInteger(0);
                        serverMessage6.AppendInteger(0);
                        serverMessage6.AppendInteger(0);
                        serverMessage6.AppendInteger(0);
                        serverMessage6.AppendInteger(0);
                        serverMessage6.AppendInteger(0);
                        session.SendMessage(serverMessage6);
                        return;
                    }

                case InteractionType.triggerscoreachieved:
                    {
                        var serverMessage7 = new ServerMessage(LibraryParser.OutgoingRequest("WiredTriggerMessageComposer"));
                        serverMessage7.AppendBool(false);
                        serverMessage7.AppendInteger(5);
                        serverMessage7.AppendInteger(0);
                        serverMessage7.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage7.AppendInteger(item.Id);
                        serverMessage7.AppendString("");
                        serverMessage7.AppendInteger(1);
                        serverMessage7.AppendInteger((String.IsNullOrWhiteSpace(extraInfo)) ? 100 : int.Parse(extraInfo));
                        serverMessage7.AppendInteger(0);
                        serverMessage7.AppendInteger(10);
                        serverMessage7.AppendInteger(0);
                        serverMessage7.AppendInteger(0);
                        session.SendMessage(serverMessage7);
                        return;
                    }
                case InteractionType.triggerstatechanged:
                    {
                        var serverMessage8 = new ServerMessage(LibraryParser.OutgoingRequest("WiredTriggerMessageComposer"));
                        serverMessage8.AppendBool(false);
                        serverMessage8.AppendInteger(5);
                        serverMessage8.AppendInteger(list.Count);
                        foreach (RoomItem current8 in list)
                        {
                            serverMessage8.AppendInteger(current8.Id);
                        }
                        serverMessage8.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage8.AppendInteger(item.Id);
                        serverMessage8.AppendString(extraInfo);
                        serverMessage8.AppendInteger(0);
                        serverMessage8.AppendInteger(0);
                        serverMessage8.AppendInteger(1);
                        serverMessage8.AppendInteger(i);
                        serverMessage8.AppendInteger(0);
                        serverMessage8.AppendInteger(0);
                        session.SendMessage(serverMessage8);
                        return;
                    }
                case InteractionType.triggerwalkonfurni:
                    {
                        var serverMessage9 = new ServerMessage(LibraryParser.OutgoingRequest("WiredTriggerMessageComposer"));
                        serverMessage9.AppendBool(false);
                        serverMessage9.AppendInteger(5);
                        serverMessage9.AppendInteger(list.Count);
                        foreach (RoomItem current9 in list)
                        {
                            serverMessage9.AppendInteger(current9.Id);
                        }
                        serverMessage9.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage9.AppendInteger(item.Id);
                        serverMessage9.AppendString(extraInfo);
                        serverMessage9.AppendInteger(0);
                        serverMessage9.AppendInteger(0);
                        serverMessage9.AppendInteger(1);
                        serverMessage9.AppendInteger(0);
                        serverMessage9.AppendInteger(0);
                        serverMessage9.AppendInteger(0);
                        session.SendMessage(serverMessage9);
                        return;
                    }
                case InteractionType.actionmuteuser:
                    {
                        var serverMessage18 = new ServerMessage(LibraryParser.OutgoingRequest("WiredEffectMessageComposer"));
                        serverMessage18.AppendBool(false);
                        serverMessage18.AppendInteger(5);
                        serverMessage18.AppendInteger(0);
                        serverMessage18.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage18.AppendInteger(item.Id);
                        serverMessage18.AppendString(extraInfo);
                        serverMessage18.AppendInteger(1);
                        serverMessage18.AppendInteger(i);
                        serverMessage18.AppendInteger(0);
                        serverMessage18.AppendInteger(20);
                        serverMessage18.AppendInteger(0);
                        serverMessage18.AppendInteger(0);
                        session.SendMessage(serverMessage18);
                        return;
                    }
                case InteractionType.triggerwalkofffurni:
                    {
                        var serverMessage10 = new ServerMessage(LibraryParser.OutgoingRequest("WiredTriggerMessageComposer"));
                        serverMessage10.AppendBool(false);
                        serverMessage10.AppendInteger(5);
                        serverMessage10.AppendInteger(list.Count);
                        foreach (RoomItem current10 in list)
                        {
                            serverMessage10.AppendInteger(current10.Id);
                        }
                        serverMessage10.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage10.AppendInteger(item.Id);
                        serverMessage10.AppendString(extraInfo);
                        serverMessage10.AppendInteger(0);
                        serverMessage10.AppendInteger(0);
                        serverMessage10.AppendInteger(1);
                        serverMessage10.AppendInteger(0);
                        serverMessage10.AppendInteger(0);
                        serverMessage10.AppendInteger(0);
                        serverMessage10.AppendInteger(0);
                        session.SendMessage(serverMessage10);
                        return;
                    }

                case InteractionType.actiongivescore:
                    {
                        // Por hacer.
                        var serverMessage11 = new ServerMessage(LibraryParser.OutgoingRequest("WiredEffectMessageComposer"));
                        serverMessage11.AppendBool(false);
                        serverMessage11.AppendInteger(5);
                        serverMessage11.AppendInteger(0);
                        serverMessage11.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage11.AppendInteger(item.Id);
                        serverMessage11.AppendString("");
                        serverMessage11.AppendInteger(2);
                        if (String.IsNullOrWhiteSpace(extraInfo))
                        {
                            serverMessage11.AppendInteger(10); // Puntos a dar
                            serverMessage11.AppendInteger(1); // Numero de veces por equipo
                        }
                        else
                        {
                            string[] integers = extraInfo.Split(',');
                            serverMessage11.AppendInteger(int.Parse(integers[0])); // Puntos a dar
                            serverMessage11.AppendInteger(int.Parse(integers[1])); // Numero de veces por equipo
                        }
                        serverMessage11.AppendInteger(0);
                        serverMessage11.AppendInteger(6);
                        serverMessage11.AppendInteger(0);
                        serverMessage11.AppendInteger(0);
                        serverMessage11.AppendInteger(0);
                        session.SendMessage(serverMessage11);
                        return;
                    }

                case InteractionType.conditiongroupmember:
                case InteractionType.conditionnotgroupmember:
                    {
                        var message = new ServerMessage(LibraryParser.OutgoingRequest("WiredConditionMessageComposer"));
                        message.AppendBool(false);
                        message.AppendInteger(5);
                        message.AppendInteger(0);
                        message.AppendInteger(item.GetBaseItem().SpriteId);
                        message.AppendInteger(item.Id);
                        message.AppendString("");
                        message.AppendInteger(0);
                        message.AppendInteger(0);
                        message.AppendInteger(10);
                        session.SendMessage(message);
                        return;
                    }

                case InteractionType.conditionitemsmatches:
                case InteractionType.conditionitemsdontmatch:
                    {
                        var serverMessage21 = new ServerMessage(LibraryParser.OutgoingRequest("WiredConditionMessageComposer"));
                        serverMessage21.AppendBool(false);
                        serverMessage21.AppendInteger(5);
                        serverMessage21.AppendInteger(list.Count);
                        foreach (RoomItem current20 in list)
                        {
                            serverMessage21.AppendInteger(current20.Id);
                        }
                        serverMessage21.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage21.AppendInteger(item.Id);
                        serverMessage21.AppendString(extraString2);
                        serverMessage21.AppendInteger(3);

                        if (String.IsNullOrWhiteSpace(extraInfo))
                        {
                            serverMessage21.AppendInteger(0);
                            serverMessage21.AppendInteger(0);
                            serverMessage21.AppendInteger(0);
                        }
                        else
                        {
                            string[] boolz = extraInfo.Split(',');

                            foreach (string stringy in boolz)
                            {
                                serverMessage21.AppendInteger(stringy.ToLower() == "true" ? 1 : 0);
                            }
                        }
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(0);
                        session.SendMessage(serverMessage21);
                        return;
                    }

                case InteractionType.actionposreset:
                    {
                        var serverMessage12 = new ServerMessage(LibraryParser.OutgoingRequest("WiredEffectMessageComposer"));
                        serverMessage12.AppendBool(false);
                        serverMessage12.AppendInteger(5);
                        serverMessage12.AppendInteger(list.Count);
                        foreach (RoomItem current12 in list)
                        {
                            serverMessage12.AppendInteger(current12.Id);
                        }
                        serverMessage12.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage12.AppendInteger(item.Id);
                        serverMessage12.AppendString(extraString2);
                        serverMessage12.AppendInteger(3);

                        if (String.IsNullOrWhiteSpace(extraInfo))
                        {
                            serverMessage12.AppendInteger(0);
                            serverMessage12.AppendInteger(0);
                            serverMessage12.AppendInteger(0);
                        }
                        else
                        {
                            string[] boolz = extraInfo.Split(',');

                            foreach (string stringy in boolz)
                            {
                                serverMessage12.AppendInteger(stringy.ToLower() == "true" ? 1 : 0);
                            }
                        }
                        serverMessage12.AppendInteger(0);
                        serverMessage12.AppendInteger(3);
                        serverMessage12.AppendInteger(i); // Delay
                        serverMessage12.AppendInteger(0);
                        session.SendMessage(serverMessage12);
                        return;
                    }
                case InteractionType.actionmoverotate:
                    {
                        var serverMessage13 = new ServerMessage(LibraryParser.OutgoingRequest("WiredEffectMessageComposer"));
                        serverMessage13.AppendBool(false);
                        serverMessage13.AppendInteger(5);
                        serverMessage13.AppendInteger(list.Count);
                        foreach (RoomItem current13 in list)
                        {
                            serverMessage13.AppendInteger(current13.Id);
                        }
                        serverMessage13.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage13.AppendInteger(item.Id);
                        serverMessage13.AppendString(extraInfo);
                        serverMessage13.AppendInteger(2);
                        serverMessage13.AppendInteger(int.Parse(extraInfo.Split(';')[0]));
                        serverMessage13.AppendInteger(int.Parse(extraInfo.Split(';')[1]));
                        serverMessage13.AppendInteger(0);
                        serverMessage13.AppendInteger(4);
                        serverMessage13.AppendInteger(i);
                        serverMessage13.AppendInteger(0);
                        serverMessage13.AppendInteger(0);
                        session.SendMessage(serverMessage13);
                        return;
                    }
                case InteractionType.actionresettimer:
                    {
                        var serverMessage14 = new ServerMessage(LibraryParser.OutgoingRequest("WiredEffectMessageComposer"));
                        serverMessage14.AppendBool(false);
                        serverMessage14.AppendInteger(5);
                        serverMessage14.AppendInteger(list.Count);
                        foreach (RoomItem current14 in list)
                        {
                            serverMessage14.AppendInteger(current14.Id);
                        }
                        serverMessage14.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage14.AppendInteger(item.Id);
                        serverMessage14.AppendString(extraInfo);
                        serverMessage14.AppendInteger(0);
                        serverMessage14.AppendInteger(0);
                        serverMessage14.AppendInteger(0);
                        serverMessage14.AppendInteger(0);
                        serverMessage14.AppendInteger(0);
                        serverMessage14.AppendInteger(0);
                        session.SendMessage(serverMessage14);
                        return;
                    }
                case InteractionType.actionshowmessage:
                case InteractionType.actionkickuser:
                    {
                        var serverMessage15 = new ServerMessage(LibraryParser.OutgoingRequest("WiredEffectMessageComposer"));
                        serverMessage15.AppendBool(false);
                        serverMessage15.AppendInteger(0);
                        serverMessage15.AppendInteger(list.Count);
                        foreach (RoomItem current15 in list)
                        {
                            serverMessage15.AppendInteger(current15.Id);
                        }
                        serverMessage15.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage15.AppendInteger(item.Id);
                        serverMessage15.AppendString(extraInfo);
                        serverMessage15.AppendInteger(0);
                        serverMessage15.AppendInteger(0);
                        serverMessage15.AppendInteger(7);
                        serverMessage15.AppendInteger(0);
                        serverMessage15.AppendInteger(0);
                        serverMessage15.AppendInteger(0);
                        session.SendMessage(serverMessage15);
                        return;
                    }
                case InteractionType.actionteleportto:
                    {
                        var serverMessage16 = new ServerMessage(LibraryParser.OutgoingRequest("WiredEffectMessageComposer"));
                        serverMessage16.AppendBool(false);
                        serverMessage16.AppendInteger(5);
                        serverMessage16.AppendInteger(list.Count);
                        foreach (RoomItem current16 in list)
                        {
                            serverMessage16.AppendInteger(current16.Id);
                        }
                        serverMessage16.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage16.AppendInteger(item.Id);
                        serverMessage16.AppendString(extraInfo);
                        serverMessage16.AppendInteger(0);
                        serverMessage16.AppendInteger(8);
                        serverMessage16.AppendInteger(0);
                        serverMessage16.AppendInteger(i);
                        serverMessage16.AppendInteger(0);
                        serverMessage16.AppendByted(2);
                        session.SendMessage(serverMessage16);
                        return;
                    }
                case InteractionType.actiontogglestate:
                    {
                        var serverMessage17 = new ServerMessage(LibraryParser.OutgoingRequest("WiredEffectMessageComposer"));
                        serverMessage17.AppendBool(false);
                        serverMessage17.AppendInteger(5);
                        serverMessage17.AppendInteger(list.Count);
                        foreach (RoomItem current17 in list)
                        {
                            serverMessage17.AppendInteger(current17.Id);
                        }
                        serverMessage17.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage17.AppendInteger(item.Id);
                        serverMessage17.AppendString(extraInfo);
                        serverMessage17.AppendInteger(0);
                        serverMessage17.AppendInteger(8);
                        serverMessage17.AppendInteger(0);
                        serverMessage17.AppendInteger(i);
                        serverMessage17.AppendInteger(0);
                        serverMessage17.AppendInteger(0);
                        session.SendMessage(serverMessage17);
                        return;
                    }
                case InteractionType.actiongivereward:
                    {
                        var serverMessage18 = new ServerMessage(LibraryParser.OutgoingRequest("WiredEffectMessageComposer"));
                        serverMessage18.AppendBool(false);
                        serverMessage18.AppendInteger(5);
                        serverMessage18.AppendInteger(0);
                        serverMessage18.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage18.AppendInteger(item.Id);
                        serverMessage18.AppendString(extraInfo);
                        serverMessage18.AppendInteger(3);
                        serverMessage18.AppendInteger(extraString == "" ? 0 : int.Parse(extraString));
                        serverMessage18.AppendInteger(flag ? 1 : 0);
                        serverMessage18.AppendInteger(extraString2 == "" ? 0 : int.Parse(extraString2));
                        serverMessage18.AppendInteger(0);
                        serverMessage18.AppendInteger(17);
                        serverMessage18.AppendInteger(0);
                        serverMessage18.AppendInteger(0);
                        session.SendMessage(serverMessage18);
                        return;
                    }

                case InteractionType.conditionhowmanyusersinroom:
                case InteractionType.conditionnegativehowmanyusers:
                    {
                        var serverMessage19 = new ServerMessage(LibraryParser.OutgoingRequest("WiredConditionMessageComposer"));
                        serverMessage19.AppendBool(false);
                        serverMessage19.AppendInteger(5);
                        serverMessage19.AppendInteger(0);
                        serverMessage19.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage19.AppendInteger(item.Id);
                        serverMessage19.AppendString("");
                        serverMessage19.AppendInteger(2);
                        if (String.IsNullOrWhiteSpace(extraInfo))
                        {
                            serverMessage19.AppendInteger(1);
                            serverMessage19.AppendInteger(50);
                        }
                        else
                        {
                            foreach (string integers in extraInfo.Split(','))
                            {
                                serverMessage19.AppendInteger(int.Parse(integers));
                            }
                        }
                        serverMessage19.AppendBool(false);
                        serverMessage19.AppendInteger(0);
                        serverMessage19.AppendInteger(1290);
                        session.SendMessage(serverMessage19);
                        return;
                    }

                case InteractionType.conditionfurnishaveusers:
                case InteractionType.conditionstatepos:
                case InteractionType.conditiontriggeronfurni:
                case InteractionType.conditionfurnihasfurni:
                case InteractionType.conditionfurnitypematches:
                case InteractionType.conditionfurnihasnotfurni:
                case InteractionType.conditionfurnishavenotusers:
                case InteractionType.conditionfurnitypedontmatch:
                case InteractionType.conditiontriggerernotonfurni:
                    {
                        var serverMessage19 = new ServerMessage(LibraryParser.OutgoingRequest("WiredConditionMessageComposer"));
                        serverMessage19.AppendBool(false);
                        serverMessage19.AppendInteger(5);
                        serverMessage19.AppendInteger(list.Count);
                        foreach (RoomItem current18 in list)
                        {
                            serverMessage19.AppendInteger(current18.Id);
                        }
                        serverMessage19.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage19.AppendInteger(item.Id);
                        serverMessage19.AppendInteger(0);
                        serverMessage19.AppendInteger(0);
                        serverMessage19.AppendInteger(0);
                        serverMessage19.AppendBool(false);
                        serverMessage19.AppendBool(true);
                        session.SendMessage(serverMessage19);
                        return;
                    }
                case InteractionType.conditiontimelessthan:
                case InteractionType.conditiontimemorethan:
                    {
                        var serverMessage21 = new ServerMessage(LibraryParser.OutgoingRequest("WiredConditionMessageComposer"));
                        serverMessage21.AppendBool(false);
                        serverMessage21.AppendInteger(5);
                        serverMessage21.AppendInteger(list.Count);
                        foreach (RoomItem current20 in list)
                        {
                            serverMessage21.AppendInteger(current20.Id);
                        }
                        serverMessage21.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage21.AppendInteger(item.Id);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(0);
                        session.SendMessage(serverMessage21);
                        return;
                    }

                case InteractionType.conditionuserwearingeffect:
                case InteractionType.conditionusernotwearingeffect:
                    {
                        int effect;
                        int.TryParse(extraInfo, out effect);
                        var serverMessage21 = new ServerMessage(LibraryParser.OutgoingRequest("WiredConditionMessageComposer"));
                        serverMessage21.AppendBool(false);
                        serverMessage21.AppendInteger(5);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage21.AppendInteger(item.Id);
                        serverMessage21.AppendString("");
                        serverMessage21.AppendInteger(1);
                        serverMessage21.AppendInteger(effect);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(12);
                        session.SendMessage(serverMessage21);
                        return;
                    }

                case InteractionType.conditionuserwearingbadge:
                case InteractionType.conditionusernotwearingbadge:
                    {
                        var serverMessage21 = new ServerMessage(LibraryParser.OutgoingRequest("WiredConditionMessageComposer"));
                        serverMessage21.AppendBool(false);
                        serverMessage21.AppendInteger(5);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage21.AppendInteger(item.Id);
                        serverMessage21.AppendString(extraInfo);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(11);
                        session.SendMessage(serverMessage21);
                        return;
                    }

                case InteractionType.conditiondaterangeactive:
                    {
                        int date1 = 0;
                        int date2 = 0;

                        try
                        {
                            string[] strArray = extraInfo.Split(',');
                            date1 = int.Parse(strArray[0]);
                            date2 = int.Parse(strArray[1]);
                        }
                        catch
                        {
                        }

                        var serverMessage21 = new ServerMessage(LibraryParser.OutgoingRequest("WiredConditionMessageComposer"));
                        serverMessage21.AppendBool(false);
                        serverMessage21.AppendInteger(5);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage21.AppendInteger(item.Id);
                        serverMessage21.AppendString(extraInfo);
                        serverMessage21.AppendInteger(2);
                        serverMessage21.AppendInteger(date1);
                        serverMessage21.AppendInteger(date2);
                        serverMessage21.AppendInteger(0);
                        serverMessage21.AppendInteger(24);
                        session.SendMessage(serverMessage21);
                        return;
                    }

                case InteractionType.arrowplate:
                case InteractionType.pressurepad:
                case InteractionType.ringplate:
                case InteractionType.colortile:
                case InteractionType.colorwheel:
                case InteractionType.floorswitch1:
                case InteractionType.floorswitch2:
                case InteractionType.firegate:
                case InteractionType.glassfoor:
                    break;
                case InteractionType.specialrandom:
                    {
                        var serverMessage24 = new ServerMessage(LibraryParser.OutgoingRequest("WiredEffectMessageComposer"));
                        serverMessage24.AppendBool(false);
                        serverMessage24.AppendInteger(5);
                        serverMessage24.AppendInteger(list.Count);
                        foreach (RoomItem current23 in list)
                        {
                            serverMessage24.AppendInteger(current23.Id);
                        }
                        serverMessage24.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage24.AppendInteger(item.Id);
                        serverMessage24.AppendString(extraInfo);
                        serverMessage24.AppendInteger(0);
                        serverMessage24.AppendInteger(8);
                        serverMessage24.AppendInteger(0);
                        serverMessage24.AppendInteger(0);
                        serverMessage24.AppendInteger(0);
                        serverMessage24.AppendInteger(0);
                        session.SendMessage(serverMessage24);
                        return;
                    }
                case InteractionType.specialunseen:
                    {
                        var serverMessage25 = new ServerMessage(LibraryParser.OutgoingRequest("WiredEffectMessageComposer"));
                        serverMessage25.AppendBool(false);
                        serverMessage25.AppendInteger(5);
                        serverMessage25.AppendInteger(list.Count);
                        foreach (RoomItem current24 in list)
                        {
                            serverMessage25.AppendInteger(current24.Id);
                        }
                        serverMessage25.AppendInteger(item.GetBaseItem().SpriteId);
                        serverMessage25.AppendInteger(item.Id);
                        serverMessage25.AppendString(extraInfo);
                        serverMessage25.AppendInteger(0);
                        serverMessage25.AppendInteger(8);
                        serverMessage25.AppendInteger(0);
                        serverMessage25.AppendInteger(0);
                        serverMessage25.AppendInteger(0);
                        serverMessage25.AppendInteger(0);
                        session.SendMessage(serverMessage25);
                        return;
                    }
                default:
                    return;
            }
        }

        public void OnUserWalk(GameClient session, RoomItem item, RoomUser user)
        {
        }

        public void OnWiredTrigger(RoomItem item)
        {
        }
    }
}