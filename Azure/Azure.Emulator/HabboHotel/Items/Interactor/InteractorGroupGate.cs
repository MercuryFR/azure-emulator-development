﻿using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Rooms;
using Azure.HabboHotel.Rooms.Wired;

namespace Azure.HabboHotel.Items.Interactor
{
    internal class InteractorGroupGate : IFurniInteractor
    {
        public void OnPlace(GameClient session, RoomItem item)
        {
        }

        public void OnRemove(GameClient session, RoomItem item)
        {
        }

        public void OnTrigger(GameClient session, RoomItem item, int request, bool hasRights)
        {
            checked
            {
                if (!hasRights)
                    return;
                if (item == null || item.GetBaseItem() == null ||
                    item.GetBaseItem().InteractionType != InteractionType.gate)
                    return;
                var modes = item.GetBaseItem().Modes - 1;
                if (modes <= 0)
                    item.UpdateState(false, true);
                int currentMode;
                int.TryParse(item.ExtraData, out currentMode);
                int newMode;
                if (currentMode <= 0)
                    newMode = 1;
                else if (currentMode >= modes)
                    newMode = 0;
                else
                    newMode = currentMode + 1;
                if (newMode == 0 && !item.GetRoom().GetGameMap().ItemCanBePlacedHere(item.X, item.Y))
                    return;
                item.ExtraData = newMode.ToString();
                item.UpdateState();
                item.GetRoom().GetGameMap().UpdateMapForItem(item);
                item.GetRoom().GetWiredHandler().ExecuteWired(WiredItemType.TriggerToggleFurni, new object[]
                {
                    item.GetRoom().GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().Id),
                    item
                });
            }
        }

        public void OnUserWalk(GameClient session, RoomItem item, RoomUser user)
        {
            if (session == null || item == null || user == null)
                return;
            var UserId = user.RoomId;
            var roomId = item.GetRoom().RoomId;
            if(UserId == roomId)
            {
                var uX = user.X;
                var uY = user.Y;
                var iX = item.X;
                var iY = item.Y;
                var distance = PathFinding.PathFinder.GetDistance(uX, uY, iX, iY);
                if(distance < 3 && item.ExtraData == "0")
                {
                    item.ExtraData = "1";
                    item.UpdateState();
                    return;
                }
                else if(distance < 3 && item.ExtraData == "1")
                {
                    item.ExtraData = "0";
                    item.UpdateState();
                    return;
                }
                else
                {
                    item.ExtraData = "0";
                    item.UpdateState();
                    return;
                }
            }
        }

        public void OnWiredTrigger(RoomItem item)
        {
            checked
            {
                int num = item.GetBaseItem().Modes - 1;
                if (num <= 0)
                {
                    item.UpdateState(false, true);
                }
                int num2 = 0;
                int.TryParse(item.ExtraData, out num2);
                int num3;
                if (num2 <= 0)
                {
                    num3 = 1;
                }
                else
                {
                    if (num2 >= num)
                    {
                        num3 = 0;
                    }
                    else
                    {
                        num3 = num2 + 1;
                    }
                }
                if (num3 == 0 && !item.GetRoom().GetGameMap().ItemCanBePlacedHere(item.X, item.Y))
                {
                    return;
                }
                item.ExtraData = num3.ToString();
                item.UpdateState();
                item.GetRoom().GetGameMap().UpdateMapForItem(item);
            }
        }
    }
}