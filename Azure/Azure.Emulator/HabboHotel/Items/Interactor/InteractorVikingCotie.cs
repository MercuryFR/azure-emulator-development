using System.Timers;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Rooms;

namespace Azure.HabboHotel.Items.Interactor
{
    internal class InteractorVikingCotie : IFurniInteractor
    {
        private RoomItem _mItem;

        public void OnPlace(GameClient session, RoomItem item)
        {
        }

        public void OnRemove(GameClient session, RoomItem item)
        {
        }

        public void OnTrigger(GameClient session, RoomItem item, int request, bool hasRights)
        {
            RoomUser user = item.GetRoom().GetRoomUserManager().GetRoomUserByHabbo(session.GetHabbo().Id);
            if (user == null)
            {
                return;
            }

            if (user.CurrentEffect != 172 && user.CurrentEffect != 5 && user.CurrentEffect != 173)
            {
                return;
            }
            if (item.ExtraData != "5")
            {
                if (item.VikingCotieBurning)
                {
                    return;
                }
                item.ExtraData = "1";
                item.UpdateState();

                item.VikingCotieBurning = true;
                GameClient clientByUsername = Azure.GetGame().GetClientManager().GetClientByUserName(item.GetRoom().Owner);
                       
                if (clientByUsername != null)
                {
                    if (clientByUsername.GetHabbo().UserName != item.GetRoom().Owner)
                    {
                        clientByUsername.SendNotif(string.Format("{0} ha empezado a quemar una cabaña vikingo de tu Sala!", user.GetUserName()));
                    }
                }

                this._mItem = item;

                var timer = new Timer(5000);
                timer.Elapsed += this.OnElapse;
                timer.Enabled = true;
            }
            else
            {
                session.SendNotif("¡Lo sentimos! Esta cabaña Vikingo ya ha sido quemada y no hay marcha atrás!");
            }
        }

        public void OnUserWalk(GameClient session, RoomItem item, RoomUser user)
        {
        }

        public void OnWiredTrigger(RoomItem item)
        {
        }

        private void OnElapse(object sender, ElapsedEventArgs e)
        {
            switch (this._mItem.ExtraData)
            {
                case "1":
                    this._mItem.ExtraData = "2";
                    this._mItem.UpdateState();
                    return;

                case "2":
                    this._mItem.ExtraData = "3";
                    this._mItem.UpdateState();
                    return;

                case "3":
                    this._mItem.ExtraData = "4";
                    this._mItem.UpdateState();
                    return;

                case "4":
                    ((Timer)sender).Stop();
                    this._mItem.ExtraData = "5";
                    this._mItem.UpdateState();
                    return;
            }
        }
    }
}