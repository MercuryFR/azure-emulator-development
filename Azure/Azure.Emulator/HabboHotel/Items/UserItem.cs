using Azure.Messages;

namespace Azure.HabboHotel.Items
{
    class UserItem
    {
        internal uint Id;
        internal uint BaseItemId;
        internal string ExtraData;
        internal bool IsWallItem;
        internal int LimitedNo;
        internal int LimitedTot;
        internal string SongCode;
        internal uint GroupId;
        internal readonly Item BaseItem;

        internal UserItem(uint id, uint baseItemId, string extraData, uint @group, string songCode)
        {
            Id = id;
            BaseItemId = baseItemId;
            ExtraData = extraData;
            BaseItem = Azure.GetGame().GetItemManager().GetItem(baseItemId);
            if (BaseItem == null)
                return;
            GroupId = @group;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT * FROM items_limited WHERE item_id={0} LIMIT 1", id));
                var row = queryReactor.GetRow();
                if (row != null)
                {
                    LimitedNo = int.Parse(row[1].ToString());
                    LimitedTot = int.Parse(row[2].ToString());
                }
                else
                {
                    LimitedNo = 0;
                    LimitedTot = 0;
                }
            }
            IsWallItem = (BaseItem.Type == 'i');
            SongCode = songCode;
        }

        internal void SerializeWall(ServerMessage message, bool inventory)
        {
            message.AppendInteger(Id);
            message.AppendString(BaseItem.Type.ToString().ToUpper());
            message.AppendInteger(Id);
            message.AppendInteger(BaseItem.SpriteId);

            if (BaseItem.Name.Contains("a2") || BaseItem.Name == "floor")
                message.AppendInteger(3);
            else if (BaseItem.Name.Contains("wallpaper") && BaseItem.Name != "wildwest_wallpaper")
                message.AppendInteger(2);
            else if (BaseItem.Name.Contains("landscape"))
                message.AppendInteger(4);
            else
                message.AppendInteger(1);
            message.AppendInteger(0);
            message.AppendString(ExtraData);
            message.AppendBool(BaseItem.AllowRecycle);
            message.AppendBool(BaseItem.AllowTrade);
            message.AppendBool(BaseItem.AllowInventoryStack);
            message.AppendBool(false);
            message.AppendInteger(-1);
            message.AppendBool(true);
            message.AppendInteger(-1);
        }

        internal void SerializeFloor(ServerMessage message, bool inventory)
        {
            message.AppendInteger(Id);
            message.AppendString(BaseItem.Type.ToString().ToUpper());
            message.AppendInteger(Id);
            message.AppendInteger(BaseItem.SpriteId);
            var extraParam = 0;
            if (BaseItem.InteractionType == InteractionType.gld_item ||
                BaseItem.InteractionType == InteractionType.gld_gate)
            {
                var group = Azure.GetGame().GetGroupManager().GetGroup(GroupId);
                if (group != null)
                {
                    message.AppendInteger(17);
                    message.AppendInteger(2);
                    message.AppendInteger(5);
                    message.AppendString(ExtraData);
                    message.AppendString(group.Id.ToString());
                    message.AppendString(group.Badge);
                    message.AppendString(Azure.GetGame().GetGroupManager().GetGroupColour(group.Colour1, true));
                    message.AppendString(Azure.GetGame().GetGroupManager().GetGroupColour(group.Colour2, false));
                }
                else if (BaseItem.InteractionType == InteractionType.moplaseed)
                {
                    message.AppendInteger(19);
                    message.AppendInteger(1);
                    message.AppendInteger(1);
                    message.AppendString("rarity");
                    message.AppendString(ExtraData);
                }
                else if (LimitedNo > 0)
                {
                    message.AppendInteger(1);
                    message.AppendInteger(256);
                    message.AppendString(ExtraData);
                    message.AppendInteger(LimitedNo);
                    message.AppendInteger(LimitedTot);
                }
                else
                {
                    message.AppendInteger((BaseItem.InteractionType == InteractionType.gift) ? 9 : 0);
                    message.AppendInteger(0);
                    message.AppendString((BaseItem.InteractionType == InteractionType.gift)
                        ? string.Empty
                        : ExtraData);
                }
            }
            else
            {
                message.AppendInteger(1);
                message.AppendInteger(0);
                message.AppendString((BaseItem.InteractionType == InteractionType.gift) ? "" : ExtraData);
            }
            message.AppendBool(BaseItem.AllowRecycle);
            message.AppendBool(BaseItem.AllowTrade);
            message.AppendBool(LimitedNo <= 0 && BaseItem.AllowInventoryStack);
            message.AppendBool(false);
            message.AppendInteger(-1);
            message.AppendBool(true);
            message.AppendInteger(-1);
            message.AppendString("");

            try
            {
                if (BaseItem.InteractionType == InteractionType.gift)
                {
                    var split = ExtraData.Split((char) 9);
                    var ribbon = int.Parse(split[2]);
                    var colour = int.Parse(split[3]);
                    extraParam = (ribbon * 1000) + colour;
                }
            }
            catch
            {
                extraParam = 1001;
            }
            message.AppendInteger(extraParam);
        }
    }
}