using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Azure.Configuration;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.HabboHotel.Items
{
    class ItemManager
    {
        internal Dictionary<uint, Item> Items;
        internal uint PhotoId;

        internal ItemManager() { Items = new Dictionary<uint, Item>(); }

        internal void LoadItems(IQueryAdapter dbClient, out uint itemLoaded)
        {
            LoadItems(dbClient);
            itemLoaded = (uint) Items.Count;
        }

        internal void LoadItems(IQueryAdapter dbClient)
        {
            Items = new Dictionary<uint, Item>();
            dbClient.SetQuery("SELECT * FROM catalog_furnis");
            var table = dbClient.GetTable();
            if (table == null)
                return;
            List<double> heights = null;
            foreach (DataRow dataRow in table.Rows)
                try
                {
                    var id = Convert.ToUInt32(dataRow["id"]);
                    var type = Convert.ToChar(dataRow["type"]);
                    var name = (string) dataRow["item_name"];
                    short sprite = 0;
                    ushort x = 0, y = 0;
                    var publicName = "";
                    bool canWalk = false, canSit = false, stackMultiple = false;

                    if (name == "external_image_wallitem_poster")
                        PhotoId = id;
                    if(name.StartsWith("a0 pet"))
                    {
                        x = 1;
                        y = 1;
                        publicName = name;
                    }
                    else if (name == "landscape" || name == "floor" || name == "wallpaper")
                    {
                        sprite = FurniDataParser.WallItems[name].Id;
                        x = 1;
                        y = 1;
                    }
                    else if (type == 's' && FurniDataParser.FloorItems.ContainsKey(name))
                    {
                        sprite = FurniDataParser.FloorItems[name].Id;
                        publicName = FurniDataParser.FloorItems[name].Name;
                        x = FurniDataParser.FloorItems[name].X;
                        y = FurniDataParser.FloorItems[name].Y;
                        canWalk = FurniDataParser.FloorItems[name].CanWalk;
                        canSit = FurniDataParser.FloorItems[name].CanSit;
                    }
                    else if (type == 'i' && FurniDataParser.WallItems.ContainsKey(name))
                    {
                        sprite = FurniDataParser.WallItems[name].Id;
                        publicName = FurniDataParser.WallItems[name].Name;
                    }
                    else if (type != 'e' && type != 'h' && type != 'r' && type != 'b')
                        continue;
                    var flatId = (int) dataRow["flat_id"];

                    double height;
                    if (dataRow["stack_height"].ToString().Contains(";"))
                    {
                        var heightsStr = dataRow["stack_height"].ToString().Split(';');
                        heights = heightsStr.Select(heightStr => double.Parse(heightStr, CultureInfo.InvariantCulture)).ToList();
                        height = heights[0];
                        stackMultiple = true;
                    }
                    else
                        height = double.Parse(dataRow["stack_height"].ToString(), CultureInfo.InvariantCulture);
                    var stackable = Convert.ToInt32(dataRow["can_stack"]) == 1;
                    var allowRecycle = Convert.ToInt32(dataRow["allow_recycle"]) == 1;
                    var allowTrade = Convert.ToInt32(dataRow["allow_trade"]) == 1;
                    var allowMarketplaceSell = Convert.ToInt32(dataRow["allow_marketplace_sell"]) == 1;
                    var allowGift = Convert.ToInt32(dataRow["allow_gift"]) == 1;
                    var allowInventoryStack = Convert.ToInt32(dataRow["allow_inventory_stack"]) == 1;
                    var typeFromString = InterractionTypes.GetTypeFromString((string) dataRow["interaction_type"]);

                    if (InterractionTypes.AreFamiliar(InteractionGlobalType.Gate, typeFromString) || typeFromString == InteractionType.banzaipyramid || name.StartsWith("hole"))
                        canWalk = false;

                    var modes = (int) dataRow["interaction_modes_count"];
                    var vendingIds = (string) dataRow["vending_ids"];
                    var sub = Azure.EnumToBool(dataRow["subscriber"].ToString());
                    var effect = (int) dataRow["effectid"];

                    var value = new Item(id, sprite, publicName, name, type, x, y, height, stackable, canWalk,
                        canSit, allowRecycle, allowTrade, allowMarketplaceSell, allowGift, allowInventoryStack,
                        typeFromString, modes, vendingIds, sub, effect, stackMultiple, (heights == null ? null : heights.ToArray()), flatId);
                    Items.Add(id, value);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Console.ReadKey();
                    Logging.WriteLine(
                        string.Format("Could not load item #{0}, please verify the data is okay.",
                            Convert.ToUInt32(dataRow[0])), ConsoleColor.Violet);
                }
        }

        internal bool ContainsItem(uint id) { return Items.ContainsKey(id); }

        internal Item GetItem(uint id)
        {
            return ContainsItem(id) ? Items[id] : null;
        }

        internal Item GetItemBySprite(int spriteId, char type)
        {
            return Items.Values.FirstOrDefault(x => x.SpriteId == spriteId && x.Type == type);
        }

        internal Item GetItemByName(string name)
        {
            return (
                from x in Items.Values
                where x.Name == name
                select x).FirstOrDefault<Item>();
        }
    }
}