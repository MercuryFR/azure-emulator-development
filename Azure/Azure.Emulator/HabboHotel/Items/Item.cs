using System.Collections.Generic;

namespace Azure.HabboHotel.Items
{
    class Item
    {
        internal int SpriteId;
        internal string PublicName;
        internal string Name;
        internal char Type;
        internal int Width;
        internal int Length;
        internal double Height;
        internal bool Stackable;
        internal bool Walkable;
        internal bool IsSeat;
        internal bool AllowRecycle;
        internal bool AllowTrade;
        internal bool AllowMarketplaceSell;
        internal bool AllowGift;
        internal bool AllowInventoryStack;
        internal bool SubscriberOnly;
        internal bool StackMultipler;
        internal double[] ToggleHeight;
        internal InteractionType InteractionType;
        internal List<int> VendingIds;
        internal int Modes;
        internal int EffectId;
        internal int FlatId;
        internal bool IsGroupItem;

        internal Item(uint id, short sprite, string publicName, string name, char type, int width, int length,
            double height, bool stackable, bool walkable, bool isSeat, bool allowRecycle, bool allowTrade,
            bool allowMarketplaceSell, bool allowGift, bool allowInventoryStack, InteractionType interactionType,
            int modes, string vendingIds, bool sub, int effect, bool stackMultiple, double[] toggle, int flatId)
        {
            ItemId = id;
            SpriteId = sprite;
            PublicName = publicName;
            Name = name;
            Type = type;
            Width = width;
            Length = length;
            Height = height;
            Stackable = stackable;
            Walkable = walkable;
            IsSeat = isSeat;
            AllowRecycle = allowRecycle;
            AllowTrade = allowTrade;
            AllowMarketplaceSell = allowMarketplaceSell;
            AllowGift = allowGift;
            AllowInventoryStack = allowInventoryStack;
            InteractionType = interactionType;
            Modes = modes;
            VendingIds = new List<int>();
            SubscriberOnly = sub;
            EffectId = effect;
            if (vendingIds.Contains(","))
            {
                var array = vendingIds.Split(',');
                foreach (var s in array)
                    VendingIds.Add(int.Parse(s));
            }
            else if (!vendingIds.Equals("") && int.Parse(vendingIds) > 0)
                VendingIds.Add(int.Parse(vendingIds));
            IsGroupItem = (Name.ToLower().StartsWith("gld_") || Name.ToLower().StartsWith("guild_") || Name.ToLower().Contains("grp"));
            StackMultipler = stackMultiple;
            ToggleHeight = toggle;
            FlatId = flatId;
        }

        internal uint ItemId { get; private set; }
    }
}