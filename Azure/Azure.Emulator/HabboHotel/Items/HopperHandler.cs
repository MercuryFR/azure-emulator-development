using System;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.HabboHotel.Items
{
    static class HopperHandler
    {
        internal static uint GetAHopper(uint curRoom)
        {
            uint result;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT room_id FROM items_hopper WHERE room_id <> @room ORDER BY RAND() LIMIT 1");
                queryReactor.AddParameter("room", curRoom);
                var num = Convert.ToUInt32(queryReactor.GetInteger());
                result = num;
            }
            return result;
        }

        internal static uint GetHopperId(uint nextRoom)
        {
            uint result;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT hopper_id FROM items_hopper WHERE room_id = @room LIMIT 1");
                queryReactor.AddParameter("room", nextRoom);
                var @string = queryReactor.GetString();
                result = @string == null ? 0u : Convert.ToUInt32(@string);
            }
            return result;
        }
    }
}