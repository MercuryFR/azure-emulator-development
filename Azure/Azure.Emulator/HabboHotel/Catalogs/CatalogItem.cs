using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Azure.Configuration;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Catalogs
{
    class CatalogItem
    {
        internal readonly uint Id;
        internal readonly string ItemIdString;
        internal readonly string Name;
        internal readonly int CreditsCost;
        internal readonly int BelCreditsCost;
        internal readonly int LoyaltyCost;
        internal readonly int DucketsCost;
        internal readonly int PageId;
        internal readonly uint SongId;
        internal readonly bool IsLimited;
        internal readonly int LimitedStack;
        internal readonly bool HaveOffer;
        internal readonly bool ClubOnly;
        internal readonly string ExtraData;
        internal Dictionary<uint, int> Items;
        internal int LimitedSelled;
        internal string Badge = "";
        internal uint BaseId;
        internal int FirstAmount;

        internal CatalogItem(DataRow row, string name)
        {
            Id = Convert.ToUInt32(row["id"]);
            Name = name;
            ItemIdString = row["item_ids"].ToString();
            Items = new Dictionary<uint, int>();
            var array = ItemIdString.Split(';');
            var array2 = row["amounts"].ToString().Split(';');
            checked
            {
                for (var i = 0; i < array.Length; i++)
                {
                    uint key;
                    int value;
                    if (uint.TryParse(array[i], out key) && int.TryParse(array2[i], out value))
                        Items.Add(key, value);
                }
                BaseId = Items.Keys.First();
                FirstAmount = Items.Values.First();
                PageId = (int) row["page_id"];
                CreditsCost = (int) row["cost_credits"];
                BelCreditsCost = (int) row["cost_belcredits"];
                LoyaltyCost = (int) row["cost_loyalty"];
                DucketsCost = (int) row["cost_duckets"];
                LimitedSelled = (int) row["limited_sells"];
                LimitedStack = (int) row["limited_stack"];
                IsLimited = (LimitedStack > 0);
                Badge = (string) row["badge"];
                HaveOffer = ((string) row["offer_active"] == "1");
                ClubOnly = ((string) row["club_only"] == "1");
                ExtraData = (string) row["extradata"];
                SongId = (uint) row["song_id"];
            }
        }

        internal Item GetBaseItem(uint itemIds)
        {
            var item = Azure.GetGame().GetItemManager().GetItem(itemIds);
            if (item == null)
                Logging.WriteLine(string.Format("UNKNOWN ItemIds: {0}", itemIds), ConsoleColor.Red);
            return item;
        }

        internal Item GetFirstBaseItem() { return GetBaseItem(BaseId); }
    }
}