using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Azure.Messages;

namespace Azure.HabboHotel.Catalogs
{
    class CatalogPage
    {
        internal short ParentId;
        internal string CodeName;
        internal string Caption;
        internal bool Visible;
        internal bool Enabled;
        internal bool ComingSoon;
        internal uint MinRank;
        internal int IconImage;
        internal string Layout;
        internal string LayoutHeadline;
        internal string LayoutTeaser;
        internal string LayoutSpecial;
        internal string Text1;
        internal string Text2;
        internal string TextDetails;
        internal string TextTeaser;
        internal string PageLinkTitle;
        internal string PageLink;
        internal int OrderNum;
        internal HybridDictionary Items;
        internal Dictionary<int, uint> FlatOffers;
        internal ServerMessage CachedContentsMessage;

        internal CatalogPage(ushort id, short parentId, string codeName, string caption, bool visible, bool enabled,
            bool comingSoon, uint minRank, int iconImage, string layout, string layoutHeadline, string layoutTeaser,
            string layoutSpecial, string text1, string text2, string textDetails, string textTeaser,
            string pageLinkTitle, string pageLink, int orderNum, ref HybridDictionary cataItems)
        {
            PageId = id;
            ParentId = parentId;
            CodeName = codeName;
            Caption = caption;
            Visible = visible;
            Enabled = enabled;
            ComingSoon = comingSoon;
            MinRank = minRank;
            IconImage = iconImage;
            Layout = layout;
            LayoutHeadline = layoutHeadline;
            LayoutTeaser = layoutTeaser;
            LayoutSpecial = layoutSpecial;
            Text1 = text1;
            PageLinkTitle = pageLinkTitle;
            PageLink = pageLink;
            Text2 = text2;
            TextDetails = textDetails;
            TextTeaser = textTeaser;
            OrderNum = orderNum;
            if (layout.StartsWith("frontpage"))
                OrderNum = -2;
            Items = new HybridDictionary();
            FlatOffers = new Dictionary<int, uint>();
            foreach (
                var catalogItem in
                    cataItems.Values.OfType<CatalogItem>()
                        .Where(x => x.PageId == id)
                        .Where(catalogItem => catalogItem.GetFirstBaseItem() != null))
            {
                Items.Add(catalogItem.Id, catalogItem);
                var flatId = catalogItem.GetFirstBaseItem().FlatId;
                if (flatId != -1 && !FlatOffers.ContainsKey(flatId))
                    FlatOffers.Add(catalogItem.GetFirstBaseItem().FlatId, catalogItem.Id);
            }
            CachedContentsMessage = CatalogPacket.ComposePage(this);
        }

        internal ushort PageId { get; private set; }

        internal CatalogItem GetItem(int pId)
        {
            var num = (uint) pId;
            if (FlatOffers.ContainsKey(pId))
                return (CatalogItem) Items[FlatOffers[pId]];
            if (Items.Contains(num))
                return (CatalogItem) Items[num];
            return null;
        }
    }
}