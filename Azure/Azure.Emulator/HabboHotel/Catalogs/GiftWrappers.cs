using System.Collections.Generic;
using System.Data;
using Azure.Database.Manager.Session_Details.Interfaces;

namespace Azure.HabboHotel.Catalogs
{
    class GiftWrappers
    {
        internal List<uint> GiftWrappersList = new List<uint>();
        internal List<uint> OldGiftWrappers = new List<uint>();

        internal GiftWrappers(IRegularQueryAdapter dbClient)
        {
            dbClient.SetQuery("SELECT * FROM items_gifts_wrappers");
            var table = dbClient.GetTable();
            if (table == null)
                return;
            foreach (DataRow dataRow in table.Rows)
                switch (dataRow["type"].ToString())
                {
                    case "new":
                        GiftWrappersList.Add((uint) dataRow["sprite_id"]);
                        break;
                    case "old":
                        OldGiftWrappers.Add((uint) dataRow["sprite_id"]);
                        break;
                }
        }
    }
}