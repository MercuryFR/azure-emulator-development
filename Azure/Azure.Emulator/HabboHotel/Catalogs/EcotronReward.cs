using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.Catalogs
{
    class EcotronReward
    {
        internal uint DisplayId;
        internal uint BaseId;
        internal uint RewardLevel;

        internal EcotronReward(uint displayId, uint baseId, uint rewardLevel)
        {
            DisplayId = displayId;
            BaseId = baseId;
            RewardLevel = rewardLevel;
        }

        internal Item GetBaseItem() { return Azure.GetGame().GetItemManager().GetItem(BaseId); }
    }
}