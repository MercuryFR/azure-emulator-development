﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using Azure.Configuration;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.Quests;
using Azure.HabboHotel.RoomBots;
using Azure.HabboHotel.SoundMachine;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Catalogs
{
    class Catalog
    {
        internal static int LastSentOffer;
        internal HybridDictionary Categories;
        internal HybridDictionary Offers;
        internal Dictionary<int, uint> FlatOffers;
        internal List<CatalogItem> HabboClubItems;
        internal List<EcotronReward> EcotronRewards;
        internal List<int> EcotronLevels;

        internal static bool CheckPetName(string petName)
        {
            return petName.Length >= 3 && petName.Length <= 15 && Azure.IsValidAlphaNumeric(petName);
        }

        internal static RoomBot CreateBot(uint userId, string name, string look, string motto, string gender,
            bool bartender)
        {
            uint botId;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    "INSERT INTO bots (user_id,name,motto,look,gender,walk_mode,is_bartender) VALUES (@h_user,@b_name,@b_motto,@b_look,@b_gender,@b_walk,@b_bartender)");
                queryReactor.AddParameter("h_user", userId);
                queryReactor.AddParameter("b_name", name);
                queryReactor.AddParameter("b_motto", motto);
                queryReactor.AddParameter("b_look", look);
                queryReactor.AddParameter("b_gender", gender);
                queryReactor.AddParameter("b_walk", "freeroam");
                queryReactor.AddParameter("b_bartender", bartender ? "1" : "0");
                botId = Convert.ToUInt32(queryReactor.InsertQuery());
            }
            return new RoomBot(botId, userId, 0u, AIType.Generic, "freeroam", name, motto, look, 0, 0, 0.0, 0, 0, 0, 0,
                0, null, null, gender, 0, bartender);
        }

        internal static Pet CreatePet(uint userId, string name, int type, string Race, string Color, int Rarity = 0)
        {
            checked
            {
                var pet = new Pet(404u, userId, 0u, name, (uint) type, Race, Color, 0, 100, 100, 0,
                    Azure.GetUnixTimestamp(), 0, 0, 0.0, false, 0, 0, -1, Rarity,
                    DateTime.Now.AddHours(36.0), DateTime.Now.AddHours(48.0), null)
                {
                    DbState = DatabaseUpdateState.NeedsUpdate
                };
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery(string.Concat(new object[]
                    {
                        "INSERT INTO bots (user_id,name, ai_type) VALUES (",
                        pet.OwnerId,
                        ",@",
                        pet.PetId,
                        "name, 'pet')"
                    }));
                    queryReactor.AddParameter(string.Format("{0}name", pet.PetId), pet.Name);
                    pet.PetId = (uint) queryReactor.InsertQuery();
                    queryReactor.SetQuery(string.Concat(new object[]
                    {
                        "INSERT INTO pets_data (id,type,race,color,experience,energy,createstamp,rarity,lasthealth_stamp,untilgrown_stamp) VALUES (",
                        pet.PetId,
                        ", ",
                        pet.Type,
                        ",@",
                        pet.PetId,
                        "race,@",
                        pet.PetId,
                        "color,0,100,UNIX_TIMESTAMP(), ",
                        Rarity,
                        ", UNIX_TIMESTAMP(now() + INTERVAL 36 HOUR), UNIX_TIMESTAMP(now() + INTERVAL 48 HOUR))"
                    }));
                    queryReactor.AddParameter(string.Format("{0}race", pet.PetId), pet.Race);
                    queryReactor.AddParameter(string.Format("{0}color", pet.PetId), pet.Color);
                    queryReactor.RunQuery();
                }

                if (pet.Type != 16u)
                    return pet;
                pet.MoplaBreed = MoplaBreed.CreateMonsterplantBreed(pet);
                pet.Name = pet.MoplaBreed.Name;
                pet.DbState = DatabaseUpdateState.NeedsUpdate;
                return pet;
            }
        }

        internal static Pet GeneratePetFromRow(DataRow Row, DataRow mRow)
        {
            if (Row == null)
                return null;
            MoplaBreed moplaBreed = null;
            if (Convert.ToUInt32(mRow["type"]) == 16u)
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery(string.Format("SELECT * FROM pets_plants WHERE pet_id = {0}",
                        Convert.ToUInt32(Row["id"])));
                    var row = queryReactor.GetRow();
                    moplaBreed = new MoplaBreed(row);
                }
            return new Pet(Convert.ToUInt32(Row["id"]), Convert.ToUInt32(Row["user_id"]),
                Convert.ToUInt32(Row["room_id"]), (string) Row["name"], Convert.ToUInt32(mRow["type"]),
                (string) mRow["race"], (string) mRow["color"], (int) mRow["experience"], (int) mRow["energy"],
                (int) mRow["nutrition"], (int) mRow["respect"], Convert.ToDouble(mRow["createstamp"]), (int) Row["x"],
                (int) Row["y"], Convert.ToDouble(Row["z"]), (int) mRow["have_saddle"] == 1, (int) mRow["anyone_ride"],
                (int) mRow["hairdye"], (int) mRow["pethair"], (int) mRow["rarity"],
                Azure.UnixToDateTime((int) mRow["lasthealth_stamp"]),
                Azure.UnixToDateTime((int) mRow["untilgrown_stamp"]), moplaBreed);
        }

        internal CatalogItem GetItemFromOffer(int offerId)
        {
            CatalogItem result = null;
            if (FlatOffers.ContainsKey(offerId))
                result = (CatalogItem) Offers[FlatOffers[offerId]];
            return result ?? (Azure.GetGame().GetCatalog().GetItem(Convert.ToUInt32(offerId)));
        }

        internal void Initialize(IQueryAdapter dbClient, out uint pageLoaded)
        {
            Initialize(dbClient);
            pageLoaded = (uint) Categories.Count;
        }

        internal void Initialize(IQueryAdapter dbClient)
        {
            Categories = new HybridDictionary();
            Offers = new HybridDictionary();
            FlatOffers = new Dictionary<int, uint>();
            EcotronRewards = new List<EcotronReward>();
            EcotronLevels = new List<int>();
            HabboClubItems = new List<CatalogItem>();
            dbClient.SetQuery("SELECT * FROM catalog_pages ORDER BY order_num");
            var table = dbClient.GetTable();
            dbClient.SetQuery("SELECT * FROM catalog_ecotron ORDER BY reward_level ASC");
            var table2 = dbClient.GetTable();
            dbClient.SetQuery("SELECT * FROM catalog_items");
            var table3 = dbClient.GetTable();
            dbClient.SetQuery("SELECT * FROM  catalog_items WHERE specialName LIKE 'HABBO_CLUB_VIP%'");
            var table4 = dbClient.GetTable();
            if (table3 != null)
                foreach (DataRow dataRow in table3.Rows)
                {
                    if (string.IsNullOrEmpty(dataRow["item_ids"].ToString()) ||
                        string.IsNullOrEmpty(dataRow["amounts"].ToString()))
                        continue;
                    var source = dataRow["item_ids"].ToString();
                    var id = uint.Parse(dataRow["item_ids"].ToString().Split(';')[0]);
                    var item = Azure.GetGame().GetItemManager().GetItem(id);
                    if (item == null)
                        continue;
                    var num = !source.Contains(';') ? item.FlatId : -1;
                    if (!dataRow.IsNull("specialName"))
                        item.PublicName = (string) dataRow["specialName"];
                    var value2 = new CatalogItem(dataRow, item.PublicName);
                    if (value2.GetFirstBaseItem() == null)
                        continue;
                    Offers.Add(value2.Id, value2);
                    if (num != -1 && !FlatOffers.ContainsKey(num))
                        FlatOffers.Add(num, value2.Id);
                }
            if (table != null)
                foreach (DataRow dataRow2 in table.Rows)
                {
                    var visible = false;
                    var enabled = false;
                    var comingSoon = false;
                    if (dataRow2["visible"].ToString() == "1")
                        visible = true;
                    if (dataRow2["enabled"].ToString() == "1")
                        enabled = true;
                    Categories.Add(Convert.ToUInt16(dataRow2["id"]),
                        new CatalogPage(Convert.ToUInt16(dataRow2["id"]), short.Parse(dataRow2["parent_id"].ToString()),
                            (string) dataRow2["code_name"], (string) dataRow2["caption"], visible, enabled, comingSoon,
                            Convert.ToUInt32(dataRow2["min_rank"]), (int) dataRow2["icon_image"],
                            (string) dataRow2["page_layout"], (string) dataRow2["page_headline"],
                            (string) dataRow2["page_teaser"], (string) dataRow2["page_special"],
                            (string) dataRow2["page_text1"], (string) dataRow2["page_text2"],
                            (string) dataRow2["page_text_details"], (string) dataRow2["page_text_teaser"],
                            (string) dataRow2["page_link_description"], (string) dataRow2["page_link_pagename"],
                            (int) dataRow2["order_num"], ref Offers));
                }
            if (table2 != null)
                foreach (DataRow dataRow3 in table2.Rows)
                {
                    EcotronRewards.Add(new EcotronReward(Convert.ToUInt32(dataRow3["display_id"]),
                        Convert.ToUInt32(dataRow3["item_id"]), Convert.ToUInt32(dataRow3["reward_level"])));
                    if (!EcotronLevels.Contains(Convert.ToInt16(dataRow3["reward_level"])))
                        EcotronLevels.Add(Convert.ToInt16(dataRow3["reward_level"]));
                }
            if (table4 == null)
                return;
            foreach (DataRow row in table4.Rows)
                HabboClubItems.Add(new CatalogItem(row, "Habbo VIP"));
        }

        internal CatalogItem GetItem(uint itemId)
        {
            return Offers.Contains(itemId) ? (CatalogItem) Offers[itemId] : null;
        }

        internal CatalogPage GetPage(ushort page)
        {
            return !Categories.Contains(page) ? null : (CatalogPage) Categories[page];
        }

        internal void HandlePurchase(GameClient Session, ushort PageId, int ItemId, string ExtraData, int priceAmount,
            bool IsGift, string GiftUser, string GiftMessage, int GiftSpriteId, int GiftLazo, int GiftColor, bool undef,
            uint Group)
        {
            if (priceAmount < 1 || priceAmount > 100)
                priceAmount = 1;
            var num = priceAmount;
            var num2 = 0;
            var limtot = 0;
            CatalogItem item;
            uint num3;
            checked
            {
                if (priceAmount >= 6)
                    num -= Convert.ToInt32(Math.Ceiling(Convert.ToDouble(priceAmount) / 6)) * 2 - 1;
                if (!Categories.Contains(PageId))
                    return;
                var catalogPage = (CatalogPage) Categories[PageId];
                if (catalogPage == null || !catalogPage.Enabled || !catalogPage.Visible || Session == null ||
                    Session.GetHabbo() == null)
                    return;
                if (catalogPage.MinRank > Session.GetHabbo().Rank || catalogPage.Layout == "sold_ltd_items")
                    return;
                item = catalogPage.GetItem(ItemId);
                if (item == null)
                    return;
                if (catalogPage.Layout == "vip_buy" || catalogPage.Layout == "club_buy" || HabboClubItems.Contains(item))
                {
                    var array = item.Name.Split('_');
                    double dayLength;
                    if (item.Name.Contains("DAY"))
                        dayLength = double.Parse(array[3]);
                    else if (item.Name.Contains("MONTH"))
                    {
                        var num4 = double.Parse(array[3]);
                        dayLength = Math.Ceiling((num4 * 31) - 0.205);
                    }
                    else if (item.Name.Contains("YEAR"))
                    {
                        var num5 = double.Parse(array[3]);
                        dayLength = (num5 * 31 * 12);
                    }
                    else
                        dayLength = 31;
                    Session.GetHabbo().GetSubscriptionManager().AddSubscription(dayLength);
                    return;
                }
                if (item.Name == "room_ad_plus_badge")
                    return;
                if (item.ClubOnly && !Session.GetHabbo().GetSubscriptionManager().HasSubscription)
                {
                    var serverMessage =
                        new ServerMessage(LibraryParser.OutgoingRequest("CatalogPurchaseNotAllowedMessageComposer"));
                    serverMessage.AppendInteger(1);
                    Session.SendMessage(serverMessage);
                    return;
                }
                if (item.GetBaseItem(item.BaseId).InteractionType == InteractionType.groupforumterminal)
                {
                    uint GroupId;
                    if (!uint.TryParse(ExtraData, out GroupId))
                    {
                        Session.SendNotif("Your group forum couldn't be created by an unknown error. Please report it.");
                        return;
                    }
                    var Grap = Azure.GetGame().GetGroupManager().GetGroup(GroupId);
                    if (Grap == null)
                    {
                        Session.SendNotif("Your group forum couldn't be created by an unknown error. Please report it");
                        return;
                    }
                    if (!Grap.HasForum && Grap.CreatorId == Session.GetHabbo().Id)
                    {
                        Grap.HasForum = true;
                        Azure.GetGame()
                            .GetClientManager()
                            .SendSuperNotif("Congratulations!", "You successfully purchased a Forum for your group.",
                                "admin", Session, string.Format("event:groupforum/{0}", Grap.Id),
                                "Enter my new Group Forum", false, false);
                        Grap.UpdateForum();
                    }
                    else if (Grap.CreatorId != Session.GetHabbo().Id && !Grap.HasForum)
                        Session.SendNotif(
                            "Uhm, looks like you're not the owner of the group. Anyway, you received a Group Forum Terminal, which would work only when the owner of the group buys a forum.");
                }
                var flag =
                    item.Items.Keys.Any(
                        current =>
                            item.GetBaseItem(current).InteractionType == InteractionType.pet1 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet2 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet3 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet4 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet5 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet6 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet7 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet8 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet9 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet10 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet11 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet12 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet13 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet14 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet15 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet16 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet17 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet18 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet19 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet20 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet21 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet22 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet23 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet24 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet25 ||
                            item.GetBaseItem(current).InteractionType == InteractionType.pet26);
                if (!flag &&
                    (item.CreditsCost * num < 0 || item.DucketsCost * num < 0 || item.BelCreditsCost * num < 0 ||
                     item.LoyaltyCost * num < 0))
                    return;
                if (item.IsLimited)
                {
                    num = 1;
                    priceAmount = 1;
                    if (item.LimitedSelled >= item.LimitedStack)
                    {
                        Session.SendMessage(
                            new ServerMessage(LibraryParser.OutgoingRequest("CatalogLimitedItemSoldOutMessageComposer")));
                        return;
                    }
                    item.LimitedSelled++;
                    using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                    {
                        queryReactor.RunFastQuery(string.Concat(new object[]
                        {
                            "UPDATE catalog_items SET limited_sells = ",
                            item.LimitedSelled,
                            " WHERE id = ",
                            item.Id
                        }));
                        num2 = item.LimitedSelled;
                        limtot = item.LimitedStack;
                    }
                }
                else if (IsGift & priceAmount > 1)
                {
                    num = 1;
                    priceAmount = 1;
                }
                num3 = 0u;
                if (Session.GetHabbo().Credits < item.CreditsCost * num)
                    return;
                if (Session.GetHabbo().ActivityPoints < item.DucketsCost * num)
                    return;
                if (Session.GetHabbo().BelCredits < item.BelCreditsCost * num)
                    return;
                if (Session.GetHabbo().BelCredits < item.LoyaltyCost * num)
                    return;
                if (item.CreditsCost > 0 && !IsGift)
                {
                    Session.GetHabbo().Credits -= item.CreditsCost * num;
                    Session.GetHabbo().UpdateCreditsBalance();
                }
                if (item.DucketsCost > 0 && !IsGift)
                {
                    Session.GetHabbo().ActivityPoints -= item.DucketsCost * num;
                    Session.GetHabbo().UpdateActivityPointsBalance();
                }
                if (item.BelCreditsCost > 0 && !IsGift)
                {
                    Session.GetHabbo().BelCredits -= item.BelCreditsCost * num;
                    Session.GetHabbo().UpdateSeasonalCurrencyBalance();
                }
                if (item.LoyaltyCost > 0 && !IsGift)
                {
                    Session.GetHabbo().BelCredits -= item.LoyaltyCost * num;
                    Session.GetHabbo().UpdateSeasonalCurrencyBalance();
                }
            }
            checked
            {
                foreach (uint current2 in item.Items.Keys)
                {
                    if (IsGift)
                    {
                        if ((DateTime.Now - Session.GetHabbo().LastGiftPurchaseTime).TotalSeconds <= 15.0)
                        {
                            Session.SendNotif(
                                "You're purchasing gifts too fast! Please wait 15 seconds, then you purchase another gift.");
                            return;
                        }
                        if (!item.GetBaseItem(current2).AllowGift)
                            return;
                        DataRow row;
                        using (var queryreactor3 = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            queryreactor3.SetQuery("SELECT id FROM users WHERE Username = @gift_user");
                            queryreactor3.AddParameter("gift_user", GiftUser);
                            row = queryreactor3.GetRow();
                        }
                        if (row == null)
                        {
                            Session.GetMessageHandler()
                                .GetResponse()
                                .Init(LibraryParser.OutgoingRequest("GiftErrorMessageComposer"));
                            Session.GetMessageHandler().GetResponse().AppendString(GiftUser);
                            Session.GetMessageHandler().SendResponse();
                            return;
                        }
                        num3 = Convert.ToUInt32(row["id"]);
                        if (num3 == 0u)
                        {
                            Session.GetMessageHandler()
                                .GetResponse()
                                .Init(LibraryParser.OutgoingRequest("GiftErrorMessageComposer"));
                            Session.GetMessageHandler().GetResponse().AppendString(GiftUser);
                            Session.GetMessageHandler().SendResponse();
                            return;
                        }
                        if (item.CreditsCost > 0 && IsGift)
                        {
                            Session.GetHabbo().Credits -= item.CreditsCost * num;
                            Session.GetHabbo().UpdateCreditsBalance();
                        }
                        if (item.DucketsCost > 0 && IsGift)
                        {
                            Session.GetHabbo().ActivityPoints -= item.DucketsCost * num;
                            Session.GetHabbo().UpdateActivityPointsBalance();
                        }
                        if (item.BelCreditsCost > 0 && IsGift)
                        {
                            Session.GetHabbo().BelCredits -= item.BelCreditsCost * num;
                            Session.GetHabbo().UpdateSeasonalCurrencyBalance();
                        }
                        if (item.LoyaltyCost > 0 && IsGift)
                        {
                            Session.GetHabbo().BelCredits -= item.LoyaltyCost * num;
                            Session.GetHabbo().UpdateSeasonalCurrencyBalance();
                        }
                    }
                    if (IsGift && item.GetBaseItem(current2).Type == 'e')
                    {
                        Session.SendNotif("You can't send effects as gifts.");
                        return;
                    }
                    if (item.Name.StartsWith("builders_club_addon_"))
                    {
                        int furniAmount = 0;
                        furniAmount = Convert.ToInt32(item.Name.Replace("builders_club_addon_", "").Replace("furnis", ""));
                        Session.GetHabbo().BuildersItemsMax += furniAmount;
                        ServerMessage update = new ServerMessage(LibraryParser.OutgoingRequest("BuildersClubMembershipMessageComposer"));
                        update.AppendInteger(Session.GetHabbo().BuildersExpire);
                        update.AppendInteger(Session.GetHabbo().BuildersItemsMax);
                        update.AppendInteger(2);
                        Session.SendMessage(update);
                        using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            queryReactor.SetQuery("UPDATE users SET builders_items_max = @max WHERE id = @userId");
                            queryReactor.AddParameter("max", Session.GetHabbo().BuildersItemsMax);
                            queryReactor.AddParameter("userId", Session.GetHabbo().Id);
                            queryReactor.RunQuery();
                        }
                        Session.SendMessage(CatalogPacket.PurchaseOk());
                        Session.SendNotif("${notification.builders_club.membership_extended.message}", "${notification.builders_club.membership_extended.title}", "builders_club_membership_extended");
                        return;
                    }
                    var text = "";
                    var interactionType = item.GetBaseItem(current2).InteractionType;
                    switch (interactionType)
                    {
                        case InteractionType.none:
                            ExtraData = "";
                            break;
                        case InteractionType.gate:
                        case InteractionType.bed:
                        case InteractionType.scoreboard:
                        case InteractionType.vendingmachine:
                        case InteractionType.alert:
                        case InteractionType.onewaygate:
                        case InteractionType.loveshuffler:
                        case InteractionType.habbowheel:
                        case InteractionType.dice:
                        case InteractionType.bottle:
                        case InteractionType.hopper:
                        case InteractionType.teleport:
                        case InteractionType.pet:
                        case InteractionType.pool:
                        case InteractionType.roller:
                        case InteractionType.fbgate:
                            goto IL_DF5;
                        case InteractionType.postit:
                            ExtraData = "FFFF33";
                            break;
                        case InteractionType.roomeffect:
                        {
                            double number = 0;
                            try
                            {
                                number = string.IsNullOrEmpty(ExtraData)
                                    ? 0.0
                                    : double.Parse(ExtraData, Azure.CultureInfo);
                            }
                            catch (Exception pException)
                            {
                                Logging.HandleException(pException,
                                    string.Format("Catalog.HandlePurchase: {0}", ExtraData));
                            }
                            ExtraData = number.ToString().Replace(',', '.');
                            break;
                        }
                        case InteractionType.dimmer:
                            ExtraData = "1,1,1,#000000,255";
                            break;
                        case InteractionType.trophy:
                            ExtraData = string.Concat(new object[]
                            {
                                Session.GetHabbo().UserName,
                                Convert.ToChar(9),
                                DateTime.Now.Day,
                                "-",
                                DateTime.Now.Month,
                                "-",
                                DateTime.Now.Year,
                                Convert.ToChar(9),
                                ExtraData
                            });
                            break;
                        case InteractionType.rentals:
                            goto IL_C41;
                        case InteractionType.pet0:
                        case InteractionType.pet1:
                        case InteractionType.pet2:
                        case InteractionType.pet3:
                        case InteractionType.pet4:
                        case InteractionType.pet5:
                        case InteractionType.pet6:
                        case InteractionType.pet7:
                        case InteractionType.pet8:
                        case InteractionType.pet9:
                        case InteractionType.pet10:
                        case InteractionType.pet11:
                        case InteractionType.pet12:
                        case InteractionType.pet13:
                        case InteractionType.pet14:
                        case InteractionType.pet15:
                        case InteractionType.pet16:
                        case InteractionType.pet17:
                        case InteractionType.pet18:
                        case InteractionType.pet19:
                        case InteractionType.pet20:
                        case InteractionType.pet21:
                        case InteractionType.pet22:
                        case InteractionType.pet23:
                        case InteractionType.pet24:
                        case InteractionType.pet25:
                        case InteractionType.pet26:
                            try
                            {
                                var data = ExtraData.Split('\n');
                                var petName = data[0];
                                var race = data[1];
                                var color = data[2];
                                int.Parse(race);
                                if (!CheckPetName(petName))
                                    return;
                                if (race.Length != 1 && race.Length != 2)
                                    return;
                                if (color.Length != 6)
                                    return;
                                Azure.GetGame()
                                    .GetAchievementManager()
                                    .ProgressUserAchievement(Session, "ACH_PetLover", 1, false);
                                break;
                            }
                            catch (Exception ex)
                            {
                                Logging.WriteLine(ex.ToString(), ConsoleColor.Violet);
                                Logging.HandleException(ex, "Catalog.HandlePurchase");
                                return;
                            }
                        default:
                            switch (interactionType)
                            {
                                case InteractionType.mannequin:
                                    ExtraData = string.Concat(new object[]
                                    {
                                        "m",
                                        Convert.ToChar(5),
                                        "ch-215-92.lg-3202-1322-73",
                                        Convert.ToChar(5),
                                        "Mannequin"
                                    });
                                    break;
                                case InteractionType.vip_gate:
                                case InteractionType.mystery_box:
                                case InteractionType.youtubetv:
                                case InteractionType.tilestackmagic:
                                case InteractionType.tent:
                                case InteractionType.bedtent:
                                    goto IL_DF5;
                                case InteractionType.badge_display:
                                    if (!Session.GetHabbo().GetBadgeComponent().HasBadge(ExtraData))
                                        ExtraData = "UMAD";
                                    break;
                                case InteractionType.fbgate:
                                    ExtraData = "hd-99999-99999.lg-270-62;hd-99999-99999.ch-630-62.lg-695-62";
                                    break;
                                case InteractionType.lovelock:
                                    ExtraData = "0";
                                    break;
                                case InteractionType.pinata:
                                case InteractionType.runwaysage:
                                case InteractionType.shower:
                                    ExtraData = "0";
                                    break;
                                case InteractionType.groupforumterminal:
                                case InteractionType.gld_item:
                                case InteractionType.gld_gate:
                                case InteractionType.poster:
                                    break;
                                case InteractionType.moplaseed:
                                    ExtraData = new Random().Next(0, 12).ToString();
                                    break;
                                case InteractionType.musicdisc:
                                    text = SongManager.GetCodeById(item.SongId);
                                    var song = SongManager.GetSongById(item.SongId);
                                    ExtraData = string.Concat(new object[]
                                    {
                                        Session.GetHabbo().UserName,
                                        '\n',
                                        DateTime.Now.Year,
                                        '\n',
                                        DateTime.Now.Month,
                                        '\n',
                                        DateTime.Now.Day,
                                        '\n',
                                        song.LengthSeconds,
                                        '\n',
                                        song.Name
                                    });
                                    break;
                                default:
                                    goto IL_DF5;
                            }
                            break;
                    }
                    IL_DFC:
                    Session.GetMessageHandler()
                        .GetResponse()
                        .Init(LibraryParser.OutgoingRequest("UpdateInventoryMessageComposer"));
                    Session.GetMessageHandler().SendResponse();
                    Session.SendMessage(CatalogPacket.PurchaseOk());
                    if (IsGift)
                    {
                        var itemBySprite = Azure.GetGame()
                            .GetItemManager()
                            .GetItemBySprite(GiftSpriteId, 's');

                        if (itemBySprite == null)
                            return;
                        var clientByUserID =
                            Azure.GetGame().GetClientManager().GetClientByUserId(num3);
                        uint num7;
                        using (var queryreactor4 = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            queryreactor4.SetQuery(string.Concat(new object[]
                            {
                                "INSERT INTO items_rooms (base_item,user_id) VALUES (",
                                itemBySprite.ItemId,
                                ", ",
                                num3,
                                ")"
                            }));
                            num7 = (uint) queryreactor4.InsertQuery();
                            queryreactor4.SetQuery(string.Concat(new object[]
                            {
                                "INSERT INTO users_gifts (gift_id,item_id,extradata,giver_name,Message,ribbon,color,gift_sprite,show_sender,rare_id) VALUES (",
                                num7,
                                ", ",
                                item.GetBaseItem(current2).ItemId,
                                ",@extradata, @name, @Message,",
                                GiftLazo,
                                ",",
                                GiftColor,
                                ",",
                                GiftSpriteId,
                                ",",
                                undef ? 1 : 0,
                                ",",
                                num2,
                                ")"
                            }));
                            queryreactor4.AddParameter("extradata", ExtraData);
                            queryreactor4.AddParameter("name", GiftUser);
                            queryreactor4.AddParameter("message", GiftMessage);
                            queryreactor4.RunQuery();
                            if (Session.GetHabbo().Id != num3)
                                queryreactor4.RunFastQuery(string.Concat(new object[]
                                {
                                    "UPDATE users_stats SET gifts_given = gifts_given + 1 WHERE id = ",
                                    Session.GetHabbo().Id,
                                    " LIMIT 1;UPDATE users_stats SET gifts_received = gifts_received + 1 WHERE id = ",
                                    num3,
                                    " LIMIT 1"
                                }));
                        }
                        if (clientByUserID.GetHabbo().Id != Session.GetHabbo().Id)
                        {
                            Azure.GetGame()
                                .GetAchievementManager()
                                .ProgressUserAchievement(Session, "ACH_GiftGiver", 1, false);
                            Azure.GetGame()
                                .GetQuestManager()
                                .ProgressUserQuest(Session, QuestType.GiftOthers, 0u);
                        }
                        if (clientByUserID != null)
                        {
                            var userItem = clientByUserID.GetHabbo()
                                .GetInventoryComponent()
                                .AddNewItem(num7, itemBySprite.ItemId, string.Concat(new object[]
                                {
                                    Session.GetHabbo().Id,
                                    (char) 9,
                                    GiftMessage,
                                    (char) 9,
                                    GiftLazo,
                                    (char) 9,
                                    GiftColor,
                                    (char) 9,
                                    ((undef) ? "1" : "0"),
                                    (char) 9,
                                    Session.GetHabbo().UserName,
                                    (char) 9,
                                    Session.GetHabbo().Look,
                                    (char) 9,
                                    item.Name
                                }), 0u, false, false, 0, 0, "");
                            if (clientByUserID.GetHabbo().Id != Session.GetHabbo().Id)
                                Azure.GetGame()
                                    .GetAchievementManager()
                                    .ProgressUserAchievement(clientByUserID, "ACH_GiftReceiver", 1, false);
                        }
                        Session.GetHabbo().LastGiftPurchaseTime = DateTime.Now;
                        continue;
                    }
                    Session.GetMessageHandler()
                        .GetResponse()
                        .Init(LibraryParser.OutgoingRequest("NewInventoryObjectMessageComposer"));
                    Session.GetMessageHandler().GetResponse().AppendInteger(1);
                    var i = 1;
                    if (item.GetBaseItem(current2).Type.ToString().ToLower().Equals("s"))
                        if (item.GetBaseItem(current2).InteractionType == InteractionType.pet1 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet2 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet3 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet4 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet5 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet6 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet7 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet8 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet9 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet10 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet11 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet12 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet13 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet14 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet15 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet16 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet17 ||
                            item.GetBaseItem(current2).InteractionType == InteractionType.pet18)
                            i = 3;
                        else
                            i = 1;
                    Session.GetMessageHandler().GetResponse().AppendInteger(i);
                    var list = DeliverItems(Session, item.GetBaseItem(current2),
                        priceAmount * item.Items[current2], ExtraData, num2, limtot, text);
                    Session.GetMessageHandler().GetResponse().AppendInteger(list.Count);
                    foreach (var current3 in list)
                        Session.GetMessageHandler().GetResponse().AppendInteger(current3.Id);
                    Session.GetMessageHandler().SendResponse();
                    Session.GetHabbo().GetInventoryComponent().UpdateItems(false);
                    if (InterractionTypes.AreFamiliar(InteractionGlobalType.Pet,
                        item.GetBaseItem(current2).InteractionType))
                        Session.SendMessage(Session.GetHabbo().GetInventoryComponent().SerializePetInventory());
                    continue;
                    IL_C41:
                    ExtraData = item.ExtraData;
                    goto IL_DFC;
                    IL_DF5:
                    ExtraData = "";
                    goto IL_DFC;
                }
                if (item.Badge.Length >= 1)
                    Session.GetHabbo().GetBadgeComponent().GiveBadge(item.Badge, true, Session, false);
            }
        }

        internal List<UserItem> DeliverItems(GameClient Session, Item Item, int Amount, string ExtraData, int limno,
            int limtot, string SongCode)
        {
            var list = new List<UserItem>();
            if (Item.InteractionType == InteractionType.postit)
                Amount = Amount * 20;
            checked
            {
                var a = Item.Type;
                if (a == 'i' || a == 's')
                {
                    var i = 0;
                    while (i < Amount)
                    {
                        var interactionType = Item.InteractionType;
                        switch (interactionType)
                        {
                            case InteractionType.dimmer:
                                goto IL_F87;
                            case InteractionType.trophy:
                            case InteractionType.bed:
                            case InteractionType.scoreboard:
                            case InteractionType.vendingmachine:
                            case InteractionType.alert:
                            case InteractionType.onewaygate:
                            case InteractionType.loveshuffler:
                            case InteractionType.habbowheel:
                            case InteractionType.dice:
                            case InteractionType.bottle:
                            case InteractionType.hopper:
                            case InteractionType.rentals:
                            case InteractionType.pet:
                            case InteractionType.pool:
                            case InteractionType.roller:
                            case InteractionType.fbgate:
                                goto IL_10C3;
                            case InteractionType.teleport:
                            {
                                var userItem = Session.GetHabbo()
                                    .GetInventoryComponent()
                                    .AddNewItem(0u, Item.ItemId, "0", 0u, true, false, 0, 0, "");
                                var id = userItem.Id;
                                var userItem2 = Session.GetHabbo()
                                    .GetInventoryComponent()
                                    .AddNewItem(0u, Item.ItemId, "0", 0u, true, false, 0, 0, "");
                                var id2 = userItem2.Id;
                                list.Add(userItem);
                                list.Add(userItem2);
                                using (
                                    var queryReactor =
                                        Azure.GetDatabaseManager().GetQueryReactor())
                                {
                                    queryReactor.RunFastQuery(string.Concat(new object[]
                                    {
                                        "INSERT INTO items_teleports (tele_one_id,tele_two_id) VALUES (",
                                        id,
                                        ",",
                                        id2,
                                        ")"
                                    }));
                                    queryReactor.RunFastQuery(string.Concat(new object[]
                                    {
                                        "INSERT INTO items_teleports (tele_one_id,tele_two_id) VALUES (",
                                        id2,
                                        ",",
                                        id,
                                        ")"
                                    }));
                                    break;
                                }
                            }
                            case InteractionType.pet0:
                            case InteractionType.pet1:
                            case InteractionType.pet2:
                            case InteractionType.pet3:
                            case InteractionType.pet4:
                            case InteractionType.pet5:
                            case InteractionType.pet6:
                            case InteractionType.pet7:
                            case InteractionType.pet8:
                            case InteractionType.pet9:
                            case InteractionType.pet10:
                            case InteractionType.pet11:
                            case InteractionType.pet12:
                            case InteractionType.pet13:
                            case InteractionType.pet14:
                            case InteractionType.pet15:
                            case InteractionType.pet16:
                            case InteractionType.pet17:
                            case InteractionType.pet18:
                            case InteractionType.pet19:
                            case InteractionType.pet21:
                            case InteractionType.pet22:
                            case InteractionType.pet23:
                            case InteractionType.pet24:
                            case InteractionType.pet25:
                            case InteractionType.pet26:
                            {
                                var petData = ExtraData.Split('\n');
                                var petId = int.Parse(Item.Name.Replace("a0 pet", ""));
                                var generatedPet = CreatePet(Session.GetHabbo().Id, petData[0], petId,
                                    petData[1], petData[2], 0);


                                Session.GetHabbo().GetInventoryComponent().AddPet(generatedPet);
                                list.Add(Session.GetHabbo()
                                    .GetInventoryComponent()
                                    .AddNewItem(0, 320, "0", 0u, true, false, 0, 0, ""));

                                break;
                            }
                            default:
                                switch (interactionType)
                                {
                                    case InteractionType.musicdisc:
                                        goto IL_1067;
                                    case InteractionType.puzzlebox:
                                        goto IL_10C3;
                                    case InteractionType.roombg:
                                        goto IL_FF7;
                                    default:
                                        switch (interactionType)
                                        {
                                            case InteractionType.gld_item:
                                            case InteractionType.gld_gate:
                                            case InteractionType.groupforumterminal:
                                                list.Add(Session.GetHabbo()
                                                    .GetInventoryComponent()
                                                    .AddNewItem(0u, Item.ItemId, "0", Convert.ToUInt32(ExtraData),
                                                        true, false, 0, 0, ""));
                                                break;
                                            default:
                                                goto IL_10C3;
                                        }
                                        break;
                                }
                                break;
                        }
                        IL_10EE:
                        i++;
                        continue;
                        IL_F87:
                        var userItem3 = Session.GetHabbo()
                            .GetInventoryComponent()
                            .AddNewItem(0u, Item.ItemId, ExtraData, 0u, true, false, 0, 0, "");
                        var id3 = userItem3.Id;
                        list.Add(userItem3);
                        using (
                            var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            queryreactor2.RunFastQuery(
                                string.Format(
                                    "INSERT INTO items_moodlight (item_id,enabled,current_preset,preset_one,preset_two,preset_three) VALUES ({0},'0',1,'#000000,255,0','#000000,255,0','#000000,255,0')",
                                    id3));
                            goto IL_10EE;
                        }
                        IL_FF7:
                        var userItem4 = Session.GetHabbo()
                            .GetInventoryComponent()
                            .AddNewItem(0u, Item.ItemId, ExtraData, 0u, true, false, 0, 0, "");
                        var id4 = userItem4.Id;
                        list.Add(userItem4);
                        using (
                            var queryreactor3 = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            queryreactor3.RunFastQuery(
                                string.Format("INSERT INTO items_toners VALUES ({0},'0',0,0,0)", id4));
                            goto IL_10EE;
                        }
                        IL_1067:
                        list.Add(Session.GetHabbo()
                            .GetInventoryComponent()
                            .AddNewItem(0u, Item.ItemId, ExtraData, 0u, true, false, 0, 0, SongCode));
                        goto IL_10EE;
                        IL_10C3:
                        list.Add(Session.GetHabbo()
                            .GetInventoryComponent()
                            .AddNewItem(0u, Item.ItemId, ExtraData, 0u, true, false, limno, limtot, ""));
                        goto IL_10EE;
                    }
                    return list;
                }
                if (a == 'e')
                {
                    for (var j = 0; j < Amount; j++)
                        Session.GetHabbo().GetAvatarEffectsInventoryComponent().AddNewEffect(Item.SpriteId, 7200);
                    return list;
                }
                if (a == 'r')
                {
                    if (Item.Name == "bot_bartender")
                    {
                        var bot = CreateBot(Session.GetHabbo().Id, "Camarera",
                            "hr-9534-39.hd-600-1.ch-819-92.lg-3058-64.sh-3064-110.wa-2005",
                            "¡Te calma la sed y sabe bailar!", "f", true);
                        Session.GetHabbo().GetInventoryComponent().AddBot(bot);
                        Session.SendMessage(Session.GetHabbo().GetInventoryComponent().SerializeBotInventory());
                    }
                    else
                    {
                        var bot2 = CreateBot(Session.GetHabbo().Id, "Robbie",
                            "hr-3020-34.hd-3091-2.ch-225-92.lg-3058-100.sh-3089-1338.ca-3084-78-108.wa-2005",
                            "Habla, anda, baila y se viste", "m", false);
                        Session.GetHabbo().GetInventoryComponent().AddBot(bot2);
                        Session.SendMessage(Session.GetHabbo().GetInventoryComponent().SerializeBotInventory());
                    }
                    return list;
                }
                return list;
            }
        }

        internal EcotronReward GetRandomEcotronReward()
        {
            var level = 1u;
            if (Azure.GetRandomNumber(1, 2000) == 2000)
                level = 5u;
            else if (Azure.GetRandomNumber(1, 200) == 200)
                level = 4u;
            else if (Azure.GetRandomNumber(1, 40) == 40)
                level = 3u;
            else if (Azure.GetRandomNumber(1, 4) == 4)
                level = 2u;
            var ecotronRewardsForLevel = GetEcotronRewardsForLevel(level);
            return
                ecotronRewardsForLevel[Azure.GetRandomNumber(0, checked(ecotronRewardsForLevel.Count - 1))];
        }

        internal List<EcotronReward> GetEcotronRewards() { return EcotronRewards; }

        internal List<int> GetEcotronRewardsLevels() { return EcotronLevels; }

        internal List<EcotronReward> GetEcotronRewardsForLevel(uint level)
        {
            return EcotronRewards.Where(current => current.RewardLevel == level).ToList();
        }
    }
}