﻿namespace Azure.HabboHotel.Navigators
{
    class PromoCat
    {
        internal int Id;
        internal string Caption;
        internal int MinRank;
        internal bool Visible;

        internal PromoCat(int id, string caption, int minRank, bool visible)
        {
            Id = id;
            Caption = caption;
            MinRank = minRank;
            Visible = visible;
        }
    }
}