using System;

namespace Azure.HabboHotel.Navigators
{
    internal enum PublicItemType
    {
        NONE,
        TAG,
        FLAT,
        PUBLIC_FLAT,
        CATEGORY
    }
}