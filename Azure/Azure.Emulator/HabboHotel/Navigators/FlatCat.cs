namespace Azure.HabboHotel.Navigators
{
    class FlatCat
    {
        internal int Id;
        internal string Caption;
        internal int MinRank;
        internal int UsersNow;

        internal FlatCat(int id, string caption, int minRank)
        {
            Id = id;
            Caption = caption;
            MinRank = minRank;
            UsersNow = 0;
        }
    }
}