﻿namespace Azure.HabboHotel.Navigators
{
    struct NaviLogs
    {
        internal int Id;
        internal string Value1;
        internal string Value2;

        internal NaviLogs(int id, string value1, string value2)
        {
            Id = id;
            Value1 = value1;
            Value2 = value2;
        }
    }
}