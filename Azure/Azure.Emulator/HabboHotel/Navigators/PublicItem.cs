using System;
using Azure.HabboHotel.Rooms;
using Azure.Messages;

namespace Azure.HabboHotel.Navigators
{
    internal class PublicItem
    {
        internal int Type;
        internal string Caption;
        internal string Image;
        internal PublicImageType ImageType;
        internal uint RoomId;
        internal int ParentId;
        internal string Description;
        internal bool Recommended;
        internal int CategoryId;
        internal PublicItemType itemType;
        internal string TagsToSearch = "";

        internal PublicItem(int mId, int mType, string mCaption, string mDescription, string mImage, PublicImageType mImageType, uint mRoomId, int mCategoryId, int mParentId, bool mRecommand, int mTypeOfData, string mTags)
        {
            this.Id = mId;
            this.Type = mType;
            this.Caption = mCaption;
            this.Description = mDescription;
            this.Image = mImage;
            this.ImageType = mImageType;
            this.RoomId = mRoomId;
            this.ParentId = mParentId;
            this.CategoryId = mCategoryId;
            this.Recommended = mRecommand;
            switch(mTypeOfData)
            {
                case 1:
                    {
                        this.itemType = PublicItemType.TAG;
                        break;
                    }
                case 2:
                    {
                        this.itemType = PublicItemType.FLAT;
                        break;
                    }
                case 3:
                    {
                        this.itemType = PublicItemType.PUBLIC_FLAT;
                        break;
                    }
                case 4:
                    {
                        this.itemType = PublicItemType.CATEGORY;
                        break;
                    }
                default:
                    {
                        this.itemType = PublicItemType.NONE;
                        break;
                    }
            }
            return;      
        }

        internal int Id { get; private set; }

        internal RoomData RoomData
        {
            get
            {
                if (this.RoomId == 0u)
                    return new RoomData();
                if (Azure.GetGame() == null)
                    throw new NullReferenceException();
                else if (Azure.GetGame().GetRoomManager() == null)
                    throw new NullReferenceException();
                else if (Azure.GetGame().GetRoomManager().GenerateRoomData(this.RoomId) == null)
                    throw new NullReferenceException();
                else
                    return Azure.GetGame().GetRoomManager().GenerateRoomData(this.RoomId);
            }
        }

        internal RoomData RoomInfo
        {
            get
            {
                RoomData result;
                try
                {
                    if (this.RoomId > 0u)
                        result = Azure.GetGame().GetRoomManager().GenerateRoomData(this.RoomId);
                    else
                        result = null;
                }
                catch
                {
                    result = null;
                }
                return result;
            }
        }

        internal void Serialize(ServerMessage Message)
        {
            try
            {
                Message.AppendInteger(this.Id);
                Message.AppendString(this.Caption);
                Message.AppendString(this.Description);
                Message.AppendInteger(this.Type);
                Message.AppendString(this.Caption);
                Message.AppendString(this.Image);
                Message.AppendInteger((this.ParentId > 0) ? this.ParentId : 0);
                Message.AppendInteger((this.RoomInfo != null) ? this.RoomInfo.UsersNow : 0);
                Message.AppendInteger((this.itemType == PublicItemType.NONE) ? 0 : ((this.itemType == PublicItemType.TAG) ? 1 : ((this.itemType == PublicItemType.FLAT) ? 2 : ((this.itemType == PublicItemType.PUBLIC_FLAT) ? 2 : ((this.itemType == PublicItemType.CATEGORY) ? 4 : 0)))));
                switch (this.itemType)
                {
                    case PublicItemType.TAG:
                        {
                            Message.AppendString(this.TagsToSearch);
                            break;
                        }
                    case PublicItemType.CATEGORY:
                        {
                            Message.AppendBool(false);
                            break;
                        }
                    case PublicItemType.FLAT:
                        {
                            this.RoomInfo.Serialize(Message, false);
                            break;
                        }
                    case PublicItemType.PUBLIC_FLAT:
                        {
                            this.RoomInfo.Serialize(Message, false);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Exception on publicitems composing: {0}", ex.ToString()));
            }
        }
		
        internal void SerializeNew(ServerMessage Message)
        {
            Message.AppendInteger(this.RoomId);
            Message.AppendInteger(12);
            Message.AppendString(this.Image);
            Message.AppendString(this.Caption);
        }
    }
}