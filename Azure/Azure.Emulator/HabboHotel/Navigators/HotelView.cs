using System;
using System.Collections.Generic;
using System.Data;
using Azure.Messages;

namespace Azure.HabboHotel.Navigators
{
    public class HotelView
    {
        internal List<SmallPromo> HotelViewPromosIndexers = new List<SmallPromo>();
        internal string FurniRewardName;
        internal int FurniRewardId;

        public HotelView()
        {
            List();
            LoadReward();
        }

        public static SmallPromo Load(int index)
        {
            DataRow row;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    "SELECT hotelview_promos.`index`,hotelview_promos.header,hotelview_promos.body,hotelview_promos.button,hotelview_promos.in_game_promo,hotelview_promos.special_action,hotelview_promos.image,hotelview_promos.enabled FROM hotelview_promos WHERE hotelview_promos.`index` = @x LIMIT 1");
                queryReactor.AddParameter("x", index);
                row = queryReactor.GetRow();
            }
            return new SmallPromo(index, (string) row[1], (string) row[2], (string) row[3], Convert.ToInt32(row[4]),
                (string) row[5], (string) row[6]);
        }

        public void RefreshPromoList()
        {
            HotelViewPromosIndexers.Clear();
            List();
            LoadReward();
        }

        internal ServerMessage SmallPromoComposer(ServerMessage message)
        {
            message.AppendInteger(HotelViewPromosIndexers.Count);
            foreach (var current in HotelViewPromosIndexers)
                current.Serialize(message);
            return message;
        }

        private void LoadReward()
        {
            DataRow row;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    "SELECT hotelview_rewards_promos.furni_id, hotelview_rewards_promos.furni_name FROM hotelview_rewards_promos WHERE hotelview_rewards_promos.enabled = 1 LIMIT 1");
                row = queryReactor.GetRow();
            }
            if (row == null)
                return;
            FurniRewardId = Convert.ToInt32(row[0]);
            FurniRewardName = Convert.ToString(row[1]);
        }

        private void List()
        {
            DataTable table;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    "SELECT * from hotelview_promos WHERE hotelview_promos.enabled = '1' ORDER BY hotelview_promos.`index` DESC");
                table = queryReactor.GetTable();
            }
            foreach (DataRow dataRow in table.Rows)
                HotelViewPromosIndexers.Add(new SmallPromo(Convert.ToInt32(dataRow[0]), (string) dataRow[1],
                    (string) dataRow[2], (string) dataRow[3], Convert.ToInt32(dataRow[4]), (string) dataRow[5],
                    (string) dataRow[6]));
        }
    }
}