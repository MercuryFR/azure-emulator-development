﻿using System;

namespace Azure.HabboHotel.Navigators
{
    struct NavigatorHeader
    {
        internal UInt32 RoomId;
        internal String Caption;
        internal String Image;

        internal NavigatorHeader(UInt32 roomId, String caption, String image)
        {
            RoomId = roomId;
            Caption = caption;
            Image = image;
        }
    }
}