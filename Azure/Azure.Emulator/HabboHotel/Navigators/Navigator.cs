using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Rooms;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Navigators
{
    class Navigator
    {
        internal HybridDictionary PrivateCategories;
        internal Dictionary<int, string> InCategories;
        internal ServerMessage NewPublicRooms;
        private readonly Dictionary<int, PublicItem> _publicItems;
        private readonly List<NavigatorHeader> _navigatorHeaders;
        internal Dictionary<int, PromoCat> PromoCategories;

        internal Navigator()
        {
            PrivateCategories = new HybridDictionary();
            InCategories = new Dictionary<int, string>();
            _publicItems = new Dictionary<int, PublicItem>();
            _navigatorHeaders = new List<NavigatorHeader>();
            PromoCategories = new Dictionary<int, PromoCat>();
        }

        internal int FlatCatsCount
        {
            get { return PrivateCategories.Count; }
        }

        public void Initialize(IQueryAdapter dbClient, out uint navLoaded)
        {
            Initialize(dbClient);
            navLoaded = (uint) _navigatorHeaders.Count;
        }

        public void Initialize(IQueryAdapter dbClient)
        {
            dbClient.SetQuery("SELECT * FROM navigator_flatcats WHERE enabled = '2'");
            var table = dbClient.GetTable();
            dbClient.SetQuery("SELECT * FROM navigator_publics");
            var table2 = dbClient.GetTable();
            dbClient.SetQuery("SELECT * FROM navigator_pubcats");
            var table3 = dbClient.GetTable();
            dbClient.SetQuery("SELECT * FROM navigator_promocats");
            var table4 = dbClient.GetTable();
            if (table4 != null)
            {
                PromoCategories.Clear();
                foreach (DataRow dataRow in table4.Rows)
                    PromoCategories.Add((int)dataRow["id"], new PromoCat((int)dataRow["id"], (string)dataRow["caption"], (int)dataRow["min_rank"], Azure.EnumToBool((string)dataRow["visible"])));
            }
            if (table != null)
            {
                PrivateCategories.Clear();
                foreach (DataRow dataRow in table.Rows)
                    PrivateCategories.Add((int) dataRow["id"],
                        new FlatCat((int) dataRow["id"], (string) dataRow["caption"], (int) dataRow["min_rank"]));
            }
            if (table2 != null)
            {
                _publicItems.Clear();
                foreach (DataRow row in table2.Rows)
                    _publicItems.Add((int)row["id"], new PublicItem((int)row["id"], 2, (string)row["caption"], "", (string)row["image"], PublicImageType.Internal, (uint)row["room_id"], 0, 0, false, 0, ""));
            }
            if (table3 != null)
            {
                InCategories.Clear();
                foreach (DataRow dataRow in table3.Rows)
                    InCategories.Add((int) dataRow["id"], (string) dataRow["caption"]);
            }
            NewPublicRooms = SerializeNewPublicRooms();
        }

        public void SerializeNavigatorPopularRoomsNews(ref ServerMessage reply, KeyValuePair<RoomData, int>[] rooms,
            int category, bool direct)
        {
            if (rooms == null || !rooms.Any())
            {
                reply.AppendInteger(0);
                return;
            }

            var roomsCategory = new List<RoomData>();
            foreach (var pair in rooms.Where(pair => pair.Key.Category.Equals(category)))
            {
                roomsCategory.Add(pair.Key);
                if (roomsCategory.Count == (direct ? 40 : 8))
                    break;
            }
            reply.AppendInteger(roomsCategory.Count);
            foreach (var data in roomsCategory)
                data.Serialize(reply, false);
        }

        internal ServerMessage SerializePromotionCategories()
        {
            var categories = new ServerMessage(LibraryParser.OutgoingRequest("CatalogPromotionGetCategoriesMessageComposer"));
            categories.AppendInteger(PromoCategories.Count); //count
            foreach (var cat in PromoCategories.Values)
            {
                categories.AppendInteger(cat.Id);
                categories.AppendString(cat.Caption);
                categories.AppendBool(cat.Visible);
            }
            return categories;
        }

        internal ServerMessage SerializeNewPublicRooms()
        {
            var message = new ServerMessage();
            message.AppendInteger(0);
            // TODO : fix that
            /*message.AppendInteger(_publicItems.Count);
            foreach (var item in _publicItems.Values)
            {
                item.RoomData.Serialize(message, false);
            }*/
            return message;
        }

        internal FlatCat GetFlatCat(int id)
        {
            return PrivateCategories.Contains(id) ? (FlatCat) PrivateCategories[id] : null;
        }

        internal ServerMessage SerializeNVRecommendRooms()
        {
            var message = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorLiftedRoomsComposer"));
            message.AppendInteger(_publicItems.Count); //count
            foreach (var item in _publicItems.Values)
                item.SerializeNew(message);
            return message;
        }

        internal ServerMessage SerializeNewFlatCategories()
        {
            var flatcat = Azure.GetGame().GetNavigator().PrivateCategories.OfType<FlatCat>().ToList();
            var message = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorNewFlatCategoriesMessageComposer"));
            message.AppendInteger(flatcat.Count());
            foreach (var cat in flatcat)
            {
                message.AppendInteger(cat.Id);
                message.AppendInteger(cat.UsersNow);
                message.AppendInteger(500);
            }
            return message;
        }

        internal ServerMessage SerializeNVFlatCategories(bool myWorld)
        {
            var message = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorMetaDataComposer"));
            message.AppendInteger(InCategories.Count);
            message.AppendString("categories");
            message.AppendInteger(1);
            if (myWorld)
            {
                message.AppendInteger(1);
                message.AppendString("myworld_view");
                message.AppendString("");
                message.AppendString("br");
                foreach (string item in InCategories.Values)
                {
                    message.AppendString(item);
                    message.AppendInteger(0);
                }
            }
            else
                foreach (string item in InCategories.Values)
                {
                    message.AppendString(item);
                    message.AppendInteger(0);
                }
            return message;
        }

        internal ServerMessage SerlializeNewNavigator(string value1, string value2, GameClient session)
        {
            var newNavigator = new ServerMessage(LibraryParser.OutgoingRequest("SearchResultSetComposer"));
            newNavigator.AppendString(value1);
            newNavigator.AppendString(value2);
            newNavigator.AppendInteger((value2.Length > 0) ? 1 : GetNewNavigatorLength(value1));
            if (value2.Length > 0)
                SearchResultList.SerializeSearches(value2, newNavigator, session);
            else
                SearchResultList.SerializeSearchResultListStatics(value1, true, newNavigator, session);
            return newNavigator;
        }

        internal void EnableNewNavigator(GameClient session)
        {
            var navigatorMetaDataParser = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorMetaDataComposer"));
            navigatorMetaDataParser.AppendInteger(4);
            navigatorMetaDataParser.AppendString("official_view");
            navigatorMetaDataParser.AppendInteger(0);
            navigatorMetaDataParser.AppendString("hotel_view");
            navigatorMetaDataParser.AppendInteger(0);
            navigatorMetaDataParser.AppendString("roomads_view");
            navigatorMetaDataParser.AppendInteger(0);
            navigatorMetaDataParser.AppendString("myworld_view");
            navigatorMetaDataParser.AppendInteger(0);
            session.SendMessage(navigatorMetaDataParser);
            var navigatorSavedSearchesParser =
                new ServerMessage(LibraryParser.OutgoingRequest("NavigatorSavedSearchesComposer"));
            navigatorSavedSearchesParser.AppendInteger(session.GetHabbo().NavigatorLogs.Count);
            foreach (var navi in session.GetHabbo().NavigatorLogs.Values)
            {
                navigatorSavedSearchesParser.AppendInteger(navi.Id);
                navigatorSavedSearchesParser.AppendString(navi.Value1);
                navigatorSavedSearchesParser.AppendString(navi.Value2);
                navigatorSavedSearchesParser.AppendString("");
            }
            session.SendMessage(navigatorSavedSearchesParser);
            var navigatorLiftedRoomsParser =
                new ServerMessage(LibraryParser.OutgoingRequest("NavigatorLiftedRoomsComposer"));
            navigatorLiftedRoomsParser.AppendInteger(_navigatorHeaders.Count);
            foreach (var navHeader in _navigatorHeaders)
            {
                navigatorLiftedRoomsParser.AppendInteger(navHeader.RoomId);
                navigatorLiftedRoomsParser.AppendInteger(0);
                navigatorLiftedRoomsParser.AppendString(navHeader.Image);
                navigatorLiftedRoomsParser.AppendString(navHeader.Caption);
            }
            session.SendMessage(navigatorLiftedRoomsParser);
            var collapsedCategoriesMessageParser = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorCategorys"));
            collapsedCategoriesMessageParser.AppendInteger(FlatCatsCount + 5);
            foreach (FlatCat flat in PrivateCategories.Values)
                collapsedCategoriesMessageParser.AppendString(string.Format("category__{0}", flat.Caption));
            collapsedCategoriesMessageParser.AppendString("recommended");
            collapsedCategoriesMessageParser.AppendString("popular");
            collapsedCategoriesMessageParser.AppendString("new_ads");
            collapsedCategoriesMessageParser.AppendString("staffpicks");
            collapsedCategoriesMessageParser.AppendString("official");
            session.SendMessage(collapsedCategoriesMessageParser);
            session.SendMessage(SerlializeNewNavigator("official", "", session));
        }

        internal ServerMessage SerializeFlatCategories(GameClient Session)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("FlatCategoriesMessageComposer"));
            serverMessage.AppendInteger(PrivateCategories.Count);
            foreach (FlatCat flatCat in PrivateCategories.Values)
            {
                serverMessage.AppendInteger(flatCat.Id);
                serverMessage.AppendString(flatCat.Caption);
                serverMessage.AppendBool(flatCat.MinRank <= Session.GetHabbo().Rank);
                serverMessage.AppendBool(false);
                serverMessage.AppendString("NONE");
                serverMessage.AppendString("");
            }
            return serverMessage;
        }

        internal Int32 GetFlatCatIdByName(string flatName)
        {
            foreach (var flat in PrivateCategories.Values.Cast<FlatCat>().Where(flat => flat.Caption == flatName))
                return flat.Id;
            return -1;
        }

        internal ServerMessage SerializePublicRooms()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("OfficialRoomsMessageComposer"));
            serverMessage.AppendInteger(_publicItems.Count);
            foreach (var current in _publicItems.Values.Where(current => current.ParentId <= 0))
            {
                current.Serialize(serverMessage);
                if (current.itemType != PublicItemType.CATEGORY)
                    continue;
                foreach (var current2 in _publicItems.Values.Where(x => x.ParentId == current.Id))
                    current2.Serialize(serverMessage);
            }
            if (_publicItems.Any())
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    serverMessage.AppendInteger(1);
                    queryReactor.SetQuery("SELECT id FROM navigator_publics WHERE recommended = '1'");
                    queryReactor.RunQuery();
                    var key = Convert.ToInt32(queryReactor.GetInteger());
                    _publicItems[key].Serialize(serverMessage);
                    goto IL_118;
                }
            serverMessage.AppendInteger(0);
            IL_118:
            serverMessage.AppendInteger(0);
            return serverMessage;
        }

        internal ServerMessage SerializeFavoriteRooms(GameClient session)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorListingsMessageComposer"));
            serverMessage.AppendInteger(6);
            serverMessage.AppendString("");
            serverMessage.AppendInteger(session.GetHabbo().FavoriteRooms.Count);
            var array = session.GetHabbo().FavoriteRooms.ToArray();
            foreach (
                var roomData in
                    array.Cast<uint>()
                        .Select(roomId => Azure.GetGame().GetRoomManager().GenerateRoomData(roomId))
                        .Where(roomData => roomData != null))
                roomData.Serialize(serverMessage, false);
            serverMessage.AppendBool(false);
            return serverMessage;
        }

        internal ServerMessage SerializeRecentRooms(GameClient session)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorListingsMessageComposer"));
            serverMessage.AppendInteger(7);
            serverMessage.AppendString("");
            serverMessage.AppendInteger(session.GetHabbo().RecentlyVisitedRooms.Count);
            foreach (
                var roomData in
                    session.GetHabbo()
                        .RecentlyVisitedRooms.Select(
                            current => Azure.GetGame().GetRoomManager().GenerateRoomData(current)))
                roomData.Serialize(serverMessage, false);
            serverMessage.AppendBool(false);
            return serverMessage;
        }

        internal ServerMessage SerializeEventListing()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorListingsMessageComposer"));
            serverMessage.AppendInteger(16);
            serverMessage.AppendString("");
            var eventRooms = Azure.GetGame().GetRoomManager().GetEventRooms();
            serverMessage.AppendInteger(eventRooms.Length);
            var array = eventRooms;
            foreach (var keyValuePair in array)
                keyValuePair.Key.Serialize(serverMessage, true);
            return serverMessage;
        }

        internal bool RoomIsPublicItem(uint roomId) { return false; }

        internal ServerMessage SerializePopularRoomTags()
        {
            var Dictionary = new Dictionary<string, int>();
            ServerMessage Result;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT tags, users_now FROM rooms_data WHERE roomtype = 'private' AND users_now > 0 ORDER BY users_now DESC LIMIT 50");
                var table = queryReactor.GetTable();
                if (table != null)
                    foreach (DataRow dataRow in table.Rows)
                    {
                        int num;
                        if (!string.IsNullOrEmpty(dataRow["users_now"].ToString()))
                            num = (int) dataRow["users_now"];
                        else
                            num = 0;
                        var array = dataRow["tags"].ToString().Split(',');
                        var list = array.ToList();
                        foreach (string current in list)
                            if (Dictionary.ContainsKey(current))
                            {
                                Dictionary<string, int> Dictionary2;
                                string key;
                                (Dictionary2 = Dictionary)[key = current] = checked(Dictionary2[key] + num);
                            }
                            else
                                Dictionary.Add(current, num);
                    }
                var list2 = new List<KeyValuePair<string, int>>(Dictionary);
                list2.Sort(
                    (firstPair, nextPair) =>
                        firstPair.Value.CompareTo(nextPair.Value));
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("PopularRoomTagsMessageComposer"));
                serverMessage.AppendInteger(list2.Count);
                foreach (var current2 in list2)
                {
                    serverMessage.AppendString(current2.Key);
                    serverMessage.AppendInteger(current2.Value);
                }
                Result = serverMessage;
            }
            return Result;
        }

        internal ServerMessage SerializeNavigator(GameClient Session, int Mode)
        {
            if (Mode >= 0)
                return SerializeActiveRooms(Mode);
            var reply = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorListingsMessageComposer"));
            switch (Mode)
            {
                case -6:
                {
                    reply.AppendInteger(14);
                    var activeGRooms = new List<RoomData>();
                    var rooms = Azure.GetGame().GetRoomManager().GetActiveRooms();
                    if (rooms != null && rooms.Any())
                    {
                        activeGRooms.AddRange(from rd in rooms where rd.Key.GroupId != 0 select rd.Key);
                        activeGRooms = activeGRooms.OrderByDescending(p => p.UsersNow).ToList();
                    }
                    SerializeNavigatorRooms(ref reply, activeGRooms);
                    return reply;
                }
                case -5:
                case -4:
                {
                    reply.AppendInteger(Mode * (-1));
                    var activeFriends =
                        Session.GetHabbo()
                            .GetMessenger()
                            .GetActiveFriendsRooms()
                            .OrderByDescending(p => p.UsersNow)
                            .ToList();
                    SerializeNavigatorRooms(ref reply, activeFriends);
                    return reply;
                }
                case -3:
                {
                    reply.AppendInteger(5);
                    SerializeNavigatorRooms(ref reply, Session.GetHabbo().UsersRooms);
                    return reply;
                }
                case -2:
                {
                    reply.AppendInteger(2);
                    try
                    {
                        var rooms = Azure.GetGame().GetRoomManager().GetVotedRooms();
                        SerializeNavigatorRooms(ref reply, rooms);
                        if (rooms != null)
                            Array.Clear(rooms, 0, rooms.Length);
                        rooms = null;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        reply.AppendString("");
                        reply.AppendInteger(0);
                    }
                    return reply;
                }
                case -1:
                {
                    reply.AppendInteger(1);
                    reply.AppendString("-1");

                    try
                    {
                        var rooms = Azure.GetGame().GetRoomManager().GetActiveRooms();
                        SerializeNavigatorPopularRooms(ref reply, rooms);
                        if (rooms != null)
                            Array.Clear(rooms, 0, rooms.Length);
                        rooms = null;
                    }
                    catch
                    {
                        reply.AppendInteger(0);
                        reply.AppendBool(false);
                    }
                    return reply;
                }
            }
            return reply;
        }

        private static Int32 GetNewNavigatorLength(string value)
        {
            switch (value)
            {
                case "official":
                    return 1;
                case "myworld_view":
                    return 5;
                case "hotel_view":
                case "roomads_view":
                    return Azure.GetGame().GetNavigator().FlatCatsCount + 1;
            }
            return 1;
        }

        private static ServerMessage SerializeActiveRooms(int category) { return null; }

        private static void SerializeNavigatorRooms(ref ServerMessage reply, ICollection<RoomData> rooms)
        {
            reply.AppendString(string.Empty);
            if (rooms == null || !rooms.Any())
            {
                reply.AppendInteger(0);
                reply.AppendBool(false);
                return;
            }
            reply.AppendInteger(rooms.Count);
            foreach (var pair in rooms)
                pair.Serialize(reply, false);
            reply.AppendBool(false);
        }

        private static void SerializeNavigatorPopularRooms(ref ServerMessage reply,
            ICollection<KeyValuePair<RoomData, int>> rooms)
        {
            if (rooms == null || !rooms.Any())
            {
                reply.AppendInteger(0);
                reply.AppendBool(false);
                return;
            }
            reply.AppendInteger(rooms.Count);
            foreach (var pair in rooms)
                pair.Key.Serialize(reply, false);
            reply.AppendBool(false);
        }

        private static void SerializeNavigatorRooms(ref ServerMessage reply,
            ICollection<KeyValuePair<RoomData, int>> rooms)
        {
            reply.AppendString(string.Empty);
            if (rooms == null || !rooms.Any())
            {
                reply.AppendInteger(0);
                reply.AppendBool(false);
                return;
            }
            reply.AppendInteger(rooms.Count);
            foreach (var pair in rooms)
                pair.Key.Serialize(reply, false);
            reply.AppendBool(false);
        }

        public static ServerMessage SerializePromoted(GameClient session, int mode)
        {
            var reply = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorListingsMessageComposer"));
            reply.AppendInteger(mode);
            reply.AppendString("");
            try
            {
                var rooms = Azure.GetGame().GetRoomManager().GetEventRooms();
                SerializeNavigatorPopularRooms(ref reply, rooms);
                if (rooms != null)
                    Array.Clear(rooms, 0, rooms.Length);
                rooms = null;
            }
            catch
            {
                reply.AppendInteger(0);
                reply.AppendBool(false);
            }
            return reply;
        }

        public static ServerMessage SerializeSearchResults(string searchQuery)
        {
            var containsOwner = false;
            var containsGroup = false;
            var originalQuery = searchQuery;
            if (searchQuery.StartsWith("owner:"))
            {
                searchQuery = searchQuery.Replace("owner:", string.Empty);
                containsOwner = true;
            }
            else if (searchQuery.StartsWith("group:"))
            {
                searchQuery = searchQuery.Replace("group:", string.Empty);
                containsGroup = true;
            }
            var rooms = new List<RoomData>();
            if (!containsOwner)
            {
                var initForeach = false;
                var activeRooms = Azure.GetGame().GetRoomManager().GetActiveRooms();
                try
                {
                    if (activeRooms != null && activeRooms.Any())
                        initForeach = true;
                }
                catch
                {
                    initForeach = false;
                }
                if (initForeach)
                    foreach (var rms in activeRooms)
 
                        if (rms.Key.Name.ToLower().Contains(searchQuery.ToLower()) && rooms.Count <= 50)
                            rooms.Add(rms.Key);
                        else
                            break;
            }
            if (rooms.Count < 50 || containsOwner || containsGroup)
            {
                DataTable dTable;
                using (var dbClient = Azure.GetDatabaseManager().GetQueryReactor())
                    if (containsOwner)
                    {
                        dbClient.SetQuery("SELECT * FROM rooms_data WHERE owner = @query AND roomtype = 'private' LIMIT 50");
                        dbClient.AddParameter("query", searchQuery);
                        dTable = dbClient.GetTable();
                    }
                    else if (containsGroup)
                    {
                        dbClient.SetQuery(
                            "SELECT * FROM rooms_data JOIN groups_data ON rooms.id = groups_data.roomid WHERE groups_data.name LIKE @query AND roomtype = 'private' LIMIT 50");
                        dbClient.AddParameter("query", "%" + searchQuery + "%");
                        dTable = dbClient.GetTable();
                    }
                    else
                    {
                        dbClient.SetQuery("SELECT * FROM rooms_data WHERE caption = @query AND roomtype = 'private' LIMIT " + (50 - rooms.Count));
                        dbClient.AddParameter("query", searchQuery);
                        dTable = dbClient.GetTable();
                    }
                if (dTable != null)
                    foreach (var rData in dTable.Rows.Cast<DataRow>().Select(row => Azure.GetGame()
                        .GetRoomManager()
                        .FetchRoomData(Convert.ToUInt32(row["id"]), row)).Where(rData => !rooms.Contains(rData)))
                        rooms.Add(rData);
            }
            var message = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorListingsMessageComposer"));
            message.AppendInteger(8);
            message.AppendString(originalQuery);
            message.AppendInteger(rooms.Count);
            foreach (var room in rooms)
                room.Serialize(message, false);
            message.AppendBool(false);
            return message;
        }
    }
}