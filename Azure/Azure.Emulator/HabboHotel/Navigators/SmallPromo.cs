using System;
using Azure.Messages;

namespace Azure.HabboHotel.Navigators
{
    public class SmallPromo
    {
        private readonly int Index;
        private readonly string Header;
        private readonly string Body;
        private readonly string Button;
        private readonly int inGamePromo;
        private readonly string SpecialAction;
        private readonly string Image;

        public SmallPromo(int index, string header, string body, string button, int inGame, string specialAction, string image)
        {
            this.Index = index;
            this.Header = header;
            this.Body = body;
            this.Button = button;
            this.inGamePromo = inGame;
            this.SpecialAction = specialAction;
            this.Image = image;
        }

        internal ServerMessage Serialize(ServerMessage Composer)
        {
            Composer.AppendInteger(this.Index);
            Composer.AppendString(this.Header);
            Composer.AppendString(this.Body);
            Composer.AppendString(this.Button);
            Composer.AppendInteger(this.inGamePromo);
            Composer.AppendString(this.SpecialAction);
            Composer.AppendString(this.Image);
            return Composer;
        }
    }
}