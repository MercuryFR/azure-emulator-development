using System;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Quests.Composer
{
    internal class QuestAbortedComposer
    {
        internal static ServerMessage Compose()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("QuestAbortedMessageComposer"));
            serverMessage.AppendBool(false);
            return serverMessage;
        }
    }
}