using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Quests.Composer;
using Azure.Messages;

namespace Azure.HabboHotel.Quests
{
    internal class QuestManager
    {
        private Dictionary<uint, Quest> quests;
        private Dictionary<string, int> questCount;

        public void Initialize(IQueryAdapter dbClient)
        {
            this.quests = new Dictionary<uint, Quest>();
            this.questCount = new Dictionary<string, int>();
            this.ReloadQuests(dbClient);
        }

        public void ReloadQuests(IQueryAdapter dbClient)
        {
            this.quests.Clear();
            dbClient.SetQuery("SELECT * FROM users_quests");
            DataTable table = dbClient.GetTable();
            foreach (DataRow dataRow in table.Rows)
            {
                uint num = Convert.ToUInt32(dataRow["id"]);
                var category = (string)dataRow["type"];
                var number = (int)dataRow["level_num"];
                var goalType = (int)dataRow["goal_type"];
                uint goalData = Convert.ToUInt32(dataRow["goal_data"]);
                var name = (string)dataRow["action"];
                var reward = (int)dataRow["pixel_reward"];
                var dataBit = (string)dataRow["data_bit"];
                int rewardType = Convert.ToInt32(dataRow["reward_type"].ToString());
                var timeUnlock = (int)dataRow["timestamp_unlock"];
                var timeLock = (int)dataRow["timestamp_lock"];
                var value = new Quest(num, category, number, (QuestType)goalType, goalData, name, reward, dataBit, rewardType, timeUnlock, timeLock);
                this.quests.Add(num, value);
                this.AddToCounter(category);
            }
        }

        public ICollection<Quest> GetQuests()
        {
            return this.quests.Values;
        }

        internal Quest GetQuest(uint Id)
        {
            Quest result = null;
            this.quests.TryGetValue(Id, out result);
            return result;
        }

        internal int GetAmountOfQuestsInCategory(string Category)
        {
            int result = 0;
            this.questCount.TryGetValue(Category, out result);
            return result;
        }

        internal void ProgressUserQuest(GameClient Session, QuestType QuestType, uint EventData = 0u)
        {
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().CurrentQuestId <= 0u)
            {
                return;
            }
            Quest quest = this.GetQuest(Session.GetHabbo().CurrentQuestId);
            if (quest == null || quest.GoalType != QuestType)
            {
                return;
            }
            int questProgress = Session.GetHabbo().GetQuestProgress(quest.Id);
            int num = questProgress;
            bool flag = false;
            checked
            {
                if (QuestType != QuestType.ExploreFindItem)
                {
                    switch (QuestType)
                    {
                        case QuestType.StandOn:
                            if (EventData != quest.GoalData)
                            {
                                return;
                            }
                            num = (int)quest.GoalData;
                            flag = true;
                            goto IL_DC;
                        case QuestType.GiveItem:
                            if (EventData != quest.GoalData)
                            {
                                return;
                            }
                            num = (int)quest.GoalData;
                            flag = true;
                            goto IL_DC;
                        case QuestType.GiveCoffee:
                        case QuestType.WaveReindeer:
                            num++;
                            if (unchecked((long)num >= (long)((ulong)quest.GoalData)))
                            {
                                flag = true;
                                goto IL_DC;
                            }
                            goto IL_DC;
                        case QuestType.XmasParty:
                            num++;
                            if (unchecked((long)num == (long)((ulong)quest.GoalData)))
                            {
                                flag = true;
                                goto IL_DC;
                            }
                            goto IL_DC;
                    }
                }
                if (EventData != quest.GoalData)
                {
                    return;
                }
                num = (int)quest.GoalData;
                flag = true;
                IL_DC:
                using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.RunFastQuery(string.Concat(new object[]
                    {
                        "UPDATE users_quests_data SET progress = ",
                        num,
                        " WHERE user_id = ",
                        Session.GetHabbo().Id,
                        " AND quest_id =  ",
                        quest.Id
                    }));
                    if (flag)
                    {
                        queryReactor.RunFastQuery(string.Format("UPDATE users_stats SET quest_id = 0 WHERE id = {0}", Session.GetHabbo().Id));
                    }
                }
                Session.GetHabbo().Quests[Session.GetHabbo().CurrentQuestId] = num;
                Session.SendMessage(QuestStartedComposer.Compose(Session, quest));
                if (flag)
                {
                    Session.GetHabbo().CurrentQuestId = 0u;
                    Session.GetHabbo().LastQuestCompleted = quest.Id;
                    Session.SendMessage(QuestCompletedComposer.Compose(Session, quest));
                    Session.GetHabbo().ActivityPoints += quest.Reward;
                    Session.GetHabbo().NotifyNewPixels(quest.Reward);
                    Session.GetHabbo().UpdateSeasonalCurrencyBalance();
                    this.GetList(Session, null);
                }
            }
        }

        internal Quest GetNextQuestInSeries(string Category, int Number)
        {
            foreach (Quest current in this.quests.Values)
            {
                if (current.Category == Category && current.Number == Number)
                {
                    return current;
                }
            }
            return null;
        }

        internal List<Quest> GetSeasonalQuests(string Season)
        {
            var list = new List<Quest>();
            foreach (Quest current in this.quests.Values)
            {
                if (current.Category.Contains(Season) && checked(current.TimeUnlock - Azure.GetUnixTimestamp()) < 0)
                {
                    list.Add(current);
                }
            }
            return list;
        }

        internal void GetList(GameClient Session, ClientMessage Message)
        {
            Session.SendMessage(QuestListComposer.Compose(Session, this.quests.Values.ToList<Quest>(), Message != null));
        }

        internal void ActivateQuest(GameClient Session, ClientMessage Message)
        {
            Quest quest = this.GetQuest(Message.GetUInteger());
            if (quest == null)
            {
                return;
            }
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "REPLACE INTO users_quests_data(user_id,quest_id) VALUES (",
                    Session.GetHabbo().Id,
                    ", ",
                    quest.Id,
                    ")"
                }));
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE users_stats SET quest_id = ",
                    quest.Id,
                    " WHERE id = ",
                    Session.GetHabbo().Id
                }));
            }
            Session.GetHabbo().CurrentQuestId = quest.Id;
            this.GetList(Session, null);
            Session.SendMessage(QuestStartedComposer.Compose(Session, quest));
        }

        internal void GetCurrentQuest(GameClient Session, ClientMessage Message)
        {
            if (!Session.GetHabbo().InRoom)
            {
                return;
            }
            Quest quest = this.GetQuest(Session.GetHabbo().LastQuestCompleted);
            Quest nextQuestInSeries = this.GetNextQuestInSeries(quest.Category, checked(quest.Number + 1));
            if (nextQuestInSeries == null)
            {
                return;
            }
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "REPLACE INTO users_quests_data(user_id,quest_id) VALUES (",
                    Session.GetHabbo().Id,
                    ", ",
                    nextQuestInSeries.Id,
                    ")"
                }));
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE users_stats SET quest_id = ",
                    nextQuestInSeries.Id,
                    " WHERE id = ",
                    Session.GetHabbo().Id
                }));
            }
            Session.GetHabbo().CurrentQuestId = nextQuestInSeries.Id;
            this.GetList(Session, null);
            Session.SendMessage(QuestStartedComposer.Compose(Session, nextQuestInSeries));
        }

        internal void CancelQuest(GameClient Session, ClientMessage Message)
        {
            Quest quest = this.GetQuest(Session.GetHabbo().CurrentQuestId);
            if (quest == null)
            {
                return;
            }
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "DELETE FROM users_quests_data WHERE user_id = ",
                    Session.GetHabbo().Id,
                    " AND quest_id = ",
                    quest.Id,
                    ";UPDATE users_stats SET quest_id=0 WHERE id=",
                    Session.GetHabbo().Id
                }));
            }
            Session.GetHabbo().CurrentQuestId = 0u;
            Session.SendMessage(QuestAbortedComposer.Compose());
            this.GetList(Session, null);
        }

        private void AddToCounter(string category)
        {
            int num = 0;
            if (this.questCount.TryGetValue(category, out num))
            {
                this.questCount[category] = checked(num + 1);
                return;
            }
            this.questCount.Add(category, 1);
        }
    }
}