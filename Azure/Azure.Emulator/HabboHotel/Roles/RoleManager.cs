using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Azure.Configuration;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.HabboHotel.Roles
{
    internal class RoleManager
    {
        private readonly Dictionary<string, uint> _rights;
        private readonly Dictionary<string, int> _subRights;
        private readonly Dictionary<string, string> _cmdRights;

        internal RoleManager()
        {
            this._rights = new Dictionary<string, uint>();
            this._subRights = new Dictionary<string, int>();
            this._cmdRights = new Dictionary<string, string>();
        }

        internal void LoadRights(IQueryAdapter dbClient)
        {
            this.ClearRights();
            dbClient.SetQuery("SELECT command,rank FROM server_fuses;");
            DataTable table = dbClient.GetTable();
            if (table != null)
            {
                foreach (DataRow dataRow in table.Rows)
                {
                    if (!this._cmdRights.ContainsKey((string)dataRow[0]))
                    {
                        this._cmdRights.Add((string)dataRow[0], (string)dataRow[1]);
                    }
                    else
                    {
                        Logging.LogException(string.Format("Duplicate Fuse Command \"{0}\" found", dataRow[0]));
                    }
                }
            }
            dbClient.SetQuery("SELECT * FROM server_fuserights");
            DataTable table2 = dbClient.GetTable();
            if (table2 == null)
            {
                return;
            }
            foreach (DataRow dataRow2 in table2.Rows)
            {
                if ((int)dataRow2[3] == 0)
                {
                    if (!this._rights.ContainsKey((string)dataRow2[0]))
                    {
                        this._rights.Add((string)dataRow2[0], Convert.ToUInt32(dataRow2[1]));
                    }
                    else
                    {
                        Logging.LogException(string.Format("Unknown Subscription Fuse \"{0}\" found", dataRow2[0]));
                    }
                }
                else
                {
                    if ((int)dataRow2[3] > 0)
                    {
                        this._subRights.Add((string)dataRow2[0], (int)dataRow2[3]);
                    }
                    else
                    {
                        Logging.LogException(string.Format("Unknown fuse type \"{0}\" found", dataRow2[3]));
                    }
                }
            }
        }

        internal bool RankGotCommand(uint rankId, string cmd)
        {
            if (!this._cmdRights.ContainsKey(cmd))
            {
                return false;
            }
            if (!this._cmdRights[cmd].Contains(";"))
            {
                return rankId >= uint.Parse(this._cmdRights[cmd]);
            }

            string[] cmdranks = this._cmdRights[cmd].Split(';');
            return cmdranks.Any(rank => rank.Contains(Convert.ToString(rankId))) || this._cmdRights[cmd].Contains(Convert.ToString(rankId));
        }

        internal bool RankHasRight(uint rankId, string fuse)
        {
            return this.ContainsRight(fuse) && rankId >= this._rights[fuse];
        }

        internal bool HasVip(int sub, string fuse)
        {
            return this._subRights.ContainsKey(fuse) && this._subRights[fuse] == sub;
        }

        internal List<string> GetRightsForRank(uint rankId)
        {
            var list = new List<string>();
            foreach (KeyValuePair<string, uint> current in this._rights.Where(current => rankId >= current.Value && !list.Contains(current.Key)))
            {
                list.Add(current.Key);
            }
            return list;
        }

        internal bool ContainsRight(string right)
        {
            return this._rights.ContainsKey(right);
        }

        internal void ClearRights()
        {
            this._rights.Clear();
            this._cmdRights.Clear();
            this._subRights.Clear();
        }
    }
}