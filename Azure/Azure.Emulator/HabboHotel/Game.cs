﻿using System;
using System.Threading;
using Azure.Configuration;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.Achievements;
using Azure.HabboHotel.Catalogs;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Groups;
using Azure.HabboHotel.Guides;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Misc;
using Azure.HabboHotel.Navigators;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.Polls;
using Azure.HabboHotel.Quests;
using Azure.HabboHotel.Roles;
using Azure.HabboHotel.RoomBots;
using Azure.HabboHotel.Rooms;
using Azure.HabboHotel.SoundMachine;
using Azure.HabboHotel.Support;
using Azure.HabboHotel.Users.Inventory;
using Azure.HabboHotel.YouTube;
using Azure.Util;

namespace Azure.HabboHotel
{
    class Game
    {
        internal static bool GameLoopEnabled = true;
        internal bool ClientManagerCycleEnded, RoomManagerCycleEnded;
        private readonly GameClientManager _clientManager;
        private readonly ModerationBanManager _banManager;
        private readonly RoleManager _roleManager;
        private readonly Catalog _catalog;
        private readonly Navigator _navigator;
        private readonly ItemManager _itemManager;
        private readonly RoomManager _roomManager;
        private readonly HotelView _hotelView;
        private readonly CoinsManager _pixelManager;
        private readonly AchievementManager _achievementManager;
        private readonly ModerationTool _moderationTool;
        private readonly BotManager _botManager;
        private readonly QuestManager _questManager;
        private readonly GroupManager _groupManager;
        private readonly RoomEvents _events;
        private readonly TalentManager _talentManager;
        private readonly VideoManager _videoManager;
        private readonly PinataHandler _pinataHandler;
        private readonly PollManager _pollManager;
        private readonly GuideManager _guideManager;
        private readonly RoomRankConfig _roomRankConfig;
        private InventoryGlobal _globalInventory;
        private Thread _gameLoop;

        internal Game(int conns)
        {
            _clientManager = new GameClientManager();
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                AbstractBar bar = new AnimatedBar();
                var wait = 15;
                var end = 5;

                uint itemsLoaded;
                uint navigatorLoaded;
                uint roomModelLoaded;
                uint achievementLoaded;
                uint pollLoaded;

                Console.ForegroundColor = System.ConsoleColor.DarkCyan;
                Console.WriteLine("≫ Loading AzureEmulator...");
                WinConsole.Color = ConsoleColor.Cyan;
                Progress(bar, wait, end, "Loading Bans...");
                _banManager = new ModerationBanManager();
                _banManager.LoadBans(queryReactor);
                Progress(bar, wait, end, "Loading Roles...");
                _roleManager = new RoleManager();
                _roleManager.LoadRights(queryReactor);
                Progress(bar, wait, end, "Loading Navigator...");
                _navigator = new Navigator();
                _navigator.Initialize(queryReactor, out navigatorLoaded);
                Progress(bar, wait, end, "Loading Items...");
                _itemManager = new ItemManager();
                _itemManager.LoadItems(queryReactor, out itemsLoaded);
                Progress(bar, wait, end, "Loading Catalog...");
                _catalog = new Catalog();
                Progress(bar, wait, end, "Loading Rooms...");
                _roomManager = new RoomManager();
                Progress(bar, wait, end, "Loading Groups...");
                _groupManager = new GroupManager();
                GetGroupManager().InitGroups();
                _roomManager.LoadModels(queryReactor, out roomModelLoaded);
                _globalInventory = new InventoryGlobal();
                Progress(bar, wait, end, "Loading PixelManager...");
                _pixelManager = new CoinsManager();
                Progress(bar, wait, end, "Loading HotelView...");
                _hotelView = new HotelView();
                Progress(bar, wait, end, "Loading ModerationTool...");
                _moderationTool = new ModerationTool();
                _moderationTool.LoadMessagePresets(queryReactor);
                _moderationTool.LoadPendingTickets(queryReactor);
                Progress(bar, wait, end, "Loading Bots...");
                _botManager = new BotManager();
                Progress(bar, wait, end, "Loading Quests...");
                _questManager = new QuestManager();
                _questManager.Initialize(queryReactor);
                Progress(bar, wait, end, "Loading Events...");
                _events = new RoomEvents();
                Progress(bar, wait, end, "Loading Talents...");
                _roomRankConfig = new RoomRankConfig();
                _roomRankConfig.Initialize();
                _talentManager = new TalentManager();
                _talentManager.Initialize(queryReactor);
                Progress(bar, wait, end, "Loading Videos...");
                _videoManager = new VideoManager();
                //this.SnowStormManager = new SnowStormManager();
                Progress(bar, wait, end, "Loading Pinata...");
                _pinataHandler = new PinataHandler();
                _pinataHandler.Initialize(queryReactor);
                Progress(bar, wait, end, "Loading Polls...");
                _pollManager = new PollManager();
                _pollManager.Init(queryReactor, out pollLoaded);
                Progress(bar, wait, end, "Loading Achievements...");
                _achievementManager = new AchievementManager(queryReactor, out achievementLoaded);
                _guideManager = new GuideManager();
                //Progress(bar, wait, end, "Loading AntiMutant...");
                //this.AntiMutant = new AntiMutant();
                Console.Write("\r".PadLeft(Console.WindowWidth - Console.CursorLeft - 1));
                WinConsole.Color = ConsoleColor.Yellow;
                Console.WriteLine("≫ Yeap, Now Load Definitions...");
                Console.WriteLine();
                WinConsole.Color = ConsoleColor.Cyan;
            }
        }

        internal bool GameLoopEnabledExt
        {
            get { return GameLoopEnabled; }
        }

        internal bool GameLoopActiveExt { get; private set; }

        internal int GameLoopSleepTimeExt
        {
            get { return 25; }
        }

        public static void Progress(AbstractBar bar, int wait, int end, string message)
        {
            bar.PrintMessage(message);
            for (var cont = 0; cont < end; cont++)
            {
                bar.Step();
                Thread.Sleep(wait);
            }
        }

        internal static void DatabaseCleanup(IQueryAdapter dbClient)
        {
            dbClient.RunFastQuery("UPDATE users SET online = '0' WHERE online <> '0'");
            dbClient.RunFastQuery("UPDATE rooms_data SET users_now = 0 WHERE users_now <> 0");
            dbClient.RunFastQuery(
                string.Format(
                    "UPDATE server_status SET status = 1, users_online = 0, rooms_loaded = 0, server_ver = 'Mercury Emulator', stamp = '{0}' ",
                    Azure.GetUnixTimestamp()));
        }

        /*internal AntiMutant GetAntiMutant()
        {
        //return this.AntiMutant;
        }*/
        internal RoomRankConfig GetRoomRankConfig() { return _roomRankConfig; }

        internal GameClientManager GetClientManager() { return _clientManager; }

        internal ModerationBanManager GetBanManager() { return _banManager; }

        internal RoleManager GetRoleManager() { return _roleManager; }

        internal Catalog GetCatalog() { return _catalog; }

        internal VideoManager GetVideoManager() { return _videoManager; }

        internal RoomEvents GetRoomEvents() { return _events; }

        internal GuideManager GetGuideManager() { return _guideManager; }

        internal Navigator GetNavigator() { return _navigator; }

        internal ItemManager GetItemManager() { return _itemManager; }

        internal RoomManager GetRoomManager() { return _roomManager; }

        internal CoinsManager GetPixelManager() { return _pixelManager; }

        internal HotelView GetHotelView() { return _hotelView; }

        internal AchievementManager GetAchievementManager() { return _achievementManager; }

        internal ModerationTool GetModerationTool() { return _moderationTool; }

        internal BotManager GetBotManager() { return _botManager; }

        internal InventoryGlobal GetInventory() { return _globalInventory; }

        internal QuestManager GetQuestManager() { return _questManager; }

        internal GroupManager GetGroupManager() { return _groupManager; }

        internal TalentManager GetTalentManager() { return _talentManager; }

        internal PinataHandler GetPinataHandler() { return _pinataHandler; }

        internal PollManager GetPollManager() { return _pollManager; }

        internal void ContinueLoading()
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                uint catalogPageLoaded;
                PetRace.Init(queryReactor);
                _catalog.Initialize(queryReactor, out catalogPageLoaded);
                BlockAds.Load(queryReactor);
                SongManager.Initialize();
                LowPriorityWorker.Init(queryReactor);
                _roomManager.InitVotedRooms(queryReactor);
            }
            StartGameLoop();
            _pixelManager.StartTimer();
        }

        internal void StartGameLoop()
        {
            GameLoopActiveExt = true;
            _gameLoop = new Thread(MainGameLoop);
            _gameLoop.Start();
        }

        internal void StopGameLoop()
        {
            GameLoopActiveExt = false;
            while (!RoomManagerCycleEnded || !ClientManagerCycleEnded)
                Thread.Sleep(25);
        }

        internal void VideoGo()
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                uint videoPlaylistLoaded = 0;
                _videoManager.Load(queryReactor, out videoPlaylistLoaded);
            }
        }

        internal void Destroy()
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                DatabaseCleanup(queryReactor);
            GetClientManager();
            Console.WriteLine("≫ Habbo Hotel was destroyed.");
        }

        internal void Reloaditems()
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                _itemManager.LoadItems(queryReactor);
                _globalInventory = new InventoryGlobal();
            }
        }

        private void MainGameLoop()
        {
            LowPriorityWorker.StartProcessing();

            while (GameLoopActiveExt)
            {
                if (GameLoopEnabled)
                    try
                    {
                        RoomManagerCycleEnded = false;
                        ClientManagerCycleEnded = false;
                        _roomManager.OnCycle();
                        _clientManager.OnCycle();
                    }
                    catch (Exception ex)
                    {
                        Logging.LogCriticalException(string.Format("Exception in Game Loop!: {0}", ex));
                    }
                Thread.Sleep(25);
            }
        }
    }
}