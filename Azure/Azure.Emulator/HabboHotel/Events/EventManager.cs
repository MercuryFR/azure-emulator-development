using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Rooms;

namespace Azure.HabboHotel.Events
{
    internal class EventManager
    {
        private readonly Dictionary<RoomData, int> _events;
        private readonly Queue _addQueue;
        private readonly Queue _removeQueue;
        private readonly Queue _updateQueue;
        private readonly Dictionary<int, EventCategory> _eventCategories;
        private IOrderedEnumerable<KeyValuePair<RoomData, int>> _orderedEventRooms;

        public EventManager()
        {
            this._eventCategories = new Dictionary<int, EventCategory>();
            this._events = new Dictionary<RoomData, int>();
            this._orderedEventRooms =
                                     from t in this._events
                                     orderby t.Value descending
                                     select t;
            this._addQueue = new Queue();
            this._removeQueue = new Queue();
            this._updateQueue = new Queue();
            checked
            {
                for (int i = 0; i < 30; i++)
                    this._eventCategories.Add(i, new EventCategory(i));
            }
        }

        internal KeyValuePair<RoomData, int>[] GetRooms()
        {
            return this._orderedEventRooms.ToArray();
        }

        internal void OnCycle()
        {
            this.WorkRemoveQueue();
            this.WorkAddQueue();
            this.WorkUpdate();
            this.SortCollection();
            foreach (EventCategory current in this._eventCategories.Values)
                current.OnCycle();
        }

        internal void QueueAddEvent(RoomData data, int roomEventCategory)
        {
            lock (this._addQueue.SyncRoot)
                this._addQueue.Enqueue(data);
            this._eventCategories[roomEventCategory].QueueAddEvent(data);
        }

        internal void QueueRemoveEvent(RoomData data, int roomEventCategory)
        {
            lock (this._removeQueue.SyncRoot)
                this._removeQueue.Enqueue(data);
            this._eventCategories[roomEventCategory].QueueRemoveEvent(data);
        }

        internal void QueueUpdateEvent(RoomData data, int roomEventCategory)
        {
            lock (this._updateQueue.SyncRoot)
                this._updateQueue.Enqueue(data);
            this._eventCategories[roomEventCategory].QueueUpdateEvent(data);
        }

        private void SortCollection()
        {
            this._orderedEventRooms =
                                     from t in this._events.Take(40)
                                     orderby t.Value descending
                                     select t;
        }

        private void WorkAddQueue()
        {
            if (this._addQueue.Count <= 0)
                return;
            lock (this._addQueue.SyncRoot)
            {
                while (this._addQueue.Count > 0)
                {
                    var roomData = (RoomData)this._addQueue.Dequeue();
                    if (!this._events.ContainsKey(roomData))
                        this._events.Add(roomData, roomData.UsersNow);
                }
            }
        }

        private void WorkRemoveQueue()
        {
            if (this._removeQueue.Count <= 0)
                return;
            lock (this._removeQueue.SyncRoot)
            {
                while (this._removeQueue.Count > 0)
                {
                    var key = (RoomData)this._removeQueue.Dequeue();
                    this._events.Remove(key);
                }
            }
        }

        private void WorkUpdate()
        {
            if (this._removeQueue.Count <= 0)
                return;
            lock (this._removeQueue.SyncRoot)
                while (this._removeQueue.Count > 0)
                {
                    var roomData = (RoomData)this._updateQueue.Dequeue();
                    if (this._events.ContainsKey(roomData))
                        this._events[roomData] = roomData.UsersNow;
                }
        }
    }
}