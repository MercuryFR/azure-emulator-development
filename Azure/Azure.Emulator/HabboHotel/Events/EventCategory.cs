using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Rooms;

namespace Azure.HabboHotel.Events
{
    internal class EventCategory
    {
        private readonly int _categoryId;
        private readonly Dictionary<RoomData, int> _events;
        private readonly Queue _addQueue;
        private readonly Queue _removeQueue;
        private readonly Queue _updateQueue;
        private IOrderedEnumerable<KeyValuePair<RoomData, int>> _orderedEventRooms;

        internal EventCategory(int categoryId)
        {
            this._categoryId = categoryId;
            this._events = new Dictionary<RoomData, int>();
            this._orderedEventRooms = from t in this._events orderby t.Value descending select t;
            this._addQueue = new Queue();
            this._removeQueue = new Queue();
            this._updateQueue = new Queue();
        }

        internal KeyValuePair<RoomData, int>[] GetActiveRooms()
        {
            return this._orderedEventRooms.ToArray();
        }

        internal void OnCycle()
        {
            this.WorkRemoveQueue();
            this.WorkAddQueue();
            this.WorkUpdate();
            this.SortCollection();
        }

        internal void QueueAddEvent(RoomData data)
        {
            lock (this._addQueue.SyncRoot)
                this._addQueue.Enqueue(data);
        }

        internal void QueueRemoveEvent(RoomData data)
        {
            lock (this._removeQueue.SyncRoot)
                this._removeQueue.Enqueue(data);
        }

        internal void QueueUpdateEvent(RoomData data)
        {
            lock (this._updateQueue.SyncRoot)
                this._updateQueue.Enqueue(data);
        }

        private void SortCollection()
        {
            this._orderedEventRooms =
                                     from t in this._events.Take(40)
                                     orderby t.Value descending
                                     select t;
        }

        private void WorkAddQueue()
        {
            if (this._addQueue == null || this._addQueue.Count <= 0)
                return;
            lock (this._addQueue.SyncRoot)
            {
                while (this._addQueue.Count > 0)
                {
                    var roomData = (RoomData)this._addQueue.Dequeue();
                    if (!this._events.ContainsKey(roomData))
                        this._events.Add(roomData, roomData.UsersNow);
                }
            }
        }

        private void WorkRemoveQueue()
        {
            if (this._removeQueue == null || this._removeQueue.Count <= 0)
                return;
            lock (this._removeQueue.SyncRoot)
            {
                while (this._removeQueue.Count > 0)
                {
                    var key = (RoomData)this._removeQueue.Dequeue();
                    this._events.Remove(key);
                }
            }
        }

        private void WorkUpdate()
        {
            if (this._removeQueue == null || this._removeQueue.Count <= 0)
                return;
            lock (this._removeQueue.SyncRoot)
            {
                while (this._removeQueue.Count > 0)
                {
                    var roomData = (RoomData)this._updateQueue.Dequeue();
                    if (!this._events.ContainsKey(roomData))
                        this._events.Add(roomData, roomData.UsersNow);
                    else
                        this._events[roomData] = roomData.UsersNow;
                }
            }
        }
    }
}