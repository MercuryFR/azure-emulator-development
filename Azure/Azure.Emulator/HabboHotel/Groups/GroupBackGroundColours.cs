namespace Azure.HabboHotel.Groups
{
    struct GroupBackGroundColours
    {
        internal int Id;
        internal string Colour;

        internal GroupBackGroundColours(int id, string colour)
        {
            Id = id;
            Colour = colour;
        }
    }
}