namespace Azure.HabboHotel.Groups
{
    class GroupUser
    {
        internal uint Id;
        internal int Rank;
        internal uint GroupId;

        internal GroupUser(uint id, uint groupId, int rank)
        {
            Id = id;
            GroupId = groupId;
            Rank = rank;
        }
    }
}