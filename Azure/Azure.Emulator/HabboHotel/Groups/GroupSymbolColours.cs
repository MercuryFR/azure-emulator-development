namespace Azure.HabboHotel.Groups
{
    struct GroupSymbolColours
    {
        internal int Id;
        internal string Colour;

        internal GroupSymbolColours(int id, string colour)
        {
            Id = id;
            Colour = colour;
        }
    }
}