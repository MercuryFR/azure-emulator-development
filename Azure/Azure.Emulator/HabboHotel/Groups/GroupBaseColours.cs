namespace Azure.HabboHotel.Groups
{
    struct GroupBaseColours
    {
        internal int Id;
        internal string Colour;

        internal GroupBaseColours(int id, string colour)
        {
            Id = id;
            Colour = colour;
        }
    }
}