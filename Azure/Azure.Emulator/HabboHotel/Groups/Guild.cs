using System;
using System.Collections.Generic;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Groups
{
    class Guild
    {
        internal uint Id;
        internal string Name;
        internal string Description;
        internal uint RoomId;
        internal string Badge;
        internal uint State;
        internal uint AdminOnlyDeco;
        internal int CreateTime;
        internal uint CreatorId;
        internal int Colour1;
        internal int Colour2;
        internal Dictionary<uint, GroupUser> Members;
        internal Dictionary<uint, GroupUser> Admins;
        internal List<uint> Requests;
        internal bool HasForum;
        internal string ForumName;
        internal string ForumDescription;
        internal uint ForumMessagesCount;
        internal double ForumScore;
        internal uint ForumLastPosterId;
        internal string ForumLastPosterName;
        internal int ForumLastPosterTimestamp;

        internal Guild(uint id, string name, string desc, uint roomId, string badge, int create, uint creator,
            int colour1, int colour2, Dictionary<uint, GroupUser> members, List<uint> requests,
            Dictionary<uint, GroupUser> admins, uint state, uint adminOnlyDeco, bool hasForum, string forumName,
            string forumDescription, uint forumMessagesCount, double forumScore, uint forumLastPosterId,
            string forumLastPosterName, int forumLastPosterTimestamp)
        {
            Id = id;
            Name = name;
            Description = desc;
            RoomId = roomId;
            Badge = badge;
            CreateTime = create;
            CreatorId = creator;
            Colour1 = ((colour1 == 0) ? 1 : colour1);
            Colour2 = ((colour2 == 0) ? 1 : colour2);
            Members = members;
            Requests = requests;
            Admins = admins;
            State = state;
            AdminOnlyDeco = adminOnlyDeco;
            HasForum = hasForum;
            ForumName = forumName;
            ForumDescription = forumDescription;
            ForumMessagesCount = forumMessagesCount;
            ForumScore = forumScore;
            ForumLastPosterId = forumLastPosterId;
            ForumLastPosterName = forumLastPosterName;
            ForumLastPosterTimestamp = forumLastPosterTimestamp;
        }

        internal int ForumLastPostTime
        {
            get { return (Azure.GetUnixTimestamp() - ForumLastPosterTimestamp); }
        }

        internal ServerMessage ForumDataMessage(uint requesterId)
        {
            var message = new ServerMessage(LibraryParser.OutgoingRequest("GroupForumDataMessageComposer"));
            message.AppendInteger(Id);
            message.AppendString(Name);
            message.AppendString(Description);
            message.AppendString(Badge);
            message.AppendInteger(0);
            message.AppendInteger(0);
            message.AppendInteger(ForumMessagesCount);
            message.AppendInteger(0);
            message.AppendInteger(0);
            message.AppendInteger(ForumLastPosterId);
            message.AppendString(ForumLastPosterName);
            message.AppendInteger(ForumLastPostTime);
            message.AppendInteger(0);
            message.AppendInteger(1);
            message.AppendInteger(1);
            message.AppendInteger(2);
            message.AppendString("");
            message.AppendString((Members.ContainsKey(requesterId) ? "" : "not_member"));
            message.AppendString((Members.ContainsKey(requesterId) ? "" : "not_member"));
            message.AppendString((Admins.ContainsKey(requesterId) ? "" : "not_admin"));
            message.AppendString("");
            message.AppendBool(false);
            message.AppendBool(false);
            return message;
        }

        internal void SerializeForumRoot(ServerMessage message)
        {
            message.AppendInteger(Id);
            message.AppendString(Name);
            message.AppendString("");
            message.AppendString(Badge);
            message.AppendInteger(0);
            message.AppendInteger((int) Math.Round(ForumScore));
            message.AppendInteger(ForumMessagesCount);
            message.AppendInteger(0);
            message.AppendInteger(0);
            message.AppendInteger(ForumLastPosterId);
            message.AppendString(ForumLastPosterName);
            message.AppendInteger(ForumLastPostTime);
        }

        internal void UpdateForum()
        {
            if (!HasForum)
                return;
            using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
            {
                adapter.SetQuery(
                    string.Format(
                        "UPDATE groups_data SET has_forum = '1', forum_name = @name , forum_description = @desc , forum_messages_count = @msgcount , forum_score = @score , forum_lastposter_id = @lastposterid , forum_lastposter_name = @lastpostername , forum_lastposter_timestamp = @lasttimestamp WHERE id ={0}",
                        Id));
                adapter.AddParameter("name", ForumName);
                adapter.AddParameter("desc", ForumDescription);
                adapter.AddParameter("msgcount", ForumMessagesCount);
                adapter.AddParameter("score", ForumScore.ToString());
                adapter.AddParameter("lastposterid", ForumLastPosterId);
                adapter.AddParameter("lastpostername", ForumLastPosterName);
                adapter.AddParameter("lasttimestamp", ForumLastPosterTimestamp);
                adapter.RunQuery();
            }
        }
    }
}