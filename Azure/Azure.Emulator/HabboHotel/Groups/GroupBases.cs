namespace Azure.HabboHotel.Groups
{
    struct GroupBases
    {
        internal int Id;
        internal string Value1;
        internal string Value2;

        internal GroupBases(int id, string value1, string value2)
        {
            Id = id;
            Value1 = value1;
            Value2 = value2;
        }
    }
}