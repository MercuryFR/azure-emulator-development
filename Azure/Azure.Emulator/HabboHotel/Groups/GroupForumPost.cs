﻿using System.Data;

namespace Azure.HabboHotel.Groups
{
    class GroupForumPost
    {
        internal uint Id;
        internal uint ParentId;
        internal uint GroupId;
        internal int Timestamp;
        internal bool Pinned;
        internal bool Locked;
        internal bool Hidden;
        internal uint PosterId;
        internal string PosterName;
        internal string PosterLook;
        internal string Subject;
        internal string PostContent;
        internal int MessageCount;
        internal string Hider;

        internal GroupForumPost(DataRow row)
        {
            Id = uint.Parse(row["id"].ToString());
            ParentId = uint.Parse(row["parent_id"].ToString());
            GroupId = uint.Parse(row["group_id"].ToString());
            Timestamp = int.Parse(row["timestamp"].ToString());
            Pinned = row["pinned"].ToString() == "1";
            Locked = row["locked"].ToString() == "1";
            Hidden = row["hidden"].ToString() == "1";
            PosterId = uint.Parse(row["poster_id"].ToString());
            PosterName = row["poster_name"].ToString();
            PosterLook = row["poster_look"].ToString();
            Subject = row["subject"].ToString();
            PostContent = row["post_content"].ToString();
            Hider = row["post_hider"].ToString();
            MessageCount = 0;
            if (ParentId == 0)
                MessageCount = Azure.GetGame().GetGroupManager().GetMessageCountForThread(Id);
        }
    }
}