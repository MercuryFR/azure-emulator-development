using System.Collections.Generic;

namespace Azure.HabboHotel.Achievements
{
    class Achievement
    {
        internal readonly uint Id;
        internal readonly string GroupName;
        internal readonly string Category;
        internal readonly Dictionary<int, AchievementLevel> Levels;

        public Achievement(uint id, string groupName, string category)
        {
            Id = id;
            GroupName = groupName;
            Category = category;
            Levels = new Dictionary<int, AchievementLevel>();
        }

        public void AddLevel(AchievementLevel level) { Levels.Add(level.Level, level); }
    }
}