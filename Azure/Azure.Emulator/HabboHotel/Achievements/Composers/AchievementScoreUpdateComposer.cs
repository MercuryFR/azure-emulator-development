using System;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Achievements.Composer
{
    internal class AchievementScoreUpdateComposer
    {
        internal static ServerMessage Compose(int Score)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("AchievementPointsMessageComposer"));
            serverMessage.AppendInteger(Score);
            return serverMessage;
        }
    }
}