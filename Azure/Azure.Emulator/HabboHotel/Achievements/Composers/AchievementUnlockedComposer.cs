using System;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Achievements.Composer
{
    internal class AchievementUnlockedComposer
    {
        internal static ServerMessage Compose(Achievement Achievement, int Level, int PointReward, int PixelReward)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("UnlockAchievementMessageComposer"));
            serverMessage.AppendInteger(Achievement.Id);
            serverMessage.AppendInteger(Level);
            serverMessage.AppendInteger(144);
            serverMessage.AppendString(string.Format("{0}{1}", Achievement.GroupName, Level));
            serverMessage.AppendInteger(PointReward);
            serverMessage.AppendInteger(PixelReward);
            serverMessage.AppendInteger(0);
            serverMessage.AppendInteger(10);
            serverMessage.AppendInteger(21);
            serverMessage.AppendString(Level > 1 ? string.Format("{0}{1}", Achievement.GroupName, checked(Level - 1)) : string.Empty);
            serverMessage.AppendString(Achievement.Category);
            serverMessage.AppendBool(true);
            return serverMessage;
        }
    }
}