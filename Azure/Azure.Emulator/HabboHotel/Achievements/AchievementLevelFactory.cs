using System;
using System.Collections.Generic;
using System.Data;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.HabboHotel.Achievements
{
    internal class AchievementLevelFactory
    {
        internal static void GetAchievementLevels(out Dictionary<string, Achievement> achievements, IQueryAdapter dbClient)
        {
            achievements = new Dictionary<string, Achievement>();
            dbClient.SetQuery("SELECT * FROM achievements_data");
            DataTable table = dbClient.GetTable();
            foreach (DataRow dataRow in table.Rows)
            {
                uint id = Convert.ToUInt32(dataRow["id"]);
                var category = (string)dataRow["category"];
                var text = (string)dataRow["group_name"];
                var level = (int)dataRow["level"];
                var rewardPixels = (int)dataRow["reward_pixels"];
                var rewardPoints = (int)dataRow["reward_points"];
                var requirement = (int)dataRow["progress_needed"];
                var level2 = new AchievementLevel(level, rewardPixels, rewardPoints, requirement);
                if (!achievements.ContainsKey(text))
                {
                    var achievement = new Achievement(id, text, category);
                    achievement.AddLevel(level2);
                    achievements.Add(text, achievement);
                }
                else
                    achievements[text].AddLevel(level2);
            }
        }
    }
}