namespace Azure.HabboHotel.Pathfinding
{
    static class Rotation
    {
        internal static int Calculate(int x1, int y1, int x2, int y2)
        {
            var result = 0;
            if (x1 > x2 && y1 > y2)
                result = 7;
            else if (x1 < x2 && y1 < y2)
                result = 3;
            else if (x1 > x2 && y1 < y2)
                result = 5;
            else if (x1 < x2 && y1 > y2)
                result = 1;
            else if (x1 > x2)
                result = 6;
            else if (x1 < x2)
                result = 2;
            else if (y1 < y2)
                result = 4;
            else if (y1 > y2)
                result = 0;
            return result;
        }

        internal static int Calculate(int x1, int y1, int x2, int y2, bool moonwalk)
        {
            var num = Calculate(x1, y1, x2, y2);
            return !moonwalk ? num : RotationIverse(num);
        }

        internal static int RotationIverse(int rot)
        {
            checked
            {
                if (rot > 3)
                    rot -= 4;
                else
                    rot += 4;
                return rot;
            }
        }
    }
}