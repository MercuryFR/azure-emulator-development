using System;
using System.Collections.Generic;

namespace Azure.HabboHotel.PathFinding
{
    class MinHeap<T> where T : IComparable<T>
    {
        private int _capacity;
        private T _temp;
        private T _mHeap;
        private T[] _array;
        private T[] _tempArray;

        public MinHeap() : this(16) { }

        public MinHeap(int capacity)
        {
            Count = 0;
            _capacity = capacity;
            _array = new T[capacity];
        }

        public int Count { get; private set; }

        public void BuildHead()
        {
            checked
            {
                for (var i = Count - 1 >> 1; i >= 0; i--)
                    MinHeapify(i);
            }
        }

        public void Add(T item)
        {
            checked
            {
                Count++;
                if (Count > _capacity)
                    DoubleArray();
                _array[Count - 1] = item;
                var num = Count - 1;
                var num2 = num - 1 >> 1;
                while (num > 0 && _array[num2].CompareTo(_array[num]) > 0)
                {
                    _temp = _array[num];
                    _array[num] = _array[num2];
                    _array[num2] = _temp;
                    num = num2;
                    num2 = num - 1 >> 1;
                }
            }
        }

        public T Peek()
        {
            if (Count == 0)
                throw new InvalidOperationException("Heap is empty");
            return _array[0];
        }

        public T ExtractFirst()
        {
            if (Count == 0)
                throw new InvalidOperationException("Heap is empty");
            _temp = _array[0];
            checked
            {
                _array[0] = _array[Count - 1];
                Count--;
                MinHeapify(0);
                return _temp;
            }
        }

        private static void CopyArray(IList<T> source, IList<T> destination)
        {
            checked
            {
                for (var i = 0; i < source.Count; i++)
                    destination[i] = source[i];
            }
        }

        private void DoubleArray()
        {
            _capacity <<= 1;
            _tempArray = new T[_capacity];
            CopyArray(_array, _tempArray);
            _array = _tempArray;
        }

        private void MinHeapify(int position)
        {
            checked
            {
                while (true)
                {
                    var num = (position << 1) + 1;
                    var num2 = num + 1;
                    int num3;
                    if (num < Count && _array[num].CompareTo(_array[position]) < 0)
                        num3 = num;
                    else
                        num3 = position;
                    if (num2 < Count && _array[num2].CompareTo(_array[num3]) < 0)
                        num3 = num2;
                    if (num3 == position)
                        break;
                    _mHeap = _array[position];
                    _array[position] = _array[num3];
                    _array[num3] = _mHeap;
                    position = num3;
                }
            }
        }
    }
}