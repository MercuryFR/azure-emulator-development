using System;

namespace Azure.HabboHotel.Pathfinding
{
    struct ThreeDCoord : IEquatable<ThreeDCoord>
    {
        internal readonly int X;
        internal readonly int Y;
        internal readonly int Z;

        internal ThreeDCoord(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public bool Equals(ThreeDCoord comparedCoord)
        {
            return X == comparedCoord.X && Y == comparedCoord.Y && Z == comparedCoord.Z;
        }

        public static bool operator ==(ThreeDCoord a, ThreeDCoord b)
        {
            return a.X == b.X && a.Y == b.Y && a.Z == b.Z;
        }

        public static bool operator !=(ThreeDCoord a, ThreeDCoord b) { return !(a == b); }

        public override int GetHashCode() { return X ^ Y ^ Z; }

        public override bool Equals(object obj)
        {
            return obj != null && base.GetHashCode().Equals(obj.GetHashCode());
        }
    }
}