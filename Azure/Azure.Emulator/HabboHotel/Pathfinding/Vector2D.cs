namespace Azure.HabboHotel.PathFinding
{
    class Vector2D
    {
        public static Vector2D Zero = new Vector2D(0, 0);

        public Vector2D() { }

        public Vector2D(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }

        public int Y { get; set; }

        public int GetDistanceSquared(Vector2D point)
        {
            checked
            {
                var num = X - point.X;
                var num2 = Y - point.Y;
                return num * num + num2 * num2;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector2D))
                return false;
            var vector2D = (Vector2D) obj;
            return vector2D.X == X && vector2D.Y == Y;
        }

        public override int GetHashCode() { return string.Format("{0} {1}", X, Y).GetHashCode(); }

        public override string ToString() { return string.Format("{0}, {1}", X, Y); }

        public static Vector2D operator +(Vector2D one, Vector2D two)
        {
            return checked(new Vector2D(one.X + two.X, one.Y + two.Y));
        }

        public static Vector2D operator -(Vector2D one, Vector2D two)
        {
            return checked(new Vector2D(one.X - two.X, one.Y - two.Y));
        }
    }
}