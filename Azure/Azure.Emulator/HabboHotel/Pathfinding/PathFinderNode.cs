using System;

namespace Azure.HabboHotel.PathFinding
{
    class PathFinderNode : IComparable<PathFinderNode>
    {
        public readonly Vector2D Position;
        public PathFinderNode Next;
        public int Cost = 2147483647;
        public bool InOpen;
        public bool InClosed;

        public PathFinderNode(Vector2D position) { Position = position; }

        public override bool Equals(object obj)
        {
            return obj is PathFinderNode && ((PathFinderNode) obj).Position.Equals(Position);
        }

        public bool Equals(PathFinderNode breadCrumb) { return breadCrumb.Position.Equals(Position); }

        public override int GetHashCode() { return Position.GetHashCode(); }

        public int CompareTo(PathFinderNode other) { return Cost.CompareTo(other.Cost); }
    }
}