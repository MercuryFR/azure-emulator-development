namespace Azure.HabboHotel.Support
{
    internal enum TicketStatus
    {
        Open,
        Picked,
        Resolved,
        Abusive,
        Invalid,
        Deleted
    }
}