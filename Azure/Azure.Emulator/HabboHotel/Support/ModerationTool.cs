using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Rooms;
using Azure.HabboHotel.Users;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Support
{
    public class ModerationTool
    {
        internal List<SupportTicket> Tickets;
        internal Dictionary<uint, ModerationTemplate> ModerationTemplates;
        internal List<string> UserMessagePresets;
        internal List<string> RoomMessagePresets;
        internal StringDictionary SupportTicketHints;

        internal ModerationTool()
        {
            this.Tickets = new List<SupportTicket>();
            this.UserMessagePresets = new List<string>();
            this.RoomMessagePresets = new List<string>();
            this.SupportTicketHints = new StringDictionary();
            this.ModerationTemplates = new Dictionary<uint, ModerationTemplate>();
        }

        internal static void SendTicketUpdateToModerators(SupportTicket ticket)
        {
        }

        internal static void SendTicketToModerators(SupportTicket ticket)
        {
            var message = new ServerMessage(LibraryParser.OutgoingRequest("ModerationToolIssueMessageComposer"));
            message = ticket.Serialize(message);
            Azure.GetGame().GetClientManager().StaffAlert(message);
        }

        internal static void PerformRoomAction(GameClient modSession, uint roomId, bool kickUsers, bool lockRoom, bool inappropriateRoom, ServerMessage message)
        {
            Room room = Azure.GetGame().GetRoomManager().GetRoom(roomId);
            if (room == null)
            {
                return;
            }
            if (lockRoom)
            {
                room.State = 1;
                using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.RunFastQuery(string.Format("UPDATE rooms_data SET state = 'locked' WHERE id = {0}", room.RoomId));
                }
            }
            if (inappropriateRoom)
            {
                room.Name = "Inappropiate to Hotel Management.";
                room.Description = "Your room description is not allowed.";
                room.ClearTags();
                room.RoomData.SerializeRoomData(message, false, modSession, true);
            }
            if (kickUsers)
            {
                room.OnRoomKick();
            }
        }

        internal static ServerMessage SerializeRoomTool(RoomData Data)
        {
            Room room = Azure.GetGame().GetRoomManager().GetRoom(Data.Id);
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ModerationRoomToolMessageComposer"));
            serverMessage.AppendInteger(Data.Id);
            serverMessage.AppendInteger(Data.UsersNow);
            if (room != null)
            {
                serverMessage.AppendBool(room.GetRoomUserManager().GetRoomUserByHabbo(Data.Owner) != null);
            }
            else
            {
                serverMessage.AppendBool(false);
            }
            serverMessage.AppendInteger(room != null ? room.OwnerId : 0);
            serverMessage.AppendString(Data.Owner);
            serverMessage.AppendBool(room != null);
            serverMessage.AppendString(Data.Name);
            serverMessage.AppendString(Data.Description);
            serverMessage.AppendInteger(Data.TagCount);
            foreach (string current in Data.Tags)
            {
                serverMessage.AppendString(current);
            }
            serverMessage.AppendBool(false);
            return serverMessage;
        }

        internal static void KickUser(GameClient modSession, uint userId, string message, bool soft)
        {
            GameClient clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(userId);
            if (clientByUserId == null || clientByUserId.GetHabbo().CurrentRoomId < 1u || clientByUserId.GetHabbo().Id == modSession.GetHabbo().Id)
            {
                return;
            }
            if (clientByUserId.GetHabbo().Rank >= modSession.GetHabbo().Rank)
            {
                modSession.SendNotif("You are not allowed to kick him/her.");
                return;
            }
            Room room = Azure.GetGame().GetRoomManager().GetRoom(clientByUserId.GetHabbo().CurrentRoomId);
            if (room == null)
            {
                return;
            }
            room.GetRoomUserManager().RemoveUserFromRoom(clientByUserId, true, false);
            clientByUserId.CurrentRoomUserId = -1;
            if (soft)
            {
                return;
            }
            clientByUserId.SendNotif(message);
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Format("UPDATE users_info SET cautions = cautions + 1 WHERE user_id = {0}", userId));
            }
        }

        internal static void AlertUser(GameClient modSession, uint userId, string message, bool caution)
        {
            GameClient clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(userId);
            if (clientByUserId == null)
            {
                return;
            }
            clientByUserId.SendNotif(message);
        }

        internal static void LockTrade(GameClient modSession, uint userId, string message, int length)
        {
            GameClient clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(userId);
            if (clientByUserId == null)
            {
                return;
            }
            int num = length;
            checked
            {
                if (!clientByUserId.GetHabbo().CheckTrading())
                {
                    num += Azure.GetUnixTimestamp() - clientByUserId.GetHabbo().TradeLockExpire;
                }
                clientByUserId.GetHabbo().TradeLocked = true;
                clientByUserId.GetHabbo().TradeLockExpire = Azure.GetUnixTimestamp() + num;
                clientByUserId.SendNotif(message);
                using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.RunFastQuery(string.Format("UPDATE users SET trade_lock = '1', trade_lock_expire = '{0}'", clientByUserId.GetHabbo().TradeLockExpire));
                }
            }
        }

        internal static void BanUser(GameClient modSession, uint userId, int length, string message)
        {
            GameClient clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(userId);
            if (clientByUserId == null || clientByUserId.GetHabbo().Id == modSession.GetHabbo().Id)
            {
                return;
            }
            if (clientByUserId.GetHabbo().Rank >= modSession.GetHabbo().Rank)
            {
                modSession.SendNotif("No tienes los permisos para banear");
                return;
            }
            double lengthSeconds = length;
            Azure.GetGame().GetBanManager().BanUser(clientByUserId, modSession.GetHabbo().UserName, lengthSeconds, message, false, false);
        }

        internal static ServerMessage SerializeUserInfo(uint userId)
        {
            checked
            {
                ServerMessage result;
                using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery(string.Format("SELECT id, Username, online, mail, ip_last, look , rank , trade_lock , trade_lock_expire FROM users WHERE id = {0}", userId));
                    DataRow row = queryReactor.GetRow();
                    queryReactor.SetQuery(string.Format("SELECT reg_timestamp, login_timestamp, cfhs, cfhs_abusive, cautions, bans FROM users_info WHERE user_id = {0}", userId));
                    DataRow row2 = queryReactor.GetRow();
                    if (row == null)
                    {
                        throw new NullReferenceException("User not found in database.");
                    }
                    var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ModerationToolUserToolMessageComposer"));
                    serverMessage.AppendInteger(Convert.ToUInt32(row["id"]));
                    serverMessage.AppendString((string)row["Username"]);
                    serverMessage.AppendString((string)row["look"]);
                    if (row2 != null)
                    {
                        serverMessage.AppendInteger((int)Math.Ceiling(unchecked(Azure.GetUnixTimestamp() - (double)row2["reg_timestamp"]) / 60.0));
                        serverMessage.AppendInteger((int)Math.Ceiling(unchecked(Azure.GetUnixTimestamp() - (double)row2["login_timestamp"]) / 60.0));
                    }
                    else
                    {
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendInteger(0);
                    }
                    serverMessage.AppendBool(Azure.GetGame().GetClientManager().GetClientByUserId(Convert.ToUInt32(row["id"])) != null);
                    if (row2 != null)
                    {
                        serverMessage.AppendInteger((int)row2["cfhs"]);
                        serverMessage.AppendInteger((int)row2["cfhs_abusive"]);
                        serverMessage.AppendInteger((int)row2["cautions"]);
                        serverMessage.AppendInteger((int)row2["bans"]);
                    }
                    else
                    {
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendInteger(0);
                        serverMessage.AppendInteger(0);
                    }
                    serverMessage.AppendInteger(0);
                    serverMessage.AppendString((row["trade_lock"].ToString() == "1") ? Azure.UnixToDateTime(int.Parse(row["trade_lock_expire"].ToString())).ToLongDateString() : "Not trade-locked");
                    serverMessage.AppendString(((uint)row["rank"] < 6u) ? ((string)row["ip_last"]) : "127.0.0.1");
                    serverMessage.AppendInteger(Convert.ToUInt32(row["id"]));
                    serverMessage.AppendInteger(0);
                    serverMessage.AppendString(string.Format("E-Mail:         {0}", row["mail"]));
                    serverMessage.AppendString(string.Format("Rank ID:        {0}", (uint)row["rank"]));
                    result = serverMessage;
                }
                return result;
            }
        }

        internal static ServerMessage SerializeRoomVisits(uint userId)
        {
            ServerMessage result;
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT room_id,hour,minute FROM users_rooms_visits WHERE user_id = {0} ORDER BY entry_timestamp DESC LIMIT 50", userId));
                DataTable table = queryReactor.GetTable();
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ModerationToolRoomVisitsMessageComposer"));
                serverMessage.AppendInteger(userId);
                serverMessage.AppendString(Azure.GetGame().GetClientManager().GetNameById(userId));
                if (table != null)
                {
                    serverMessage.AppendInteger(table.Rows.Count);
                    IEnumerator enumerator = table.Rows.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            var dataRow = (DataRow)enumerator.Current;
                            RoomData roomData = Azure.GetGame().GetRoomManager().GenerateNullableRoomData(Convert.ToUInt32(dataRow["room_id"]));
                            serverMessage.AppendBool(false);
                            serverMessage.AppendInteger(roomData.Id);
                            serverMessage.AppendString(roomData.Name);
                            serverMessage.AppendInteger((int)dataRow["hour"]);
                            serverMessage.AppendInteger((int)dataRow["minute"]);
                        }
                        goto IL_120;
                    }
                    finally
                    {
                        var disposable = enumerator as IDisposable;
                        if (disposable != null)
                        {
                            disposable.Dispose();
                        }
                    }
                }
                serverMessage.AppendInteger(0);
                IL_120:
                result = serverMessage;
            }
            return result;
        }

        internal static ServerMessage SerializeUserChatlog(uint userId)
        {
            ServerMessage result;
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT DISTINCT room_id FROM users_chatlogs WHERE user_id = {0} ORDER BY timestamp DESC LIMIT 4", userId));
                DataTable table = queryReactor.GetTable();
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ModerationToolUserChatlogMessageComposer"));
                serverMessage.AppendInteger(userId);
                serverMessage.AppendString(Azure.GetGame().GetClientManager().GetNameById(userId));
                if (table != null)
                {
                    serverMessage.AppendInteger(table.Rows.Count);
                    IEnumerator enumerator = table.Rows.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            var dataRow = (DataRow)enumerator.Current;
                            queryReactor.SetQuery(string.Concat(new object[]
                            {
                                "SELECT user_id,timestamp,message FROM users_chatlogs WHERE room_id = ",
                                (uint)dataRow["room_id"],
                                " AND user_id = ",
                                userId,
                                " ORDER BY timestamp DESC LIMIT 30"
                            }));
                            DataTable table2 = queryReactor.GetTable();
                            RoomData roomData = Azure.GetGame().GetRoomManager().GenerateRoomData((uint)dataRow["room_id"]);
                            if (table2 != null)
                            {
                                serverMessage.AppendByted(1);
                                serverMessage.AppendShort(2);
                                serverMessage.AppendString("roomName");
                                serverMessage.AppendByted(2);
                                serverMessage.AppendString(roomData == null ? "This room was deleted" : roomData.Name);
                                serverMessage.AppendString("roomId");
                                serverMessage.AppendByted(1);
                                serverMessage.AppendInteger((uint)dataRow["room_id"]);
                                serverMessage.AppendShort(table2.Rows.Count);
                                IEnumerator enumerator2 = table2.Rows.GetEnumerator();
                                try
                                {
                                    while (enumerator2.MoveNext())
                                    {
                                        var dataRow2 = (DataRow)enumerator2.Current;
                                        Habbo habboForId = Azure.GetHabboForId((uint)dataRow2["user_id"]);
                                        Azure.UnixToDateTime((double)dataRow2["timestamp"]);
                                        if (habboForId == null)
                                        {
                                            return null;
                                        }
                                        serverMessage.AppendInteger(checked((int)unchecked(Azure.GetUnixTimestamp() - (double)dataRow2["timestamp"])));
                                        serverMessage.AppendInteger(habboForId.Id);
                                        serverMessage.AppendString(habboForId.UserName);
                                        serverMessage.AppendString(dataRow2["message"].ToString());
                                        serverMessage.AppendBool(false);
                                    }
                                    continue;
                                }
                                finally
                                {
                                    var disposable = enumerator2 as IDisposable;
                                    if (disposable != null)
                                    {
                                        disposable.Dispose();
                                    }
                                }
                            }
                            serverMessage.AppendByted(1);
                            serverMessage.AppendShort(0);
                            serverMessage.AppendShort(0);
                        }
                        goto IL_29B;
                    }
                    finally
                    {
                        var disposable2 = enumerator as IDisposable;
                        if (disposable2 != null)
                        {
                            disposable2.Dispose();
                        }
                    }
                }
                serverMessage.AppendInteger(0);
                IL_29B:
                result = serverMessage;
            }
            return result;
        }
    
        internal static ServerMessage SerializeTicketChatlog(SupportTicket ticket, RoomData roomData, double timestamp)
        {
            ServerMessage result;
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Concat(new object[]
                {
                    "SELECT user_id,timestamp,message FROM users_chatlogs WHERE room_id = ",
                    roomData.Id,
                    " AND (timestamp >= ",
                    timestamp - 300.0,
                    " AND timestamp <= ",
                    timestamp,
                    ") OR (timestamp >= ",
                    timestamp - 300.0,
                    " AND timestamp = 0) ORDER BY timestamp DESC LIMIT 150"
                }));
                DataTable table = queryReactor.GetTable();
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ModerationToolIssueChatlogMessageComposer"));
                serverMessage.AppendInteger(ticket.TicketId);
                serverMessage.AppendInteger(ticket.SenderId);
                serverMessage.AppendInteger(ticket.ReportedId);
                serverMessage.AppendInteger(roomData.Id);
                serverMessage.AppendBool(false);
                serverMessage.AppendInteger(roomData.Id);
                serverMessage.AppendString(roomData.Name);
                if (table != null)
                {
                    serverMessage.AppendInteger(table.Rows.Count);
                    IEnumerator enumerator = table.Rows.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            var dataRow = (DataRow)enumerator.Current;
                            Habbo habboForId = Azure.GetHabboForId(Convert.ToUInt32(dataRow["user_id"]));
                            serverMessage.AppendInteger(Azure.UnixToDateTime(Convert.ToDouble(dataRow["timestamp"])).Minute);
                            serverMessage.AppendInteger(habboForId.Id);
                            serverMessage.AppendString(habboForId.UserName);
                            serverMessage.AppendString((string)dataRow["message"]);
                        }
                        goto IL_1B8;
                    }
                    finally
                    {
                        var disposable = enumerator as IDisposable;
                        if (disposable != null)
                        {
                            disposable.Dispose();
                        }
                    }
                }
                serverMessage.AppendInteger(0);
                IL_1B8:
                result = serverMessage;
            }
            return result;
        }

        internal static ServerMessage SerializeRoomChatlog(uint roomId)
        {
            // NEW CHATLOGS [March 2014] Coded by Finn
            // Please don't remove credits, this took me some time to do... :(
            // Credits to Itachi for the structure's "context" enigma :D
            var message = new ServerMessage();
            RoomData room = Azure.GetGame().GetRoomManager().GenerateRoomData(roomId);
            if (room == null)
            {
                throw new NullReferenceException("No room found.");
            }

            message.Init(LibraryParser.OutgoingRequest("ModerationToolRoomChatlogMessageComposer"));
            message.AppendByted(1);
            message.AppendShort(2);
            message.AppendString("roomName");
            message.AppendByted(2);
            message.AppendString(room.Name);
            message.AppendString("roomId");
            message.AppendByted(1);
            message.AppendInteger(room.Id);

            IEnumerable<Chatlog> tempChatlogs = room.RoomChat.Reverse().Take(60);
            message.AppendShort(tempChatlogs.Count());
            foreach (Chatlog current in tempChatlogs)
            {
                Habbo habbo = Azure.GetHabboForId(current.UserId);
                DateTime date = Azure.UnixToDateTime(current.Timestamp);
                if (habbo == null)
                {
                    message.AppendInteger((DateTime.Now - date).Seconds);
                    message.AppendInteger(current.UserId);
                    message.AppendString("*User not found*");
                    message.AppendString(current.Message);
                    message.AppendBool(true);
                }
                else
                {
                    message.AppendInteger((DateTime.Now - date).Seconds);
                    message.AppendInteger(habbo.Id);
                    message.AppendString(habbo.UserName);
                    message.AppendString(current.Message);
                    message.AppendBool(false); // Text is bold
                }
            }
            tempChatlogs = null;

            return message;
        }

        internal ServerMessage SerializeTool()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("LoadModerationToolMessageComposer"));
            serverMessage.AppendInteger(this.Tickets.Count);
            foreach (SupportTicket current in this.Tickets)
            {
                current.Serialize(serverMessage);
            }
            serverMessage.AppendInteger(this.UserMessagePresets.Count);
            foreach (string current2 in this.UserMessagePresets)
            {
                serverMessage.AppendString(current2);
            }

            /*
            using (IQueryAdapter dbClient = MercuryEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * from moderation_templates where category = '1' ");
                DataTable PII = dbClient.GetTable();

                dbClient.SetQuery("SELECT * from moderation_templates where category = '2' ");
                DataTable sex = dbClient.GetTable();

                serverMessage.AppendInt32(2);
                serverMessage.AppendString("PII");
                serverMessage.AppendBoolean(true);
                serverMessage.AppendInt32(PII.Rows.Count); //array count

                foreach (DataRow PIIRow in PII.Rows)
                {
                    serverMessage.AppendString(Convert.ToString(PIIRow["caption"])); //type
                    serverMessage.AppendString(Convert.ToString(PIIRow["ban_message"])); //ban message
                    serverMessage.AppendInt32(Convert.ToInt32(PIIRow["ban_hours"]));
                    serverMessage.AppendInt32(Convert.ToInt32(PIIRow["avatar_ban"]));
                    serverMessage.AppendInt32(Convert.ToInt32(PIIRow["mute"]));
                    serverMessage.AppendInt32(Convert.ToInt32(PIIRow["trade_lock"]));// REMOVE THIS LINE IF YOUR NOT USING THE NEW_UI
                    serverMessage.AppendString(Convert.ToString(PIIRow["warning_message"])); // warning message

                }

                serverMessage.AppendString("Sexually Explicit");
                serverMessage.AppendBoolean(false);
                serverMessage.AppendInt32(sex.Rows.Count); //array count
                foreach (DataRow Sex in sex.Rows)
                {
                    serverMessage.AppendString(Convert.ToString(Sex["caption"])); //type
                    serverMessage.AppendString(Convert.ToString(Sex["ban_message"])); //ban message
                    serverMessage.AppendInt32(Convert.ToInt32(Sex["ban_hours"]));
                    serverMessage.AppendInt32(Convert.ToInt32(Sex["avatar_ban"]));
                    serverMessage.AppendInt32(Convert.ToInt32(Sex["mute"]));
                    serverMessage.AppendInt32(Convert.ToInt32(Sex["trade_lock"]));// REMOVE THIS LINE IF YOUR NOT USING THE NEW_UI
                    serverMessage.AppendString(Convert.ToString(Sex["warning_message"])); // warning message

                }
            }
            */

            IEnumerable<ModerationTemplate> enumerable = from x in this.ModerationTemplates.Values
                                                         where x.Category == -1
                                                         select x;


            serverMessage.AppendInteger(enumerable.Count());
            using (IEnumerator<ModerationTemplate> enumerator3 = enumerable.GetEnumerator())
            {
                bool first = true;
                while (enumerator3.MoveNext())
                {
                    ModerationTemplate template = enumerator3.Current;
                    IEnumerable<ModerationTemplate> enumerable2 = from x in this.ModerationTemplates.Values
                                                                  where x.Category == (long)((ulong)template.Id)
                                                                  select x;
                    serverMessage.AppendString(template.CName);
                    serverMessage.AppendBool(first);
                    serverMessage.AppendInteger(enumerable2.Count());
                    foreach (ModerationTemplate current3 in enumerable2)
                    {
                        serverMessage.AppendString(current3.Caption);
                        serverMessage.AppendString(current3.BanMessage);
                        serverMessage.AppendInteger(current3.BanHours);
                        serverMessage.AppendInteger(Azure.BoolToInteger(current3.AvatarBan));
                        serverMessage.AppendInteger(Azure.BoolToInteger(current3.Mute));
                        serverMessage.AppendInteger(Azure.BoolToInteger(current3.TradeLock));
                        serverMessage.AppendString(current3.WarningMessage);
                    }
                    first = false;
                }
            }

            serverMessage.AppendBool(true);
            serverMessage.AppendBool(true);
            serverMessage.AppendBool(true);
            serverMessage.AppendBool(true);
            serverMessage.AppendBool(true);
            serverMessage.AppendBool(true);
            serverMessage.AppendBool(true);
            serverMessage.AppendInteger(this.RoomMessagePresets.Count);
            foreach (string current4 in this.RoomMessagePresets)
            {
                serverMessage.AppendString(current4);
            }
            return serverMessage;
        }

        internal void LoadMessagePresets(IQueryAdapter dbClient)
        {
            this.UserMessagePresets.Clear();
            this.RoomMessagePresets.Clear();
            this.SupportTicketHints.Clear();
            this.ModerationTemplates.Clear();
            dbClient.SetQuery("SELECT type,message FROM moderation_presets WHERE enabled = 2");
            DataTable table = dbClient.GetTable();
            dbClient.SetQuery("SELECT word,hint FROM moderation_tickethints");
            DataTable table2 = dbClient.GetTable();
            dbClient.SetQuery("SELECT * FROM moderation_templates");
            DataTable table3 = dbClient.GetTable();
            if (table == null || table2 == null)
            {
                return;
            }
            foreach (DataRow dataRow in table.Rows)
            {
                var item = (string)dataRow["message"];
                string a = dataRow["type"].ToString().ToLower();

                if (a != "message")
                {
                    if (a == "roommessage")
                    {
                        this.RoomMessagePresets.Add(item);
                    }
                }
                else
                {
                    this.UserMessagePresets.Add(item);
                }
            }
            foreach (DataRow dataRow2 in table2.Rows)
            {
                this.SupportTicketHints.Add((string)dataRow2[0], (string)dataRow2[1]);
            }
            foreach (DataRow dataRow3 in table3.Rows)
            {
                this.ModerationTemplates.Add(uint.Parse(dataRow3["id"].ToString()), new ModerationTemplate(uint.Parse(dataRow3["id"].ToString()), short.Parse(dataRow3["category"].ToString()), dataRow3["cName"].ToString(), dataRow3["caption"].ToString(), dataRow3["warning_message"].ToString(), dataRow3["ban_message"].ToString(), short.Parse(dataRow3["ban_hours"].ToString()), dataRow3["avatar_ban"].ToString() == "1", dataRow3["mute"].ToString() == "1", dataRow3["trade_lock"].ToString() == "1"));
            }
        }

        internal void LoadPendingTickets(IQueryAdapter dbClient)
        {
            dbClient.RunFastQuery("TRUNCATE TABLE moderation_tickets");
        }

        internal void SendNewTicket(GameClient session, int category, uint reportedUser, string message, int type,
            List<string> messages)
        {
            uint id;

            if (session.GetHabbo().CurrentRoomId <= 0)
            {
                using (IQueryAdapter dbClient = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    dbClient.SetQuery(string.Concat(new object[]
                    {
                        "INSERT INTO moderation_tickets (score,type,status,sender_id,reported_id,moderator_id,message,room_id,room_name,timestamp) VALUES (1,'",
                        category,
                        "','open','",
                        session.GetHabbo().Id,
                        "','",
                        reportedUser,
                        "','0',@message,'0','','",
                        Azure.GetUnixTimestamp(),
                        "')"
                    }));
                    dbClient.AddParameter("message", message);
                    id = (uint)dbClient.InsertQuery();
                    dbClient.RunFastQuery(string.Format("UPDATE users_info SET cfhs = cfhs + 1 WHERE user_id = {0}", session.GetHabbo().Id));
                }

                var ticket = new SupportTicket(id, 1, type, session.GetHabbo().Id, reportedUser, message, 0u, "",
                    Azure.GetUnixTimestamp(), messages);
                this.Tickets.Add(ticket);
                SendTicketToModerators(ticket);
                return;
            }

            RoomData data = Azure.GetGame().GetRoomManager().GenerateNullableRoomData(session.GetHabbo().CurrentRoomId);
            using (IQueryAdapter dbClient = Azure.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery(string.Concat(new object[]
                {
                    "INSERT INTO moderation_tickets (score,type,status,sender_id,reported_id,moderator_id,message,room_id,room_name,timestamp) VALUES (1,'",
                    category,
                    "','open','",
                    session.GetHabbo().Id,
                    "','",
                    reportedUser,
                    "','0',@message,'",
                    data.Id,
                    "',@name,'",
                    Azure.GetUnixTimestamp(),
                    "')"
                }));
                dbClient.AddParameter("message", message);
                dbClient.AddParameter("name", data.Name);
                id = (uint)dbClient.InsertQuery();
                dbClient.RunFastQuery(string.Format("UPDATE users_info SET cfhs = cfhs + 1 WHERE user_id = {0}", session.GetHabbo().Id));
            }
            var ticket2 = new SupportTicket(id, 1, type, session.GetHabbo().Id, reportedUser, message, 0u, "",
                Azure.GetUnixTimestamp(), messages);
            this.Tickets.Add(ticket2);
            SendTicketToModerators(ticket2);
        }

        internal void SerializeOpenTickets(ref QueuedServerMessage serverMessages, uint userId)
        {
            var message = new ServerMessage(LibraryParser.OutgoingRequest("ModerationToolIssueMessageComposer"));
            foreach (SupportTicket current in this.Tickets.Where(current => current.Status == TicketStatus.Open || (current.Status == TicketStatus.Picked && current.ModeratorId == userId) || (current.Status == TicketStatus.Picked && current.ModeratorId == 0u)))
            {
                message = current.Serialize(message);
                serverMessages.AppendResponse(message);
            }
        }

        internal SupportTicket GetTicket(uint ticketId)
        {
            return this.Tickets.FirstOrDefault(current => current.TicketId == ticketId);
        }

        internal void PickTicket(GameClient session, uint ticketId)
        {
            SupportTicket ticket = this.GetTicket(ticketId);
            if (ticket == null || ticket.Status != TicketStatus.Open)
            {
                return;
            }
            ticket.Pick(session.GetHabbo().Id, true);
            SendTicketToModerators(ticket);
        }

        internal void ReleaseTicket(GameClient session, uint ticketId)
        {
            SupportTicket ticket = this.GetTicket(ticketId);
            if (ticket == null || ticket.Status != TicketStatus.Picked || ticket.ModeratorId != session.GetHabbo().Id)
            {
                return;
            }
            ticket.Release(true);
            SendTicketToModerators(ticket);
        }

        internal void CloseTicket(GameClient session, uint ticketId, int result)
        {
            SupportTicket ticket = this.GetTicket(ticketId);
            if (ticket == null || ticket.Status != TicketStatus.Picked || ticket.ModeratorId != session.GetHabbo().Id)
            {
                return;
            }
            GameClient clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(ticket.SenderId);
            int i;
            TicketStatus newStatus;
            switch (result)
            {
                case 1:
                    i = 1;
                    newStatus = TicketStatus.Invalid;
                    goto IL_9E;
                case 2:
                    i = 2;
                    newStatus = TicketStatus.Abusive;
                    using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                    {
                        queryReactor.RunFastQuery(string.Format("UPDATE users_info SET cfhs_abusive = cfhs_abusive + 1 WHERE user_id = {0}", ticket.SenderId));
                        goto IL_9E;
                    }
            }
            i = 0;
            newStatus = TicketStatus.Resolved;
            IL_9E:
            if (clientByUserId != null && (ticket.Type != 3 && ticket.Type != 4))
            {
                clientByUserId.GetMessageHandler().GetResponse().Init(LibraryParser.OutgoingRequest("ModerationToolUpdateIssueMessageComposer"));
                clientByUserId.GetMessageHandler().GetResponse().AppendInteger(1);
                clientByUserId.GetMessageHandler().GetResponse().AppendInteger(ticket.TicketId);
                clientByUserId.GetMessageHandler().GetResponse().AppendInteger(ticket.ModeratorId);
                clientByUserId.GetMessageHandler()
                              .GetResponse()
                              .AppendString((Azure.GetHabboForId(ticket.ModeratorId) != null)
                                            ? Azure.GetHabboForId(ticket.ModeratorId).UserName
                                            : "Undefined");
                clientByUserId.GetMessageHandler().GetResponse().AppendBool(false);
                clientByUserId.GetMessageHandler().GetResponse().AppendInteger(0);
                clientByUserId.GetMessageHandler().GetResponse().Init(LibraryParser.OutgoingRequest("ModerationTicketResponseMessageComposer"));
                clientByUserId.GetMessageHandler().GetResponse().AppendInteger(i);
                clientByUserId.GetMessageHandler().SendResponse();
            }
            using (IQueryAdapter queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryreactor2.RunFastQuery(string.Format("UPDATE users_stats SET tickets_answered = tickets_answered+1 WHERE id={0} LIMIT 1", session.GetHabbo().Id));
            }
            ticket.Close(newStatus, true);
            SendTicketToModerators(ticket);
        }

        internal bool UsersHasPendingTicket(uint id)
        {
            return this.Tickets.Any(current => current.SenderId == id && current.Status == TicketStatus.Open);
        }

        internal void DeletePendingTicketForUser(uint id)
        {
            foreach (SupportTicket current in this.Tickets.Where(current => current.SenderId == id))
            {
                current.Delete(true);
                SendTicketToModerators(current);
                break;
            }
        }

        internal void LogStaffEntry(string modName, string target, string type, string description)
        {
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("INSERT INTO server_stafflogs (staffuser,target,action_type,description) VALUES (@Username,@target,@type,@desc)");
                queryReactor.AddParameter("Username", modName);
                queryReactor.AddParameter("target", target);
                queryReactor.AddParameter("type", type);
                queryReactor.AddParameter("desc", description);
                queryReactor.RunQuery();
            }
        }
    }
}