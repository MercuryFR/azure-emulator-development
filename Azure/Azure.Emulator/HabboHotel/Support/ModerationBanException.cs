using System;

namespace Azure.HabboHotel.Support
{
    [Serializable]
    public class ModerationBanException : Exception
    {
        internal ModerationBanException(string reason) : base(reason)
        {
        }
    }
}