using System.Collections.Generic;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.Messages;
using System.Drawing;
using System.Linq;

namespace Azure.HabboHotel.Support
{
    internal class SupportTicket
    {
        internal int Score;
        internal int Type;
        internal TicketStatus Status;
        internal uint SenderId;
        internal uint ReportedId;
        internal uint ModeratorId;
        internal string Message;
        internal uint RoomId;
        internal string RoomName;
        internal double Timestamp;
        internal List<string> ReportedChats;
        private readonly string _senderName;
        private readonly string _reportedName;
        private string _modName;

        internal SupportTicket(uint id, int score, int type, uint senderId, uint reportedId, string message, uint roomId,
            string roomName, double timestamp, List<string> reportedChats)
        {
            this.TicketId = id;
            this.Score = score;
            this.Type = type;
            this.Status = TicketStatus.Open;
            this.SenderId = senderId;
            this.ReportedId = reportedId;
            this.ModeratorId = 0u;
            this.Message = message;
            this.RoomId = roomId;
            this.RoomName = roomName;
            this.Timestamp = timestamp;
            this._senderName = Azure.GetGame().GetClientManager().GetNameById(senderId);
            this._reportedName = Azure.GetGame().GetClientManager().GetNameById(reportedId);
            this._modName = Azure.GetGame().GetClientManager().GetNameById(this.ModeratorId);
            this.ReportedChats = reportedChats;
        }

        internal int TabId
        {
            get
            {
                if (this.Status == TicketStatus.Open)
                {
                    return 1;
                }
                if (this.Status == TicketStatus.Picked)
                {
                    return 2;
                }
                if (this.Status == TicketStatus.Abusive || this.Status == TicketStatus.Invalid || this.Status == TicketStatus.Resolved)
                {
                    return 0;
                }
                if (this.Status == TicketStatus.Deleted)
                {
                    return 0;
                }
                return 0;
            }
        }

        internal uint TicketId { get; private set; }

        internal void Pick(uint pModeratorId, bool updateInDb)
        {
            this.Status = TicketStatus.Picked;
            this.ModeratorId = pModeratorId;
            this._modName = Azure.GetHabboForId(pModeratorId).UserName;
            if (!updateInDb)
            {
                return;
            }
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE moderation_tickets SET status = 'picked', moderator_id = ",
                    pModeratorId,
                    ", timestamp = '",
                    Azure.GetUnixTimestamp(),
                    "' WHERE id = ",
                    this.TicketId
                }));
            }
        }

        internal void Close(TicketStatus newStatus, bool updateInDb)
        {
            this.Status = newStatus;
            if (!updateInDb)
            {
                return;
            }
            string text;
            switch (newStatus)
            {
                case TicketStatus.Abusive:
                    text = "abusive";
                    goto IL_41;
                case TicketStatus.Invalid:
                    text = "invalid";
                    goto IL_41;
            }
            text = "resolved";
            IL_41:
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE moderation_tickets SET status = '",
                    text,
                    "' WHERE id = ",
                    this.TicketId
                }));
            }
        }

        internal void Release(bool updateInDb)
        {
            this.Status = TicketStatus.Open;
            if (!updateInDb)
            {
                return;
            }
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Format("UPDATE moderation_tickets SET status = 'open' WHERE id = {0}", this.TicketId));
            }
        }

        internal void Delete(bool updateInDb)
        {
            this.Status = TicketStatus.Deleted;
            if (!updateInDb)
            {
                return;
            }
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Format("UPDATE moderation_tickets SET status = 'deleted' WHERE id = {0}", this.TicketId));
            }
        }

        internal ServerMessage Serialize(ServerMessage message)
        {
            message.AppendInteger(TicketId); // id
            message.AppendInteger(TabId); // state
            message.AppendInteger(3); // type (3 or 4 for new style)
            message.AppendInteger(114); // priority
            message.AppendInteger((Azure.GetUnixTimestamp() - (int)Timestamp) * 1000); // -->> timestamp
            message.AppendInteger(Score); // priority
            message.AppendInteger(0);
            message.AppendInteger(SenderId); // sender id 8 ints
            message.AppendString(_senderName); // sender name
            message.AppendInteger(ReportedId);
            message.AppendString(_reportedName);
            message.AppendInteger((Status == TicketStatus.Picked) ? ModeratorId : 0); // mod id
            message.AppendString(_modName); // mod name
            message.AppendString(this.Message); // issue message
            message.AppendInteger(0); // is room public?

            message.AppendInteger(ReportedChats.Count);
            foreach (string str in this.ReportedChats)
            {
                message.AppendString(str);
                message.AppendInteger(-1);
                message.AppendInteger(-1);
            }

            return message;
        }
    }
}