using System;

namespace Azure.HabboHotel.Support
{
    internal enum ModerationBanType
    {
        Ip,
        UserName,
        Machine
    }
}