namespace Azure.HabboHotel.Support
{
    internal class ModerationTemplate
    {
        internal uint Id;
        internal short Category;
        internal string CName;
        internal string Caption;
        internal string WarningMessage;
        internal string BanMessage;
        internal short BanHours;
        internal bool AvatarBan;
        internal bool Mute;
        internal bool TradeLock;

        internal ModerationTemplate(uint id, short category, string cName, string caption, string warningMessage, string banMessage,
            short banHours, bool avatarBan, bool mute, bool tradeLock)
        {
            Id = id;
            Category = category;
            CName = cName;
            Caption = caption;
            WarningMessage = warningMessage;
            BanMessage = banMessage;
            BanHours = banHours;
            AvatarBan = avatarBan;
            Mute = mute;
            TradeLock = tradeLock;
        }
    }
}