using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.GameClients;

namespace Azure.HabboHotel.Support
{
    internal class ModerationBanManager
    {
        private readonly HybridDictionary _bannedUsernames;
        private readonly HybridDictionary _bannedIPs;
        private readonly Dictionary<string, ModerationBan> _bannedMachines;

        internal ModerationBanManager()
        {
            this._bannedUsernames = new HybridDictionary();
            this._bannedIPs = new HybridDictionary();
            this._bannedMachines = new Dictionary<string, ModerationBan>();
        }

        internal void LoadBans(IQueryAdapter dbClient)
        {
            this._bannedUsernames.Clear();
            this._bannedIPs.Clear();
            this._bannedMachines.Clear();
            dbClient.SetQuery("SELECT bantype,value,reason,expire FROM users_bans");
            DataTable table = dbClient.GetTable();
            double num = Azure.GetUnixTimestamp();
            foreach (DataRow dataRow in table.Rows)
            {
                var text = (string)dataRow["value"];
                var reasonMessage = (string)dataRow["reason"];
                var num2 = (double)dataRow["expire"];
                var a = (string)dataRow["bantype"];
                ModerationBanType type;
                switch (a)
                {
                    case "user":
                        type = ModerationBanType.UserName;
                        break;
                    case "ip":
                        type = ModerationBanType.Ip;
                        break;
                    default:
                        type = ModerationBanType.Machine;
                        break;
                }
                var moderationBan = new ModerationBan(type, text, reasonMessage, num2);
                if (!(num2 > num))
                {
                    continue;
                }
                switch (moderationBan.Type)
                {
                    case ModerationBanType.UserName:
                        if (!this._bannedUsernames.Contains(text))
                        {
                            this._bannedUsernames.Add(text, moderationBan);
                        }
                        break;
                    case ModerationBanType.Ip:
                        if (!this._bannedIPs.Contains(text))
                        {
                            this._bannedIPs.Add(text, moderationBan);
                        }
                        break;
                    default:
                        if (!this._bannedMachines.ContainsKey(text))
                        {
                            this._bannedMachines.Add(text, moderationBan);
                        }
                        break;
                }
            }
        }

        internal string GetBanReason(string Username, string ip, string machineid)
        {
            if (this._bannedUsernames.Contains(Username))
            {
                var moderationBan = (ModerationBan)this._bannedUsernames[Username];
                if (!moderationBan.Expired)
                {
                    return moderationBan.ReasonMessage;
                }
            }
            else
            {
                if (this._bannedIPs.Contains(ip))
                {
                    var moderationBan2 = (ModerationBan)this._bannedIPs[Username];
                    if (!moderationBan2.Expired)
                    {
                        return moderationBan2.ReasonMessage;
                    }
                }
                else
                {
                    if (!this._bannedMachines.ContainsKey(machineid))
                    {
                        return string.Empty;
                    }
                    ModerationBan moderationBan3 = this._bannedMachines[Username];
                    if (!moderationBan3.Expired)
                    {
                        return moderationBan3.ReasonMessage;
                    }
                }
            }
            return string.Empty;
        }

        internal string CheckMachineBan(string machineId)
        {
            return this._bannedMachines.ContainsKey(machineId) ? this._bannedMachines[machineId].ReasonMessage : string.Empty;
        }

        internal void BanUser(GameClient client, string moderator, double lengthSeconds, string reason, bool ipBan, bool machine)
        {
            ModerationBanType type = ModerationBanType.UserName;
            string text = client.GetHabbo().UserName;
            string query = "user";
            double num = Azure.GetUnixTimestamp() + lengthSeconds;
            if (ipBan)
            {
                type = ModerationBanType.Ip;
                using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery("SELECT ip_last FROM users WHERE Username=@Username LIMIT 1");
                    queryReactor.AddParameter("Username", text);
                    text = queryReactor.GetString();
                }
                query = "ip";
            }
            if (machine)
            {
                type = ModerationBanType.Machine;
                query = "machine";
                text = client.MachineId;
            }
            var moderationBan = new ModerationBan(type, text, reason, num);
            switch (moderationBan.Type)
            {
                case ModerationBanType.Ip:
                    if (this._bannedIPs.Contains(text))
                    {
                        this._bannedIPs[text] = moderationBan;
                    }
                    else
                    {
                        this._bannedIPs.Add(text, moderationBan);
                    }
                    break;
                case ModerationBanType.Machine:
                    if (this._bannedMachines.ContainsKey(text))
                    {
                        this._bannedMachines[text] = moderationBan;
                    }
                    else
                    {
                        this._bannedMachines.Add(text, moderationBan);
                    }
                    break;
                default:
                    if (this._bannedUsernames.Contains(text))
                    {
                        this._bannedUsernames[text] = moderationBan;
                    }
                    else
                    {
                        this._bannedUsernames.Add(text, moderationBan);
                    }
                    break;
            }
            using (IQueryAdapter queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryreactor2.SetQuery(string.Concat(new object[]
                {
                    "INSERT INTO bans (bantype,value,reason,expire,added_by,added_date) VALUES (@rawvar,@var,@reason,'",
                    num,
                    "',@mod,'",
                    DateTime.Now.ToLongDateString(),
                    "')"
                }));
                queryreactor2.AddParameter("rawvar", query);
                queryreactor2.AddParameter("var", text);
                queryreactor2.AddParameter("reason", reason);
                queryreactor2.AddParameter("mod", moderator);
                queryreactor2.RunQuery();
            }
            if (ipBan)
            {
                DataTable dataTable = null;
                using (IQueryAdapter queryreactor3 = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryreactor3.SetQuery("SELECT id FROM users WHERE ip_last = @var");
                    queryreactor3.AddParameter("var", text);
                    dataTable = queryreactor3.GetTable();
                }
                if (dataTable != null)
                {
                    using (IQueryAdapter queryreactor4 = Azure.GetDatabaseManager().GetQueryReactor())
                    {
                        foreach (DataRow dataRow in dataTable.Rows)
                        {
                            queryreactor4.RunFastQuery(string.Format("UPDATE users_info SET bans = bans + 1 WHERE user_id = {0}", Convert.ToUInt32(dataRow["id"])));
                        }
                    }
                }
                this.BanUser(client, moderator, lengthSeconds, reason, false, false);
                return;
            }
            using (IQueryAdapter queryreactor5 = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryreactor5.RunFastQuery(string.Format("UPDATE users_info SET bans = bans + 1 WHERE user_id = {0}", client.GetHabbo().Id));
            }
            client.Disconnect("banned");
        }

        internal void UnbanUser(string UsernameOrIp)
        {
            this._bannedUsernames.Remove(UsernameOrIp);
            this._bannedIPs.Remove(UsernameOrIp);
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("DELETE FROM bans WHERE value = @userorip");
                queryReactor.AddParameter("userorip", UsernameOrIp);
                queryReactor.RunQuery();
            }
        }
    }
}