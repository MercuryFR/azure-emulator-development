namespace Azure.HabboHotel.Support
{
    internal struct ModerationBan
    {
        internal ModerationBanType Type;
        internal string Variable;
        internal string ReasonMessage;
        internal double Expire;

        internal ModerationBan(ModerationBanType type, string variable, string reasonMessage, double expire)
        {
            this.Type = type;
            this.Variable = variable;
            this.ReasonMessage = reasonMessage;
            this.Expire = expire;
        }

        internal bool Expired
        {
            get
            {
                return Azure.GetUnixTimestamp() >= this.Expire;
            }
        }
    }
}