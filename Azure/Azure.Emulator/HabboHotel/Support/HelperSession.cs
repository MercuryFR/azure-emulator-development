﻿using System.Collections.Generic;
using Azure.HabboHotel.GameClients;

namespace Azure.HabboHotel.Support
{
    internal class HelperSession
    {
        internal GameClient Helper;
        internal GameClient Requester;
        internal List<string> Chats;

        internal HelperSession(GameClient helper, GameClient requester, string question)
        {
            this.Helper = helper;
            this.Requester = requester;
            this.Chats = new List<string> { question };
            this.Response(requester, question);
        }

        internal void Response(GameClient responseClient, string response)
        {
        }
    }
}