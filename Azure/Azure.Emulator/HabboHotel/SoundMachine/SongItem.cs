using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.Items;

namespace Azure.HabboHotel.SoundMachine
{
    internal class SongItem
    {
        internal uint ItemId;
        internal uint SongId;
        internal Item BaseItem;
        internal string ExtraData;
        internal string SongCode;

        public SongItem(uint itemId, uint songId, int baseItem, string extraData, string songCode)
        {
            this.ItemId = itemId;
            this.SongId = songId;
            this.BaseItem = Azure.GetGame().GetItemManager().GetItem(checked((uint)baseItem));
            this.ExtraData = extraData;
            this.SongCode = songCode;
        }

        public SongItem(UserItem item)
        {
            this.ItemId = item.Id;
            this.SongId = SongManager.GetSongId(item.SongCode);
            this.BaseItem = item.BaseItem;
            this.ExtraData = item.ExtraData;
            this.SongCode = item.SongCode;
        }

        internal void SaveToDatabase(uint roomId)
        {
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "REPLACE INTO items_songs VALUES (",
                    this.ItemId,
                    ",",
                    roomId,
                    ",",
                    this.SongId,
                    ")"
                }));
            }
        }

        internal void RemoveFromDatabase()
        {
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Format("DELETE FROM items_songs WHERE itemid = {0}", this.ItemId));
            }
        }
    }
}