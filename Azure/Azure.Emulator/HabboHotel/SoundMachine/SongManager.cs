using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.HabboHotel.SoundMachine
{
    internal class SongManager
    {
        internal static Dictionary<uint, SongData> Songs;
        private static Dictionary<uint, double> _cacheTimer;

        internal static uint GetSongId(string codeName)
        {
            return (from current in Songs.Values where current.CodeName == codeName select current.Id).FirstOrDefault();
        }

        internal static SongData GetSong(string codeName)
        {
            return Songs.Values.FirstOrDefault(current => current.CodeName == codeName);
        }

        internal static SongData GetSongById(uint id)
        {
            return Songs.Values.FirstOrDefault(current => current.Id == id);
        }

        internal static String GetCodeById(uint id)
        {
            return (from current in Songs.Values where current.Id == id select current.CodeName).FirstOrDefault();
        }

        internal static void Initialize()
        {
            Songs = new Dictionary<uint, SongData>();
            _cacheTimer = new Dictionary<uint, double>();
            DataTable table;

            Songs.Clear();
            using (IQueryAdapter queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT * FROM items_songs_data ORDER BY id");
                table = queryReactor.GetTable();
            }
            foreach (SongData songFromDataRow in from DataRow dRow in table.Rows select GetSongFromDataRow(dRow))
            {
                Songs.Add(songFromDataRow.Id, songFromDataRow);
            }
        }

        internal static void ProcessThread()
        {
            double num = Azure.GetUnixTimestamp();
            List<uint> list = (from current in _cacheTimer where num - current.Value >= 180.0 select current.Key).ToList();
            foreach (uint current2 in list)
            {
                Songs.Remove(current2);
                _cacheTimer.Remove(current2);
            }
        }

        internal static SongData GetSongFromDataRow(DataRow dRow)
        {
            return new SongData(Convert.ToUInt32(dRow["id"]), dRow["codename"].ToString(), (string)dRow["name"],
                (string)dRow["artist"], (string)dRow["song_data"], (double)dRow["length"]);
        }

        internal static SongData GetSong(uint songId)
        {
            SongData result;
            Songs.TryGetValue(songId, out result);
            return result;
        }
    }
}