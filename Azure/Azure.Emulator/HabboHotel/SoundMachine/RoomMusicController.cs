using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Rooms;
using Azure.HabboHotel.SoundMachine.Composers;

namespace Azure.HabboHotel.SoundMachine
{
    internal class RoomMusicController
    {
        private static bool _mBroadcastNeeded;
        private Dictionary<uint, SongItem> _mLoadedDisks;
        private SortedDictionary<int, SongInstance> _mPlaylist;
        private double _mStartedPlayingTimestamp;
        private RoomItem _mRoomOutputItem;

        public RoomMusicController()
        {
            this._mLoadedDisks = new Dictionary<uint, SongItem>();
            this._mPlaylist = new SortedDictionary<int, SongInstance>();
        }

        public SongInstance CurrentSong { get; private set; }

        public bool IsPlaying { get; private set; }

        public double TimePlaying
        {
            get
            {
                return Azure.GetUnixTimestamp() - this._mStartedPlayingTimestamp;
            }
        }

        public int SongSyncTimestamp
        {
            get
            {
                if (!this.IsPlaying || this.CurrentSong == null)
                {
                    return 0;
                }
                checked
                {
                    if (this.TimePlaying >= this.CurrentSong.SongData.LengthSeconds)
                    {
                        return (int)this.CurrentSong.SongData.LengthSeconds;
                    }
                    return (int)unchecked(this.TimePlaying * 1000.0);
                }
            }
        }

        public SortedDictionary<int, SongInstance> Playlist
        {
            get
            {
                var sortedDictionary = new SortedDictionary<int, SongInstance>();
                lock (this._mPlaylist)
                {
                    foreach (KeyValuePair<int, SongInstance> current in this._mPlaylist)
                    {
                        sortedDictionary.Add(current.Key, current.Value);
                    }
                }
                return sortedDictionary;
            }
        }

        public int PlaylistCapacity
        {
            get
            {
                return 20;
            }
        }

        public int PlaylistSize
        {
            get
            {
                return this._mPlaylist.Count;
            }
        }

        public bool HasLinkedItem
        {
            get
            {
                return this._mRoomOutputItem != null;
            }
        }

        public uint LinkedItemId
        {
            get
            {
                return this._mRoomOutputItem == null ? 0u : this._mRoomOutputItem.Id;
            }
        }

        public int SongQueuePosition { get; private set; }

        public void LinkRoomOutputItem(RoomItem item)
        {
            this._mRoomOutputItem = item;
        }

        public int AddDisk(SongItem diskItem)
        {
            uint songId = diskItem.SongId;
            if (songId == 0u)
            {
                return -1;
            }
            SongData song = SongManager.GetSong(songId);
            if (song == null)
            {
                return -1;
            }
            if (this._mLoadedDisks.ContainsKey(diskItem.ItemId))
            {
                return -1;
            }
            this._mLoadedDisks.Add(diskItem.ItemId, diskItem);
            int count = this._mPlaylist.Count;
            lock (this._mPlaylist)
            {
                this._mPlaylist.Add(count, new SongInstance(diskItem, song));
            }
            return count;
        }

        public SongItem RemoveDisk(int playlistIndex)
        {
            SongInstance songInstance;
            lock (this._mPlaylist)
            {
                if (!this._mPlaylist.ContainsKey(playlistIndex))
                {
                    return null;
                }
                songInstance = this._mPlaylist[playlistIndex];
                this._mPlaylist.Remove(playlistIndex);
            }
            lock (this._mLoadedDisks)
            {
                this._mLoadedDisks.Remove(songInstance.DiskItem.ItemId);
            }
            this.RepairPlaylist();
            if (playlistIndex == this.SongQueuePosition)
            {
                this.PlaySong();
            }
            return songInstance.DiskItem;
        }

        public void Update(Room instance)
        {
            if (this.IsPlaying && (this.CurrentSong == null || this.TimePlaying >= this.CurrentSong.SongData.LengthSeconds + 1.0))
            {
                if (!this._mPlaylist.Any())
                {
                    this.Stop();
                    this._mRoomOutputItem.ExtraData = "0";
                    this._mRoomOutputItem.UpdateState();
                }
                else
                {
                    this.SetNextSong();
                }
                _mBroadcastNeeded = true;
            }
            if (!_mBroadcastNeeded)
                return;
            this.BroadcastCurrentSongData(instance);
            _mBroadcastNeeded = false;
        }

        public void RepairPlaylist()
        {
            List<SongItem> list;
            lock (this._mLoadedDisks)
            {
                list = this._mLoadedDisks.Values.ToList();
                this._mLoadedDisks.Clear();
            }
            lock (this._mPlaylist)
            {
                this._mPlaylist.Clear();
            }
            foreach (SongItem current in list)
            {
                this.AddDisk(current);
            }
        }

        public void SetNextSong()
        {
            checked
            {
                this.SongQueuePosition++;
                this.PlaySong();
            }
        }

        public void PlaySong()
        {
            if (this.SongQueuePosition >= this._mPlaylist.Count)
            {
                this.SongQueuePosition = 0;
            }
            if (!this._mPlaylist.Any())
            {
                this.Stop();
                return;
            }
            this.CurrentSong = this._mPlaylist[this.SongQueuePosition];
            this._mStartedPlayingTimestamp = Azure.GetUnixTimestamp();
            _mBroadcastNeeded = true;
        }

        public void Start()
        {
            this.IsPlaying = true;
            this.SongQueuePosition = -1;
            this.SetNextSong();
        }

        public void Stop()
        {
            this.CurrentSong = null;
            this.IsPlaying = false;
            this.SongQueuePosition = -1;
            _mBroadcastNeeded = true;
        }

        public void Reset()
        {
            lock (this._mLoadedDisks)
            {
                this._mLoadedDisks.Clear();
            }
            lock (this._mPlaylist)
            {
                this._mPlaylist.Clear();
            }
            this._mRoomOutputItem = null;
            this.SongQueuePosition = -1;
            this._mStartedPlayingTimestamp = 0.0;
        }

        internal void BroadcastCurrentSongData(Room instance)
        {
            if (this.CurrentSong != null)
            {
                instance.SendMessage(JukeboxComposer.ComposePlayingComposer(this.CurrentSong.SongData.Id, this.SongQueuePosition, 0));
                return;
            }
            instance.SendMessage(JukeboxComposer.ComposePlayingComposer(0u, 0, 0));
        }

        internal void OnNewUserEnter(RoomUser user)
        {
            if (user.IsBot || user.GetClient() == null || this.CurrentSong == null)
            {
                return;
            }
            user.GetClient()
                .SendMessage(JukeboxComposer.ComposePlayingComposer(this.CurrentSong.SongData.Id, this.SongQueuePosition,
                    this.SongSyncTimestamp));
        }

        internal void Destroy()
        {
            if (this._mLoadedDisks != null)
            {
                this._mLoadedDisks.Clear();
            }
            if (this._mPlaylist != null)
            {
                this._mPlaylist.Clear();
            }
            this._mPlaylist = null;
            this._mLoadedDisks = null;
            this.CurrentSong = null;
            this._mRoomOutputItem = null;
        }
    }
}