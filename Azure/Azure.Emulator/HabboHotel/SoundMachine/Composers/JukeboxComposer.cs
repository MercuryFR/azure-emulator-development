using System.Collections.Generic;
using System.Collections.Specialized;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.SoundMachine.Composers
{
    internal class JukeboxComposer
    {
        public static ServerMessage Compose(List<SongData> songs)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SongsMessageComposer"));
            serverMessage.AppendInteger(songs.Count);
            foreach (SongData current in songs)
            {
                serverMessage.AppendInteger(current.Id);
                serverMessage.AppendString(current.CodeName);
                serverMessage.AppendString(current.Name);
                serverMessage.AppendString(current.Data);
                serverMessage.AppendInteger(current.LengthMiliseconds);
                serverMessage.AppendString(current.Artist);
            }
            return serverMessage;
        }

        public static ServerMessage ComposePlayingComposer(uint songId, int playlistItemNumber, int syncTimestampMs)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("JukeboxNowPlayingMessageComposer"));
            if (songId == 0u)
            {
                serverMessage.AppendInteger(-1);
                serverMessage.AppendInteger(-1);
                serverMessage.AppendInteger(-1);
                serverMessage.AppendInteger(-1);
                serverMessage.AppendInteger(0);
            }
            else
            {
                serverMessage.AppendInteger(songId);
                serverMessage.AppendInteger(playlistItemNumber);
                serverMessage.AppendInteger(songId);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(syncTimestampMs);
            }
            return serverMessage;
        }

        internal static ServerMessage Compose(GameClient session)
        {
            return session.GetHabbo().GetInventoryComponent().SerializeMusicDiscs();
        }

        internal static ServerMessage Compose(int playlistCapacity, List<SongInstance> playlist)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("JukeboxPlaylistMessageComposer"));
            serverMessage.AppendInteger(playlistCapacity);
            serverMessage.AppendInteger(playlist.Count);
            foreach (SongInstance current in playlist)
            {
                serverMessage.AppendInteger(current.DiskItem.ItemId);
                serverMessage.AppendInteger(current.SongData.Id);
            }
            return serverMessage;
        }

        internal static ServerMessage Compose(uint songId, int playlistItemNumber, int syncTimestampMs)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("JukeboxNowPlayingMessageComposer"));
            if (songId == 0u)
            {
                serverMessage.AppendInteger(-1);
                serverMessage.AppendInteger(-1);
                serverMessage.AppendInteger(-1);
                serverMessage.AppendInteger(-1);
                serverMessage.AppendInteger(0);
            }
            else
            {
                serverMessage.AppendInteger(songId);
                serverMessage.AppendInteger(playlistItemNumber);
                serverMessage.AppendInteger(songId);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(syncTimestampMs);
            }
            return serverMessage;
        }

        internal static ServerMessage SerializeSongInventory(HybridDictionary songs)
        {

            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SongsLibraryMessageComposer"));
            serverMessage.AppendInteger(songs.Count);
            foreach (UserItem userItem in songs.Values)
            {
                    serverMessage.AppendInteger(userItem.Id);
                    serverMessage.AppendInteger(SongManager.GetSong(userItem.SongCode).Id);
            }
            return serverMessage;
        }
    }
}