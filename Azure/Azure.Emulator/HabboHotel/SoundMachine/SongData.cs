namespace Azure.HabboHotel.SoundMachine
{
    internal class SongData
    {
        public SongData(uint id, string codeName, string name, string artist, string data, double length)
        {
            this.Id = id;
            this.CodeName = codeName;
            this.Name = name;
            this.Artist = artist;
            this.Data = data;
            this.LengthSeconds = length;
        }

        public uint Id { get; private set; }

        public string CodeName { get; private set; }

        public string Name { get; private set; }

        public string Artist { get; private set; }

        public string Data { get; private set; }

        public double LengthSeconds { get; private set; }

        public int LengthMiliseconds
        {
            get
            {
                return checked((int)unchecked(this.LengthSeconds * 1000.0));
            }
        }
    }
}