namespace Azure.HabboHotel.SoundMachine
{
    internal class SongInstance
    {
        public SongInstance(SongItem item, SongData songData)
        {
            this.DiskItem = item;
            this.SongData = songData;
        }

        public SongItem DiskItem { get; private set; }

        public SongData SongData { get; private set; }
    }
}