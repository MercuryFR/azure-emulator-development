namespace Azure.HabboHotel.Users.Inventory
{
    class AvatarEffect
    {
        internal int EffectId;
        internal int TotalDuration;
        internal bool Activated;
        internal double StampActivated;

        internal AvatarEffect(int effectId, int totalDuration, bool activated, double activateTimestamp)
        {
            EffectId = effectId;
            TotalDuration = totalDuration;
            Activated = activated;
            StampActivated = activateTimestamp;
        }

        internal int TimeLeft
        {
            get
            {
                if (!Activated)
                    return -1;
                var num = Azure.GetUnixTimestamp() - StampActivated;
                if (num >= TotalDuration)
                    return 0;
                return checked((int) unchecked(TotalDuration - num));
            }
        }

        internal bool HasExpired
        {
            get { return TimeLeft != -1 && TimeLeft <= 0; }
        }

        internal void Activate()
        {
            Activated = true;
            StampActivated = Azure.GetUnixTimestamp();
        }
    }
}