using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Rooms;
using Azure.HabboHotel.Users.UserDataManagement;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Users.Inventory
{
    class AvatarEffectsInventoryComponent
    {
        internal int CurrentEffect;
        private readonly uint _userId;
        private List<AvatarEffect> _effects;
        private GameClient _session;

        internal AvatarEffectsInventoryComponent(uint userId, GameClient client, UserData data)
        {
            _userId = userId;
            _session = client;
            _effects = new List<AvatarEffect>();
            foreach (var current in data.Effects)
                if (!current.HasExpired)
                    _effects.Add(current);
                else
                    using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                        queryReactor.RunFastQuery(string.Concat(new object[]
                        {
                            "DELETE FROM users_effects WHERE user_id = ",
                            userId,
                            " AND effect_id = ",
                            current.EffectId,
                            "; "
                        }));
        }

        internal ServerMessage GetPacket()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("EffectsInventoryMessageComposer"));
            serverMessage.AppendInteger(_effects.Count);
            foreach (var current in _effects)
            {
                serverMessage.AppendInteger(current.EffectId);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(current.TotalDuration);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(current.TimeLeft);
            }
            return serverMessage;
        }

        internal void AddNewEffect(int effectId, int duration)
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "INSERT INTO users_effects (user_id,effect_id,total_duration,is_activated,activated_stamp) VALUES (",
                    _userId,
                    ",",
                    effectId,
                    ",",
                    duration,
                    ",'0',0)"
                }));
            _effects.Add(new AvatarEffect(effectId, duration, false, 0.0));
            GetClient()
                .GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("AddEffectToInventoryMessageComposer"));
            GetClient().GetMessageHandler().GetResponse().AppendInteger(effectId);
            GetClient().GetMessageHandler().GetResponse().AppendInteger(0);
            GetClient().GetMessageHandler().GetResponse().AppendInteger(duration);
            GetClient().GetMessageHandler().SendResponse();
        }

        internal bool HasEffect(int effectId)
        {
            return effectId < 1 || (
                from x in _effects
                where x.EffectId == effectId
                select x).Any();
        }

        internal void ActivateEffect(int effectId)
        {
            if (!_session.GetHabbo().InRoom)
                return;
            if (!HasEffect(effectId))
                return;
            if (effectId < 1)
            {
                ActivateCustomEffect(effectId);
                return;
            }
            var avatarEffect = (
                from x in _effects
                where x.EffectId == effectId
                select x).Last<AvatarEffect>();
            avatarEffect.Activate();
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE users_effects SET is_activated = '1', activated_stamp = ",
                    Azure.GetUnixTimestamp(),
                    " WHERE user_id = ",
                    _userId,
                    " AND effect_id = ",
                    effectId
                }));
            EnableInRoom(effectId);
        }

        internal void ActivateCustomEffect(int effectId, bool setAsCurrentEffect = true)
        {
            EnableInRoom(effectId, setAsCurrentEffect);
        }

        internal void OnRoomExit() { CurrentEffect = 0; }

        internal void CheckExpired()
        {
            if (!_effects.Any())
                return;
            var list = _effects.Where(current => current.HasExpired).ToList();
            foreach (var current2 in list)
                StopEffect(current2.EffectId);
            list.Clear();
        }

        internal void StopEffect(int effectId)
        {
            var avatarEffect = (
                from x in _effects
                where x.EffectId == effectId
                select x).Last<AvatarEffect>();
            if (avatarEffect == null || !avatarEffect.HasExpired)
                return;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "DELETE FROM users_effects WHERE user_id = ",
                    _userId,
                    " AND effect_id = ",
                    effectId,
                    " AND is_activated = 1"
                }));
            _effects.Remove(avatarEffect);
            GetClient()
                .GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("StopAvatarEffectMessageComposer"));
            GetClient().GetMessageHandler().GetResponse().AppendInteger(effectId);
            GetClient().GetMessageHandler().SendResponse();
            if (CurrentEffect >= 0)
                ActivateCustomEffect(-1);
        }

        internal void Dispose()
        {
            _effects.Clear();
            _effects = null;
            _session = null;
        }

        internal GameClient GetClient() { return _session; }

        private void EnableInRoom(int effectId, bool setAsCurrentEffect = true)
        {
            var userRoom = GetUserRoom();
            if (userRoom == null)
                return;
            var roomUserByHabbo = userRoom.GetRoomUserManager().GetRoomUserByHabbo(GetClient().GetHabbo().Id);
            if (roomUserByHabbo == null)
                return;
            if (setAsCurrentEffect)
                CurrentEffect = effectId;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ApplyEffectMessageComposer"));
            serverMessage.AppendInteger(roomUserByHabbo.VirtualId);
            serverMessage.AppendInteger(effectId);
            serverMessage.AppendInteger(0);
            userRoom.SendMessage(serverMessage);
        }

        private Room GetUserRoom() { return _session.GetHabbo().CurrentRoom; }
    }
}