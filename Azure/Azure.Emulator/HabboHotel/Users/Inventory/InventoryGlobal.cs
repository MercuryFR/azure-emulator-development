using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Users.UserDataManagement;

namespace Azure.HabboHotel.Users.Inventory
{
    class InventoryGlobal
    {
        internal static InventoryComponent GetInventory(uint userId, GameClient client, UserData data)
        {
            return new InventoryComponent(userId, client, data);
        }
    }
}