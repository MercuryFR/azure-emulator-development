using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using Azure.Configuration;
using Azure.HabboHotel.Catalogs;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.RoomBots;
using Azure.HabboHotel.Users.UserDataManagement;
using Azure.Messages;
using Azure.Messages.Parsers;
using Azure.Util;

namespace Azure.HabboHotel.Users.Inventory
{
    class InventoryComponent
    {
        internal uint UserId;
        private readonly HybridDictionary _floorItems;
        private readonly HybridDictionary _wallItems;
        private readonly HybridDictionary _inventoryPets;
        private readonly HybridDictionary _mAddedItems;
        private readonly HybridDictionary _mRemovedItems;
        private readonly HybridDictionary _inventoryBots;
        private GameClient _mClient;
        private bool _isUpdated;
        private bool _userAttatched;

        internal InventoryComponent(uint userId, GameClient client, UserData userData)
        {
            _mClient = client;
            UserId = userId;
            _floorItems = new HybridDictionary();
            _wallItems = new HybridDictionary();
            SongDisks = new HybridDictionary();
            foreach (var current in userData.Inventory)
            {
                if (current.BaseItem.InteractionType == InteractionType.musicdisc)
                    SongDisks.Add(current.Id, current);
                if (current.IsWallItem)
                    _wallItems.Add(current.Id, current);
                else
                    _floorItems.Add(current.Id, current);
            }
            _inventoryPets = new HybridDictionary();
            _inventoryBots = new HybridDictionary();
            _mAddedItems = new HybridDictionary();
            _mRemovedItems = new HybridDictionary();
            _isUpdated = false;

            foreach (var bot in userData.Bots)
                AddBot(bot.Value);
        }

        public bool IsInactive
        {
            get { return !_userAttatched; }
        }

        internal bool NeedsUpdate
        {
            get { return !_userAttatched && !_isUpdated; }
        }

        internal HybridDictionary SongDisks { get; private set; }

        internal void ClearItems()
        {
            UpdateItems(true);
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Format("DELETE FROM items_rooms WHERE room_id='0' AND user_id = {0}",
                    UserId));
            _mAddedItems.Clear();
            _mRemovedItems.Clear();
            _floorItems.Clear();
            _wallItems.Clear();
            SongDisks.Clear();
            _inventoryPets.Clear();
            _isUpdated = true;
            _mClient.GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("UpdateInventoryMessageComposer"));
            GetClient().GetMessageHandler().SendResponse();
        }

        internal void Redeemcredits(GameClient session)
        {
            var currentRoom = session.GetHabbo().CurrentRoom;
            if (currentRoom == null)
                return;
            DataTable table;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT id FROM items_rooms WHERE user_id={0} AND room_id='0'",
                    session.GetHabbo().Id));
                table = queryReactor.GetTable();
            }
            checked
            {
                foreach (DataRow dataRow in table.Rows)
                {
                    var item = GetItem(Convert.ToUInt32(dataRow[0]));
                    if (item == null ||
                        (!item.BaseItem.Name.StartsWith("CF_") && !item.BaseItem.Name.StartsWith("CFC_")))
                        continue;
                    var array = item.BaseItem.Name.Split('_');
                    var num = int.Parse(array[1]);
                    using (var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
                        queryreactor2.RunFastQuery(string.Format("DELETE FROM items_rooms WHERE id={0} LIMIT 1",
                            item.Id));
                    if (currentRoom.GetRoomItemHandler().GetItem(item.Id) != null)
                    {
                        var item2 = currentRoom.GetRoomItemHandler().GetItem(item.Id);
                        currentRoom.GetRoomItemHandler().RemoveItem(item2);
                    }
                    RemoveItem(item.Id, false);
                    if (num <= 0)
                        continue;
                    session.GetHabbo().Credits += num;
                    session.GetHabbo().UpdateCreditsBalance();
                }
            }
        }

        internal void SetActiveState(GameClient client)
        {
            _mClient = client;
            _userAttatched = true;
        }

        internal void SetIdleState()
        {
            _userAttatched = false;
            _mClient = null;
        }

        internal Pet GetPet(uint id) { return _inventoryPets.Contains(id) ? _inventoryPets[id] as Pet : null; }

        internal bool RemovePet(uint petId)
        {
            _isUpdated = false;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("604)"));
            serverMessage.AppendInteger(petId);
            GetClient().SendMessage(serverMessage);
            _inventoryPets.Remove(petId);
            return true;
        }

        internal void MovePetToRoom(uint petId)
        {
            _isUpdated = false;
            RemovePet(petId);
        }

        internal void AddPet(Pet pet)
        {
            _isUpdated = false;
            if (pet == null || _inventoryPets.Contains(pet.PetId))
                return;
            pet.PlacedInRoom = false;
            pet.RoomId = 0u;
            _inventoryPets.Add(pet.PetId, pet);

            SerializePetInventory();
        }

        internal void LoadInventory()
        {
            _floorItems.Clear();
            _wallItems.Clear();
            DataTable table;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("SELECT * FROM items_rooms WHERE user_id=@userid AND room_id='0' LIMIT 8000;");
                queryReactor.AddParameter("userid", checked((int) UserId));
                table = queryReactor.GetTable();
            }
            foreach (DataRow dataRow in table.Rows)
            {
                var id = Convert.ToUInt32(dataRow[0]);
                var itemId = Convert.ToUInt32(dataRow[3]);

                if (!Azure.GetGame().GetItemManager().ContainsItem(itemId))
                    continue;

                string extraData;
                if (!DBNull.Value.Equals(dataRow[4]))
                    extraData = (string) dataRow[4];
                else
                    extraData = string.Empty;
                var group = Convert.ToUInt32(dataRow[10]);
                string songCode;
                if (!DBNull.Value.Equals(dataRow["songcode"]))
                    songCode = (string) dataRow["songcode"];
                else
                    songCode = string.Empty;
                var userItem = new UserItem(id, itemId, extraData, group, songCode);

                if (userItem.BaseItem.InteractionType == InteractionType.musicdisc && !SongDisks.Contains(id))
                    SongDisks.Add(id, userItem);
                if (userItem.IsWallItem)
                {
                    if (!_wallItems.Contains(id))
                        _wallItems.Add(id, userItem);
                }
                else if (!_floorItems.Contains(id))
                    _floorItems.Add(id, userItem);
            }
            SongDisks.Clear();
            _inventoryPets.Clear();
            _inventoryBots.Clear();

            using (var queryReactor2 = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor2.SetQuery(
                    string.Format("SELECT * FROM bots WHERE user_id = {0} AND room_id = 0",
                        UserId));
                var table2 = queryReactor2.GetTable();
                if (table2 == null)
                    return;
                foreach (DataRow botRow in table2.Rows)
                    switch ((string) botRow["ai_type"])
                    {
                        case "pet":
                        {
                            queryReactor2.SetQuery(string.Format("SELECT * FROM pets_data WHERE id={0} LIMIT 1",
                                botRow[0]));
                            var row = queryReactor2.GetRow();
                            if (row == null)
                                continue;
                            var pet = Catalog.GeneratePetFromRow(botRow, row);
                            if (_inventoryPets.Contains(pet.PetId))
                                _inventoryPets.Remove(pet.PetId);
                            _inventoryPets.Add(pet.PetId, pet);
                        }
                            break;
                        case "generic":
                            AddBot(BotManager.GenerateBotFromRow(botRow));
                            break;
                    }
            }
        }

        internal void UpdateItems(bool fromDatabase)
        {
            if (fromDatabase)
            {
                RunDbUpdate();
                LoadInventory();
            }
            _mClient.GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("UpdateInventoryMessageComposer"));
            _mClient.GetMessageHandler().SendResponse();
        }

        internal UserItem GetItem(uint id)
        {
            _isUpdated = false;
            if (_floorItems.Contains(id))
                return (UserItem) _floorItems[id];
            if (_wallItems.Contains(id))
                return (UserItem) _wallItems[id];
            return null;
        }

        internal void AddBot(RoomBot bot)
        {
            _isUpdated = false;
            if (bot == null)
            {
                Logging.WriteLine("Bot was null", ConsoleColor.Violet);
                return;
            }
            if (_inventoryBots.Contains(bot.BotId))
                Logging.WriteLine("Contains Bot", ConsoleColor.Violet);
            bot.RoomId = 0u;
            _inventoryBots.Add(bot.BotId, bot);
        }

        internal RoomBot GetBot(uint id) { return _inventoryBots.Contains(id) ? _inventoryBots[id] as RoomBot : null; }

        internal bool RemoveBot(uint petId)
        {
            _isUpdated = false;
            if (_inventoryBots.Contains(petId))
                _inventoryBots.Remove(petId);
            return true;
        }

        internal void MoveBotToRoom(uint petId)
        {
            _isUpdated = false;
            RemoveBot(petId);
        }

        internal UserItem AddNewItem(uint id, uint baseItem, string extraData, uint @group, bool insert, bool fromRoom,
            int limno, int limtot, string songCode = "")
        {
            _isUpdated = false;
            if (insert)
            {
                if (fromRoom)
                    using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                    {
                        queryReactor.RunFastQuery(
                            string.Format("UPDATE items_rooms SET room_id='0' WHERE id={0} LIMIT 1", id));
                        goto IL_1C9;
                    }
                using (var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryreactor2.SetQuery(string.Concat(new object[]
                    {
                        "INSERT INTO items_rooms (base_item, user_id, group_id) VALUES (",
                        baseItem,
                        ", ",
                        UserId,
                        ", ",
                        @group,
                        ")"
                    }));
                    id = checked((uint) queryreactor2.InsertQuery());
                    SendNewItems(id);

                    if (!string.IsNullOrEmpty(extraData))
                    {
                        queryreactor2.SetQuery(
                            string.Format("UPDATE items_rooms SET extra_data=@extradata WHERE id={0} LIMIT 1", id));
                        queryreactor2.AddParameter("extradata", extraData);
                        queryreactor2.RunQuery();
                    }
                    if (limno > 0)
                        queryreactor2.RunFastQuery(string.Concat(new object[]
                        {
                            "INSERT INTO items_limited VALUES (",
                            id,
                            ", ",
                            limno,
                            ", ",
                            limtot,
                            ")"
                        }));
                    if (!string.IsNullOrEmpty(songCode))
                    {
                        queryreactor2.SetQuery(
                            string.Format("UPDATE items_rooms SET songcode=@song_code WHERE id={0} LIMIT 1", id));
                        queryreactor2.AddParameter("song_code", songCode);
                        queryreactor2.RunQuery();
                    }
                }
            }
            IL_1C9:
            var userItem = new UserItem(id, baseItem, extraData, @group, songCode);
            if (UserHoldsItem(id))
                RemoveItem(id, false);
            if (userItem.BaseItem.InteractionType == InteractionType.musicdisc)
                SongDisks.Add(userItem.Id, userItem);
            if (userItem.IsWallItem)
                _wallItems.Add(userItem.Id, userItem);
            else
                _floorItems.Add(userItem.Id, userItem);
            if (_mRemovedItems.Contains(id))
                _mRemovedItems.Remove(id);
            if (!_mAddedItems.Contains(id))
                _mAddedItems.Add(id, userItem);
            return userItem;
        }

        internal void RemoveItem(uint id, bool placedInroom)
        {
            if (GetClient() == null || GetClient().GetHabbo() == null ||
                GetClient().GetHabbo().GetInventoryComponent() == null)
                GetClient().Disconnect("user null RemoveItem");
            _isUpdated = false;
            GetClient()
                .GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("RemoveInventoryObjectMessageComposer"));
            GetClient().GetMessageHandler().GetResponse().AppendInteger(id);
            //this.GetClient().GetMessageHandler().GetResponse().AppendInt32(Convert.ToInt32(this.GetClient().GetHabbo().Id));
            GetClient().GetMessageHandler().SendResponse();
            if (_mAddedItems.Contains(id))
                _mAddedItems.Remove(id);
            if (_mRemovedItems.Contains(id))
                return;
            var item = GetClient().GetHabbo().GetInventoryComponent().GetItem(id);
            SongDisks.Remove(id);
            _floorItems.Remove(id);
            _wallItems.Remove(id);
            _mRemovedItems.Add(id, item);
        }

        internal ServerMessage SerializeFloorItemInventory()
        {
            var i = checked(_floorItems.Count + SongDisks.Count + _wallItems.Count);
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("LoadInventoryMessageComposer"));
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(0);
            serverMessage.AppendInteger(i);
            foreach (UserItem userItem in _floorItems.Values)
                userItem.SerializeFloor(serverMessage, true);
            foreach (UserItem userItem2 in SongDisks.Values)
                userItem2.SerializeFloor(serverMessage, true);
            foreach (UserItem userItem3 in _wallItems.Values)
                userItem3.SerializeWall(serverMessage, true);
            return serverMessage;
        }

        internal ServerMessage SerializeWallItemInventory()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("LoadInventoryMessageComposer"));
            serverMessage.AppendString("I");
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(_wallItems.Count);
            foreach (UserItem userItem in _wallItems.Values)
                userItem.SerializeWall(serverMessage, true);
            return serverMessage;
        }

        internal ServerMessage SerializePetInventory()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("PetInventoryMessageComposer"));
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(_inventoryPets.Count);
            foreach (Pet current in _inventoryPets.Values)
                current.SerializeInventory(serverMessage);
            return serverMessage;
        }

        internal ServerMessage SerializeBotInventory()
        {
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("BotInventoryMessageComposer"));

            serverMessage.AppendInteger(_inventoryBots.Count);
            foreach (RoomBot current in _inventoryBots.Values)
            {
                serverMessage.AppendInteger(current.BotId);
                serverMessage.AppendString(current.Name);
                serverMessage.AppendString(current.Motto);
                serverMessage.AppendString("m");
                serverMessage.AppendString(current.Look);
            }
            return serverMessage;
        }

        internal void AddItemArray(List<RoomItem> roomItemList)
        {
            foreach (var current in roomItemList)
                AddItem(current);
        }

        internal void AddItem(RoomItem item)
        {
            AddNewItem(item.Id, item.BaseItem, item.ExtraData, item.GroupId, true, true, 0, 0, item.SongCode);
        }

        internal void RunCycleUpdate()
        {
            _isUpdated = true;
            RunDbUpdate();
        }

        internal void RunDbUpdate()
        {
            try
            {
                if (_mRemovedItems.Count <= 0 && _mAddedItems.Count <= 0 && _inventoryPets.Count <= 0)
                    return;
                var queryChunk = new QueryChunk();
                if (_mAddedItems.Count > 0)
                {
                    foreach (UserItem userItem in _mAddedItems.Values)
                        queryChunk.AddQuery(string.Concat(new object[]
                        {
                            "UPDATE items_rooms SET user_id = ",
                            UserId,
                            ", room_id='0' WHERE id = ",
                            userItem.Id
                        }));
                    _mAddedItems.Clear();
                }
                if (_mRemovedItems.Count > 0)
                {
                    try
                    {
                        foreach (UserItem userItem2 in _mRemovedItems.Values)
                        {
                            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                                GetClient()
                                    .GetHabbo()
                                    .CurrentRoom.GetRoomItemHandler()
                                    .SaveFurniture(queryReactor);
                            if (SongDisks.Contains(userItem2.Id))
                                SongDisks.Remove(userItem2.Id);
                        }
                    }
                    catch (Exception)
                    {
                    }
                    _mRemovedItems.Clear();
                }
                foreach (Pet current in _inventoryPets.Values)
                {
                    if (current.DbState == DatabaseUpdateState.NeedsUpdate)
                    {
                        queryChunk.AddParameter(string.Format("{0}name", current.PetId), current.Name);
                        queryChunk.AddParameter(string.Format("{0}race", current.PetId), current.Race);
                        queryChunk.AddParameter(string.Format("{0}color", current.PetId), current.Color);
                        queryChunk.AddQuery(string.Concat(new object[]
                        {
                            "UPDATE bots SET room_id = ",
                            current.RoomId,
                            ", name = @",
                            current.PetId,
                            "name, x = ",
                            current.X,
                            ", Y = ",
                            current.Y,
                            ", Z = ",
                            current.Z,
                            " WHERE id = ",
                            current.PetId
                        }));
                        queryChunk.AddQuery(string.Concat(new object[]
                        {
                            "UPDATE pets_data SET race = @",
                            current.PetId,
                            "race, color = @",
                            current.PetId,
                            "color, type = ",
                            current.Type,
                            ", experience = ",
                            current.Experience,
                            ", energy = ",
                            current.Energy,
                            ", nutrition = ",
                            current.Nutrition,
                            ", respect = ",
                            current.Respect,
                            ", createstamp = '",
                            current.CreationStamp,
                            "', lasthealth_stamp = ",
                            Azure.DateTimeToUnix(current.LastHealth),
                            ", untilgrown_stamp = ",
                            Azure.DateTimeToUnix(current.UntilGrown),
                            " WHERE id = ",
                            current.PetId
                        }));
                    }
                    current.DbState = DatabaseUpdateState.Updated;
                }
                using (var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
                    queryChunk.Execute(queryreactor2);
            }
            catch (Exception ex)
            {
                Logging.LogCacheError(string.Format("FATAL ERROR DURING USER INVENTORY DB UPDATE: {0}", ex));
            }
        }

        internal ServerMessage SerializeMusicDiscs()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SongsLibraryMessageComposer"));
            serverMessage.AppendInteger(SongDisks.Count);
            foreach (var current in
                from x in _floorItems.Values.OfType<UserItem>()
                where x.BaseItem.InteractionType == InteractionType.musicdisc
                select x)
            {
                uint i;
                uint.TryParse(current.ExtraData, out i);
                serverMessage.AppendInteger(current.Id);
                serverMessage.AppendInteger(i);
            }
            return serverMessage;
        }

        internal List<Pet> GetPets()
        {
            return _inventoryPets.Values.Cast<Pet>().ToList();
        }

        internal void SendFloorInventoryUpdate() { _mClient.SendMessage(SerializeFloorItemInventory()); }

        internal void SendNewItems(uint id)
        {
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("NewInventoryObjectMessageComposer"));
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(id);
            _mClient.SendMessage(serverMessage);
        }

        private bool UserHoldsItem(uint itemId)
        {
            return SongDisks.Contains(itemId) || _floorItems.Contains(itemId) || _wallItems.Contains(itemId);
        }

        private GameClient GetClient()
        {
            return Azure.GetGame().GetClientManager().GetClientByUserId(UserId);
        }
    }
}