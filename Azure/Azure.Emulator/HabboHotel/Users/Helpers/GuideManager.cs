﻿using System;
using System.Collections.Generic;
using Azure.HabboHotel.GameClients;

namespace Azure.HabboHotel.Guides
{
    internal class GuideManager
    {
        public Dictionary<uint, GameClient> EnCours = new Dictionary<uint, GameClient>();
        //internal int HelpersCount = 0;
        //internal int GuardiansCount = 0;
        internal List<GameClient> GuidesOnDuty = new List<GameClient>();

        public int GuidesCount
        {
            get
            {
                return this.GuidesOnDuty.Count;
            }
            set
            {
            }
        }

        /*public int Helpers
        {
        get { return HelpersCount; }
        set { HelpersCount = value; }
        }
        public int Guardians
        {
        get { return GuardiansCount; }
        set { GuardiansCount = value; }
        }*/
        public GameClient GetRandomGuide()
        {
            var random = new Random();
            return this.GuidesOnDuty[random.Next(0, this.GuidesCount - 1)];
        }

        public void AddGuide(GameClient guide)
        {
            if (!this.GuidesOnDuty.Contains(guide))
            {
                this.GuidesOnDuty.Add(guide);
            }
        }

        public void RemoveGuide(GameClient guide)
        {
            if (this.GuidesOnDuty.Contains(guide))
            {
                this.GuidesOnDuty.Remove(guide);
            }
        }
    }
}