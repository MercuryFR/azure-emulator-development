namespace Azure.HabboHotel.Users.Relationships
{
    class Relationship
    {
        public int Id;
        public int UserId;
        public int Type;

        public Relationship(int id, int user, int type)
        {
            Id = id;
            UserId = user;
            Type = type;
        }
    }
}