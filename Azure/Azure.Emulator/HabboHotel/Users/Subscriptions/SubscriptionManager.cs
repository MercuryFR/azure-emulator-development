using System;
using Azure.HabboHotel.Users.UserDataManagement;

namespace Azure.HabboHotel.Users.Subscriptions
{
    class SubscriptionManager
    {
        private readonly uint _userId;
        private Subscription _subscription;

        internal SubscriptionManager(uint userId, UserData userData)
        {
            _userId = userId;
            _subscription = userData.Subscriptions;
        }

        internal bool HasSubscription
        {
            get { return _subscription != null && _subscription.IsValid; }
        }

        internal Subscription GetSubscription() { return _subscription; }

        internal void AddSubscription(double dayLength)
        {
            var num = checked((int) Math.Round(dayLength));
            var clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(_userId);
            DateTime target;
            int num2;
            int num3;
            if (_subscription != null)
            {
                target = Azure.UnixToDateTime(_subscription.ExpireTime).AddDays(num);
                num2 = _subscription.ActivateTime;
                num3 = _subscription.LastGiftTime;
            }
            else
            {
                target = DateTime.Now.AddDays(num);
                num2 = Azure.GetUnixTimestamp();
                num3 = Azure.GetUnixTimestamp();
            }
            var num4 = Azure.DateTimeToUnix(target);
            _subscription = new Subscription(2, num2, num4, num3);
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(
                    string.Concat(new object[]
                    {"REPLACE INTO users_subscriptions VALUES (", _userId, ", 2, ", num2, ", ", num4, ", ", num3, ");"}));
            clientByUserId.GetHabbo().SerializeClub();
            Azure.GetGame().GetAchievementManager().TryProgressHabboClubAchievements(clientByUserId);
        }

        internal void ReloadSubscription()
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    "SELECT * FROM users_subscriptions WHERE user_id=@id AND timestamp_expire > UNIX_TIMESTAMP() ORDER BY subscription_id DESC LIMIT 1");
                queryReactor.AddParameter("id", _userId);
                var row = queryReactor.GetRow();
                _subscription = row == null
                    ? null
                    : new Subscription((int) row[1], (int) row[2], (int) row[3], (int) row[4]);
            }
        }
    }
}