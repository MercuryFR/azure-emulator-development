namespace Azure.HabboHotel.Users.Subscriptions
{
    class Subscription
    {
        internal Subscription(int id, int activated, int timeExpire, int timeLastGift)
        {
            SubscriptionId = id;
            ActivateTime = activated;
            ExpireTime = timeExpire;
            LastGiftTime = timeLastGift;
        }

        internal int SubscriptionId { get; private set; }

        internal int ExpireTime { get; private set; }

        internal int ActivateTime { get; private set; }

        internal int LastGiftTime { get; private set; }

        internal bool IsValid
        {
            get { return ExpireTime > Azure.GetUnixTimestamp(); }
        }
    }
}