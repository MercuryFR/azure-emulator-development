using System.Collections.Generic;
using Azure.HabboHotel.Achievements;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.RoomBots;
using Azure.HabboHotel.Rooms;
using Azure.HabboHotel.Users.Badges;
using Azure.HabboHotel.Users.Inventory;
using Azure.HabboHotel.Users.Messenger;
using Azure.HabboHotel.Users.Relationships;
using Azure.HabboHotel.Users.Subscriptions;

namespace Azure.HabboHotel.Users.UserDataManagement
{
    class UserData
    {
        internal uint UserId;
        internal Dictionary<string, UserAchievement> Achievements;
        internal Dictionary<int, UserTalent> Talents;
        internal List<uint> FavouritedRooms;
        internal List<uint> Ignores;
        internal List<string> Tags;
        internal Subscription Subscriptions;
        internal List<Badge> Badges;
        internal List<UserItem> Inventory;
        internal List<AvatarEffect> Effects;
        internal Dictionary<uint, MessengerBuddy> Friends;
        internal Dictionary<uint, MessengerRequest> Requests;
        internal HashSet<RoomData> Rooms;
        internal Dictionary<uint, Pet> Pets;
        internal Dictionary<uint, int> Quests;
        internal Habbo User;
        internal Dictionary<uint, RoomBot> Bots;
        internal Dictionary<int, Relationship> Relations;
        internal HashSet<uint> SuggestedPolls;
        internal int MiniMailCount;

        public UserData(uint userId, Dictionary<string, UserAchievement> achievements,
            Dictionary<int, UserTalent> talents, List<uint> favouritedRooms, List<uint> ignores, List<string> tags,
            Subscription sub, List<Badge> badges, List<UserItem> inventory, List<AvatarEffect> effects,
            Dictionary<uint, MessengerBuddy> friends, Dictionary<uint, MessengerRequest> requests,
            HashSet<RoomData> rooms, Dictionary<uint, Pet> pets, Dictionary<uint, int> quests, Habbo user,
            Dictionary<uint, RoomBot> bots, Dictionary<int, Relationship> relations, HashSet<uint> suggestedPolls,
            int miniMailCount)
        {
            UserId = userId;
            Achievements = achievements;
            Talents = talents;
            FavouritedRooms = favouritedRooms;
            Ignores = ignores;
            Tags = tags;
            Subscriptions = sub;
            Badges = badges;
            Inventory = inventory;
            Effects = effects;
            Friends = friends;
            Requests = requests;
            Rooms = rooms;
            Pets = pets;
            Quests = quests;
            User = user;
            Bots = bots;
            Relations = relations;
            SuggestedPolls = suggestedPolls;
            MiniMailCount = miniMailCount;
        }
    }
}