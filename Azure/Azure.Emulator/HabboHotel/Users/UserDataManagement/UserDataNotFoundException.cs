using System;

namespace Azure.HabboHotel.Users.UserDataManagement
{
    class UserDataNotFoundException : Exception
    {
        public UserDataNotFoundException(string reason) : base(reason) { }
    }
}