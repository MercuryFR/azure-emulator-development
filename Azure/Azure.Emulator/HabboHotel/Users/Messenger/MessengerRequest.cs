using Azure.Messages;

namespace Azure.HabboHotel.Users.Messenger
{
    class MessengerRequest
    {
        private readonly string _userName;

        internal MessengerRequest(uint toUser, uint fromUser, string userName)
        {
            To = toUser;
            From = fromUser;
            _userName = userName;
        }

        internal uint To { get; private set; }

        internal uint From { get; private set; }

        internal void Serialize(ServerMessage request)
        {
            request.AppendInteger(From);
            request.AppendString(_userName);
            var habboForName = Azure.GetHabboForName(_userName);
            request.AppendString((habboForName != null) ? habboForName.Look : "");
        }
    }
}