using System;
using System.Linq;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Rooms;
using Azure.Messages;

namespace Azure.HabboHotel.Users.Messenger
{
    class MessengerBuddy
    {
        internal string UserName;
        internal GameClient Client;
        private string _look;
        private string _motto;
        //private readonly int _lastOnline;
        private readonly bool _appearOffline;
        private readonly bool _hideInroom;

        internal MessengerBuddy(uint userId, string userName, string look, string motto, int lastOnline,
            bool appearOffline, bool hideInroom)
        {
            Id = userId;
            UserName = userName;
            _look = look;
            _motto = motto;
            //_lastOnline = lastOnline;
            _appearOffline = appearOffline;
            _hideInroom = hideInroom;
        }

        internal uint Id { get; private set; }

        internal bool IsOnline
        {
            get
            {
                return Client != null && Client.GetHabbo() != null && Client.GetHabbo().GetMessenger() != null &&
                       !Client.GetHabbo().GetMessenger().AppearOffline;
            }
        }

        internal bool InRoom
        {
            get { return CurrentRoom != null; }
        }

        internal Room CurrentRoom { get; set; }

        internal void UpdateUser()
        {
            Client = Azure.GetGame().GetClientManager().GetClientByUserId(Id);
            if (Client == null || Client.GetHabbo() == null)
                return;
            CurrentRoom = Client.GetHabbo().CurrentRoom;
            _look = Client.GetHabbo().Look;
            _motto = Client.GetHabbo().Motto;
        }

        internal void Serialize(ServerMessage message, GameClient session)
        {
            var value = session.GetHabbo().Relationships.FirstOrDefault(x => x.Value.UserId == Convert.ToInt32(Id)).Value;
            var i = (value == null) ? 0 : value.Type;
            message.AppendInteger(Id);
            message.AppendString(UserName);
            message.AppendInteger(1);
            if (Id == 0)
            {
                message.AppendBool(true);
                message.AppendBool(false);
            }
            else
            {
                message.AppendBool((!_appearOffline || session.GetHabbo().Rank >= 4) && IsOnline);
                message.AppendBool((!_hideInroom || session.GetHabbo().Rank >= 4) && InRoom);
            }
            message.AppendString(IsOnline ? _look : "");
            message.AppendInteger(0);
            message.AppendString(_motto);
            message.AppendString(string.Empty);
            message.AppendString(string.Empty);
            message.AppendBool(true); // persistedMessageUser
            message.AppendBool(false);
            message.AppendBool(false);
            message.AppendShort(i);
        }
    }
}