using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Quests;
using Azure.HabboHotel.Rooms;
using Azure.Messages;
using Azure.Messages.Parsers;
using Azure.Util;

namespace Azure.HabboHotel.Users.Messenger
{
    class HabboMessenger
    {
        internal Dictionary<uint, MessengerRequest> Requests;
        internal Dictionary<uint, MessengerBuddy> Friends;
        internal bool AppearOffline;
        private readonly uint _userId;

        internal HabboMessenger(uint userId)
        {
            Requests = new Dictionary<uint, MessengerRequest>();
            Friends = new Dictionary<uint, MessengerBuddy>();
            _userId = userId;
        }

        internal void Init(Dictionary<uint, MessengerBuddy> friends, Dictionary<uint, MessengerRequest> requests)
        {
            Requests = new Dictionary<uint, MessengerRequest>(requests);
            Friends = new Dictionary<uint, MessengerBuddy>(friends);
        }

        internal void ClearRequests() { Requests.Clear(); }

        internal MessengerRequest GetRequest(uint senderId)
        {
            return Requests.ContainsKey(senderId) ? Requests[senderId] : null;
        }

        internal void Destroy()
        {
            var clientsById = Azure.GetGame().GetClientManager().GetClientsById(Friends.Keys);
            foreach (var current in clientsById.Where(current => current.GetHabbo() != null && current.GetHabbo().GetMessenger() != null))
                current.GetHabbo().GetMessenger().UpdateFriend(_userId, null, true);
        }

        internal void OnStatusChanged(bool notification)
        {
            if (Friends == null)
                return;
            var clientsById = Azure.GetGame().GetClientManager().GetClientsById(Friends.Keys);
            foreach (var current in clientsById.Where(current => current != null && current.GetHabbo() != null && current.GetHabbo().GetMessenger() != null))
            {
                current.GetHabbo().GetMessenger().UpdateFriend(_userId, current, true);
                UpdateFriend(current.GetHabbo().Id, current, notification);
            }
        }

        internal void UpdateFriend(uint userid, GameClient client, bool notification)
        {
            if (!Friends.ContainsKey(userid))
                return;
            Friends[userid].UpdateUser();
            if (!notification)
                return;
            var client2 = GetClient();
            if (client2 != null)
                client2.SendMessage(SerializeUpdate(Friends[userid]));
        }

        internal void SerializeMessengerAction(int type, string name)
        {
            if (GetClient() == null)
                return;
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("ConsoleMessengerActionMessageComposer"));
            serverMessage.AppendString(GetClient().GetHabbo().Id.ToString());
            serverMessage.AppendInteger(type);
            serverMessage.AppendString(name);
            foreach (var current in Friends.Values.Where(current => current.Client != null))
                current.Client.SendMessage(serverMessage);
        }

        internal void HandleAllRequests()
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "DELETE FROM messenger_requests WHERE from_id = ",
                    _userId,
                    " OR to_id = ",
                    _userId
                }));
            ClearRequests();
        }

        internal void HandleRequest(uint sender)
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "DELETE FROM messenger_requests WHERE (from_id = ",
                    _userId,
                    " AND to_id = ",
                    sender,
                    ") OR (to_id = ",
                    _userId,
                    " AND from_id = ",
                    sender,
                    ")"
                }));
            Requests.Remove(sender);
        }

        internal void CreateFriendship(uint friendId)
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "REPLACE INTO messenger_friendships (user_one_id,user_two_id) VALUES (",
                    _userId,
                    ",",
                    friendId,
                    ")"
                }));
            OnNewFriendship(friendId);
            var clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(friendId);
            if (clientByUserId != null && clientByUserId.GetHabbo().GetMessenger() != null)
                clientByUserId.GetHabbo().GetMessenger().OnNewFriendship(_userId);
        }

        internal void DestroyFriendship(uint friendId)
        {
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "DELETE FROM messenger_friendships WHERE (user_one_id = ",
                    _userId,
                    " AND user_two_id = ",
                    friendId,
                    ") OR (user_two_id = ",
                    _userId,
                    " AND user_one_id = ",
                    friendId,
                    ")"
                }));
            OnDestroyFriendship(friendId);
            var clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(friendId);
            if (clientByUserId != null && clientByUserId.GetHabbo().GetMessenger() != null)
                clientByUserId.GetHabbo().GetMessenger().OnDestroyFriendship(_userId);
        }

        internal void OnNewFriendship(uint friendId)
        {
            var clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(friendId);
            MessengerBuddy messengerBuddy;
            if (clientByUserId == null || clientByUserId.GetHabbo() == null)
            {
                DataRow row;
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery(
                        string.Format(
                            "SELECT id,username,motto,look,last_online,hide_inroom,hide_online FROM users WHERE id = {0}",
                            friendId));
                    row = queryReactor.GetRow();
                }
                messengerBuddy = new MessengerBuddy(friendId, (string) row["Username"], (string) row["look"],
                    (string) row["motto"], (int) row["last_online"], Azure.EnumToBool(row["hide_online"].ToString()),
                    Azure.EnumToBool(row["hide_inroom"].ToString()));
            }
            else
            {
                var habbo = clientByUserId.GetHabbo();
                messengerBuddy = new MessengerBuddy(friendId, habbo.UserName, habbo.Look, habbo.Motto, 0,
                    habbo.AppearOffline, habbo.HideInRoom);
                messengerBuddy.UpdateUser();
            }
            if (!Friends.ContainsKey(friendId))
                Friends.Add(friendId, messengerBuddy);
            GetClient().SendMessage(SerializeUpdate(messengerBuddy));
        }

        internal bool RequestExists(uint requestId)
        {
            if (Requests.ContainsKey(requestId))
                return true;
            checked
            {
                bool result;
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery(
                        "SELECT user_one_id FROM messenger_friendships WHERE user_one_id = @myID AND user_two_id = @friendID");
                    queryReactor.AddParameter("myID", (int) _userId);
                    queryReactor.AddParameter("friendID", (int) requestId);
                    result = queryReactor.FindsResult();
                }
                return result;
            }
        }

        internal bool FriendshipExists(uint friendId) { return Friends.ContainsKey(friendId); }

        internal void OnDestroyFriendship(uint friend)
        {
            Friends.Remove(friend);
            GetClient()
                .GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("FriendUpdateMessageComposer"));
            GetClient().GetMessageHandler().GetResponse().AppendInteger(0);
            GetClient().GetMessageHandler().GetResponse().AppendInteger(1);
            GetClient().GetMessageHandler().GetResponse().AppendInteger(-1);
            GetClient().GetMessageHandler().GetResponse().AppendInteger(friend);
            GetClient().GetMessageHandler().SendResponse();
        }

        internal bool RequestBuddy(string userQuery)
        {
            var clientByUsername = Azure.GetGame().GetClientManager().GetClientByUserName(userQuery);
            uint num;
            bool flag;
            if (clientByUsername == null)
            {
                DataRow dataRow;
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery("SELECT id,block_newfriends FROM users WHERE Username = @query");
                    queryReactor.AddParameter("query", userQuery.ToLower());
                    dataRow = queryReactor.GetRow();
                }
                if (dataRow == null)
                    return false;
                num = Convert.ToUInt32(dataRow["id"]);
                flag = Azure.EnumToBool(dataRow["block_newfriends"].ToString());
            }
            else
            {
                num = clientByUsername.GetHabbo().Id;
                flag = clientByUsername.GetHabbo().HasFriendRequestsDisabled;
            }
            if (flag && GetClient().GetHabbo().Rank < 4u)
            {
                GetClient()
                    .GetMessageHandler()
                    .GetResponse()
                    .Init(LibraryParser.OutgoingRequest("NotAcceptingRequestsMessageComposer"));
                GetClient().GetMessageHandler().GetResponse().AppendInteger(39);
                GetClient().GetMessageHandler().GetResponse().AppendInteger(3);
                GetClient().GetMessageHandler().SendResponse();
                return false;
            }
            var num2 = num;
            if (RequestExists(num2))
                return true;
            using (var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
                queryreactor2.RunFastQuery(string.Concat(new object[]
                {
                    "REPLACE INTO messenger_requests (from_id,to_id) VALUES (",
                    _userId,
                    ",",
                    num2,
                    ")"
                }));
            Azure.GetGame().GetQuestManager().ProgressUserQuest(GetClient(), QuestType.AddFriends, 0u);
            var clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(num2);
            if (clientByUserId == null || clientByUserId.GetHabbo() == null)
                return true;
            var messengerRequest = new MessengerRequest(num2, _userId,
                Azure.GetGame().GetClientManager().GetNameById(_userId));
            clientByUserId.GetHabbo().GetMessenger().OnNewRequest(_userId);
            var serverMessage =
                new ServerMessage(LibraryParser.OutgoingRequest("ConsoleSendFriendRequestMessageComposer"));
            messengerRequest.Serialize(serverMessage);
            clientByUserId.SendMessage(serverMessage);
            Requests.Add(num2, messengerRequest);
            return true;
        }

        internal void OnNewRequest(uint friendId)
        {
            if (!Requests.ContainsKey(friendId))
                Requests.Add(friendId,
                    new MessengerRequest(_userId, friendId, Azure.GetGame().GetClientManager().GetNameById(friendId)));
        }

        internal void SendInstantMessage(uint toId, string message)
        {
            checked
            {
                if (BlockAds.CheckPublicistas(message))
                {
                    GetClient().PublicistaCount += 1;
                    GetClient().HandlePublicista(message);
                    return;
                }
                if (!FriendshipExists(toId))
                {
                    DeliverInstantMessageError(6, toId);
                    return;
                }
                if (toId == 0) // Staff Chat
                {
                    var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ConsoleChatMessageComposer"));
                    serverMessage.AppendInteger(0); //userid
                    serverMessage.AppendString(GetClient().GetHabbo().UserName + " : " + message);
                    serverMessage.AppendInteger(0);
                    Azure.GetGame().GetClientManager().StaffAlert(serverMessage, GetClient().GetHabbo().Id);
                }
                else
                {
                    var clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(toId);
                    if (clientByUserId == null || clientByUserId.GetHabbo().GetMessenger() == null)
                    {
                        if (Azure.OfflineMessages.ContainsKey(toId))
                            Azure.OfflineMessages[toId].Add(new OfflineMessage(GetClient().GetHabbo().Id, message,
                                Azure.GetUnixTimestamp()));
                        else
                        {
                            Azure.OfflineMessages.Add(toId, new List<OfflineMessage>());
                            Azure.OfflineMessages[toId].Add(new OfflineMessage(GetClient().GetHabbo().Id, message,
                                Azure.GetUnixTimestamp()));
                        }
                        OfflineMessage.SaveMessage(Azure.GetDatabaseManager().GetQueryReactor(), toId,
                            GetClient().GetHabbo().Id, message);
                        return;
                    }
                    if (GetClient().GetHabbo().Muted)
                    {
                        DeliverInstantMessageError(4, toId);
                        return;
                    }
                    if (clientByUserId.GetHabbo().Muted)
                        DeliverInstantMessageError(3, toId);
                    if (message == "")
                        return;
                    clientByUserId.GetHabbo().GetMessenger().DeliverInstantMessage(message, _userId);
                }
                // CAUSES LAG: this.LogPM(this.UserId, ToId, Message);
            }
        }

        /*
        internal void LogPM(uint From_Id, uint ToId, string Message)
        {
            var arg_10_0 = GetClient().GetHabbo().Id;
            var arg_16_0 = DateTime.Now;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Concat(new object[]
                {
                    "INSERT INTO chatlogs_console VALUES (NULL, ",
                    From_Id,
                    ", ",
                    ToId,
                    ", @Message, UNIX_TIMESTAMP())"
                }));
                queryReactor.AddParameter("message", Message);
                queryReactor.RunQuery();
            }
        }
         * */

        internal void DeliverInstantMessage(string message, uint convoId)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ConsoleChatMessageComposer"));
            serverMessage.AppendInteger(convoId);
            serverMessage.AppendString(message);
            serverMessage.AppendInteger(0);
            GetClient().SendMessage(serverMessage);
        }

        internal void DeliverInstantMessageError(int errorId, uint conversationId)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ConsoleChatErrorMessageComposer"));
            serverMessage.AppendInteger(errorId);
            serverMessage.AppendInteger(conversationId);
            serverMessage.AppendString("");
            GetClient().SendMessage(serverMessage);
        }

        internal ServerMessage SerializeCategories()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("LoadFriendsCategories"));
            serverMessage.AppendInteger(2000);
            serverMessage.AppendInteger(300);
            serverMessage.AppendInteger(800);
            serverMessage.AppendInteger(1100);
            serverMessage.AppendInteger(0); // categories
            // int id 
            // str name
            return serverMessage;
        }

        internal ServerMessage SerializeFriends()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("LoadFriendsMessageComposer"));
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(0);
            serverMessage.AppendInteger(Friends.Count);
            foreach (var current in Friends.Values)
            {
                current.UpdateUser();
                current.Serialize(serverMessage, GetClient());
            }
            return serverMessage;
        }

        internal ServerMessage SerializeOfflineMessages(OfflineMessage message)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ConsoleChatMessageComposer"));
            serverMessage.AppendInteger(message.FromId);
            serverMessage.AppendString(message.Message);
            serverMessage.AppendInteger(checked((int) unchecked(Azure.GetUnixTimestamp() - message.Timestamp)));
            return serverMessage;
        }

        internal ServerMessage SerializeUpdate(MessengerBuddy friend)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("FriendUpdateMessageComposer"));
            serverMessage.AppendInteger(0);
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(0);
            friend.Serialize(serverMessage, GetClient());
            serverMessage.AppendBool(false);
            return serverMessage;
        }

        internal ServerMessage SerializeRequests()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("FriendRequestsMessageComposer"));
            serverMessage.AppendInteger(((long) Requests.Count > (long) ((ulong) Azure.FriendRequestLimit))
                ? checked((int) Azure.FriendRequestLimit)
                : Requests.Count);
            serverMessage.AppendInteger(((long) Requests.Count > (long) ((ulong) Azure.FriendRequestLimit))
                ? checked((int) Azure.FriendRequestLimit)
                : Requests.Count);
            var num = 0;
            foreach (var current in Requests.Values)
            {
                checked
                {
                    num++;
                }
                if (num > Azure.FriendRequestLimit)
                    break;
                current.Serialize(serverMessage);
            }
            return serverMessage;
        }

        internal ServerMessage PerformSearch(string query)
        {
            var searchResult = SearchResultFactory.GetSearchResult(query);
            var list = new List<SearchResult>();
            var list2 = new List<SearchResult>();
            foreach (var current in searchResult.Where(current => current.UserId != GetClient().GetHabbo().Id))
                if (FriendshipExists(current.UserId))
                    list.Add(current);
                else
                    list2.Add(current);
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ConsoleSearchFriendMessageComposer"));
            serverMessage.AppendInteger(list.Count);
            foreach (var current2 in list)
                current2.Searialize(serverMessage);
            serverMessage.AppendInteger(list2.Count);
            foreach (var current3 in list2)
                current3.Searialize(serverMessage);
            return serverMessage;
        }

        internal HashSet<RoomData> GetActiveFriendsRooms()
        {
            var toReturn = new HashSet<RoomData>();
            foreach (var current in
                from p in Friends.Values
                where p.InRoom
                select p)
                toReturn.Add(current.CurrentRoom.RoomData);
            return toReturn;
        }

        private GameClient GetClient() { return Azure.GetGame().GetClientManager().GetClientByUserId(_userId); }
    }
}