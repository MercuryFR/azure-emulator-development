using System.Collections.Generic;
using System.Data;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.HabboHotel.Users.Messenger
{
    class OfflineMessage
    {
        internal uint FromId;
        internal string Message;
        internal double Timestamp;

        internal OfflineMessage(uint id, string msg, double ts)
        {
            FromId = id;
            Message = msg;
            Timestamp = ts;
        }

        internal static void InitOfflineMessages(IQueryAdapter dbClient)
        {
            dbClient.SetQuery("SELECT * FROM messenger_offline_messages");
            var table = dbClient.GetTable();
            foreach (DataRow dataRow in table.Rows)
            {
                var key = (uint) dataRow[1];
                var id = (uint) dataRow[2];
                var msg = dataRow[3].ToString();
                var ts = (double) dataRow[4];
                if (!Azure.OfflineMessages.ContainsKey(key))
                    Azure.OfflineMessages.Add(key, new List<OfflineMessage>());
                Azure.OfflineMessages[key].Add(new OfflineMessage(id, msg, ts));
            }
        }

        internal static void SaveMessage(IQueryAdapter dbClient, uint toId, uint fromId, string message)
        {
            dbClient.SetQuery(
                "INSERT INTO messenger_offline_messages (to_id, from_id, Message, timestamp) VALUES (@tid, @fid, @msg, UNIX_TIMESTAMP())");
            dbClient.AddParameter("tid", toId);
            dbClient.AddParameter("fid", fromId);
            dbClient.AddParameter("msg", message);
            dbClient.RunQuery();
        }

        internal static void RemoveAllMessages(IQueryAdapter dbClient, uint ToId)
        {
            dbClient.RunFastQuery(string.Format("DELETE FROM messenger_offline_messages WHERE to_id={0}", ToId));
        }
    }
}