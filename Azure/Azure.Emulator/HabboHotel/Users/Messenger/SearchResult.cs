using Azure.Messages;

namespace Azure.HabboHotel.Users.Messenger
{
    struct SearchResult
    {
        internal uint UserId;
        internal string UserName;
        internal string Motto;
        internal string Look;
        internal string LastOnline;

        public SearchResult(uint userId, string userName, string motto, string look, string lastOnline)
        {
            UserId = userId;
            UserName = userName;
            Motto = motto;
            Look = look;
            LastOnline = lastOnline;
        }

        internal void Searialize(ServerMessage reply)
        {
            reply.AppendInteger(UserId);
            reply.AppendString(UserName);
            reply.AppendString(Motto);
            reply.AppendBool(Azure.GetGame().GetClientManager().GetClient(UserId) != null);
            reply.AppendBool(false);
            reply.AppendString(string.Empty);
            reply.AppendInteger(0);
            reply.AppendString(Look);
            reply.AppendString(LastOnline);
        }
    }
}