namespace Azure.HabboHotel.Users.Badges
{
    class Badge
    {
        internal string Code;
        internal int Slot;

        internal Badge(string code, int slot)
        {
            Code = code;
            Slot = slot;
        }
    }
}