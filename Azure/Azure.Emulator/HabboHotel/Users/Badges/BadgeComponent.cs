using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Users.UserDataManagement;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Users.Badges
{
    class BadgeComponent
    {
        private readonly uint _userId;

        internal BadgeComponent(uint userId, UserData data)
        {
            BadgeList = new HybridDictionary();
            foreach (var current in data.Badges.Where(current => !BadgeList.Contains(current.Code)))
                BadgeList.Add(current.Code, current);
            _userId = userId;
        }

        internal int Count
        {
            get { return BadgeList.Count; }
        }

        internal int EquippedCount
        {
            get
            {
                var num = 0;
                checked
                {
                    num += BadgeList.Values.Cast<Badge>().Count(badge => badge.Slot > 0);
                    return num;
                }
            }
        }

        internal HybridDictionary BadgeList { get; private set; }

        internal Badge GetBadge(string badge) { return BadgeList.Contains(badge) ? (Badge) BadgeList[badge] : null; }

        internal bool HasBadge(string badge) { return BadgeList.Contains(badge); }

        internal void GiveBadge(string badge, bool inDatabase, GameClient session, bool wiredReward = false)
        {
            if (wiredReward)
                session.SendMessage(SerializeBadgeReward(!HasBadge(badge)));
            if (HasBadge(badge))
                return;
            if (inDatabase)
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery(string.Concat(new object[]
                    {
                        "INSERT INTO users_badges (user_id,badge_id,badge_slot) VALUES (",
                        _userId,
                        ",@badge,",
                        0,
                        ")"
                    }));
                    queryReactor.AddParameter("badge", badge);
                    queryReactor.RunQuery();
                }
            BadgeList.Add(badge, new Badge(badge, 0));
            session.SendMessage(SerializeBadge(badge));
            session.SendMessage(Update(badge));
        }

        internal ServerMessage SerializeBadge(string badge)
        {
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("ReceiveBadgeMessageComposer"));
            serverMessage.AppendInteger(1);
            serverMessage.AppendString(badge);
            return serverMessage;
        }

        internal ServerMessage SerializeBadgeReward(bool success)
        {
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("WiredRewardAlertMessageComposer"));
            serverMessage.AppendInteger(success ? 7 : 1);
            return serverMessage;
        }

        internal void ResetSlots()
        {
            foreach (Badge badge in BadgeList.Values)
                badge.Slot = 0;
        }

        internal void RemoveBadge(string badge, GameClient session)
        {
            if (!HasBadge(badge))
                return;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    string.Format("DELETE FROM users_badges WHERE badge_id = @badge AND user_id = {0} LIMIT 1", _userId));
                queryReactor.AddParameter("badge", badge);
                queryReactor.RunQuery();
            }
            BadgeList.Remove(GetBadge(badge));
            session.SendMessage(Serialize());
        }

        internal ServerMessage Update(string badgeId)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("NewInventoryObjectMessageComposer"));
            serverMessage.AppendInteger(1);
            serverMessage.AppendInteger(4);
            serverMessage.AppendInteger(1);
            serverMessage.AppendString(badgeId);
            return serverMessage;
        }

        internal ServerMessage Serialize()
        {
            var list = new List<Badge>();
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("LoadBadgesWidgetMessageComposer"));
            serverMessage.AppendInteger(Count);
            foreach (Badge badge in BadgeList.Values)
            {
                serverMessage.AppendInteger(1);
                serverMessage.AppendString(badge.Code);
                if (badge.Slot > 0)
                    list.Add(badge);
            }
            serverMessage.AppendInteger(list.Count);
            foreach (var current in list)
            {
                serverMessage.AppendInteger(current.Slot);
                serverMessage.AppendString(current.Code);
            }
            return serverMessage;
        }
    }
}