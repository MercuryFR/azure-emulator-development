﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Azure.Configuration;
using Azure.Connection.ServerManager;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.Achievements;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Groups;
using Azure.HabboHotel.Misc;
using Azure.HabboHotel.Navigators;
using Azure.HabboHotel.Rooms;
using Azure.HabboHotel.Users.Badges;
using Azure.HabboHotel.Users.Inventory;
using Azure.HabboHotel.Users.Messenger;
using Azure.HabboHotel.Users.Relationships;
using Azure.HabboHotel.Users.Subscriptions;
using Azure.HabboHotel.Users.UserDataManagement;
using Azure.Messages;
using Azure.Messages.Parsers;
using System.Timers;

namespace Azure.HabboHotel.Users
{
    public class Habbo
    {
        public GameClient GuideOtherUser;

        internal uint Id;
        internal string UserName, RealName, Motto, Look, Gender;
        internal double CreateDate;
        internal uint Rank;
        internal int LastChange;
        internal int Credits, AchievementPoints, ActivityPoints, BelCredits;
        internal bool Muted;
        internal int Respect, DailyRespectPoints, DailyPetRespectPoints;
        internal uint LoadingRoom;
        internal bool LoadingChecksPassed;
        internal uint CurrentRoomId;
        internal uint HomeRoom;
        internal int LastOnline;
        internal int PreviousOnline;
        internal bool IsTeleporting;
        internal bool IsHopping;
        internal uint TeleportingRoomId;
        internal uint TeleporterId;
        internal uint HopperId;
        internal List<uint> FavoriteRooms;
        internal List<uint> MutedUsers;
        internal List<string> Tags;
        internal Dictionary<string, UserAchievement> Achievements;
        internal Dictionary<int, UserTalent> Talents;
        internal HashSet<uint> RatedRooms;
        internal HashSet<uint> RecentlyVisitedRooms;
        internal bool SpectatorMode;
        internal bool Disconnected;
        internal bool HasFriendRequestsDisabled;
        internal HashSet<RoomData> UsersRooms;
        internal HashSet<GroupUser> UserGroups;
        internal uint FavouriteGroup;
        internal bool SpamProtectionBol;
        internal int SpamProtectionCount = 1, SpamProtectionTime, SpamProtectionAbuse;
        internal uint FriendCount;
        internal DateTime SpamFloodTime;
        internal Dictionary<uint, int> Quests;
        internal uint CurrentQuestId;
        internal uint LastQuestCompleted;
        internal int FloodTime;
        internal bool HideInRoom;
        internal bool AppearOffline;
        internal bool VIP;
        internal DateTime LastGiftPurchaseTime;
        internal DateTime LastGiftOpenTime;
        internal int TradeLockExpire;
        internal bool TradeLocked;
        internal string TalentStatus;
        internal int CurrentTalentLevel;
        internal Dictionary<int, Relationship> Relationships;
        internal HashSet<uint> AnsweredPolls;
        internal bool NuxPassed;
        internal int MinimailUnreadMessages;
        internal int LastSqlQuery;
        internal int BuildersExpire;
        internal int BuildersItemsMax;
        internal int BuildersItemsUsed;
        internal bool OnDuty;
        internal string ReleaseName;
        internal Dictionary<Int32, NaviLogs> NavigatorLogs;

        private readonly List<UInt32> _myGroups;
        private SubscriptionManager _subscriptionManager;
        private HabboMessenger _messenger;
        private BadgeComponent _badgeComponent;
        private InventoryComponent _inventoryComponent;
        private AvatarEffectsInventoryComponent _avatarEffectsInventoryComponent;
        private ChatCommandHandler _commandHandler;
        private GameClient _mClient;
        private bool _habboinfoSaved;
        private Boolean _loadedMyGroups;

        internal Boolean OwnRoomsSerialized = false;

        public bool timer_Elapsed = false;

        public void timer_ElapsedEvent(object source, ElapsedEventArgs e)
        {
            timer_Elapsed = true;
        }

        internal Habbo(uint id, string userName, string realName, uint rank, string motto, string look, string gender,
            int credits, int activityPoints, double lastActivityPointsUpdate, bool muted, uint homeRoom, int respect,
            int dailyRespectPoints, int dailyPetRespectPoints, bool hasFriendRequestsDisabled, uint currentQuestId,
            int currentQuestProgress, int achievementPoints, int regTimestamp, int lastOnline, bool appearOffline,
            bool hideInRoom, bool vip, double createDate, bool online, string citizenShip, int belCredits,
            HashSet<GroupUser> groups, uint favId, int lastChange, bool tradeLocked, int tradeLockExpire, bool nuxPassed,
            int buildersExpire, int buildersItemsMax, int buildersItemsUsed, int releaseVersion, bool onDuty,
            Dictionary<Int32, NaviLogs> naviLogs)
        {
            Id = id;
            UserName = userName;
            RealName = realName;
            _myGroups = new List<uint>();
            BuildersExpire = buildersExpire;
            BuildersItemsMax = buildersItemsMax;
            BuildersItemsUsed = buildersItemsUsed;
            if (rank < 1u)
                rank = 1u;
            ReleaseName = "";
            OnDuty = onDuty;
            Rank = rank;
            Motto = motto;
            Look = look.ToLower();
            if (rank > 5)
                VIP = true;
            else
                VIP = vip;
            LastChange = lastChange;
            TradeLocked = tradeLocked;
            NavigatorLogs = naviLogs;
            TradeLockExpire = tradeLockExpire;
            Gender = gender.ToLower();
            Credits = credits;
            ActivityPoints = activityPoints;
            BelCredits = belCredits;
            AchievementPoints = achievementPoints;
            Muted = muted;
            LoadingRoom = 0u;
            CreateDate = createDate;
            LoadingChecksPassed = false;
            FloodTime = 0;
            NuxPassed = nuxPassed;
            CurrentRoomId = 0u;
            HomeRoom = homeRoom;
            HideInRoom = hideInRoom;
            AppearOffline = appearOffline;
            FavoriteRooms = new List<uint>();
            MutedUsers = new List<uint>();
            Tags = new List<string>();
            Achievements = new Dictionary<string, UserAchievement>();
            Talents = new Dictionary<int, UserTalent>();
            Relationships = new Dictionary<int, Relationship>();
            RatedRooms = new HashSet<uint>();
            Respect = respect;
            DailyRespectPoints = dailyRespectPoints;
            DailyPetRespectPoints = dailyPetRespectPoints;
            IsTeleporting = false;
            TeleporterId = 0u;
            UsersRooms = new HashSet<RoomData>();
            HasFriendRequestsDisabled = hasFriendRequestsDisabled;
            LastOnline = Azure.GetUnixTimestamp();
            PreviousOnline = lastOnline;
            RecentlyVisitedRooms = new HashSet<uint>();
            CurrentQuestId = currentQuestId;
            IsHopping = false;

            FavouriteGroup = Azure.GetGame().GetGroupManager().GetGroup(favId) != null ? favId : 0u;
            UserGroups = groups;
            if (DailyPetRespectPoints > 99)
                DailyPetRespectPoints = 99;
            if (DailyRespectPoints > 99)
                DailyRespectPoints = 99;
            LastGiftPurchaseTime = DateTime.Now;
            LastGiftOpenTime = DateTime.Now;
            TalentStatus = citizenShip;
            CurrentTalentLevel = GetCurrentTalentLevel();
        }

        public bool CanChangeName
        {
            get
            {
                return (ExtraSettings.CHANGE_NAME_STAFF && HasFuse("fuse_can_change_name")) ||
                       (ExtraSettings.CHANGE_NAME_VIP && VIP) ||
                       (ExtraSettings.CHANGE_NAME_EVERYONE &&
                        Azure.GetUnixTimestamp() > (LastChange + 604800));
            }
        }

        internal string HeadPart
        {
            get
            {
                var strtmp = Look.Split('.');
                var tmp2 = strtmp.FirstOrDefault(x => x.Contains("hd-"));
                var lookToReturn = tmp2 ?? "";

                if (Look.Contains("ha-"))
                    lookToReturn += string.Format(".{0}", strtmp.FirstOrDefault(x => x.Contains("ha-")));
                if (Look.Contains("ea-"))
                    lookToReturn += string.Format(".{0}", strtmp.FirstOrDefault(x => x.Contains("ea-")));
                if (Look.Contains("hr-"))
                    lookToReturn += string.Format(".{0}", strtmp.FirstOrDefault(x => x.Contains("hr-")));
                if (Look.Contains("he-"))
                    lookToReturn += string.Format(".{0}", strtmp.FirstOrDefault(x => x.Contains("he-")));
                if (Look.Contains("fa-"))
                    lookToReturn += string.Format(".{0}", strtmp.FirstOrDefault(x => x.Contains("fa-")));

                return lookToReturn;
            }
        }

        internal bool InRoom
        {
            get { return CurrentRoomId >= 1 && CurrentRoom != null; }
        }

        internal Room CurrentRoom
        {
            get
            {
                return CurrentRoomId <= 0u ? null : Azure.GetGame().GetRoomManager().GetRoom(CurrentRoomId);
            }
        }

        internal bool IsHelper
        {
            get { return TalentStatus == "helper" || Rank >= 4; }
        }

        internal bool IsCitizen
        {
            get { return CurrentTalentLevel > 4; }
        }

        internal string GetQueryString
        {
            get
            {
                _habboinfoSaved = true;
                return string.Concat(new object[]
                {
                    "UPDATE users SET online='0', last_online = '",
                    Azure.GetUnixTimestamp(),
                    "', activity_points = '",
                    ActivityPoints,
                    "', seasonal_currency = '",
                    BelCredits,
                    "', credits = '",
                    Credits,
                    "' WHERE id = '",
                    Id,
                    "'; UPDATE users_stats SET achievement_score = ",
                    AchievementPoints,
                    " WHERE id=",
                    Id,
                    " LIMIT 1; "
                });
            }
        }

        internal List<uint> MyGroups
        {
            get
            {
                if (!_loadedMyGroups)
                    _LoadMyGroups();

                return _myGroups;
            }
        }

        internal void InitInformation(UserData data)
        {
            _subscriptionManager = new SubscriptionManager(Id, data);
            _badgeComponent = new BadgeComponent(Id, data);
            Quests = data.Quests;
            _messenger = new HabboMessenger(Id);
            _messenger.Init(data.Friends, data.Requests);
            SpectatorMode = false;
            Disconnected = false;
            UsersRooms = data.Rooms;
            Relationships = data.Relations;
            AnsweredPolls = data.SuggestedPolls;
        }

        internal void Init(GameClient client, UserData data)
        {
            _mClient = client;
            _subscriptionManager = new SubscriptionManager(Id, data);
            _badgeComponent = new BadgeComponent(Id, data);
            _inventoryComponent = InventoryGlobal.GetInventory(Id, client, data);
            _inventoryComponent.SetActiveState(client);
            _commandHandler = new ChatCommandHandler(client);
            _avatarEffectsInventoryComponent = new AvatarEffectsInventoryComponent(Id, client, data);
            Quests = data.Quests;
            _messenger = new HabboMessenger(Id);
            _messenger.Init(data.Friends, data.Requests);
            FriendCount = Convert.ToUInt32(data.Friends.Count);
            SpectatorMode = false;
            Disconnected = false;
            UsersRooms = data.Rooms;
            MinimailUnreadMessages = data.MiniMailCount;
            Relationships = data.Relations;
            AnsweredPolls = data.SuggestedPolls;
        }

        internal ChatCommandHandler GetCommandHandler() { return _commandHandler; }

        internal void UpdateRooms()
        {
            using (var dbClient = Azure.GetDatabaseManager().GetQueryReactor())
            {
                UsersRooms.Clear();
                dbClient.SetQuery("SELECT * FROM rooms_data WHERE owner = @name ORDER BY id ASC LIMIT 50");
                dbClient.AddParameter("name", UserName);
                var table = dbClient.GetTable();
                foreach (DataRow dataRow in table.Rows)
                    UsersRooms.Add(
                        Azure.GetGame()
                            .GetRoomManager()
                            .FetchRoomData(Convert.ToUInt32(dataRow["id"]), dataRow));
            }
        }

        internal void LoadData(UserData data)
        {
            LoadAchievements(data.Achievements);
            LoadTalents(data.Talents);
            LoadFavorites(data.FavouritedRooms);
            LoadMutedUsers(data.Ignores);
            LoadTags(data.Tags);
        }

        internal void SerializeQuests(ref QueuedServerMessage response)
        {
            Azure.GetGame().GetQuestManager().GetList(_mClient, null);
        }

        internal bool GotCommand(string cmd)
        {
            return Azure.GetGame().GetRoleManager().RankGotCommand(Rank, cmd);
        }

        internal bool HasFuse(string fuse)
        {
            return Azure.GetGame().GetRoleManager().RankHasRight(Rank, fuse) ||
                   (GetSubscriptionManager().HasSubscription &&
                    Azure.GetGame()
                        .GetRoleManager()
                        .HasVip(GetSubscriptionManager().GetSubscription().SubscriptionId, fuse));
        }

        internal void LoadFavorites(List<uint> roomId)
        {
            FavoriteRooms = roomId;
        }

        internal void LoadMutedUsers(List<uint> usersMuted) { MutedUsers = usersMuted; }

        internal void LoadTags(List<string> tags) { Tags = tags; }

        internal void SerializeClub()
        {
            var client = GetClient();
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("SubscriptionStatusMessageComposer"));
            serverMessage.AppendString("club_habbo");
            if (client.GetHabbo().GetSubscriptionManager().HasSubscription)
            {
                double num = client.GetHabbo().GetSubscriptionManager().GetSubscription().ExpireTime;
                var num2 = num - Azure.GetUnixTimestamp();
                checked
                {
                    var num3 = (int) Math.Ceiling(num2 / 86400.0);
                    var i =
                        (int)
                            Math.Ceiling(
                                unchecked(
                                    Azure.GetUnixTimestamp() -
                                    (double) client.GetHabbo().GetSubscriptionManager().GetSubscription().ActivateTime) /
                                86400.0);
                    var num4 = num3 / 31;
                    if (num4 >= 1)
                        num4--;
                    serverMessage.AppendInteger(num3 - num4 * 31);
                    serverMessage.AppendInteger(1);
                    serverMessage.AppendInteger(num4);
                    serverMessage.AppendInteger(1);
                    serverMessage.AppendBool(true);
                    serverMessage.AppendBool(true);
                    serverMessage.AppendInteger(i);
                    serverMessage.AppendInteger(i);
                    serverMessage.AppendInteger(10);
                }
            }
            else
            {
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(0);
                serverMessage.AppendBool(false);
                serverMessage.AppendBool(false);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(0);
            }
            client.SendMessage(serverMessage);
            var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("UserClubRightsMessageComposer"));
            serverMessage2.AppendInteger(GetSubscriptionManager().HasSubscription ? 2 : 0);
            serverMessage2.AppendInteger(Rank);
            serverMessage2.AppendBool(Rank >= Convert.ToUInt32(Azure.GetDbConfig().DbData["ambassador.minrank"]));
            client.SendMessage(serverMessage2);
        }

        internal void LoadAchievements(Dictionary<string, UserAchievement> achievements)
        {
            Achievements = achievements;
        }

        internal void LoadTalents(Dictionary<int, UserTalent> talents) { Talents = talents; }

        internal void OnDisconnect(string reason)
        {
            if (Disconnected)
                return;
            Disconnected = true;
            if (_inventoryComponent != null)
            {
                _inventoryComponent.RunDbUpdate();
                _inventoryComponent.SetIdleState();
            }
            var navilogs = "";
            if (NavigatorLogs.Any())
            {
                navilogs = NavigatorLogs.Values.Aggregate(navilogs, (current, navi) => current + string.Format("{0},{1},{2};", navi.Id, navi.Value1, navi.Value2));
                navilogs = navilogs.Remove(navilogs.Length - 1);
            }
            Azure.GetGame().GetClientManager().UnregisterClient(Id, UserName);
            SessionManagement.IncreaseDisconnection();
            Logging.WriteLine(string.Format("[UserMgr] [ {0} ] got disconnected. Reason: {1}", UserName, reason), ConsoleColor.Red);
            if (!_habboinfoSaved)
            {
                _habboinfoSaved = true;
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.RunFastQuery(
                        string.Concat(new object[]
                        {
                            "UPDATE users SET activity_points = ", ActivityPoints, ", credits = ", Credits,
                            ", seasonal_currency = ", BelCredits, ",online='0' WHERE id = ", Id,
                            " LIMIT 1;UPDATE users_stats SET achievement_score=", AchievementPoints, " WHERE id=", Id,
                            " LIMIT 1;"
                        }));
                    if (Rank >= 4u)
                        queryReactor.RunFastQuery(
                            string.Format(
                                "UPDATE moderation_tickets SET status='open', moderator_id=0 WHERE status='picked' AND moderator_id={0}",
                                Id));
                }
            }
            if (InRoom && CurrentRoom != null)
                CurrentRoom.GetRoomUserManager().RemoveUserFromRoom(_mClient, false, false);
            if (_messenger != null)
            {
                _messenger.AppearOffline = true;
                _messenger.Destroy();
            }

            if (_avatarEffectsInventoryComponent != null)
                _avatarEffectsInventoryComponent.Dispose();
            _mClient = null;
        }

        internal void InitMessenger()
        {
            var client = GetClient();
            if (client == null)
                return;
            _messenger.OnStatusChanged(false);
            client.SendMessage(_messenger.SerializeCategories());
            client.SendMessage(_messenger.SerializeFriends());
            client.SendMessage(_messenger.SerializeRequests());
            if (Azure.OfflineMessages.ContainsKey(Id))
            {
                var list = Azure.OfflineMessages[Id];
                foreach (var current in list)
                    client.SendMessage(_messenger.SerializeOfflineMessages(current));
                Azure.OfflineMessages.Remove(Id);
                OfflineMessage.RemoveAllMessages(Azure.GetDatabaseManager().GetQueryReactor(), Id);
            }
            if (_messenger.Requests.Count > Azure.FriendRequestLimit)
                client.SendNotif("¡Tienes muchos amigos! No puedes tener más.");
        }

        internal void UpdateCreditsBalance()
        {
            if (_mClient == null || _mClient.GetMessageHandler() == null ||
                _mClient.GetMessageHandler().GetResponse() == null)
                return;
            _mClient.GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("CreditsBalanceMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendString(string.Format("{0}.0", Credits));
            _mClient.GetMessageHandler().SendResponse();
        }

        internal void UpdateActivityPointsBalance()
        {
            if (_mClient == null || _mClient.GetMessageHandler() == null ||
                _mClient.GetMessageHandler().GetResponse() == null)
                return;
            _mClient.GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("ActivityPointsMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendInteger(3);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(0);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(ActivityPoints);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(5);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(BelCredits);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(105);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(BelCredits);
            _mClient.GetMessageHandler().SendResponse();
        }

        internal void UpdateSeasonalCurrencyBalance()
        {
            if (_mClient == null || _mClient.GetMessageHandler() == null ||
                _mClient.GetMessageHandler().GetResponse() == null)
                return;
            _mClient.GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("ActivityPointsMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendInteger(3);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(0);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(ActivityPoints);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(5);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(BelCredits);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(105);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(BelCredits);
            _mClient.GetMessageHandler().SendResponse();
        }

        internal void NotifyNewPixels(int change)
        {
            if (_mClient == null || _mClient.GetMessageHandler() == null ||
                _mClient.GetMessageHandler().GetResponse() == null)
                return;
            _mClient.GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("ActivityPointsNotificationMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendInteger(ActivityPoints);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(change);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(0);
            _mClient.GetMessageHandler().SendResponse();
        }

        internal void NotifyNewDiamonds(int change)
        {
            if (_mClient == null || _mClient.GetMessageHandler() == null ||
                _mClient.GetMessageHandler().GetResponse() == null)
                return;

            _mClient.GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("ActivityPointsNotificationMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendInteger(BelCredits);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(change);
            _mClient.GetMessageHandler().GetResponse().AppendInteger(5);
            _mClient.GetMessageHandler().SendResponse();
        }

        internal void NotifyVoucher(bool isValid, string productName, string productDescription)
        {
            if (isValid)
            {
                _mClient.GetMessageHandler()
                    .GetResponse()
                    .Init(LibraryParser.OutgoingRequest("VoucherValidMessageComposer"));
                _mClient.GetMessageHandler().GetResponse().AppendString(productName);
                _mClient.GetMessageHandler().GetResponse().AppendString(productDescription);
                _mClient.GetMessageHandler().SendResponse();
                return;
            }
            _mClient.GetMessageHandler()
                .GetResponse()
                .Init(LibraryParser.OutgoingRequest("VoucherErrorMessageComposer"));
            _mClient.GetMessageHandler().GetResponse().AppendString("1");
            _mClient.GetMessageHandler().SendResponse();
        }

        internal void Mute()
        {
            if (!Muted)
                Muted = true;
        }

        internal void Unmute()
        {
            if (!Muted)
                return;
            GetClient().SendNotif("You were unmuted.");
            Muted = false;
        }

        internal SubscriptionManager GetSubscriptionManager() { return _subscriptionManager; }

        internal HabboMessenger GetMessenger() { return _messenger; }

        internal BadgeComponent GetBadgeComponent() { return _badgeComponent; }

        internal InventoryComponent GetInventoryComponent() { return _inventoryComponent; }

        internal AvatarEffectsInventoryComponent GetAvatarEffectsInventoryComponent()
        {
            return _avatarEffectsInventoryComponent;
        }

        internal void RunDbUpdate(IQueryAdapter dbClient)
        {
            dbClient.RunFastQuery(string.Concat(new object[]
            {
                "UPDATE users SET last_online = '",
                Azure.GetUnixTimestamp(),
                "', activity_points = '",
                ActivityPoints,
                "', credits = '",
                Credits,
                "', seasonal_currency = '",
                BelCredits,
                "' WHERE id = '",
                Id,
                "' LIMIT 1; "
            }));
        }

        internal int GetQuestProgress(uint p)
        {
            int result;
            Quests.TryGetValue(p, out result);
            return result;
        }

        internal UserAchievement GetAchievementData(string p)
        {
            UserAchievement result;
            Achievements.TryGetValue(p, out result);
            return result;
        }

        internal UserTalent GetTalentData(int t)
        {
            UserTalent result;
            Talents.TryGetValue(t, out result);
            return result;
        }

        internal int GetCurrentTalentLevel()
        {
            return Talents.Values.Select(current => Azure.GetGame().GetTalentManager().GetTalent(current.TalentId).Level).Concat(new[] {1}).Max();
        }

        internal void _LoadMyGroups()
        {
            DataTable dTable;
            using (var dbClient = Azure.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery(string.Format("SELECT id FROM groups_data WHERE owner_id = {0}", Id));
                dTable = dbClient.GetTable();
            }

            foreach (DataRow dRow in dTable.Rows)
                _myGroups.Add(Convert.ToUInt32(dRow["id"]));

            _loadedMyGroups = true;
        }

        internal bool GotPollData(uint pollId) { return (AnsweredPolls.Contains(pollId)); }

        internal bool CheckTrading()
        {
            if (!TradeLocked)
                return true;
            if (TradeLockExpire < Azure.GetUnixTimestamp())
                return false;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Format("UPDATE users SET trade_lock = '0' WHERE id = {0}", Id));
            TradeLocked = false;
            return true;
        }

        private GameClient GetClient() { return Azure.GetGame().GetClientManager().GetClientByUserId(Id); }
    }
}