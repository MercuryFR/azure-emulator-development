using System.Collections.Generic;
using Azure.Messages;

namespace Azure.HabboHotel.Polls
{
    internal class Poll
    {
        internal uint Id;
        internal uint RoomId;
        internal string PollName;
        internal string PollInvitation;
        internal string Thanks;
        internal string Prize;
        internal PollType Type;
        internal List<PollQuestion> Questions;
        internal int answersPositive;
        internal int answersNegative;

        internal Poll(uint id, uint roomId, string pollName, string pollInvitation, string thanks, string prize, int type, List<PollQuestion> questions)
        {
            this.Id = id;
            this.RoomId = roomId;
            this.PollName = pollName;
            this.PollInvitation = pollInvitation;
            this.Thanks = thanks;
            this.Type = (PollType)type;
            this.Prize = prize;
            this.Questions = questions;
            this.answersPositive = 0;
            this.answersNegative = 0;
        }

        internal enum PollType
        {
            Opinion,
            Prize_Badge,
            Prize_Furni,
            Matching
        }

        internal void Serialize(ServerMessage message)
        {
            message.AppendInteger(this.Id);
            message.AppendString("");//?
            message.AppendString(this.PollInvitation);
        }
    }
}