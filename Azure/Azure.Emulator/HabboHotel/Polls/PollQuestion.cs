using System.Collections.Generic;
using System.Linq;
using Azure.Messages;

namespace Azure.HabboHotel.Polls
{
    internal class PollQuestion
    {
        internal uint Index;
        internal string Question;
        internal PollAnswerType AType;
        internal List<string> Answers = new List<string>();
        internal string CorrectAnswer;

        internal PollQuestion(uint index, string question, int aType, IEnumerable<string> answers, string correctAnswer)
        {
            this.Index = index;
            this.Question = question;
            this.AType = (PollAnswerType)aType;
            this.Answers = answers.ToList();
            this.CorrectAnswer = correctAnswer;
        }

        internal enum PollAnswerType
        {
            RadioSelection = 1,
            Selection = 2,
            Text = 3,
        }

        public void Serialize(ServerMessage message, int questionNumber)
        {
            message.AppendInteger(this.Index);
            message.AppendInteger(questionNumber);
            message.AppendInteger((int)this.AType);
            message.AppendString(this.Question);
            if (this.AType != PollAnswerType.Selection && this.AType != PollAnswerType.RadioSelection)
            {
                return;
            }
            message.AppendInteger(1);
            message.AppendInteger(this.Answers.Count);
            foreach (string current in this.Answers)
            {
                message.AppendString(current);
                message.AppendString(current);
            }
        }
    }
}