using System.Collections.Generic;
using System.Data;
using System.Linq;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.HabboHotel.Polls
{
    internal class PollManager
    {
        internal Dictionary<uint, Poll> Polls;

        internal PollManager()
        {
            this.Polls = new Dictionary<uint, Poll>();
        }

        internal void Init(IQueryAdapter dbClient, out uint pollLoaded)
        {
            this.Init(dbClient);
            pollLoaded = (uint)this.Polls.Count;
        }

        internal void Init(IQueryAdapter dbClient)
        {
            this.Polls.Clear();
            dbClient.SetQuery("SELECT * FROM polls_data WHERE enabled = '1'");
            DataTable table = dbClient.GetTable();
            if (table == null)
            {
                return;
            }
            foreach (DataRow dataRow in table.Rows)
            {
                uint num = uint.Parse(dataRow["id"].ToString());
                dbClient.SetQuery(string.Format("SELECT * FROM polls_questions WHERE poll_id = {0}", num));
                DataTable table2 = dbClient.GetTable();
                List<PollQuestion> list = (from DataRow dataRow2 in table2.Rows
                                           select new PollQuestion(uint.Parse(dataRow2["id"].ToString()), (string)dataRow2["question"], int.Parse(dataRow2["answertype"].ToString()), dataRow2["answers"].ToString().Split('|'), (string)dataRow2["correct_answer"])).ToList();
                var value = new Poll(num, uint.Parse(dataRow["room_id"].ToString()), (string)dataRow["caption"], (string)dataRow["invitation"], (string)dataRow["greetings"], (string)dataRow["prize"], int.Parse(dataRow["type"].ToString()), list);
                this.Polls.Add(num, value);
            }
        }

        internal bool TryGetPoll(uint roomId, out Poll poll)
        {
            foreach (Poll current in this.Polls.Values.Where(current => current.RoomId == roomId))
            {
                poll = current;
                return true;
            }
            poll = null;
            return false;
        }
        internal Poll TryGetPollById(uint id)
        {
            foreach (Poll current in this.Polls.Values.Where(current => current.Id == id))
            {
                return current;
            }
            return null;
        }
    }
}