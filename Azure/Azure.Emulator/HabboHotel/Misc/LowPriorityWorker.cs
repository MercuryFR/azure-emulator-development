using System;
using System.Threading;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.HabboHotel.Misc
{
    class LowPriorityWorker
    {
        private static int _userPeak;
        private static Timer _mTimer;

        internal static void Init(IQueryAdapter dbClient)
        {
            dbClient.SetQuery("SELECT userpeak FROM server_status");
            _userPeak = dbClient.GetInteger();
        }

        internal static void StartProcessing() { _mTimer = new Timer(Process, null, 0, 60000); }

        internal static void Process(object caller)
        {
            var clientCount = Azure.GetGame().GetClientManager().ClientCount;
            var loadedRoomsCount = Azure.GetGame().GetRoomManager().LoadedRoomsCount;
            var dateTime = new DateTime((DateTime.Now - Azure.ServerStarted).Ticks);
            var text = dateTime.ToString("HH:mm:ss");
            Console.Title = string.Concat(new object[]
            {
                "AzureEmulator v", Azure.PrettyBuild, " | TIME: ",
                text,
                " | ONLINE COUNT: ",
                clientCount,
                " | ROOM COUNT: ",
                loadedRoomsCount
            });
            if (clientCount > _userPeak)
                _userPeak = clientCount;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE server_status SET stamp = '",
                    Azure.GetUnixTimestamp(),
                    "', users_online = ",
                    clientCount,
                    ", rooms_loaded = ",
                    loadedRoomsCount,
                    ", server_ver = 'Azure Emulator', userpeak = ",
                    _userPeak
                }));
        }
    }
}