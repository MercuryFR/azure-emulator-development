using System.Linq;
using System.Timers;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

namespace Azure.HabboHotel.Misc
{
    class CoinsManager
    {
        static Timer _timer;

        internal void StartTimer()
        {
            if (!ExtraSettings.CURRENCY_LOOP_ENABLED)
                return;
            _timer = new Timer(ExtraSettings.CURRENTY_LOOP_TIME_IN_MINUTES * 60000);
            _timer.Elapsed += GiveCoins;
            _timer.Enabled = true;
        }

        internal void GiveCoins(object source, ElapsedEventArgs e)
        {
            var clients = Azure.GetGame().GetClientManager().Clients.Values;
            foreach (
                var client in clients.Cast<GameClient>().Where(client => client != null && client.GetHabbo() != null))
            {
                client.GetHabbo().Credits += ExtraSettings.CREDITS_TO_GIVE;
                client.GetHabbo().UpdateCreditsBalance();
                client.GetHabbo().ActivityPoints += ExtraSettings.PIXELS_TO_GIVE;
                if (ExtraSettings.DIAMONDS_LOOP_ENABLED)
                    if (ExtraSettings.DIAMONDS_VIP_ONLY)
                        if (client.GetHabbo().VIP || client.GetHabbo().Rank >= 6)
                            client.GetHabbo().BelCredits += ExtraSettings.DIAMONDS_TO_GIVE;
                        else
                            client.GetHabbo().BelCredits += ExtraSettings.DIAMONDS_TO_GIVE;
                client.GetHabbo().UpdateSeasonalCurrencyBalance();
            }
        }

        internal void Destroy()
        {
            _timer.Dispose();
            _timer = null;
        }
    }
}