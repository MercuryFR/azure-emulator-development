using System.Collections.Generic;
using System.Linq;

namespace Azure.HabboHotel.Misc
{
    class AntiMutant
    {
        private readonly Dictionary<string, Dictionary<string, Figure>> _parts;

        public AntiMutant()
        {
            _parts = new Dictionary<string, Dictionary<string, Figure>>();
        }

        internal string RunLook(string Look)
        {
            var toReturnFigureParts = new List<string>();
            var fParts = new List<string>();
            string[] requiredParts = {"hd", "ch"};
            var flagForDefault = false;

            var figureParts = Look.Split('.');
            var genderLook = GetLookGender(Look);
            foreach (string Part in figureParts)
            {
                var newPart = Part;
                var tPart = Part.Split('-');
                if (tPart.Count() < 2)
                {
                    flagForDefault = true;
                    continue;
                }
                var partName = tPart[0];
                var partId = tPart[1];
                if (!_parts.ContainsKey(partName) || !_parts[partName].ContainsKey(partId) ||
                    (genderLook != "U" && _parts[partName][partId].Gender != "U" &&
                     _parts[partName][partId].Gender != genderLook))
                    newPart = SetDefault(partName, genderLook);
                if (!fParts.Contains(partName))
                    fParts.Add(partName);
                if (!toReturnFigureParts.Contains(newPart))
                    toReturnFigureParts.Add(newPart);
            }

            if (flagForDefault)
            {
                toReturnFigureParts.Clear();
                toReturnFigureParts.AddRange("hr-115-42.hd-190-1.ch-215-62.lg-285-91.sh-290-62".Split('.'));
            }

            foreach (string requiredPart in requiredParts.Where(requiredPart => !fParts.Contains(requiredPart) &&
                                                                                !toReturnFigureParts.Contains(
                                                                                    SetDefault(requiredPart, genderLook)))
                )
                toReturnFigureParts.Add(SetDefault(requiredPart, genderLook));
            return string.Join(".", toReturnFigureParts);
        }


        private string GetLookGender(string look)
        {
            var figureParts = look.Split('.');

            foreach (string part in figureParts)
            {
                var tPart = part.Split('-');
                if (tPart.Count() < 2)
                    continue;
                var partName = tPart[0];
                var partId = tPart[1];
                if (partName != "hd")
                    continue;
                return _parts.ContainsKey(partName) && _parts[partName].ContainsKey(partId)
                    ? _parts[partName][partId].Gender
                    : "U";
            }
            return "U";
        }

        private string SetDefault(string partName, string gender)
        {
            var partId = "0";
            if (!_parts.ContainsKey(partName))
                return string.Format("{0}-{1}-0", partName, partId);
            var part = _parts[partName].FirstOrDefault(x => x.Value.Gender == gender || gender == "U");
            partId = part.Equals(default(KeyValuePair<string, Figure>)) ? "0" : part.Key;
            return string.Format("{0}-{1}-0", partName, partId);
        }
    }
}