using System.Linq;
using Azure.HabboHotel.Polls;
using Azure.HabboHotel.SoundMachine;
using Azure.HabboHotel.Support;
using Azure.HabboHotel.Rooms;

namespace Azure.HabboHotel.Misc
{
    using System;
    using System.Data;
    using System.Text;
    using System.Threading.Tasks;
    using Configuration;
    using global::Azure;
    using GameClients;
    using PathFinding;
    using Pets;
    using Rooms.Games;
    using Rooms.RoomInvokedItems;
    using Messages;
    using Messages.Parsers;
    using Util;
    using System.Threading;

    class ChatCommandHandler
    {
        public DataTable Commands;
        public GameClient Session;

        public ChatCommandHandler(GameClient session)
        {
            Session = session;
            LoadCommandsList();
        }

        public static string MergeParams(string[] pms, int start)
        {
            var mergedParams = new StringBuilder();

            for (var i = 0; i < pms.Length; i++)
            {
                if (i < start)
                    continue;

                if (i > start)
                    mergedParams.Append(" ");

                mergedParams.Append(pms[i]);
            }

            return mergedParams.ToString();
        }

        public static void SendChatMessage(GameClient session, string message) { session.SendWhisper(message); }

        public void LoadCommandsList()
        {
            using (var dbClient = Azure.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery(
                    string.Format(
                        "SELECT command,params,description FROM server_fuses WHERE rank <= {0} ORDER BY command ASC",
                        Session.GetHabbo().Rank));
                Commands = dbClient.GetTable();
            }
        }

        public bool Parse(string input)
        {
            if (!input.StartsWith(":"))
                return false;
            input = input.Substring(1);
            var pms = input.Split(' ');

            switch (pms[0].ToLower())
            {
                case "commands":
                case "comandos":
                    if (Session.GetHabbo().GotCommand("commands"))
                    {
                        var builder = new StringBuilder();
                        builder.Append("Your commands:\n");
                        foreach (DataRow row in Commands.Rows)
                            builder.AppendFormat(":{0} {1} - {2}\n", Convert.ToString(row[0]),
                                Convert.ToString(row[1]), Convert.ToString(row[2]));
                        Session.SendNotifWithScroll(builder.ToString());
                    }
                    return true;
                case "about":
                case "info":
                {
                    var message =
                        new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));

                    message.AppendString("Azure");
                    message.AppendInteger(4);
                    message.AppendString("title");
                    message.AppendString("About the server");
                    message.AppendString("message");
                    var info = new StringBuilder();
                    info.Append("<b>Azure the Revolution it's your Revolution.</b>");
                    info.Append("<br />");
                    info.AppendFormat(
                        "<font color=\"#002C59\"><b>Azure</b></font>  developed by <i>Xdr</i>, <i>bi0s</i>, <i>Antoine</i><br />");
                    info.Append("<i>Other Developers</i> IhToN, VabboH, Jonas, Maartenvn, Jamal<br />");
                    info.Append("<i>Contributors</i> KyleeProZ, AndersonAge<br />");
                    var userCount = Azure.GetGame().GetClientManager().Clients.Count;
                    var roomsCount = Azure.GetGame().GetRoomManager().LoadedRooms.Count;
                    info.AppendFormat("<b>Users:</b> {0} in {1}{2}.<br /><br /><br />", userCount, roomsCount,
                        (roomsCount == 1) ? " Room" : " Rooms");
                    message.AppendString(info.ToString());
                    message.AppendString("linkUrl");
                    message.AppendString("event:");
                    message.AppendString("linkTitle");
                    message.AppendString("ok");
                    Session.SendMessage(message);
                }
                    return true;

                case "sit":
                {
                    var currentRoom = Session.GetHabbo().CurrentRoom;
                    if (currentRoom == null)
                        return true;

                    var roomUserByHabbo = currentRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    if (roomUserByHabbo == null)
                        return true;

                    if (((roomUserByHabbo.IsLyingDown || roomUserByHabbo.IsLyingDown) ||
                         (roomUserByHabbo.RidingHorse || roomUserByHabbo.IsWalking)) ||
                        roomUserByHabbo.Statusses.ContainsKey("sit"))
                        return true;

                    if ((roomUserByHabbo.RotBody % 2) != 0)
                        roomUserByHabbo.RotBody--;
                    roomUserByHabbo.Statusses.Add("sit", "0.55");
                    roomUserByHabbo.IsSitting = true;
                    roomUserByHabbo.UpdateNeeded = true;
                    return true;
                }

                case "lay":
                {
                    var currentRoom = Session.GetHabbo().CurrentRoom;
                    if (currentRoom == null)
                        return true;

                    var roomUserByHabbo = currentRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    if (roomUserByHabbo == null)
                        return true;

                    if (((roomUserByHabbo.IsSitting || roomUserByHabbo.IsSitting) ||
                         (roomUserByHabbo.RidingHorse || roomUserByHabbo.IsWalking)) ||
                        roomUserByHabbo.Statusses.ContainsKey("lay"))
                        return true;

                    if ((roomUserByHabbo.RotBody % 2) != 0)
                        roomUserByHabbo.RotBody--;
                    roomUserByHabbo.Statusses.Add("lay", "0.55");
                    roomUserByHabbo.IsLyingDown = true;
                    roomUserByHabbo.UpdateNeeded = true;
                    return true;
                }

                case "stand":
                {
                    var room = Session.GetHabbo().CurrentRoom;
                    if (room == null)
                        return true;

                    var user = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    if ((user == null))
                        return true;

                    if (user.IsSitting)
                    {
                        user.Statusses.Remove("sit");
                        user.IsSitting = false;
                        user.UpdateNeeded = true;
                    }
                    else if (user.IsLyingDown)
                    {
                        user.Statusses.Remove("lay");
                        user.IsLyingDown = false;
                        user.UpdateNeeded = true;
                    }
                    return true;
                }

                case "ejectpets":
                case "pickpets":
                {
                    if (!Session.GetHabbo().GotCommand("pickall"))
                        return true;
                    var room = Session.GetHabbo().CurrentRoom;
                    if (room == null)
                        return true;
                    foreach (
                        var pet in
                            room.GetRoomUserManager().GetPets().Where(pet => pet.OwnerId == Session.GetHabbo().Id))
                    {
                        Session.GetHabbo().GetInventoryComponent().AddPet(pet);
                        room.GetRoomUserManager().RemoveBot(pet.VirtualId, false);
                    }
                    Session.SendMessage(Session.GetHabbo().GetInventoryComponent().SerializePetInventory());
                    return true;
                }

                case "pickall":
                {
                    if (!Session.GetHabbo().GotCommand("pickall"))
                        return true;
                    var room = Session.GetHabbo().CurrentRoom;
                    if ((room == null) || !room.CheckRights(Session, true, false))
                    {
                        Session.SendNotif("Ocurri\x00f3 un error.");
                        return true;
                    }
                    var roomItemList = room.GetRoomItemHandler().RemoveAllFurniture(Session);
                    if (Session.GetHabbo().GetInventoryComponent() == null)
                        return true;
                    Session.GetHabbo().GetInventoryComponent().AddItemArray(roomItemList);
                    Session.GetHabbo().GetInventoryComponent().UpdateItems(false);
                    return true;
                }

                case "unbugwalk":
                case "unbug":
                case "desbuggear":
                case "desbuggearsala":
                {
                    var room = Session.GetHabbo().CurrentRoom;
                    if (room == null || !room.CheckRights(Session, true))
                        return true;
                    room.GetRoomUserManager().ToSet.Clear();
                    foreach (var user in room.GetRoomUserManager().GetRoomUsers())
                        user.ClearMovement(true);

                    return true;
                }
                case "unload":
                case "reload":
                    if (Session.GetHabbo().GotCommand("reload"))
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        if ((room != null) && room.CheckRights(Session, true, false))
                            Azure.GetGame().GetRoomManager().UnloadRoom(room, "Unload command");
                    }
                    return true;

                case "setmax":
                    if (!Session.GetHabbo().GotCommand("setmax"))
                        return true;
                    if (pms.Length != 1)
                    {
                        try
                        {
                            var maxUsers = int.Parse(pms[1]);
                            if ((maxUsers > 100) && (Session.GetHabbo().Rank < 3))
                            {
                                Session.SendNotif("El m\x00e1ximo es 100");
                                return true;
                            }
                            if ((maxUsers < 10) && (Session.GetHabbo().Rank < 3))
                            {
                                Session.SendNotif("El m\x00ednimo es 10");
                                return true;
                            }
                            var room5 = Session.GetHabbo().CurrentRoom;
                            room5.UsersMax = maxUsers;
                            room5.SetMaxUsers(maxUsers);
                        }
                        catch
                        {
                        }
                        return true;
                    }
                    SendChatMessage(Session, "You must enter a number to set");
                    return true;

                case "userinfo":
                case "ui":
                {
                    if (!Session.GetHabbo().GotCommand("userinfo"))
                        return true;
                    var str = pms[1];

                    if (!string.IsNullOrEmpty(str))
                    {
                        var clientByUserName = Azure.GetGame().GetClientManager().GetClientByUserName(str);
                        if ((clientByUserName == null) || (clientByUserName.GetHabbo() == null))
                        {
                            using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
                            {
                                adapter.SetQuery(
                                    "SELECT username, rank, online, id, motto, credits FROM users WHERE Username=@user LIMIT 1");
                                adapter.AddParameter("user", str);
                                var row2 = adapter.GetRow();
                                Session.SendNotif(
                                    string.Format(
                                        "User Info for {0}:\rRank: {1} \rUser Id: {2} \rMotto: {3} \rCredits: {4} \r",
                                        str, row2[1], row2[3], row2[4], row2[5]));
                            }
                            return true;
                        }
                        var habbo = clientByUserName.GetHabbo();
                        var builder = new StringBuilder();
                        if (habbo.CurrentRoom != null)
                        {
                            builder.AppendFormat(" - ROOM INFORMAtiON [{0}] - \r", habbo.CurrentRoom.RoomId);
                            builder.AppendFormat("Owner: {0}\r", habbo.CurrentRoom.Owner);
                            builder.AppendFormat("Room Name: {0}\r", habbo.CurrentRoom.Name);
                            builder.Append(
                                string.Concat(new object[]
                                {"Current Users: ", habbo.CurrentRoom.UserCount, "/", habbo.CurrentRoom.UsersMax}));
                        }
                        Session.SendNotif(string.Concat(new object[]
                        {
                            "User info for: ", str, ":\rRank: ", habbo.Rank, " \rOnline: ", "1",
                            " \rUser Id: ", habbo.Id, " \rCurrent Room: ", habbo.CurrentRoomId, " \rMotto: ",
                            habbo.Motto, " \rCredits: ", habbo.Credits, " \rMuted: ", habbo.Muted.ToString(),
                            "\r\r\r", builder.ToString()
                        }));
                        return true;
                    }
                    Session.SendNotif("Please enter a UserName");
                    return true;
                }
                case "disablediagonal":
                case "disablediag":
                case "togglediagonal":
                    if (Session.GetHabbo().GotCommand("disablediagonal"))
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        if ((room == null) || !room.CheckRights(Session, true, false))
                            return true;
                        room.GetGameMap().DiagonalEnabled = !room.GetGameMap().DiagonalEnabled;
                    }
                    return true;

                case "freeze":
                    if (Session.GetHabbo().GotCommand("freeze"))
                    {
                        var user = Session.GetHabbo()
                            .CurrentRoom.GetRoomUserManager()
                            .GetRoomUserByHabbo(pms[1]);
                        if (user != null)
                            user.Frozen = true;
                    }
                    return true;

                case "unfreeze":
                    if (Session.GetHabbo().GotCommand("unfreeze"))
                    {
                        var user = Session.GetHabbo()
                            .CurrentRoom.GetRoomUserManager()
                            .GetRoomUserByHabbo(pms[1]);
                        if (user != null)
                            user.Frozen = false;
                    }
                    return true;

                case "setspeed":
                    if (Session.GetHabbo().GotCommand("setspeed"))
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        if ((room == null) || !room.CheckRights(Session, true, false))
                            return true;
                        try
                        {
                            Session.GetHabbo().CurrentRoom.GetRoomItemHandler().SetSpeed(int.Parse(pms[1]));
                        }
                        catch
                        {
                            Session.SendNotif("Numbers Only!");
                        }
                    }
                    return true;
                case "regenmaps":
                case "reloadmaps":
                case "fixroom":
                {
                    if (!Session.GetHabbo().GotCommand("regenmaps"))
                        return true;
                    var room = Session.GetHabbo().CurrentRoom;
                    if (room == null || !room.CheckRights(Session, true, false))
                        return true;

                    room.GetGameMap().GenerateMaps(true);
                    Session.SendNotif("The room map has been refreshed!");
                    return true;
                }

                case "convertcredits":
                case "redeemall":
                    if (Session.GetHabbo().GotCommand("redeemall"))
                        try
                        {
                            Session.GetHabbo().GetInventoryComponent().Redeemcredits(Session);
                            SendChatMessage(Session, "All credits were succesfully converted!");
                        }
                        catch
                        {
                        }
                    return true;

                case "setvideo":
                case "ponervideo":
                case "colocarvideo":
                    if (Session.GetHabbo().GotCommand("setvideo"))
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        if (room == null || !room.CheckRights(Session))
                            return true;
                        if (pms.Length < 2)
                            return true;
                        var video = pms[1].Replace("https://", "http://").Split('&')[0];
                        Session.SendNotif(
                            Azure.GetGame().GetVideoManager().PlayVideoInRoom(room, video)
                                ? "Good, your custom video is being now played in all the room TVs! Double click one, wait and you'll see."
                                : "There was an error. Please, verify your video link is correct.");
                    }
                    return true;

                case "mutebots":
                case "mutepets":
                    if (Session.GetHabbo().GotCommand("mutebots") &&
                        Session.GetHabbo().CurrentRoom.CheckRights(Session, true, false))
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        room.MutedBots = !room.MutedBots;
                        SendChatMessage(Session, "Muted bots have been toggled");
                    }
                    return true;

                case "dance":
                {
                    var result = 1;
                    if ((pms.Length > 1) && int.TryParse(pms[1], out result))
                        result = 1;
                    if ((result > 4) || (result < 0))
                    {
                        Session.SendWhisper("The dance ID must be between 0 and 4!");
                        result = 0;
                    }
                    var message = new ServerMessage();
                    message.Init(LibraryParser.OutgoingRequest("DanceStatusMessageComposer"));
                    message.AppendInteger(Session.CurrentRoomUserId);
                    message.AppendInteger(result);
                    Session.GetHabbo().CurrentRoom.SendMessage(message);
                    return true;
                }
                case "deletegroup":
                {
                    if (!Session.GetHabbo().GotCommand("deletegroup"))
                        return true;
                    var room = Session.GetHabbo().CurrentRoom;
                    if (room.CheckRights(Session, true, false))
                    {
                        if ((pms.Length == 1) || (pms[1].ToLower() != "yes"))
                        {
                            Session.SendNotif(
                                "Are you sure you want to delete this group?\nOnce you delete it you will never be able to get it back.\n\nTo continue, type ':deletegroup yes' (without '')");
                            return true;
                        }
                        if (room.Group == null)
                        {
                            Session.SendNotif("This room does not have a group.");
                            return true;
                        }
                        var group = room.Group;
                        foreach (var user7 in @group.Members.Values)
                        {
                            var clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(user7.Id);
                            if (clientByUserId == null)
                                continue;
                            clientByUserId.GetHabbo().UserGroups.Remove(user7);
                            if (clientByUserId.GetHabbo().FavouriteGroup == @group.Id)
                                clientByUserId.GetHabbo().FavouriteGroup = 0;
                        }
                        room.RoomData.Group = null;
                        Azure.GetGame().GetGroupManager().DeleteGroup(@group.Id);
                        Azure.GetGame().GetRoomManager().UnloadRoom(room, "Group deleted");
                        return true;
                    }
                    Session.SendNotif("You do not own this room!");
                    return true;
                }
                case "moonwalk":
                {
                    if (!Session.GetHabbo().HasFuse("fuse_vip_commands") && !Session.GetHabbo().VIP)
                        return false;
                    var room = Session.GetHabbo().CurrentRoom;
                    if (room == null)
                        return true;

                    var user = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    user.IsMoonwalking = !user.IsMoonwalking;
                    return true;
                }
                case "habnam":
                {
                    if (!Session.GetHabbo().HasFuse("fuse_vip_commands") && !Session.GetHabbo().VIP)
                        return false;
                    var room = Session.GetHabbo().CurrentRoom;
                    if (room == null)
                        return true;

                    var user = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    Session.GetHabbo()
                        .GetAvatarEffectsInventoryComponent()
                        .ActivateCustomEffect((user.CurrentEffect != 140) ? 140 : 0);
                    return true;
                }
                case "faceless":
                {
                    if (!Session.GetHabbo().HasFuse("faceless"))
                        return true;
                    var figureParts = Session.GetHabbo().Look.Split('.');
                    foreach (string part in figureParts)
                        if (part.StartsWith("hd"))
                        {
                            var headParts = part.Split('-');
                            if (!headParts[1].Equals("99999"))
                                headParts[1] = "99999";
                            else
                                break;
                            var newHead = string.Format("hd-{0}-{1}", headParts[1], headParts[2]);
                            Session.GetHabbo().Look = Session.GetHabbo().Look.Replace(part, newHead);
                            break;
                        }
                    using (var dbClient = Azure.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.SetQuery(
                            "UPDATE users SET look = @look WHERE username = @username");
                        dbClient.AddParameter("look", Session.GetHabbo().Look);
                        dbClient.AddParameter("username", Session.GetHabbo().UserName);
                        dbClient.RunQuery();
                    }
                    var room = Session.GetHabbo().CurrentRoom;
                    if (room == null)
                        return true;
                    var user = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    if (user == null)
                        return true;
                    Session.GetMessageHandler()
                        .GetResponse()
                        .Init(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                    Session.GetMessageHandler().GetResponse().AppendInteger(-1);
                    Session.GetMessageHandler().GetResponse().AppendString(Session.GetHabbo().Look);
                    Session.GetMessageHandler().GetResponse().AppendString(Session.GetHabbo().Gender.ToLower());
                    Session.GetMessageHandler().GetResponse().AppendString(Session.GetHabbo().Motto);
                    Session.GetMessageHandler()
                        .GetResponse()
                        .AppendInteger(Session.GetHabbo().AchievementPoints);
                    Session.GetMessageHandler().SendResponse();
                    var roomUpdate =
                        new ServerMessage(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                    roomUpdate.AppendInteger(user.VirtualId);
                    roomUpdate.AppendString(Session.GetHabbo().Look);
                    roomUpdate.AppendString(Session.GetHabbo().Gender.ToLower());
                    roomUpdate.AppendString(Session.GetHabbo().Motto);
                    roomUpdate.AppendInteger(Session.GetHabbo().AchievementPoints);
                    room.SendMessage(roomUpdate);
                    return true;
                }
                case "mimic":
                case "copylook":
                case "copy":
                    if (Session.GetHabbo().HasFuse("fuse_vip_commands") || Session.GetHabbo().VIP)
                    {
                        var pName = pms[1];
                        var user = Session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(pName);
                        if (user == null)
                            return true;

                        var query = user.GetClient().GetHabbo().Gender;
                        var look = user.GetClient().GetHabbo().Look;
                        Session.GetHabbo().Gender = query;
                        Session.GetHabbo().Look = look;
                        using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            adapter.SetQuery(
                                "UPDATE users SET gender = @gender, look = @look WHERE Username = @Username");
                            adapter.AddParameter("gender", query);
                            adapter.AddParameter("look", look);
                            adapter.AddParameter("Username", Session.GetHabbo().UserName);
                            adapter.RunQuery();
                        }
                        var room = Session.GetHabbo().CurrentRoom;
                        if (room == null)
                            return true;
                        var user2 = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                        if (user2 == null)
                            return true;
                        Session.GetMessageHandler()
                            .GetResponse()
                            .Init(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                        Session.GetMessageHandler().GetResponse().AppendInteger(-1);
                        Session.GetMessageHandler().GetResponse().AppendString(Session.GetHabbo().Look);
                        Session.GetMessageHandler()
                            .GetResponse()
                            .AppendString(Session.GetHabbo().Gender.ToLower());
                        Session.GetMessageHandler().GetResponse().AppendString(Session.GetHabbo().Motto);
                        Session.GetMessageHandler()
                            .GetResponse()
                            .AppendInteger(Session.GetHabbo().AchievementPoints);
                        Session.GetMessageHandler().SendResponse();
                        var message =
                            new ServerMessage(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                        message.AppendInteger(user2.VirtualId);
                        message.AppendString(Session.GetHabbo().Look);
                        message.AppendString(Session.GetHabbo().Gender.ToLower());
                        message.AppendString(Session.GetHabbo().Motto);
                        message.AppendInteger(Session.GetHabbo().AchievementPoints);
                        room.SendMessage(message);
                    }
                    return true;

                case "push":
                {
                    if (!Session.GetHabbo().HasFuse("fuse_vip_commands") && !Session.GetHabbo().VIP)
                        return true;
                    var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                    if (room == null)
                        return true;
                    if (pms.Length == 1)
                    {
                        SendChatMessage(Session, "Ingresa un usuario");
                        return true;
                    }
                    var user = room.GetRoomUserManager().GetRoomUserByHabbo(Convert.ToString(pms[1]));
                    if (user == null)
                    {
                        SendChatMessage(Session, "No se pudo encontrar el user!");
                        return true;
                    }
                    if (user.GetUserName() == Session.GetHabbo().UserName)
                    {
                        SendChatMessage(Session, "S\x00e9 que no quieres empujarte a ti mismo");
                        return true;
                    }
                    var user2 = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    if ((user2 == null) || user.TeleportEnabled)
                        return true;
                    if ((Math.Abs((user.X - user2.X)) < 2) && (Math.Abs((user.Y - user2.Y)) < 2))
                    {
                        switch (user2.RotBody)
                        {
                            case 0:
                                user.MoveTo(user.X, user.Y - 1);
                                break;
                            case 1:
                                user.MoveTo(user.X + 1, user.Y);
                                user.MoveTo(user.X, user.Y - 1);
                                break;
                            case 2:
                                user.MoveTo(user.X + 1, user.Y);
                                break;
                            case 3:
                                user.MoveTo(user.X + 1, user.Y);
                                user.MoveTo(user.X, user.Y + 1);
                                break;
                            case 4:
                                user.MoveTo(user.X, user.Y + 1);
                                break;
                            case 5:
                                user.MoveTo(user.X - 1, user.Y);
                                user.MoveTo(user.X, user.Y + 1);
                                break;
                            case 6:
                                user.MoveTo(user.X - 1, user.Y);
                                break;
                            case 7:
                                user.MoveTo(user.X - 1, user.Y);
                                user.MoveTo(user.X, user.Y - 1);
                                break;
                        }

                        user.UpdateNeeded = true;
                        user2.UpdateNeeded = true;
                        user2.SetRot(PathFinder.CalculateRotation(user2.X, user2.Y, user.GoalX,
                            user.GoalY));
                    }
                    else
                        SendChatMessage(Session, string.Format("{0} no est\x00e1 tan cerca.", pms[1]));
                    return true;
                }
                case "pull":
                    if (Session.GetHabbo().HasFuse("fuse_vip_commands") || Session.GetHabbo().VIP)
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        if (room == null)
                            return true;
                        var user = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                        if (user == null)
                            return true;
                        if (pms.Length == 1)
                        {
                            SendChatMessage(Session, "Unable to find the user you specified.");
                            return true;
                        }
                        var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                        if (client == null)
                            return true;
                        if (client.GetHabbo().Id == Session.GetHabbo().Id)
                        {
                            SendChatMessage(Session, "You can't pull yourself!");
                            return true;
                        }
                        var user2 = room.GetRoomUserManager().GetRoomUserByHabbo(client.GetHabbo().Id);
                        if (user2 == null)
                            return true;
                        if (user2.TeleportEnabled)
                            return true;
                        if ((Math.Abs((user.X - user2.X)) >= 3) || (Math.Abs((user.Y - user2.Y)) >= 3))
                        {
                            SendChatMessage(Session, "User is too far away to pull.");
                            return true;
                        }
                        if ((user.RotBody % 2) != 0)
                            user.RotBody--;
                        switch (user.RotBody)
                        {
                            case 0:
                                user2.MoveTo(user.X, user.Y - 1);
                                break;
                            case 2:
                                user2.MoveTo(user.X + 1, user.Y);
                                break;
                            case 4:
                                user2.MoveTo(user.X, user.Y + 1);
                                break;
                            case 6:
                                user2.MoveTo(user.X - 1, user.Y);
                                break;
                        }
                    }
                    return true;

                case "enable":
                {
                    if (!Session.GetHabbo().HasFuse("fuse_vip_commands") && !Session.GetHabbo().VIP)
                        return false;
                    if (pms.Length == 1)
                        return true;
                    var user =
                        Session.GetHabbo()
                            .CurrentRoom.GetRoomUserManager()
                            .GetRoomUserByHabbo(Session.GetHabbo().UserName);
                    if (user.RidingHorse)
                        return true;
                    if (user.Team != Team.none)
                        return true;
                    if (user.IsLyingDown)
                        return true;
                    var s = pms[1];
                    int effect;

                    if (int.TryParse(s, out effect))
                    {
                        Session.GetHabbo()
                            .GetAvatarEffectsInventoryComponent()
                            .ActivateCustomEffect(effect);
                        return true;
                    }
                    SendChatMessage(Session,
                        string.Format("You're pretty bad at math, '{0}' ain't a number, nigga.", s));
                    return true;
                }
                case "handitem":
                {
                    if (!Session.GetHabbo().HasFuse("fuse_vip_commands") && !Session.GetHabbo().VIP)
                        return false;
                    if (pms.Length == 1)
                    {
                        SendChatMessage(Session, "Escribe un item ID");
                        return true;
                    }
                    var user =
                        Session.GetHabbo()
                            .CurrentRoom.GetRoomUserManager()
                            .GetRoomUserByHabbo(Session.GetHabbo().UserName);
                    if (user.RidingHorse)
                    {
                        SendChatMessage(Session, "No puedes hacer eso mientras est\x00e1s montado en un caballo!");
                        return true;
                    }
                    if (user.Team != Team.none)
                        return true;
                    int item;
                    if (user.IsLyingDown)
                        return true;
                    var str6 = pms[1];
                    if (int.TryParse(str6, out item))
                    {
                        user.CarryItem(item);
                        return true;
                    }
                    SendChatMessage(Session,
                        string.Format(
                            "\x00bfAcaso no has aprendido matem\x00e1ticas nunca? '{0}' no es un n\x00famero.",
                            str6));
                    return true;
                }
                case "empty":
                    if (Session.GetHabbo().GotCommand("empty"))
                        Session.GetHabbo().GetInventoryComponent().ClearItems();
                    return true;

                case "emptysom":
                    if (!Session.GetHabbo().GotCommand("emptysom"))
                        return false;
                    if (pms.Length != 1)
                    {
                        var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                        if (client == null || client.GetHabbo().Rank >= Session.GetHabbo().Rank)
                            return true;
                        client.GetHabbo().GetInventoryComponent().ClearItems();
                        return false;
                    }
                    return true;

                case "hit":
                    if (Session.GetHabbo().HasFuse("fuse_vip_commands") || Session.GetHabbo().VIP)
                    {
                        var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                        if (room == null)
                            return true;
                        if (pms.Length == 1)
                        {
                            SendChatMessage(Session, "\x00bfUsuario?");
                            return true;
                        }
                        var user = room.GetRoomUserManager().GetRoomUserByHabbo(Convert.ToString(pms[1]));
                        if (user == null)
                        {
                            SendChatMessage(Session, "El usuario no se encontr\x00f3");
                            return true;
                        }
                        if (user.GetUserName() == Session.GetHabbo().UserName)
                        {
                            SendChatMessage(Session, "\x00a1No querr\x00e1s golpearte a ti mismo!");
                            return true;
                        }
                        var user2 = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                        if (user2 == null)
                            return true;
                        if ((Math.Abs((user.X - user2.X)) >= 2) || (Math.Abs((user.Y - user2.Y)) >= 2))
                            return true;
                        switch (user2.RotBody)
                        {
                            case 0:
                                user.MoveTo(user.X, user.Y - 1);
                                break;
                            case 1:
                                user.MoveTo(user.X + 1, user.Y);
                                user.MoveTo(user.X, user.Y - 1);
                                break;
                            case 2:
                                user.MoveTo(user.X + 1, user.Y);
                                break;
                            case 3:
                                user.MoveTo(user.X + 1, user.Y);
                                user.MoveTo(user.X, user.Y + 1);
                                break;
                            case 4:
                                user.MoveTo(user.X, user.Y + 1);
                                break;
                            case 5:
                                user.MoveTo(user.X - 1, user.Y);
                                user.MoveTo(user.X, user.Y + 1);
                                break;
                            case 6:
                                user.MoveTo(user.X - 1, user.Y);
                                break;
                            case 7:
                                user.MoveTo(user.X - 1, user.Y);
                                user.MoveTo(user.X, user.Y - 1);
                                break;
                        }
                        user.UpdateNeeded = true;
                    }
                    return true;

                case "roomalert":
                case "alertroom":
                case "ra":
                case "alertarsala":
                    if (!Session.GetHabbo().GotCommand("alert"))
                        return false;
                    var alert = MergeParams(pms, 1);

                    foreach (
                        var user in
                            Session.GetHabbo()
                                .CurrentRoom.GetRoomUserManager()
                                .GetRoomUsers()
                                .Where(user => !user.IsBot && user.GetClient() != null))
                        user.GetClient().SendNotif(alert);
                    return true;

                case "alert":
                    if (!Session.GetHabbo().GotCommand("alert"))
                        return false;
                    if (pms[1] != null && pms[2] != null)
                    {
                        var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                        if (client == null)
                        {
                            Session.SendNotif("User could not be found.");
                            return true;
                        }
                        client.SendNotif(string.Format("{0} -{1}", pms[2], Session.GetHabbo().UserName));
                        return true;
                    }
                    Session.SendNotif("You left something empty.");
                    return true;

                case "kick":
                    if (Session.GetHabbo().GotCommand("kick"))
                    {
                        var userName = pms[1];
                        var session = Azure.GetGame().GetClientManager().GetClientByUserName(userName);
                        if (session != null)
                        {
                            if (Session.GetHabbo().Rank <= session.GetHabbo().Rank)
                            {
                                Session.SendNotif("You are not allowed to kick that user.");
                                return true;
                            }
                            if (session.GetHabbo().CurrentRoomId < 1)
                            {
                                Session.SendNotif("That user is not in a room and can not be kicked.");
                                return true;
                            }
                            var room = Azure.GetGame().GetRoomManager().GetRoom(session.GetHabbo().CurrentRoomId);
                            if (room == null)
                                return true;
                            room.GetRoomUserManager().RemoveUserFromRoom(session, true, false);
                            session.CurrentRoomUserId = -1;
                            if (pms.Length > 2)
                                session.SendNotif(
                                    string.Format(
                                        "A moderator has kicked you from the room for the following reason: {0}",
                                        MergeParams(pms, 2)));
                            else
                                session.SendNotif("A moderator has kicked you from the room.");
                            return true;
                        }
                        Session.SendNotif("User could not be found.");
                    }
                    return true;

                case "roommute":
                    if (Session.GetHabbo().GotCommand("roommute") || Session.GetHabbo().GotCommand("roomunmute"))
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        if (!Session.GetHabbo().CurrentRoom.RoomMuted)
                            Session.GetHabbo().CurrentRoom.RoomMuted = true;
                        var reason = MergeParams(pms, 1);
                        var message = new ServerMessage();
                        message.Init(LibraryParser.OutgoingRequest("AlertNotificationMessageComposer"));
                        message.AppendString(string.Format("La sala fue muteada debido a:\n{0}", reason));
                        message.AppendString("");
                        room.SendMessage(message);
                        Azure.GetGame()
                            .GetModerationTool()
                            .LogStaffEntry(Session.GetHabbo().UserName, string.Empty, "Room Mute", "Room muted");
                    }
                    return true;

                case "roomunmute":
                    if (Session.GetHabbo().GotCommand("roomunmute"))
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        if (Session.GetHabbo().CurrentRoom.RoomMuted)
                            Session.GetHabbo().CurrentRoom.RoomMuted = false;
                        var message = new ServerMessage();
                        message.Init(LibraryParser.OutgoingRequest("AlertNotificationMessageComposer"));
                        message.AppendString("Fuiste desmuteado");
                        message.AppendString("");
                        room.SendMessage(message);
                    }
                    return true;

                case "mute":
                {
                    if (!Session.GetHabbo().GotCommand("mute"))
                        return true;
                    var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                    if (client != null && client.GetHabbo() != null)
                    {
                        if (client.GetHabbo().Rank >= 4)
                        {
                            Session.SendNotif("You are not allowed to (un)mute that user.");
                            return true;
                        }
                        Azure.GetGame()
                            .GetModerationTool()
                            .LogStaffEntry(Session.GetHabbo().UserName, client.GetHabbo().UserName, "Mute",
                                "Muted user");
                        client.GetHabbo().Mute();
                        return true;
                    }
                    Session.SendNotif("User could not be found.");
                    return true;
                }
                case "flood":
                {
                    if (!Session.GetHabbo().GotCommand("flood"))
                        return true;
                    var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                    if (pms.Length == 3)
                    {
                        client.GetHabbo().FloodTime = Azure.GetUnixTimestamp() + Convert.ToInt32(pms[2]);
                        var message = new ServerMessage(LibraryParser.OutgoingRequest("FloodFilterMessageComposer"));
                        message.AppendInteger(Convert.ToInt32(pms[2]));
                        client.SendMessage(message);
                        return true;
                    }
                    Session.SendNotif("You must include a Username and a time for the person you want to flood.");
                    return true;
                }
                case "unmute":
                {
                    if (!Session.GetHabbo().GotCommand("unmute"))
                        return true;
                    var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                    if (client != null && client.GetHabbo() != null)
                    {
                        if (!client.GetHabbo().Muted)
                            return true;
                        Azure.GetGame()
                            .GetModerationTool()
                            .LogStaffEntry(Session.GetHabbo().UserName, client.GetHabbo().UserName, "Mute",
                                "Un Muted user");
                        client.GetHabbo().Unmute();
                        return true;
                    }
                    Session.SendNotif("User could not be found.");
                    return true;
                }
                case "summon":
                case "traer":
                case "come":
                    if (Session.GetHabbo().GotCommand("summon"))
                    {
                        if (pms.Length >= 1)
                        {
                            var userName = pms[1];
                            if (String.Equals(userName, Session.GetHabbo().UserName,
                                StringComparison.CurrentCultureIgnoreCase))
                            {
                                Session.SendNotif("You can't summon yourself!");
                                return true;
                            }
                            var client = Azure.GetGame().GetClientManager().GetClientByUserName(userName);
                            if (client == null)
                            {
                                Session.SendNotif(string.Format("Could not find user \"{0}\"", userName));
                                return true;
                            }
                            if (Session.GetHabbo().CurrentRoom != null &&
                                Session.GetHabbo().CurrentRoomId != client.GetHabbo().CurrentRoomId)
                                client.GetMessageHandler()
                                    .PrepareRoomForUser(Session.GetHabbo().CurrentRoom.RoomId,
                                        Session.GetHabbo().CurrentRoom.Password);
                            return true;
                        }
                        Session.SendNotif("No use specified");
                    }
                    return true;

                case "summonall":
                    if (Session.GetHabbo().GotCommand("summonall"))
                    {
                        var reason = input.Substring(9);
                        foreach (GameClient client in Azure.GetGame().GetClientManager().Clients.Values)
                        {
                            client.SendNotif(
                                string.Format("* Todos hab\x00e9is sido atra\x00eddos por {0}:\r\n{1}",
                                    Session.GetHabbo().UserName, reason));
                            Azure.GetGame()
                                .GetRoomManager()
                                .GenerateRoomData(Session.GetHabbo().CurrentRoomId)
                                .SerializeRoomData(new ServerMessage(), client.GetHabbo().CurrentRoom == null,
                                    client, false);
                            var roomFwd =
                                new ServerMessage(LibraryParser.OutgoingRequest("RoomForwardMessageComposer"));
                            roomFwd.AppendInteger(Session.GetHabbo().CurrentRoomId);
                            client.SendMessage(roomFwd);
                        }
                    }
                    return true;

                case "follow":
                case "seguir":
                case "stalk":
                    if (Session.GetHabbo().GotCommand("follow"))
                    {
                        var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                        if ((client != null) && (client.GetHabbo() != null))
                        {
                            if (((client.GetHabbo().CurrentRoom == null)) ||
                                (client.GetHabbo().CurrentRoom == Session.GetHabbo().CurrentRoom))
                                return true;
                            var roomFwd =
                                new ServerMessage(LibraryParser.OutgoingRequest("RoomForwardMessageComposer"));
                            roomFwd.AppendInteger(client.GetHabbo().CurrentRoom.RoomId);
                            Session.SendMessage(roomFwd);
                            return true;
                        }
                        Session.SendNotif("This user could not be found");
                    }
                    return true;

                case "roomkick":
                    if (Session.GetHabbo().GotCommand("roomkick"))
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        if (room == null)
                            return true;
                        var allert = MergeParams(pms, 1);
                        var kick = new RoomKick(allert, (int) Session.GetHabbo().Rank);
                        Azure.GetGame()
                            .GetModerationTool()
                            .LogStaffEntry(Session.GetHabbo().UserName, string.Empty, "Room kick",
                                "Kicked the whole room");
                        room.QueueRoomKick(kick);
                        return true;
                    }
                    return true;

                case "banear":
                case "ban":
                    if (Session.GetHabbo().GotCommand("ban"))
                    {
                        var user = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);

                        if (user != null)
                        {
                            if (user.GetHabbo().Rank >= Session.GetHabbo().Rank)
                            {
                                Session.SendNotif("You are not allowed to ban that user.");
                                return true;
                            }
                            var length = int.Parse(pms[2]);
                            var message = MergeParams(pms, 3);
                            if (string.IsNullOrWhiteSpace(message))
                                message = "El moderador no ha visto necesario darte un motivo de baneo";
                            ModerationTool.BanUser(Session, user.GetHabbo().Id, length, message);
                            Azure.GetGame()
                                .GetModerationTool()
                                .LogStaffEntry(Session.GetHabbo().UserName, user.GetHabbo().UserName, "Ban",
                                    string.Format("User have been banned [{0}]", pms[2]));
                        }
                        Session.SendNotif("User could not be found.");
                        return true;
                    }
                    return true;

                case "desbanear":
                case "unban":
                    if (Session.GetHabbo().GotCommand("unban"))
                    {
                        var user = Azure.GetHabboForName(pms[1]);

                        if (user != null)
                        {
                            if (user.Rank >= Session.GetHabbo().Rank)
                            {
                                Session.SendNotif("You are not allowed to unban that user.");
                                return true;
                            }
                            using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
                            {
                                adapter.SetQuery(string.Format("DELETE FROM users_bans WHERE value = '{0}'",
                                    user.UserName));
                                adapter.RunQuery();
                                Azure.GetGame()
                                    .GetModerationTool()
                                    .LogStaffEntry(Session.GetHabbo().UserName, user.UserName, "Unban",
                                        string.Format("Se ha desbaneado al usuario [{0}]", pms[2]));
                            }
                        }
                        Session.SendNotif("User could not be found.");
                        return true;
                    }
                    return true;

                case "superban":
                {
                    if (!Session.GetHabbo().GotCommand("superban"))
                        return true; //35DC;
                    var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                    if (client != null)
                    {
                        if (client.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendNotif("You are not allowed to ban that user.");
                            return true;
                        }
                        Azure.GetGame()
                            .GetModerationTool()
                            .LogStaffEntry(Session.GetHabbo().UserName, client.GetHabbo().UserName, "Ban",
                                "Long ban forever");
                        Azure.GetGame()
                            .GetBanManager()
                            .BanUser(client, Session.GetHabbo().UserName, 788922000.0, MergeParams(pms, 2),
                                false, false);
                        return true; //35DC;
                    }
                    Session.SendNotif("User could not be found.");
                    return true;
                }
                case "togglewhisper":
                    Session.GetHabbo().GotCommand("togglewhisper");
                    return true;

                case "fastwalk":
                case "run":
                    if (Session.GetHabbo().HasFuse("fuse_vip_commands") || Session.GetHabbo().VIP)
                    {
                        var user =
                            Azure.GetGame()
                                .GetRoomManager()
                                .GetRoom(Session.GetHabbo().CurrentRoomId)
                                .GetRoomUserManager()
                                .GetRoomUserByHabbo(Session.GetHabbo().Id);
                        user.FastWalking = !user.FastWalking;
                    }
                    return true;

                case "promoteroom":
                    if (!Session.GetHabbo().GotCommand("promoteroom"))
                        return true; //37B3;
                    if (pms[1] != null)
                    {
                        int time;
                        if (!int.TryParse(pms[1], out time))
                        {
                            Session.SendNotif(
                                "You need use command like :promoteroom time (time being how long to run event for in seconds).");
                            return true;
                        }
                        var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                        Azure.GetGame()
                            .GetRoomEvents()
                            .AddNewEvent(room.RoomId, "Default Name", "Default Desc", Session, 0x1c20, 1);
                        return true; //37B3;
                    }
                    Session.SendNotif("You need to enter event name and description.");
                    return true;

                case "massdance":
                    if (Session.GetHabbo().GotCommand("massdance"))
                    {
                        var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                        var user = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                        var roomUsers = room.GetRoomUserManager().GetRoomUsers();
                        var i = Convert.ToInt32(pms[1]);
                        if (i <= 4)
                        {
                            if ((i > 0) && (user.CarryItemId > 0))
                                user.CarryItem(0);
                            user.DanceId = i;
                            foreach (var roomUser in roomUsers)
                            {
                                var message =
                                    new ServerMessage(LibraryParser.OutgoingRequest("DanceStatusMessageComposer"));
                                message.AppendInteger(roomUser.VirtualId);
                                message.AppendInteger(i);
                                room.SendMessage(message);
                                roomUser.DanceId = i;
                            }
                            return true;
                        }
                        Session.SendNotif("That is an invalid dance ID");
                    }
                    return true;

                case "goboom":
                    if (Session.GetHabbo().GotCommand("goboom"))
                    {
                        var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                        room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                        foreach (var user in room.GetRoomUserManager().GetRoomUsers())
                            user.ApplyEffect(108);
                    }
                    return true;

                case "massenable":
                    if (Session.GetHabbo().GotCommand("massenable"))
                    {
                        var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                        room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                        foreach (var user in room.GetRoomUserManager().GetRoomUsers().Where(user => !user.RidingHorse))
                            user.ApplyEffect(Convert.ToInt32(pms[1]));
                    }
                    return true;

                case "givecredits":
                case "credits":
                case "coins":
                {
                    if (!Session.GetHabbo().GotCommand("credits"))
                        return true;
                    int amount;
                    var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                    if (client == null)
                    {
                        Session.SendNotif("User could not be found.");
                        return true;
                    }
                    if (!int.TryParse(pms[2], out amount))
                    {
                        Session.SendNotif("Invalid numbers no, please!");
                        return true;
                    }
                    client.GetHabbo().Credits += amount;
                    client.GetHabbo().UpdateCreditsBalance();
                    client.SendNotif(string.Format("{0} gave you {1} credits :3", Session.GetHabbo().UserName,
                        amount));
                    Session.SendNotif("You succesfully updated the user's purse!");
                }
                    return true;

                case "pixels":
                case "givepixels":
                case "duckets":
                {
                    if (!Session.GetHabbo().GotCommand("duckets"))
                        return false;
                    int amounts;
                    var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                    if (client == null)
                        Session.SendNotif("User could not be found.");
                    else if (!int.TryParse(pms[2], out amounts))
                        Session.SendNotif("Invalid numbers no, please!");
                    else
                    {
                        client.GetHabbo().ActivityPoints += amounts;
                        Session.GetHabbo().NotifyNewPixels(amounts);
                        client.GetHabbo().UpdateActivityPointsBalance();
                        client.SendNotif(string.Format("{0} gave you {1} Duckets!", Session.GetHabbo().UserName,
                            amounts));
                        Session.SendNotif("You succesfully updated the User's Duckets balance!");
                    }
                }
                    return true;

                case "belcredits":
                case "diamonds":
                case "diamantes":
                case "givediamonds":
                case "loyalty":
                {
                    if (!Session.GetHabbo().GotCommand("diamonds"))
                        return false;
                    int amount;
                    var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                    if (client == null)
                        Session.SendNotif("User could not be found.");
                    else if (!int.TryParse(pms[2], out amount))
                        Session.SendNotif("OMG Numbers only, please!");
                    else
                    {
                        client.GetHabbo().BelCredits += amount;
                        client.GetHabbo().UpdateSeasonalCurrencyBalance();
                        client.SendNotif(string.Format("{0} gave you {1} Diamonds!", Session.GetHabbo().UserName,
                            amount));
                        Session.SendNotif("You gave him)/her diamonds!");
                    }
                    return true;
                }

                case "oldha":
                case "ha":
                {
                    if (!Session.GetHabbo().GotCommand("ha"))
                        return false;
                    var str = MergeParams(pms, 1);
                    var message = new ServerMessage(LibraryParser.OutgoingRequest("BroadcastNotifMessageComposer"));
                    message.AppendString(string.Format("{0}\r\n- {1}", str, Session.GetHabbo().UserName));
                    Azure.GetGame().GetClientManager().QueueBroadcaseMessage(message);
                    Azure.GetGame()
                        .GetModerationTool()
                        .LogStaffEntry(Session.GetHabbo().UserName, string.Empty, "HotelAlert",
                            string.Format("Hotel alert [{0}]", str));
                    return true;
                }

                case "apagar":
                case "shutdown":
                {
                    if (!Session.GetHabbo().GotCommand("shutdown"))
                        return false;
                    new Task(Azure.PerformShutDown).Start();
                    Azure.GetGame()
                        .GetModerationTool()
                        .LogStaffEntry(Session.GetHabbo().UserName, string.Empty, "Shutdown",
                            "Issued shutdown command");
                    return true;
                }
                case "restart":
                {
                    if (!Session.GetHabbo().GotCommand("shutdown"))
                        return false;
                    new Task(Azure.PerformRestart).Start();
                    Azure.GetGame()
                        .GetModerationTool()
                        .LogStaffEntry(Session.GetHabbo().UserName, string.Empty, "Restart", "Issued New Restart");
                    return true;
                }
                case "selfdestruct":
                {
                    Session.Disconnect("selfdestruct");
                    return true;
                }
                case "brb":
                {
                    if (!Session.GetHabbo().GotCommand("brb"))
                    {
                        Session.SendWhisper("Je mag dit niet");
                        return true;
                    }
                    if (Session.GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect == 178)
                    {
                        Session.SendWhisper("Je bent al brb! Zeg :back om je weer terug te melden");
                        return true;
                    }
                    var kamerrr = Session.GetHabbo().CurrentRoom;
                    if (kamerrr == null)
                        return true;
                    var dudediewegis = kamerrr.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    Session.GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(178);
                    Session.SendWhisper("Succesvol brb gegaan. Zeg :back om je weer terug te melden");
                    dudediewegis.Chat(dudediewegis.GetClient(), "* Ik ben brb! *", true, 0, 1);
                    return true;
                }

                case "back":
                case "biw":
                case "bew":
                {
                    if (!Session.GetHabbo().GotCommand("back"))
                    {
                        Session.SendWhisper("Je mag dit niet");
                        return true;
                    }
                    var kamerrrr = Session.GetHabbo().CurrentRoom;
                    if (kamerrrr == null)
                        return true;
                    var benerweer = kamerrrr.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    if (Session.GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect == 178)
                    {
                        Session.GetHabbo().GetAvatarEffectsInventoryComponent().ActivateCustomEffect(0);
                        benerweer.Chat(benerweer.GetClient(), "* Ik ben er weer! *", true, 0, 1);
                        return true;
                    }
                    Session.SendWhisper("Je bent niet brb!");
                    return true;
                }
                case "staffbadge":
                case "sb":
                case "staffduty":
                {
                    if (!Session.GetHabbo().GotCommand("staffbadge"))
                        return false;
                    var closet = Session.GetHabbo().CurrentRoom;
                    if (closet == null)
                        return true;
                    var spjeweler = closet.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    Session.GetHabbo()
                        .GetAvatarEffectsInventoryComponent()
                        .ActivateCustomEffect((spjeweler.CurrentEffect != 102) ? 102 : 0);
                    return true;
                }
                case "buyvip":
                case "koopvip":
                {
                    if (Session.GetHabbo().VIP)
                    {
                        Session.SendNotif("You Are VIP!");
                        return true;
                    }
                    if (Session.GetHabbo().BelCredits < 100)
                    {
                        Session.SendNotif("You do not have enough diamonds (cost 100 diamonds)");
                        return true;
                    }
                    if (Session.GetHabbo().BelCredits <= 99)
                        return true;
                    using (var geefvip = Azure.GetDatabaseManager().GetQueryReactor())
                    {
                        geefvip.RunFastQuery("UPDATE `users` SET `vip`='1' WHERE `id`='" + Session.GetHabbo().Id +
                                             "'");
                        var vipper =
                            Azure.GetGame()
                                .GetRoomManager()
                                .GetRoom(Session.GetHabbo().CurrentRoomId)
                                .GetRoomUserManager()
                                .GetRoomUserByHabbo(Session.GetHabbo().UserName);
                        Session.GetHabbo()
                            .GetBadgeComponent()
                            .GiveBadge("VIP", false, vipper.GetClient(), false);
                        Session.GetHabbo().BelCredits += -100;
                        Session.GetHabbo().UpdateSeasonalCurrencyBalance();
                        Session.SendNotif(
                            "You have successfully bought VIP! <br><i> Go back to hotel in order to use your special commandos!");
                    }
                    return true;
                }
                case "giverank":
                {
                    if (!Session.GetHabbo().GotCommand("giverank"))
                    {
                        Session.SendWhisper("You Can't Give a Rank");
                        return true;
                    }
                    var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);

                    if (client == null)
                        return true;
                    int rankId;
                    if (!int.TryParse(pms[2], out rankId))
                    {
                        Session.SendNotif("Choose a number between 1 and 10, no 5/8!");
                        return true;
                    }
                    if (rankId == 8 || rankId == 5 || rankId < 1 || rankId > 10)
                    {
                        Session.SendNotif("Choose a number between 1 and 10, no 5/8!");
                        return true;
                    }
                    if (rankId == 2 || rankId == 3 || rankId == 4 || rankId == 6 ||
                        rankId == 7 || rankId == 9 || rankId == 10)
                        using (var geefderank = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            geefderank.RunFastQuery("UPDATE `users` SET `rank`=" + rankId + " WHERE `id`=" +
                                                    client.GetHabbo().Id);
                            client.SendNotif("Congratulations! Now you are Rank <b>" + rankId +
                                             "</b> And from the Staff");
                            Session.SendWhisper("Succesvol speler " + client.GetHabbo().UserName + " rank " +
                                                rankId + " gegeven.");
                        }
                    if (rankId != 1)
                        return true;
                    using (var geefderank = Azure.GetDatabaseManager().GetQueryReactor())
                    {
                        geefderank.RunFastQuery("UPDATE `users` SET `rank`=" + rankId + " WHERE `id`=" +
                                                client.GetHabbo().Id);
                        client.SendNotif(Session.GetHabbo().UserName + " you Are Fired from Staff.");
                        Session.SendWhisper("You Suceffully " + client.GetHabbo().UserName + " fired him.");
                        client.GetConnection().Dispose();
                    }
                    return true;
                }
                case "makesay":
                {
                    if (!Session.GetHabbo().GotCommand("makesay"))
                        return false;
                    if (pms.Length == 1)
                        return false;
                    var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                    if (room == null)
                        return true;
                    var user = room.GetRoomUserManager().GetRoomUserByHabbo(Convert.ToString(pms[1]));
                    if (user == null)
                        return true;
                    var msg = MergeParams(pms, 2);
                    if (string.IsNullOrEmpty(msg))
                        return false;
                    user.Chat(user.GetClient(), msg, false, 0, 0);
                    return true;
                }
                case "massbelcredits":
                case "massdiamonds":
                    if (!Session.GetHabbo().GotCommand("masscredits"))
                        return true;
                    if (pms.Length != 1)
                    {
                        try
                        {
                            var amount = int.Parse(pms[1]);
                            foreach (GameClient client in Azure.GetGame().GetClientManager().Clients.Values)
                            {
                                var habbo = client.GetHabbo();
                                habbo.BelCredits += amount;
                                client.GetHabbo().UpdateSeasonalCurrencyBalance();
                                client.SendNotif("You received <b>" + amount +
                                                 "</b> diamonds from the Hotel Management!");
                            }
                        }
                        catch
                        {
                        }
                        return true;
                    }
                    Session.SendNotif("Has to be a number.");
                    return true;
                case "deleteroom":
                {
                    if (!Session.GetHabbo().GotCommand("deleteroom"))
                    {
                        Session.SendWhisper("You are not allowed to do this");
                        return true;
                    }
                    var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                    if (!room.CheckRights(Session, true, false))
                        return true;
                    Session.GetHabbo().UsersRooms.Remove(room.RoomData);
                    Azure.GetGame().GetRoomManager().UnloadRoom(room, "Room deleted");
                    return true;
                }
                case "kill":
                {
                    if (!Session.GetHabbo().GotCommand("kill"))
                    {
                        Session.SendWhisper("You are not allowed to perform this command");
                        return true;
                    }
                    if (pms.Length <= 1)
                    {
                        SendChatMessage(Session, "Please fill in a user");
                        return true;
                    }
                    var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                    if (room == null)
                        return true;
                    var user2 = room.GetRoomUserManager()
                        .GetRoomUserByHabbo(Convert.ToString(pms[1]));
                    var user =
                        room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().UserName);
                    if (user2 != null)
                    {
                        if ((Math.Abs(user2.X - user.X) < 2) &&
                            (Math.Abs(user2.Y - user.Y) < 2))
                        {
                            if (user2.IsLyingDown == false && user2.IsSitting == false)
                            {
                                if (pms[1] != Session.GetHabbo().UserName)
                                {
                                    user2.Statusses.Add("lay", "0.55");
                                    user2.IsLyingDown = true;
                                    user2.UpdateNeeded = true;
                                    user.Chat(user.GetClient(), "* Here I, I stop! Muhahaha!!! *",
                                        true, 0, 3);
                                    user2.Chat(user2.GetClient(), "* Argh! I touched death! *", true, 0,
                                        3);
                                    return true;
                                }
                                var autist =
                                    Azure.GetGame()
                                        .GetRoomManager()
                                        .GetRoom(Session.GetHabbo().CurrentRoomId)
                                        .GetRoomUserManager()
                                        .GetRoomUserByHabbo(Session.GetHabbo().UserName);
                                autist.Chat(autist.GetClient(), "I am sad", false, 0, 0);
                                return true;
                            }
                            Session.SendWhisper("You can not kill someone who already lies/sits");
                            return true;
                        }
                        Session.SendWhisper("User is not besides you");
                        return true;
                    }
                    Session.SendWhisper("User could not be found");
                    return true;
                }
                case "givekiss":
                case "kus":
                {
                    if (!Session.GetHabbo().GotCommand("givekiss"))
                    {
                        Session.SendWhisper("Je mag dit niet");
                        return true;
                    }
                    if (pms.Length <= 1)
                    {
                        Session.SendWhisper("Please fill in a user");
                        return true;
                    }
                    if (Session.GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect != 12 ||
                        Session.GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect != 69)
                    {
                        var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                        if (room == null)
                            return true;
                        var user2 =
                            room.GetRoomUserManager().GetRoomUserByHabbo(Convert.ToString(pms[1]));
                        var user =
                            room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().UserName);
                        if (user2 != null)
                        {
                            if ((Math.Abs(user2.X - user.X) < 2) && (Math.Abs(user2.Y - user.Y) < 2) &&
                                Session.GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect != 178)
                            {
                                user2.Chat(user2.GetClient(),
                                    "* Gets kissed by " + Session.GetHabbo().UserName + " :$ *", false, 0, 16);
                                Session.GetHabbo()
                                    .GetAvatarEffectsInventoryComponent()
                                    .ActivateCustomEffect(9, true);
                                return true;
                            }
                            Session.SendWhisper("User is not besides you");
                            return true;
                        }
                        Session.SendWhisper("User could not be found");
                        return true;
                    }
                    Session.SendWhisper(
                        "You are frozen or driving a car, and therefor you may not perform this command");
                    return true;
                }
                case "boom":
                {
                    if (Session.GetHabbo().Rank <= 4)
                        return false;
                    foreach (var user in Session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUsers())
                    {
                        user.ApplyEffect(0x6c);
                        return true;
                    }
                    return true;
                }
                case "auto":
                case "car":
                {
                    if ((!Session.GetHabbo().VIP && !Session.GetHabbo().GotCommand("car")) ||
                        !Session.GetHabbo().VIP)
                    {
                        Session.SendWhisper("Only For VIP Users");
                        return true;
                    }

                    if (Session.GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect != 12)
                    {
                        var user = Azure.GetGame()
                            .GetRoomManager()
                            .GetRoom(Session.GetHabbo().CurrentRoomId)
                            .GetRoomUserManager()
                            .GetRoomUserByHabbo(Session.GetHabbo().Id);
                        if (Session.GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect != 69)
                        {
                            user.ApplyEffect(69);
                            user.FastWalking = true;
                            user.Chat(user.GetClient(), "* Start engine car. brum brum *", false, 0, 2);
                            return true;
                        }
                        if (Session.GetHabbo().GetAvatarEffectsInventoryComponent().CurrentEffect == 12)
                        {
                            Session.SendWhisper("This can not while you're frozen");
                            return true;
                        }
                        user.ApplyEffect(0);
                        user.FastWalking = false;
                        user.Chat(user.GetClient(), "* Turn engine car. *", false, 0, 2);
                        return true;
                    }
                    Session.SendWhisper("This can not while you're frozen");
                    return true;
                }
                case "disconnect":
                case "dc":
                {
                    if (!Session.GetHabbo().GotCommand("dc"))
                        return true;
                    var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                    if (client != null)
                    {
                        if ((client.GetHabbo().Rank >= Session.GetHabbo().Rank))
                        {
                            Session.SendNotif(Azure.GetLanguage().GetVar("ha_disconnect"));
                            return true;
                        }
                        Azure.GetGame()
                            .GetModerationTool()
                            .LogStaffEntry(Session.GetHabbo().UserName, client.GetHabbo().UserName, "Disconnect",
                                "User disconnected by user");
                        client.GetConnection().Dispose();
                        return true;
                    }
                    Session.SendNotif(Azure.GetLanguage().GetVar("ha_notfound"));
                    return true;
                }
                case "superha":
                case "supernotif":
                {
                    if (!Session.GetHabbo().GotCommand("superha"))
                        return false;
                    var notice = MergeParams(pms, 2);
                    var picture = MergeParams(pms, 1);
                    Azure.GetGame()
                        .GetClientManager()
                        .SendSuperNotif(Azure.GetLanguage().GetVar("ha_title"), notice, picture, Session, "", "", true,
                            false);
                    return true;
                }

                case "anonha":
                {
                    if (!Session.GetHabbo().GotCommand("anonha"))
                        return false;
                    var str = MergeParams(pms, 1);
                    var message = new ServerMessage(LibraryParser.OutgoingRequest("BroadcastNotifMessageComposer"));
                    message.AppendString(str);
                    Azure.GetGame().GetClientManager().QueueBroadcaseMessage(message);
                    return true;
                }
                case "eventha":
                {
                    if (!Session.GetHabbo().GotCommand("eventha"))
                        return false;
                    var str = MergeParams(pms, 1);
                    Azure.GetGame()
                        .GetClientManager()
                        .SendSuperNotif(Azure.GetLanguage().GetVar("alert_event_title"), str, "game_promo_small", Session,
                            string.Format("event:navigator/goto/{0}", Session.GetHabbo().CurrentRoom.RoomId),
                            Azure.GetLanguage().GetVar("alert_event_goRoom"), true, true);
                    return true;
                }
                case "dcroom":
                {
                    if (!Session.GetHabbo().GotCommand("dcroom"))
                        return false;
                    var connectionInformations =
                        (from user in Session.GetHabbo().CurrentRoom.GetRoomUserManager().UserList.Values
                            where
                                ((user != null) && (user.GetClient() != null)) &&
                                ((Session.GetHabbo().Id != user.GetClient().GetHabbo().Id) &&
                                 (user.GetClient().GetHabbo().Rank < Session.GetHabbo().Rank))
                            select user.GetClient().GetConnection()).ToList();
                    foreach (var something in connectionInformations)
                        something.Dispose();
                    connectionInformations = null;

                    return true;
                }

                case "dchotel":
                {
                    if (!Session.GetHabbo().GotCommand("dchotel"))
                        return false;
                    var connectionInformations =
                        (from GameClient client in Azure.GetGame().GetClientManager().Clients.Values
                            where client != null && client != Session
                            select client.GetConnection()).ToList();
                    foreach (var something in connectionInformations)
                        something.Dispose();
                    connectionInformations = null;
                    return true;
                }

                case "reloadall":
                    if (Session.GetHabbo().GotCommand("reloadall"))
                    {
                        var roomsToUnload =
                            Azure.GetGame().GetRoomManager().LoadedRooms.Values.Where(room => (room != null)).ToList();
                        foreach (var roomj in roomsToUnload)
                            Azure.GetGame().GetRoomManager().UnloadRoom(roomj, "Reloadall command");
                        roomsToUnload = null;
                    }
                    return false;

                case "coord":
                case "coords":
                case "position":
                {
                    if (!Session.GetHabbo().GotCommand("coords"))
                        return true; //436A;
                    var room = Session.GetHabbo().CurrentRoom;
                    if (room == null)
                        return true;
                    var user = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    if (user == null)
                        return true;
                    Session.SendNotif(
                        string.Concat(new object[]
                        {
                            "X: ", user.X, "\n - Y: ", user.Y, "\n - Z: ", user.Z, "\n - Rot: ",
                            user.RotBody, ", sqState: ",
                            room.GetGameMap().GameMap[user.X, user.Y].ToString(), "\n\n - RoomID: ",
                            Session.GetHabbo().CurrentRoomId
                        }));
                    return true; //436A;
                }
                case "teleport":
                case "tele":
                {
                    if (!Session.GetHabbo().GotCommand("tele"))
                        return true; //4419;
                    var room = Session.GetHabbo().CurrentRoom;
                    var user = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    if (user == null)
                        return true;
                    if (!user.RidingHorse)
                    {
                        user.TeleportEnabled = !user.TeleportEnabled;
                        room.GetGameMap().GenerateMaps(true);
                        return true; //4419;
                    }
                    SendChatMessage(Session, "You cannot teleport whilst riding a horse!");
                    return true;
                }
                case "update_youtube":
                case "refresh_youtube":
                    if (!Session.GetHabbo().GotCommand("refresh_youtube"))
                        return false;
                    Session.SendWhisper("Please wait, updating YouTube playlists...");
                    using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
                        Azure.GetGame().GetVideoManager().Load(adapter);
                    Session.SendWhisper("Done! YouTube playlists were reloaded.");
                    return true;
                case "reload_polls":
                case "refresh_polls":
                case "update_polls":
                    if (!Session.GetHabbo().GotCommand("refresh_polls"))
                        return false;
                    using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
                        Azure.GetGame().GetPollManager().Init(adapter);
                    return true;
                case "update_breeds":
                case "refresh_petbreeds":
                    if (!Session.GetHabbo().GotCommand("refresh_petbreeds"))
                        return false;
                    using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
                        PetRace.Init(adapter);
                    return true;
                case "update_publi":
                case "refresh_bannedhotels":
                    if (!Session.GetHabbo().GotCommand("refresh_bannedhotels"))
                        return false;
                    using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
                        BlockAds.Load(adapter);
                    return true;
                case "update_songs":
                case "refresh_songs":
                    if (!Session.GetHabbo().GotCommand("refresh_songs"))
                        return false;
                    SongManager.Initialize();
                    return true;
                case "update_achievements":
                case "refresh_achievements":
                    if (!Session.GetHabbo().GotCommand("refresh_achievements"))
                        return false;
                    Azure.GetGame()
                        .GetAchievementManager()
                        .LoadAchievements(Azure.GetDatabaseManager().GetQueryReactor());
                    return true;
                case "update_catalog":
                case "reload_catalog":
                case "recache_catalog":
                case "refresh_catalog":
                case "update_catalogue":
                case "reload_catalogue":
                case "recache_catalogue":
                case "refresh_catalogue":
                case "refreshcata":
                    if (Session.GetHabbo().GotCommand("refresh_catalogue"))
                    {
                        using (var adapter8 = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            FurniDataParser.SetCache();
                            Azure.GetGame().GetItemManager().LoadItems(adapter8);
                            Azure.GetGame().GetCatalog().Initialize(adapter8);
                            FurniDataParser.Clear();
                        }
                        Azure.GetGame()
                            .GetClientManager()
                            .QueueBroadcaseMessage(
                                new ServerMessage(LibraryParser.OutgoingRequest("PublishShopMessageComposer")));
                    }
                    return true;
                case "refresh_promos":
                    if (Session.GetHabbo().GotCommand("refresh_promos"))
                        Azure.GetGame().GetHotelView().RefreshPromoList();
                    return true;
                case "update_items":
                case "reload_items":
                case "recache_items":
                case "refresh_items":
                    if (Session.GetHabbo().GotCommand("refresh_items"))
                        using (var adapter9 = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            FurniDataParser.SetCache();
                            Azure.GetGame().GetItemManager().LoadItems(adapter9);
                            FurniDataParser.Clear();
                        }
                    return true;
                case "update_navigator":
                case "reload_navigator":
                case "recache_navigator":
                case "refresh_navigator":
                    if (Session.GetHabbo().GotCommand("refresh_navigator"))
                    {
                        using (var adapter11 = Azure.GetDatabaseManager().GetQueryReactor())
                            Azure.GetGame().GetNavigator().Initialize(adapter11);
                        Session.SendNotif("The navigator has been updated!");
                    }
                    return true;
                case "update_ranks":
                case "reload_ranks":
                case "recache_ranks":
                case "refresh_ranks":
                    if (Session.GetHabbo().GotCommand("refresh_ranks"))
                    {
                        using (var adapter12 = Azure.GetDatabaseManager().GetQueryReactor())
                            Azure.GetGame().GetRoleManager().LoadRights(adapter12);
                        Session.SendNotif("Ranks have been refreshed!");
                    }
                    return true;
                case "update_settings":
                case "reload_settings":
                case "recache_settings":
                case "refresh_settings":
                    if (Session.GetHabbo().GotCommand("refresh_settings"))
                        using (var adapter13 = Azure.GetDatabaseManager().GetQueryReactor())
                            Azure.ConfigData = new ConfigData(adapter13);
                    return true;
                case "update_groups":
                case "reload_groups":
                case "recache_groups":
                case "refresh_groups":
                    if (Session.GetHabbo().GotCommand("refresh_groups"))
                    {
                        Azure.GetGame().GetGroupManager().InitGroups();
                        Session.SendNotif("Groups have been successfully reloaded");
                    }
                    return true;
                case "update_bans":
                case "refresh_bans":
                    if (Session.GetHabbo().GotCommand("refresh_bans"))
                    {
                        using (var adapter14 = Azure.GetDatabaseManager().GetQueryReactor())
                            Azure.GetGame().GetBanManager().LoadBans(adapter14);
                        Session.SendNotif("Bans have been refreshed!");
                    }
                    return true;
                case "update_quests":
                case "refresh_quests":
                    if (Session.GetHabbo().GotCommand("refresh_quests"))
                    {
                        Azure.GetGame().GetQuestManager().Initialize(Azure.GetDatabaseManager().GetQueryReactor());
                        Session.SendNotif("Quests have been successfully reloaed!");
                    }
                    return true;
                case "spull":
                    if (Session.GetHabbo().HasFuse("fuse_vip_commands") || Session.GetHabbo().VIP)
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        if (room == null)
                            return true;
                        var user = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                        if (user == null)
                            return true;
                        if (pms.Length == 1)
                            return true;
                        var client2 = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                        if (client2 == null)
                            return true;
                        var user2 = room.GetRoomUserManager().GetRoomUserByHabbo(client2.GetHabbo().Id);
                        if (client2.GetHabbo().Id == Session.GetHabbo().Id)
                        {
                            SendChatMessage(Session, "\x00a1No puedes empujarte a ti mismo!");
                            return true;
                        }
                        if (user2.TeleportEnabled)
                            return true;
                        if ((user.RotBody % 2) != 0)
                            user.RotBody--;
                        switch (user.RotBody)
                        {
                            case 0:
                                user2.MoveTo(user.X, user.Y - 1);
                                break;
                            case 2:
                                user2.MoveTo(user.X + 1, user.Y);
                                break;
                            case 4:
                                user2.MoveTo(user.X, user.Y + 1);
                                break;
                            case 6:
                                user2.MoveTo(user.X - 1, user.Y);
                                break;
                        }
                    }
                    return true;
                case "globalcredits":
                    if (!Session.GetHabbo().GotCommand("globalcredits"))
                        return true;
                    if (pms.Length != 1)
                    {
                        try
                        {
                            var amount = int.Parse(pms[1]);
                            using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
                                adapter.RunFastQuery(string.Format("UPDATE users SET credits=credits+{0}", amount));
                            foreach (
                                var client in
                                    Azure.GetGame()
                                        .GetClientManager()
                                        .Clients.Values.Cast<GameClient>()
                                        .Where(client => client.GetHabbo() != null))
                            {
                                client.GetHabbo().Credits += amount;
                                client.GetHabbo().UpdateCreditsBalance();
                            }
                        }
                        catch
                        {
                        }
                        return true;
                    }
                    Session.SendNotif("You need to enter an amount!");
                    return true;
                case "masscredits":
                    if (!Session.GetHabbo().GotCommand("masscredits"))
                        return true;
                    if (pms.Length != 1)
                    {
                        try
                        {
                            var amount = int.Parse(pms[1]);
                            foreach (
                                var client in
                                    Azure.GetGame()
                                        .GetClientManager()
                                        .Clients.Values.Cast<GameClient>()
                                        .Where(client => client.GetHabbo() != null))
                            {
                                client.GetHabbo().Credits += amount;
                                client.GetHabbo().UpdateCreditsBalance();
                            }
                        }
                        catch
                        {
                        }
                        return true; //4A9D;
                    }
                    Session.SendNotif("You need to enter an amount!");
                    return true;
                case "placa":
                case "darplaca":
                case "badge":
                case "givebadge":
                    if (Session.GetHabbo().GotCommand("givebadge"))
                    {
                        if (pms.Length == 3)
                        {
                            GameClient client = null;
                            client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                            if (client != null)
                            {
                                client.GetHabbo().GetBadgeComponent().GiveBadge(pms[2], true, client, false);
                                Azure.GetGame()
                                    .GetModerationTool()
                                    .LogStaffEntry(Session.GetHabbo().UserName, client.GetHabbo().UserName,
                                        "Badge", string.Format("Badge given to user [{0}]", pms[2]));
                                return true;
                            }
                            Session.SendNotif("User no se encontr\x00f3.");
                            return true;
                        }
                        Session.SendNotif("\x00a1Incluye c\x00f3digo de placa y User!");
                    }
                    return true;
                case "quitarplaca":
                case "takebadge":
                case "removebadge":
                    if (Session.GetHabbo().GotCommand("takebadge"))
                    {
                        if (pms.Length == 3)
                        {
                            var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                            if (client != null)
                            {
                                if (client.GetHabbo().GetBadgeComponent().HasBadge(pms[2]))
                                {
                                    client.GetHabbo().GetBadgeComponent().RemoveBadge(pms[2], client);
                                    Azure.GetGame()
                                        .GetModerationTool()
                                        .LogStaffEntry(Session.GetHabbo().UserName, client.GetHabbo().UserName,
                                            "Badge Taken", string.Format("Badge taken from user [{0}]", pms[2]));
                                    return true;
                                }
                                Session.SendNotif("El User no tiene esa placa.");
                                return true;
                            }
                            Session.SendNotif("User no se encontr\x00f3.");
                            return true;
                        }
                        Session.SendNotif("\x00a1Incluye c\x00f3digo de placa y User!");
                    }
                    return true;
                case "roombadge":
                    if (!Session.GetHabbo().GotCommand("roombadge"))
                        return true;
                    if (pms.Length != 1)
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        if (Session.GetHabbo().CurrentRoom == null)
                            return true;
                        foreach (var current in room.GetRoomUserManager().UserList.Values)
                            try
                            {
                                if (!current.IsBot && current.GetClient() != null &&
                                    current.GetClient().GetHabbo() != null)
                                    current.GetClient()
                                        .GetHabbo()
                                        .GetBadgeComponent()
                                        .GiveBadge(pms[1], true, current.GetClient(), false);
                            }
                            catch
                            {
                            }
                        Azure.GetGame()
                            .GetModerationTool()
                            .LogStaffEntry(Session.GetHabbo().UserName, string.Empty, "Badge",
                                string.Concat(new object[]
                                {"Roombadge in room [", room.RoomId, "] with badge [", pms[1], "]"}));
                        return true;
                    }
                    Session.SendNotif("You must enter a badge code!");
                    return true;
                case "massbadge":
                    if (!Session.GetHabbo().GotCommand("massbadge"))
                        return true;
                    if (pms.Length != 1)
                    {
                        Azure.GetGame().GetClientManager().QueueBadgeUpdate(pms[1]);
                        Azure.GetGame()
                            .GetModerationTool()
                            .LogStaffEntry(Session.GetHabbo().UserName, string.Empty, "Badge",
                                string.Format("Mass badge with badge [{0}]", pms[1]));
                        return true;
                    }
                    Session.SendNotif("You must enter a badge code!");
                    return true;
                case "alleyesonme":
                {
                    if (!Session.GetHabbo().GotCommand("alleyesonme"))
                        return false;
                    if (Session.GetHabbo().CurrentRoom == null)
                        return true;
                    var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                    var user = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                    foreach (
                        var roomUser in
                            room.GetRoomUserManager()
                                .GetRoomUsers()
                                .Where(roomUser => Session.GetHabbo().Id != roomUser.UserId))
                        roomUser.SetRot(PathFinder.CalculateRotation(roomUser.X, roomUser.Y, user.X, user.Y));
                    return true;
                }
                case "ipban":
                case "banip":
                    if (Session.GetHabbo().GotCommand("ipban"))
                    {
                        if (pms.Length == 1)
                            Session.SendNotif("You must include a Username and reason!");
                        var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                        if (client == null)
                        {
                            Session.SendNotif("An unknown error occured whilst finding this user!");
                            return true;
                        }
                        try
                        {
                            Azure.GetGame()
                                .GetBanManager()
                                .BanUser(client, Session.GetHabbo().UserName, 788922000.0, MergeParams(pms, 2),
                                    true, false);
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine(exception);
                        }
                    }
                    return true;

                case "machineban":
                case "banmachine":
                case "mban":
                    if (Session.GetHabbo().GotCommand("machineban"))
                    {
                        if (pms.Length == 1)
                            Session.SendNotif("You must include a Username and reason!");
                        var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                        if (client == null)
                        {
                            Session.SendNotif("An unknown error occured whilst finding this user!");
                            return true;
                        }
                        if (string.IsNullOrWhiteSpace(client.MachineId))
                        {
                            Session.SendNotif("Unable to ban this user, they don't have a machine ID");
                            return true;
                        }
                        try
                        {
                            Azure.GetGame()
                                .GetBanManager()
                                .BanUser(client, Session.GetHabbo().UserName, 2678400.0,
                                    string.Format("You have been banned! The reason given was:\n{0}",
                                        MergeParams(pms, 2)), false, true);
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine(exception);
                        }
                    }
                    return true;

                case "mip":
                    if (Session.GetHabbo().GotCommand("mip"))
                    {
                        if (pms.Length == 1)
                            Session.SendNotif("You must include a Username and reason!");
                        var client = Azure.GetGame().GetClientManager().GetClientByUserName(pms[1]);
                        if (client == null)
                        {
                            Session.SendNotif("An unknown error occured whilst finding this user!");
                            return true;
                        }
                        try
                        {
                            if (string.IsNullOrWhiteSpace(client.MachineId))
                            {
                                Session.SendNotif("Unable to ban this user, they don't have a machine ID");
                                return true;
                            }
                            Azure.GetGame()
                                .GetBanManager()
                                .BanUser(client, Session.GetHabbo().UserName, 2678400.0,
                                    string.Format("You have been banned! The reason given was:\n{0}",
                                        MergeParams(pms, 2)), false, true);
                            Azure.GetGame()
                                .GetBanManager()
                                .BanUser(client, Session.GetHabbo().UserName, 788922000.0, MergeParams(pms, 2),
                                    true, false);
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine(exception);
                        }
                    }
                    return true;

                case "allaroundme":
                {
                    if (!Session.GetHabbo().GotCommand("allaroundme"))
                        return true;
                    if (Session.GetHabbo().CurrentRoom != null)
                    {
                        var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                        var user = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                        var list = room.GetRoomUserManager().GetRoomUsers();
                        foreach (var roomUser in list.Where(roomUser => Session.GetHabbo().Id != roomUser.UserId))
                            roomUser.MoveTo(user.X, user.Y, true);
                        if (pms.Length != 2 || pms[1] != "override")
                            return true;
                        foreach (var roomUser in list.Where(roomUser => Session.GetHabbo().Id != roomUser.UserId))
                        {
                            roomUser.AllowOverride = true;
                            roomUser.MoveTo(user.X, user.Y, true);
                            roomUser.AllowOverride = false;
                        }
                        return true;
                    }
                    Session.SendNotif("An unknown error occured!");
                    return true;
                }
                case "sayall":
                    if (Session.GetHabbo().GotCommand("sayall"))
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        if (room == null)
                            return true;
                        var str = MergeParams(pms, 1);
                        if (str == "")
                            return true;
                        foreach (var user in room.GetRoomUserManager().GetRoomUsers())
                            user.Chat(user.GetClient(), str, false, 0, 0);
                    }
                    return true;

                case "hal":
                    if (Session.GetHabbo().GotCommand("hal"))
                    {
                        var url = pms[1];
                        var msg = MergeParams(pms, 2);
                        var message =
                            new ServerMessage(LibraryParser.OutgoingRequest("AlertNotificationMessageComposer"));
                        message.AppendString(string.Format("{0}\r\n-{1}", msg, Session.GetHabbo().UserName));
                        message.AppendString(url);
                        Azure.GetGame().GetClientManager().QueueBroadcaseMessage(message);
                        Azure.GetGame()
                            .GetModerationTool()
                            .LogStaffEntry(Session.GetHabbo().UserName, string.Empty, "HotelAlert",
                                string.Format("Hotel alert [{0}]", msg));
                    }
                    return true;

                case "sa":
                case "sm":
                    if (Session.GetHabbo().GotCommand("sa"))
                    {
                        var msg = MergeParams(pms, 1);
                        var message =
                            new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
                        message.AppendString("staffcloud");
                        message.AppendInteger(2);
                        message.AppendString("title");
                        message.AppendString("Mensaje entre Staff");
                        message.AppendString("message");
                        message.AppendString(
                            string.Format(
                                "<b><font color=\"#6E09A0\" size=\"14\">Mensaje entre Staffs:<)/font></b>\r\n{0}\r\n- <i>{1}</i>",
                                msg, Session.GetHabbo().UserName));
                        Azure.GetGame().GetClientManager().StaffAlert(message, 0);
                        Azure.GetGame()
                            .GetModerationTool()
                            .LogStaffEntry(Session.GetHabbo().UserName, string.Empty, "StaffAlert",
                                string.Format("Staff alert [{0}]", msg));
                    }
                    return true;

                case "invisible":
                case "spec":
                case "spectatorsmode":
                    if (Session.GetHabbo().GotCommand("invisible"))
                        if (Session.GetHabbo().SpectatorMode)
                        {
                            Session.GetHabbo().SpectatorMode = false;
                            Session.SendNotif("You are not invisible anymore.");
                        }
                        else
                        {
                            Session.GetHabbo().SpectatorMode = true;
                            Session.SendNotif("Reload the room to be invisible");
                        }
                    return true;

                case "makepublic":
                {
                    if (!Session.GetHabbo().GotCommand("makepublic"))
                        return true;
                    var room = Session.GetHabbo().CurrentRoom;
                    if (room == null)
                        return true;
                    using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
                        adapter.RunFastQuery(
                            string.Format("UPDATE rooms_data SET roomtype='public' WHERE id={0} LIMIT 1",
                                room.RoomId));
                    room.Type = "public";
                    room.RoomData.Type = "public";
                    room.RoomData.SerializeRoomData(new ServerMessage(), false, Session, true);
                    return true;
                }
                case "makeprivate":
                {
                    if (!Session.GetHabbo().GotCommand("makeprivate"))
                        return true;
                    var room = Session.GetHabbo().CurrentRoom;
                    if (room == null)
                        return true;
                    using (var adapter = Azure.GetDatabaseManager().GetQueryReactor())
                        adapter.RunFastQuery(
                            string.Format("UPDATE rooms_data SET roomtype='private' WHERE id={0} LIMIT 1",
                                room.RoomId));
                    room.Type = "private";
                    room.RoomData.Type = "private";
                    room.RoomData.SerializeRoomData(new ServerMessage(), false, Session, true);
                    return true;
                }

                case "roomaction":
                {
                    if (!Session.GetHabbo().GotCommand("roomaction"))
                        return true;
                    try
                    {
                        var room = Session.GetHabbo().CurrentRoom;
                        var list = room.GetRoomUserManager().GetRoomUsers();
                        int action = short.Parse(pms[1]);
                        foreach (var user in list)
                            if (user != null)
                            {
                                var actionMsg = new ServerMessage();
                                actionMsg.Init(LibraryParser.OutgoingRequest("RoomUserActionMessageComposer"));
                                actionMsg.AppendInteger(user.VirtualId);
                                actionMsg.AppendInteger(action);
                                room.SendMessage(actionMsg);
                            }
                    }
                    catch
                    {
                    }
                    return true;
                }
                case "startquestion":
                {
                    if (!Session.GetHabbo().GotCommand("startquestion"))
                        return true;
                    var id = uint.Parse(pms[1]);
                    var poll = Azure.GetGame().GetPollManager().TryGetPollById(id);
                    if (poll == null || poll.Type != Poll.PollType.Matching)
                    {
                        Session.SendWhisper("Poll doesn't exists or isn't a matching poll.");
                        return true;
                    }
                    var message = new ServerMessage(LibraryParser.OutgoingRequest("MatchingPollMessageComposer"));
                    message.AppendString("MATCHING_POLL");
                    message.AppendInteger(poll.Id); //poll id
                    message.AppendInteger(poll.Id); //question_id
                    message.AppendInteger(15580);
                    message.AppendInteger(poll.Id); //question_id
                    message.AppendInteger(29); //number
                    message.AppendInteger(5); //type
                    message.AppendString(poll.PollName);
                    Session.GetHabbo().CurrentRoom.SendMessage(message);
                    Thread ShowPoll = new Thread(delegate () { MatchingPollResults(Session.GetHabbo().CurrentRoom, poll); });
                    ShowPoll.Start();
                    return true;
                }
                case "offer":
                    {
                        if (Session.GetHabbo().Rank < 6) return true;
                        var offer = new ServerMessage(LibraryParser.OutgoingRequest("TargetedOfferDataMessageComposer"));
                        offer.AppendInteger(4);//?
                        offer.AppendInteger(1);//Id
                        offer.AppendString("ufo_ny2015_offer");//??
                        offer.AppendString("ufo_ny2015_offer");//identifier
                        offer.AppendInteger(10);//price(credits)
                        offer.AppendInteger(10);//price(activity)
                        offer.AppendInteger(5);//price(activity type)
                        offer.AppendInteger(259009);//expiration time
                        offer.AppendString("targeted.offer.ufo_ny2015_offer.title");//title
                        offer.AppendString("targeted.offer.ufo_ny2015_offer.desc");//desc
                        offer.AppendInteger(2);//items count
                        offer.AppendString("throne");
                        offer.AppendString("HABBO_CLUB_VIP_1_MONTH");
                        Session.SendMessage(offer);
                        return true;
                    }
            }
            return false;
        }
        public static void MatchingPollResults(Room room,Poll poll)
        {
            if (poll == null || poll.Type != Poll.PollType.Matching || room == null)
                return;
            Thread.Sleep(10000);//10 secondes to vote
            ServerMessage Result = new ServerMessage(LibraryParser.OutgoingRequest("MatchingPollResultMessageComposer"));
            Result.AppendInteger(poll.Id);//question_id
            Result.AppendInteger(2);//while
            Result.AppendString("0");
            Result.AppendInteger(poll.answersNegative);
            Result.AppendString("1");
            Result.AppendInteger(poll.answersPositive);
            room.SendMessage(Result);
        }
    }
}