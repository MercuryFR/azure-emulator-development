using System;
using System.Collections.Generic;
using System.Drawing;
using Azure.HabboHotel.Rooms;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Pets
{
    class Pet
    {
        internal uint PetId;
        internal uint OwnerId;
        internal int VirtualId;
        internal uint Type;
        internal string Name;
        internal string Race;
        internal string Color;
        internal int HairDye;
        internal int PetHair;
        internal int Experience;
        internal int Energy;
        internal int Nutrition;
        internal uint RoomId;
        internal int X;
        internal int Y;
        internal double Z;
        internal int Respect;
        internal int Rarity;
        internal double CreationStamp;
        internal bool PlacedInRoom;
        internal DateTime LastHealth;
        internal DateTime UntilGrown;
        internal uint WaitingForBreading;
        internal Point BreadingTile;
        internal MoplaBreed MoplaBreed;
        internal Dictionary<short, bool> PetCommands;

        internal int[] ExperienceLevels =
        {
            100,
            200,
            400,
            600,
            1000,
            1300,
            1800,
            2400,
            3200,
            4300,
            7200,
            8500,
            10100,
            13300,
            17500,
            23000,
            51900
        };

        internal DatabaseUpdateState DbState;
        internal bool HaveSaddle;
        internal int AnyoneCanRide;

        internal Pet(uint petId, uint ownerId, uint roomId, string name, uint type, string race, string color,
            int experience, int energy, int nutrition, int respect, double creationStamp, int x, int y, double z,
            bool havesaddle, int anyoneCanRide, int dye, int petHer, int rarity, DateTime lastHealth,
            DateTime untilGrown, MoplaBreed moplaBreed)
        {
            PetId = petId;
            OwnerId = ownerId;
            RoomId = roomId;
            Name = name;
            Type = type;
            Race = race;
            Color = color;
            Experience = experience;
            Energy = energy;
            Nutrition = nutrition;
            Respect = respect;
            CreationStamp = creationStamp;
            X = x;
            Y = y;
            Z = z;
            PlacedInRoom = false;
            DbState = DatabaseUpdateState.Updated;
            HaveSaddle = havesaddle;
            AnyoneCanRide = anyoneCanRide;
            PetHair = petHer;
            HairDye = dye;
            Rarity = rarity;
            LastHealth = lastHealth;
            UntilGrown = untilGrown;
            MoplaBreed = moplaBreed;
            PetCommands = PetCommandHandler.GetPetCommands(this);
            WaitingForBreading = 0;
        }

        internal static int MaxLevel
        {
            get { return 20; }
        }

        internal static int MaxEnergy
        {
            get { return 100; }
        }

        internal static int MaxNutrition
        {
            get { return 150; }
        }

        internal Room Room
        {
            get { return !IsInRoom ? null : Azure.GetGame().GetRoomManager().GetRoom(RoomId); }
        }

        internal bool IsInRoom
        {
            get { return RoomId > 0u; }
        }

        internal int Level
        {
            get
            {
                checked
                {
                    for (var i = 0; i < ExperienceLevels.Length; i++)
                        if (Experience < ExperienceLevels[i])
                            return i + 1;
                    return ExperienceLevels.Length + 1;
                }
            }
        }

        internal int ExperienceGoal
        {
            get { return ExperienceLevels[checked(Level - 1)]; }
        }

        internal int Age
        {
            get
            {
                var creation = Azure.UnixToDateTime(CreationStamp);
                return (int) (DateTime.Now - creation).TotalDays;
            }
        }

        internal string Look
        {
            get
            {
                return string.Concat(new object[]
                {
                    Type,
                    " ",
                    Race,
                    " ",
                    Color
                });
            }
        }

        internal string OwnerName
        {
            get { return Azure.GetGame().GetClientManager().GetNameById(OwnerId); }
        }

        internal bool HasCommand(short command)
        {
            return PetCommands.ContainsKey(command) && PetCommands[command];
        }

        internal void OnRespect()
        {
            checked
            {
                Respect++;
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("RespectPetMessageComposer"));
                serverMessage.AppendInteger(VirtualId);
                serverMessage.AppendBool(true);
                Room.SendMessage(serverMessage);

                serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("PetRespectNotificationMessageComposer"));
                serverMessage.AppendInteger(1);
                serverMessage.AppendInteger(VirtualId);
                SerializeInventory(serverMessage);
                Room.SendMessage(serverMessage);

                if (DbState != DatabaseUpdateState.NeedsInsert)
                    DbState = DatabaseUpdateState.NeedsUpdate;
                if (Type != 16 && Experience <= 51900)
                    AddExperience(10);
                if (Type == 16)
                    Energy = 100;
                LastHealth = DateTime.Now.AddSeconds(129600.0);
            }
        }

        internal void AddExperience(int amount)
        {
            checked
            {
                var oldExperienceGoal = ExperienceGoal;
                Experience += amount;
                if (Experience >= 51900)
                    return;
                if (DbState != DatabaseUpdateState.NeedsInsert)
                    DbState = DatabaseUpdateState.NeedsUpdate;
                if (Room == null)
                    return;
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("AddPetExperienceMessageComposer"));
                serverMessage.AppendInteger(PetId);
                serverMessage.AppendInteger(VirtualId);
                serverMessage.AppendInteger(amount);
                Room.SendMessage(serverMessage);
                if (Experience < oldExperienceGoal)
                    return;
                var ownerSession = Azure.GetGame().GetClientManager().GetClientByUserId(OwnerId);

                // Reset pet commands
                PetCommands.Clear();
                PetCommands = PetCommandHandler.GetPetCommands(this);

                if (ownerSession == null)
                    return;
                Console.WriteLine("new level notified##############");
                var levelNotify = new ServerMessage(LibraryParser.OutgoingRequest("NotifyNewPetLevelMessageComposer"));
                SerializeInventory(levelNotify, true);
                ownerSession.SendMessage(levelNotify);

                var tp = new ServerMessage();
                tp.Init(LibraryParser.OutgoingRequest("PetTrainerPanelMessageComposer"));
                tp.AppendInteger(PetId);

                var availableCommands = new List<short>();

                tp.AppendInteger(PetCommands.Count);
                foreach (short sh in PetCommands.Keys)
                {
                    tp.AppendInteger(sh);
                    if (PetCommands[sh])
                        availableCommands.Add(sh);
                }

                tp.AppendInteger(availableCommands.Count);
                foreach (short sh in availableCommands)
                    tp.AppendInteger(sh);
                ownerSession.SendMessage(tp);
            }
        }

        internal void PetEnergy(bool add)
        {
            checked
            {
                int num;
                if (add)
                {
                    if (Energy > 100)
                    {
                        Energy = 100;
                        return;
                    }
                    if (Energy > 85)
                        return;
                    if (Energy > 85)
                        num = MaxEnergy - Energy;
                    else
                        num = 10;
                }
                else
                    num = 15;
                if (num <= 4)
                    num = 15;
                var randomNumber = Azure.GetRandomNumber(4, num);
                if (!add)
                {
                    Energy -= randomNumber;
                    if (Energy < 0)
                        Energy = 1;
                }
                else
                    Energy += randomNumber;
                if (DbState != DatabaseUpdateState.NeedsInsert)
                    DbState = DatabaseUpdateState.NeedsUpdate;
            }
        }

        internal void SerializeInventory(ServerMessage message, bool levelAfterName = false)
        {
            message.AppendInteger(PetId);
            message.AppendString(Name);
            if (levelAfterName)
                message.AppendInteger(Level);
            message.AppendInteger(Type);
            message.AppendInteger(int.Parse(Race));
            message.AppendString((Type == 16u) ? "ffffff" : Color);
            message.AppendInteger((Type == 16u) ? 0u : Type);
            if (Type == 16u && MoplaBreed != null)
            {
                var array = MoplaBreed.PlantData.Substring(12).Split(new[]
                {
                    ' '
                });
                var array2 = array;
                foreach (string s in array2)
                    message.AppendInteger(int.Parse(s));
                message.AppendInteger(MoplaBreed.GrowingStatus);
                return;
            }
            message.AppendInteger(0);
            message.AppendInteger(0);
        }

        internal void ManageGestures() { }

        internal ServerMessage SerializeInfo()
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("PetInfoMessageComposer"));
            serverMessage.AppendInteger(PetId);
            serverMessage.AppendString(Name);
            if (Type == 16)
            {
                serverMessage.AppendInteger(MoplaBreed.GrowingStatus);
                serverMessage.AppendInteger(7);
            }
            else
            {
                serverMessage.AppendInteger(Level);
                serverMessage.AppendInteger(MaxLevel);
            }
            serverMessage.AppendInteger(Experience);
            serverMessage.AppendInteger(ExperienceGoal);
            serverMessage.AppendInteger(Energy);
            serverMessage.AppendInteger(MaxEnergy);
            serverMessage.AppendInteger(Nutrition);
            serverMessage.AppendInteger(MaxNutrition);
            serverMessage.AppendInteger(Respect);
            serverMessage.AppendInteger(OwnerId);
            serverMessage.AppendInteger(Age);
            serverMessage.AppendString(OwnerName);
            serverMessage.AppendInteger((Type == 16) ? 0 : 1);
            serverMessage.AppendBool(HaveSaddle);
            serverMessage.AppendBool(
                Azure.GetGame()
                    .GetRoomManager()
                    .GetRoom(RoomId)
                    .GetRoomUserManager()
                    .GetRoomUserByVirtualId(VirtualId)
                    .RidingHorse);
            serverMessage.AppendInteger(0);
            serverMessage.AppendInteger(AnyoneCanRide);
            if (Type == 16)
                serverMessage.AppendBool(MoplaBreed.LiveState == MoplaState.Grown);
            else
                serverMessage.AppendBool(false);
            serverMessage.AppendBool(false);
            if (Type == 16)
                serverMessage.AppendBool(MoplaBreed.LiveState == MoplaState.Dead);
            else
                serverMessage.AppendBool(false);
            serverMessage.AppendInteger(Rarity);
            checked
            {
                if (Type == 16u)
                {
                    serverMessage.AppendInteger(129600);
                    var lastHealthSeconds = (int) (LastHealth - DateTime.Now).TotalSeconds;
                    var untilGrownSeconds = (int) (UntilGrown - DateTime.Now).TotalSeconds;

                    if (lastHealthSeconds < 0)
                        lastHealthSeconds = 0;
                    if (untilGrownSeconds < 0)
                        untilGrownSeconds = 0;

                    switch (MoplaBreed.LiveState)
                    {
                        case MoplaState.Dead:
                            serverMessage.AppendInteger(0);
                            serverMessage.AppendInteger(0);
                            break;
                        case MoplaState.Grown:
                            serverMessage.AppendInteger(lastHealthSeconds);
                            serverMessage.AppendInteger(0);
                            break;
                        default:
                            serverMessage.AppendInteger(lastHealthSeconds);
                            serverMessage.AppendInteger(untilGrownSeconds);
                            break;
                    }
                }
                else
                {
                    serverMessage.AppendInteger(-1);
                    serverMessage.AppendInteger(-1);
                    serverMessage.AppendInteger(-1);
                }

                serverMessage.AppendBool(true); // Allow breed?
                return serverMessage;
            }
        }
    }
}