using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.HabboHotel.Pets
{
    public class PetRace
    {
        public static List<PetRace> Races;

        public int RaceId;
        public int Color1;
        public int Color2;
        public bool Has1Color;
        public bool Has2Color;

        public static void Init(IQueryAdapter dbClient)
        {
            dbClient.SetQuery("SELECT * FROM pets_breeds");
            var table = dbClient.GetTable();
            Races = new List<PetRace>();
            foreach (var item in from DataRow row in table.Rows select new PetRace
            {
                RaceId = (int) row["breed_id"],
                Color1 = (int) row["color1"],
                Color2 = (int) row["color2"],
                Has1Color = ((string) row["color1_enabled"]) == "1",
                Has2Color = ((string) row["color2_enabled"]) == "1"
            })
            {
                Races.Add(item);
            }
        }

        public static List<PetRace> GetRacesForRaceId(int sRaceId)
        {
            return Races.Where(current => current.RaceId == sRaceId).ToList();
        }

        public static bool RaceGotRaces(int sRaceId) { return GetRacesForRaceId(sRaceId).Any(); }

        public static int GetPetId(string type, out string packet)
        {
            var result = Convert.ToInt32(type.Replace("a0 pet", ""));
            packet = type;

            if (result >= 0 && result <= 26)
                return result;
            packet = "a0 pet0";
            return 0;
        }
    }
}