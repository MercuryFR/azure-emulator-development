﻿namespace Azure.HabboHotel.Pets
{
    struct PetCommand
    {
        internal readonly int CommandId;
        internal readonly string CommandInput;
        internal readonly int MinRank;

        public PetCommand(int commandId, string commandInput, int minRank = 0)
        {
            CommandId = commandId;
            CommandInput = commandInput;
            MinRank = minRank;
        }
    }
}