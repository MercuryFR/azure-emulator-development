namespace Azure.HabboHotel.Pets
{
    enum MoplaState
    {
        Alive = 0,
        Grown = 1,
        Dead = 2
    }
}