namespace Azure.HabboHotel.Pets
{
    enum DatabaseUpdateState
    {
        Updated,
        NeedsUpdate,
        NeedsInsert
    }
}