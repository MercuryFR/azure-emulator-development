﻿using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.Pets
{
    class PetBreeding
    {
        internal static int[] TerrierEpicRace = {17, 18, 19};
        internal static int[] TerrierRareRace = {10, 14, 15, 16};
        internal static int[] TerrierNoRareRace = {11, 12, 13, 4, 5, 6};
        internal static int[] TerrierNormalRace = {0, 1, 2, 7, 8, 9};

        internal static int[] BearEpicRace = {3, 10, 11};
        internal static int[] BearRareRace = {12, 13, 15, 16, 17, 18};
        internal static int[] BearNoRareRace = {7, 8, 9, 14, 19};
        internal static int[] BearNormalRace = {0, 1, 2, 4, 5, 6};

        internal static ServerMessage GetMessage(uint furniId, Pet pet1, Pet pet2)
        {
            var message = new ServerMessage(LibraryParser.OutgoingRequest("PetBreedMessageComposer"));
            message.AppendInteger(furniId);
            message.AppendInteger(pet1.PetId);
            message.AppendString(pet1.Name);
            message.AppendInteger(pet1.Level);
            message.AppendString(pet1.Look);
            message.AppendString(pet1.OwnerName);
            message.AppendInteger(pet2.PetId);
            message.AppendString(pet2.Name);
            message.AppendInteger(pet2.Level);
            message.AppendString(pet2.Look);
            message.AppendString(pet2.OwnerName);
            message.AppendInteger(4); // 4 razas (ÉPICO, RARO, NADA COMÚN, COMÚN)

            message.AppendInteger(1);
            switch (pet1.Type)
            {
                case 3:
                    message.AppendInteger(TerrierEpicRace.Length);
                    foreach (int value in TerrierEpicRace)
                        message.AppendInteger(value);
                    break;
                case 4:
                    message.AppendInteger(BearEpicRace.Length);
                    foreach (int value in BearEpicRace)
                        message.AppendInteger(value);
                    break;
            }

            message.AppendInteger(2);
            switch (pet1.Type)
            {
                case 3:
                    message.AppendInteger(TerrierRareRace.Length);
                    foreach (int value in TerrierRareRace)
                        message.AppendInteger(value);
                    break;
                case 4:
                    message.AppendInteger(BearRareRace.Length);
                    foreach (int value in BearRareRace)
                        message.AppendInteger(value);
                    break;
            }

            message.AppendInteger(3);
            switch (pet1.Type)
            {
                case 3:
                    message.AppendInteger(TerrierNoRareRace.Length);
                    foreach (int value in TerrierNoRareRace)
                        message.AppendInteger(value);
                    break;
                case 4:
                    message.AppendInteger(BearNoRareRace.Length);
                    foreach (int value in BearNoRareRace)
                        message.AppendInteger(value);
                    break;
            }

            message.AppendInteger(94);
            switch (pet1.Type)
            {
                case 3:
                    message.AppendInteger(TerrierNormalRace.Length);
                    foreach (int value in TerrierNormalRace)
                        message.AppendInteger(value);
                    break;
                case 4:
                    message.AppendInteger(BearNormalRace.Length);
                    foreach (int value in BearNormalRace)
                        message.AppendInteger(value);
                    break;
            }

            message.AppendInteger((pet1.Type == 3) ? 25 : 24);

            return message;
        }
    }
}