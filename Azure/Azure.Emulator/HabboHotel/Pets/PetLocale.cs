using System.Collections.Generic;
using System.Data;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.HabboHotel.Pets
{
    class PetLocale
    {
        private static Dictionary<string, string[]> _values;

        internal static void Init(IQueryAdapter dbClient)
        {
            dbClient.SetQuery("SELECT * FROM pets_speech");
            var table = dbClient.GetTable();
            _values = new Dictionary<string, string[]>();
            foreach (DataRow dataRow in table.Rows)
                _values.Add(dataRow[0].ToString(), dataRow[1].ToString().Split(';'));
        }

        internal static string[] GetValue(string key)
        {
            string[] result;
            return _values.TryGetValue(key, out result)
                ? result
                : new[]
                {
                    key
                };
        }
    }
}