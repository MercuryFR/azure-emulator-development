using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Net;
using System.Xml;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Rooms;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.YouTube
{
    internal class VideoManager
    {
        private readonly Dictionary<string, PlayList> _playLists;
        private readonly HybridDictionary _loadedTVs;

        private PlayList _defaultPlayList;

        internal VideoManager()
        {
            this._playLists = new Dictionary<string, PlayList>();
            this._loadedTVs = new HybridDictionary();
        }

        public bool TVExists(uint itemId)
        {
            return this._loadedTVs.Contains(itemId);
        }

        internal static async void GetVideosForPl(PlayList playList)
        {
            if (playList.Id == -1)
            {
                return;
            }
            var wC = new WebClient();
            try
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.Load(
                    await wC.DownloadStringTaskAsync(string.Format("http://gdata.youtube.com/feeds/api/playlists/{0}?v=2.1", playList.PlayListId)));
                {
                    XmlNodeList elementsByTagName = xmlDocument.GetElementsByTagName("yt:videoid");
                    foreach (XmlNode xmlNode in elementsByTagName)
                    {
                        try
                        {
                            string innerText = xmlNode.InnerText;
                            if (playList.Videos.ContainsKey(innerText))
                            {
                                continue;
                            }
                            if (!playList.Videos.Keys.Contains(innerText))
                            {
                                playList.Videos.Add(innerText, 720);
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
            catch
            {
            }
        }

        internal void Load(IQueryAdapter dbClient, out uint loadedPly)
        {
            this.Load(dbClient);
            loadedPly = (uint)this._playLists.Count;
        }

        internal void Load(IQueryAdapter dbClient)
        {
            this._playLists.Clear();
            dbClient.SetQuery("SELECT * FROM youtube_playlists");
            DataTable table = dbClient.GetTable();
            if (table != null)
            {
                foreach (DataRow dataRow in table.Rows)
                {
                    this._playLists.Add(dataRow["playlist_id"].ToString(),
                        new PlayList(int.Parse(dataRow["id"].ToString()), dataRow["playlist_id"].ToString(),
                            dataRow["name"].ToString(), dataRow["description"].ToString()));
                }
            }
            this._defaultPlayList = !this._playLists.Any() ? new PlayList(-1, "", "", "") : this._playLists.Values.First();
        }

        internal PlayerTV AddOrGetTV(uint itemId, uint roomId)
        {
            if (this.TVExists(itemId))
            {
                return (PlayerTV)this._loadedTVs[itemId];
            }
            var playerTV = new PlayerTV(itemId, roomId);
            this._loadedTVs.Add(itemId, playerTV);
            return playerTV;
        }

        internal Dictionary<string, PlayList> GetPlaylists()
        {
            return this._playLists;
        }

        internal PlayList GetPlaylist(string playList)
        {
            return this.PlaylistExists(playList) ? this._playLists[playList] : this._defaultPlayList;
        }

        internal PlayList GetDefaultPlaylist()
        {
            return this._defaultPlayList;
        }

        internal bool PlaylistExists(string playList)
        {
            return this._playLists.ContainsKey(playList);
        }

        internal bool PlayVideoInRoom(Room room, string video)
        {
            video = video.Replace("http://www.youtube.", "");
            video = video.Replace("www.youtube.", "");
            try
            {
                video = video.Split('=')[1];
            }
            catch
            {
                return false;
            }

            if (string.IsNullOrEmpty(video))
            {
                return false;
            }

            foreach (PlayerTV TV in this._loadedTVs.Values.Cast<PlayerTV>().Where(TV => TV.RoomId == room.RoomId))
            {
                TV.CustomVideo = video;

                RoomItem Item = room.GetRoomItemHandler().GetItem(TV.Item);
                if (Item == null)
                {
                    continue;
                }
                Item.ExtraData = video;
                var ServerMessage = new ServerMessage(LibraryParser.OutgoingRequest("UpdateRoomItemMessageComposer"));
                Item.Serialize(ServerMessage);
                room.SendMessage(ServerMessage);
            }

            return true;
        }
    }
}