using System.Collections.Generic;
using System.Linq;
using Azure.Messages;

namespace Azure.HabboHotel.YouTube
{
    public class PlayList
    {
        public Dictionary<string, int> Videos;
        public int Id;
        public string PlayListId;
        public string Caption;
        public string Description;

        public PlayList(int id, string playListId, string caption, string description)
        {
            this.Id = id;
            this.PlayListId = playListId;
            this.Caption = caption;
            this.Description = description;
            this.Videos = new Dictionary<string, int>();
            VideoManager.GetVideosForPl(this);
        }

        internal string FirstVideo
        {
            get
            {
                return !this.Videos.Any() ? "" : this.Videos.Keys.First();
            }
        }

        internal List<string> VideoList
        {
            get
            {
                return this.Videos.Keys.ToList();
            }
        }

        internal void Serialize(ServerMessage message)
        {
            message.AppendString(this.PlayListId);
            message.AppendString(this.Caption);
            message.AppendString(this.Description);
        }

        internal int GetDuration(string video)
        {
            if (!this.Videos.ContainsKey(video))
            {
                return 0;
            }
            return (this.Videos[video] + 120);
        }
    }
}