using System.Linq;

namespace Azure.HabboHotel.YouTube
{
    internal class PlayerTV
    {
        //TODO: Add a timer for videos
        public uint Item;
        public uint RoomId;
        public PlayList Playlist;
        public int CurrentOrder;
        internal string CustomVideo = "";

        public PlayerTV(uint item, uint roomId)
        {
            this.Item = item;
            this.RoomId = roomId;
            this.CurrentOrder = 1;
        }

        internal string CurrentVideo
        {
            get
            {
                if (!string.IsNullOrEmpty(this.CustomVideo))
                {
                    return this.CustomVideo;
                }

                if (this.Playlist == null || !this.Playlist.Videos.Any())
                {
                    return "";
                }
                return this.Playlist.VideoList[checked(this.CurrentOrder - 1)];
            }
        }

        internal void SetPlaylist(PlayList playlist)
        {
            this.CustomVideo = "";
            this.CurrentOrder = 1;
            this.Playlist = playlist;
        }

        internal void SetPreviousVideo()
        {
            this.CustomVideo = "";
            if (this.CurrentOrder <= 1)
            {
                this.CurrentOrder = this.Playlist.Videos.Count;
                return;
            }
            checked
            {
                this.CurrentOrder--;
            }
        }

        internal void SetNextVideo()
        {
            this.CustomVideo = "";
            if (this.CurrentOrder >= this.Playlist.Videos.Count)
            {
                this.CurrentOrder = 1;
                return;
            }
            checked
            {
                this.CurrentOrder++;
            }
        }
    }
}