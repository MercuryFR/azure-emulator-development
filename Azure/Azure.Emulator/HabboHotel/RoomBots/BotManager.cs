using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Azure.HabboHotel.RoomBots
{
    class BotManager
    {
        private readonly List<RoomBot> _bots;

        internal BotManager() { _bots = new List<RoomBot>(); }

        internal static RoomBot GenerateBotFromRow(DataRow row)
        {
            if (row == null)
                return null;

            var id = Convert.ToUInt32(row["id"]);

            List<string> speeches = null;
            if (!row.IsNull("speech") && !string.IsNullOrEmpty(row["speech"].ToString()))
                speeches = row["speech"].ToString().Split(';').ToList();

            var bot = new RoomBot(id, Convert.ToUInt32(row["user_id"]), AIType.Generic,
                row["is_bartender"].ToString() == "1");

            bot.Update(Convert.ToUInt32(row["room_id"]), (string)row["walk_mode"], (string) row["name"], (string) row["motto"],
                (string) row["look"],
                int.Parse(row["x"].ToString()), int.Parse(row["y"].ToString()), int.Parse(row["z"].ToString()), 4, 0, 0,
                0, 0, speeches, null, (string) row["gender"], (int) row["dance"], (int) row["speaking_interval"],
                Convert.ToInt32(row["automatic_chat"]) == 1, Convert.ToInt32(row["mix_phrases"]) == 1);

            return bot;
        }

        internal List<RoomBot> GetBotsForRoom(uint roomId)
        {
            return new List<RoomBot>(
                from p in _bots
                where p.RoomId == roomId
                select p);
        }

        internal RoomBot GetBot(uint botId)
        {
            return (
                from p in _bots
                where p.BotId == botId
                select p).FirstOrDefault<RoomBot>();
        }
    }
}