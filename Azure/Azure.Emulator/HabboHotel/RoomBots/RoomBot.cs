using System;
using System.Collections.Generic;
using System.Linq;
using Azure.HabboHotel.Rooms;

namespace Azure.HabboHotel.RoomBots
{
    class RoomBot
    {
        internal uint BotId;
        internal uint RoomId;
        internal int VirtualId;
        internal uint OwnerId;
        internal AIType AiType;
        internal string WalkingMode;
        internal string Name;
        internal string Motto;
        internal string Look;
        internal string Gender;
        internal int X;
        internal int Y;
        internal double Z;
        internal int Rot;
        internal int MinX;
        internal int MaxX;
        internal int MinY;
        internal int MaxY;
        internal int DanceId;
        internal RoomUser RoomUser;
        internal int LastSpokenPhrase;
        internal bool WasPicked;
        internal bool IsBartender;
        internal List<string> RandomSpeech;
        internal List<string> Responses;
        internal int SpeechInterval;
        internal bool AutomaticChat, MixPhrases;
        private readonly int minX;
        private readonly int minY;
        private readonly int maxX;
        private readonly int maxY;

        internal RoomBot(uint botId, uint ownerId, AIType aiType, bool bartender)
        {
            OwnerId = ownerId;
            BotId = botId;
            AiType = aiType;
            VirtualId = -1;
            RoomUser = null;
            LastSpokenPhrase = 1;
            IsBartender = bartender;
        }

        internal RoomBot(uint botId, uint ownerId, uint roomId, AIType aiType, string walkingMode, string name,
            string motto, string look, int x, int y, double z, int rot, int minX, int minY, int maxX, int maxY,
            List<string> speeches, List<string> responses, string gender, int dance, bool bartender)
        {
            OwnerId = ownerId;
            BotId = botId;
            RoomId = roomId;
            AiType = aiType;
            WalkingMode = walkingMode;
            Name = name;
            Motto = motto;
            Look = look;
            X = x;
            Y = y;
            Z = z;
            Rot = rot;
            this.minX = minX;
            this.minY = minY;
            this.maxX = maxX;
            this.maxY = maxY;
            Gender = gender.ToUpper();
            VirtualId = -1;
            RoomUser = null;
            DanceId = dance;
            RandomSpeech = speeches;
            Responses = responses;
            LastSpokenPhrase = 1;
            IsBartender = bartender;
            WasPicked = (roomId == 0);
        }

        internal bool IsPet
        {
            get { return AiType == AIType.Pet; }
        }

        internal void Update(uint roomId, string walkingMode, string name, string motto, string look, int x, int y,
            double z, int rot, int minX, int minY, int maxX, int maxY, List<string> speeches,
            List<string> responses, string gender, int dance, int speechInterval, bool automaticChat,
            bool mixPhrases)
        {
            RoomId = roomId;
            WalkingMode = walkingMode;
            Name = name;
            Motto = motto;
            Look = look;
            X = x;
            Y = y;
            Z = z;
            Rot = rot;
            MinX = minX;
            MinY = minY;
            MaxX = maxX;
            MaxY = maxY;
            Gender = gender.ToUpper();
            VirtualId = -1;
            RoomUser = null;
            DanceId = dance;
            RandomSpeech = speeches;
            Responses = responses;
            WasPicked = (roomId == 0);
            MixPhrases = mixPhrases;
            AutomaticChat = automaticChat;
            SpeechInterval = speechInterval;
        }

        internal string GetRandomSpeech(bool mixPhrases)
        {
            if (!RandomSpeech.Any())
                return "";
            checked
            {
                if (mixPhrases)
                    return RandomSpeech[Azure.GetRandomNumber(0, RandomSpeech.Count - 1)];
                if (LastSpokenPhrase >= RandomSpeech.Count)
                    LastSpokenPhrase = 1;
                var result = RandomSpeech[LastSpokenPhrase - 1];
                LastSpokenPhrase++;
                return result;
            }
        }

        internal BotAI GenerateBotAI(int virtualId, int botId)
        {
            var aiType = AiType;
            if (aiType == AIType.Pet)
                return new PetBot(virtualId);
            return new GenericBot(this, virtualId, botId, AiType, IsBartender, SpeechInterval);
        }
    }
}