using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.Rooms;
using Azure.Messages;
using Azure.Messages.Parsers;
using Azure.Util;

namespace Azure.HabboHotel.RoomBots
{
    internal class PetBot : BotAI
    {
        private int _speechTimer;
        private int _actionTimer;
        private int _energyTimer;

        internal PetBot(int virtualId)
        {
            checked
            {
                this._speechTimer = new Random((virtualId ^ 2) + DateTime.Now.Millisecond).Next(10, 60);
                this._actionTimer = new Random((virtualId ^ 2) + DateTime.Now.Millisecond).Next(10, 30 + virtualId);
                this._energyTimer = new Random((virtualId ^ 2) + DateTime.Now.Millisecond).Next(10, 60);
            }
        }

        internal override void OnSelfEnterRoom()
        {
            Point randomWalkableSquare = this.GetRoom().GetGameMap().GetRandomWalkableSquare();
            if (this.GetRoomUser() != null && this.GetRoomUser().PetData.Type != 16u)
            {
                this.GetRoomUser().MoveTo(randomWalkableSquare.X, randomWalkableSquare.Y);
            }
        }

        internal override void OnSelfLeaveRoom(bool kicked)
        {
        }

        internal override void Modified()
        {
        }

        internal override void OnUserEnterRoom(RoomUser user)
        {
            if (user.GetClient() == null || user.GetClient().GetHabbo() == null)
            {
                return;
            }
            RoomUser roomUser = this.GetRoomUser();
            if (roomUser == null || user.GetClient().GetHabbo().UserName != roomUser.PetData.OwnerName)
            {
                return;
            }
            var random = new Random();
            string[] value = PetLocale.GetValue("welcome.speech.pet");
            string message = value[random.Next(0, checked(value.Length - 1))];
            message += user.GetUserName();
            roomUser.Chat(null, message, false, 0, 0);
        }

        internal override void OnUserLeaveRoom(GameClient client)
        {
        }

        internal override void OnUserSay(RoomUser user, string msg)
        {
            RoomUser roomUser = this.GetRoomUser();

            if (roomUser.PetData.OwnerId != user.GetClient().GetHabbo().Id)
            {
                return;
            }
            if (string.IsNullOrEmpty(msg))
            {
                msg = " ";
            }
            msg = msg.Substring(1);

            bool lazy = false;
            bool unknown = false;
            bool sleeping = false;
            try
            {
                int command = PetCommandHandler.TryInvoke(msg);
                switch (command)
                {
                    case 1:
                        this.RemovePetStatus();
                        roomUser.FollowingOwner = null;
                        break;
                    case 2:
                        if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                        {
                            lazy = true;
                            break;
                        }
                        this.RemovePetStatus();
                        roomUser.PetData.AddExperience(10);
                        roomUser.Statusses.Add("sit", "");
                        roomUser.Statusses.Add("gst", "joy");
                        roomUser.UpdateNeeded = true;
                        this._actionTimer = 25;
                        this._energyTimer = 10;

                        this.SubtractAttributes();
                        break;
                    case 3:
                        if (!roomUser.PetData.HasCommand(2))
                        {
                            unknown = true;
                            break;
                        }
                        if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                        {
                            lazy = true;
                            break;
                        }
                        this.RemovePetStatus();
                        roomUser.PetData.AddExperience(10);
                        roomUser.Statusses.Add("lay", "");
                        roomUser.Statusses.Add("gst", "sml");
                        roomUser.UpdateNeeded = true;
                        this._actionTimer = 25;
                        this._energyTimer = 10;

                        this.SubtractAttributes();
                        break;
                    case 4:
                        if (!roomUser.PetData.HasCommand(3))
                        {
                            unknown = true;
                            break;
                        }
                        if (!roomUser.PetData.HasCommand(7))
                        {
                            unknown = true;
                            break;
                        }
                        if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                        {
                            lazy = true;
                            break;
                        }
                        this.RemovePetStatus();
                        roomUser.PetData.AddExperience(11);
                        roomUser.MoveTo(user.SquareInFront);
                        roomUser.Statusses.Add("gst", "sml");
                        roomUser.UpdateNeeded = true;

                        this._actionTimer = 25;
                        this._energyTimer = 10;
                        this.SubtractAttributes();
                        break;
                    case 5:
                        if (!roomUser.PetData.HasCommand(8))
                        {
                            unknown = true;
                            break;
                        }
                        if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                        {
                            lazy = true;
                            break;
                        }
                        this.RemovePetStatus();
                        roomUser.PetData.AddExperience(25);
                        roomUser.Statusses.Add("std", "");
                        roomUser.UpdateNeeded = true;

                        this._actionTimer = 25;
                        this._energyTimer = 10;
                        this.SubtractAttributes();
                        break;
                    case 6:
                        if (!roomUser.PetData.HasCommand(19))
                        {
                            unknown = true;
                            break;
                        }
                        if (!roomUser.PetData.HasCommand(9))
                        {
                            unknown = true;
                            break;
                        }
                        if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                        {
                            lazy = true;
                            break;
                        }
                        this.RemovePetStatus();
                        roomUser.PetData.AddExperience(35);
                        roomUser.Statusses.Add("jmp", "");
                        roomUser.Statusses.Add("gst", "joy");
                        roomUser.UpdateNeeded = true;

                        this._actionTimer = 45;
                        this._energyTimer = 20;
                        this.SubtractAttributes();
                        break;
                    case 7:
                        if (!roomUser.PetData.HasCommand(15))
                        {
                            unknown = true;
                            break;
                        }
                        if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                        {
                            lazy = true;
                            break;
                        }
                        roomUser.FollowingOwner = roomUser;

                        this.RemovePetStatus();
                        switch (roomUser.RotBody)
                        {
                            case 0:
                                roomUser.MoveTo(roomUser.X + 2, roomUser.Y);
                                break;
                            case 1:
                                roomUser.MoveTo(roomUser.X - 2, roomUser.Y - 2);
                                break;
                            case 2:
                                roomUser.MoveTo(roomUser.X, roomUser.Y + 2);
                                break;
                            case 3:
                                roomUser.MoveTo(roomUser.X + 2, roomUser.Y - 2);
                                break;
                            case 4:
                                roomUser.MoveTo(roomUser.X - 2, roomUser.Y);
                                break;
                            case 5:
                                roomUser.MoveTo(roomUser.X + 2, roomUser.Y + 2);
                                break;
                            case 6:
                                roomUser.MoveTo(roomUser.X, roomUser.Y - 2);
                                break;
                            case 7:
                                roomUser.MoveTo(roomUser.X - 2, roomUser.Y + 2);
                                break;
                        }
                        roomUser.PetData.AddExperience(35);
                        roomUser.Statusses.Add("gst", "sml");
                        roomUser.UpdateNeeded = true;
                        break;
                    case 8:
                        if (!roomUser.PetData.HasCommand(4))
                        {
                            unknown = true;
                            break;
                        }
                        if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                        {
                            lazy = true;
                            break;
                        }
                        this.RemovePetStatus();
                        roomUser.PetData.AddExperience(11);
                        roomUser.Statusses.Add("beg", "");
                        roomUser.Statusses.Add("gst", "sml");
                        roomUser.UpdateNeeded = true;

                        this._actionTimer = 25;
                        this._energyTimer = 10;
                        this.SubtractAttributes();
                        break;
                    case 9:
                        if (!roomUser.PetData.HasCommand(10))
                        {
                            unknown = true;
                            break;
                        }
                        if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                        {
                            lazy = true;
                            break;
                        }
                        this._actionTimer = 1;
                        this._energyTimer = 10;
                        roomUser.Statusses.Add("gst", "sml");
                        roomUser.UpdateNeeded = true;
                        roomUser.PetData.AddExperience(35);
                        this.SubtractAttributes();
                        break;
                    case 10:
                        this.RemovePetStatus();

                        IEnumerable<RoomItem> petNest = this.GetRoom().GetRoomItemHandler().MFloorItems.Values.Where(x => x.GetBaseItem().InteractionType == Items.InteractionType.petnest);
                        if (!petNest.Any())
                        {
                            lazy = true;
                        }
                        RoomItem roomItems = petNest.FirstOrDefault();
                        roomUser.MoveTo(roomItems.X, roomItems.Y);
                        roomUser.PetData.AddExperience(40);
                        int rndmEnergy = new Random().Next(25, 51);
                        if (roomUser.PetData.Energy < (Pet.MaxEnergy - rndmEnergy))
                        {
                            roomUser.PetData.Energy += rndmEnergy;
                        }
                        roomUser.PetData.Nutrition += 15;
                        roomUser.AddStatus("lay", "");
                        roomUser.AddStatus("gst", "eyb");
                        roomUser.UpdateNeeded = true;
                        sleeping = true;
                        this._actionTimer = 500;
                        this._energyTimer = 500;
                        break;
                    case 46:
                        RemovePetStatus();

                        var coord = new Point();
                        switch (roomUser.PetData.Type)
                        {
                            case 3:
                                coord = GetRoom().GetRoomItemHandler().GetRandomBreedingTerrier(roomUser.PetData);
                                break;
                            case 4:
                                coord = GetRoom().GetRoomItemHandler().GetRandomBreedingBear(roomUser.PetData);
                                break;
                        }

                        if (coord == new Point())
                        {
                            var alert = new ServerMessage(LibraryParser.OutgoingRequest("PetBreedErrorMessageComposer"));
                            alert.AppendInteger(0);
                            user.GetClient().SendMessage(alert);

                            return;
                        }

                        roomUser.MoveTo(coord);
                        roomUser.PetData.AddExperience(0);
                        roomUser.PetData.PetEnergy(true);

                        this._actionTimer = 25;
                        this._energyTimer = 120;
                        break;
                    default:
                        lazy = true;
                        this.SubtractAttributes();
                        break;
                }
                /*
                case "DESCANSA":
                case "RELAX":
                case "REST":
                case "FREE":
                this.RemovePetStatus();
                break;
                case "COMER":
                case "EAT":
                if (!roomUser.PetData.HasCommand(43))
                {
                unknown = true;
                break;
                }
                this.RemovePetStatus();
                break;
                case "SI�NTATE":
                case "SIENTATE":
                case "SIT":
                case "SIÉNTATE":
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                roomUser.PetData.AddExperience(10);
                roomUser.Statusses.Add("sit", "");
                roomUser.Statusses.Add("gst", "joy");
                roomUser.UpdateNeeded = true;
                this._actionTimer = 25;
                this._energyTimer = 10;
                this.SubtractAttributes();
                break;
                case "TUMBATE":
                case "LAY":
                case "ACUESTATE":
                case "ACUÉSTATE":
                case "DOWN":
                case "TÚMBATE":
                if (!roomUser.PetData.HasCommand(2))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                roomUser.PetData.AddExperience(10);
                roomUser.Statusses.Add("lay", "");
                roomUser.Statusses.Add("gst", "sml");
                roomUser.UpdateNeeded = true;
                this._actionTimer = 25;
                this._energyTimer = 10;
                this.SubtractAttributes();
                break;
                case "VEN":
                case "VEN AQUÍ":
                case "VEN AQU�":
                case "VEN AQUí":
                case "SÍGUEME":
                case "HERE":
                case "FOLLOW":
                if (!roomUser.PetData.HasCommand(3))
                {
                unknown = true;
                break;
                }
                if (!roomUser.PetData.HasCommand(7))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                roomUser.PetData.AddExperience(11);
                roomUser.MoveTo(user.SquareInFront);
                roomUser.Statusses.Add("gst", "sml");
                roomUser.UpdateNeeded = true;
                this._actionTimer = 25;
                this._energyTimer = 10;
                this.SubtractAttributes();
                break;
                case "LEVANTA":
                case "STAND":
                if (!roomUser.PetData.HasCommand(8))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                roomUser.PetData.AddExperience(25);
                roomUser.Statusses.Add("std", "");
                roomUser.UpdateNeeded = true;
                this._actionTimer = 25;
                this._energyTimer = 10;
                this.SubtractAttributes();
                break;
                case "JUMP":
                case "SALTA":
                case "BOTA":
                if (!roomUser.PetData.HasCommand(19))
                {
                unknown = true;
                break;
                }
                if (!roomUser.PetData.HasCommand(9))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                roomUser.PetData.AddExperience(35);
                roomUser.Statusses.Add("jmp", "");
                roomUser.Statusses.Add("gst", "joy");
                roomUser.UpdateNeeded = true;
                this._actionTimer = 45;
                this._energyTimer = 20;
                this.SubtractAttributes();
                break;
                case "ADELANTE":
                case "FORWARD":
                case "DELANTE":
                case "MOVE FORWARD":
                case "STRAIGHT":
                if (!roomUser.PetData.HasCommand(24))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                roomUser.MoveTo(roomUser.SquareInFront);
                roomUser.PetData.AddExperience(35);
                roomUser.Statusses.Add("gst", "sml");
                roomUser.UpdateNeeded = true;
                break;
                case "IZQUIERDA":
                case "FOLLOW LEFT":
                case "LEFT":
                if (!roomUser.PetData.HasCommand(15))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                switch (roomUser.RotBody)
                {
                case 0:
                roomUser.MoveTo(roomUser.X + 2, roomUser.Y);
                break;
                case 1:
                roomUser.MoveTo(roomUser.X - 2, roomUser.Y - 2);
                break;
                case 2:
                roomUser.MoveTo(roomUser.X, roomUser.Y + 2);
                break;
                case 3:
                roomUser.MoveTo(roomUser.X + 2, roomUser.Y - 2);
                break;
                case 4:
                roomUser.MoveTo(roomUser.X - 2, roomUser.Y);
                break;
                case 5:
                roomUser.MoveTo(roomUser.X + 2, roomUser.Y + 2);
                break;
                case 6:
                roomUser.MoveTo(roomUser.X, roomUser.Y - 2);
                break;
                case 7:
                roomUser.MoveTo(roomUser.X - 2, roomUser.Y + 2);
                break;
                }
                roomUser.PetData.AddExperience(35);
                roomUser.Statusses.Add("gst", "sml");
                roomUser.UpdateNeeded = true;
                break;
                case "DERECHA":
                case "FOLLOW RIGHT":
                case "RIGHT":
                if (!roomUser.PetData.HasCommand(16))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                switch (roomUser.RotBody)
                {
                case 0:
                roomUser.MoveTo(roomUser.X - 2, roomUser.Y);
                break;
                case 1:
                roomUser.MoveTo(roomUser.X + 2, roomUser.Y + 2);
                break;
                case 2:
                roomUser.MoveTo(roomUser.X, roomUser.Y - 2);
                break;
                case 3:
                roomUser.MoveTo(roomUser.X - 2, roomUser.Y + 2);
                break;
                case 4:
                roomUser.MoveTo(roomUser.X + 2, roomUser.Y);
                break;
                case 5:
                roomUser.MoveTo(roomUser.X - 2, roomUser.Y - 2);
                break;
                case 6:
                roomUser.MoveTo(roomUser.X, roomUser.Y + 2);
                break;
                case 7:
                roomUser.MoveTo(roomUser.X + 2, roomUser.Y - 2);
                break;
                }
                roomUser.PetData.AddExperience(35);
                roomUser.Statusses.Add("gst", "sml");
                roomUser.UpdateNeeded = true;
                break;
                case "PIDE":
                case "BEG":
                if (!roomUser.PetData.HasCommand(4))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                roomUser.PetData.AddExperience(11);
                roomUser.Statusses.Add("beg", "");
                roomUser.Statusses.Add("gst", "sml");
                roomUser.UpdateNeeded = true;
                this._actionTimer = 25;
                this._energyTimer = 10;
                this.SubtractAttributes();
                break;
                case "DEAD":
                case "PLAY DEAD":
                case "HAZ EL MUERTO":
                //
                if (!roomUser.PetData.HasCommand(5))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                roomUser.PetData.AddExperience(12);
                roomUser.Statusses.Add("ded", "");
                roomUser.UpdateNeeded = true;
                this._actionTimer = 25;
                this._energyTimer = 10;
                break;
                case "FUTBOL":
                case "FOOTBALL":
                case "SOCCER":
                case "FÚTBOL":
                if (!roomUser.PetData.HasCommand(5))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                var footballs = this.GetRoom().GetRoomItemHandler().MFloorItems.Values.Where(x => x.GetBaseItem().InteractionType == Items.InteractionType.football);
                if (!footballs.Any())
                {
                lazy = true;
                break;
                }
                var item = footballs.FirstOrDefault();
                this._actionTimer = 50;
                this._energyTimer = 30;
                roomUser.MoveTo(item.X, item.Y);
                roomUser.PetData.AddExperience(35);
                this.SubtractAttributes();
                break;
                case "JUEGA":
                case "JUGAR":
                case "PLAY":
                if (!roomUser.PetData.HasCommand(11))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this.RemovePetStatus();
                this.SubtractAttributes();
                break;
                case "QUIETO":
                case "CALLA":
                case "CALM":
                case "ESTATUA":
                case "STATUE":
                case "SHUT":
                case "SHUT UP":
                case "SILENCE":
                case "SILENT":
                case "STAY":
                this.RemovePetStatus();
                this._actionTimer = 650;
                this._energyTimer = 20;
                roomUser.AddStatus("wav", "");
                roomUser.UpdateNeeded = true;
                break;
                case "HABLA":
                case "SPEAK":
                case "TALK":
                if (!roomUser.PetData.HasCommand(10))
                {
                unknown = true;
                break;
                }
                if (roomUser.PetData.Energy < 20 || roomUser.PetData.Nutrition < 25)
                {
                lazy = true;
                break;
                }
                this._actionTimer = 1;
                this._energyTimer = 10;
                roomUser.Statusses.Add("gst", "sml");
                roomUser.UpdateNeeded = true;
                roomUser.PetData.AddExperience(35);
                this.SubtractAttributes();
                break;
                case "DORMIR":
                case "DUERME":
                case "A CASA":
                case "TO NEST":
                case "A DORMIR":
                case "NEST":
                this.RemovePetStatus();
                var petNest = this.GetRoom().GetRoomItemHandler().MFloorItems.Values.Where(x => x.GetBaseItem().InteractionType == Items.InteractionType.petnest);
                if (!petNest.Any())
                {
                lazy = true;
                }
                var roomItem = petNest.FirstOrDefault();
                roomUser.MoveTo(roomItem.X, roomItem.Y);
                roomUser.PetData.AddExperience(40);
                var rndmEnergy = new Random().Next(25, 51);
                if (roomUser.PetData.Energy < (Pet.MaxEnergy - rndmEnergy))
                {
                roomUser.PetData.Energy += rndmEnergy;
                }
                roomUser.PetData.Nutrition += 15;
                roomUser.AddStatus("lay", "");
                roomUser.AddStatus("gst", "eyb");
                roomUser.UpdateNeeded = true;
                sleeping = true;
                this._actionTimer = 500;
                this._energyTimer = 500;
                break;
                default:
                lazy = true;
                this.SubtractAttributes();
                break;
                }*/
            }
            catch (Exception)
            {
                lazy = true;
                this.SubtractAttributes();
            }

            if (sleeping)
            {
                string[] value = PetLocale.GetValue("tired");
                string message = value[new Random().Next(0, checked(value.Length - 1))];
                roomUser.Chat(null, message, false, 0, 0);
            }
            else if (unknown)
            {
                string[] value = PetLocale.GetValue("pet.unknowncommand");
                string message = value[new Random().Next(0, checked(value.Length - 1))];
                roomUser.Chat(null, message, false, 0, 0);
            }
            else if (lazy)
            {
                string[] value = PetLocale.GetValue("pet.lazy");
                string message = value[new Random().Next(0, checked(value.Length - 1))];
                roomUser.Chat(null, message, false, 0, 0);
            }
            else
            {
                string[] value = PetLocale.GetValue("pet.done");
                string message = value[new Random().Next(0, checked(value.Length - 1))];
                roomUser.Chat(null, message, false, 0, 0);
            }
        }

        internal override void OnUserShout(RoomUser user, string message)
        {
        }

        internal override void OnTimerTick()
        {
            checked
            {
                if (this._speechTimer <= 0)
                {
                    RoomUser roomUser = this.GetRoomUser();
                    if (roomUser != null)
                    {
                        if (roomUser.PetData.DbState != DatabaseUpdateState.NeedsInsert)
                            roomUser.PetData.DbState = DatabaseUpdateState.NeedsUpdate;
                        var random = new Random();
                        this.RemovePetStatus();
                        string[] value = PetLocale.GetValue(string.Format("speech.pet{0}", roomUser.PetData.Type));
                        string text = value[random.Next(0, value.Length - 1)];
                        if (text.Length != 3)
                            roomUser.Chat(null, text, false, 0, 0);
                        else
                            roomUser.Statusses.Add(text, TextHandling.GetString(roomUser.Z));
                    }
                    this._speechTimer = Azure.GetRandomNumber(20, 120);
                }
                else
                    this._speechTimer--;
                if (this._actionTimer <= 0)
                {
                    try
                    {
                        if (GetRoomUser().FollowingOwner != null)
                            _actionTimer = 2;
                        else
                            _actionTimer = Azure.GetRandomNumber(15, 40 + GetRoomUser().PetData.VirtualId);
                        this.RemovePetStatus();
                        this._actionTimer = Azure.GetRandomNumber(15, 40 + this.GetRoomUser().PetData.VirtualId);
                        if (GetRoomUser().RidingHorse != true)
                        {
                            RemovePetStatus();

                            if (GetRoomUser().FollowingOwner != null)
                            {
                                GetRoomUser().MoveTo(GetRoomUser().FollowingOwner.SquareBehind);
                            }
                            else
                            {
                                var nextCoord = GetRoom().GetGameMap().getRandomWalkableSquare();
                                GetRoomUser().MoveTo(nextCoord.X, nextCoord.Y);
                            }
                        }
                        if (new Random().Next(2, 15) % 2 == 0)
                        {
                            if (this.GetRoomUser().PetData.Type == 16)
                            {
                                MoplaBreed breed = this.GetRoomUser().PetData.MoplaBreed;
                                this.GetRoomUser().PetData.Energy--;
                                this.GetRoomUser().AddStatus("gst", (breed.LiveState == MoplaState.Dead) ? "sad" : "sml");
                                this.GetRoomUser().PetData.MoplaBreed.OnTimerTick(this.GetRoomUser().PetData.LastHealth, this.GetRoomUser().PetData.UntilGrown);
                            }
                            else
                            {
                                if (this.GetRoomUser().PetData.Energy < 30)
                                    this.GetRoomUser().AddStatus("lay", "");
                                else
                                {
                                    this.GetRoomUser().AddStatus("gst", "joy");
                                    if (new Random().Next(1, 7) == 3)
                                        this.GetRoomUser().AddStatus("snf", "");
                                }
                            }
                            this.GetRoomUser().UpdateNeeded = true;
                        }                    
                        goto IL_1B5;
                    }
                    catch (Exception pException)
                    {
                        Logging.HandleException(pException, "PetBot.OnTimerTick");
                        goto IL_1B5;
                    }
                }
                this._actionTimer--;
                IL_1B5:
                if (this._energyTimer <= 0)
                {
                    this.RemovePetStatus();
                    RoomUser roomUser2 = this.GetRoomUser();
                    roomUser2.PetData.PetEnergy(true);
                    this._energyTimer = Azure.GetRandomNumber(30, 120);
                    return;
                }
                this._energyTimer--;
            }
        }

        private void RemovePetStatus()
        {
            RoomUser roomUser = this.GetRoomUser();
            roomUser.Statusses.Clear();
            roomUser.UpdateNeeded = true;
        }

        private void SubtractAttributes()
        {
            RoomUser roomUser = this.GetRoomUser();
            if (roomUser.PetData.Energy < 11)
                roomUser.PetData.Energy = 0;
            else
                roomUser.PetData.Energy -= 10;
            if (roomUser.PetData.Nutrition < 6)
                roomUser.PetData.Nutrition = 0;
            else
                roomUser.PetData.Nutrition -= 5;
        }
    }
}