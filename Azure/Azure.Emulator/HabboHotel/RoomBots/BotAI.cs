using System;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Rooms;

namespace Azure.HabboHotel.RoomBots
{
    internal abstract class BotAI
    {
        internal uint BaseId;
        private int _roomUserId;
        private uint _roomId;
        private RoomUser _roomUser;
        private Room _room;

        internal void Init(uint pBaseId, int pRoomUserId, uint pRoomId, RoomUser user, Room room)
        {
            BaseId = pBaseId;
            _roomUserId = pRoomUserId;
            _roomId = pRoomId;
            _roomUser = user;
            _room = room;
        }

        internal Room GetRoom()
        {
            return _room;
        }

        internal RoomUser GetRoomUser()
        {
            return _roomUser;
        }

        internal RoomBot GetBotData()
        {
            return GetRoomUser() == null ? null : GetRoomUser().BotData;
        }

        internal abstract void OnSelfEnterRoom();

        internal abstract void OnSelfLeaveRoom(bool kicked);

        internal abstract void OnUserEnterRoom(RoomUser user);

        internal abstract void OnUserLeaveRoom(GameClient client);

        internal abstract void OnUserSay(RoomUser user, string msg);

        internal abstract void OnUserShout(RoomUser user, string message);

        internal abstract void OnTimerTick();

        internal abstract void Modified();
    }
}