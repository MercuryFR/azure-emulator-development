﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using Azure.Configuration;
using Azure.Connection.Connection;
using Azure.HabboHotel.Users.Messenger;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.GameClients
{
    class GameClientManager
    {
        internal HybridDictionary Clients;
        private readonly Queue _clientsAddQueue;
        private readonly Queue _clientsToRemove;
        private readonly Queue _badgeQueue;
        private readonly Queue _broadcastQueue;
        private readonly HybridDictionary _userNameRegister;
        private readonly HybridDictionary _userIdRegister;
        private readonly HybridDictionary _userNameIdRegister;
        private readonly HybridDictionary _idUserNameRegister;
        private readonly Queue _timedOutConnections;

        internal GameClientManager()
        {
            Clients = new HybridDictionary();
            _clientsAddQueue = new Queue();
            _clientsToRemove = new Queue();
            _badgeQueue = new Queue();
            _broadcastQueue = new Queue();
            _timedOutConnections = new Queue();
            _userNameRegister = new HybridDictionary();
            _userIdRegister = new HybridDictionary();
            _userNameIdRegister = new HybridDictionary();
            _idUserNameRegister = new HybridDictionary();
            var thread = new Thread(HandleTimeouts);
            thread.Start();
        }

        internal int ClientCount
        {
            get { return Clients.Count; }
        }

        internal GameClient GetClientByUserId(uint userId)
        {
            return _userIdRegister.Contains(userId) ? (GameClient) _userIdRegister[userId] : null;
        }

        internal GameClient GetClientByUserName(string userName)
        {
            return _userNameRegister.Contains(userName.ToLower())
                ? (GameClient) _userNameRegister[userName.ToLower()]
                : null;
        }

        internal GameClient GetClient(uint clientId)
        {
            return Clients.Contains(clientId) ? (GameClient) Clients[clientId] : null;
        }

        internal string GetNameById(uint id)
        {
            var clientByUserId = GetClientByUserId(id);
            if (clientByUserId != null)
                return clientByUserId.GetHabbo().UserName;
            string String;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT username FROM users WHERE id = {0}", id));
                String = queryReactor.GetString();
            }
            return String;
        }

        internal IEnumerable<GameClient> GetClientsById(Dictionary<uint, MessengerBuddy>.KeyCollection users)
        {
            return users.Select(GetClientByUserId).Where(clientByUserId => clientByUserId != null);
        }

        internal void SendSuperNotif(string title, string notice, string picture, GameClient client, string link,
            string linkTitle, bool broadCast, bool Event)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
            serverMessage.AppendString(picture);
            serverMessage.AppendInteger(4);
            serverMessage.AppendString("title");
            serverMessage.AppendString(title);
            serverMessage.AppendString("message");
            if (broadCast)
                if (Event)
                {
                    var text1 = Azure.GetLanguage().GetVar("ha_event_one");
                    var text2 = Azure.GetLanguage().GetVar("ha_event_two");
                    var text3 = Azure.GetLanguage().GetVar("ha_event_three");
                    serverMessage.AppendString(string.Format("<b>{0} {1}!</b>\r\n {2} .\r\n<b>{3}</b>\r\n{4}", text1,
                        client.GetHabbo().CurrentRoom.Owner, text2, text3, notice));
                }
                else
                {
                    var text4 = Azure.GetLanguage().GetVar("ha_title");
                    serverMessage.AppendString(string.Concat(new[]
                    {
                        "<b>" + text4 + "</b>\r\n",
                        notice,
                        "\r\n- <i>",
                        client.GetHabbo().UserName,
                        "</i>"
                    }));
                }
            else
                serverMessage.AppendString(notice);
            if (link != "")
            {
                serverMessage.AppendString("linkUrl");
                serverMessage.AppendString(link);
                serverMessage.AppendString("linkTitle");
                serverMessage.AppendString(linkTitle);
            }
            else
            {
                serverMessage.AppendString("linkUrl");
                serverMessage.AppendString("event:");
                serverMessage.AppendString("linkTitle");
                serverMessage.AppendString("ok");
            }

            if (broadCast)
            {
                QueueBroadcaseMessage(serverMessage);
                return;
            }
            client.SendMessage(serverMessage);
        }

        internal void OnCycle()
        {
            try
            {
                RemoveClients();
                AddClients();
                GiveBadges();
                BroadcastPackets();
                Azure.GetGame().ClientManagerCycleEnded = true;
            }
            catch (Exception ex)
            {
                Logging.LogThreadException(ex.ToString(), "GameClientManager.OnCycle Exception --> Not inclusive");
            }
        }

        internal void StaffAlert(ServerMessage message, uint exclude = 0u)
        {
            var gameClients =
                Clients.Values.OfType<GameClient>()
                    .Where(
                        x =>
                            x.GetHabbo() != null && x.GetHabbo().Rank >= Azure.StaffAlertMinRank &&
                            x.GetHabbo().Id != exclude);
            foreach (var current in gameClients)
                current.SendMessage(message);
        }

        internal void ModAlert(ServerMessage message)
        {
            var bytes = message.GetReversedBytes();
            foreach (
                var current in
                    Clients.Values.Cast<GameClient>().Where(current => current != null && current.GetHabbo() != null))
            {
                if (current.GetHabbo().Rank != 4u && current.GetHabbo().Rank != 5u)
                    if (current.GetHabbo().Rank != 6u)
                        continue;
                try
                {
                    current.GetConnection().SendData(bytes);
                }
                catch
                {

                }
            }
        }

        internal void CreateAndStartClient(uint clientId, ConnectionInformation connection)
        {
            var obj = new GameClient(clientId, connection);
            if (Clients.Contains(clientId))
                Clients.Remove(clientId);
            lock (_clientsAddQueue.SyncRoot)
                _clientsAddQueue.Enqueue(obj);
        }

        internal void DisposeConnection(uint clientId)
        {
            var client = GetClient(clientId);
            if (client != null)
                client.Stop();
            lock (_clientsToRemove.SyncRoot)
                _clientsToRemove.Enqueue(clientId);
        }

        internal void QueueBroadcaseMessage(ServerMessage message)
        {
            lock (_broadcastQueue.SyncRoot)
                _broadcastQueue.Enqueue(message);
        }

        internal void QueueBadgeUpdate(string badge)
        {
            lock (_badgeQueue.SyncRoot)
                _badgeQueue.Enqueue(badge);
        }

        internal void LogClonesOut(uint userId)
        {
            var clientByUserId = GetClientByUserId(userId);
            if (clientByUserId != null)
                clientByUserId.Disconnect("user null LogClonesOut");
        }

        internal void RegisterClient(GameClient client, uint userId, string userName)
        {
            if (_userNameRegister.Contains(userName.ToLower()))
                _userNameRegister[userName.ToLower()] = client;
            else
                _userNameRegister.Add(userName.ToLower(), client);
            if (_userIdRegister.Contains(userId))
                _userIdRegister[userId] = client;
            else
                _userIdRegister.Add(userId, client);
            if (!_userNameIdRegister.Contains(userName))
                _userNameIdRegister.Add(userName, userId);
            if (!_idUserNameRegister.Contains(userId))
                _idUserNameRegister.Add(userId, userName);
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.SetQuery(string.Format("UPDATE users SET online='1' WHERE id={0} LIMIT 1", userId));
        }

        internal void UnregisterClient(uint userid, string Username)
        {
            _userIdRegister.Remove(userid);
            _userNameRegister.Remove(Username.ToLower());
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.SetQuery(string.Format("UPDATE users SET online='0' WHERE id={0} LIMIT 1", userid));
        }

        internal void CloseAll()
        {
            var stringBuilder = new StringBuilder();
            var flag = false;
            var num = 0;
            checked
            {
                num += Clients.Values.Cast<GameClient>().Count(current => current.GetHabbo() != null);
                if (num < 1)
                    num = 1;
                var num2 = 0;
                var count = Clients.Count;
                foreach (GameClient current2 in Clients.Values)
                {
                    num2++;
                    if (current2.GetHabbo() == null)
                        continue;
                    try
                    {
                        current2.GetHabbo().GetInventoryComponent().RunDbUpdate();
                        current2.GetHabbo().RunDbUpdate(Azure.GetDatabaseManager().GetQueryReactor());
                        stringBuilder.Append(current2.GetHabbo().GetQueryString);
                        flag = true;
                        System.Console.ForegroundColor = System.ConsoleColor.DarkMagenta;
                        Console.WriteLine("≫ Saving Inventary Content: {0}%",
                            string.Format("{0:0.##}", unchecked(num2 / (double) num * 100.0)));
                    }
                    catch
                    {

                    }
                }
                if (flag)
                    try
                    {
                        if (stringBuilder.Length > 0)
                            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                                queryReactor.RunFastQuery(stringBuilder.ToString());
                    }
                    catch (Exception pException)
                    {
                        Logging.HandleException(pException, "GameClientManager.CloseAll()");
                    }
                try
                {
                    var num3 = 0;
                    foreach (GameClient current3 in Clients.Values)
                    {
                        num3++;
                        if (current3.GetConnection() == null)
                            continue;
                        try
                        {
                            current3.GetConnection().Dispose();
                        }
                        catch
                        {

                        }
                        System.Console.ForegroundColor = System.ConsoleColor.DarkMagenta;
                        Console.WriteLine("≫ Closing Connection Manager: {0}%",
                            string.Format("{0:0.##}", unchecked(num3 / (double) count * 100.0)));
                    }
                }
                catch (Exception ex)
                {
                    Logging.LogCriticalException(ex.ToString());
                }
                Clients.Clear();
                Console.WriteLine("≫ Connections were closed!");
            }
        }

        internal void UpdateClient(string oldName, string newName)
        {
            if (!_userNameRegister.Contains(oldName.ToLower()))
                return;
            var old = (GameClient) _userNameRegister[oldName.ToLower()];
            _userNameRegister.Remove(oldName.ToLower());
            _userNameRegister.Add(newName.ToLower(), old);
        }

        private void HandleTimeouts()
        {
            while (true)
            {
                try
                {
                    while (_timedOutConnections != null && _timedOutConnections.Count > 0)
                    {
                        GameClient gameClient = null;
                        lock (_timedOutConnections.SyncRoot)
                            if (_timedOutConnections.Count > 0)
                                gameClient = (GameClient) _timedOutConnections.Dequeue();
                        if (gameClient != null)
                            gameClient.Disconnect("user null HandleTimeouts");
                    }
                }
                catch (Exception ex)
                {
                    Logging.LogThreadException(ex.ToString(), "HandleTimeoutsVoid");
                }
                Thread.Sleep(5000);
            }
        }

        private void AddClients()
        {
            var now = DateTime.Now;
            if (_clientsAddQueue.Count > 0)
                lock (_clientsAddQueue.SyncRoot)
                {
                    while (_clientsAddQueue.Count > 0)
                    {
                        var gameClient = (GameClient) _clientsAddQueue.Dequeue();
                        Clients.Add(gameClient.ConnectionId, gameClient);
                        gameClient.StartConnection();
                    }
                }
            var timeSpan = DateTime.Now - now;
            if (timeSpan.TotalSeconds > 3.0)
                Console.WriteLine("GameClientManager.AddClients spent: {0} seconds in working.", timeSpan.TotalSeconds);
        }

        private void RemoveClients()
        {
            try
            {
                var now = DateTime.Now;
                if (_clientsToRemove.Count > 0)
                    lock (_clientsToRemove.SyncRoot)
                    {
                        while (_clientsToRemove.Count > 0)
                        {
                            var key = (uint) _clientsToRemove.Dequeue();
                            Clients.Remove(key);
                        }
                    }
                var timeSpan = DateTime.Now - now;
                if (timeSpan.TotalSeconds > 3.0)
                    Console.WriteLine("GameClientManager.RemoveClients spent: {0} seconds in working.",
                        timeSpan.TotalSeconds);
            }
            catch (Exception ex)
            {
                Logging.LogThreadException(ex.ToString(), "GameClientManager.RemoveClients Exception --> Not inclusive");
            }
        }

        private void GiveBadges()
        {
            try
            {
                var now = DateTime.Now;
                if (_badgeQueue.Count > 0)
                    lock (_badgeQueue.SyncRoot)
                        while (_badgeQueue.Count > 0)
                        {
                            var badge = (string) _badgeQueue.Dequeue();
                            foreach (
                                var current in
                                    Clients.Values.Cast<GameClient>().Where(current => current.GetHabbo() != null))
                                try
                                {
                                    current.GetHabbo().GetBadgeComponent().GiveBadge(badge, true, current, false);
                                    current.SendNotif("¡Has recibido una placa! Mira en tu inventario.");
                                }
                                catch
                                {

                                }
                        }
                var timeSpan = DateTime.Now - now;
                if (timeSpan.TotalSeconds > 3.0)
                    Console.WriteLine("GameClientManager.GiveBadges spent: {0} seconds in working.",
                        timeSpan.TotalSeconds);
            }
            catch (Exception ex)
            {
                Logging.LogThreadException(ex.ToString(), "GameClientManager.GiveBadges Exception --> Not inclusive");
            }
        }

        private void BroadcastPackets()
        {
            try
            {
                var now = DateTime.Now;
                if (_broadcastQueue.Count > 0)
                    lock (_broadcastQueue.SyncRoot)
                        while (_broadcastQueue.Count > 0)
                        {
                            var serverMessage = (ServerMessage) _broadcastQueue.Dequeue();
                            var bytes = serverMessage.GetReversedBytes();
                            foreach (GameClient current in Clients.Values)
                                try
                                {
                                    current.GetConnection().SendData(bytes);
                                }
                                catch
                                {

                                }
                        }
                var timeSpan = DateTime.Now - now;
                if (timeSpan.TotalSeconds > 3.0)
                    Console.WriteLine("GameClientManager.BroadcastPackets spent: {0} seconds in working.",
                        timeSpan.TotalSeconds);
            }
            catch (Exception ex)
            {
                Logging.LogThreadException(ex.ToString(),
                    "GameClientManager.BroadcastPackets Exception --> Not inclusive");
            }
        }
    }
}