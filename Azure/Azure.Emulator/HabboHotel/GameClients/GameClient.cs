using System;
using System.Linq;
using Azure.Configuration;
using Azure.Connection.Connection;
using Azure.Connection.Net;
using Azure.Encryption.Hurlant.Crypto.Prng;
using Azure.HabboHotel.Misc;
using Azure.HabboHotel.Users;
using Azure.HabboHotel.Users.UserDataManagement;
using Azure.Messages;
using Azure.Messages.Handlers;
using Azure.Messages.Parsers;

namespace Azure.HabboHotel.GameClients
{
    public class GameClient
    {
        internal byte PublicistaCount;
        internal DateTime TimePingedReceived;
        internal GamePacketParser PacketParser;
        internal int DesignedHandler = 1;
        internal int CurrentRoomUserId;
        internal string MachineId;
        private ConnectionInformation _connection;
        private GameClientMessageHandler _messageHandler;
        private Habbo _habbo;
        private bool _disconnected;

        internal GameClient(uint clientId, ConnectionInformation pConnection)
        {
            ConnectionId = clientId;
            _connection = pConnection;
            CurrentRoomUserId = -1;
            PacketParser = new GamePacketParser(this);
            _connection.SetClient(this);
        }

        internal uint ConnectionId { get; private set; }

        internal ARC4 ARC4 { get; set; }

        internal void HandlePublicista(string message)
        {
            var flag = false;
            if (PublicistaCount < 2)
                Azure.GetGame()
                    .GetClientManager()
                    .SendSuperNotif("Hey!!",
                        "Please stop advertising other hotels. You will be muted if you do it again.<br /> Need more information? Click the link below.",
                        "frank10", this, "event:", "ok", false, false);
            else if (PublicistaCount < 3)
            {
                Azure.GetGame()
                    .GetClientManager()
                    .SendSuperNotif("You have been muted", "Sorry but you were muted by <b>advertising other hotel</b>",
                        "frank10", this, "event:", "ok", false, false);
                GetHabbo().Mute();
                flag = true;
            }
            else if (PublicistaCount >= 3)
                return;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
            serverMessage.AppendString("staffcloud");
            serverMessage.AppendInteger(4);
            serverMessage.AppendString("title");
            serverMessage.AppendString("Possible advertiser found!");
            serverMessage.AppendString("message");
            serverMessage.AppendString(string.Concat(new[]
            {
                "This user has been detected as advertiser: <b>",
                GetHabbo().UserName,
                ".<)/b> Is he/she advertising an hotel like this?:<br />\"<b>",
                message,
                "</b>\".<br /><br />",
                flag ? "<i>The user was automatically muted.</i>" : "The user was automatically warned.</i>"
            }));
            serverMessage.AppendString("link");
            serverMessage.AppendString("event:");
            serverMessage.AppendString("linkTitle");
            serverMessage.AppendString("ok");
            Azure.GetGame().GetClientManager().StaffAlert(serverMessage, 0U);
        }

        internal ConnectionInformation GetConnection() { return _connection; }

        internal GameClientMessageHandler GetMessageHandler() { return _messageHandler; }

        internal Habbo GetHabbo() { return _habbo; }

        internal void StartConnection()
        {
            if (_connection == null)
                return;
            TimePingedReceived = DateTime.Now;
            (_connection.Parser as InitialPacketParser).PolicyRequest += PolicyRequest;
            (_connection.Parser as InitialPacketParser).SwitchParserRequest += SwitchParserRequest;
            _connection.StartPacketProcessing();
        }

        internal void InitHandler() { _messageHandler = new GameClientMessageHandler(this); }

        internal bool TryLogin(string authTicket)
        {
            try
            {
                var ip = GetConnection().GetIp();
                byte b;
                var userData = UserDataFactory.GetUserData(authTicket, ip, out b);
                if (b == 1 || b == 2)
                    return false;
                Azure.GetGame()
                    .GetClientManager()
                    .RegisterClient(this, userData.UserId, userData.User.UserName);
                _habbo = userData.User;
                userData.User.LoadData(userData);
                var banReason = Azure.GetGame()
                    .GetBanManager()
                    .GetBanReason(userData.User.UserName, ip, MachineId);
                if (!string.IsNullOrEmpty(banReason) || userData.User.UserName == null)
                {
                    SendNotifWithScroll(banReason);
                    using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                    {
                        queryReactor.SetQuery(string.Format("SELECT ip_last FROM users WHERE id={0} LIMIT 1",
                            GetHabbo().Id));
                        var @string = queryReactor.GetString();
                        queryReactor.SetQuery(
                            string.Format("SELECT COUNT(0) FROM users_bans_access WHERE user_id={0} LIMIT 1", _habbo.Id));
                        var integer = queryReactor.GetInteger();
                        if (integer > 0)
                            queryReactor.RunFastQuery(string.Concat(new object[]
                            {
                                "UPDATE users_bans_access SET attempts = attempts + 1, ip='",
                                @string,
                                "' WHERE user_id=",
                                GetHabbo().Id,
                                " LIMIT 1"
                            }));
                        else
                            queryReactor.RunFastQuery(string.Concat(new object[]
                            {
                                "INSERT INTO users_bans_access (user_id, ip) VALUES (",
                                GetHabbo().Id,
                                ", '",
                                @string,
                                "')"
                            }));
                    }
                    return false;
                }
                userData.User.Init(this, userData);
                var queuedServerMessage = new QueuedServerMessage(_connection);
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("UniqueMachineIDMessageComposer"));
                serverMessage.AppendString(MachineId);
                queuedServerMessage.AppendResponse(serverMessage);
                queuedServerMessage.AppendResponse(
                    new ServerMessage(LibraryParser.OutgoingRequest("AuthenticationOKMessageComposer")));
                if (_habbo != null)
                {
                    var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("HomeRoomMessageComposer"));
                    serverMessage2.AppendInteger(_habbo.HomeRoom);
                    serverMessage2.AppendInteger(_habbo.HomeRoom);
                    queuedServerMessage.AppendResponse(serverMessage2);
                }
                serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("MinimailCountMessageComposer"));
                serverMessage.AppendInteger(_habbo.MinimailUnreadMessages);
                queuedServerMessage.AppendResponse(serverMessage);

                serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("FavouriteRoomsMessageComposer"));
                serverMessage.AppendInteger(30);

                if (userData.User.FavoriteRooms == null || !userData.User.FavoriteRooms.Any())
                    serverMessage.AppendInteger(0);
                else
                {
                    serverMessage.AppendInteger(userData.User.FavoriteRooms.Count);
                    foreach (uint i in userData.User.FavoriteRooms)
                        serverMessage.AppendInteger(i);
                }
                queuedServerMessage.AppendResponse(serverMessage);

                var rightsMessage = new ServerMessage(LibraryParser.OutgoingRequest("UserClubRightsMessageComposer"));
                rightsMessage.AppendInteger(userData.User.GetSubscriptionManager().HasSubscription ? 2 : 0);
                rightsMessage.AppendInteger(userData.User.Rank);
                rightsMessage.AppendInteger(0);
                queuedServerMessage.AppendResponse(rightsMessage);

                serverMessage =
                    new ServerMessage(LibraryParser.OutgoingRequest("EnableNotificationsMessageComposer"));
                serverMessage.AppendBool(true);
                serverMessage.AppendBool(false);
                queuedServerMessage.AppendResponse(serverMessage);

                serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("EnableTradingMessageComposer"));
                serverMessage.AppendBool(true);
                queuedServerMessage.AppendResponse(serverMessage);
                userData.User.UpdateCreditsBalance();

                serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ActivityPointsMessageComposer"));
                serverMessage.AppendInteger(2);
                serverMessage.AppendInteger(0);
                serverMessage.AppendInteger(userData.User.ActivityPoints);
                serverMessage.AppendInteger(5);
                serverMessage.AppendInteger(userData.User.BelCredits);
                queuedServerMessage.AppendResponse(serverMessage);

                if (userData.User.HasFuse("fuse_mod"))
                    queuedServerMessage.AppendResponse(Azure.GetGame().GetModerationTool().SerializeTool());
                queuedServerMessage.AppendResponse(
                    Azure.GetGame().GetAchievementManager().AchievementDataCached);
                if (!GetHabbo().NuxPassed && ExtraSettings.NEW_users_gifts_ENABLED)
                    queuedServerMessage.AppendResponse(
                        new ServerMessage(LibraryParser.OutgoingRequest("NuxSuggestFreeGiftsMessageComposer")));
                queuedServerMessage.AppendResponse(GetHabbo().GetAvatarEffectsInventoryComponent().GetPacket());
                queuedServerMessage.SendResponse();
                Azure.GetGame().GetAchievementManager().TryProgressHabboClubAchievements(this);
                Azure.GetGame().GetAchievementManager().TryProgressRegistrationAchievements(this);
                Azure.GetGame().GetAchievementManager().TryProgressLoginAchievements(this);
                return true;
            }
            catch (Exception ex)
            {
                Logging.LogCriticalException(string.Format("Bug during user login: {0}", ex));
            }
            return false;
        }

        internal void SendNotifWithScroll(string message)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("MOTDNotificationMessageComposer"));
            serverMessage.AppendInteger(1);
            serverMessage.AppendString(message);
            SendMessage(serverMessage);
        }

        internal void SendBroadcastMessage(string message)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("BroadcastNotifMessageComposer"));
            serverMessage.AppendString(message);
            serverMessage.AppendString("");
            SendMessage(serverMessage);
        }

        internal void SendWhisper(string message)
        {
            if (GetHabbo().CurrentRoom == null)
                return;
            var roomUserByHabbo =
                GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(GetHabbo().UserName);
            if (roomUserByHabbo == null)
                return;
            var whisp = new ServerMessage(LibraryParser.OutgoingRequest("WhisperMessageComposer"));
            whisp.AppendInteger(roomUserByHabbo.VirtualId);
            whisp.AppendString(message);
            whisp.AppendInteger(0);
            whisp.AppendInteger(roomUserByHabbo.LastBubble);
            whisp.AppendInteger(0);
            whisp.AppendInteger(0);
            SendMessage(whisp);
        }

        internal void SendNotif(string message, string title = "Notification", string picture = "")
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
            serverMessage.AppendString(picture);
            serverMessage.AppendInteger(4);
            serverMessage.AppendString("title");
            serverMessage.AppendString(title);
            serverMessage.AppendString("message");
            serverMessage.AppendString(message);
            serverMessage.AppendString("linkUrl");
            serverMessage.AppendString("event:");
            serverMessage.AppendString("linkTitle");
            serverMessage.AppendString("ok");
            SendMessage(serverMessage);
        }

        internal void Stop()
        {
            if (GetMessageHandler() != null)
                _messageHandler.Destroy();
            if (GetHabbo() != null)
                _habbo.OnDisconnect("disconnect");
            CurrentRoomUserId = -1;
            _messageHandler = null;
            _habbo = null;
            _connection = null;
        }

        internal void Disconnect(string reason)
        {
            if (GetHabbo() != null)
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                    queryReactor.RunFastQuery(GetHabbo().GetQueryString);
            GetHabbo().OnDisconnect(reason);
            if (_disconnected)
                return;
            if (_connection != null)
                _connection.Dispose();
            _disconnected = true;
        }

        internal void SendMessage(ServerMessage message)
        {
            if (message == null)
                return;
            var bytes = message.GetReversedBytes();
            if (GetConnection() == null)
                return;
            GetConnection().SendData(bytes);
        }

        private void SwitchParserRequest()
        {
            if (_messageHandler == null)
                InitHandler();
            PacketParser.SetConnection(_connection);
            PacketParser.OnNewPacket += parser_onNewPacket;
            var currentData = (_connection.Parser as InitialPacketParser).CurrentData;
            _connection.Parser.Dispose();
            _connection.Parser = PacketParser;
            _connection.Parser.HandlePacketData(currentData);
        }

        private void parser_onNewPacket(ClientMessage message)
        {
            try
            {
                _messageHandler.HandleRequest(message);
                //Logging.WriteLine("Handled Packet: " + Message, Mercury.ConsoleColor.Red);
                WinConsole.Color = ConsoleColor.Cyan;
            }
            catch (Exception ex)
            {
                Logging.LogPacketException(message.ToString(), ex.ToString());
            }
        }

        private void PolicyRequest()
        {
            _connection.SendData(Azure.GetDefaultEncoding().GetBytes(CrossdomainPolicy.GetXmlPolicy()));
        }
    }
}