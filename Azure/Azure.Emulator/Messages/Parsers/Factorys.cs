using System.Collections.Concurrent;

namespace Azure.Messages.Parsers
{
    static class Factorys
    {
        private static readonly ConcurrentQueue<ClientMessage> FreeObjects = new ConcurrentQueue<ClientMessage>();

        public static ClientMessage GetClientMessage(int messageId, byte[] body)
        {
            ClientMessage clientMessage;
            if (!FreeObjects.TryDequeue(out clientMessage))
                return new ClientMessage(messageId, body);
            clientMessage.Init(messageId, body);
            return clientMessage;
        }

        public static void ObjectCallback(ClientMessage message) { FreeObjects.Enqueue(message); }
    }
}