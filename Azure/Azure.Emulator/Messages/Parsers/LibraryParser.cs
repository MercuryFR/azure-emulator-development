using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Azure.Configuration;
using Azure.Messages.Handlers;

namespace Azure.Messages.Parsers
{
    static class LibraryParser
    {
        internal static Dictionary<int, StaticRequestHandler> Incoming;
        internal static Dictionary<string, string> Library;
        internal static Dictionary<string, int> Outgoing;
        internal static Dictionary<string, string> Config;
        internal static int CountReleases;
        internal static string ReleaseName;
        public delegate void ParamLess();
        internal delegate void StaticRequestHandler(GameClientMessageHandler handler);

        public static int OutgoingRequest(string packetName)
        {
            if (!Outgoing.ContainsKey(packetName))
                return 0;
            var packetId = Convert.ToInt32(Outgoing[packetName]);
            if(Azure.StartOutgoing && packetId != 2638)
                Logging.WriteLine("[Outgoing] Handled " + packetId, ConsoleColor.White);
            return packetId;
        }

        public static void LibRequest(string handler) { }

        public static void Initialize()
        {
            Logging.WriteLine(string.Format("Loaded {0} Habbo Releases", CountReleases), ConsoleColor.Cyan);
            Logging.WriteLine(string.Format("Loaded {0} Event Controllers", Incoming.Count), ConsoleColor.Cyan);
        }

        public static void HandlePacket(GameClientMessageHandler handler, ClientMessage message)
        {

            if (Incoming.ContainsKey(message.Id))
            {
                if (Azure.Debugmode)
                    Logging.WriteLine(string.Format("[Incoming] Handled  {0}", message), ConsoleColor.Green);
                var staticRequestHandler = Incoming[message.Id];
                staticRequestHandler(handler);
            }
            else if(Azure.Debugmode)
                Logging.WriteLine(string.Format("[Incoming] Refused  {0}", message), ConsoleColor.Red);
        }

        internal static void ReloadDictionarys()
        {
            Incoming.Clear();
            Outgoing.Clear();
            Library.Clear();
            Config.Clear();
        }

        internal static void RegisterIncoming()
        {
            CountReleases = 0;
            var filePaths = Directory.GetFiles(string.Format("{0}\\Packets", Application.StartupPath), "*.incoming");
            foreach (var fileContents in filePaths.Select(currentFile => File.ReadAllLines(currentFile, Encoding.UTF8)))
            {
                CountReleases++;
                foreach (var fields in fileContents.Select(line => line.Replace(" ", string.Empty).Split('=')))
                {
                    var packetName = fields[0];
                    if (fields[1].Contains('/')) // anti comments
                        fields[1] = fields[1].Split('/')[0];

                    var packetId = fields[1].ToLower().Contains('x') ? Convert.ToInt32(fields[1], 16) : Convert.ToInt32(fields[1]);
                    if (!Library.ContainsKey(packetName))
                        continue;
                    var libValue = Library[packetName];
                    var del =
                        (PacketLibrary.GetProperty)
                            Delegate.CreateDelegate(typeof(PacketLibrary.GetProperty), typeof(PacketLibrary),
                                libValue);
                    if (Incoming.ContainsKey(packetId))
                        Console.WriteLine(
                            "> A Incoming Packet with Same Id was Founded: " + packetId);
                    else
                        Incoming.Add(packetId, new StaticRequestHandler(del));
                }
            }
        }

        internal static void RegisterConfig()
        {
            var filePaths = Directory.GetFiles(string.Format("{0}\\Packets", Application.StartupPath), "*.inf");
            foreach (var fields in filePaths.Select(File.ReadAllLines).SelectMany(fileContents => fileContents.Select(line => line.Split('='))))
            {
                if (fields[1].Contains('/')) // anti comments
                    fields[1] = fields[1].Split('/')[0];

                Config.Add(fields[0], fields[1]);
            }
        }

        internal static void RegisterOutgoing()
        {
            var filePaths = Directory.GetFiles(string.Format("{0}\\Packets", Application.StartupPath), "*.outgoing");
            foreach (var fields in filePaths.Select(File.ReadAllLines).SelectMany(fileContents => fileContents.Select(line => line.Replace(" ", string.Empty).Split('='))))
            {
                if (fields[1].Contains('/')) // anti comments
                    fields[1] = fields[1].Split('/')[0];

                var packetName = fields[0];
                var packetId = int.Parse(fields[1]);
                Outgoing.Add(packetName, packetId);
            }
        }

        internal static void RegisterLibrary()
        {
            var filePaths = Directory.GetFiles(string.Format("{0}\\Packets", Application.StartupPath), "*.library");
            foreach (var fields in filePaths.Select(File.ReadAllLines).SelectMany(fileContents => fileContents.Select(line => line.Split('='))))
            {
                if (fields[1].Contains('/')) // anti comments
                    fields[1] = fields[1].Split('/')[0];

                var incomingName = fields[0];
                var libraryName = fields[1];
                Library.Add(incomingName, libraryName);
            }
        }
    }
}