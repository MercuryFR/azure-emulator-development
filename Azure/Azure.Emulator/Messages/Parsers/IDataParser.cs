using System;

namespace Azure.Messages.Parsers
{
    public interface IDataParser : IDisposable, ICloneable
    {
        void HandlePacketData(byte[] packet);
    }
}