﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.Encryption;
using Azure.Encryption.Hurlant.Crypto.Prng;
using Azure.HabboHotel.Achievements;
using Azure.HabboHotel.Catalogs;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Groups;
using Azure.HabboHotel.Guides;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Navigators;
using Azure.HabboHotel.PathFinding;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.Polls;
using Azure.HabboHotel.Quests;
using Azure.HabboHotel.Quests.Composer;
using Azure.HabboHotel.RoomBots;
using Azure.HabboHotel.Rooms;
using Azure.HabboHotel.Rooms.Wired;
using Azure.HabboHotel.SoundMachine;
using Azure.HabboHotel.SoundMachine.Composers;
using Azure.HabboHotel.Support;
using Azure.HabboHotel.Users.Badges;
using Azure.HabboHotel.Users.Messenger;
using Azure.HabboHotel.Users.Relationships;
using Azure.HabboHotel.YouTube;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.Messages.Handlers
{
    internal partial class GameClientMessageHandler
    {
        internal void InitMessenger()
        {
            this.Session.GetHabbo().InitMessenger();
        }

        internal void FriendsListUpdate()
        {
            this.Session.GetHabbo().GetMessenger();
        }

        internal void RemoveBuddy()
        {
            if (this.Session.GetHabbo().GetMessenger() == null)
            {
                return;
            }
            int num = this.Request.GetInteger();
            checked
            {
                for (int i = 0; i < num; i++)
                {
                    uint num2 = this.Request.GetUInteger();
                    if (this.Session.GetHabbo().Relationships.ContainsKey(Convert.ToInt32(num2)))
                    {
                        this.Session.SendNotif("Could not remove someone, they are set to your relationship!");
                    }
                    else
                    {
                        this.Session.GetHabbo().GetMessenger().DestroyFriendship(num2);
                    }
                }
            }
        }

        internal void SearchHabbo()
        {
            if (this.Session.GetHabbo().GetMessenger() == null)
            {
                return;
            }
            this.Session.SendMessage(this.Session.GetHabbo().GetMessenger().PerformSearch(this.Request.GetString()));
        }

        internal void AcceptRequest()
        {
            if (this.Session.GetHabbo().GetMessenger() == null)
            {
                return;
            }
            int num = this.Request.GetInteger();
            checked
            {
                for (int i = 0; i < num; i++)
                {
                    uint num2 = this.Request.GetUInteger();
                    MessengerRequest request = this.Session.GetHabbo().GetMessenger().GetRequest(num2);
                    if (request != null)
                    {
                        if (request.To != this.Session.GetHabbo().Id)
                        {
                            return;
                        }
                        if (!this.Session.GetHabbo().GetMessenger().FriendshipExists(request.To))
                        {
                            this.Session.GetHabbo().GetMessenger().CreateFriendship(request.From);
                        }
                        this.Session.GetHabbo().GetMessenger().HandleRequest(num2);
                    }
                }
            }
        }

        internal void DeclineRequest()
        {
            if (this.Session.GetHabbo().GetMessenger() == null)
            {
                return;
            }
            bool flag = this.Request.GetBool();
            this.Request.GetInteger();
            if (!flag)
            {
                uint sender = this.Request.GetUInteger();
                this.Session.GetHabbo().GetMessenger().HandleRequest(sender);
                return;
            }
            this.Session.GetHabbo().GetMessenger().HandleAllRequests();
        }

        internal void RequestBuddy()
        {
            if (this.Session.GetHabbo().GetMessenger() == null)
            {
                return;
            }
            if (this.Session.GetHabbo().GetMessenger().RequestBuddy(this.Request.GetString()))
            {
                global::Azure.Azure.GetGame().GetQuestManager().ProgressUserQuest(this.Session, QuestType.SocialFriend, 0u);
            }
        }

        internal void SendInstantMessenger()
        {
            uint toId = this.Request.GetUInteger();
            string text = this.Request.GetString();
            if (this.Session.GetHabbo().GetMessenger() == null)
            {
                return;
            }
            if (!string.IsNullOrWhiteSpace(text))
            {
                this.Session.GetHabbo().GetMessenger().SendInstantMessage(toId, text);
            }
        }

        internal void FollowBuddy()
        {
            uint userID = this.Request.GetUInteger();
            GameClient clientByUserID = global::Azure.Azure.GetGame().GetClientManager().GetClientByUserId(userID);

            if (clientByUserID != null && clientByUserID.GetHabbo() != null)
            {
                if (clientByUserID.GetHabbo().GetMessenger() == null || clientByUserID.GetHabbo().CurrentRoom == null)
                {
                    if (this.Session.GetHabbo().GetMessenger() != null)
                    {
                        this.Response.Init(LibraryParser.OutgoingRequest("FollowFriendErrorMessageComposer"));
                        this.Response.AppendInteger(2);
                        this.SendResponse();
                        this.Session.GetHabbo().GetMessenger().UpdateFriend(userID, clientByUserID, true);
                    }
                    return;
                }
                else if (this.Session.GetHabbo().GetMessenger() != null && !this.Session.GetHabbo().GetMessenger().FriendshipExists(userID))
                {
                    this.Response.Init(LibraryParser.OutgoingRequest("FollowFriendErrorMessageComposer"));
                    this.Response.AppendInteger(0);
                    this.SendResponse();
                    return;
                }

                var roomFwd = new ServerMessage(LibraryParser.OutgoingRequest("RoomForwardMessageComposer"));
                roomFwd.AppendInteger(clientByUserID.GetHabbo().CurrentRoom.RoomId);
                this.Session.SendMessage(roomFwd);
            }
        }

        internal void SendInstantInvite()
        {
            int num = this.Request.GetInteger();
            var list = new List<uint>();
            checked
            {
                for (int i = 0; i < num; i++)
                {
                    list.Add(this.Request.GetUInteger());
                }
                string s = this.Request.GetString();
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("ConsoleInvitationMessageComposer"));
                serverMessage.AppendInteger(this.Session.GetHabbo().Id);
                serverMessage.AppendString(s);
                foreach (uint current in list)
                {
                    if (this.Session.GetHabbo().GetMessenger().FriendshipExists(current))
                    {
                        GameClient clientByUserID = global::Azure.Azure.GetGame().GetClientManager().GetClientByUserId(current);
                        if (clientByUserID == null)
                        {
                            break;
                        }
                        clientByUserID.SendMessage(serverMessage);
                    }
                }
            }
        }
    }
}
