﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.Encryption;
using Azure.Encryption.Hurlant.Crypto.Prng;
using Azure.HabboHotel.Achievements;
using Azure.HabboHotel.Catalogs;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Groups;
using Azure.HabboHotel.Guides;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Navigators;
using Azure.HabboHotel.PathFinding;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.Polls;
using Azure.HabboHotel.Quests;
using Azure.HabboHotel.Quests.Composer;
using Azure.HabboHotel.RoomBots;
using Azure.HabboHotel.Rooms;
using Azure.HabboHotel.Rooms.Wired;
using Azure.HabboHotel.SoundMachine;
using Azure.HabboHotel.SoundMachine.Composers;
using Azure.HabboHotel.Support;
using Azure.HabboHotel.Users.Badges;
using Azure.HabboHotel.Users.Messenger;
using Azure.HabboHotel.Users.Relationships;
using Azure.HabboHotel.YouTube;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.Messages.Handlers
{
    internal partial class GameClientMessageHandler
    {
        internal void GetInventory()
        {
            var queuedServerMessage = new QueuedServerMessage(this.Session.GetConnection());
            queuedServerMessage.AppendResponse(this.Session.GetHabbo().GetInventoryComponent().SerializeFloorItemInventory());
            queuedServerMessage.SendResponse();
        }

    }
}
