﻿using System.Collections.Generic;
using Azure.HabboHotel.Support;
using Azure.Messages.Parsers;

namespace Azure.Messages.Handlers
{
    partial class GameClientMessageHandler
    {
        internal void InitHelpTool()
        {
            Response.Init(LibraryParser.OutgoingRequest("OpenHelpToolMessageComposer"));
            Response.AppendInteger(0);
            SendResponse();
        }

        internal void SubmitHelpTicket()
        {
            if (Azure.GetGame().GetModerationTool().UsersHasPendingTicket(Session.GetHabbo().Id))
                return;
            var message = Request.GetString();
            var category = Request.GetInteger();
            var reportedUser = Request.GetUInteger();
            Request.GetUInteger(); // roomId

            var messageCount = Request.GetInteger();

            var chats = new List<string>();

            for (var i = 0; i < messageCount; i++)
            {
                Request.GetInteger();
                chats.Add(Request.GetString());
            }
            Azure.GetGame()
                .GetModerationTool()
                .SendNewTicket(Session, category, reportedUser, message, 7, chats);
            Response.Init(LibraryParser.OutgoingRequest("TicketUserAlert"));
            Response.AppendInteger(0);
            SendResponse();
        }

        internal void DeletePendingCfh()
        {
            if (!Azure.GetGame().GetModerationTool().UsersHasPendingTicket(Session.GetHabbo().Id))
                return;
            Azure.GetGame().GetModerationTool().DeletePendingTicketForUser(Session.GetHabbo().Id);
        }

        internal void ModGetUserInfo()
        {
            if (!Session.GetHabbo().HasFuse("fuse_mod"))
                return;
            var num = Request.GetUInteger();
            if (Azure.GetGame().GetClientManager().GetNameById(num) != "Unknown User")
            {
                Session.SendMessage(ModerationTool.SerializeUserInfo(num));
                return;
            }
            Session.SendNotif("No se pudo cargar la información del usuario.");
        }

        internal void ModGetUserChatlog()
        {
            if (!Session.GetHabbo().HasFuse("fuse_chatlogs"))
                return;
            Session.SendMessage(ModerationTool.SerializeUserChatlog(Request.GetUInteger()));
        }

        internal void ModGetRoomChatlog()
        {
            if (!Session.GetHabbo().HasFuse("fuse_chatlogs"))
            {
                Session.SendBroadcastMessage("No puedes ver chatlogs. No estás autorizado");
                return;
            }
            Request.GetInteger();
            var roomId = Request.GetUInteger();
            if (Azure.GetGame().GetRoomManager().GetRoom(roomId) != null)
                Session.SendMessage(ModerationTool.SerializeRoomChatlog(roomId));
        }

        internal void ModGetRoomTool()
        {
            if (!Session.GetHabbo().HasFuse("fuse_mod"))
                return;
            var roomId = Request.GetUInteger();
            var data = Azure.GetGame().GetRoomManager().GenerateNullableRoomData(roomId);
            Session.SendMessage(ModerationTool.SerializeRoomTool(data));
        }

        internal void ModPickTicket()
        {
            if (!Session.GetHabbo().HasFuse("fuse_mod"))
                return;
            Request.GetInteger();
            var ticketId = Request.GetUInteger();
            Azure.GetGame().GetModerationTool().PickTicket(Session, ticketId);
        }

        internal void ModReleaseTicket()
        {
            if (!Session.GetHabbo().HasFuse("fuse_mod"))
                return;
            var num = Request.GetInteger();
            checked
            {
                for (var i = 0; i < num; i++)
                {
                    var ticketId = Request.GetUInteger();
                    Azure.GetGame().GetModerationTool().ReleaseTicket(Session, ticketId);
                }
            }
        }

        internal void ModCloseTicket()
        {
            if (!Session.GetHabbo().HasFuse("fuse_mod"))
                return;
            var result = Request.GetInteger();
            Request.GetInteger();
            var ticketId = Request.GetUInteger();
            Azure.GetGame().GetModerationTool().CloseTicket(Session, ticketId, result);
        }

        internal void ModGetTicketChatlog()
        {
            if (!Session.GetHabbo().HasFuse("fuse_mod"))
                return;
            var ticket = Azure.GetGame().GetModerationTool().GetTicket(Request.GetUInteger());
            if (ticket == null)
                return;
            var roomData = Azure.GetGame().GetRoomManager().GenerateNullableRoomData(ticket.RoomId);
            if (roomData == null)
                return;
            Session.SendMessage(ModerationTool.SerializeTicketChatlog(ticket, roomData, ticket.Timestamp));
        }

        internal void ModGetRoomVisits()
        {
            if (!Session.GetHabbo().HasFuse("fuse_mod"))
                return;
            var userId = Request.GetUInteger();
            Session.SendMessage(ModerationTool.SerializeRoomVisits(userId));
        }

        internal void ModSendRoomAlert()
        {
            if (!Session.GetHabbo().HasFuse("fuse_alert"))
                return;
            Request.GetInteger();
            var str = Request.GetString();
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
            serverMessage.AppendString("admin");
            serverMessage.AppendInteger(3);
            serverMessage.AppendString("message");
            serverMessage.AppendString(string.Format("{0}\r\n\r\n- {1}", str, Session.GetHabbo().UserName));
            serverMessage.AppendString("link");
            serverMessage.AppendString("event:");
            serverMessage.AppendString("linkTitle");
            serverMessage.AppendString("ok");
            Session.GetHabbo().CurrentRoom.SendMessage(serverMessage);
        }

        internal void ModPerformRoomAction()
        {
            if (!Session.GetHabbo().HasFuse("fuse_mod"))
                return;
            var roomId = Request.GetUInteger();
            var lockRoom = Request.GetInteger() == 1;
            var inappropriateRoom = Request.GetInteger() == 1;
            var kickUsers = Request.GetInteger() == 1;
            ModerationTool.PerformRoomAction(Session, roomId, kickUsers, lockRoom, inappropriateRoom, Response);
        }

        internal void ModSendUserCaution()
        {
            if (!Session.GetHabbo().HasFuse("fuse_alert"))
                return;
            var userId = Request.GetUInteger();
            var message = Request.GetString();
            ModerationTool.AlertUser(Session, userId, message, true);
        }

        internal void ModSendUserMessage()
        {
            if (!Session.GetHabbo().HasFuse("fuse_alert"))
                return;
            var userId = Request.GetUInteger();
            var message = Request.GetString();
            ModerationTool.AlertUser(Session, userId, message, false);
        }

        internal void ModMuteUser()
        {
            if (!Session.GetHabbo().HasFuse("fuse_mute"))
                return;
            var userId = Request.GetUInteger();
            var message = Request.GetString();
            var clientByUserId = Azure.GetGame().GetClientManager().GetClientByUserId(userId);
            clientByUserId.GetHabbo().Mute();
            clientByUserId.SendNotif(message);
        }

        internal void ModLockTrade()
        {
            if (!Session.GetHabbo().HasFuse("fuse_lock_trade"))
                return;
            var userId = Request.GetUInteger();
            var message = Request.GetString();
            var length = checked(Request.GetInteger() * 3600);
            ModerationTool.LockTrade(Session, userId, message, length);
        }

        internal void ModKickUser()
        {
            if (!Session.GetHabbo().HasFuse("fuse_kick"))
                return;
            var userId = Request.GetUInteger();
            var message = Request.GetString();
            ModerationTool.KickUser(Session, userId, message, false);
        }

        internal void ModBanUser()
        {
            if (!Session.GetHabbo().HasFuse("fuse_ban"))
                return;
            var userId = Request.GetUInteger();
            var message = Request.GetString();
            var length = checked(Request.GetInteger() * 3600);
            ModerationTool.BanUser(Session, userId, length, message);
        }
    }
}