﻿using Azure.HabboHotel.Navigators;
using Azure.HabboHotel.Rooms;
using Azure.Messages.Parsers;

namespace Azure.Messages.Handlers
{
    internal partial class GameClientMessageHandler
    {
        internal void GetFlatCats()
        {
            if (this.Session.GetHabbo() == null)
                return;
            this.Session.SendMessage(global::Azure.Azure.GetGame().GetNavigator().SerializeFlatCategories(this.Session));
        }

        internal void EnterInquiredRoom()
        {
        }
        internal void GetPub()
        {
            uint roomId = this.Request.GetUInteger();
            RoomData roomData = global::Azure.Azure.GetGame().GetRoomManager().GenerateRoomData(roomId);
            if (roomData == null)
                return;
            this.GetResponse().Init(LibraryParser.OutgoingRequest("453"));
            this.GetResponse().AppendInteger(roomData.Id);
            this.GetResponse().AppendString(roomData.CCTs);
            this.GetResponse().AppendInteger(roomData.Id);
            this.SendResponse();
        }

        internal void OpenPub()
        {
            this.Request.GetInteger();
            uint roomId = this.Request.GetUInteger();
            this.Request.GetInteger();
            RoomData roomData = global::Azure.Azure.GetGame().GetRoomManager().GenerateRoomData(roomId);
            if (roomData == null)
                return;
            this.PrepareRoomForUser(roomData.Id, "");
        }

        internal void NewNavigator()
        {
            if (this.Session == null)
                return;
            Azure.GetGame().GetNavigator().EnableNewNavigator(this.Session);
        }

        internal void SearchNewNavigator()
        {
            if (this.Session == null)
                return;
            string name = this.Request.GetString();
            string junk = this.Request.GetString();
            Session.SendMessage(global::Azure.Azure.GetGame().GetNavigator().SerlializeNewNavigator(name, junk, this.Session));
        }

        internal void SavedSearch()
        {
            if (this.Session.GetHabbo().NavigatorLogs.Count > 50)
            {
                this.Session.SendNotif(Azure.GetLanguage().GetVar("navigator_max"));
                return;
            }
            string value1 = this.Request.GetString();
            string value2 = this.Request.GetString();
            var naviLogs = new NaviLogs(this.Session.GetHabbo().NavigatorLogs.Count, value1, value2);
            if (!this.Session.GetHabbo().NavigatorLogs.ContainsKey(naviLogs.Id))
                this.Session.GetHabbo().NavigatorLogs.Add(naviLogs.Id, naviLogs);
            var Message = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorSavedSearchesComposer"));
            Message.AppendInteger(this.Session.GetHabbo().NavigatorLogs.Count);
            foreach (NaviLogs navi in this.Session.GetHabbo().NavigatorLogs.Values)
            {
                Message.AppendInteger(navi.Id);
                Message.AppendString(navi.Value1);
                Message.AppendString(navi.Value2);
                Message.AppendString("");
            }
            this.Session.SendMessage(Message);
        }

        internal void SerializeSavedSearch(string textOne, string textTwo)
        {
            this.GetResponse().AppendString(textOne);
            this.GetResponse().AppendString(textTwo);
            this.GetResponse().AppendString("fr");
        }

        internal void NewNavigatorResize()
        {
            int x = this.Request.GetInteger();
            int y = this.Request.GetInteger();
            int width = this.Request.GetInteger();
            int height = this.Request.GetInteger();
        }

        internal void NewNavigatorAddSavedSearch()
        {
            this.SavedSearch();
        }

        internal void NewNavigatorDeleteSavedSearch()
        {
            int searchId = this.Request.GetInteger();
            if (!this.Session.GetHabbo().NavigatorLogs.ContainsKey(searchId))
                return;
            this.Session.GetHabbo().NavigatorLogs.Remove(searchId);
            var Message = new ServerMessage(LibraryParser.OutgoingRequest("NavigatorSavedSearchesComposer"));
            Message.AppendInteger(this.Session.GetHabbo().NavigatorLogs.Count);
            foreach (NaviLogs navi in this.Session.GetHabbo().NavigatorLogs.Values)
            {
                Message.AppendInteger(navi.Id);
                Message.AppendString(navi.Value1);
                Message.AppendString(navi.Value2);
                Message.AppendString("");
            }
            this.Session.SendMessage(Message);
        }

        internal void NewNavigatorCollapseCategory()
        {
            this.Request.GetString();
        }

        internal void NewNavigatorUncollapseCategory()
        {
            this.Request.GetString();
        }

        internal void GetPubs()
        {
            if (this.Session.GetHabbo() == null)
                return;
            this.Session.SendMessage(global::Azure.Azure.GetGame().GetNavigator().SerializePublicRooms());
        }

        internal void GetRoomInfo()
        {
            if (this.Session.GetHabbo() == null)
                return;
            uint roomId = this.Request.GetUInteger();
            this.Request.GetBool();
            this.Request.GetBool();
            RoomData roomData = global::Azure.Azure.GetGame().GetRoomManager().GenerateRoomData(roomId);
            if (roomData == null)
                return;
            this.GetResponse().Init(LibraryParser.OutgoingRequest("1491"));
            this.GetResponse().AppendInteger(0);
            roomData.Serialize(this.GetResponse(), false);
            this.SendResponse();
        }

        internal void GetPopularRooms()
        {
            if (this.Session.GetHabbo() == null)
                return;
            Session.SendMessage(Azure.GetGame().GetNavigator().SerializeNavigator(Session, int.Parse(Request.GetString())));
        }

        internal void GetRecommendedRooms()
        {
            if (this.Session.GetHabbo() == null)
                return;
            Session.SendMessage(Azure.GetGame().GetNavigator().SerializeNavigator(Session, -1));
        }

        internal void GetPopularGroups()
        {
            if (this.Session.GetHabbo() == null)
                return;
            Session.SendMessage(Azure.GetGame().GetNavigator().SerializeNavigator(Session, -2));
        }

        internal void GetHighRatedRooms()
        {
            if (this.Session.GetHabbo() == null)
                return;
            Session.SendMessage(Azure.GetGame().GetNavigator().SerializeNavigator(Session, -2));
        }

        internal void GetFriendsRooms()
        {
            if (this.Session.GetHabbo() == null)
                return;
            Session.SendMessage(Azure.GetGame().GetNavigator().SerializeNavigator(Session, -4));
        }

        internal void GetRoomsWithFriends()
        {
            if (this.Session.GetHabbo() == null)
                return;
            Session.SendMessage(Azure.GetGame().GetNavigator().SerializeNavigator(Session, -5));
        }

        internal void GetOwnRooms()
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            if (Session.GetHabbo().OwnRoomsSerialized == false)
            {
                Session.GetHabbo().UpdateRooms();
                Session.GetHabbo().OwnRoomsSerialized = true;
            }

            Session.SendMessage(Azure.GetGame().GetNavigator().SerializeNavigator(Session, -3));
        }

        internal void NewNavigatorFlatCats()
        {
            if (this.Session.GetHabbo() == null)
                return;
            this.Session.SendMessage(global::Azure.Azure.GetGame().GetNavigator().SerializeNewFlatCategories());
        }

        internal void GetFavoriteRooms()
        {
            if (this.Session.GetHabbo() == null)
                return;
            this.Session.SendMessage(global::Azure.Azure.GetGame().GetNavigator().SerializeFavoriteRooms(this.Session));
        }

        internal void GetRecentRooms()
        {
            if (this.Session.GetHabbo() == null)
                return;
            this.Session.SendMessage(global::Azure.Azure.GetGame().GetNavigator().SerializeRecentRooms(this.Session));
        }

        internal void GetPopularTags()
        {
            if (this.Session.GetHabbo() == null)
                return;
            this.Session.SendMessage(global::Azure.Azure.GetGame().GetNavigator().SerializePopularRoomTags());
        }

        internal void GetEventRooms()
        {
            if (this.Session.GetHabbo() == null)
                return;
            Session.SendMessage(Navigator.SerializePromoted(Session, Request.GetInteger()));
        }

        internal void PerformSearch()
        {
            if (this.Session.GetHabbo() == null)
                return;
            Session.SendMessage(
                Navigator.SerializeSearchResults(Request.GetString()));
        }

        internal void SearchByTag()
        {
            if (this.Session.GetHabbo() == null)
                return;
            //this.Session.SendMessage(MercuryEnvironment.GetGame().GetNavigator().SerializeSearchResults(string.Format("tag:{0}", this.Request.PopFixedString())));
        }

        internal void PerformSearch2()
        {
            if (this.Session.GetHabbo() == null)
                return;
            this.Request.GetInteger();
            //this.Session.SendMessage(MercuryEnvironment.GetGame().GetNavigator().SerializeSearchResults(this.Request.PopFixedString()));
        }

        internal void OpenFlat()
        {
            if (this.Session.GetHabbo() == null)
                return;
            uint num = this.Request.GetUInteger();
            string password = this.Request.GetString();

            this.PrepareRoomForUser(num, password);
        }
    }
}
