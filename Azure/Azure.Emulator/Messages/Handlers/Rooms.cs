﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Azure.Configuration;
using Azure.HabboHotel.Catalogs;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Navigators;
using Azure.HabboHotel.PathFinding;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.Polls;
using Azure.HabboHotel.Quests;
using Azure.HabboHotel.RoomBots;
using Azure.HabboHotel.Rooms;
using Azure.Messages.Parsers;
using Azure.Util;

namespace Azure.Messages.Handlers
{
    partial class GameClientMessageHandler
    {
        public void GetPetBreeds()
        {
            var type = Request.GetString();
            string petType;
            var petId = PetRace.GetPetId(type, out petType);
            var races = PetRace.GetRacesForRaceId(petId);
            var message = new ServerMessage(LibraryParser.OutgoingRequest("SellablePetBreedsMessageComposer"));
            message.AppendString(petType);
            message.AppendInteger(races.Count);
            foreach (var current in races)
            {
                message.AppendInteger(petId);
                message.AppendInteger(current.Color1);
                message.AppendInteger(current.Color2);
                message.AppendBool(current.Has1Color);
                message.AppendBool(current.Has2Color);
            }
            Session.SendMessage(message);
        }

        internal void GoRoom()
        {
            if (Session.GetHabbo() == null)
                return;
            var num = Request.GetUInteger();
            var roomData = Azure.GetGame().GetRoomManager().GenerateRoomData(num);
            Session.GetHabbo().GetInventoryComponent().RunDbUpdate();
            if (roomData == null || roomData.Id == Session.GetHabbo().CurrentRoomId)
                return;
            roomData.SerializeRoomData(Response, !Session.GetHabbo().InRoom, Session, false);
            PrepareRoomForUser(num, roomData.Password);
        }

        internal void AddFavorite()
        {
            if (Session.GetHabbo() == null)
                return;

            var num = Request.GetUInteger();

            GetResponse().Init(LibraryParser.OutgoingRequest("FavouriteRoomsUpdateMessageComposer"));
            GetResponse().AppendInteger(num);
            GetResponse().AppendBool(true);
            SendResponse();

            Session.GetHabbo().FavoriteRooms.Add(num);
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "INSERT INTO users_favorites (user_id,room_id) VALUES (",
                    Session.GetHabbo().Id,
                    ",",
                    num,
                    ")"
                }));
        }

        internal void RemoveFavorite()
        {
            if (Session.GetHabbo() == null)
                return;
            var num = Request.GetUInteger();
            Session.GetHabbo().FavoriteRooms.Remove(num);
            GetResponse().Init(LibraryParser.OutgoingRequest("FavouriteRoomsUpdateMessageComposer"));
            GetResponse().AppendInteger(num);
            GetResponse().AppendBool(false);
            SendResponse();
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "DELETE FROM users_favorites WHERE user_id = ",
                    Session.GetHabbo().Id,
                    " AND room_id = ",
                    num
                }));
        }

        internal void OnlineConfirmationEvent()
        {
            Logging.WriteLine(string.Format("[UserMgr] [ {0} ] is now online.", Request.GetString()), ConsoleColor.Green);
        }

        internal void GoToHotelView()
        {
            var hotelView = Azure.GetGame().GetHotelView();
            if (hotelView.FurniRewardName != null)
            {
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("LandingRewardMessageComposer"));
                serverMessage.AppendString(hotelView.FurniRewardName);
                serverMessage.AppendInteger(hotelView.FurniRewardId);
                serverMessage.AppendInteger(120);
                serverMessage.AppendInteger(checked(120 - Session.GetHabbo().Respect));
                Session.SendMessage(serverMessage);
            }
            if (Session == null || Session.GetHabbo() == null)
                return;
            if (!Session.GetHabbo().InRoom)
                return;
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room != null)
                room.GetRoomUserManager().RemoveUserFromRoom(Session, true, false);
            Session.CurrentRoomUserId = -1;
        }

        internal void LandingCommunityGoal()
        {
            var onlineFriends = Session.GetHabbo().GetMessenger().Friends.Count(x => x.Value.IsOnline);
            var goalMeter = new ServerMessage(LibraryParser.OutgoingRequest("LandingCommunityChallengeMessageComposer"));
            goalMeter.AppendBool(true); //
            goalMeter.AppendInteger(0); //points
            goalMeter.AppendInteger(0); //my rank
            goalMeter.AppendInteger(onlineFriends); //totalAmount
            goalMeter.AppendInteger(onlineFriends >= 20 ? 1 : onlineFriends >= 50 ? 2 : onlineFriends >= 80 ? 3 : 0);
                //communityHighestAchievedLevel
            goalMeter.AppendInteger(0); //scoreRemainingUntilNextLevel
            goalMeter.AppendInteger(0); //percentCompletionTowardsNextLevel
            goalMeter.AppendString("friendshipChallenge"); //Type
            goalMeter.AppendInteger(0); //unknown
            goalMeter.AppendInteger(0); //ranks and loop
            Session.SendMessage(goalMeter);
        }

        internal void RequestFloorItems() { }

        internal void RequestWallItems() { }

        internal void SaveBranding()
        {
            var pId = Request.GetUInteger();
            var num = Request.GetUInteger();
            var text = string.Format("state{0}0", Convert.ToChar(9));
            var num2 = 1;
            while (num2 <= num)
            {
                text = string.Format("{0}{1}{2}", text, Convert.ToChar(9), Request.GetString());
                checked
                {
                    num2++;
                }
            }
            var currentRoom = Session.GetHabbo().CurrentRoom;
            var item = currentRoom.GetRoomItemHandler().GetItem(pId);
            item.ExtraData = text;
            currentRoom.GetRoomItemHandler()
                .SetFloorItem(Session, item, item.X, item.Y, item.Rot, false, false, true);
        }

        internal void OnRoomUserAdd()
        {
            if (Session == null || GetResponse() == null)
                return;
            var queuedServerMessage = new QueuedServerMessage(Session.GetConnection());
            if (CurrentLoadingRoom == null || CurrentLoadingRoom.GetRoomUserManager() == null ||
                CurrentLoadingRoom.GetRoomUserManager().UserList == null)
                return;
            var list =
                CurrentLoadingRoom.GetRoomUserManager()
                    .UserList.Values.Where(current => current != null && !current.IsSpectator)
                    .ToList();
            Response.Init(LibraryParser.OutgoingRequest("SetRoomUserMessageComposer"));
            Response.AppendInteger(list.Count);
            foreach (var current2 in list)
                current2.Serialize(Response, CurrentLoadingRoom.GetGameMap().GotPublicPool);
            queuedServerMessage.AppendResponse(GetResponse());
            queuedServerMessage.AppendResponse(RoomFloorAndWallComposer(CurrentLoadingRoom));
            queuedServerMessage.AppendResponse(GetResponse());
            Response.Init(LibraryParser.OutgoingRequest("RoomOwnershipMessageComposer"));
            Response.AppendInteger(CurrentLoadingRoom.RoomId);
            Response.AppendBool(CurrentLoadingRoom.CheckRights(Session, true, false));
            queuedServerMessage.AppendResponse(GetResponse());
            foreach (var habboForId in CurrentLoadingRoom.UsersWithRights.Select(Azure.GetHabboForId1).Where(habboForId => true))
            {
                GetResponse().Init(LibraryParser.OutgoingRequest("GiveRoomRightsMessageComposer"));
                GetResponse().AppendInteger(CurrentLoadingRoom.RoomId);
                GetResponse().AppendInteger(habboForId.Id);
                GetResponse().AppendString(habboForId.UserName);
                queuedServerMessage.AppendResponse(GetResponse());
            }
            var serverMessage = CurrentLoadingRoom.GetRoomUserManager().SerializeStatusUpdates(true);
            if (serverMessage != null)
                queuedServerMessage.AppendResponse(serverMessage);
            if (CurrentLoadingRoom.Event != null)
                Azure.GetGame().GetRoomEvents().SerializeEventInfo(CurrentLoadingRoom.RoomId);
            foreach (var current4 in CurrentLoadingRoom.GetRoomUserManager().UserList.Values.Where(current4 => current4 != null))
            {
                if (current4.IsBot)
                {
                    if (current4.BotData.DanceId > 0)
                    {
                        Response.Init(LibraryParser.OutgoingRequest("DanceStatusMessageComposer"));
                        Response.AppendInteger(current4.VirtualId);
                        Response.AppendInteger(current4.BotData.DanceId);
                        queuedServerMessage.AppendResponse(GetResponse());
                    }
                }
                else if (current4.IsDancing)
                {
                    Response.Init(LibraryParser.OutgoingRequest("DanceStatusMessageComposer"));
                    Response.AppendInteger(current4.VirtualId);
                    Response.AppendInteger(current4.DanceId);
                    queuedServerMessage.AppendResponse(GetResponse());
                }
                if (current4.IsAsleep)
                {
                    var sleepMsg = new ServerMessage(LibraryParser.OutgoingRequest("RoomUserIdleMessageComposer"));
                    sleepMsg.AppendInteger(current4.VirtualId);
                    sleepMsg.AppendBool(true);
                    queuedServerMessage.AppendResponse(sleepMsg);
                }
                if (current4.CarryItemId > 0 && current4.CarryTimer > 0)
                {
                    Response.Init(LibraryParser.OutgoingRequest("ApplyHanditemMessageComposer"));
                    Response.AppendInteger(current4.VirtualId);
                    Response.AppendInteger(current4.CarryTimer);
                    queuedServerMessage.AppendResponse(GetResponse());
                }
                if (current4.IsBot)
                    continue;
                try
                {
                    if (current4.GetClient() != null &&
                        current4.GetClient().GetHabbo() != null)
                    {
                        if (current4.GetClient().GetHabbo().GetAvatarEffectsInventoryComponent() != null &&
                            current4.CurrentEffect >= 1)
                        {
                            Response.Init(LibraryParser.OutgoingRequest("ApplyEffectMessageComposer"));
                            Response.AppendInteger(current4.VirtualId);
                            Response.AppendInteger(current4.CurrentEffect);
                            Response.AppendInteger(0);
                            queuedServerMessage.AppendResponse(GetResponse());
                        }
                        var serverMessage2 =
                            new ServerMessage(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                        serverMessage2.AppendInteger(current4.VirtualId);
                        serverMessage2.AppendString(current4.GetClient().GetHabbo().Look);
                        serverMessage2.AppendString(current4.GetClient().GetHabbo().Gender.ToLower());
                        serverMessage2.AppendString(current4.GetClient().GetHabbo().Motto);
                        serverMessage2.AppendInteger(current4.GetClient().GetHabbo().AchievementPoints);
                        if (CurrentLoadingRoom != null)
                            CurrentLoadingRoom.SendMessage(serverMessage2);
                    }
                }
                catch (Exception pException)
                {
                    Logging.HandleException(pException, "Rooms.SendRoomData3");
                }
            }
            queuedServerMessage.SendResponse();
        }

        internal void EnterOnRoom()
        {
            var id = Request.GetUInteger();
            var password = Request.GetString();
            PrepareRoomForUser(id, password);
        }


        private void Set_Timer(System.Timers.Timer timer)
        {
            timer.Interval = 3000;
            timer.Elapsed += Session.GetHabbo().timer_ElapsedEvent;
        }



        internal void PrepareRoomForUser(uint id, string pWd)
        {
            try
            {
                if (Session == null || Session.GetHabbo() == null)
                    return;
                var timer = new System.Timers.Timer();
                Set_Timer(timer);
                if (!timer.Enabled)
                {
                    timer.Start();
                }
                else
                {
                    if (!Session.GetHabbo().timer_Elapsed)
                    {
                        return;
                    }
                    timer.Stop();
                    Session.GetHabbo().timer_Elapsed = false;
                }                             
                ClearRoomLoading();
                var queuedServerMessage = new QueuedServerMessage(Session.GetConnection());
                if (Azure.ShutdownStarted)
                {
                    Session.SendNotif("The server is shutting down. What are you doing?");
                    return;
                }
                if (Session.GetHabbo().InRoom)
                {
                    var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
                    if (room != null && room.GetRoomUserManager() != null)
                        room.GetRoomUserManager().RemoveUserFromRoom(Session, false, false);
                }
                var room2 = Azure.GetGame().GetRoomManager().LoadRoom(id);
                if (room2 == null)
                    return;

                if (room2.UserCount >= room2.UsersMax && !Session.GetHabbo().HasFuse("fuse_enter_full_rooms") &&
                    Session.GetHabbo().Id != (ulong) room2.OwnerId)
                {
                    var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("RoomEnterErrorMessageComposer"));
                    serverMessage.AppendInteger(1);
                    Session.SendMessage(serverMessage);
                    var message = new ServerMessage(LibraryParser.OutgoingRequest("OutOfRoomMessageComposer"));
                    Session.SendMessage(message);
                    return;
                }
                if (Session == null || Session.GetHabbo() == null)
                    return;
                if (Session.GetHabbo().IsTeleporting && Session.GetHabbo().TeleportingRoomId != id)
                    return;

                Session.GetHabbo().LoadingRoom = id;
                CurrentLoadingRoom = room2;

                if (!Session.GetHabbo().HasFuse("fuse_enter_any_room") && room2.UserIsBanned(Session.GetHabbo().Id))
                {
                    if (!room2.HasBanExpired(Session.GetHabbo().Id))
                    {
                        var serverMessage2 =
                            new ServerMessage(LibraryParser.OutgoingRequest("RoomEnterErrorMessageComposer"));
                        serverMessage2.AppendInteger(4);
                        Session.SendMessage(serverMessage2);
                        Response.Init(LibraryParser.OutgoingRequest("OutOfRoomMessageComposer"));
                        queuedServerMessage.AppendResponse(GetResponse());
                        queuedServerMessage.SendResponse();
                        return;
                    }
                    room2.RemoveBan(Session.GetHabbo().Id);
                }
                Response.Init(LibraryParser.OutgoingRequest("PrepareRoomMessageComposer"));
                queuedServerMessage.AppendResponse(GetResponse());
                if (!Session.GetHabbo().HasFuse("fuse_enter_any_room") && !room2.CheckRights(Session, true, false) &&
                    !Session.GetHabbo().IsTeleporting && !Session.GetHabbo().IsHopping)
                {
                    if (room2.State == 1)
                    {
                        if (room2.UserCount == 0)
                        {
                            Response.Init(LibraryParser.OutgoingRequest("DoorbellNoOneMessageComposer"));
                            queuedServerMessage.AppendResponse(GetResponse());
                        }
                        else
                        {
                            Response.Init(LibraryParser.OutgoingRequest("DoorbellMessageComposer"));
                            Response.AppendString("");
                            queuedServerMessage.AppendResponse(GetResponse());
                            var serverMessage3 =
                                new ServerMessage(LibraryParser.OutgoingRequest("DoorbellMessageComposer"));
                            serverMessage3.AppendString(Session.GetHabbo().UserName);
                            room2.SendMessageToUsersWithRights(serverMessage3);
                        }
                        queuedServerMessage.SendResponse();
                        return;
                    }
                    if (room2.State == 2 &&
                        !String.Equals(pWd, room2.Password, StringComparison.CurrentCultureIgnoreCase))
                    {
                        Response.Init(LibraryParser.OutgoingRequest("OutOfRoomMessageComposer"));
                        queuedServerMessage.AppendResponse(GetResponse());
                        queuedServerMessage.SendResponse();
                        return;
                    }
                }

                Session.GetHabbo().LoadingChecksPassed = true;
                queuedServerMessage.AddBytes(LoadRoomForUser().GetPacket);
                queuedServerMessage.SendResponse();

                if (Session.GetHabbo().RecentlyVisitedRooms.Contains(room2.RoomId))
                    Session.GetHabbo().RecentlyVisitedRooms.Remove(room2.RoomId);
                Session.GetHabbo().RecentlyVisitedRooms.Add(room2.RoomId);

            }
            catch (Exception e)
            {
                Writer.Writer.LogException("PrepareRoomForUser. RoomId: " + id + "; UserId: " +
                                           (Session != null
                                               ? Session.GetHabbo().Id.ToString(CultureInfo.InvariantCulture)
                                               : "null") + Environment.NewLine + e);
            }
        }

        internal void ReqLoadRoomForUser() { LoadRoomForUser().SendResponse(); }

        internal QueuedServerMessage LoadRoomForUser()
        {
            var currentLoadingRoom = CurrentLoadingRoom;
            var queuedServerMessage = new QueuedServerMessage(Session.GetConnection());
            if (currentLoadingRoom == null || !Session.GetHabbo().LoadingChecksPassed)
                return queuedServerMessage;
            if (Session.GetHabbo().FavouriteGroup > 0u)
            {
                if (CurrentLoadingRoom.Group != null &&
                    !CurrentLoadingRoom.LoadedGroups.ContainsKey(CurrentLoadingRoom.Group.Id))
                    CurrentLoadingRoom.LoadedGroups.Add(CurrentLoadingRoom.Group.Id, CurrentLoadingRoom.Group.Badge);
                if (!CurrentLoadingRoom.LoadedGroups.ContainsKey(Session.GetHabbo().FavouriteGroup) &&
                    Azure.GetGame().GetGroupManager().GetGroup(Session.GetHabbo().FavouriteGroup) != null)
                    CurrentLoadingRoom.LoadedGroups.Add(Session.GetHabbo().FavouriteGroup,
                        Azure.GetGame().GetGroupManager().GetGroup(Session.GetHabbo().FavouriteGroup).Badge);
            }
            Response.Init(LibraryParser.OutgoingRequest("RoomGroupMessageComposer"));
            Response.AppendInteger(CurrentLoadingRoom.LoadedGroups.Count);
            foreach (var guild1 in CurrentLoadingRoom.LoadedGroups)
            {
                Response.AppendInteger(guild1.Key);
                Response.AppendString(guild1.Value);
            }
            queuedServerMessage.AppendResponse(GetResponse());
            Response.Init(LibraryParser.OutgoingRequest("InitialRoomInfoMessageComposer"));
            Response.AppendString(currentLoadingRoom.ModelName);
            Response.AppendInteger(currentLoadingRoom.RoomId);
            queuedServerMessage.AppendResponse(GetResponse());
            if (Session.GetHabbo().SpectatorMode)
            {
                Response.Init(LibraryParser.OutgoingRequest("SpectatorModeMessageComposer"));
                queuedServerMessage.AppendResponse(GetResponse());
            }
            if (currentLoadingRoom.Wallpaper != "0.0")
            {
                Response.Init(LibraryParser.OutgoingRequest("RoomSpacesMessageComposer"));
                Response.AppendString("wallpaper");
                Response.AppendString(currentLoadingRoom.Wallpaper);
                queuedServerMessage.AppendResponse(GetResponse());
            }
            if (currentLoadingRoom.Floor != "0.0")
            {
                Response.Init(LibraryParser.OutgoingRequest("RoomSpacesMessageComposer"));
                Response.AppendString("floor");
                Response.AppendString(currentLoadingRoom.Floor);
                queuedServerMessage.AppendResponse(GetResponse());
            }
            Response.Init(LibraryParser.OutgoingRequest("RoomSpacesMessageComposer"));
            Response.AppendString("landscape");
            Response.AppendString(currentLoadingRoom.Landscape);
            queuedServerMessage.AppendResponse(GetResponse());
            if (currentLoadingRoom.CheckRights(Session, true, false))
            {
                Response.Init(LibraryParser.OutgoingRequest("RoomRightsLevelMessageComposer"));
                Response.AppendInteger(4);
                queuedServerMessage.AppendResponse(GetResponse());
                Response.Init(LibraryParser.OutgoingRequest("HasOwnerRightsMessageComposer"));
                queuedServerMessage.AppendResponse(GetResponse());
            }
            else if (currentLoadingRoom.CheckRights(Session))
            {
                Response.Init(LibraryParser.OutgoingRequest("RoomRightsLevelMessageComposer"));
                Response.AppendInteger(1);
                queuedServerMessage.AppendResponse(GetResponse());
            }
            else
            {
                Response.Init(LibraryParser.OutgoingRequest("RoomRightsLevelMessageComposer"));
                Response.AppendInteger(0);
                queuedServerMessage.AppendResponse(GetResponse());
            }
            Response.Init(LibraryParser.OutgoingRequest("RoomRatingMessageComposer"));
            Response.AppendInteger(currentLoadingRoom.Score);
            Response.AppendBool(!Session.GetHabbo().RatedRooms.Contains(currentLoadingRoom.RoomId) &&
                                !currentLoadingRoom.CheckRights(Session, true, false));
            queuedServerMessage.AppendResponse(GetResponse());

            Response.Init(LibraryParser.OutgoingRequest("EnableRoomInfoMessageComposer"));
            Response.AppendInteger(currentLoadingRoom.RoomId);
            queuedServerMessage.AppendResponse(GetResponse());

            return queuedServerMessage;
        }

        internal void ClearRoomLoading()
        {
            Session.GetHabbo().LoadingRoom = 0u;
            Session.GetHabbo().LoadingChecksPassed = false;
        }

        internal void Move()
        {
            var currentRoom = Session.GetHabbo().CurrentRoom;
            if (currentRoom == null)
                return;
            var roomUserByHabbo = currentRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (roomUserByHabbo == null || !roomUserByHabbo.CanWalk)
                return;
            var num = Request.GetInteger();
            var num2 = Request.GetInteger();
            if (num == roomUserByHabbo.X && num2 == roomUserByHabbo.Y)
                return;
            roomUserByHabbo.MoveTo(num, num2);
            if (!roomUserByHabbo.RidingHorse)
                return;
            var roomUserByVirtualId =
                currentRoom.GetRoomUserManager().GetRoomUserByVirtualId(Convert.ToInt32(roomUserByHabbo.HorseId));
            roomUserByVirtualId.MoveTo(num, num2);
        }

        internal void CanCreateRoom()
        {
            Response.Init(LibraryParser.OutgoingRequest("CanCreateRoomMessageComposer"));
            Response.AppendInteger((Session.GetHabbo().UsersRooms.Count >= 75) ? 1 : 0);
            Response.AppendInteger(75);
            SendResponse();
        }

        internal void CreateRoom()
        {
            if (Session.GetHabbo().UsersRooms.Count >= 75)
            {
                Session.SendNotif("You can't have more than 75 rooms. Try to delete some rooms!");
                return;
            }
            if ((Azure.GetUnixTimestamp() - Session.GetHabbo().LastSqlQuery) < 20)
            {
                Session.SendNotif("Please wait 20 seconds before creating the next room!");
                return;
            }

            var name = Request.GetString();
            var description = Request.GetString();
            var roomModel = Request.GetString();
            var category = Request.GetInteger();
            var maxVisitors = Request.GetInteger();
            var tradeState = Request.GetInteger();

            var data = Azure.GetGame()
                .GetRoomManager()
                .CreateRoom(Session, name, description, roomModel, category, maxVisitors, tradeState);
            if (data == null)
                return;

            Session.GetHabbo().LastSqlQuery = Azure.GetUnixTimestamp();
            Response.Init(LibraryParser.OutgoingRequest("OnCreateRoomInfoMessageComposer"));
            Response.AppendInteger(data.Id);
            Response.AppendString(data.Name);
            SendResponse();
        }

        internal void GetRoomEditData()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Convert.ToUInt32(Request.GetInteger()));
            if (room == null)
                return;
     
            GetResponse().Init(LibraryParser.OutgoingRequest("RoomSettingsDataMessageComposer"));
            GetResponse().AppendInteger(room.RoomId);
            GetResponse().AppendString(room.Name);
            GetResponse().AppendString(room.Description);
            GetResponse().AppendInteger(room.State);
            GetResponse().AppendInteger(room.Category);
            GetResponse().AppendInteger(room.UsersMax);
            GetResponse()
                .AppendInteger((checked(room.RoomData.Model.MapSizeX * room.RoomData.Model.MapSizeY) > 200) ? 50 : 25);
            GetResponse().AppendInteger(room.TagCount);
            var tags = room.Tags.ToArray();
            foreach (var s in tags.Cast<string>())
            {
                GetResponse().AppendString(s);
            }
            GetResponse().AppendInteger(room.TradeState);
            GetResponse().AppendInteger(room.AllowPets);
            GetResponse().AppendInteger(room.AllowPetsEating);
            GetResponse().AppendInteger(room.AllowWalkthrough);
            GetResponse().AppendInteger(room.Hidewall);
            GetResponse().AppendInteger(room.WallThickness);
            GetResponse().AppendInteger(room.FloorThickness);
            GetResponse().AppendInteger(room.ChatType);
            GetResponse().AppendInteger(room.ChatBalloon);
            GetResponse().AppendInteger(room.ChatSpeed);
            GetResponse().AppendInteger(room.ChatMaxDistance);
            GetResponse().AppendInteger(room.ChatFloodProtection);
            GetResponse().AppendBool(false); //allow_dyncats_checkbox
            GetResponse().AppendInteger(room.WhoCanMute);
            GetResponse().AppendInteger(room.WhoCanKick);
            GetResponse().AppendInteger(room.WhoCanBan);
            SendResponse();
        }

        internal void RoomSettingsOkComposer(uint roomId)
        {
            GetResponse().Init(LibraryParser.OutgoingRequest("RoomSettingsSavedMessageComposer"));
            GetResponse().AppendInteger(roomId);
            SendResponse();
        }

        internal void RoomUpdatedOkComposer(uint roomId)
        {
            GetResponse().Init(LibraryParser.OutgoingRequest("RoomUpdateMessageComposer"));
            GetResponse().AppendInteger(roomId);
            SendResponse();
        }

        internal ServerMessage RoomFloorAndWallComposer(Room room)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("RoomFloorWallLevelsMessageComposer"));
            serverMessage.AppendBool(room.Hidewall == 1);
            serverMessage.AppendInteger(room.WallThickness);
            serverMessage.AppendInteger(room.FloorThickness);
            return serverMessage;
        }

        internal ServerMessage SerializeRoomChatOption(Room room)
        {
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("RoomChatOptionsMessageComposer"));
            serverMessage.AppendInteger(room.ChatType);
            serverMessage.AppendInteger(room.ChatBalloon);
            serverMessage.AppendInteger(room.ChatSpeed);
            serverMessage.AppendInteger(room.ChatMaxDistance);
            serverMessage.AppendInteger(room.ChatFloodProtection);
            return serverMessage;
        }

        internal void ParseRoomDataInformation()
        {
            var id = Request.GetUInteger();
            var num = Request.GetInteger();
            var num2 = Request.GetInteger();
            var room = Azure.GetGame().GetRoomManager().LoadRoom(id);
            if (num == 0 && num2 == 1)
            {
                SerializeRoomInformation(room, false);
                return;
            }
            if (num == 1 && num2 == 0)
            {
                SerializeRoomInformation(room, true);
                return;
            }
            SerializeRoomInformation(room, true);
        }

        internal void SerializeRoomInformation(Room room, bool show)
        {
            if (room == null)
                return;

            GetResponse().Init(LibraryParser.OutgoingRequest("RoomDataMessageComposer"));
            GetResponse().AppendBool(show);
            GetResponse().AppendInteger(room.RoomId);
            GetResponse().AppendString(room.Name);
            GetResponse().AppendBool(room.Type == "private");
            GetResponse().AppendInteger(room.OwnerId);
            GetResponse().AppendString(room.Owner);
            GetResponse().AppendInteger(room.State);
            GetResponse().AppendInteger(room.UsersNow);
            GetResponse().AppendInteger(room.UsersMax);
            GetResponse().AppendString(room.Description);
            GetResponse().AppendInteger(room.TradeState);
            GetResponse().AppendInteger(room.Score);
            GetResponse().AppendInteger(0);
            GetResponse().AppendInteger(0);
            GetResponse().AppendInteger(room.Category);

            if (room.GroupId > 0 && room.Group != null)
            {
                GetResponse().AppendInteger(room.Group.Id);
                GetResponse().AppendString(room.Group.Name);
                GetResponse().AppendString(room.Group.Badge);
                Response.AppendString("");
            }
            else
            {
                GetResponse().AppendInteger(0);
                GetResponse().AppendString("");
                GetResponse().AppendString("");
                GetResponse().AppendString("");
            }
            GetResponse().AppendInteger(room.TagCount);
            var tags = room.Tags.ToArray();
            foreach (var s in tags.Cast<string>())
            {
                GetResponse().AppendString(s);
            }
            GetResponse().AppendInteger(0);
            GetResponse().AppendInteger(0);
            GetResponse().AppendInteger(0);
            GetResponse().AppendBool(room.AllowPets == 1);
            GetResponse().AppendBool(room.AllowPetsEating == 1);
            GetResponse().AppendString("");
            GetResponse().AppendString("");
            GetResponse().AppendInteger(0);
            GetResponse().AppendBool(room.RoomId != Session.GetHabbo().CurrentRoomId);
            GetResponse().AppendBool(false); //Staffpick
            GetResponse().AppendBool(false);
            GetResponse().AppendBool(false); //Muted
            GetResponse().AppendInteger(room.WhoCanMute);
            GetResponse().AppendInteger(room.WhoCanKick);
            GetResponse().AppendInteger(room.WhoCanBan);
            GetResponse().AppendBool(room.CheckRights(Session, true, false));
            GetResponse().AppendInteger(room.ChatType);
            GetResponse().AppendInteger(room.ChatBalloon);
            GetResponse().AppendInteger(room.ChatSpeed);
            GetResponse().AppendInteger(room.ChatMaxDistance);
            GetResponse().AppendInteger(room.ChatFloodProtection);
            SendResponse();
            if (room.Group != null)
                return;
            DataTable table;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT user_id FROM rooms_rights WHERE room_id={0}",
                    room.RoomId));
                table = queryReactor.GetTable();
            }
            Response.Init(LibraryParser.OutgoingRequest("LoadRoomRightsListMessageComposer"));
            GetResponse().AppendInteger(room.RoomData.Id);
            GetResponse().AppendInteger(table.Rows.Count);
            foreach (var habboForId in table.Rows.Cast<DataRow>().Select(dataRow => Azure.GetHabboForId1((uint) dataRow[0])).Where(habboForId => habboForId != null))
            {
                GetResponse().AppendInteger(habboForId.Id);
                GetResponse().AppendString(habboForId.UserName);
            }
            SendResponse();
        }

        internal void SaveRoomData()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null || !room.CheckRights(Session, true, false))
                return;
            Request.GetInteger();
            var text = Request.GetString();
            var description = Request.GetString();
            var num = Request.GetInteger();
            var password = Request.GetString();
            var num2 = Request.GetInteger();
            var num3 = Request.GetInteger();
            var num4 = Request.GetInteger();
            var list = new List<string>();
            var stringBuilder = new StringBuilder();
            int allowPets;
            int allowPetsEating;
            int allowWalkthrough;
            int hidewall;
            int num5;
            int num6;
            int whoCanMute;
            int whoCanKick;
            int whoCanBan;
            int chatType;
            int chatBalloon;
            int chatSpeed;
            int num7;
            int chatFloodProtection;
            int tradestate;
            FlatCat flatCat;
            checked
            {
                for (var i = 0; i < num4; i++)
                {
                    if (i > 0)
                        stringBuilder.Append(",");
                    var text2 = Request.GetString().ToLower();
                    list.Add(text2);
                    stringBuilder.Append(text2);
                }
                tradestate = Request.GetInteger();
                allowPets = Convert.ToInt32(Azure.BoolToEnum(Request.GetBool()));
                allowPetsEating = Convert.ToInt32(Azure.BoolToEnum(Request.GetBool()));
                allowWalkthrough = Convert.ToInt32(Azure.BoolToEnum(Request.GetBool()));
                hidewall = Convert.ToInt32(Azure.BoolToEnum(Request.GetBool()));
                num5 = Request.GetInteger();
                num6 = Request.GetInteger();
                whoCanMute = Request.GetInteger();
                whoCanKick = Request.GetInteger();
                whoCanBan = Request.GetInteger();
                chatType = Request.GetInteger();
                chatBalloon = Request.GetInteger();
                chatSpeed = Request.GetInteger();
                num7 = Request.GetInteger();
                chatFloodProtection = Request.GetInteger();
                if (num5 < -2 || num5 > 1)
                    num5 = 0;
                if (num6 < -2 || num6 > 1)
                    num6 = 0;
                if (text.Length < 1)
                    return;
                if (num < 0 || num > 2)
                    return;
                if (num2 < 0)
                    return;
                if (num7 > 99)
                    num7 = 99;
                flatCat = Azure.GetGame().GetNavigator().GetFlatCat(num3);
                if (flatCat == null)
                    return;
            }
            if (flatCat.MinRank > Session.GetHabbo().Rank)
                num3 = 0;
            if (num4 > 2)
                return;
            room.TradeState = tradestate;
            room.AllowPets = allowPets;
            room.AllowPetsEating = allowPetsEating;
            room.AllowWalkthrough = allowWalkthrough;
            room.Hidewall = hidewall;
            room.RoomData.AllowPets = allowPets;
            room.RoomData.AllowPetsEating = allowPetsEating;
            room.RoomData.AllowWalkthrough = allowWalkthrough;
            room.RoomData.Hidewall = hidewall;
            room.Name = text;
            room.State = num;
            room.Description = description;
            room.Category = num3;
            room.Password = password;
            room.RoomData.Name = text;
            room.RoomData.State = num;
            room.RoomData.Description = description;
            room.RoomData.Category = num3;
            room.RoomData.Password = password;
            room.WhoCanBan = whoCanBan;
            room.WhoCanKick = whoCanKick;
            room.WhoCanMute = whoCanMute;
            room.RoomData.WhoCanBan = whoCanBan;
            room.RoomData.WhoCanKick = whoCanKick;
            room.RoomData.WhoCanMute = whoCanMute;
            room.ClearTags();
            room.AddTagRange(list);
            room.UsersMax = num2;
            room.RoomData.Tags.Clear();
            room.RoomData.Tags.AddRange(list);
            room.RoomData.UsersMax = num2;
            room.WallThickness = num5;
            room.FloorThickness = num6;
            room.RoomData.WallThickness = num5;
            room.RoomData.FloorThickness = num6;
            room.ChatType = chatType;
            room.ChatBalloon = chatBalloon;
            room.ChatSpeed = chatSpeed;
            room.ChatMaxDistance = num7;
            room.ChatFloodProtection = chatFloodProtection;
            room.RoomData.ChatType = chatType;
            room.RoomData.ChatBalloon = chatBalloon;
            room.RoomData.ChatSpeed = chatSpeed;
            room.RoomData.ChatMaxDistance = num7;
            room.RoomData.ChatFloodProtection = chatFloodProtection;
            RoomSettingsOkComposer(room.RoomId);
            RoomUpdatedOkComposer(room.RoomId);
            Session.GetHabbo().CurrentRoom.SendMessage(RoomFloorAndWallComposer(room));
            Session.GetHabbo().CurrentRoom.SendMessage(SerializeRoomChatOption(room));
            room.RoomData.SerializeRoomData(Response, false, Session, true);
        }

        internal void GetBannedUsers()
        {
            var num = Request.GetUInteger();
            var room = Azure.GetGame().GetRoomManager().GetRoom(num);
            if (room == null)
                return;
            var list = room.BannedUsers();
            Response.Init(LibraryParser.OutgoingRequest("RoomBannedListMessageComposer"));
            Response.AppendInteger(num);
            Response.AppendInteger(list.Count);
            foreach (var current in list)
            {
                Response.AppendInteger(current);
                Response.AppendString(Azure.GetHabboForId1(current) != null
                    ? Azure.GetHabboForId1(current).UserName
                    : "Undefined");
            }
            SendResponse();
        }

        internal void UsersWithRights()
        {
            Response.Init(LibraryParser.OutgoingRequest("LoadRoomRightsListMessageComposer"));
            Response.AppendInteger(Session.GetHabbo().CurrentRoom.RoomId);
            Response.AppendInteger(Session.GetHabbo().CurrentRoom.UsersWithRights.Count);
            foreach (var current in Session.GetHabbo().CurrentRoom.UsersWithRights)
            {
                var habboForId = Azure.GetHabboForId1(current);
                Response.AppendInteger(current);
                Response.AppendString((habboForId == null) ? "Undefined" : habboForId.UserName);
            }
            SendResponse();
        }

        internal void UnbanUser()
        {
            var num = Request.GetUInteger();
            var num2 = Request.GetUInteger();
            var room = Azure.GetGame().GetRoomManager().GetRoom(num2);
            if (room == null)
                return;
            room.Unban(num);
            Response.Init(LibraryParser.OutgoingRequest("RoomUnbanUserMessageComposer"));
            Response.AppendInteger(num2);
            Response.AppendInteger(num);
            SendResponse();
        }

        internal void GiveRights()
        {
            var num = Request.GetUInteger();
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null)
                return;
            var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(num);
            if (!room.CheckRights(Session, true, false))
                return;
            if (room.UsersWithRights.Contains(num))
            {
                Session.SendNotif("No puedes darle permisos.");
                return;
            }
            room.UsersWithRights.Add(num);
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "INSERT INTO rooms_rights (room_id,user_id) VALUES (",
                    room.RoomId,
                    ",",
                    num,
                    ")"
                }));
            if (roomUserByHabbo != null && !roomUserByHabbo.IsBot)
            {
                Response.Init(LibraryParser.OutgoingRequest("GiveRoomRightsMessageComposer"));
                Response.AppendInteger(room.RoomId);
                Response.AppendInteger(roomUserByHabbo.GetClient().GetHabbo().Id);
                Response.AppendString(roomUserByHabbo.GetClient().GetHabbo().UserName);
                SendResponse();
                roomUserByHabbo.UpdateNeeded = true;
                if (!roomUserByHabbo.IsBot)
                {
                    roomUserByHabbo.AddStatus("flatctrl 1", "");
                    Response.Init(LibraryParser.OutgoingRequest("RoomRightsLevelMessageComposer"));
                    Response.AppendInteger(1);
                    roomUserByHabbo.GetClient().SendMessage(GetResponse());
                }
            }
            UsersWithRights();
        }

        internal void TakeRights()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null || !room.CheckRights(Session, true, false))
                return;
            var stringBuilder = new StringBuilder();
            var num = Request.GetInteger();
            checked
            {
                for (var i = 0; i < num; i++)
                {
                    if (i > 0)
                        stringBuilder.Append(" OR ");
                    var num2 = Request.GetUInteger();
                    if (room.UsersWithRights.Contains(num2))
                        room.UsersWithRights.Remove(num2);
                    stringBuilder.Append(string.Concat(new object[]
                    {
                        "room_id = '",
                        room.RoomId,
                        "' AND user_id = '",
                        num2,
                        "'"
                    }));
                    var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(num2);
                    if (roomUserByHabbo != null && !roomUserByHabbo.IsBot)
                    {
                        Response.Init(LibraryParser.OutgoingRequest("RoomRightsLevelMessageComposer"));
                        Response.AppendInteger(0);
                        roomUserByHabbo.GetClient().SendMessage(GetResponse());
                        roomUserByHabbo.RemoveStatus("flatctrl 1");
                        roomUserByHabbo.UpdateNeeded = true;
                    }
                    Response.Init(LibraryParser.OutgoingRequest("RemoveRightsMessageComposer"));
                    Response.AppendInteger(room.RoomId);
                    Response.AppendInteger(num2);
                    SendResponse();
                }
                UsersWithRights();
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                    queryReactor.RunFastQuery(string.Format("DELETE FROM rooms_rights WHERE {0}", stringBuilder));
            }
        }

        internal void TakeAllRights()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null || !room.CheckRights(Session, true, false))
                return;
            DataTable table;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Format("SELECT user_id FROM rooms_rights WHERE room_id={0}", room.RoomId));
                table = queryReactor.GetTable();
            }
            foreach (DataRow dataRow in table.Rows)
            {
                var num = (uint) dataRow[0];
                var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(num);
                Response.Init(LibraryParser.OutgoingRequest("RemoveRightsMessageComposer"));
                Response.AppendInteger(room.RoomId);
                Response.AppendInteger(num);
                SendResponse();
                if (roomUserByHabbo == null || roomUserByHabbo.IsBot)
                    continue;
                Response.Init(LibraryParser.OutgoingRequest("RoomRightsLevelMessageComposer"));
                Response.AppendInteger(0);
                roomUserByHabbo.GetClient().SendMessage(GetResponse());
                roomUserByHabbo.RemoveStatus("flatctrl 1");
                roomUserByHabbo.UpdateNeeded = true;
            }
            using (var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
                queryreactor2.RunFastQuery(string.Format("DELETE FROM rooms_rights WHERE room_id = {0}", room.RoomId));
            room.UsersWithRights.Clear();
            UsersWithRights();
        }

        internal void KickUser()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null)
                return;
            if (!room.CheckRights(Session) && room.WhoCanKick != 2 
                && Session.GetHabbo().Rank < Convert.ToUInt32(Azure.GetDbConfig().DbData["ambassador.minrank"]))
                return;
            var pId = Request.GetUInteger();
            var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(pId);
            if (roomUserByHabbo == null || roomUserByHabbo.IsBot)
                return;
            if (room.CheckRights(roomUserByHabbo.GetClient(), true, false) ||
                roomUserByHabbo.GetClient().GetHabbo().HasFuse("fuse_mod") ||
                roomUserByHabbo.GetClient().GetHabbo().HasFuse("fuse_no_kick"))
                return;
            room.GetRoomUserManager().RemoveUserFromRoom(roomUserByHabbo.GetClient(), true, true);
            roomUserByHabbo.GetClient().CurrentRoomUserId = -1;
        }

        internal void BanUser()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null || (room.WhoCanBan == 0 && !room.CheckRights(Session, true, false)) ||
                (room.WhoCanBan == 1 && !room.CheckRights(Session)))
                return;
            var num = Request.GetInteger();
            Request.GetUInteger();
            var text = Request.GetString();
            var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(Convert.ToUInt32(num));
            if (roomUserByHabbo == null || roomUserByHabbo.IsBot)
                return;
            if (roomUserByHabbo.GetClient().GetHabbo().HasFuse("fuse_mod") ||
                roomUserByHabbo.GetClient().GetHabbo().HasFuse("fuse_no_kick"))
                return;
            var time = 0L;
            if (text.ToLower().Contains("hour"))
                time = 3600L;
            else if (text.ToLower().Contains("day"))
                time = 86400L;
            else if (text.ToLower().Contains("perm"))
                time = 788922000L;
            room.AddBan(num, time);
            room.GetRoomUserManager().RemoveUserFromRoom(roomUserByHabbo.GetClient(), true, true);
            Session.CurrentRoomUserId = -1;
        }

        internal void SetHomeRoom()
        {
            var num = Request.GetUInteger();
            var roomData = Azure.GetGame().GetRoomManager().GenerateRoomData(num);
            if (num != 0u && roomData == null)
                return;
            Session.GetHabbo().HomeRoom = num;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE users SET home_room = ",
                    num,
                    " WHERE id = ",
                    Session.GetHabbo().Id
                }));
            Response.Init(LibraryParser.OutgoingRequest("HomeRoomMessageComposer"));
            Response.AppendInteger(num);
            Response.AppendInteger(num);
            SendResponse();
        }

        internal void DeleteRoom()
        {
            var roomId = Request.GetUInteger();
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().UsersRooms == null)
                return;
            var room = Azure.GetGame().GetRoomManager().GetRoom(roomId);
            if (room == null)
                return;
            if (room.Owner != Session.GetHabbo().UserName && Session.GetHabbo().Rank <= 6u)
                return;
            if (Session.GetHabbo().GetInventoryComponent() != null)
                Session.GetHabbo()
                    .GetInventoryComponent()
                    .AddItemArray(room.GetRoomItemHandler().RemoveAllFurniture(Session));
            var roomData = room.RoomData;
            Azure.GetGame().GetRoomManager().UnloadRoom(room, "Delete room");
            Azure.GetGame().GetRoomManager().QueueVoteRemove(roomData);
            if (roomData == null || Session == null)
                return;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunFastQuery(string.Format("DELETE FROM rooms_data WHERE id = {0}", roomId));
                queryReactor.RunFastQuery(string.Format("DELETE FROM users_favorites WHERE room_id = {0}", roomId));
                queryReactor.RunFastQuery(string.Format("DELETE FROM items_rooms WHERE room_id = {0}", roomId));
                queryReactor.RunFastQuery(string.Format("DELETE FROM rooms_rights WHERE room_id = {0}", roomId));
                queryReactor.RunFastQuery(string.Format("UPDATE users SET home_room = '0' WHERE home_room = {0}",
                    roomId));
            }
            if (Session.GetHabbo().Rank > 5u && Session.GetHabbo().UserName != roomData.Owner)
                Azure.GetGame()
                    .GetModerationTool()
                    .LogStaffEntry(Session.GetHabbo().UserName, roomData.Name, "Room deletion",
                        string.Format("Deleted room ID {0}", roomData.Id));
            var roomData2 = (
                from p in Session.GetHabbo().UsersRooms
                where p.Id == roomId
                select p).SingleOrDefault<RoomData>();
            if (roomData2 != null)
                Session.GetHabbo().UsersRooms.Remove(roomData2);
        }

        internal void LookAt()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null)
                return;
            var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (roomUserByHabbo == null)
                return;
            roomUserByHabbo.UnIdle();
            var num = Request.GetInteger();
            var num2 = Request.GetInteger();
            if (num == roomUserByHabbo.X && num2 == roomUserByHabbo.Y)
                return;
            var rotation = PathFinder.CalculateRotation(roomUserByHabbo.X, roomUserByHabbo.Y, num, num2);
            roomUserByHabbo.SetRot(rotation, false);
            roomUserByHabbo.UpdateNeeded = true;
            if (!roomUserByHabbo.RidingHorse)
                return;
            var roomUserByVirtualId =
                Session.GetHabbo()
                    .CurrentRoom.GetRoomUserManager()
                    .GetRoomUserByVirtualId(Convert.ToInt32(roomUserByHabbo.HorseId));
            roomUserByVirtualId.SetRot(rotation, false);
            roomUserByVirtualId.UpdateNeeded = true;
        }

        internal void StartTyping()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null)
                return;
            var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (roomUserByHabbo == null)
                return;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TypingStatusMessageComposer"));
            serverMessage.AppendInteger(roomUserByHabbo.VirtualId);
            serverMessage.AppendInteger(1);
            room.SendMessage(serverMessage);
        }

        internal void StopTyping()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null)
                return;
            var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (roomUserByHabbo == null)
                return;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("TypingStatusMessageComposer"));
            serverMessage.AppendInteger(roomUserByHabbo.VirtualId);
            serverMessage.AppendInteger(0);
            room.SendMessage(serverMessage);
        }

        internal void IgnoreUser()
        {
            if (Session.GetHabbo().CurrentRoom == null)
                return;
            var text = Request.GetString();
            var habbo = Azure.GetGame().GetClientManager().GetClientByUserName(text).GetHabbo();
            if (habbo == null)
                return;
            if (Session.GetHabbo().MutedUsers.Contains(habbo.Id) || habbo.Rank > 4u)
                return;
            Session.GetHabbo().MutedUsers.Add(habbo.Id);
            Response.Init(LibraryParser.OutgoingRequest("UpdateIgnoreStatusMessageComposer"));
            Response.AppendInteger(1);
            Response.AppendString(text);
            SendResponse();
        }

        internal void UnignoreUser()
        {
            if (Session.GetHabbo().CurrentRoom == null)
                return;
            var text = Request.GetString();
            var habbo = Azure.GetGame().GetClientManager().GetClientByUserName(text).GetHabbo();
            if (habbo == null)
                return;
            if (!Session.GetHabbo().MutedUsers.Contains(habbo.Id))
                return;
            Session.GetHabbo().MutedUsers.Remove(habbo.Id);
            Response.Init(LibraryParser.OutgoingRequest("UpdateIgnoreStatusMessageComposer"));
            Response.AppendInteger(3);
            Response.AppendString(text);
            SendResponse();
        }

        internal void CanCreateRoomEvent()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null || !room.CheckRights(Session, true, false))
                return;
            var b = true;
            var i = 0;
            if (room.State != 0)
            {
                b = false;
                i = 3;
            }
            Response.AppendBool(b);
            Response.AppendInteger(i);
        }

        internal void Sign()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null)
                return;
            var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (roomUserByHabbo == null)
                return;
            roomUserByHabbo.UnIdle();
            var value = Request.GetInteger();
            roomUserByHabbo.AddStatus("sign", Convert.ToString(value));
            roomUserByHabbo.UpdateNeeded = true;
            roomUserByHabbo.SignTime = checked(Azure.GetUnixTimestamp() + 5);
        }

        internal void InitRoomGroupBadges()
        {
            Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().LoadingRoom);
        }

        internal void RateRoom()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null || Session.GetHabbo().RatedRooms.Contains(room.RoomId) ||
                room.CheckRights(Session, true, false))
                return;
            checked
            {
                switch (Request.GetInteger())
                {
                    case -1:
                        room.Score--;
                        room.RoomData.Score--;
                        break;
                    case 0:
                        return;
                    case 1:
                        room.Score++;
                        room.RoomData.Score++;
                        break;
                    default:
                        return;
                }
                Azure.GetGame().GetRoomManager().QueueVoteAdd(room.RoomData);
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                    queryReactor.RunFastQuery(string.Concat(new object[]
                    {
                        "UPDATE rooms_data SET score = ",
                        room.Score,
                        " WHERE id = ",
                        room.RoomId
                    }));
                Session.GetHabbo().RatedRooms.Add(room.RoomId);
                Response.Init(LibraryParser.OutgoingRequest("RoomRatingMessageComposer"));
                Response.AppendInteger(room.Score);
                Response.AppendBool(room.CheckRights(Session, true, false));
                SendResponse();
            }
        }

        internal void Dance()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null)
                return;
            var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (roomUserByHabbo == null)
                return;
            roomUserByHabbo.UnIdle();
            var num = Request.GetInteger();
            if (num < 0 || num > 4)
                num = 0;
            if (num > 0 && roomUserByHabbo.CarryItemId > 0)
                roomUserByHabbo.CarryItem(0);
            roomUserByHabbo.DanceId = num;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("DanceStatusMessageComposer"));
            serverMessage.AppendInteger(roomUserByHabbo.VirtualId);
            serverMessage.AppendInteger(num);
            room.SendMessage(serverMessage);
            Azure.GetGame().GetQuestManager().ProgressUserQuest(Session, QuestType.SocialDance, 0u);
            if (room.GetRoomUserManager().GetRoomUsers().Count > 19)
                Azure.GetGame().GetQuestManager().ProgressUserQuest(Session, QuestType.MassDance, 0u);
        }

        internal void AnswerDoorbell()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null || !room.CheckRights(Session))
                return;
            var userName = Request.GetString();
            var flag = Request.GetBool();
            var clientByUserName = Azure.GetGame().GetClientManager().GetClientByUserName(userName);
            if (clientByUserName == null)
                return;
            if (flag)
            {
                clientByUserName.GetHabbo().LoadingChecksPassed = true;
                clientByUserName.GetMessageHandler()
                    .Response.Init(LibraryParser.OutgoingRequest("DoorbellOpenedMessageComposer"));
                clientByUserName.GetMessageHandler().Response.AppendString("");
                clientByUserName.GetMessageHandler().SendResponse();
                return;
            }
            if (clientByUserName.GetHabbo().CurrentRoomId != Session.GetHabbo().CurrentRoomId)
            {
                clientByUserName.GetMessageHandler()
                    .Response.Init(LibraryParser.OutgoingRequest("DoorbellNoOneMessageComposer"));
                clientByUserName.GetMessageHandler().Response.AppendString("");
                clientByUserName.GetMessageHandler().SendResponse();
            }
        }

        internal void AlterRoomFilter()
        {
            var num = Request.GetUInteger();
            var flag = Request.GetBool();
            var text = Request.GetString();
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null || !room.CheckRights(Session, true, false))
                return;
            if (!flag)
            {
                if (!room.WordFilter.Contains(text))
                    return;
                room.WordFilter.Remove(text);
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery("DELETE FROM rooms_wordfilter WHERE room_id = @id AND word = @word");
                    queryReactor.AddParameter("id", num);
                    queryReactor.AddParameter("word", text);
                    queryReactor.RunQuery();
                    return;
                }
            }
            if (room.WordFilter.Contains(text))
                return;
            if (text.Contains("+"))
            {
                Session.SendNotif("No puedes colocar ninguna palabra que tenga un ' + ' por asuntos técnicos.");
                return;
            }
            room.WordFilter.Add(text);
            using (var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryreactor2.SetQuery("INSERT INTO rooms_wordfilter (room_id, word) VALUES (@id, @word);");
                queryreactor2.AddParameter("id", num);
                queryreactor2.AddParameter("word", text);
                queryreactor2.RunQuery();
            }
        }

        internal void GetRoomFilter()
        {
            var roomId = Request.GetUInteger();
            var room = Azure.GetGame().GetRoomManager().GetRoom(roomId);
            if (room == null || !room.CheckRights(Session, true, false))
                return;
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("RoomLoadFilterMessageComposer"));
            serverMessage.AppendInteger(room.WordFilter.Count);
            foreach (var current in room.WordFilter)
                serverMessage.AppendString(current);
            Response = serverMessage;
            SendResponse();
        }

        internal void ApplyRoomEffect()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null || !room.CheckRights(Session, true, false))
                return;
            var item = Session.GetHabbo().GetInventoryComponent().GetItem(Request.GetUInteger());
            if (item == null)
                return;
            var type = "floor";

            if (item.BaseItem.Name.ToLower().Contains("wallpaper"))
                type = "wallpaper";
            else if (item.BaseItem.Name.ToLower().Contains("landscape"))
                type = "landscape";

            switch (type)
            {
                case "floor":

                    room.RoomData.Floor = item.ExtraData;

                    Azure.GetGame()
                        .GetAchievementManager()
                        .ProgressUserAchievement(Session, "ACH_RoomDecoFloor", 1);
                    Azure.GetGame()
                        .GetQuestManager()
                        .ProgressUserQuest(Session, QuestType.FurniDecorationFloor);
                    break;

                case "wallpaper":

                    room.RoomData.Wallpaper = item.ExtraData;

                    Azure.GetGame()
                        .GetAchievementManager()
                        .ProgressUserAchievement(Session, "ACH_RoomDecoWallpaper", 1);
                    Azure.GetGame()
                        .GetQuestManager()
                        .ProgressUserQuest(Session, QuestType.FurniDecorationWall);
                    break;

                case "landscape":

                    room.RoomData.Landscape = item.ExtraData;

                    Azure.GetGame()
                        .GetAchievementManager()
                        .ProgressUserAchievement(Session, "ACH_RoomDecoLandscape", 1);
                    break;
            }
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(string.Concat(new object[]
                {
                    "UPDATE rooms_data SET ",
                    type,
                    " = @extradata WHERE id = ",
                    room.RoomId
                }));
                queryReactor.AddParameter("extradata", item.ExtraData);
                queryReactor.RunQuery();
                queryReactor.RunFastQuery(string.Format("DELETE FROM items_rooms WHERE id={0} LIMIT 1", item.Id));
            }
            Session.GetHabbo().GetInventoryComponent().RemoveItem(item.Id, false);
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("RoomSpacesMessageComposer"));
            serverMessage.AppendString(type);
            serverMessage.AppendString(item.ExtraData);
            room.SendMessage(serverMessage);
        }

        internal void PromoteRoom()
        {
            var pageId = Request.GetUInteger16();
            var item = Request.GetInteger();

            var page2 = Azure.GetGame().GetCatalog().GetPage(pageId);
            if (page2 == null)
                return;
            var catalogItem = page2.GetItem(item);

            if (catalogItem == null)
                return;

            var num = Request.GetUInteger();
            var text = Request.GetString();
            Request.GetBool();

            var text2 = "";
            try
            {
                text2 = Request.GetString();
            }
            catch (Exception)
            {
            }
            var category = Request.GetInteger();

            var room = Azure.GetGame().GetRoomManager().GetRoom(num);
            if (room == null)
            {
                var roomData = Azure.GetGame().GetRoomManager().GenerateRoomData(num);
                if (roomData == null)
                    return;
                room = new Room(roomData);
            }

            if (!room.CheckRights(Session, true))
                return;

            checked
            {
                if (catalogItem.CreditsCost > 0)
                {
                    if (catalogItem.CreditsCost > Session.GetHabbo().Credits)
                        return;
                    Session.GetHabbo().Credits -= catalogItem.CreditsCost;
                    Session.GetHabbo().UpdateCreditsBalance();
                }
                if (catalogItem.DucketsCost > 0)
                {
                    if (catalogItem.DucketsCost > Session.GetHabbo().ActivityPoints)
                        return;
                    Session.GetHabbo().ActivityPoints -= catalogItem.DucketsCost;
                    Session.GetHabbo().UpdateActivityPointsBalance();
                }
                if (catalogItem.BelCreditsCost > 0 || catalogItem.LoyaltyCost > 0)
                {
                    if (catalogItem.BelCreditsCost > Session.GetHabbo().BelCredits)
                        return;
                    Session.GetHabbo().BelCredits -= catalogItem.BelCreditsCost;
                    Session.GetHabbo().UpdateSeasonalCurrencyBalance();
                }
                Session.SendMessage(CatalogPacket.PurchaseOk());

                if (room.Event != null && !room.Event.HasExpired)
                {
                    room.Event.Time = Azure.GetUnixTimestamp();
                    Azure.GetGame().GetRoomEvents().SerializeEventInfo(room.RoomId);
                }
                else
                {
                    Azure.GetGame().GetRoomEvents().AddNewEvent(room.RoomId, text, text2, Session, 7200, category);
                    Azure.GetGame().GetRoomEvents().SerializeEventInfo(room.RoomId);
                }
                Session.GetHabbo().GetBadgeComponent().GiveBadge("RADZZ", true, Session, false);
            }
        }

        internal void GetPromotionableRooms()
        {
            var serverMessage = new ServerMessage();
            serverMessage.Init(LibraryParser.OutgoingRequest("CatalogPromotionGetRoomsMessageComposer"));
            serverMessage.AppendBool(true);
            serverMessage.AppendInteger(Session.GetHabbo().UsersRooms.Count);
            foreach (var current in Session.GetHabbo().UsersRooms)
            {
                serverMessage.AppendInteger(current.Id);
                serverMessage.AppendString(current.Name);
                serverMessage.AppendBool(false);
            }
            Response = serverMessage;
            SendResponse();
        }

        internal void SaveHeightmap()
        {
            if (!Session.GetHabbo().VIP && Session.GetHabbo().Rank < 1)
                return;
            var room = Session.GetHabbo().CurrentRoom;

            if (room == null)
            {
                Session.SendNotif("You must be in room to save these changes.");
                return;
            }

            if (!room.CheckRights(Session, true))
            {
                Session.SendNotif("No puedes hacer esto en una Sala que no es tuya...");
                return;
            }

            var heightMap = Request.GetString();
            var doorX = Request.GetInteger();
            var doorY = Request.GetInteger();
            var doorOrientation = Request.GetInteger();
            var wallThickness = Request.GetInteger();
            var floorThickness = Request.GetInteger();
            var wallHeight = Request.GetInteger();

            if (heightMap.Length < 2)
            {
                Session.SendNotif("Invalid room length");
                return;
            }

            if (wallThickness < -2 || wallThickness > 1)
                wallThickness = 0;

            if (floorThickness < -2 || floorThickness > 1)
                floorThickness = 0;

            if (doorOrientation < 0 || doorOrientation > 8)
                doorOrientation = 2;

            if (wallHeight < -1 || wallHeight > 16)
                wallHeight = -1;

            var doorZ = 0.0;

            char[] validLetters =
            {
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
                'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', '\r'
            };
            if (heightMap.Any(letter => !validLetters.Contains(letter)))
            {
                Session.SendNotif(
                    "El modelo que has elegido tiene carácteres no válidos. Recuerda meterlos en minúscula y de a-k (x).");
                return;
            }

            if (heightMap.Last() == Convert.ToChar(13))
                heightMap = heightMap.Remove(heightMap.Length - 1);

            if (heightMap.Length > 1024)
            {
                var message = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
                message.AppendString("floorplan_editor.error");
                message.AppendInteger(1);
                message.AppendString("errors");
                message.AppendString(
                    "(general): too large height (max 64 tiles)\r(general): too large area (max 1024 tiles)");
                Session.SendMessage(message);

                return;
            }

            if (heightMap.Split((char)13).Length - 1 < doorY)
            {
                var message = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
                message.AppendString("floorplan_editor.error");
                message.AppendInteger(1);
                message.AppendString("errors");
                message.AppendString("Y: Door is in invalid place.");
                Session.SendMessage(message);

                return;
            }

            var lines = heightMap.Split((char)13);
            var lineWidth = lines[0].Length;
            for (var i = 1; i < lines.Length; i++)
                if (lines[i].Length != lineWidth)
                {
                    var message = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
                    message.AppendString("floorplan_editor.error");
                    message.AppendInteger(1);
                    message.AppendString("errors");
                    message.AppendString("(general): Line " + (i + 1) + " is of different length than line 1");
                    Session.SendMessage(message);

                    return;
                }

            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("REPLACE INTO rooms_models_customs (roomid,door_x,door_y,door_z,door_dir,heightmap,poolmap) VALUES ('" + room.RoomId + "', '" + doorX + "','" +
                                  doorY + "','" + room.GetGameMap().StaticModel.DoorZ + "','" + doorOrientation +
                                  "',@newmodel,'')");
                queryReactor.AddParameter("newmodel", heightMap);
                queryReactor.RunQuery();

                room.WallHeight = wallHeight;
                room.WallThickness = wallThickness;
                room.FloorThickness = floorThickness;

                queryReactor.RunFastQuery(
                    string.Format(
                        "UPDATE rooms_data SET model_name = 'custom', wallthick = '{0}', floorthick = '{1}', walls_height = '{2}' WHERE id = {3};",
                        wallThickness, floorThickness, wallHeight, room.RoomId));
                Azure.GetGame().GetRoomManager().LoadModels(queryReactor);
                room.ResetGamemap("custom", wallHeight, wallThickness, floorThickness);
                Azure.GetGame().GetRoomManager().UnloadRoom(room, "Reload floor");
            }
            Session.SendNotif("Your new floorplan was saved! Your room was unloaded for applying changes.");
        }

        internal void PlantMonsterplant(RoomItem mopla, Room room)
        {
            if (room == null)
                return;
            if (mopla == null)
                return;
            if (mopla.GetBaseItem().InteractionType != InteractionType.moplaseed)
                return;
            var rarity = int.Parse(mopla.ExtraData);
            var getX = mopla.X;
            var getY = mopla.Y;
            room.GetRoomItemHandler().RemoveFurniture(Session, mopla.Id, false);
            var pet = Catalog.CreatePet(Session.GetHabbo().Id, "Monsterplant", 16, "0", "0", rarity);
            Response.Init(LibraryParser.OutgoingRequest("SendMonsterplantIdMessageComposer"));
            Response.AppendInteger(pet.PetId);
            SendResponse();
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE bots SET room_id = '",
                    room.RoomId,
                    "', x = '",
                    getX,
                    "', y = '",
                    getY,
                    "' WHERE id = '",
                    pet.PetId,
                    "'"
                }));
            pet.PlacedInRoom = true;
            pet.RoomId = room.RoomId;
            var bot = new RoomBot(pet.PetId, pet.OwnerId, pet.RoomId, AIType.Pet, "freeroam", pet.Name, "", pet.Look,
                getX, getY, 0.0, 4, 0, 0, 0, 0, null, null, "", 0, false);
            room.GetRoomUserManager().DeployBot(bot, pet);

            if (pet.DbState != DatabaseUpdateState.NeedsInsert)
                pet.DbState = DatabaseUpdateState.NeedsUpdate;

            using (var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryreactor2.RunFastQuery(string.Format("DELETE FROM items_rooms WHERE id = {0}", mopla.Id));
                room.GetRoomUserManager().SavePets(queryreactor2);
            }
        }

        internal void KickBot()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null || !room.CheckRights(Session, true, false))
                return;
            var roomUserByVirtualId = room.GetRoomUserManager().GetRoomUserByVirtualId(Request.GetInteger());
            if (roomUserByVirtualId == null || !roomUserByVirtualId.IsBot)
                return;

            room.GetRoomUserManager().RemoveBot(roomUserByVirtualId.VirtualId, true);
        }

        internal void PlacePet()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);

            if (room == null || (room.AllowPets == 0 && !room.CheckRights(Session, true, false)) ||
                !room.CheckRights(Session, true, false))
                return;

            var num = Request.GetUInteger();
            var pet = Session.GetHabbo().GetInventoryComponent().GetPet(num);

            if (pet == null || pet.PlacedInRoom)
                return;

            var x = Request.GetInteger();
            var y = Request.GetInteger();

            if (!room.GetGameMap().CanWalk(x, y, false, 0u))
                return;

            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "UPDATE bots SET room_id = '",
                    room.RoomId,
                    "', x = '",
                    x,
                    "', y = '",
                    y,
                    "' WHERE id = '",
                    num,
                    "'"
                }));

            pet.PlacedInRoom = true;
            pet.RoomId = room.RoomId;

            room.GetRoomUserManager()
                .DeployBot(
                    new RoomBot(pet.PetId, Convert.ToUInt32(pet.OwnerId), pet.RoomId, AIType.Pet, "freeroam", pet.Name,
                        "", pet.Look, x, y, 0.0, 4, 0, 0, 0, 0, null, null, "", 0, false), pet);
            Session.GetHabbo().GetInventoryComponent().MovePetToRoom(pet.PetId);
            if (pet.DbState != DatabaseUpdateState.NeedsInsert)
                pet.DbState = DatabaseUpdateState.NeedsUpdate;
            using (var queryreactor2 = Azure.GetDatabaseManager().GetQueryReactor())
                room.GetRoomUserManager().SavePets(queryreactor2);
            Session.SendMessage(Session.GetHabbo().GetInventoryComponent().SerializePetInventory());
        }

        internal void UpdateEventInfo()
        {
            Request.GetInteger();
            var original = Request.GetString();
            var original2 = Request.GetString();
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null || !room.CheckRights(Session, true, false) || room.Event == null)
                return;
            room.Event.Name = original;
            room.Event.Description = original2;
            Azure.GetGame().GetRoomEvents().UpdateEvent(room.Event);
        }

        internal void HandleBotSpeechList()
        {
            var botId = Request.GetUInteger();
            var num2 = Request.GetInteger();
            var num3 = num2;

            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null)
                return;
            var bot = room.GetRoomUserManager().GetBot(botId);
            if (bot == null || !bot.IsBot)
                return;

            if (num3 == 2)
            {
                var text = bot.BotData.RandomSpeech == null ? "" : string.Join("\n", bot.BotData.RandomSpeech);
                text += ";#;";
                text += bot.BotData.AutomaticChat ? "true" : "false";
                text += ";#;";
                text += bot.BotData.SpeechInterval;
                text += ";#;";
                text += bot.BotData.MixPhrases ? "true" : "false";

                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("BotSpeechListMessageComposer"));
                serverMessage.AppendInteger(botId);
                serverMessage.AppendInteger(num2);
                serverMessage.AppendString(text);
                Response = serverMessage;
                SendResponse();
                return;
            }
            if (num3 != 5)
                return;

            var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("BotSpeechListMessageComposer"));
            serverMessage2.AppendInteger(botId);
            serverMessage2.AppendInteger(num2);
            serverMessage2.AppendString(bot.BotData.Name);

            Response = serverMessage2;
            SendResponse();
        }

        internal void ManageBotActions()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            var num = Request.GetUInteger();
            var num2 = Request.GetInteger();
            var data = Azure.FilterInjectionChars(Request.GetString());
            var bot = room.GetRoomUserManager().GetBot(num);
            var flag = false;
            checked
            {
                switch (num2)
                {
                    case 1:
                        bot.BotData.Look = Session.GetHabbo().Look;
                        goto IL_439;
                    case 2:
                        try
                        {
                            var array = data.Split(new[] {";#;"}, StringSplitOptions.None);

                            var speechsJunk =
                                array[0].Substring(0, array[0].Length > 1024 ? 1024 : array[0].Length)
                                    .Split(Convert.ToChar(13));
                            var speak = (array[1] == "true");
                            var speechDelay = int.Parse(array[2]);
                            var mix = (array[3] == "true");
                            if (speechDelay < 7)
                                speechDelay = 7;

                            var speechs = "";
                            foreach (string speech in speechsJunk)
                            {
                                if (string.IsNullOrEmpty(speech) ||
                                    (speech.ToLower().Contains("update") && speech.ToLower().Contains("set")) ||
                                    speech.Contains("<font"))
                                    return;

                                speechs += speech + ";";
                            }

                            using (var queryreactor3 = Azure.GetDatabaseManager().GetQueryReactor())
                            {
                                queryreactor3.SetQuery(
                                    "UPDATE bots SET automatic_chat = @autochat, speaking_interval = @interval, mix_phrases = @mix_phrases, speech = @speech WHERE id = @botid");

                                {
                                    queryreactor3.AddParameter("autochat", speak ? "1" : "0");
                                    queryreactor3.AddParameter("interval", speechDelay);
                                    queryreactor3.AddParameter("mix_phrases", mix ? "1" : "0");
                                    queryreactor3.AddParameter("botid", num);
                                    queryreactor3.AddParameter("speech", speechs);
                                    queryreactor3.RunQuery();
                                }
                            }
                            var randomSpeech = speechs.Split(';').ToList();

                            room.GetRoomUserManager()
                                .UpdateBot(bot.VirtualId, bot, bot.BotData.Name, bot.BotData.Motto, bot.BotData.Look,
                                    bot.BotData.Gender, randomSpeech, null, speak, speechDelay, mix);
                            flag = true;
                            goto IL_439;
                        }
                        catch (Exception e)
                        {
                            Writer.Writer.LogException(e.ToString());
                            return;
                        }
                    case 3:
                        if (bot.BotData.WalkingMode == "freeroam")
                            bot.BotData.WalkingMode = "stand";
                        else
                            bot.BotData.WalkingMode = "freeroam";
                        using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                        {
                            queryReactor.SetQuery("UPDATE bots SET walk_mode = @walkmode WHERE id = @botid");
                            queryReactor.AddParameter("walkmode", bot.BotData.WalkingMode);
                            queryReactor.AddParameter("botid", num);
                            queryReactor.RunQuery();
                        }
                        goto IL_439;
                    case 4:
                        break;
                    case 5:
                        bot.BotData.Name = data;
                        goto IL_439;
                    default:
                        goto IL_439;
                }
                if (bot.BotData.DanceId > 0)
                    bot.BotData.DanceId = 0;
                else
                {
                    var random = new Random();
                    bot.DanceId = random.Next(1, 4);
                    bot.BotData.DanceId = bot.DanceId;
                }
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("DanceStatusMessageComposer"));
                serverMessage.AppendInteger(bot.VirtualId);
                serverMessage.AppendInteger(bot.BotData.DanceId);
                Session.GetHabbo().CurrentRoom.SendMessage(serverMessage);
                IL_439:
                if (!flag)
                {
                    var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("SetRoomUserMessageComposer"));
                    serverMessage2.AppendInteger(1);
                    bot.Serialize(serverMessage2, room.GetGameMap().GotPublicPool);
                    room.SendMessage(serverMessage2);
                }
            }
        }

        internal void RoomOnLoad()
        {
            // TODO!
            Response.Init(LibraryParser.OutgoingRequest("SendRoomCampaignFurnitureMessageComposer"));
            Response.AppendInteger(0);
            SendResponse();
        }

        internal void MuteAll()
        {
            var currentRoom = Session.GetHabbo().CurrentRoom;
            if (currentRoom == null || !currentRoom.CheckRights(Session, true, false))
                return;
            currentRoom.RoomMuted = !currentRoom.RoomMuted;

            Response.Init(LibraryParser.OutgoingRequest("RoomMuteStatusMessageComposer"));
            Response.AppendBool(currentRoom.RoomMuted);
            Session.SendMessage(Response);
        }

        internal void HomeRoom()
        {
            GetResponse().Init(LibraryParser.OutgoingRequest("HomeRoomMessageComposer"));
            GetResponse().AppendInteger(Session.GetHabbo().HomeRoom);
            GetResponse().AppendInteger(Session.GetHabbo().HomeRoom);
            SendResponse();
        }

        internal void RemoveFavouriteRoom()
        {
            if (Session.GetHabbo() == null)
                return;
            var num = Request.GetUInteger();
            Session.GetHabbo().FavoriteRooms.Remove(num);
            Response.Init(LibraryParser.OutgoingRequest("FavouriteRoomsUpdateMessageComposer"));
            Response.AppendInteger(num);
            Response.AppendBool(false);
            SendResponse();

            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                queryReactor.RunFastQuery(string.Concat(new object[]
                {
                    "DELETE FROM users_favorites WHERE user_id = ",
                    Session.GetHabbo().Id,
                    " AND room_id = ",
                    num
                }));
        }

        internal void RoomUserAction()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null)
                return;
            var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (roomUserByHabbo == null)
                return;
            roomUserByHabbo.UnIdle();
            var num = Request.GetInteger();
            roomUserByHabbo.DanceId = 0;

            var action = new ServerMessage(LibraryParser.OutgoingRequest("RoomUserActionMessageComposer"));
            action.AppendInteger(roomUserByHabbo.VirtualId);
            action.AppendInteger(num);
            room.SendMessage(action);

            if (num == 5)
            {
                roomUserByHabbo.IsAsleep = true;
                var Sleep = new ServerMessage(LibraryParser.OutgoingRequest("RoomUserIdleMessageComposer"));
                Sleep.AppendInteger(roomUserByHabbo.VirtualId);
                Sleep.AppendBool(roomUserByHabbo.IsAsleep);
                room.SendMessage(Sleep);
            }
            Azure.GetGame().GetQuestManager().ProgressUserQuest(Session, QuestType.SocialWave, 0u);
        }

        internal void GetRoomData1()
        {
            if (Session.GetHabbo().LoadingRoom <= 0u)
            {
            }
            /*this.Response.Init(StaticClientMessageHandler.OutgoingRequest("297"));//Not in release
            this.Response.AppendInt32(0);
            this.SendResponse();*/
        }

        internal void GetRoomData2()
        {
            try
            {
                var queuedServerMessage = new QueuedServerMessage(Session.GetConnection());
                if (Session.GetHabbo().LoadingRoom <= 0u || CurrentLoadingRoom == null)
                    return;
                var roomData = CurrentLoadingRoom.RoomData;
                if (roomData == null)
                    return;
                if (roomData.Model == null)
                {
                    Session.SendNotif("Go To Fuck Off You Stupid Damn AssHOLE  !  Fuck You !!");
                    Session.SendMessage(
                        new ServerMessage(LibraryParser.OutgoingRequest("OutOfRoomMessageComposer")));
                    ClearRoomLoading();
                }
                else
                {
                    queuedServerMessage.AppendResponse(CurrentLoadingRoom.GetGameMap().GetNewHeightmap());
                    queuedServerMessage.AppendResponse(CurrentLoadingRoom.GetGameMap().Model.GetHeightmap());
                    queuedServerMessage.SendResponse();
                    GetRoomData3();
                }
            }
            catch (Exception ex)
            {
                Logging.LogException(string.Concat(new object[]
                {
                    "Unable to load room ID [",
                    Session.GetHabbo().LoadingRoom,
                    "] ",
                    ex.ToString()
                }));
            }
        }

        internal void GetRoomData3()
        {
            if (Session.GetHabbo().LoadingRoom <= 0u || !Session.GetHabbo().LoadingChecksPassed ||
                CurrentLoadingRoom == null || Session == null)
                return;
            QueuedServerMessage queuedServerMessage;
            RoomItem[] array;
            RoomItem[] array2;
            RoomItem[] array3;
            checked
            {
                if (CurrentLoadingRoom.UsersNow + 1 > CurrentLoadingRoom.UsersMax &&
                    !Session.GetHabbo().HasFuse("fuse_enter_full_rooms"))
                {
                    var roomFull = new ServerMessage(LibraryParser.OutgoingRequest("RoomEnterErrorMessageComposer"));
                    roomFull.AppendInteger(1);
                    return;
                }
                ClearRoomLoading();
                queuedServerMessage = new QueuedServerMessage(Session.GetConnection());
                array = CurrentLoadingRoom.GetRoomItemHandler().MFloorItems.Values.ToArray();
                array2 = CurrentLoadingRoom.GetRoomItemHandler().MWallItems.Values.ToArray();
                Response.Init(LibraryParser.OutgoingRequest("RoomFloorItemsMessageComposer"));
                if (CurrentLoadingRoom.Group != null)
                {
                    if (CurrentLoadingRoom.Group.AdminOnlyDeco == 1u)
                    {
                        Response.AppendInteger(CurrentLoadingRoom.Group.Admins.Count + 1);
                        using (var enumerator = CurrentLoadingRoom.Group.Admins.Values.GetEnumerator())
                        {
                            while (enumerator.MoveNext())
                            {
                                var current = enumerator.Current;
                                Response.AppendInteger(current.Id);
                                Response.AppendString(Azure.GetHabboForId1(current.Id).UserName);
                            }
                            goto IL_220;
                        }
                    }
                    Response.AppendInteger(CurrentLoadingRoom.Group.Members.Count + 1);
                    foreach (var current2 in CurrentLoadingRoom.Group.Members.Values)
                    {
                        Response.AppendInteger(current2.Id);
                        Response.AppendString(Azure.GetHabboForId1(current2.Id).UserName);
                    }
                    IL_220:
                    Response.AppendInteger(CurrentLoadingRoom.OwnerId);
                    Response.AppendString(CurrentLoadingRoom.Owner);
                }
                else
                {
                    Response.AppendInteger(1);
                    Response.AppendInteger(CurrentLoadingRoom.OwnerId);
                    Response.AppendString(CurrentLoadingRoom.Owner);
                }
                Response.AppendInteger(array.Length);
                array3 = array;
            }
            foreach (var roomItem in array3)
            {
                roomItem.Serialize(Response);
            }
            queuedServerMessage.AppendResponse(GetResponse());
            Response.Init(LibraryParser.OutgoingRequest("RoomWallItemsMessageComposer"));
            RoomItem[] array4;
            checked
            {
                if (CurrentLoadingRoom.Group != null)
                {
                    if (CurrentLoadingRoom.Group.AdminOnlyDeco == 1u)
                    {
                        Response.AppendInteger(CurrentLoadingRoom.Group.Admins.Count + 1);
                        using (var enumerator3 = CurrentLoadingRoom.Group.Admins.Values.GetEnumerator())
                        {
                            while (enumerator3.MoveNext())
                            {
                                var current3 = enumerator3.Current;
                                Response.AppendInteger(current3.Id);
                                Response.AppendString(Azure.GetHabboForId1(current3.Id).UserName);
                            }
                            goto IL_423;
                        }
                    }
                    Response.AppendInteger(CurrentLoadingRoom.Group.Members.Count + 1);
                    foreach (var current4 in CurrentLoadingRoom.Group.Members.Values)
                    {
                        Response.AppendInteger(current4.Id);
                        Response.AppendString(Azure.GetHabboForId1(current4.Id).UserName);
                    }
                    IL_423:
                    Response.AppendInteger(CurrentLoadingRoom.OwnerId);
                    Response.AppendString(CurrentLoadingRoom.Owner);
                }
                else
                {
                    Response.AppendInteger(1);
                    Response.AppendInteger(CurrentLoadingRoom.OwnerId);
                    Response.AppendString(CurrentLoadingRoom.Owner);
                }
                Response.AppendInteger(array2.Length);
                array4 = array2;
            }
            foreach (var roomItem2 in array4)
            {
                roomItem2.Serialize(Response);
            }
            queuedServerMessage.AppendResponse(GetResponse());
            Array.Clear(array, 0, array.Length);
            Array.Clear(array2, 0, array2.Length);
            array = null;
            array2 = null;
            CurrentLoadingRoom.GetRoomUserManager().AddUserToRoom(Session, Session.GetHabbo().SpectatorMode, false);
            queuedServerMessage.SendResponse();
            if (Azure.GetUnixTimestamp() < Session.GetHabbo().FloodTime && Session.GetHabbo().FloodTime != 0)
            {
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("FloodFilterMessageComposer"));
                serverMessage.AppendInteger(checked(Session.GetHabbo().FloodTime - Azure.GetUnixTimestamp()));
                Session.SendMessage(serverMessage);
            }

            Poll poll = null;
            if (!Azure.GetGame().GetPollManager().TryGetPoll(CurrentLoadingRoom.RoomId, out poll) ||
                Session.GetHabbo().GotPollData(poll.Id))
                return;
            Response.Init(LibraryParser.OutgoingRequest("SuggestPollMessageComposer"));
            poll.Serialize(Response);
            SendResponse();
        }

        internal void WidgetContainers()
        {
            var text = Request.GetString();
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("LandingWidgetMessageComposer"));
            if (!string.IsNullOrEmpty(text))
            {
                var array = text.Split(',');
                if (array[1] == "gamesmaker")
                    return;
                serverMessage.AppendString(text);
                serverMessage.AppendString(array[1]);
            }
            else
            {
                serverMessage.AppendString("");
                serverMessage.AppendString("");
            }
            Session.SendMessage(serverMessage);
        }

        internal void RefreshPromoEvent()
        {
            var hotelView = Azure.GetGame().GetHotelView();
            if (Session == null || Session.GetHabbo() == null)
                return;
            if (hotelView.HotelViewPromosIndexers.Count <= 0)
                return;
            var message =
                hotelView.SmallPromoComposer(
                    new ServerMessage(LibraryParser.OutgoingRequest("LandingPromosMessageComposer")));
            Session.SendMessage(message);
        }

        internal void AcceptPoll()
        {
            var key = Request.GetUInteger();
            var poll = Azure.GetGame().GetPollManager().Polls[key];
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("PollQuestionsMessageComposer"));
            serverMessage.AppendInteger(poll.Id);
            serverMessage.AppendString(poll.PollName);
            serverMessage.AppendString(poll.Thanks);
            serverMessage.AppendInteger(poll.Questions.Count);
            foreach (var current in poll.Questions)
            {
                var questionNumber = checked(poll.Questions.IndexOf(current) + 1);
                current.Serialize(serverMessage, questionNumber);
            }
            Response = serverMessage;
            SendResponse();
        }

        internal void RefusePoll()
        {
            var num = Request.GetUInteger();
            Session.GetHabbo().AnsweredPolls.Add(num);
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery("INSERT INTO users_polls VALUES (@userid , @pollid , 0 , '0' , '')");
                queryReactor.AddParameter("userid", Session.GetHabbo().Id);
                queryReactor.AddParameter("pollid", num);
                queryReactor.RunQuery();
            }
        }

        internal void AnswerPoll()
        {
            var pollId = Request.GetUInteger();
            var questionId = Request.GetUInteger();
            var num3 = Request.GetInteger();
            var list = new List<string>();
            checked
            {
                for (var i = 0; i < num3; i++)
                    list.Add(Request.GetString());
                var text = string.Join("\r\n", list);
                var poll = Azure.GetGame().GetPollManager().TryGetPollById(pollId);
                if(poll != null && poll.Type == Poll.PollType.Matching)//MATCHING_POLL
                {
                    if (text == "1") { poll.answersPositive++; }
                    else { poll.answersNegative++; }
                    ServerMessage Answered = new ServerMessage(LibraryParser.OutgoingRequest("MatchingPollAnsweredMessageComposer"));
                    Answered.AppendInteger(Session.GetHabbo().Id);
                    Answered.AppendString(text);
                    Answered.AppendInteger(0);//count
                    /*Answered.AppendInteger(2);//count
                    Answered.AppendString("0");
                    Answered.AppendInteger(poll.answersNegative);
                    Answered.AppendString("1");
                    Answered.AppendInteger(poll.answersPositive);*/
                    Session.SendMessage(Answered);
                    return;
                }
                Session.GetHabbo().AnsweredPolls.Add(pollId);
                using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery(
                        "INSERT INTO users_polls VALUES (@userid , @pollid , @questionid , '1' , @answer)");
                    queryReactor.AddParameter("userid", Session.GetHabbo().Id);
                    queryReactor.AddParameter("pollid", pollId);
                    queryReactor.AddParameter("questionid", questionId);
                    queryReactor.AddParameter("answer", text);
                    queryReactor.RunQuery();
                }
            }
        }

        public string WallPositionCheck(string wallPosition)
        {
            string result;
            try
            {
                if (wallPosition.Contains(Convert.ToChar(13)))
                    result = null;
                else if (wallPosition.Contains(Convert.ToChar(9)))
                    result = null;
                else
                {
                    var array = wallPosition.Split(' ');
                    if (array[2] != "l" && array[2] != "r")
                        result = null;
                    else
                    {
                        var array2 = array[0].Substring(3).Split(',');
                        var num = int.Parse(array2[0]);
                        var num2 = int.Parse(array2[1]);
                        if (num < 0 || num2 < 0 || num > 200 || num2 > 200)
                            result = null;
                        else
                        {
                            var array3 = array[1].Substring(2).Split(',');
                            var num3 = int.Parse(array3[0]);
                            var num4 = int.Parse(array3[1]);
                            if (num3 < 0 || num4 < 0 || num3 > 200 || num4 > 200)
                                result = null;
                            else
                                result = string.Concat(new object[]
                                {
                                    ":w=",
                                    num,
                                    ",",
                                    num2,
                                    " l=",
                                    num3,
                                    ",",
                                    num4,
                                    " ",
                                    array[2]
                                });
                        }
                    }
                }
            }
            catch
            {
                result = null;
            }
            return result;
        }

        public void Whisper()
        {
            if (!Session.GetHabbo().InRoom)
                return;
            var currentRoom = Session.GetHabbo().CurrentRoom;
            var text = Request.GetString();
            var text2 = text.Split(' ')[0];

            checked
            {
                var text3 = text.Substring(text2.Length + 1);
                var colour = Request.GetInteger();
                var roomUserByHabbo = currentRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                var roomUserByHabbo2 = currentRoom.GetRoomUserManager().GetRoomUserByHabbo(text2);

                text3 = currentRoom.WordFilter.Aggregate(text3, (current1, current) => Regex.Replace(current1, current, "bobba", RegexOptions.IgnoreCase));

                if (Session.GetHabbo().Rank < 9 && BlockAds.CheckPublicistas(text3))
                {
                    Session.PublicistaCount++;
                    Session.HandlePublicista(text3);
                    return;
                }

                if (roomUserByHabbo == null || roomUserByHabbo2 == null)
                {
                    Session.SendWhisper(text3);
                    return;
                }
                if (Session.GetHabbo().Rank < 4 && currentRoom.CheckMute(Session))
                    return;
                currentRoom.AddChatlog(Session.GetHabbo().Id, string.Format("<Whispering to {0}>: {1}", text2, text3),
                    false);

                Azure.GetGame().GetQuestManager().ProgressUserQuest(Session, QuestType.SocialChat, 0u);
                var colour2 = colour;
                if (!roomUserByHabbo.IsBot)
                    if (colour2 == 2 || (colour2 == 23 && !Session.GetHabbo().HasFuse("fuse_mod")) || colour2 < 0 ||
                        colour2 > 29)
                        colour2 = roomUserByHabbo.LastBubble; // or can also be just 0

                roomUserByHabbo.UnIdle();

                var whisp = new ServerMessage(LibraryParser.OutgoingRequest("WhisperMessageComposer"));
                whisp.AppendInteger(roomUserByHabbo.VirtualId);
                whisp.AppendString(text3);
                whisp.AppendInteger(0);
                whisp.AppendInteger(colour2);
                whisp.AppendInteger(0);
                whisp.AppendInteger(-1);

                roomUserByHabbo.GetClient().SendMessage(whisp);
                if (!roomUserByHabbo2.IsBot &&
                    roomUserByHabbo2.UserId != roomUserByHabbo.UserId &&
                    !roomUserByHabbo2.GetClient().GetHabbo().MutedUsers.Contains(Session.GetHabbo().Id))
                    roomUserByHabbo2.GetClient().SendMessage(whisp);
                var roomUserByRank = currentRoom.GetRoomUserManager().GetRoomUserByRank(4);
                if (!roomUserByRank.Any())
                    return;
                foreach (var current2 in roomUserByRank)
                    if (current2 != null && current2.HabboId != roomUserByHabbo2.HabboId &&
                        current2.HabboId != roomUserByHabbo.HabboId && current2.GetClient() != null)
                    {
                        var whispStaff = new ServerMessage(LibraryParser.OutgoingRequest("WhisperMessageComposer"));
                        whispStaff.AppendInteger(roomUserByHabbo.VirtualId);
                        whispStaff.AppendString(string.Format("Whisper to {0}: {1}", text2, text3));
                        whispStaff.AppendInteger(0);
                        whispStaff.AppendInteger(colour2);
                        whispStaff.AppendInteger(0);
                        whispStaff.AppendInteger(-1);
                        current2.GetClient().SendMessage(whispStaff);
                    }
            }
        }

        public void Chat()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null)
                return;
            var roomUser = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (roomUser == null)
                return;
            var message = Request.GetString();
            var bubble = Request.GetInteger();
            var count = Request.GetInteger();

            if (!roomUser.IsBot)
                if (bubble == 2 || (bubble == 23 && !Session.GetHabbo().HasFuse("fuse_mod")) || bubble < 0 ||
                    bubble > 29)
                    bubble = roomUser.LastBubble; // or can also be just 0

            roomUser.Chat(Session, message, false, count, bubble);
        }

        public void Shout()
        {
            var room = Azure.GetGame().GetRoomManager().GetRoom(Session.GetHabbo().CurrentRoomId);
            if (room == null)
                return;
            var roomUserByHabbo = room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (roomUserByHabbo == null)
                return;
            var Msg = Request.GetString();
            var bubble = Request.GetInteger();
            if (!roomUserByHabbo.IsBot)
                if (bubble == 2 || (bubble == 23 && !Session.GetHabbo().HasFuse("fuse_mod")) || bubble < 0 ||
                    bubble > 29)
                    bubble = roomUserByHabbo.LastBubble; // or can also be just 0

            roomUserByHabbo.Chat(Session, Msg, true, -1, bubble);
        }

        public void GetFloorPlanUsedCoords()
        {
            Response.Init(LibraryParser.OutgoingRequest("GetFloorPlanUsedCoordsMessageComposer"));

            var room = Session.GetHabbo().CurrentRoom;

            if (room == null)
                Response.AppendInteger(0);
            else
            {
                var coords = room.GetGameMap().CoordinatedItems.Keys.OfType<Point>().ToArray();

                Response.AppendInteger(coords.Count());

                foreach (var point in coords)
                {
                    Response.AppendInteger(point.X);
                    Response.AppendInteger(point.Y);
                }
            }

            SendResponse();
        }

        public void GetFloorPlanDoor()
        {
            var room = Session.GetHabbo().CurrentRoom;
            if (room == null)
                return;
            Response.Init(LibraryParser.OutgoingRequest("SetFloorPlanDoorMessageComposer"));
            Response.AppendInteger(room.GetGameMap().Model.DoorX);
            Response.AppendInteger(room.GetGameMap().Model.DoorY);
            Response.AppendInteger(room.GetGameMap().Model.DoorOrientation);
            SendResponse();
        }
    }
}