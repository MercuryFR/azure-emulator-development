﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.Encryption;
using Azure.Encryption.Hurlant.Crypto.Prng;
using Azure.HabboHotel.Achievements;
using Azure.HabboHotel.Catalogs;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Groups;
using Azure.HabboHotel.Guides;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Navigators;
using Azure.HabboHotel.PathFinding;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.Polls;
using Azure.HabboHotel.Quests;
using Azure.HabboHotel.Quests.Composer;
using Azure.HabboHotel.RoomBots;
using Azure.HabboHotel.Rooms;
using Azure.HabboHotel.Rooms.Wired;
using Azure.HabboHotel.SoundMachine;
using Azure.HabboHotel.SoundMachine.Composers;
using Azure.HabboHotel.Support;
using Azure.HabboHotel.Users.Badges;
using Azure.HabboHotel.Users.Messenger;
using Azure.HabboHotel.Users.Relationships;
using Azure.HabboHotel.YouTube;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.Messages.Handlers
{
    internal partial class GameClientMessageHandler
    {
        internal void GameCenterLoadGame()
        {
            /*ServerMessage Achievements = new ServerMessage(LibraryParser.OutgoingRequest("GameCenterGameAchievementsMessageComposer"));
            Achievements.AppendInteger(18);
            Achievements.AppendInteger(1);//count
            Achievements.AppendInteger(295);//id
            Achievements.AppendInteger(1);
            Achievements.AppendString("ACH_StoryChallengeChampion1");
            Achievements.AppendInteger(0);
            Achievements.AppendInteger(1);
            Achievements.AppendInteger(0);
            Achievements.AppendInteger(0);
            Achievements.AppendInteger(0);
            Achievements.AppendBool(false);
            Achievements.AppendString("games");
            Achievements.AppendString("elisa_habbo_stories");
            Achievements.AppendInteger(1);//max
            Achievements.AppendInteger(0);
            Achievements.AppendString("");
            Session.SendMessage(Achievements);
 
            //Game2WeeklyLeaderboardParser
            ServerMessage WeeklyLeaderboard = new ServerMessage(LibraryParser.OutgoingRequest("GameCenterGameAchievementsMessageComposer"));
            WeeklyLeaderboard.AppendInteger(2014);//year
            WeeklyLeaderboard.AppendInteger(49);
            WeeklyLeaderboard.AppendInteger(0);
            WeeklyLeaderboard.AppendInteger(0);
            WeeklyLeaderboard.AppendInteger(6526);
            WeeklyLeaderboard.AppendInteger(1);//count
            WeeklyLeaderboard.AppendInteger(Session.GetHabbo().Id);
            WeeklyLeaderboard.AppendInteger(0);
            WeeklyLeaderboard.AppendInteger(1);
            WeeklyLeaderboard.AppendString(Session.GetHabbo().UserName);
            WeeklyLeaderboard.AppendString(Session.GetHabbo().Look);
            WeeklyLeaderboard.AppendString(Session.GetHabbo().Gender);
            WeeklyLeaderboard.AppendInteger(1);
            WeeklyLeaderboard.AppendInteger(18);//gameId
            Session.SendMessage(WeeklyLeaderboard);
 
            //Game2WeeklyLeaderboardParser 2
            ServerMessage WeeklyLeaderboard2 = new ServerMessage(LibraryParser.OutgoingRequest("GameCenterLeaderboard2MessageComposer"));
            WeeklyLeaderboard2.AppendInteger(2014);//year
            WeeklyLeaderboard2.AppendInteger(49);
            WeeklyLeaderboard2.AppendInteger(0);
            WeeklyLeaderboard2.AppendInteger(0);
            WeeklyLeaderboard2.AppendInteger(6526);
            WeeklyLeaderboard2.AppendInteger(1);//count
            WeeklyLeaderboard2.AppendInteger(Session.GetHabbo().Id);
            WeeklyLeaderboard2.AppendInteger(0);
            WeeklyLeaderboard2.AppendInteger(1);
            WeeklyLeaderboard2.AppendString(Session.GetHabbo().UserName);
            WeeklyLeaderboard2.AppendString(Session.GetHabbo().Look);
            WeeklyLeaderboard2.AppendString(Session.GetHabbo().Gender);
            WeeklyLeaderboard2.AppendInteger(0);
            WeeklyLeaderboard2.AppendInteger(18);//gameId
            Session.SendMessage(WeeklyLeaderboard2);
 
            //Game2WeeklyLeaderboardParser 3
            ServerMessage WeeklyLeaderboard3 = new ServerMessage(LibraryParser.OutgoingRequest("GameCenterLeaderboard2MessageComposer"));
            WeeklyLeaderboard3.AppendInteger(2014);//year
            WeeklyLeaderboard3.AppendInteger(48);
            WeeklyLeaderboard3.AppendInteger(0);
            WeeklyLeaderboard3.AppendInteger(1);
            WeeklyLeaderboard3.AppendInteger(6526);
            WeeklyLeaderboard3.AppendInteger(1);//count
            WeeklyLeaderboard3.AppendInteger(Session.GetHabbo().Id);
            WeeklyLeaderboard3.AppendInteger(0);
            WeeklyLeaderboard3.AppendInteger(1);
            WeeklyLeaderboard3.AppendString(Session.GetHabbo().UserName);
            WeeklyLeaderboard3.AppendString(Session.GetHabbo().Look);
            WeeklyLeaderboard3.AppendString(Session.GetHabbo().Gender);
            WeeklyLeaderboard3.AppendInteger(0);
            WeeklyLeaderboard3.AppendInteger(18);//gameId
            Session.SendMessage(WeeklyLeaderboard3);*/

            ServerMessage GamesLeft = new ServerMessage(LibraryParser.OutgoingRequest("GameCenterGamesLeftMessageComposer"));
            GamesLeft.AppendInteger(18);
            GamesLeft.AppendInteger(-1);
            GamesLeft.AppendInteger(0);
            Session.SendMessage(GamesLeft);

            ServerMessage PreviousWinner = new ServerMessage(LibraryParser.OutgoingRequest("GameCenterPreviousWinnerMessageComposer"));
            PreviousWinner.AppendInteger(18);
            PreviousWinner.AppendInteger(0);//count
            /*
            PreviousWinner.AppendString("name");
            PreviousWinner.AppendString("figure");
            PreviousWinner.AppendString("gender");
            PreviousWinner.AppendInteger(0);//rank
            PreviousWinner.AppendInteger(0);//score
            */
            Session.SendMessage(PreviousWinner);

            ServerMessage Products = new ServerMessage(LibraryParser.OutgoingRequest("GameCenterProductsMessageComposer"));
            Products.AppendInteger(18);//gameId
            Products.AppendInteger(0);//count
            Products.AppendInteger(6526);
            Products.AppendBool(false);
            Session.SendMessage(Products);

            //ServerMessage AllAchievements = new ServerMessage(LibraryParser.OutgoingRequest("GameCenterAllAchievementsMessageComposer"));
            //AllAchievements.AppendInteger(0);//count
            /*
            For stories :
            PacketName5.AppendInteger(18);
            PacketName5.AppendInteger(1);
            PacketName5.AppendInteger(191);
            PacketName5.AppendString("StoryChallengeChampion");
            PacketName5.AppendInteger(20);
            */
            /*
            AllAchievements.AppendInteger(0);//gameId
            AllAchievements.AppendInteger(0);//count
            AllAchievements.AppendInteger(0);//achId
            AllAchievements.AppendString("SnowWarTotalScore");//achName
            AllAchievements.AppendInteger(0);//levels
            */
            //Session.SendMessage(AllAchievements);

            ServerMessage EnterInGame = new ServerMessage(LibraryParser.OutgoingRequest("GameCenterEnterInGameMessageComposer"));
            EnterInGame.AppendInteger(18);//gameId
            EnterInGame.AppendInteger(0);//0 : ok - 1 : error
            Session.SendMessage(EnterInGame);

        }
        internal void GameCenterJoinQueue()
        {
            ServerMessage JoinQueue = new ServerMessage(LibraryParser.OutgoingRequest("GameCenterJoinGameQueueMessageComposer"));
            JoinQueue.AppendInteger(18);//gameId
            Session.SendMessage(JoinQueue);

            ServerMessage LoadGame = new ServerMessage(LibraryParser.OutgoingRequest("GameCenterLoadGameUrlMessageComposer"));
            LoadGame.AppendInteger(18);//gameId
            LoadGame.AppendString(Convert.ToString(Azure.GetUnixTimestamp()));
            LoadGame.AppendString("http://azurehotel.com.br/photo/index.php");
            Session.SendMessage(LoadGame);
        }
    }
}