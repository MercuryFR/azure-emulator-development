﻿using System.Linq;
using Azure.HabboHotel.Catalogs;
using Azure.HabboHotel.Groups;
using Azure.Messages.Parsers;

namespace Azure.Messages.Handlers
{
    partial class GameClientMessageHandler
    {
        public void CatalogueIndex()
        {
            var rank = Session.GetHabbo().Rank;
            if (rank < 1)
                rank = 1;
            Session.SendMessage(CatalogPacket.ComposeIndex(rank, Request.GetString().ToUpper()));
        }

        public void CataloguePage()
        {
            var pageId = Request.GetUInteger16();
            int Num = Request.GetInteger();
            var cPage = Azure.GetGame().GetCatalog().GetPage(pageId);
            if (cPage == null || !cPage.Enabled || !cPage.Visible || cPage.MinRank > Session.GetHabbo().Rank)
                return;
            Session.SendMessage(cPage.CachedContentsMessage);
        }

        public void CatalogueClubPage()
        {
            var requestType = Request.GetInteger();
            Session.SendMessage(CatalogPacket.ComposeClubPurchasePage(Session, requestType));
        }

        public void ReloadEcotron()
        {
            Response.Init(LibraryParser.OutgoingRequest("ReloadEcotronMessageComposer"));
            Response.AppendInteger(1);
            Response.AppendInteger(0);
            SendResponse();
        }

        public void GiftWrappingConfig()
        {
            Response.Init(LibraryParser.OutgoingRequest("GiftWrappingConfigurationMessageComposer"));
            Response.AppendBool(true);
            Response.AppendInteger(1);
            Response.AppendInteger(Azure.GiftWrappers.GiftWrappersList.Count);
            foreach (var i in Azure.GiftWrappers.GiftWrappersList)
                Response.AppendInteger(i);
            Response.AppendInteger(8);
            Response.AppendInteger(0);
            Response.AppendInteger(1);
            Response.AppendInteger(2);
            Response.AppendInteger(3);
            Response.AppendInteger(4);
            Response.AppendInteger(5);
            Response.AppendInteger(6);
            Response.AppendInteger(8);
            Response.AppendInteger(11);
            Response.AppendInteger(0);
            Response.AppendInteger(1);
            Response.AppendInteger(2);
            Response.AppendInteger(3);
            Response.AppendInteger(4);
            Response.AppendInteger(5);
            Response.AppendInteger(6);
            Response.AppendInteger(7);
            Response.AppendInteger(8);
            Response.AppendInteger(9);
            Response.AppendInteger(10);
            Response.AppendInteger(Azure.GiftWrappers.OldGiftWrappers.Count);
            foreach (var i in Azure.GiftWrappers.OldGiftWrappers)
                Response.AppendInteger(i);
            SendResponse();
        }

        public void GetRecyclerRewards()
        {
            Response.Init(LibraryParser.OutgoingRequest("RecyclerRewardsMessageComposer"));
            var ecotronRewardsLevels = Azure.GetGame().GetCatalog().GetEcotronRewardsLevels();
            Response.AppendInteger(ecotronRewardsLevels.Count);
            foreach (var current in ecotronRewardsLevels)
            {
                Response.AppendInteger(current);
                Response.AppendInteger(current);
                var ecotronRewardsForLevel =
                    Azure.GetGame().GetCatalog().GetEcotronRewardsForLevel(uint.Parse(current.ToString()));
                Response.AppendInteger(ecotronRewardsForLevel.Count);
                foreach (var current2 in ecotronRewardsForLevel)
                {
                    Response.AppendString(current2.GetBaseItem().PublicName);
                    Response.AppendInteger(1);
                    Response.AppendString(current2.GetBaseItem().Type.ToString());
                    Response.AppendInteger(current2.GetBaseItem().SpriteId);
                }
            }
            SendResponse();
        }

        public void PurchaseItem()
        {
            var pageId = Request.GetUInteger16();
            var itemId = Request.GetInteger();
            var extraData = Request.GetString();
            var priceAmount = Request.GetInteger();
            Azure.GetGame().GetCatalog().HandlePurchase(Session, pageId, itemId, extraData, priceAmount, false, "", "", 0, 0, 0, false, 0u);
        }

        public void PurchaseGift()
        {
            var pageId = Request.GetUInteger16();
            var itemId = Request.GetInteger();
            var extraData = Request.GetString();
            var giftUser = Request.GetString();
            var giftMessage = Request.GetString();
            var giftSpriteId = Request.GetInteger();
            var giftLazo = Request.GetInteger();
            var giftColor = Request.GetInteger();
            var undef = Request.GetBool();
            Azure.GetGame().GetCatalog().HandlePurchase(Session, pageId, itemId, extraData, 1, true, giftUser, giftMessage, giftSpriteId, giftLazo, giftColor, undef, 0u);
        }

        public void CheckPetName()
        {
            var petName = Request.GetString();
            var i = 0;
            if (petName.Length > 15)
                i = 1;
            else if (petName.Length < 3)
                i = 2;
            else if (!Azure.IsValidAlphaNumeric(petName))
                i = 3;
            Response.Init(LibraryParser.OutgoingRequest("CheckPetNameMessageComposer"));
            Response.AppendInteger(i);
            Response.AppendString(petName);
            SendResponse();
        }

        public void CatalogueOffer()
        {
            var num = Request.GetInteger();
            var catalogItem = Azure.GetGame().GetCatalog().GetItemFromOffer(num);
            if (catalogItem == null || Catalog.LastSentOffer == num)
                return;
            Catalog.LastSentOffer = num;
            var message = new ServerMessage(LibraryParser.OutgoingRequest("CatalogOfferMessageComposer"));
            CatalogPacket.ComposeItem(catalogItem, message);
            Session.SendMessage(message);
        }

        public void CatalogueOfferConfig()
        {
            Response.Init(LibraryParser.OutgoingRequest("CatalogueOfferConfigMessageComposer"));
            Response.AppendInteger(100);
            Response.AppendInteger(6);
            Response.AppendInteger(1);
            Response.AppendInteger(1);
            Response.AppendInteger(2);
            Response.AppendInteger(40);
            Response.AppendInteger(99);
            SendResponse();
        }

        internal void SerializeGroupFurniPage()
        {
            try
            {


                var userGroups = Azure.GetGame().GetGroupManager().GetUserGroups(Session.GetHabbo().Id);
                Response.Init(LibraryParser.OutgoingRequest("GroupFurniturePageMessageComposer"));

                var responseList = new System.Collections.Generic.List<ServerMessage>();
                foreach (
                    var @group in
                        userGroups.Where(current => current != null).Select(
                            current => Azure.GetGame().GetGroupManager().GetGroup(current.GroupId)))
                {
                    if (@group == null)
                        continue;
                    var subResponse = new ServerMessage();
                    subResponse.AppendInteger(@group.Id);
                    subResponse.AppendString(@group.Name);
                    subResponse.AppendString(@group.Badge);
                    subResponse.AppendString(
                        Azure.GetGame().GetGroupManager().SymbolColours.Contains(@group.Colour1)
                            ? ((GroupSymbolColours)
                                Azure.GetGame().GetGroupManager().SymbolColours[@group.Colour1]).Colour
                            : "4f8a00");
                    subResponse.AppendString(
                        Azure.GetGame().GetGroupManager().BackGroundColours.Contains(@group.Colour2)
                            ? ((GroupBackGroundColours)
                                Azure.GetGame().GetGroupManager().BackGroundColours[@group.Colour2]).Colour
                            : "4f8a00");
                    subResponse.AppendBool(@group.CreatorId == Session.GetHabbo().Id);
                    subResponse.AppendInteger(@group.CreatorId);
                    subResponse.AppendBool(@group.HasForum);

                    responseList.Add(subResponse);
                }
                Response.AppendInteger(responseList.Count());
                Response.AppendServerMessages(responseList);

                responseList.Clear();
                responseList = null;

                SendResponse();

            }
            catch(System.Exception e)
            {
                System.Console.WriteLine(e);
            }

        }
    }
}