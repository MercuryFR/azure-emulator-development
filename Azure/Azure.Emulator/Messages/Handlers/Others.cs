﻿using System;
using System.Globalization;
using Azure.Configuration;
using Azure.Encryption;
using Azure.Encryption.Hurlant.Crypto.Prng;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Items;
using Azure.HabboHotel.Rooms;
using Azure.Messages.Parsers;

namespace Azure.Messages.Handlers
{
    partial class GameClientMessageHandler
    {
        internal Room CurrentLoadingRoom;

        protected GameClient Session;
        protected ClientMessage Request;
        protected ServerMessage Response;
        private string _photoData;

        internal GameClientMessageHandler(GameClient session)
        {
            Session = session;
            Response = new ServerMessage();
        }

        internal GameClient GetSession() { return Session; }

        internal ServerMessage GetResponse() { return Response; }

        internal void Destroy() { Session = null; }

        internal void HandleRequest(ClientMessage request)
        {
            Request = request;
            LibraryParser.HandlePacket(this, request);
        }

        internal void SendResponse()
        {
            if (Response != null && Response.Id > 0 && Session.GetConnection() != null)
                Session.GetConnection().SendData(Response.GetReversedBytes());
        }

        internal void AddStaffPick() { Session.SendNotif("You do not have permission to do that yet!"); }

        internal void GetClientVersionMessageEvent()
        {
            var release = Request.GetString();
            if (release.Contains("201409222303-304766480"))
            {
                Session.GetHabbo().ReleaseName = "304766480";
                Console.WriteLine("[Handled] Release Id: RELEASE63-201409222303-304766480");
            }
            else if (release.Contains("201411201226-580134750"))
            {
                Session.GetHabbo().ReleaseName = "304766480";
                Console.WriteLine("[Handled] Release Id: RELEASE63-201411201226-580134750");
            }
            else
                LibraryParser.ReleaseName = "Undefined Release";
        }

        internal void Pong() { Session.TimePingedReceived = DateTime.Now; }

        internal void DisconnectEvent() { Session.Disconnect("close window"); }

        internal void LatencyTest()
        {
            if (Session == null)
                return;
            Session.TimePingedReceived = DateTime.Now;
            GetResponse().Init(LibraryParser.OutgoingRequest("LatencyTestResponseMessageComposer"));
            GetResponse().AppendInteger(Request.GetIntegerFromString());
            SendResponse();
            Azure.GetGame().GetAchievementManager().ProgressUserAchievement(Session, "ACH_AllTimeHotelPresence", 1);
        }

        internal void Fuckyou() { }

        internal void InitCrypto()
        {
            if (LibraryParser.Config["Crypto.Enabled"] == "false")
            {
                Response.Init(LibraryParser.OutgoingRequest("InitCryptoMessageComposer"));
                Response.AppendString("Azure");
                Response.AppendString("Disabled Crypto");
                SendResponse();
                return;
            }
            Response.Init(LibraryParser.OutgoingRequest("InitCryptoMessageComposer"));
            Response.AppendString(Handler.GetRsaDiffieHellmanPrimeKey());
            Response.AppendString(Handler.GetRsaDiffieHellmanGeneratorKey());
            SendResponse();
        }

        internal void SecretKey()
        {
            var cipherKey = Request.GetString();
            var sharedKey = Handler.CalculateDiffieHellmanSharedKey(cipherKey);

            if (LibraryParser.Config["Crypto.Enabled"] == "false")
            {
                Response.Init(LibraryParser.OutgoingRequest("SecretKeyMessageComposer"));
                Response.AppendString("Crypto disabled");
                Response.AppendBool(false); //Rc4 clientside. 
                SendResponse();
                return;
            }
            if (sharedKey != 0)
            {
                Response.Init(LibraryParser.OutgoingRequest("SecretKeyMessageComposer"));
                Response.AppendString(Handler.GetRsaDiffieHellmanPublicKey());
                Response.AppendBool(false); //Rc4 clientside. 
                SendResponse();

                var data = sharedKey.ToByteArray();

                if (data[data.Length - 1] == 0)
                    Array.Resize(ref data, data.Length - 1);

                Array.Reverse(data, 0, data.Length);

                Session.ARC4 = new ARC4(data);
                //   Session.SecondaryRC4 = new ARC4(SharedKey.getBytes());
            }
            else
                Session.Disconnect("cyrpto error");
        }

        internal void MachineId()
        {
            Request.GetString();
            var machineId = Request.GetString();
            Session.MachineId = machineId;
        }

        internal void LoginWithTicket()
        {
            if (Session == null || Session.GetHabbo() != null)
                return;
            Session.TryLogin(Request.GetString());
            if (Session != null)
                Session.TimePingedReceived = DateTime.Now;
        }

        internal void InfoRetrieve()
        {
            if (Session == null || Session.GetHabbo() == null)
                return;
            var habbo = Session.GetHabbo();
            Response.Init(LibraryParser.OutgoingRequest("UserObjectMessageComposer"));
            Response.AppendInteger(habbo.Id);
            Response.AppendString(habbo.UserName);
            Response.AppendString(habbo.Look);
            Response.AppendString(habbo.Gender.ToUpper());
            Response.AppendString(habbo.Motto);
            Response.AppendString("");
            Response.AppendBool(false);
            Response.AppendInteger(habbo.Respect);
            Response.AppendInteger(habbo.DailyRespectPoints);
            Response.AppendInteger(habbo.DailyPetRespectPoints);
            Response.AppendBool(true);
            Response.AppendString(habbo.LastOnline.ToString(CultureInfo.InvariantCulture));
            Response.AppendBool(habbo.CanChangeName);
            Response.AppendBool(false);
            SendResponse();
            Response.Init(LibraryParser.OutgoingRequest("BuildersClubMembershipMessageComposer"));
            Response.AppendInteger(Session.GetHabbo().BuildersExpire);
            Response.AppendInteger(Session.GetHabbo().BuildersItemsMax);
            Response.AppendInteger(2);
            SendResponse();
            var tradeLocked = Session.GetHabbo().CheckTrading();
            var canUseFloorEditor = (ExtraSettings.EVERYONE_USE_FLOOR || Session.GetHabbo().VIP ||
                                     Session.GetHabbo().Rank >= 4);
            Response.Init(LibraryParser.OutgoingRequest("SendPerkAllowancesMessageComposer"));
            Response.AppendInteger(14);
            Response.AppendString("NEW_UI");
            Response.AppendString("");
            Response.AppendBool(true);
            Response.AppendString("EXPERIMENTAL_TOOLBAR");
            Response.AppendString(""); // DISABLED: requirement.unfulfilled.group_membership
            Response.AppendBool(true);
            Response.AppendString("BUILDER_AT_WORK");
            Response.AppendString("");
            Response.AppendBool(canUseFloorEditor);
            Response.AppendString("CALL_ON_HELPERS");
            Response.AppendString("");
            Response.AppendBool(true);
            Response.AppendString("NAVIGATOR_PHASE_TWO_2014");
            Response.AppendString("");
            Response.AppendBool(ExtraSettings.NAVIGATOR_NEW_ENABLED);
            Response.AppendString("VOTE_IN_COMPETITIONS");
            Response.AppendString("requirement.unfulfilled.helper_level_2");
            Response.AppendBool(false);
            Response.AppendString("NAVIGATOR_PHASE_ONE_2014");
            Response.AppendString("");
            Response.AppendBool(true);
            Response.AppendString("CAMERA");
            Response.AppendString("");
            Response.AppendBool(true);
            Response.AppendString("CITIZEN");
            Response.AppendString("");
            Response.AppendBool(Session.GetHabbo().TalentStatus == "helper" ||
                                Session.GetHabbo().CurrentTalentLevel >= 4);
            Response.AppendString("USE_GUIDE_TOOL");
            Response.AppendString((Session.GetHabbo().TalentStatus == "helper" &&
                                   Session.GetHabbo().CurrentTalentLevel >= 4) ||
                                  (Session.GetHabbo().Rank >= 4)
                ? ""
                : "requirement.unfulfilled.helper_level_4");
            Response.AppendBool((Session.GetHabbo().TalentStatus == "helper" &&
                                 Session.GetHabbo().CurrentTalentLevel >= 4) ||
                                (Session.GetHabbo().Rank >= 4));
            Response.AppendString("HEIGHTMAP_EDITOR_BETA");
            Response.AppendString("");
            Response.AppendBool(canUseFloorEditor);
            Response.AppendString("JUDGE_CHAT_REVIEWS");
            Response.AppendString("requirement.unfulfilled.helper_level_6");
            Response.AppendBool(false);
            Response.AppendString("TRADE");
            Response.AppendString(tradeLocked ? "" : "requirement.unfulfilled.no_trade_lock");
            Response.AppendBool(tradeLocked);
            Response.AppendString("EXPERIMENTAL_CHAT_BETA");
            Response.AppendString("");
            Response.AppendBool(true);
            SendResponse();
            Session.GetHabbo().InitMessenger();
            GetResponse().Init(LibraryParser.OutgoingRequest("CitizenshipStatusMessageComposer"));
            GetResponse().AppendString("citizenship");
            GetResponse().AppendInteger(1);
            GetResponse().AppendInteger(4);
            SendResponse();

            GetResponse().Init(LibraryParser.OutgoingRequest("GameCenterGamesListMessageComposer"));
            GetResponse().AppendInteger(1);
            GetResponse().AppendInteger(18);
            GetResponse().AppendString("elisa_habbo_stories");
            GetResponse().AppendString("000000");
            GetResponse().AppendString("ffffff");
            GetResponse().AppendString("http://localhost/photo/");
            GetResponse().AppendString("");
            SendResponse();
            Session.SendMessage(Azure.GetGame().GetNavigator().SerializePromotionCategories());
        }

        internal void HabboCamera()
        {
            //string one = this.Request.GetString();
            /*var two = */Request.GetInteger();
            _photoData =
                "{\"t\":1392233862000,\"u\":\"52fbcd84e4b09ba9e304112e\",\"m\":\"Happy 2014 Valentine's day <3\",\"s\":53517374,\"w\":\"http://localhost/supersecret/c_images/photos/komok.png\"}";
                //" + Azure.GetRandomNumber(0, int.MaxValue) + " -hhes-" + Azure.GetRandomNumber(0, 99) + RandomString(2) + Azure.GetRandomNumber(0, 9) + RandomString(2) + Azure.GetRandomNumber(0, 99) + RandomString(1) + Azure.GetRandomNumber(0, 9) + RandomString(3) + Azure.GetRandomNumber(0, 999) + RandomString(2) + Azure.GetRandomNumber(0, 9999) + RandomString(1) + Azure.GetRandomNumber(0, 99) + RandomString(3) + Azure.GetRandomNumber(0, 999) + ".png\"}";// http://localhost/supersecret/c_images/photos/komok.png\"}";
        }

        internal void OnClick()
        {
            // System by Komok
            // Powered by Azure

            var section = Request.GetString();
            var subSection = Request.GetString();
            var action = Request.GetString();

            switch (section.ToLower())
            {
                case "stories":
                {
                    switch (subSection.ToLower())
                    {
                        case "camera":
                        {
                            switch (action.ToLower())
                            {
                                case "stories.photo.purchase.attempt":
                                {
                                    HabboCamera();
                                    //Console.WriteLine("Buying photo... || Photo Data: " + _photoData);
                                    var item = Session.GetHabbo()
                                        .GetInventoryComponent()
                                        .AddNewItem(0, Azure.GetGame().GetItemManager().PhotoId, _photoData, 0, true, false, 0, 0);
                                    Session.GetHabbo().GetInventoryComponent().UpdateItems(false);

                                    Session.GetHabbo().Credits -= 3;
                                    Session.GetHabbo().UpdateCreditsBalance();
                                    Session.GetHabbo().GetInventoryComponent().SendNewItems(item.Id);
                                    Session.SendNotif("You received this item! -> " + item.BaseItem.Name +
                                                      ", you have now -3 credits, total -> " +
                                                      Session.GetHabbo().Credits);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }

        private static int GetFriendsCount(uint userId)
        {
            int result;
            using (var queryReactor = Azure.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.SetQuery(
                    "SELECT COUNT(*) FROM messenger_friendships WHERE user_one_id = @id OR user_two_id = @id;");
                queryReactor.AddParameter("id", userId);
                result = queryReactor.GetInteger();
            }
            return result;
        }

        internal void TargetedOfferBuy()
        {
            int offerId = Request.GetInteger();
        }

        internal void GoToRoomByName()
        {
            string name = Request.GetString();
            switch (name)
            {
                case "predefined_noob_lobby":
                    var roomFwd = new ServerMessage(LibraryParser.OutgoingRequest("RoomForwardMessageComposer"));
                    roomFwd.AppendInteger(Convert.ToInt32(Azure.GetDbConfig().DbData["noob.lobby.roomid"]));
                    Session.SendMessage(roomFwd);
                    break;
            }
        }
        internal void GetUCPanel()
        {
            string name = Request.GetString();
            switch (name)
            {
                case "new":

                    break;
                case "camera":
                    var camera = new ServerMessage(1610);
                    camera.AppendString("token");
                    camera.AppendString("?????");
                    Session.SendMessage(camera);
                    break;
            }
        }

        internal void GetUCPanelHotel()
        {
            int id = Request.GetInteger();
        }
    }
}