using System.Collections.Generic;
using Azure.Connection.Connection;

namespace Azure.Messages
{
    public class QueuedServerMessage
    {
        private readonly List<byte> _packet;
        private ConnectionInformation _userConnection;

        public QueuedServerMessage(ConnectionInformation connection)
        {
            _userConnection = connection;
            _packet = new List<byte>();
        }

        internal byte[] GetPacket
        {
            get { return _packet.ToArray(); }
        }

        internal void Dispose()
        {
            _packet.Clear();
            _userConnection = null;
        }

        internal void AppendResponse(ServerMessage message) { AppendBytes(message.GetReversedBytes()); }

        internal void AddBytes(byte[] bytes) { AppendBytes(bytes); }

        internal void SendResponse()
        {
            if (_userConnection != null)
                _userConnection.SendMuchData(_packet.ToArray());
            Dispose();
        }

        private void AppendBytes(IEnumerable<byte> bytes) { _packet.AddRange(bytes); }
    }
}