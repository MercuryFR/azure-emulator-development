using System;
using System.Collections;
using System.Text;
using Azure.Connection.Connection.LoggingSystem;
using Azure.Database.Manager.Database;

namespace Azure.Messages
{
    class MessageLoggerManager
    {
        private static Queue _loggedMessages;
        private static bool _enabled;
        private static DateTime _timeSinceLastPacket;

        internal static bool Enabled
        {
            get { return _enabled; }
            set
            {
                _enabled = value;
                if (_enabled)
                    _loggedMessages = new Queue();
            }
        }

        internal static void AddMessage(byte[] data, int connectionId, LogState state)
        {
            if (!_enabled)
                return;
            string data2;
            switch (state)
            {
                case LogState.ConnectionOpen:
                    data2 = "CONCLOSE";
                    break;
                case LogState.ConnectionClose:
                    data2 = "CONOPEN";
                    break;
                default:
                    data2 = Encoding.Default.GetString(data);
                    break;
            }
            lock (_loggedMessages.SyncRoot)
            {
                var message = new Message(connectionId, GenerateTimestamp(), data2);
                _loggedMessages.Enqueue(message);
            }
        }

        internal static void Save()
        {
            if (!_enabled)
                return;
            lock (_loggedMessages.SyncRoot)
            {
                if (_loggedMessages.Count <= 0)
                    return;
                var databaseManager = new DatabaseManager(1u, 1u);
                using (var queryReactor = databaseManager.GetQueryReactor())
                    while (_loggedMessages.Count > 0)
                    {
                        var message = (Message) _loggedMessages.Dequeue();
                        queryReactor.SetQuery(
                            "INSERT INTO system_packetlog (connectionid, timestamp, data) VALUES @connectionid @timestamp, @data");
                        queryReactor.AddParameter("connectionid", message.ConnectionID);
                        queryReactor.AddParameter("timestamp", message.GetTimestamp);
                        queryReactor.AddParameter("data", message.GetData);
                        queryReactor.RunQuery();
                    }
            }
        }

        private static int GenerateTimestamp()
        {
            var now = DateTime.Now;
            var timeSpan = now - _timeSinceLastPacket;
            _timeSinceLastPacket = now;
            return checked((int) timeSpan.TotalMilliseconds);
        }
    }
}