using System;

namespace Azure.Messages
{
    class HabboEncoding
    {
        internal static int DecodeInt32(byte[] v)
        {
            if ((v[0] | v[1] | v[2] | v[3]) < 0)
                return -1;
            return ((v[0] << 24) + (v[1] << 16) + (v[2] << 8) + (v[3]));
        }

        internal static Int16 DecodeInt16(byte[] v)
        {
            if ((v[0] | v[1]) < 0)
                return -1;
            var result = ((v[0] << 8) + (v[1]));
            return (Int16) result;
        }

        public static string GetCharFilter(string data)
        {
            for (var i = 0; i <= 13; i++)
                data = data.Replace(Convert.ToChar(i) + "", "[" + i + "]");
            return data;
        }
    }
}