using System;
using System.Collections.Generic;

namespace Azure.Messages
{
    class ServerMessage
    {
        private List<byte> _message = new List<byte>();

        public ServerMessage() { Id = 0; }

        public ServerMessage(int header)
        {
            Id = 0;
            Init(header);
        }

        public int Id { get; private set; }

        public void Init(int header)
        {
            _message = new List<byte>();
            Id = header;
            AppendShort(header);
        }

        public void SetInt(int i, int startOn)
        {
            try
            {
                var n = _message;
                var intvalue = AppendBytesTo(BitConverter.GetBytes(i), true);
                n.RemoveRange(startOn, intvalue.Count);
                n.InsertRange(startOn, intvalue);
                _message = n;
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}{1}", @"Error on setInt: ", e);
            }
        }

        public void AppendServerMessage(ServerMessage message) { AppendBytes(message.GetBytes(), false); }

        public void AppendServerMessages(List<ServerMessage> messages)
        {
            foreach (var message in messages)
                AppendServerMessage(message);
        }

        public void AppendShort(int i)
        {
            var s = (short) i;
            AppendBytes(BitConverter.GetBytes(s), true);
        }

        public void AppendInteger(int i) { AppendBytes(BitConverter.GetBytes(i), true); }

        public void AppendInteger(uint i) { AppendInteger((int) i); }

        public void AppendBool(bool b) { AppendBytes(new[] {(byte) (b ? 1 : 0)}, false); }

        public void AppendString(string s)
        {
            var toAdd = Azure.GetDefaultEncoding().GetBytes(s);
            AppendShort(toAdd.Length);
            AppendBytes(toAdd, false);
        }

        public void AppendBytes(byte[] b, bool isInt)
        {
            if (isInt)
                for (var i = (b.Length - 1); i > -1; i--)
                    _message.Add(b[i]);
            else
                _message.AddRange(b);
        }

        public void AppendByted(int number) { AppendBytes(new[] {(byte) number}, false); }

        public List<byte> AppendBytesTo(byte[] b, bool isInt)
        {
            var message = new List<byte>();
            if (isInt)
                for (var i = (b.Length - 1); i > -1; i--)
                    message.Add(b[i]);
            else
                message.AddRange(b);
            return message;
        }

        public byte[] GetBytes() { return _message.ToArray(); }

        public byte[] GetReversedBytes()
        {
            var final = new List<byte>();
            final.AddRange(BitConverter.GetBytes(_message.Count));
            final.Reverse();
            final.AddRange(_message);
            return final.ToArray();
        }

        public override string ToString()
        {
            return (HabboEncoding.GetCharFilter(Azure.GetDefaultEncoding().GetString(GetReversedBytes())));
        }
    }
}