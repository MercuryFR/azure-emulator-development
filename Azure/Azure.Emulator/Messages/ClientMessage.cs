using System;
using System.Globalization;
using System.Text;
using Azure.Messages.Parsers;

namespace Azure.Messages
{
    public class ClientMessage : IDisposable
    {
        private byte[] _body;
        private int _pointer;

        internal ClientMessage(int messageId, byte[] body) { Init(messageId, body); }

        internal int Id { get; private set; }

        internal int RemainingLength
        {
            get { return checked(_body.Length - _pointer); }
        }

        public void Dispose()
        {
            Factorys.ObjectCallback(this);
            GC.SuppressFinalize(this);
        }

        public override string ToString()
        {
            var str = string.Format(" ({0}) ", Id);
            str += Encoding.Default.GetString(_body);
            for (var i = 0; i < 13; i++)
                str = str.Replace(char.ToString(Convert.ToChar(i)), string.Format("[{0}]", i));
            return str;
        }

        internal void Init(int messageId, byte[] body)
        {
            if (body == null)
                body = new byte[0];
            Id = messageId;
            _body = body;
            _pointer = 0;
        }

        internal byte[] ReadBytes(int bytes)
        {
            if (bytes > RemainingLength)
                bytes = RemainingLength;
            var array = new byte[bytes];
            checked
            {
                for (var i = 0; i < bytes; i++)
                    array[i] = _body[unchecked(_pointer++)];
                return array;
            }
        }

        internal byte[] GetBytes(int bytes)
        {
            if (bytes > RemainingLength)
                bytes = RemainingLength;
            var array = new byte[bytes];
            var i = 0;
            var num = _pointer;
            checked
            {
                while (i < bytes)
                {
                    array[i] = _body[num];
                    i++;
                    num++;
                }
                return array;
            }
        }

        internal byte[] GetNext()
        {
            int bytes = HabboEncoding.DecodeInt16(ReadBytes(2));
            return ReadBytes(bytes);
        }

        internal string GetString() { return GetString(Azure.GetDefaultEncoding()); }

        internal string GetString(Encoding encoding) { return encoding.GetString(GetNext()); }

        internal int GetIntegerFromString()
        {
            int result;
            var s = GetString(Encoding.ASCII);
            int.TryParse(s, out result);
            return result;
        }

        internal bool GetBool() { return RemainingLength > 0 && (char) _body[_pointer++] == Convert.ToChar(1); }

        internal short GetInteger16() { return short.Parse(GetInteger().ToString(CultureInfo.InvariantCulture)); }
        internal int GetInteger()
        {
            if (RemainingLength < 1)
                return 0;
            var v = GetBytes(4);
            var result = HabboEncoding.DecodeInt32(v);
            checked
            {
                _pointer += 4;
                return result;
            }
        }

        internal uint GetUInteger() { return Convert.ToUInt32(GetInteger()); }

        internal ushort GetUInteger16() { return Convert.ToUInt16(GetInteger()); }
    }
}