using System;

namespace Azure.Connection.Connection.LoggingSystem
{
    internal struct Message
    {
        public Message(int connectionID, int timeStamp, string data) : this()
        {
            this.ConnectionID = connectionID;
            this.GetTimestamp = timeStamp;
            this.GetData = data;
        }
        internal int ConnectionID { get; private set; }
        internal int GetTimestamp { get; private set; }
        internal string GetData { get; private set; }
    }
}