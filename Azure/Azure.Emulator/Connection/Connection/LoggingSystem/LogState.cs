using System;

namespace Azure.Connection.Connection.LoggingSystem
{
    internal enum LogState
    {
        Normal,
        ConnectionOpen,
        ConnectionClose
    }
}