using System;

namespace Azure.Connection.Connection
{
    public enum ConnectionState
    {
        Open,
        Closed,
        MalfunctioningPacket
    }
}