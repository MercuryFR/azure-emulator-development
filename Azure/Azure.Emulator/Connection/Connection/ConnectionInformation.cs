using System;
using System.Net.Sockets;
using System.Text;
using Azure.Configuration;
using Azure.Connection.Connection.LoggingSystem;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Users;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.Connection.Connection
{
    public class ConnectionInformation : IDisposable
    {
        public static bool DisableSend;
        public static bool DisableReceive;
        private readonly Socket _dataSocket;
        private readonly NewSocketManager _manager;
        private readonly string _ip;
        private readonly int _connectionId;
        private readonly byte[] _buffer;
        private readonly AsyncCallback _sendCallback;
        private bool _isConnected;
        private GameClient _client;

        public ConnectionInformation(Socket dataStream, int connectionId, NewSocketManager manager, IDataParser parser,
            string ip)
        {
            try
            {
                Parser = parser;
                _buffer = new byte[GameSocketManagerStatics.BufferSize];
                _manager = manager;
                _dataSocket = dataStream;
                _dataSocket.SendBufferSize = GameSocketManagerStatics.BufferSize;
                _ip = ip;
                _connectionId = connectionId;
                _sendCallback = SentData;
                if (ConnectionChanged != null)
                    ConnectionChanged(this, ConnectionState.Open);
                MessageLoggerManager.AddMessage(null, connectionId, LogState.ConnectionOpen);
            }
            catch
            {
            }
        }

        public GameClient Client
        {
            get { return _client; }
            set { _client = value; }
        }

        public NewSocketManager Manager
        {
            get { return _manager; }
        }

        public delegate void ConnectionChange(ConnectionInformation information, ConnectionState state);

        public event ConnectionChange ConnectionChanged;
        public IDataParser Parser { get; set; }

        public void SetClient(GameClient client) { _client = client; }

        public void StartPacketProcessing()
        {
            if (_isConnected)
                return;
            _isConnected = true;
            try
            {
                _dataSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, IncomingDataPacket, _dataSocket);
            }
            catch
            {
                Disconnect();
            }
        }

        public string GetIp() { return _ip; }

        public int GetConnectionId() { return _connectionId; }

        public void Dispose()
        {
            if (_isConnected)
                Disconnect();
        }

        public void SendData(byte[] packet) { SendData1(packet); }

        public void SendMuchData(byte[] packet) { SendData1(packet); }

        public void SendbData(byte[] packet) { SendData1(packet); }

        public void SendUnsafeData(byte[] packet)
        {
            if (!_isConnected || DisableSend)
                return;
            _dataSocket.BeginSend(packet, 0, packet.Length, SocketFlags.None, _sendCallback, null);
        }

        internal void Disconnect()
        {
            try
            {
                if (!_isConnected)
                    return;
                _isConnected = false;
                MessageLoggerManager.AddMessage(null, _connectionId, LogState.ConnectionClose);
                try
                {
                    if (_dataSocket != null && _dataSocket.Connected)
                    {
                        _dataSocket.Shutdown(SocketShutdown.Both);
                        _dataSocket.Close();
                    }
                }
                catch
                {
                }

                if(_dataSocket != null)
                    _dataSocket.Dispose();
                Parser.Dispose();
                try
                {
                    if (ConnectionChanged != null)
                        ConnectionChanged(this, ConnectionState.Closed);
                }
                catch
                {
                }
                ConnectionChanged = null;
            }
            catch
            {
            }
        }

        private void IncomingDataPacket(IAsyncResult iAr)
        {
            int num;
            try
            {
                num = _dataSocket.EndReceive(iAr);
            }
            catch
            {
                Disconnect();
                return;
            }
            if (num != 0)
            {
                try
                {
                    if (!DisableReceive)
                    {
                        var array = new byte[num];
                        Array.Copy(_buffer, array, num);
                        MessageLoggerManager.AddMessage(array, _connectionId, LogState.Normal);
                        HandlePacketData(array);
                    }
                }
                catch
                {
                    Disconnect();
                }
                finally
                {
                    try
                    {
                        _dataSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, IncomingDataPacket,
                            _dataSocket);
                    }
                    catch
                    {
                        Disconnect();
                    }
                }
                return;
            }
            Disconnect();
        }

        private void HandlePacketData(byte[] packet)
        {
            if (Parser != null)
                Parser.HandlePacketData(packet);
        }

        private void SendData1(byte[] packet)
        {
            try
            {
                SendUnsafeData(packet);
            }
            catch
            {
                Disconnect();
            }
        }

        private void SentData(IAsyncResult iAr)
        {
            try
            {
                if (_dataSocket == null || !_dataSocket.Connected)
                {
                    Disconnect();
                    return;
                }

                _dataSocket.EndSend(iAr);
            }
            catch
            {
                Disconnect();
            }
        }
    }
}