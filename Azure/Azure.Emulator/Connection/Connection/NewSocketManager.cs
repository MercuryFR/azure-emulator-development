﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Net.Sockets;
using Azure.Messages.Parsers;

namespace Azure.Connection.Connection
{
    public class NewSocketManager
    {
        private int acceptedConnections;
        private Socket connectionListener;
        private int portInformation;
        private bool acceptConnections;
        private IDataParser parser;
        private bool disableNagleAlgorithm;
        public delegate void ConnectionEvent(ConnectionInformation connection);
        public event NewSocketManager.ConnectionEvent connectionEvent;
        public int MaximumConnections { get; set; }
        public int MaxIpConnectionCount { get; set; }
        public HybridDictionary IpConnectionCount { get; set; }
        public HybridDictionary ActiveConnections { get; set; }
        public void Init(int portID, int maxConnections, int connectionsPerIP, IDataParser parser, bool disableNaglesAlgorithm)
        {
            this.parser = parser;
            this.disableNagleAlgorithm = disableNaglesAlgorithm;
            this.InitializeFields();
            this.MaximumConnections = maxConnections;
            this.portInformation = portID;
            this.MaxIpConnectionCount = connectionsPerIP;
            this.acceptedConnections = 0;
            this.PrepareConnectionDetails();
        }
        public void InitializeConnectionRequests()
        {
            this.connectionListener.Listen(100);
            this.acceptConnections = true;
            string hostName = Dns.GetHostName();
            Dns.GetHostEntry(hostName);
            try
            {
                this.connectionListener.BeginAccept(new AsyncCallback(this.NewConnectionRequest), this.connectionListener);
            }
            catch
            {
                this.Destroy();
            }
        }
        public void Destroy()
        {
            this.acceptConnections = false;
            try
            {
                this.connectionListener.Close();
            }
            catch
            {
            }
            this.connectionListener = null;
        }
        public void ReportDisconnect(ConnectionInformation gameConnection)
        {
            gameConnection.ConnectionChanged -= new ConnectionInformation.ConnectionChange(this.CConnectionChanged);
            this.ReportUserLogout(gameConnection.GetIp());
        }
        public int GetAcceptedConnections()
        {
            return this.acceptedConnections;
        }
        internal bool IsConnected()
        {
            return this.connectionListener != null;
        }
        private void InitializeFields()
        {
            this.ActiveConnections = new HybridDictionary();
            this.IpConnectionCount = new HybridDictionary();
        }
        private void PrepareConnectionDetails()
        {
            this.connectionListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.connectionListener.NoDelay = this.disableNagleAlgorithm;
            try
            {
                this.connectionListener.Bind(new IPEndPoint(IPAddress.Any, this.portInformation));
            }
            catch (SocketException ex)
            {
                throw new SocketInitializationException(ex.Message);
            }
        }
        private void NewConnectionRequest(IAsyncResult iAr)
        {
            checked
            {
                if (this.connectionListener != null && this.acceptConnections)
                {
                    try
                    {
                        Socket socket = ((Socket)iAr.AsyncState).EndAccept(iAr);
                        socket.NoDelay = this.disableNagleAlgorithm;
                        string ip = socket.RemoteEndPoint.ToString().Split(new char[]
                        {
                            ':'
                        })[0];
                        this.acceptedConnections++;
                        var connectionInformation = new ConnectionInformation(socket, this.acceptedConnections, this, this.parser.Clone() as IDataParser, ip);
                        this.ReportUserLogin(ip);
                        connectionInformation.ConnectionChanged += new ConnectionInformation.ConnectionChange(this.CConnectionChanged);
                        if (this.connectionEvent != null)
                        {
                            this.connectionEvent(connectionInformation);
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        this.connectionListener.BeginAccept(new AsyncCallback(this.NewConnectionRequest), this.connectionListener);
                    }
                }
            }
        }

        private void CConnectionChanged(ConnectionInformation information, ConnectionState state)
        {
            if (state == ConnectionState.Closed)
            {
                this.ReportDisconnect(information);
            }
        }

        private void ReportUserLogin(string ip)
        {
            this.AlterIpConnectionCount(ip, checked(this.GetAmountOfConnectionFromIp(ip) + 1));
        }

        private void ReportUserLogout(string ip)
        {
            this.AlterIpConnectionCount(ip, checked(this.GetAmountOfConnectionFromIp(ip) - 1));
        }

        private void AlterIpConnectionCount(string ip, int amount)
        {
        }

        private int GetAmountOfConnectionFromIp(string ip)
        {
            return 0;
        }
    }
}