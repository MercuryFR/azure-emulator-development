using System;

namespace Azure.Connection.Connection
{
    public class GameSocketManagerStatics
    {
        public static readonly int BufferSize = 1024;
        public static readonly int MaxPacketSize = checked(GameSocketManagerStatics.BufferSize - 4);
    }
}