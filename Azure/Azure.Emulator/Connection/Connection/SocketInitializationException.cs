using System;

namespace Azure.Connection.Connection
{
    internal class SocketInitializationException : Exception
    {
        public SocketInitializationException(string message) : base(message)
        {
        }
    }
}