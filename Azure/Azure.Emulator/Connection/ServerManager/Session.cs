using System;
using System.Net.Sockets;
using System.Text;
using Azure.Configuration;

namespace Azure.Connection.ServerManager
{
    internal class Session
    {
        private readonly Socket mSock;
        private readonly AsyncCallback mReceivedCallback;
        private readonly byte[] mDataBuffer;
        private readonly string mIP;
        private readonly string mLongIP;
        private bool mClosed;

        public Session(Socket pSock)
        {
            this.mSock = pSock;
            this.mDataBuffer = new byte[1024];
            this.mReceivedCallback = new AsyncCallback(this.BytesReceived);
            this.mClosed = false;
            Logging.WriteLine("Received connection", global::Azure.ConsoleColor.Violet);
            this.mIP = this.mSock.RemoteEndPoint.ToString().Split(new char[]
            {
                ':'
            })[0];
            this.mLongIP = pSock.RemoteEndPoint.ToString();
            this.SendData("authreq");
            this.ContinueListening();
        }

        internal int Disconnection { get; set; }

        internal int DisconnectionError { get; set; }

        public string MLongIP
        {
            get
            {
                return this.mLongIP;
            }
        }

        public string Mip
        {
            get
            {
                return this.mIP;
            }
        }

        internal string GetLongIP
        {
            get
            {
                return this.mSock.RemoteEndPoint.ToString();
            }
        }

        internal void SendMessage(string data)
        {
            this.SendData(data);
        }

        internal void Close()
        {
            if (this.mClosed)
            {
                return;
            }
            this.mClosed = true;
            try
            {
                this.mSock.Close();
            }
            catch
            {
            }
            SessionManagement.RemoveSession(this);
            Logging.WriteLine("Reached end of session", global::Azure.ConsoleColor.Violet);
        }

        internal bool GetState()
        {
            bool result;
            try
            {
                result = this.mSock.Connected;
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void BytesReceived(IAsyncResult pIar)
        {
            try
            {
                int num = this.mSock.EndReceive(pIar);
                try
                {
                    byte[] destinationArray = new byte[num];
                    Array.Copy(this.mDataBuffer, destinationArray, num);
                    string[] array = Encoding.Default.GetString(this.mDataBuffer, 0, num).Split(new char[]
                    {
                        '|'
                    });
                    string[] array2 = array;
                    for (int i = 0; i < array2.Length; i++)
                    {
                        string text = array2[i];
                        if (!string.IsNullOrEmpty(text))
                        {
                            string[] array3 = text.Split(new char[]
                            {
                                ':'
                            });
                            if (array3[0].Length == 0)
                            {
                                this.Close();
                                return;
                            }
                        }
                    }
                    this.ContinueListening();
                }
                catch
                {
                    this.Close();
                }
            }
            catch
            {
                this.Close();
            }
        }

        private void ContinueListening()
        {
            try
            {
                this.mSock.BeginReceive(this.mDataBuffer, 0, this.mDataBuffer.Length, SocketFlags.None, this.mReceivedCallback, this);
            }
            catch
            {
                this.Close();
            }
        }

        private void SendData(string pData)
        {
            try
            {
                byte[] bytes = Encoding.Default.GetBytes(string.Format("{0}|", pData));
                this.mSock.Send(bytes);
            }
            catch
            {
                this.Close();
            }
        }
    }
}