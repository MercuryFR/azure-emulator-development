using System;
using Azure.Messages.Parsers;

namespace Azure.Connection.Net
{
    public class InitialPacketParser : IDataParser, IDisposable, ICloneable
    {
        public byte[] CurrentData;

        public delegate void NoParamDelegate();

        public event NoParamDelegate PolicyRequest;
        public event NoParamDelegate SwitchParserRequest;

        public void HandlePacketData(byte[] packet)
        {
            if (packet[0] == 60 && this.PolicyRequest != null)
            {
                this.PolicyRequest();
                return;
            }
            if (packet[0] != 67 && this.SwitchParserRequest != null)
            {
                this.CurrentData = packet;
                this.SwitchParserRequest();
            }
        }

        public void Dispose()
        {
            this.PolicyRequest = null;
            this.SwitchParserRequest = null;
        }

        public object Clone()
        {
            return new InitialPacketParser();
        }
    }
}