using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Azure.Connection.Net;

namespace Azure.Connection.Net
{
    internal class MusSocket
    {
        internal Socket MsSocket;
        internal string MusIp;
        internal int MusPort;
        internal HashSet<string> AllowedIps;

        internal MusSocket(string musIp, int musPort, string[] allowedIps, int backlog)
        {
            this.MusIp = musIp;
            this.MusPort = musPort;
            this.AllowedIps = new HashSet<string>();
            for (int i = 0; i < allowedIps.Length; i++)
            {
                string item = allowedIps[i];
                this.AllowedIps.Add(item);
            }
            try
            {
                this.MsSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                this.MsSocket.Bind(new IPEndPoint(IPAddress.Any, this.MusPort));
                this.MsSocket.Listen(backlog);
                this.MsSocket.BeginAccept(new AsyncCallback(this.OnEvent_NewConnection), this.MsSocket);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(string.Format("No se pudo iniciar el Socket MUS:\n{0}", ex.ToString()));
            }
        }

        internal void OnEvent_NewConnection(IAsyncResult iAr)
        {
            try
            {
                Socket socket = ((Socket)iAr.AsyncState).EndAccept(iAr);
                string text = socket.RemoteEndPoint.ToString().Split(new char[]
                {
                    ':'
                })[0];
                if (this.AllowedIps.Contains(text) || text == "127.0.0.1")
                {
                    new MusConnection(socket);
                }
                else
                {
                    socket.Close();
                }
            }
            catch (Exception)
            {
            }
            this.MsSocket.BeginAccept(new AsyncCallback(this.OnEvent_NewConnection), this.MsSocket);
        }
    }
}