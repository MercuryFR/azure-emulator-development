using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;
using Azure.HabboHotel.Rooms;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.Connection.Net
{
    internal class MusConnection
    {
        private Socket socket;
        private byte[] buffer = new byte[1024];

        internal MusConnection(Socket socket)
        {
            this.socket = socket;
            try
            {
                this.socket.BeginReceive(this.buffer, 0, this.buffer.Length, SocketFlags.None, new AsyncCallback(this.OnEvent_RecieveData), this.socket);
            }
            catch
            {
                this.TryClose();
            }
        }

        public static string MergeParams(string[] @params, int start)
        {
            var stringBuilder = new StringBuilder();
            checked
            {
                for (int i = 0; i < @params.Length; i++)
                {
                    if (i >= start)
                    {
                        if (i > start)
                        {
                            stringBuilder.Append(" ");
                        }
                        stringBuilder.Append(@params[i]);
                    }
                }
                return stringBuilder.ToString();
            }
        }

        internal void TryClose()
        {
            try
            {
                this.socket.Shutdown(SocketShutdown.Both);
                this.socket.Close();
                this.socket.Dispose();
            }
            catch
            {
            }
            this.socket = null;
            this.buffer = null;
        }

        internal void OnEvent_RecieveData(IAsyncResult iAr)
        {
            try
            {
                int count = 0;
                try
                {
                    count = this.socket.EndReceive(iAr);
                }
                catch
                {
                    this.TryClose();
                    return;
                }
                string @string = Encoding.Default.GetString(this.buffer, 0, count);
                if (@string.Length > 0)
                {
                    this.ProcessCommand(@string);
                }
            }
            catch (Exception value)
            {
                Console.WriteLine(value);
            }
            this.TryClose();
        }

        internal void ProcessCommand(string data)
        {
            string text = data.Split(new char[]
            {
                Convert.ToChar(1)
            })[0];
            string text2 = data.Split(new char[]
            {
                Convert.ToChar(1)
            })[1];
            string[] array = text2.Split(new char[]
            {
                Convert.ToChar(5)
            });
            string a;
            if ((a = text.ToLower()) != null)
            {
                if (!(a == "updatemotto"))
                {
                    GameClient clientByUserID;
                    if (!(a == "updaterooms"))
                    {
                        if (!(a == "addtoinventory"))
                        {
                            if (!(a == "updatecredits"))
                            {
                                if (!(a == "updatesubscription"))
                                {
                                    goto IL_38B;
                                }
                                uint userID = Convert.ToUInt32(array[0]);
                                clientByUserID = global::Azure.Azure.GetGame().GetClientManager().GetClientByUserId(userID);
                                if (clientByUserID != null && clientByUserID.GetHabbo() != null)
                                {
                                    clientByUserID.GetHabbo().GetSubscriptionManager().ReloadSubscription();
                                    clientByUserID.GetHabbo().SerializeClub();
                                    clientByUserID.SendMessage(new ServerMessage(LibraryParser.OutgoingRequest("PublishShopMessageComposer")));
                                    goto IL_3A3;
                                }
                                goto IL_3A3;
                            }
                            else
                            {
                                uint userID2 = Convert.ToUInt32(array[0]);
                                int credits = Convert.ToInt32(array[1]);
                                clientByUserID = global::Azure.Azure.GetGame().GetClientManager().GetClientByUserId(userID2);
                                if (clientByUserID != null && clientByUserID.GetHabbo() != null)
                                {
                                    clientByUserID.GetHabbo().Credits = credits;
                                    clientByUserID.GetHabbo().UpdateCreditsBalance();
                                    goto IL_3A3;
                                }
                                goto IL_3A3;
                            }
                        }
                    }
                    else
                    {
                        uint num = Convert.ToUInt32(array[0]);
                        using (Dictionary<uint, Room>.ValueCollection.Enumerator enumerator = global::Azure.Azure.GetGame().GetRoomManager().LoadedRooms.Values.GetEnumerator())
                        {
                            while (enumerator.MoveNext())
                            {
                                Room current = enumerator.Current;
                                if ((long)current.OwnerId == (long)((ulong)num))
                                {
                                    global::Azure.Azure.GetGame().GetRoomManager().UnloadRoom(current, "mus request unload");
                                }
                            }
                            goto IL_3A3;
                        }
                    }
                    uint userID3 = Convert.ToUInt32(array[0]);
                    int id = Convert.ToInt32(array[1]);
                    clientByUserID = global::Azure.Azure.GetGame().GetClientManager().GetClientByUserId(userID3);
                    if (clientByUserID != null && clientByUserID.GetHabbo() != null && clientByUserID.GetHabbo().GetInventoryComponent() != null)
                    {
                        clientByUserID.GetHabbo().GetInventoryComponent().UpdateItems(true);
                        clientByUserID.GetHabbo().GetInventoryComponent().SendNewItems((uint)id);
                    }
                }
                else
                {
                    GameClient clientByUserID = global::Azure.Azure.GetGame().GetClientManager().GetClientByUserId(Convert.ToUInt32(array[0]));
                    clientByUserID.GetHabbo().Motto = MusConnection.MergeParams(array, 1);
                    var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                    serverMessage.AppendInteger(-1);
                    serverMessage.AppendString(clientByUserID.GetHabbo().Look);
                    serverMessage.AppendString(clientByUserID.GetHabbo().Gender.ToLower());
                    serverMessage.AppendString(clientByUserID.GetHabbo().Motto);
                    serverMessage.AppendInteger(clientByUserID.GetHabbo().AchievementPoints);
                    clientByUserID.SendMessage(serverMessage);
                    if (clientByUserID.GetHabbo().CurrentRoom != null)
                    {
                        RoomUser roomUserByHabbo = clientByUserID.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(clientByUserID.GetHabbo().UserName);
                        var serverMessage2 = new ServerMessage(LibraryParser.OutgoingRequest("UpdateUserDataMessageComposer"));
                        serverMessage2.AppendInteger(roomUserByHabbo.VirtualId);//BUGG
                        //serverMessage2.AppendInt32(-1);
                        serverMessage2.AppendString(clientByUserID.GetHabbo().Look);
                        serverMessage2.AppendString(clientByUserID.GetHabbo().Gender.ToLower());
                        serverMessage2.AppendString(clientByUserID.GetHabbo().Motto);
                        serverMessage2.AppendInteger(clientByUserID.GetHabbo().AchievementPoints);
                        clientByUserID.GetHabbo().CurrentRoom.SendMessage(serverMessage2);
                    }
                }
                IL_3A3:
                Logging.WriteLine(string.Format("[MUS SOCKET] Comando MUS procesado correctamente: '{0}'", text), global::Azure.ConsoleColor.Green);
                return;
            }
            IL_38B:
            Logging.WriteLine(string.Format("[MUS SOCKET] Paquete MUS no reconocido: {0}//{1}", text, data), global::Azure.ConsoleColor.Red);
        }
    }
}