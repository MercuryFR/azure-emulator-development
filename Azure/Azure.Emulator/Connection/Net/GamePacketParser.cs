using System;
using Azure.Configuration;
using Azure.Connection.Connection;
using Azure.HabboHotel.GameClients;
using Azure.Messages;
using Azure.Messages.Parsers;

namespace Azure.Connection.Net
{
    public class GamePacketParser : IDataParser, IDisposable, ICloneable
    {
        private readonly GameClient currentClient;
        private ConnectionInformation con;

        internal GamePacketParser(GameClient me)
        {
            this.currentClient = me;
        }

        public delegate void HandlePacket(ClientMessage message);

        public event GamePacketParser.HandlePacket OnNewPacket;

        public void SetConnection(ConnectionInformation con)
        {
            this.con = con;
            this.OnNewPacket = null;
        }

        public void HandlePacketData(byte[] data)
        {
            int i = 0;
            if (this.currentClient != null && this.currentClient.ARC4 != null)
            {
                this.currentClient.ARC4.Decrypt(ref data);
            }
            checked
            {
                while (i < data.Length)
                {
                    try
                    {
                        int num = HabboEncoding.DecodeInt32(new byte[]
                        {
                            data[i++],
                            data[i++],
                            data[i++],
                            data[i++]
                        });
                        if (num >= 2 && num <= 1024)
                        {
                            int MessageId = HabboEncoding.DecodeInt16(new byte[]
                            {
                                data[i++],
                                data[i++]
                            });
                            byte[] array = new byte[num - 2];
                            int num2 = 0;
                            while (num2 < array.Length && i < data.Length)
                            {
                                array[num2] = data[i++];
                                num2++;
                            }
                            if (this.OnNewPacket != null)
                            {
                                using (ClientMessage clientMessage = Factorys.GetClientMessage(MessageId, array))
                                {
                                    this.OnNewPacket(clientMessage);
                                }
                            }
                        }
                    }
                    catch (Exception pException)
                    {
                        HabboEncoding.DecodeInt32(new byte[]
                        {
                            data[i++],
                            data[i++],
                            data[i++],
                            data[i++]
                        });
                        int num3 = HabboEncoding.DecodeInt16(new byte[] { data[i++], data[i++] });
                        Logging.HandleException(pException, string.Format("packet handling ----> {0}", num3));
                        this.con.Dispose();
                    }
                }
            }
        }

        public void Dispose()
        {
            this.OnNewPacket = null;
        }

        public object Clone()
        {
            return new GamePacketParser(this.currentClient);
        }
    }
}