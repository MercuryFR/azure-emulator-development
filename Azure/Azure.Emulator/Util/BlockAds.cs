using System.Data;
using System.Linq;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.Util
{
    static class BlockAds
    {
        internal static string[] BannedHotels;

        internal static void Load(IQueryAdapter dBClient)
        {
            dBClient.SetQuery("SELECT * FROM server_banned_hotels");
            var table = dBClient.GetTable();
            BannedHotels = new string[table.Rows.Count];

            var i = 0;
            foreach (DataRow dataRow in dBClient.GetTable().Rows)
            {
                BannedHotels[i] = (dataRow[0].ToString());
                i++;
            }
        }

        internal static bool CheckPublicistas(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
                return false;
            message =
                message.ToLower()
                    .Replace(" ", "")
                    .Replace("\u00a0", "")
                    .Replace("/", "")
                    .Replace("-", "")
                    .Replace("_", "")
                    .Replace("*", "")
                    .Replace("0", "o")
                    .Replace("4", "a")
                    .Replace("1", "i");
            return BannedHotels.Any(message.Contains) && !message.Contains("ban") && !message.Contains("mip");
        }
    }
}