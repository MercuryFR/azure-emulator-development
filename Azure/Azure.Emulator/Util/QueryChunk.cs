using System.Collections.Generic;
using System.Text;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

namespace Azure.Util
{
    class QueryChunk
    {
        private readonly EndingType _endingType;
        private Dictionary<string, object> _parameters;
        private StringBuilder _queries;
        private int _queryCount;

        public QueryChunk()
        {
            _parameters = new Dictionary<string, object>();
            _queries = new StringBuilder();
            _queryCount = 0;
            _endingType = EndingType.Sequential;
        }

        public QueryChunk(string startQuery)
        {
            _parameters = new Dictionary<string, object>();
            _queries = new StringBuilder(startQuery);
            _endingType = EndingType.Continuous;
            _queryCount = 0;
        }

        internal void AddQuery(string query)
        {
            checked
            {
                _queryCount++;
                _queries.Append(query);

                switch (_endingType)
                {
                    case EndingType.Sequential:
                        _queries.Append(";");
                        return;
                    case EndingType.Continuous:
                        _queries.Append(",");
                        return;
                    default:
                        return;
                }
            }
        }

        internal void AddParameter(string parameterName, object value) { _parameters.Add(parameterName, value); }

        internal void Execute(IQueryAdapter dbClient)
        {
            if (_queryCount == 0)
                return;
            _queries = _queries.Remove(checked(_queries.Length - 1), 1);
            dbClient.SetQuery(_queries.ToString());
            foreach (var current in _parameters)
                dbClient.AddParameter(current.Key, current.Value);
            dbClient.RunQuery();
        }

        internal void Dispose()
        {
            _parameters.Clear();
            _queries.Clear();
            _parameters = null;
            _queries = null;
        }
    }
}