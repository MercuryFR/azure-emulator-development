namespace Azure.Util
{
    enum EndingType
    {
        None,
        Sequential,
        Continuous
    }
}