using System;

namespace Azure.Util
{
    static class RandomNumber
    {
        private static readonly Random GlobalRandom = new Random();
        [ThreadStatic] private static Random _localRandom;

        public static int Get(int min, int max)
        {
            var random = _localRandom;
            if (random != null)
                return random.Next(min, max);
            int seed;
            lock (GlobalRandom)
                seed = GlobalRandom.Next();
            random = (_localRandom = new Random(seed));
            return random.Next(min, max);
        }
    }
}