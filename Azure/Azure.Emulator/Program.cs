using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Azure.Configuration;

namespace Azure
{
    class Program
    {
        public const int ScClose = 61536;

        /// <summary>
        /// Main Void of Azure.Emulator
        /// </summary>
        /// <param name="args"></param>
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        public static void Main(string[] args)
        {
            StartConsoleWindow();
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), 61536, 0);
            InitEnvironment();
            while (Azure.IsLive)
            {
                Console.CursorVisible = true;
                if (!Logging.DisabledState)
                    Console.Write("root@azure> ");
                ConsoleCommandHandling.InvokeCommand(Console.ReadLine());
            }
        }

        /// <summary>
        /// Initialize the Azure Environment 
        /// </summary>
        public static void InitEnvironment()
        {
            if (Azure.IsLive)
                return;
            WinConsole.Color = ConsoleColor.White;
            Console.CursorVisible = false;
            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += MyHandler;
            Azure.Initialize();
        }

        [DllImport("user32.dll")]
        public static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

        /// <summary>
        /// Start's the Console Window
        /// </summary>
        private static void StartConsoleWindow()
        {
            Console.Clear();
            Console.SetWindowSize(120, 40);
            Console.SetCursorPosition(0, 0);
            Console.BackgroundColor = System.ConsoleColor.Black;
            Console.ForegroundColor = System.ConsoleColor.DarkCyan;
            Console.WriteLine();
            Console.WriteLine(@"     " + @"                               ______                 _       _              ");
            Console.WriteLine(@"     " + @"     /\                       |  ____|               | |     | |             ");
            Console.WriteLine(@"     " + @"    /  \    _____   _ _ __ ___| |__   _ __ ___  _   _| | __ _| |_ ___  _ __  ");
            Console.WriteLine(@"     " + @"   / /\ \  |_  / | | | '__/ _ \  __| | '_ ` _ \| | | | |/ _` | __/ _ \| '__| ");
            Console.WriteLine(@"     " + @"  / ____ \  / /| |_| | | |  __/ |____| | | | | | |_| | | (_| | || (_) | |    ");
            Console.WriteLine(@"     " + @" / _/    \_\/___|\__,_|_|  \___|______|_| |_| |_|\__,_|_|\__,_|\__\___/|_|   ");
            Console.WriteLine();
            Console.WriteLine(@"     " + @"  Developed by Xdr, IhToN, Antoine, bi0s, Jamal, Jonas, Maartenvn, VabboH");
            Console.ForegroundColor = System.ConsoleColor.DarkYellow;
            Console.WriteLine();
            Console.WriteLine(@"     " + @"  Contributors: KyleeIsProzZ");
            Console.WriteLine();
            Console.ForegroundColor = System.ConsoleColor.DarkCyan;
            Console.ForegroundColor = System.ConsoleColor.DarkYellow;
            Console.WriteLine(@"------------------------------------------------------------------------------------------------------------------------");
        }

        private static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Logging.DisablePrimaryWriting(true);
            var ex = (Exception) args.ExceptionObject;
            Logging.LogCriticalException(string.Format("SYSTEM CRITICAL EXCEPTION: {0}", ex));
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();
    }
}