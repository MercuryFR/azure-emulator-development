﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using Azure.Configuration;
using Azure.Connection.Net;
using Azure.Encryption;
using Azure.Database;
using Azure.HabboHotel;
using Azure.HabboHotel.Catalogs;
using Azure.HabboHotel.Groups;
using Azure.HabboHotel.Pets;
using Azure.HabboHotel.Users;
using Azure.HabboHotel.Users.Messenger;
using Azure.HabboHotel.Users.UserDataManagement;
using Azure.Messages;
using Azure.Messages.Parsers;
using Azure.Util;
using MySql.Data.MySqlClient;
using Timer = System.Timers.Timer;

namespace Azure
{
    public static class Azure
    {
        /// <summary>
        /// Azure Environment: Main Thread of Azure Emulator, SetUp's the Emulator 
        /// Contains Initialize: Responsible of the Emulator Loadings
        /// </summary>
        public static DatabaseManager Manager;

        public static uint FriendRequestLimit = 1000;
        public static string DatabaseConnectionType = "MySQL";
        internal static readonly string PrettyBuild = "1.0";
        internal static readonly string PrettyVersion = "Azure Emulator";
        internal static bool IsLive;
        internal static bool SeparatedTasksInGameClientManager = false;
        internal static bool SeparatedTasksInMainLoops = false;
        internal static bool Debugmode = false;
        internal static ConfigData ConfigData;
        internal static DateTime ServerStarted;
        internal static Dictionary<uint, List<OfflineMessage>> OfflineMessages;
        internal static GiftWrappers GiftWrappers;
        internal static int LiveCurrencyType = 105;
        internal static MusSocket MusSystem;
        internal static bool StartOutgoing = false;
        internal static int ConsoleTimer = 2000;
        internal static bool ConsoleTimerOn = false;
        internal static Timer Timer;
        internal static CultureInfo CultureInfo;
        internal static uint StaffAlertMinRank = 4;
        internal static string ServerLanguage = "english";

        private static readonly HashSet<char> AllowedChars = new HashSet<char>(new[]
        {
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '0',
            '-',
            '.',
            'á',
            'é',
            'í',
            'ó',
            'ú',
            'ñ',
            'Ñ',
            'ü',
            'Ü',
            'Á',
            'É',
            'Í',
            'Ó',
            'Ú',
            ' ', 'Ã', '©', '¡', '­', 'º', '³', 'Ã', '‰', '_'
        });

        private static readonly HybridDictionary UsersCached = new HybridDictionary();
        private static readonly HybridDictionary UsersCached1 = new HybridDictionary();
        private static ConfigurationData _configuration;
        private static ConnectionHandling _connectionManager;
        private static Encoding _defaultEncoding;
        private static Game _game;
        private static Languages _languages;

        /// <summary>
        /// Check's if the Shutdown Has Started
        /// </summary>
        internal static bool ShutdownStarted { get; private set; }

        /// <summary>
        /// Get's Habbo By The User Id 
        /// </summary>
        /// <param name="userId"></param> Table: users.id
        /// <returns></returns>
        internal static Habbo GetHabboForId(uint userId)
        {
            try
            {
                var clientByUserId = GetGame().GetClientManager().GetClientByUserId(userId);
                if (clientByUserId != null)
                {
                    var habbo = clientByUserId.GetHabbo();
                    if (habbo != null && habbo.Id > 0u)
                    {
                        if (UsersCached1.Contains(userId))
                            UsersCached1.Remove(userId);
                        return habbo;
                    }
                }
                else
                {
                    if (UsersCached1.Contains(userId))
                        return (Habbo) UsersCached1[userId];

                    var userData = UserDataFactory.GetUserData(checked((int) userId));
                    var user = userData.User;
                    if (user != null)
                    {
                        user.InitInformation(userData);
                        UsersCached1.Add(userId, user);
                        return user;
                    }
                }
            }
            catch (Exception e)
            {
                Writer.Writer.LogException("Habbo GetHabboForId: " + e);
            }
            return null;
        }

        /// <summary>
        /// Console Clear Thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal static void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            Console.Clear();
            WinConsole.Color = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("≫ Console Cleared in: {0} Next Time on: {1} Seconds ", DateTime.Now, ConsoleTimer);
            Console.WriteLine();
            WinConsole.Color = ConsoleColor.Cyan;
            GC.Collect();
            Timer.Start();
        }

        /// <summary>
        /// Main Void, Initializes the Emulator.
        /// </summary>
        internal static void Initialize()
        {
            #region Precheck

            ServerStarted = DateTime.Now;
            Console.Title = "Azure Emulator | Loading [...]";
            _defaultEncoding = Encoding.Default;

            #endregion

            #region Database Connection

            CultureInfo = CultureInfo.CreateSpecificCulture("en-GB");
            try
            {
                _configuration = new ConfigurationData(Path.Combine(Application.StartupPath, "config.ini"), false);
                DatabaseConnectionType = GetConfig().Data["db.type"];
                var mySqlConnectionStringBuilder = new MySqlConnectionStringBuilder
                {
                    Server = (GetConfig().Data["db.hostname"]),
                    Port = (uint.Parse(GetConfig().Data["db.port"])),
                    UserID = (GetConfig().Data["db.username"]),
                    Password = (GetConfig().Data["db.password"]),
                    Database = (GetConfig().Data["db.name"]),
                    MinimumPoolSize = (uint.Parse(GetConfig().Data["db.pool.minsize"])),
                    MaximumPoolSize = (uint.Parse(GetConfig().Data["db.pool.maxsize"])),
                    Pooling = (true),
                    AllowZeroDateTime = (true),
                    ConvertZeroDateTime = (true),
                    DefaultCommandTimeout = (300u),
                    ConnectionTimeout = (10u)
                };
                var mySqlConnectionStringBuilder2 = mySqlConnectionStringBuilder;
                Manager = new DatabaseManager(mySqlConnectionStringBuilder2.ToString(), DatabaseConnectionType);
                using (var queryReactor = GetDatabaseManager().GetQueryReactor())
                {
                    ConfigData = new ConfigData(queryReactor);
                    PetCommandHandler.Init(queryReactor);
                    PetLocale.Init(queryReactor);
                    OfflineMessages = new Dictionary<uint, List<OfflineMessage>>();
                    OfflineMessage.InitOfflineMessages(queryReactor);
                    GiftWrappers = new GiftWrappers(queryReactor);
                }

                #endregion

                #region Packets Registering

                ConsoleTimer = (int.Parse(GetConfig().Data["console.clear.time"]));
                ConsoleTimerOn = (bool.Parse(GetConfig().Data["console.clear.enabled"]));
                FriendRequestLimit = checked((uint) int.Parse(GetConfig().Data["client.maxrequests"]));
                LibraryParser.Incoming = new Dictionary<int, LibraryParser.StaticRequestHandler>();
                LibraryParser.Library = new Dictionary<string, string>();
                LibraryParser.Outgoing = new Dictionary<string, int>();
                LibraryParser.Config = new Dictionary<string, string>();
                LibraryParser.RegisterLibrary();
                LibraryParser.RegisterOutgoing();
                LibraryParser.RegisterIncoming();
                LibraryParser.RegisterConfig();

                #endregion

                #region Game Initalizer

                ExtraSettings.RunExtraSettings();
                FurniDataParser.SetCache();
                _game = new Game(int.Parse(GetConfig().Data["game.tcp.conlimit"]));
                _game.ContinueLoading();
                FurniDataParser.Clear();

                #endregion

                #region Languages Parser

                ServerLanguage = (Convert.ToString(GetConfig().Data["system.lang"]));
                _languages = new Languages(ServerLanguage);
                Logging.WriteLine("Loaded " + _languages.Count() + " Languages Vars", ConsoleColor.Cyan);

                #endregion

                #region Environment SetUp

                //StartOutgoing = true;
                if (ConsoleTimerOn)
                    Logging.WriteLine(string.Format("Console Clear Timer is Enabled, with {0} Seconds.", ConsoleTimer),
                        ConsoleColor.Cyan);
                _connectionManager = new ConnectionHandling(int.Parse(GetConfig().Data["game.tcp.port"]),
                    int.Parse(GetConfig().Data["game.tcp.conlimit"]), int.Parse(GetConfig().Data["game.tcp.conperip"]),
                    GetConfig().Data["game.tcp.enablenagles"].ToLower() == "true");

                if (LibraryParser.Config["Crypto.Enabled"] == "true")
                {
                    Handler.Initialize(LibraryParser.Config["Crypto.RSA.N"], LibraryParser.Config["Crypto.RSA.D"],
                        LibraryParser.Config["Crypto.RSA.E"]);
                }

                _connectionManager.Init();
                _connectionManager.Start();
                LibraryParser.Initialize();
                Console.WriteLine();
                WinConsole.Color = ConsoleColor.Yellow;
                Console.WriteLine("≫ Game Environment Suceffuly Loaded.");
                _game.VideoGo();
                WinConsole.Color = ConsoleColor.Cyan;
                Console.WriteLine();

                #endregion

                #region Tasks and MusSystem

                if (ConsoleTimerOn)
                {
                    Timer = new Timer {Interval = ConsoleTimer};
                    Timer.Elapsed += TimerElapsed;
                    Timer.Start();
                }
               // var allowedIps = GetConfig().Data["mus.tcp.allowedaddr"].Split(';');
                /* MusSystem Disabled for More Stability and Speed, Mus System will Be Rewrited
                MusSystem = new MusSocket(GetConfig().Data["mus.tcp.bindip"],
                    int.Parse(GetConfig().Data["mus.tcp.port"]), allowedIps, 0);
                    */
                if (_configuration.Data.ContainsKey("StaffAlert.MinRank"))
                    StaffAlertMinRank = uint.Parse(GetConfig().Data["StaffAlert.MinRank"]);
                if (_configuration.Data.ContainsKey("SeparatedTasksInMainLoops.enabled") &&
                    _configuration.Data["SeparatedTasksInMainLoops.enabled"] == "true")
                    SeparatedTasksInMainLoops = true;
                if (_configuration.Data.ContainsKey("SeparatedTasksInGameClientManager.enabled") &&
                    _configuration.Data["SeparatedTasksInGameClientManager.enabled"] == "true")
                    SeparatedTasksInGameClientManager = true;
                if (GetConfig().Data.ContainsKey("Debug"))
                    if (GetConfig().Data["Debug"] == "true")
                    {
                        Debugmode = true;
                        StartOutgoing = true;
                    }
                IsLive = true;
            }
            catch (KeyNotFoundException)
            {
                Logging.WriteLine("Something is missing in your configuration", ConsoleColor.Red);
                Logging.WriteLine("Please type a key to shut down ...", ConsoleColor.Violet);
                Console.ReadKey(true);
                Destroy();
            }
            catch (InvalidOperationException ex)
            {
                Logging.WriteLine(string.Format("Something wrong happened: {0}", ex.Message), ConsoleColor.Red);
                Logging.WriteLine("Please type a key to shut down...", ConsoleColor.Violet);
                Console.ReadKey(true);
                Destroy();
            }
            catch (Exception ex2)
            {
                Logging.WriteLine(string.Format("An exception got caught: {0}", ex2.Message), ConsoleColor.Red);
                Logging.WriteLine("Type a key to know more about the error", ConsoleColor.Violet);
                Console.ReadKey();
                Logging.WriteLine(ex2.ToString(), ConsoleColor.Yellow);
                Logging.WriteLine("Please type a ket to shut down...", ConsoleColor.Violet);
                Console.ReadKey();
                Environment.Exit(1);
            }

            #endregion
        }

        /// <summary>
        /// Convert's Enum to Boolean
        /// </summary>
        /// <param name="enum"></param>
        /// <returns></returns>
        internal static bool EnumToBool(string @enum) { return @enum == "1"; }

        /// <summary>
        /// Convert's Boolean to Integer
        /// </summary>
        /// <param name="bool"></param>
        /// <returns></returns>
        internal static int BoolToInteger(bool @bool) { return @bool ? 1 : 0; }

        /// <summary>
        /// Convert's Boolean to Enum
        /// </summary>
        /// <param name="bool"></param>
        /// <returns></returns>
        internal static string BoolToEnum(bool @bool) { return @bool ? "1" : "0"; }

        /// <summary>
        /// Generates a Random Number in the Interval Min,Max
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        internal static int GetRandomNumber(int min, int max) { return RandomNumber.Get(min, max); }

        /// <summary>
        /// Get's the Actual Timestamp in Unix Format
        /// </summary>
        /// <returns></returns>
        internal static int GetUnixTimestamp()
        {
            var totalSeconds = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
            return checked((int) totalSeconds);
        }

        /// <summary>
        /// Convert's a Unix TimeStamp to DateTime
        /// </summary>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        internal static DateTime UnixToDateTime(double unixTimeStamp)
        {
            var result = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            result = result.AddSeconds(unixTimeStamp).ToLocalTime();
            return result;
        }

        /// <summary>
        /// Convert's a DateTime to Unix TimeStamp
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        internal static int DateTimeToUnix(DateTime target)
        {
            var d = new DateTime(1970, 1, 1, 0, 0, 0, target.Kind);
            return Convert.ToInt32((target - d).TotalSeconds);
        }

        /// <summary>
        /// Get the Actual Time
        /// </summary>
        /// <returns></returns>
        internal static long Now()
        {
            var totalMilliseconds = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds;
            return checked((long) totalMilliseconds);
        }

        /// <summary>
        /// Filter's the Habbo Avatars Figure
        /// </summary>
        /// <param name="figure"></param>
        /// <returns></returns>
        internal static string FilterFigure(string figure)
        {
            return figure.Any(character => !IsValid(character))
                ? "lg-3023-1335.hr-828-45.sh-295-1332.hd-180-4.ea-3168-89.ca-1813-62.ch-235-1332"
                : figure;
        }

        /// <summary>
        /// Check if is a Valid AlphaNumeric String
        /// </summary>
        /// <param name="inputStr"></param>
        /// <returns></returns>
        internal static bool IsValidAlphaNumeric(string inputStr)
        {
            inputStr = inputStr.ToLower();
            if (string.IsNullOrEmpty(inputStr))
                return false;
            checked
            {
                return inputStr.All(IsValid);
            }
        }

        /// <summary>
        ///  Get a Habbo With a User Id.
        /// </summary>
        /// <param name="userId"></param> Table: users.id
        /// <returns></returns>
        internal static Habbo GetHabboForId1(uint userId)
        {
            try
            {
                var clientByUserId = GetGame().GetClientManager().GetClientByUserId(userId);
                Habbo result;
                if (clientByUserId != null)
                {
                    var habbo = clientByUserId.GetHabbo();
                    if (habbo != null && habbo.Id > 0u)
                    {
                        if (UsersCached.Contains(userId))
                            UsersCached.Remove(userId);
                        result = habbo;
                        return result;
                    }
                }
                else
                {
                    if (UsersCached.Contains(userId))
                    {
                        result = (Habbo) UsersCached[userId];
                        return result;
                    }
                    var userData = UserDataFactory.GetUserData(checked((int) userId));
                    var user = userData.User;
                    if (user != null)
                    {
                        user.InitInformation(userData);
                        UsersCached.Add(userId, user);
                        result = user;
                        return result;
                    }
                }
            }
            catch (Exception e)
            {
                Writer.Writer.LogException("Habbo GetHabboForId1" + e);
            }
            return null;
        }

        /// <summary>
        /// Get a Habbo With the Habbo's Username
        /// </summary>
        /// <param name="userName"></param> Table: users.username
        /// <returns></returns>
        internal static Habbo GetHabboForName(string userName)
        {
            try
            {
                using (var queryReactor = GetDatabaseManager().GetQueryReactor())
                {
                    queryReactor.SetQuery("SELECT id FROM users WHERE username = @user");
                    queryReactor.AddParameter("user", userName);
                    var integer = queryReactor.GetInteger();
                    if (integer > 0)
                    {
                        var result = GetHabboForId1(checked((uint) integer));
                        return result;
                    }
                }
            }
            catch
            {
            }
            return null;
        }

        /// <summary>
        /// Check if the Input String is a Integer
        /// </summary>
        /// <param name="int"></param>
        /// <returns></returns>
        internal static bool IsNum(string @int)
        {
            double num;
            return double.TryParse(@int, out num);
        }

        /// <summary>
        /// Get the Configuration Data Handler
        /// </summary>
        /// <returns></returns>
        internal static ConfigurationData GetConfig() { return _configuration; }

        /// <summary>
        /// Get the Database Configuration Data
        /// </summary>
        /// <returns></returns>
        internal static ConfigData GetDbConfig() { return ConfigData; }

        /// <summary>
        /// Get's the Default Emulator Encoding
        /// </summary>
        /// <returns></returns>
        internal static Encoding GetDefaultEncoding() { return _defaultEncoding; }

        /// <summary>
        /// Get's the Game Connection Manager Handler
        /// </summary>
        /// <returns></returns>
        internal static ConnectionHandling GetConnectionManager() { return _connectionManager; }

        /// <summary>
        /// Get's the Game Environment Handler
        /// </summary>
        /// <returns></returns>
        internal static Game GetGame() { return _game; }

        internal static Languages GetLanguage() { return _languages; }

        /// <summary>
        /// Destroy's the Habbo Hotel Environment
        /// </summary>
        internal static void Destroy()
        {
            IsLive = false;
            Logging.WriteLine("Destroying AzureEnvironment...", ConsoleColor.Violet);
            if (GetGame() != null)
            {
                GetGame().Destroy();
                GetGame().GetPixelManager().Destroy();
                _game = null;
            }
            if (GetConnectionManager() != null)
            {
                Logging.WriteLine("Destroying ConnectionManager...", ConsoleColor.Violet);
                GetConnectionManager().Destroy();
            }
            if (Manager != null)
                try
                {
                    Logging.WriteLine("Destroying DatabaseManager...", ConsoleColor.Violet);
                    Manager.Destroy();
                }
                catch
                {
                }
            Logging.WriteLine("Closing...", ConsoleColor.Violet);
            Thread.Sleep(500);
            Environment.Exit(1);
        }

        /// <summary>
        /// Send a Message to Everyone in the Habbo Client
        /// </summary>
        /// <param name="message"></param>
        internal static void SendMassMessage(string message)
        {
            try
            {
                var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("BroadcastNotifMessageComposer"));
                serverMessage.AppendString(message);
                GetGame().GetClientManager().QueueBroadcaseMessage(serverMessage);
            }
            catch (Exception pException)
            {
                Logging.HandleException(pException, "AzureEnvironment.SendMassMessage");
            }
        }

        /// <summary>
        /// Filter's SQL Injection Characters
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        internal static string FilterInjectionChars(string input)
        {
            input = input.Replace('\u0001', ' ');
            input = input.Replace('\u0002', ' ');
            input = input.Replace('\u0003', ' ');
            input = input.Replace('\t', ' ');
            return input;
        }

        /// <summary>
        /// Get's the Database Manager Handler
        /// </summary>
        /// <returns></returns>
        internal static DatabaseManager GetDatabaseManager() { return Manager; }

        /// <summary>
        /// Perform's the Emulator Shutdown
        /// </summary>
        internal static void PerformShutDown() { PerformShutDown(false); }
        internal static void PerformRestart() { PerformShutDown(true); }

        /// <summary>
        /// Shutdown the Emulator
        /// </summary>
        /// <param name="restart"></param> Set a Different Message in Hotel
        internal static void PerformShutDown(bool restart)
        {
            var now = DateTime.Now;
            ShutdownStarted = true;
            var serverMessage = new ServerMessage(LibraryParser.OutgoingRequest("SuperNotificationMessageComposer"));
            serverMessage.AppendString("disconnection");
            serverMessage.AppendInteger(2);
            serverMessage.AppendString("title");
            serverMessage.AppendString("HEY EVERYONE!");
            serverMessage.AppendString("message");
            serverMessage.AppendString(
                restart
                    ? "<b>The hotel is shutting down for a break.<)/b>\nYou may come back later.\r\n<b>So long!</b>"
                    : "<b>The hotel is shutting down for a break.</b><br />You may come back soon. Don't worry, everything's going to be saved..<br /><b>So long!</b>\r\n~ This session was powered by AzureEmulator");
            GetGame().GetClientManager().QueueBroadcaseMessage(serverMessage);
            Console.Title = "Azure Emulator | Shutting down...";
            Console.WriteLine("≫ Starting Shutdown");
            _game.StopGameLoop();
            GetGame().GetClientManager().CloseAll();
            _game.GetRoomManager().RemoveAllRooms();
            foreach (Guild group in _game.GetGroupManager().Groups.Values)
                group.UpdateForum();
            GetConnectionManager().Destroy();
            using (var queryReactor = Manager.GetQueryReactor())
            {
                queryReactor.RunFastQuery("UPDATE users SET online = '0'");
                queryReactor.RunFastQuery("UPDATE rooms_data SET users_now = 0");
                queryReactor.RunFastQuery("TRUNCATE TABLE users_rooms_visits");
            }
            _connectionManager.Destroy();
            _game.Destroy();
            try
            {
                Console.WriteLine("≫ Destroying Game Manager");
                Manager.Destroy();
            }
            catch
            {
            }
            var span = DateTime.Now - now;
            Console.WriteLine("≫ Azure Elapsed {0}ms on Shutdown Proccess", TimeSpanToString(span));
            Console.WriteLine("≫ Shutdown Completed. Press Any Key to Continue...");
            Console.ReadKey();
            IsLive = false;
            if (restart)
                Process.Start(string.Format("{0}\\Azure.exe", AppDomain.CurrentDomain.BaseDirectory));
            Console.WriteLine("Closing...");
            Environment.Exit(0);
        }

        /// <summary>
        /// Convert's a Unix TimeSpan to A String
        /// </summary>
        /// <param name="span"></param>
        /// <returns></returns>
        internal static string TimeSpanToString(TimeSpan span)
        {
            return string.Concat(new object[]
            {
                span.Seconds,
                " s, ",
                span.Milliseconds,
                " ms"
            });
        }

        /// <summary>
        /// Check's if Input Data is a Valid AlphaNumeric Character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        private static bool IsValid(char character) { return AllowedChars.Contains(character); }
    }
}