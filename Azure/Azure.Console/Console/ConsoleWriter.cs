﻿using System.IO;
using System.Text;

namespace Azure
{
    /// <summary>
    /// Summary description for ConsoleWriter.
    /// </summary>
    /// 
    public class ConsoleWriter : TextWriter
    {
        #region Variables

        private readonly TextWriter _writer;

        #endregion

        #region Construction

        public ConsoleWriter(TextWriter writer, ConsoleColor color, ConsoleFlashMode mode, bool beep)
        {
            Color = color;
            FlashMode = mode;
            _writer = writer;
            BeepOnWrite = beep;
        }

        #endregion

        #region Properties

        public ConsoleColor Color { get; set; }

        public ConsoleFlashMode FlashMode { get; set; }

        public bool BeepOnWrite { get; set; }

        #endregion

        #region Write Routines

        public override Encoding Encoding
        {
            get { return _writer.Encoding; }
        }

        protected void Flash()
        {
            switch (FlashMode)
            {
                case ConsoleFlashMode.FlashOnce:
                    WinConsole.Flash(true);
                    break;
                case ConsoleFlashMode.FlashUntilResponse:
                    WinConsole.Flash(false);
                    break;
            }

            if (BeepOnWrite)
                WinConsole.Beep();
        }

        public override void Write(char ch)
        {
            var oldColor = WinConsole.Color;
            try
            {
                WinConsole.Color = Color;
                _writer.Write(ch);
            }
            finally
            {
                WinConsole.Color = oldColor;
            }
            Flash();
        }

        public override void Write(string s)
        {
            var oldColor = WinConsole.Color;
            try
            {
                WinConsole.Color = Color;
                Flash();
                _writer.Write(s);
            }
            finally
            {
                WinConsole.Color = oldColor;
            }
            Flash();
        }

        public override void Write(char[] data, int start, int count)
        {
            var oldColor = WinConsole.Color;
            try
            {
                WinConsole.Color = Color;
                _writer.Write(data, start, count);
            }
            finally
            {
                WinConsole.Color = oldColor;
            }
            Flash();
        }

        #endregion
    }
}