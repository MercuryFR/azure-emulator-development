﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;

namespace Azure
{
    /// <summary>
    /// Extended console for both Windows and Console Applications.
    /// </summary>
    public class WinConsole
    {
        #region Construction

        private WinConsole() { Initialize(); }

        #endregion

        #region Windows API

        [DllImport("user32")]
        private static extern void FlashWindowEx(ref FlashWInfo info);

        [DllImport("user32")]
        private static extern void MessageBeep(int type);

        [DllImport("user32")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int newValue);

        [DllImport("user32")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("kernel32")]
        private static extern bool AllocConsole();

        [DllImport("kernel32")]
        private static extern bool GetConsoleScreenBufferInfo(IntPtr consoleOutput, out ConsoleScreenBufferInfo info);

        [DllImport("kernel32")]
        private static extern bool GetConsoleTitle(StringBuilder text, int size);

        [DllImport("kernel32")]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("kernel32")]
        private static extern IntPtr GetStdHandle(int handle);

        [DllImport("kernel32")]
        private static extern int SetConsoleCursorPosition(IntPtr buffer, Coord position);

        [DllImport("kernel32")]
        private static extern int FillConsoleOutputCharacter(IntPtr buffer, char character, int length, Coord position,
            out int written);

        [DllImport("kernel32")]
        private static extern bool SetConsoleTextAttribute(IntPtr hConsoleOutput, ConsoleColor wAttributes);

        [DllImport("kernel32")]
        private static extern bool SetConsoleTitle(string lpConsoleTitle);

        [DllImport("kernel32")]
        private static extern bool SetConsoleCtrlHandler(HandlerRoutine routine, bool add);

        [DllImport("user32")]
        private static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

        [DllImport("user32")]
        private static extern bool IsWindowVisible(IntPtr hwnd);

        [DllImport("kernel32")]
        private static extern IntPtr CreateConsoleScreenBuffer(int access, int share, IntPtr security, int flags,
            IntPtr reserved);

        [DllImport("kernel32")]
        private static extern bool SetConsoleActiveScreenBuffer(IntPtr handle);

        [DllImport("kernel32")]
        private static extern int GetConsoleCP();

        [DllImport("kernel32")]
        private static extern int GetConsoleOutputCP();

        [DllImport("kernel32")]
        private static extern bool SetStdHandle(int handle1, IntPtr handle2);

        [DllImport("user32")]
        private static extern IntPtr SetParent(IntPtr hwnd, IntPtr hwnd2);

        [DllImport("user32")]
        private static extern IntPtr GetParent(IntPtr hwnd);

        [DllImport("user32")]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter,
            int x, int y, int cx, int cy, int flags);

        [DllImport("user32")]
        private static extern bool GetClientRect(IntPtr hWnd, ref RECT rect);

        #endregion

        #region Constants

        private const int WS_POPUP = unchecked((int) 0x80000000);
        private const int WS_OVERLAPPED = 0x0;
        private const int WS_CHILD = 0x40000000;
        private const int WS_MINIMIZE = 0x20000000;
        private const int WS_VISIBLE = 0x10000000;
        private const int WS_DISABLED = 0x8000000;
        private const int WS_CLIPSIBLINGS = 0x4000000;
        private const int WS_CLIPCHILDREN = 0x2000000;
        private const int WS_MAXIMIZE = 0x1000000;
        private const int WS_CAPTION = 0xC00000; //  WS_BORDER | WS_DLGFRAME
        private const int WS_BORDER = 0x800000;
        private const int WS_DLGFRAME = 0x400000;
        private const int WS_VSCROLL = 0x200000;
        private const int WS_HSCROLL = 0x100000;
        private const int WS_SYSMENU = 0x80000;
        private const int WS_THICKFRAME = 0x40000;
        private const int WS_GROUP = 0x20000;
        private const int WS_TABSTOP = 0x10000;

        private const int WS_MINIMIZEBOX = 0x20000;
        private const int WS_MAXIMIZEBOX = 0x10000;

        private const int WS_TILED = WS_OVERLAPPED;
        private const int WS_ICONIC = WS_MINIMIZE;
        private const int WS_SIZEBOX = WS_THICKFRAME;

        private const int WS_OVERLAPPEDWINDOW =
            (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);

        private const int WS_TILEDWINDOW = WS_OVERLAPPEDWINDOW;

        private const int GWL_STYLE = (-16);

        private const int SW_HIDE = 0;
        private const int SW_SHOWNORMAL = 1;
        private const int SW_NORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;
        private const int SW_MAXIMIZE = 3;
        private const int SW_SHOWNOACTIVATE = 4;
        private const int SW_SHOW = 5;
        private const int SW_MINIMIZE = 6;
        private const int SW_SHOWMINNOACTIVE = 7;
        private const int SW_SHOWNA = 8;
        private const int SW_RESTORE = 9;
        private const int SW_SHOWDEFAULT = 10;
        private const int SW_MAX = 10;

        private const int EMPTY = 32;
        private const int CONSOLE_TEXTMODE_BUFFER = 1;

        private const int FLASHW_STOP = 0;
        private const int FLASHW_CAPTION = 1;
        private const int FLASHW_TRAY = 2;
        private const int FLASHW_ALL = 3;
        private const int FLASHW_TIMER = 4;
        private const int FLASHW_TIMERNOFG = 0xc;

        private const int _DefaultConsoleBufferSize = 256;

        private const int FOREGROUND_BLUE = 0x1; //  text color contains blue.
        private const int FOREGROUND_GREEN = 0x2; //  text color contains green.
        private const int FOREGROUND_RED = 0x4; //  text color contains red.
        private const int FOREGROUND_INTENSITY = 0x8; //  text color is intensified.
        private const int BACKGROUND_BLUE = 0x10; //  background color contains blue.
        private const int BACKGROUND_GREEN = 0x20; //  background color contains green.
        private const int BACKGROUND_RED = 0x40; //  background color contains red.
        private const int BACKGROUND_INTENSITY = 0x80; //  background color is intensified.
        private const int COMMON_LVB_REVERSE_VIDEO = 0x4000;
        private const int COMMON_LVB_UNDERSCORE = 0x8000;

        private const int GENERIC_READ = unchecked((int) 0x80000000);
        private const int GENERIC_WRITE = 0x40000000;

        private const int FILE_SHARE_READ = 0x1;
        private const int FILE_SHARE_WRITE = 0x2;

        private const int STD_INPUT_HANDLE = -10;
        private const int STD_OUTPUT_HANDLE = -11;
        private const int STD_ERROR_HANDLE = -12;

        private const int SWP_NOSIZE = 0x1;
        private const int SWP_NOMOVE = 0x2;
        private const int SWP_NOZORDER = 0x4;
        private const int SWP_NOREDRAW = 0x8;
        private const int SWP_NOACTIVATE = 0x10;

        #endregion

        #region Structures

        public delegate bool HandlerRoutine(int type);

        private struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        };

        private struct ConsoleScreenBufferInfo
        {
            public Coord Size;
            public Coord CursorPosition;
            public ConsoleColor Attributes;
            public SmallRect Window;
            public Coord MaximumWindowSize;
        }

        public struct ConsoleSelectionInfo
        {
            public int Flags;
            public Coord SelectionAnchor;
            public SmallRect Selection;
        }

        public struct SmallRect
        {
            public short Left;
            public short Top;
            public short Right;
            public short Bottom;
        }

        private struct FlashWInfo
        {
            public int Size;
            public IntPtr Hwnd;
            public int Flags;
            public int Count;
            public int Timeout;
        }

        #endregion

        #region Variables

        private static IntPtr _buffer;
        private static bool _initialized, _breakHit;
        public static event HandlerRoutine Break;

        #endregion

        #region Properties

        /// <summary>
        /// Specifies whether the console window should be visible or hidden
        /// </summary>
        public static bool Visible
        {
            get
            {
                var hwnd = GetConsoleWindow();
                return hwnd != IntPtr.Zero && IsWindowVisible(hwnd);
            }
            set
            {
                Initialize();
                var hwnd = GetConsoleWindow();
                if (hwnd != IntPtr.Zero)
                    ShowWindow(hwnd, value ? SW_SHOW : SW_HIDE);
            }
        }

        /// <summary>
        /// Initializes WinConsole -- should be called at the start of the program using it
        /// </summary>
        public static void Initialize()
        {
            if (_initialized)
                return;

            var hwnd = GetConsoleWindow();
            _initialized = true;

            SetConsoleCtrlHandler(HandleBreak, true);

            // Console app
            if (hwnd != IntPtr.Zero)
            {
                _buffer = GetStdHandle(STD_OUTPUT_HANDLE);
                return;
            }

            // Windows app
            var success = AllocConsole();
            if (!success)
                return;

            _buffer = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE,
                FILE_SHARE_READ | FILE_SHARE_WRITE, IntPtr.Zero, CONSOLE_TEXTMODE_BUFFER, IntPtr.Zero);

            SetConsoleActiveScreenBuffer(_buffer);

            SetStdHandle(STD_OUTPUT_HANDLE, _buffer);
            SetStdHandle(STD_ERROR_HANDLE, _buffer);

            Title = "Console";

            var s = Console.OpenStandardInput(_DefaultConsoleBufferSize);
            StreamReader reader;
            if (s == Stream.Null)
                reader = StreamReader.Null;
            else
                reader = new StreamReader(s, Encoding.GetEncoding(GetConsoleCP()),
                    false, _DefaultConsoleBufferSize);

            Console.SetIn(reader);

            // Set up Console.Out
            StreamWriter writer;
            s = Console.OpenStandardOutput(_DefaultConsoleBufferSize);
            if (s == Stream.Null)
                writer = StreamWriter.Null;
            else
            {
                writer = new StreamWriter(s, Encoding.GetEncoding(GetConsoleOutputCP()),
                    _DefaultConsoleBufferSize) {AutoFlush = true};
            }

            Console.SetOut(writer);

            s = Console.OpenStandardError(_DefaultConsoleBufferSize);
            if (s == Stream.Null)
                writer = StreamWriter.Null;
            else
            {
                writer = new StreamWriter(s, Encoding.GetEncoding(GetConsoleOutputCP()),
                    _DefaultConsoleBufferSize) {AutoFlush = true};
            }

            Console.SetError(writer);
        }

        /// <summary>
        /// Gets or sets the title of the console window
        /// </summary>
        public static string Title
        {
            get
            {
                var sb = new StringBuilder(256);
                GetConsoleTitle(sb, sb.Capacity);
                return sb.ToString();
            }
            set { SetConsoleTitle(value); }
        }

        /// <summary>
        /// Get the HWND of the console window
        /// </summary>
        /// <returns></returns>
        public static IntPtr Handle
        {
            get
            {
                Initialize();
                return GetConsoleWindow();
            }
        }

        /// <summary>
        /// Gets and sets a new parent hwnd to the console window
        /// </summary>
        public static IntPtr ParentHandle
        {
            get
            {
                var hwnd = GetConsoleWindow();
                return GetParent(hwnd);
            }
            set
            {
                var hwnd = Handle;
                if (hwnd == IntPtr.Zero)
                    return;

                SetParent(hwnd, value);
                var style = GetWindowLong(hwnd, GWL_STYLE);
                if (value == IntPtr.Zero)
                    SetWindowLong(hwnd, GWL_STYLE, (style & ~ WS_CHILD) | WS_OVERLAPPEDWINDOW);
                else
                    SetWindowLong(hwnd, GWL_STYLE, (style | WS_CHILD) & ~ WS_OVERLAPPEDWINDOW);
                SetWindowPos(hwnd, IntPtr.Zero, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE);
            }
        }

        /// <summary>
        /// Get the current Win32 buffer handle
        /// </summary>
        public static IntPtr Buffer
        {
            get
            {
                if (!_initialized)
                    Initialize();
                return _buffer;
            }
        }

        /// <summary>
        /// Produces a simple beep.
        /// </summary>
        public static void Beep()
        {
            MessageBeep(-1);
        }

        /// <summary>
        /// Flashes the console window
        /// </summary>
        /// <param name="once">if off, flashes repeated until the user makes the console foreground</param>
        public static void Flash(bool once)
        {
            var hwnd = GetConsoleWindow();
            if (hwnd == IntPtr.Zero)
                return;

            var style = GetWindowLong(hwnd, GWL_STYLE);
            if ((style & WS_CAPTION) == 0)
                return;

            var info = new FlashWInfo {Size = Marshal.SizeOf(typeof (FlashWInfo)), Flags = FLASHW_ALL};
            if (!once)
                info.Flags |= FLASHW_TIMERNOFG;
            FlashWindowEx(ref info);
        }

        /// <summary>
        /// Clear the console window
        /// </summary>
        public static void Clear()
        {
            Initialize();
            ConsoleScreenBufferInfo info;
            int writtenChars;
            GetConsoleScreenBufferInfo(_buffer, out info);
            FillConsoleOutputCharacter(_buffer, ' ', info.Size.X * info.Size.Y, new Coord(), out writtenChars);
            CursorPosition = new Coord();
        }

        /// <summary>
        /// Get the current position of the cursor
        /// </summary>
        /// 
        public static Coord CursorPosition
        {
            get { return Info.CursorPosition; }
            set
            {
                Initialize();
                SetConsoleCursorPosition(_buffer, new Coord());
            }
        }

        /// <summary>
        /// Returns a coordinates of visible window of the buffer
        /// </summary>
        public static SmallRect ScreenSize
        {
            get { return Info.Window; }
        }

        /// <summary>
        /// Returns the size of buffer
        /// </summary>
        public static Coord BufferSize
        {
            get { return Info.Size; }
        }

        /// <summary>
        /// Returns the maximum size of the screen given the desktop dimensions
        /// </summary>
        public static Coord MaximumScreenSize
        {
            get { return Info.MaximumWindowSize; }
        }

        /// <summary>
        /// Redirects debug output to the console
        /// </summary>
        /// <param name="clear">clear all other listeners first</param>
        /// <param name="color">color to use for display debug output</param>
        public void RedirectDebugOutput(bool clear, ConsoleColor color, bool beep)
        {
            if (clear)
                Debug.Listeners.Clear();
                // Debug.Listeners.Remove("Default");
            Debug.Listeners.Add(
                new TextWriterTraceListener(
                    new ConsoleWriter(Console.Error, color, ConsoleFlashMode.FlashUntilResponse, beep), "console"));
        }

        /// <summary>
        /// Redirects trace output to the console
        /// </summary>
        /// <param name="clear">clear all other listeners first</param>
        /// <param name="color">color to use for display trace output</param>
        public void RedirectTraceOutput(bool clear, ConsoleColor color)
        {
            if (clear)
                Trace.Listeners.Clear();
                // Trace.Listeners.Remove("Default");
            Trace.Listeners.Add(new TextWriterTraceListener(new ConsoleWriter(Console.Error, color, 0, false), "console"));
        }

        /// <summary>
        /// Returns various information about the screen buffer
        /// </summary>
        private static ConsoleScreenBufferInfo Info
        {
            get
            {
                var info = new ConsoleScreenBufferInfo();
                var buffer = Buffer;
                if (buffer != IntPtr.Zero)
                    GetConsoleScreenBufferInfo(buffer, out info);
                return info;
            }
        }

        /// <summary>
        /// Gets or sets the current color and attributes of text 
        /// </summary>
        public static ConsoleColor Color
        {
            get { return Info.Attributes; }
            set
            {
                var buffer = Buffer;
                if (buffer != IntPtr.Zero)
                    SetConsoleTextAttribute(buffer, value);
            }
        }

        /// <summary>
        /// Returns true if Ctrl-C or Ctrl-Break was hit since the last time this property
        /// was called. The value of this property is set to false after each request.
        /// </summary>
        public static bool CtrlBreakPressed
        {
            get
            {
                var value = _breakHit;
                _breakHit = false;
                return value;
            }
        }

        private static bool HandleBreak(int type)
        {
            _breakHit = true;
            if (Break != null)
                Break(type);

            return true;
        }

        #endregion

        #region Location

        /// <summary>
        /// Gets the Console Window location and size in pixels
        /// </summary>
        public void GetWindowPosition(out int x, out int y, out int width, out int height)
        {
            var rect = new RECT();
            GetClientRect(Handle, ref rect);
            x = rect.top;
            y = rect.left;
            width = rect.right - rect.left;
            height = rect.bottom - rect.top;
        }

        /// <summary>
        /// Sets the console window location and size in pixels
        /// </summary>
        public void SetWindowPosition(int x, int y, int width, int height)
        {
            SetWindowPos(Handle, IntPtr.Zero, x, y, width, height, SWP_NOZORDER | SWP_NOACTIVATE);
        }

        #endregion
    }
}