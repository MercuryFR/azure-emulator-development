﻿using System;

namespace Azure.AStar.Algorithm
{
    public interface IPathNode
    {
        Boolean IsBlocked(int x, int y, bool lastTile);
    }
}